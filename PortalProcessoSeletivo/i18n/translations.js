[
    {
        "l-Carregando": {
            "pt": "Carregando! Aguarde um instante!",
            "en": "Loading",
			"es": "Cargando"
			
        },"l-carregando": {
            "pt": "Carregando! Aguarde um instante!",
            "en": "Loading",
			"es": "Cargando"
        },
        "l-Menu-Inscricoes": {
            "pt": "INSCRIÇÕES",
            "en": "REGISTRATIONS",
			"es": "INSCRIPCIONES"
        },
        "l-Menu-Informacoes": {
            "pt": "INFORMAÇÕES",
            "en": "INFORMATION",
			"es": "INFORMACIÓN"
        },
        "l-Menu-Resultados": {
            "pt": "RESULTADOS",
            "en": "RESULTS",
			"es": "RESULTADOS"
        },
        "l-Menu-CentralCandidato": {
            "pt": "CENTRAL DO CANDIDATO",
            "en": "CANDIDATE'S CENTER",
			"es": "CENTRAL DEL CANDIDATO"
        },
        "l-seminscricao": {
            "pt": "Você não possui inscrições neste processo seletivo",
            "en": "You do not have any registration in this selective process",
			"es": "Usted no tiene inscripciones en este proceso de selección"
        },
        "l-Proximo": {
            "pt": "Proximo",
            "en": "Next",
			"es": "Proximo"
        },
        "l-Anterior": {
            "pt": "Anterior",
            "en": "Previous",
			"es": "Anterior"
        },
        "l-Menu":{
            "pt": "Menu",
            "en": "Menu",
			"es": "Menú"
        },
        "l-yes": {
            "pt": "Sim",
            "en": "Yes",
            "es": "Sí"
        },
        "l-no": {
            "pt": "Não",
            "en": "No",
            "es": "No"
        },
        "l-Confirmacao":{
            "pt": "Confirmação",
            "en": "Confirmation",
			"es": "Confirmación"
        },
        "l-titulo-erro-login": {
            "pt": "Acesso Expirado",
            "en": "Access Expired",
			"es": "Acceso expirado"
        },
        "l-msg-erro-login": {
            "pt": "Acesso ao portal expirado. É necessário fazer o Login na aplicação novamente!",
            "en": "Access to portal expired. You must log in the application again!",
			"es": "Acceso al portal expirado. ¡Es necesario hacer nuevamente el Login en la aplicación!"
        },
         "btn-close": {
            "pt": "Continuar",
            "en": "Close",
			"es": "Cerrar"
        }
    }
]
