/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsLoginAutomaticoModule
* @name EdupsLoginAutomaticoController
* @object controller
*
* @created 05/01/2017 v12.1.15
* @updated
*
* @requires login.automatico.module
*
* @dependencies edupsLoginAutomaticoFactory
*
* @description Controller da funcionalidade de login automático do novo portal do processo seletivo
*/
define(['login/login.module',
    'login/login.factory'], function () {

    'use strict';

    angular
        .module('edupsLoginModule')
        .controller('EdupsLoginAutomaticoController', EdupsLoginAutomaticoController);

    EdupsLoginAutomaticoController.$inject = [
        '$timeout',
        '$state',
        '$window',
        'i18nFilter',
        '$rootScope',
        'edupsLoginFactory',
        'totvs.app-notification.Service'
    ];

    function EdupsLoginAutomaticoController(
        $timeout,
        $state,
        $window,
        i18nFilter,
        $rootScope,
        edupsLoginFactory,
        totvsNotification
    ) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;

        self.model = {};

        // Variável utilizada na página principal para retirar cabeçalho, banners e rodapé quando
        // a página for aberta através do link de validação de login automático.
        $rootScope.TelaLimpa = true;

        self.validaLogin = validaLogin;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        init();

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            if (angular.isDefined($state.params.token)) {
                validaLogin($state.params.token);
            }
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function validaLogin(token) {
            edupsLoginFactory.validarLoginAutomatico(token,
                function (result) {
                    // Verifica se houve excessões
                    if (!result.LOGADOSUCESSO) {

                        // Caso encontre erro, é necessário setar false nessa variável pois ela
                        // é utilizada na página index.html para esconder o cabeçalho, banner e rodapé.
                        // Ao redirecionar para a página de informações do processo seletivo, tudo deve
                        // ser exibido novamente.
                        $rootScope.TelaLimpa = false;

                        $state.go('login.manual', { action: $state.params.action, nivelEnsino: $rootScope.nivelEnsino });

                    } else {

                        totvsNotification.notify({
                            type: 'success',
                            title: i18nFilter('l-titulo', [], 'js/login'),
                            detail: i18nFilter('l-login-success', [], 'js/login'),
                        });

                        $rootScope.TelaLimpa = false;

                        $state.go('centralcandidato.start', { action: $state.params.action, token: $state.params.token, c: $state.params.c, ps: $state.params.ps, insc: $state.params.insc });
                    }
                });
        }
    }
});
