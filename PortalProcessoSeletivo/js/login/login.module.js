/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsLoginModule
* @object module
*
* @created 02/01/2017 v12.1.15
* @updated
*
* @dependencies totvsHtmlFramework
*
* @description Módulo da funcionalidade de login do novo portal do processo seletivo
*/
define([], function () {

    'use strict';

    angular
        .module('edupsLoginModule', ['totvsHtmlFramework']);

});
