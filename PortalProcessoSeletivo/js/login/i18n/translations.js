[
    {
        "l-login": {
            "pt": "Login",
            "en": "Login",
			"es": "Login"
        },
        "l-tipo-identificacao": {
            "pt": "Tipo identificação",
            "en": "Identification type",
			"es": "Tipo identificación"
        },
        "l-acesso-cpf": {
            "pt": "CPF",
            "en": "CPF",
			"es": "RCPF"
        },
        "l-acesso-rg": {
            "pt": "Carteira de Identidade",
            "en": "Identification Card",
			"es": "Documento de identidad"
        },
        "l-acesso-codusuario": {
            "pt": "Usuário",
            "en": "User",
			"es": "Usuario"
        },
        "l-acesso-email": {
            "pt": "E-mail",
            "en": "E-mail",
			"es": "E-mail"
        },
        "l-senha": {
            "pt": "Senha",
            "en": "Password",
			"es": "Contraseña"
        },
        "l-data-nascimento": {
            "pt": "Data de Nascimento",
            "en": "Birthdate",
			"es": "Fecha de nacimiento"
        },
        "l-entrar": {
            "pt": "Entrar",
            "en": "Enter",
			"es": "Entrar"
        },
        "l-titulo": {
            "pt": "Processo Seletivo",
            "en": "Selective Process",
			"es": "Proceso de selección"
        },
        "l-mostrar-senha": {
            "pt": "Mostrar senha",
            "en": "Display password",
			"es": "Mostrar contraseña"
        },
        "l-esqueci-senha": {
            "pt": "Esqueci minha senha/login",
            "en": "I forgot my password/login",
			"es": "Olvidé mi contraseña/login"
        },
        "l-login-success": {
            "pt": "Acesso realizado com sucesso!",
            "en": "Access successfully executed!",
			"es": "¡Acceso realizado con éxito!"
        },
        "l-voltar-login": {
            "pt": "voltar ao login",
            "en": "back to login",
			"es": "volver al login"
        },
        "l-enviar-senha": {
            "pt": "Receber senha",
            "en": "Receive password",
			"es": "Recibir contraseña"
        },
        "l-campos-obrigatorios": {
            "pt": "Os campos a seguir devem ser preenchidos: ",
            "en": "The following fields must be completed: ",
			"es": "Deben completarse los siguientes campos: "
        },
        "l-atencao":{
            "pt": "ATENÇÃO",
            "en": "ATTENTION",
			"es": "ATENCIÓN"
        },
        "l-recuperacao-senha-success": {
            "pt": "Email enviado com sucesso.",
            "en": "E-mail successfully sent.",
			"es": "E-mail enviado con éxito."
        }
    }
]
