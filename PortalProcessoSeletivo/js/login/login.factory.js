/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsLoginModule
* @name edupsLoginFactory
* @object factory
*
* @created 02/01/2017 v12.1.15
* @updated
*
* @requires edupsLoginModule
*
* @description Factory utilizada na funcionalidade de login do novo portal do processo seletivo
*/
define(['login/login.module'], function () {

	'use strict';

	angular
		.module('edupsLoginModule')
		.factory('edupsLoginFactory', EdupsLoginFactory);

	EdupsLoginFactory.$inject = ['$totvsresource', '$filter'];

	function EdupsLoginFactory($totvsresource, $filter) {

		var self = this,
			url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS;

		self.realizarLogin = realizarLogin;
		self.validarLoginAutomatico = validarLoginAutomatico;
		self.realizarLogout = realizarLogout;
		self.recuperarSenha = recuperarSenha;
		self.alterarSenha = alterarSenha;
		self.buscaDadosUsuario = buscaDadosUsuario;

		return self;

        /**
         * Retorna o objeto factory a partir da URL padrão juntamente ao complemento
         *
         * @param {any} urlComplemento - Url de complemento
         * @returns objeto factory
         */
		function getInstanciaFactory(urlComplemento) {
			var urlCompletaServico = url + urlComplemento;
			return $totvsresource.REST(urlCompletaServico, {}, {});
		}

        /**
         * Realiza o login no portal do candidato
         *
         * @param {any} login - login do candidato
         * @param {any} senha - senha do candidato
         * @param {any} tipoIdentificacao - tipo de idenficação: 0 - CPF | 1 - RG | 2 - Usuário | 3 - Email
         * @param {any} codColigada - código da coligada
         * @param {any} codFilial - código da filial
         * @param {any} idPS  - identificação do processo seletivo
         * @param {any} utilizaCampoSenha  - utiliza campo senha
         * @param {any} callback - metódo callback
         * @returns  objeto factory
         */
		function realizarLogin(login, senha, tipoIdentificacao, codColigada, codFilial, idPS, utilizaCampoSenha, callback) {

			var parametros = {},
				factory = getInstanciaFactory('/Login');

			parametros.login = login;
			parametros.senha = senha;

			//Verifica se a senha informada é do tipo data
			if (typeof(parametros.senha.getDate) === 'function') {
				var dateString = $filter('date')(parametros.senha, 'dd/MM/yyyy');
				parametros.senha = dateString;
			}

			parametros.tipoIdentificacao = tipoIdentificacao;
			parametros.codColigada = codColigada;
			parametros.codFilial = codFilial;
			parametros.idPS = idPS;

			return factory.TOTVSPost(parametros, {}, callback);
		}

        /**
         * Realiza o login no portal do candidato
         *
         * @param {any} login - login do candidato
         * @param {any} tipoIdentificacao - tipo de idenficação: 0 - CPF | 1 - RG | 2 - Usuário | 3 - Email
         * @param {any} codColigada - código da coligada
         * @param {any} codFilial - código da filial
         * @param {any} dataNascimento  - data de nascimento no formato dd/mm/yyyy
         * @param {any} idPS  - identificador do processo seletivo
         * @param {any} callback - metódo callback
         * @returns  objeto factory
         */
		function recuperarSenha(login, tipoIdentificacao, codColigada, codFilial, dataNascimento, idPS, callback) {

			var parametros = {},
				factory = getInstanciaFactory('/RecuperarSenha');

			parametros.login = login;
			parametros.tipoIdentificacao = tipoIdentificacao;
			parametros.codColigada = codColigada;
			parametros.codFilial = codFilial;
			parametros.dataNascimento = dataNascimento;
			parametros.idPS = idPS;

			return factory.TOTVSPost(parametros, {}, callback);
		}

        /**
         * Valida o login automático a partir do token.
         *
         * @param {any} token - token de login
         * @param {any} callback - método callback
         * @returns objeto factory
         */
		function validarLoginAutomatico(token, callback) {
			var factory = getInstanciaFactory('/LoginAutomatico'),
				parametros = {};

			parametros.token = token;

			return factory.TOTVSGet(parametros, callback);
		}

        /**
         * Realiza o logout na aplicação
         *
         * @param {any} callback - método callback
         * @returns objeto factory
         */
		function realizarLogout(callback) {
			var parametros = {},
				factory = getInstanciaFactory('/Logout');

			return factory.TOTVSGet(parametros, callback);
		}

        /**
         * Alterar senha do usuário
         *
         * @param {any} senha - senha
         * @param {any} novaSenha - nova senha
         * @param {any} confirmacaoNovaSenha - confirmação da nova senha
         * @param {any} callback - callback
         * @returns
         */
		function alterarSenha(senha, novaSenha, confirmacaoNovaSenha, callback) {

			var factory = getInstanciaFactory(''),
				parametros = {},
				dados = {};

			parametros.method = 'AlterarSenha';

			dados.novaSenha = novaSenha;
			dados.confirmacaoNovaSenha = confirmacaoNovaSenha;
			dados.senha = senha;

			return factory.TOTVSUpdate(parametros, dados, callback);

		}

        /**
         * Busca os dados pessoais do usuário
         *
         * @returns
         */
		function buscaDadosUsuario(callback) {
			var factory = getInstanciaFactory('/Inscricao/BuscaUsuario');
			return factory.TOTVSGet({}, callback);
		}
	}
});
