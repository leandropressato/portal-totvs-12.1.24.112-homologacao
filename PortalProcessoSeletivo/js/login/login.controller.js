/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsLoginModule
 * @name EdupsLoginController
 * @object controller
 *
 * @created 02/01/2017 v12.1.15
 * @updated
 *
 * @requires login.module
 *
 * @dependencies edupsLoginFactory
 *
 * @description Controller da funcionalidade de login do novo portal do processo seletivo
 */
define(['login/login.module',
    'login/login.factory'
], function() {

    'use strict';

    angular
        .module('edupsLoginModule')
        .controller('EdupsLoginController', EdupsLoginController);

    EdupsLoginController.$inject = [
        '$window',
        'i18nFilter',
        '$rootScope',
        '$scope',
        'edupsLoginFactory',
        'totvs.app-notification.Service',
        '$state'
    ];

    function EdupsLoginController(
        $window,
        i18nFilter,
        $rootScope,
        $scope,
        edupsLoginFactory,
        totvsNotification,
        $state
    ) {
        var self = this;
        self.model = {};
        self.listaTipoIdentificacao = [];
        self.recuperarSenha = false; // a tela está aberta em modo de recuperação de senha

        self.labelIdentificacao;
        self.login = login;
        self.redirectRecuperarSenha = redirectRecuperarSenha;
        self.recuperarSenhaClick = recuperarSenhaClick;
        self.defineLabelIdentificacao = defineLabelIdentificacao;
        self.getNivelEnsino = getNivelEnsino;
        self.redirectLogin = redirectLogin;
        self.objInscricao = {};

        // Só executa o método init após carregar o objeto com os parâmetros
        var myWatch = $scope.$watch('objParametros', function(data) {
            if (angular.isDefined(data) && angular.isDefined(data.NomePortalInscricoes)) {
                self.objParametros = data;
                preInit();
                myWatch();
            }
        });

        function preInit() {
            if ($state.params.expired === 1) {
                totvsNotification.message({
                    title: i18nFilter('l-titulo-erro-login'),
                    text: i18nFilter('l-msg-erro-login'),
                    size: 'md'
                });
            }
            realizarLogout(init);
        }

        //A view de login é única para os dois níveis de ensino.
        //é preciso saber qual Menu irá exibir.
        function getNivelEnsino() {
            var nivelEnsino = document.eduPSNivelEnsino;
            return nivelEnsino;
        }

        function init() {
            self.recuperarSenha = ($state.params.recuperarSenha === 1);
            self.objInscricao = $state.params.objInscricao;

            var myWatch = $scope.$watch('listaTipoIdentificacaoLogin', function(data) {
                if (angular.isArray(data)) {
                    if ($rootScope.listaTipoIdentificacaoLogin.length > 0) {
                        self.model.TIPOIDENTIFICACAO = $rootScope.listaTipoIdentificacaoLogin[0].CODIGO;
                    }
                    myWatch();
                }
            });
        }

        function defineLabelIdentificacao(){
            angular.forEach($rootScope.listaTipoIdentificacaoLogin, function(value){
                if ( value.CODIGO == self.model.TIPOIDENTIFICACAO ) 
                    self.labelIdentificacao = value.DESCRICAO;
            });
        }

        /**
         * Valida os dados do login
         *
         * @param {any} usaSenhaLogin
         * @param {any} login
         * @param {any} senha
         */
        function validarDadosLogin(usaSenhaLogin, login, senha) {

            var msg;

            if (!login || !senha) {

                msg = i18nFilter('l-campos-obrigatorios', [], 'js/login');

                if (!login) {
                    msg += i18nFilter('l-login', [], 'js/login');

                    if (!senha) {
                        msg += ', ';
                    }
                }

                if (!senha) {
                    if (usaSenhaLogin) {
                        msg += i18nFilter('l-senha', [], 'js/login');
                    } else {
                        msg += i18nFilter('l-data-nascimento', [], 'js/login');
                    }
                }

                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/login'),
                    detail: msg
                });

            }

            return !(msg);

        }

        /**
         * Realiza o login no portal do aluno
         *
         * @param {any} login Login
         * @param {any} senha Senha
         * @param {any} tipoIdentificacao Tipo de Identificação
         * @param {any} usaSenhaLogin Usa senha para logar
         */
        function login(login, senha, tipoIdentificacao, usaSenhaLogin) {

            if (validarDadosLogin(usaSenhaLogin, login, senha)) {
                edupsLoginFactory.realizarLogin(login, senha, tipoIdentificacao, $rootScope.CodColigada, $rootScope.CodFilial, $rootScope.IdPS, usaSenhaLogin,
                    function(result) {
                        if (result && result.LOGADOSUCESSO) {
                            totvsNotification.notify({
                                type: 'success',
                                title: i18nFilter('l-titulo', [], 'js/login'),
                                detail: i18nFilter('l-login-success', [], 'js/login'),
                            });

                            RedirecionaParaAction(result.CODUSUARIOPS);
                        }
                    });
            }

        }

        function RedirecionaParaAction(codUsuarioPS) {
            if (codUsuarioPS) {
                var params = {
                    nivelEnsino: $rootScope.nivelEnsino,
                    action: $state.params.action,
                    objInscricao: {},
                    inscricaoAposLogin: false
			    };

				if (angular.isDefined(self.objInscricao.souCandidato)) {
					params.objInscricao.souCandidato = self.objInscricao.souCandidato;
				}

                if ($state.params.action === 'inscricoesWizard') {
                    params.inscricaoAposLogin = true;
                    $state.go('inscricoesWizard.dados-basicos', params);
                } else {
                    $state.go('centralcandidato.start', params);
                }
            }
        }

        /**
         * Redireciona para recuperação de senha
         *
         */
        function redirectRecuperarSenha() {
            var params = {
                recuperarSenha: 1,
                nivelEnsino: $rootScope.nivelEnsino
            };
            $state.go('login.manual', params);
        }

        /**
         * Valida os dados da recuperação de senha
         *
         * @param {any} login
         * @param {any} dataNascimento
         */
        function validarDadosRecuperarSenha(login, dataNascimento) {

            var msg;

            if (!login || !dataNascimento) {

                msg = i18nFilter('l-campos-obrigatorios', [], 'js/login');

                if (!login) {
                    msg += i18nFilter('l-login', [], 'js/login');

                    if (!dataNascimento) {
                        msg += ', ';
                    }
                }

                if (!dataNascimento) {
                    msg += i18nFilter('l-data-nascimento', [], 'js/login');
                }

                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/login'),
                    detail: msg
                });

            }

            return !(msg);

        }

        /**
         * Recuperar senha de um usuário
         *
         * @param {any} login nome do usuário
         * @param {any} tipoIdentificacao tipo de identificação de usuário
         * @param {any} dataNascimento data de nascimento
         */
        function recuperarSenhaClick(login, tipoIdentificacao, dataNascimento) {

            if (validarDadosRecuperarSenha(login, dataNascimento)) {
                edupsLoginFactory.recuperarSenha(login, tipoIdentificacao, $rootScope.CodColigada, $rootScope.CodFilial, dataNascimento, $rootScope.IdPS,
                    function(result) {
                        if (result.value === true) {
                            totvsNotification.notify({
                                type: 'success',
                                title: i18nFilter('l-titulo', [], 'js/login'),
                                detail: i18nFilter('l-recuperacao-senha-success', [], 'js/login'),
                            });

                            var params = {
                                action: $state.params.action,
                                recuperarSenha: 0,
                                nivelEnsino: $rootScope.nivelEnsino
                            };

                            $state.go('login.manual', params);
                        }
                    });
            }

        }

        /**
         * Redireciona para o login
         *
         */
        function redirectLogin() {
            var params = {
                recuperarSenha: 0,
                nivelEnsino: $rootScope.nivelEnsino
            };
            $state.go('login.manual', params);
        }

        function realizarLogout(callback) {
            edupsLoginFactory.realizarLogout(callback);
        }
    }
});
