/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsLoginModule
* @object routeConfig
*
* @created 02/01/2017 v12.1.15
* @updated
*
* @dependencies $stateProvider
*
* @description Rotas da funcionalidade de login do novo portal do processo seletivo
*/
define(['login/login.module'],function () {

    'use strict';

    angular
        .module('edupsLoginModule')
        .config(edupsLoginRouteConfig);

    edupsLoginRouteConfig.$inject = ['$stateProvider'];

    function edupsLoginRouteConfig($stateProvider) {

        $stateProvider.state('login', {
            abstract: true,
            template: '<ui-view/>'

        }).state('login.manual', {
            url: '/:nivelEnsino/login?{action:string}&{expired:int}&{recuperarSenha:int}',
            controller: 'EdupsLoginController',
            params: {
                action: '',
                expired: 0,
                recuperarSenha: 0,
                objInscricao: {}
            },
            controllerAs: 'controller',
            templateUrl: function () {
                if (EDUPS_CONST_GLOBAL_CUSTOM_VIEW_LOGIN === false)
                {
                    return 'js/templates/login.edit.view.html';
                }
                else
                {
                    return 'js/templates/custom/login.edit.view.html';
                }
            },
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsLoginModule',
                        files: ['js/login/login.controller.js']
                    }]);
                }]
            }
        }).state('login.automatico', {
            url: '/:nivelEnsino/validalogin?{token:string}&{action:string}&{c:string}&{ps:string}&{insc:string}',
            controller: 'EdupsLoginAutomaticoController',
            params: {
                token: '',
                action: ''
            },
            controllerAs: 'controller',
            templateUrl: function () {
                if (EDUPS_CONST_GLOBAL_CUSTOM_VIEW_LOGIN === false)
                {
                    return 'js/templates/loginautomatico.edit.view.html';
                }
                else
                {
                    return 'js/templates/custom/loginautomatico.edit.view.html';
                }
            },
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsLoginAutomaticoModule',
                        files: ['js/login/login.automatico.controller.js']
                    }]);
                }]
            }
        });
    }
});
