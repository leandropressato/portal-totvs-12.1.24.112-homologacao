/**
 * @license TOTVS | Portal Processo Seletivo v12.1.18
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsResultadosModule
 * @name EdupsResultadosService
 * @object service
 *
 * @created 25/07/2017 v12.1.18
 * @updated
 *
 * @requires
 *
 * @description Service da funcionalidade Resultados do Processo Seletivo, utilizada por todos os níveis de ensino.
 *              Todos os métodos considerados comuns entre os níveis de ensino devem ser colocados aqui e consumidos
 *              pelos controllers específicos de cada nível de ensino.
 */

define(['resultados/resultados.module',
        'utils/edups-utils.factory'], function () {

    'use strict';

    angular
        .module('edupsResultadosModule')
        .service('EdupsResultadosService', EdupsResultadosService);

    EdupsResultadosService.$inject = ['$rootScope', '$state', 'edupsUtilsFactory'];

    function EdupsResultadosService($rootScope, $state, EdupsUtilsFactory) {

        var self = this;

        self.listarArquivosPS = listarArquivosPS;
        self.baixarArquivo = baixarArquivo;
        self.efetuarLogin = efetuarLogin;
        self.separaBarraServico = separaBarraServico;
        self.isDefinedNotNull = isDefinedNotNull;

        function listarArquivosPS(codColigada, idPS, idCategoria, callback) {
            var listaArquivosProcessoSeletivo = [];

            EdupsUtilsFactory.getListaArquivosProcessoSeletivo(codColigada, idPS, idCategoria, function (result) {
                if ((angular.isDefined(result)) && (result.length > 0)) {
                    listaArquivosProcessoSeletivo = result;
                }

                if (typeof callback === 'function') {
                    callback(listaArquivosProcessoSeletivo);
                }
            });
        }

        function baixarArquivo(idArquivo, formaArmazenamento, pathArquivo, nomeArquivo) {
            var link = document.createElement('a');
            link.download = nomeArquivo;
            link.href = EdupsUtilsFactory.getArquivoDownload(idArquivo, formaArmazenamento, pathArquivo);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        function efetuarLogin(tipoServico) {
            var params = { action: tipoServico };

            $state.go('centralcandidato.start', params);
        }

        function separaBarraServico() {
            $rootScope.ExibeServicoAlteracaoInformacoesPessoais = ($rootScope.objParametros.ServicoDeResultado.indexOf('0') != -1);
            $rootScope.ExibeServicoAlteracaoSenha = ($rootScope.objParametros.ServicoDeResultado.indexOf('1') != -1);
            $rootScope.ExibeServicoSegundaViaComprovante = ($rootScope.objParametros.ServicoDeResultado.indexOf('4') != -1);
            $rootScope.ExibeServicoConsultaSituacaoCandidato = ($rootScope.objParametros.ServicoDeResultado.indexOf('5') != -1);

            $rootScope.ExibeBarraServico = $rootScope.ExibeServicoAlteracaoInformacoesPessoais ||
                                           $rootScope.ExibeServicoAlteracaoSenha ||
                                           $rootScope.ExibeServicoSegundaViaComprovante ||
                                           $rootScope.ExibeServicoConsultaSituacaoCandidato;
        }

        /**
         * Verifica se o objeto isDefined e diferente de nulo
         * @param {any} objeto
         * @returns
         */
        function isDefinedNotNull (objeto) {
            return (angular.isDefined(objeto) && objeto !== null);
        }
    }
});
