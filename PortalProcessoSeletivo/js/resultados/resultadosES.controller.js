/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsResultadosModule
* @name EdupsResultadosESController
* @object controller
*
* @created 14/09/2016 v12.1.15
* @updated
*
* @requires resultados.module
*
* @dependencies edupsResultadosService
*
* @description Controller da funcionalidade Resultados do Processo Seletivo (ENSINO SUPERIOR).
*              Qualquer regra específica do nível de ensino superior, relacionado a funcionalidade
*              de resultado, deve ser colocada aqui nesse controller.
*/
define(['resultados/resultados.module',
        'resultados/resultados.service'], function () {

    'use strict';

    angular
        .module('edupsResultadosModule')
        .controller('EdupsResultadosESController', EdupsResultadosESController);

    EdupsResultadosESController.$inject = ['EdupsResultadosService', '$rootScope', '$scope'];

    function EdupsResultadosESController(EdupsResultadosService, $rootScope, $scope) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.listaArquivosProcessoSeletivo = [];

        self.efetuarLogin = efetuarLogin;
        self.baixarArquivo = EdupsResultadosService.baixarArquivo;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        // Só executa o método init após carregar o objeto com os parâmetros
        var myWatch = $scope.$watch('objParametros', function (data) {
            if (EdupsResultadosService.isDefinedNotNull(data) && EdupsResultadosService.isDefinedNotNull(data.NomePortalInscricoes)) {

                self.objParametros = data;

                init();
                myWatch();
            }
        });

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            EdupsResultadosService.listarArquivosPS($rootScope.CodColigada, $rootScope.IdPS, 2, function (result) {
                self.listaArquivosProcessoSeletivo = result;
            });

            EdupsResultadosService.separaBarraServico();
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function efetuarLogin(tipoServico) {
            EdupsResultadosService.efetuarLogin(tipoServico);
        }
    }
});
