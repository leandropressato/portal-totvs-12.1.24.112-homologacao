[
    {
        "l-Titulo-Arquivos": {
            "pt": "ARQUIVOS",
			"en": "FILES",
			"es": "ARCHIVOS"
        },
        "l-Download-Arquivos":{
            "pt": "Faça o download do(s) arquivo(s) para imprimir",
			"en": "Download the file(s) to print",
			"es": "Haga el download de el(los) archivo(s) para imprimir"
        },
        "l-editar-inscricao": {
            "pt": "Editar dados pessoais",
			"en": "Edit personal data",
			"es": "Editar datos personales"
        },
        "l-alterar-senha": {
            "pt": "Alterar senha",
			"en": "Edit password",
			"es": "Modificar contraseña"
        },
        "l-2a-via": {
            "pt": "2ª Via comprovante",
			"en": "Second copy of receipt",
			"es": "Copia del comprobante"
        },
        "l-checar-status": {
            "pt": "Checar status",
			"en": "Institutional Assessment",
			"es": "Verificar estatus"
        },
        "l-servico-altera-dados": {
            "pt": "Acesse a Central do Candidato para atualizar o seu cadastro.",
			"en": "Access the Candidate Center to update your record.",
			"es": "Acceda a la Central del candidato para actualizar su registro."
        },
        "l-servico-altera-senha": {
            "pt": "Acesse a Central do Candidato para alterar a sua senha de acesso.",
			"en": "Access the Candidade Center to edit your access password.",
			"es": "Acceda a la Central del candidato para modificar su contraseña de acceso."
        },
        "l-servico-comprovante-inscricao": {
            "pt": "Acesse a Central do Candidato para imprimir a segunda via do comprovante de inscrição.",
			"en": "Access the Candidate Center to print the second copy of the registration receipt.",
			"es": "Acceda a la Central del candidato para imprimir la copia del comprobante de inscripción."
        },
        "l-servico-status": {
            "pt": "Acesse a Central do Candidato para acompanhar o seu status e andamento das etapas no Processo Seletivo.",
			"en": "Access the Candidate Center to follow your status and progress of the stages in the Selective Process.",
			"es": "Acceda a la Central del candidato para acompañar su estatus y el progreso de las etapas en el Proceso de selección."
        },
        "l-acesso-rapido-voce-candidato": {
            "pt": "Para você que já é candidato",
			"en": "For you who are already a candidate",
			"es": "Para usted, que ya es candidato"
        }
    }
]
