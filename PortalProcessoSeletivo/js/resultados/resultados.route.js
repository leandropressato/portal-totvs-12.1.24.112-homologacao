/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsResultadosModule
* @object routeConfig
*
* @created 14/09/2016 v12.1.15
* @updated
*
* @dependencies $stateProvider
*
* @description Rotas da funcionalidade Resultados do Processo Seletivo
*/
define(['resultados/resultados.module', 'utils/edups-utils.globais'],function () {

    'use strict';

    angular
        .module('edupsResultadosModule')
        .config(edupsResultadosRouteConfig);

    edupsResultadosRouteConfig.$inject = ['$stateProvider'];

    function edupsResultadosRouteConfig($stateProvider) {

        $stateProvider.state('resultados', {
            abstract: true,
            template: '<ui-view/>'

        }).state('resultados.start', {
            url: '/:nivelEnsino/resultados',
            controller: 'EdupsResultados' + document.eduPSNivelEnsino.toUpperCase() + 'Controller',
            controllerAs: 'controller',
            templateUrl: function () {

                var utilGlobais = requirejs('utils/edups-utils.globais');
                var baseTemplate = 'js/templates/resultados-';

                if (utilGlobais.validaViewCustomizada('resultados.start')) {
                    baseTemplate = 'js/templates/custom/resultados-';
                }

                return baseTemplate + document.eduPSNivelEnsino + '-list.view.html';
            },
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsResultadosModule',
                        files: ['js/resultados/resultados' + document.eduPSNivelEnsino.toUpperCase() + '.controller.js']
                    }]);
                }]
            }
        });
    }
});
