/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsCustomModule
* @name edupsCustomFactory
* @object factory
*
* @created 12/12/2018 v12.1.17
* @updated
*
* @requires edupsCustomModule
*
* @description Factory utilizada nas funcionalidades Customizadas do Processo Seletivo
*/
define(['totvs-custom/custom.module'], function () {

    'use strict';

    angular
        .module('edupsCustomModule')
        .factory('edupsCustomFactory', EdupsCustomFactory);

    EdupsCustomFactory.$inject = ['$totvsresource'];

    function EdupsCustomFactory($totvsresource) {
        // o método precisa de um return falso para não dar erro
        // o mesmo será sobrescrito quando houver customização
        return '';
    }
});
