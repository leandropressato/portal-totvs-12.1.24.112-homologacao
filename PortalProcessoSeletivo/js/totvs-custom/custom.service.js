/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['totvs-custom/custom.module', 'totvs-custom/custom.factory'], function () {

    'use strict';

    angular
        .module('edupsCustomModule')
        .service('edupsCustomService', edupsCustomService);

    edupsCustomService.$inject = ['$totvsresource', 'edupsCustomFactory', '$compile'];

    function edupsCustomService($totvsresource, EdupsCustomFactory, $compile, totvsNotification) {

        var self = this;
        self.initPost = initPost;
        self.initPre = initPre;
        return self;

        //Evento Pre da inicialização do controller para customização
        function initPre(controller, scope) {

        }

        //Evento Post da inicialização do controller para customização
        function initPost(controller, scope) {

        }
    }
});
