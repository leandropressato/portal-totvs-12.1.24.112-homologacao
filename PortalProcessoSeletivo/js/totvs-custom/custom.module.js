/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsCustomModule
* @object module
*
* @created 12/12/2018 v12.1.17
* @updated
*
* @dependencies
*
* @description Módulo da Customização.
*/
define([], function () {

    'use strict';

    angular
        .module('edupsCustomModule', []);
});
