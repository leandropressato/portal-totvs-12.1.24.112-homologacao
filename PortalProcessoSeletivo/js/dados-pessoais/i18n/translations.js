[
    {
        "l-salvar": {
            "pt": "Salvar",
			"en": "Save",
			"es": "Grabar"
        },
        "l-editar":{
            "pt": "Alterar dados pessoais",
			"en": "Edit personal data",
			"es": "Modificar datos personales"
        },
        "l-cancelar":{
            "pt": "Cancelar",
			"en": "Cancel",
			"es": "Anular"
        }
    }
]
