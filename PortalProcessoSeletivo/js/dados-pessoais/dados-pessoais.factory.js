/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsDadosPessoaisModule
* @name edupsDadosPessoaisFactory
* @object factory
*
* @created 20/04/2017 v12.1.17
* @updated
*
* @requires edupsDadosPessoaisModule
*
* @description Factory utilizada na funcionalidade Central do Candidato.
*/
define(['dados-pessoais/dados-pessoais.module'], function () {

    'use strict';

    angular
        .module('edupsDadosPessoaisModule')
        .factory('edupsDadosPessoaisFactory', edupsDadosPessoaisFactory);

    edupsDadosPessoaisFactory.$inject = ['$totvsresource'];

    function edupsDadosPessoaisFactory($totvsresource) {
        var factory,
            url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS;

        factory = $totvsresource.REST(url, {}, {});

        factory.getDadosPessoaisCandidato = getDadosPessoaisCandidato;
        factory.getDadosPessoaisResponsavel = getDadosPessoaisResponsavel;
        factory.saveDadosPessoaisCandidato = saveDadosPessoaisCandidato;
        factory.saveDadosPessoaisResponsavel = saveDadosPessoaisResponsavel;

        return factory;

        // /**
        //  * Retorna os dados pessoais do candidato logado.
        //  *
        //  * @param {any} callback função de retorno
        //  * @returns objeto contendo os dados pessoais
        //  */
        // function getDadosPessoaisCandidato(callback) {
        //     var parametros = {};
        //     parametros.method = 'DadosPessoaisCandidato';

        //     return factory.TOTVSGet(parametros, callback);
        // }

        /**
         * Retorna os dados pessoais do candidato logado.
         *
         * @param {any} callback função de retorno
         * @returns objeto contendo os dados pessoais
         */
        function getDadosPessoaisResponsavel(callback) {
            var parametros = {};
            parametros.method = 'DadosPessoaisResponsavel';

            return factory.TOTVSGet(parametros, callback);
        }        

        /**
         * Retorna os dados pessoais do candidato logado.
         *
         * @param {any} callback função de retorno
         * @returns objeto contendo os dados pessoais
         */
        function getDadosPessoaisCandidato(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'DadosPessoaisCandidato';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSGet(parametros, callback);
        }

        /**
         * Salva os dados pessoais do candidato
         *
         * @param {any} objDadosPessoais - objeto contendo os dados pessoais
         * @param {any} callback - função de retorno
         * @returns objeot alterado
         */
        function saveDadosPessoaisCandidato(objDadosPessoais, callback) {
            var parametros = {};
            parametros.method = 'DadosPessoaisCandidato';
            
            return factory.TOTVSUpdate(parametros, objDadosPessoais, callback);
        }

        /**
         * Salva os dados pessoais do candidato
         *
         * @param {any} objDadosPessoais - objeto contendo os dados pessoais
         * @param {any} callback - função de retorno
         * @returns objeot alterado
         */
        function saveDadosPessoaisResponsavel(objDadosPessoais, callback) {
            var parametros = {};
            parametros.method = 'DadosPessoaisResponsavel';
            
            return factory.TOTVSUpdate(parametros, objDadosPessoais, callback);
        }        
    }

});
