/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsDadosPessoaisModule
* @object module
*
* @created 20/04/2017 v12.1.17
* @updated
*
* @dependencies totvsHtmlFramework
*
* @description Módulo da funcionalidade de dados pessoais
*/
define([], function () {

    'use strict';

    angular
        .module('edupsDadosPessoaisModule', ['totvsHtmlFramework']);

});
