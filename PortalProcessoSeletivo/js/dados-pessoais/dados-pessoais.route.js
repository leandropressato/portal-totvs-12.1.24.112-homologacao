/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsDadosPessoaisModule
 * @object routeConfig
 *
 * @created 20/04/2017 v12.1.17
 * @updated
 *
 * @dependencies $stateProvider
 *
 * @description Rotas da funcionalidade Informações da Central do Candidato
 */
define(['dados-pessoais/dados-pessoais.module'], function() {

    'use strict';

    angular
        .module('edupsDadosPessoaisModule')
        .config(edupsDadosPessoaisRouteConfig);

    edupsDadosPessoaisRouteConfig.$inject = ['$stateProvider'];

    function edupsDadosPessoaisRouteConfig($stateProvider) {

        $stateProvider.state('dadospessoais', {
            abstract: true,
            template: '<ui-view/>'

        }).state('dadospessoais.start', {
            url: '/:nivelEnsino/dadospessoais?{isResponsavel:bool}&{numeroInscricao:int}',
            controller: 'edupsDadosPessoaisController',
            controllerAs: 'controller',
            templateUrl: function() {
                return 'js/templates/dados-pessoais-edit.view.html';
            },
            resolve: {
                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsDadosPessoaisModule',
                        files: ['js/dados-pessoais/dados-pessoais.controller.js']
                    }]);
                }]
            }
        });
    }
});
