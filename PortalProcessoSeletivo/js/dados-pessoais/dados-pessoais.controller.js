/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsDadosPessoaisModule
 * @name edupsDadosPessoaisController
 * @object controller
 *
 * @created 20/04/2017 v12.1.17
 * @updated
 *
 * @requires edupsDadosPessoaisModule
 *
 * @dependencies edupsCentralCandidatoFactory
 *
 * @description Controller da funcionalidade Informações da Central do Candidato
 */
define(['dados-pessoais/dados-pessoais.module',
    'dados-pessoais/dados-pessoais.factory',
    'utils/edups-utils.factory',
    'utils/edups-enums.constants',
    'utils/edups-utils.service'
], function() {

    'use strict';

    angular
        .module('edupsDadosPessoaisModule')
        .controller('edupsDadosPessoaisController', edupsDadosPessoaisController);

    edupsDadosPessoaisController.$inject = [
        '$rootScope',
        '$scope',
        '$state',
        '$modal',
        '$window',
        'edupsDadosPessoaisFactory',
        'edupsUtilsFactory',
        'edupsUtilsService',
        'edupsEnumsConsts',
        'totvs.app-notification.Service',
        '$filter',
        '$compile',
        '$timeout',
        'i18nFilter'
    ];

    function edupsDadosPessoaisController(
        $rootScope,
        $scope,
        $state,
        $modal,
        $window,
        edupsDadosPessoaisFactory,
        EdupsUtilsFactory,
        EdupsUtilsService,
        edupsEnumsConsts,
        totvsNotification,
        $filter,
        $compile,
        $timeout,
        i18nFilter
    ) {

        /**
         * VARIÁVEIS
         */
        var self = this;

        self.objParametros = {};
        self.dadosPessoais = {};
        self.objCamposCompl = {};
        self.listaPaises = [];
        self.listaEstados = [];
        self.listaMunicipios = [];
        self.listaCorRaca = [];
        self.listaEstadoCivil = [];
        self.listaGrauInstrucao = [];
        self.listaNacionalidade = [];
        self.listaProfissao = [];
        self.listaSituacaoMilitar = [];
        self.listaTipoBairro = [];
        self.listaTipoRua = [];
		self.listaTipoSanguineo = [];
		self.camposCPFPassRGObrigatorio = [];

        self.listaSexo = [{
            value: 'F',
            label: i18nFilter('l-sexo-feminino', [], 'js/inscricoes')
        }, {
            value: 'M',
            label: i18nFilter('l-sexo-masculino', [], 'js/inscricoes')
		}];

        /**
         * MÉTODOS
         */
        self.campoDocumentoObrigatorio = campoDocumentoObrigatorio;
        self.campoObrigatorio = campoObrigatorio;
        self.campoVisivel = campoVisivel;
        self.exibeGrupoCampos = exibeGrupoCampos;
        self.exibeGrupoCamposComplementares = self.exibeGrupoCamposComplementares;
        self.getDadosPessoais = getDadosPessoais;
        self.getListaPaises = getListaPaises;
        self.getListaEstados = getListaEstados;
        self.getListaMunicipios = getListaMunicipios;
        self.getListaCorRaca = getListaCorRaca;
        self.getListaEstadoCivil = getListaEstadoCivil;
        self.getListaGrauInstrucao = getListaGrauInstrucao;
        self.getListaNacionalidade = getListaNacionalidade;
        self.getListaProfissao = getListaProfissao;
        self.getListaSituacaoMilitar = getListaSituacaoMilitar;
        self.getListaTipoBairro = getListaTipoBairro;
        self.getListaTipoRua = getListaTipoRua;
        self.getListaTipoSanguineo = getListaTipoSanguineo;
        self.isEstrangeiro = isEstrangeiro;
        self.salvarDadosPessoais = salvarDadosPessoais;
        self.cancelar = cancelar;
        self.getModelCampo = getModelCampo;
		self.buscarEnderecoPorCEP = buscarEnderecoPorCEP;
        self.setCamposCPFPassRGObrigatorio = setCamposCPFPassRGObrigatorio;
        self.concatenaName = concatenaName;

        /**
         * Inicializa o controller
         *
         */
        $rootScope.contarListaCarregadas = 6;

        var myWatch = $scope.$watch('objParametros', function(data) {
            if (angular.isDefined(data) && angular.isDefined(data.NomePortalInscricoes)) {
                self.objParametros = data;
                init();
                myWatch();
            }
        });

        function init() {

            carregarListas(function() {
                if ($state.params) {
                    self.numeroInscricao = $state.params.numeroInscricao;
                    self.isResponsavel = $state.params.isResponsavel;

                    getDadosPessoais(self.isResponsavel);
                }
            });
        }

        function toggleCollapse() {

            $timeout(function() {
                $('#dadosCandidato').collapse('show');

                $('#dadosCandidato')
                    .on('shown.bs.collapse', function() {
                        $('.cabecalho-grupo').find('.glyphicon-chevron-down').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
                    });

                $('#dadosCandidato')
                    .on('hidden.bs.collapse', function() {
                        $('.cabecalho-grupo').find('.glyphicon-chevron-up').addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
                    });
            }, 0);

        }

        function carregarListas(callback) {
            self.getListaPaises(function(result) {
                self.listaPaises = result;

                self.getListaCorRaca();
                self.getListaEstadoCivil();
                self.getListaGrauInstrucao();
                self.getListaNacionalidade();
                self.getListaProfissao();
                self.getListaSituacaoMilitar();
                self.getListaTipoBairro();
                self.getListaTipoRua();
                self.getListaTipoSanguineo();

                if (angular.isFunction(callback)) {
                    callback();
                }
            });
        }

        /**
         * Busca os dados pessoais do candidato logado.
         *
         */
        function getDadosPessoais(isResponsavel) {

            if (isResponsavel) {
                edupsDadosPessoaisFactory.getDadosPessoaisResponsavel(function(result) {

                    if (result) {

                        if (result.SPSUSUARIO && result.SPSUSUARIO.length > 0) {
                            self.dadosPessoais = result.SPSUSUARIO[0];

							setCamposCPFPassRGObrigatorio('CPF');
							setCamposCPFPassRGObrigatorio('PassaporteNumero');
							setCamposCPFPassRGObrigatorio('EstrangeirosRegistroGeral');
                        }

                        if (result.SPSINSCAREAOFERTACOMPL && result.SPSINSCAREAOFERTACOMPL.length > 0) {
                            self.objCamposCompl.dadosCamposComplementaresInscricao = result.SPSINSCAREAOFERTACOMPL[0];
                        }

                        iniciarlizarCamposView();
                    }

                });
            } else {
                edupsDadosPessoaisFactory.getDadosPessoaisCandidato(self.numeroInscricao, function(result) {

                    if (result) {
                        if (result.SPSUSUARIO && result.SPSUSUARIO.length > 0) {
							self.dadosPessoais = result.SPSUSUARIO[0];

							setCamposCPFPassRGObrigatorio('CPF');
							setCamposCPFPassRGObrigatorio('PassaporteNumero');
							setCamposCPFPassRGObrigatorio('EstrangeirosRegistroGeral');
                        }

                        if (result.SPSINSCAREAOFERTACOMPL && result.SPSINSCAREAOFERTACOMPL.length > 0) {
                            self.objCamposCompl.dadosCamposComplementaresInscricao = result.SPSINSCAREAOFERTACOMPL[0];
                        }

                        iniciarlizarCamposView();
                    }

                });
            }
        }


        /**
         * Método responsável pela conversão do objeto para o formato JSON que será usado no POST.
         *
         * @returns Objeto no formado correto para criação do JSON utilizado no evento POST.
         */
        function iniciarlizarCamposView() {

            self.paisNatal = {};
            self.endereco = {};
            self.estadoEmissorCartTrab = {};
            self.infoAdicionais = {};
            self.certReservista = {};
            self.estadoEmissorTitEleitor = {};
            self.estadoEmissorRG = {};

            // dados básicos
            if (self.dadosPessoais.CANHOTO === edupsEnumsConsts.EdupsTrueFalseEnum.True) {
                self.dadosPessoais.CANHOTO = true;
            } else {
                self.dadosPessoais.CANHOTO = false;
            }

            self.dadosPessoais.FUMANTE = Boolean(self.dadosPessoais.FUMANTE);

            if (self.dadosPessoais.ESTADONATAL) {

                self.paisNatal.PAISES = {
                    IDPAIS: self.dadosPessoais.IDPAISESTADONATAL,
                    DESCRICAO: self.dadosPessoais.NOMEPAISESTADONATAL
                };

                self.paisNatal.ESTADOS = {
                    CODETD: self.dadosPessoais.ESTADONATAL,
                    NOME: self.dadosPessoais.NOMEESTADONATAL
                };
            }

            self.paisNatal.MUNICIPIOS = {};
            if (self.dadosPessoais.CODNATURALIDADE) {
                self.paisNatal.MUNICIPIOS.CODMUNICIPIO = self.dadosPessoais.CODNATURALIDADE;
            }
            else {
                self.paisNatal.MUNICIPIOS.NATURALIDADE = self.dadosPessoais.NATURALIDADE;
            }

            // documentos
            self.dadosPessoais.NIT = Boolean(self.dadosPessoais.NIT);

            if (self.dadosPessoais.UFCARTIDENT) {
                self.estadoEmissorRG.ESTADOSCARTIDENT = {};
                self.estadoEmissorRG.ESTADOSCARTIDENT.CODETD = self.dadosPessoais.UFCARTIDENT;
            }
            if (self.dadosPessoais.ESTELEIT) {
                self.estadoEmissorTitEleitor.ESTELEITOBJ = {};
                self.estadoEmissorTitEleitor.ESTELEITOBJ.CODETD = self.dadosPessoais.ESTELEIT;
            }
            if (self.dadosPessoais.UFCARTTRAB) {
                self.estadoEmissorCartTrab.UFCARTTRABOBJ = {};
                self.estadoEmissorCartTrab.UFCARTTRABOBJ.CODETD = self.dadosPessoais.UFCARTTRAB;
            }

            // endereço
            if (self.dadosPessoais.IDPAIS) {
                self.endereco.PAISESENDERECO = {};
                self.endereco.PAISESENDERECO.IDPAIS = self.dadosPessoais.IDPAIS;
            }
            if (self.dadosPessoais.ESTADO) {
                self.endereco.ESTADOSENDERECO = {};
                self.endereco.ESTADOSENDERECO.CODETD = self.dadosPessoais.ESTADO;
			}

			self.endereco.MUNICIPIOSENDERECO = {};
            if (self.dadosPessoais.CODMUNICIPIO) {
                self.endereco.MUNICIPIOSENDERECO.CODMUNICIPIO = self.dadosPessoais.CODMUNICIPIO;
			}
			else {
				self.endereco.MUNICIPIOSENDERECO.CIDADE = self.dadosPessoais.CIDADE;
			}

            // estrangeiro
            self.dadosPessoais.CONJUGEBRASIL = Boolean(self.dadosPessoais.CONJUGEBRASIL);
            self.dadosPessoais.NATURALIZADO = Boolean(self.dadosPessoais.NATURALIZADO);

            // Campos complementares
            getListaTabelaDinamicaItem(function() {

                criarCamposComplementares('DadosBasicos');
                criarCamposComplementares('InformacoesAdicionais');
                criarCamposComplementares('Endereco');
                criarCamposComplementares('Documentos');
                criarCamposComplementares('Passaporte');
                criarCamposComplementares('DadosEstrangeiros');
            });

            toggleCollapse();
        }

        /**
         *  Valida a visibilidade dos campos da tela de acordo com os parâmetros
         *
         * @param {any} idCampo - identificador do campo
         */
        function campoVisivel(idCampo) {
            var result = [],
                mostraCampo = false;
            if ((angular.isDefined($rootScope.objParametros)) && (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {
                result = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.IdCampo === idCampo);
                });

                if (angular.isArray(result) && result.length > 0) {

                    mostraCampo = result[0].VisivelCandidato;
                }
            }
            return mostraCampo;
        }

        /**
         * Valida exibição dos grupos de campos complementares.
         *
         * @param {any} idGrupoCampo - Identificador do grupo
         * @returns true or false
         */
        function exibeGrupoCamposComplementares(idGrupoCampo) {
            var arrayGrupos = [];

            arrayGrupos = $.grep(listaCamposComplementaresPorGrupo(idGrupoCampo), function(e) {
                return (e.GrupoCampo === idGrupoCampo);
            });

            return !self.isResponsavel && arrayGrupos.length > 0;
        }

        /**
         * Verifica exibição do grupo de campos
         *
         * @param {any} idGrupoCampo - Identificador do grupo
         * @returns true or false
         */
        function exibeGrupoCampos(idGrupoCampo) {
            var arrayGrupos = [],
                grupoVisivel = false;

            if ((angular.isDefined($rootScope.objParametros)) && (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {
                arrayGrupos = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.GrupoCampo === idGrupoCampo);
                });
            }

            if (arrayGrupos.length > 0) {
                arrayGrupos.forEach(function(item) {
                    grupoVisivel = (grupoVisivel || item.VisivelCandidato || exibeGrupoCamposComplementares(idGrupoCampo));
                });
            }
            return grupoVisivel;
        }

        /**
         * Valida a obrigatoriedade dos campos da tela de acordo com os parêmetros
         *
         * @param {any} idCampo - Identificador do campo
         * @returns true or false
         */
        function campoObrigatorio(idCampo) {
            var result = [];
            if ((angular.isDefined($rootScope.objParametros)) && (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {
                result = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.IdCampo === idCampo);
                });
            }
            if (result.length > 0) {
                if (self.isResponsavel) {
                    return result[0].ObrigatorioResponsavel;
                } else {
                    return result[0].ObrigatorioCandidato;
                }
            } else {
                return false;
            }
        }

        /**
         * Valida a obrigatoriedade dos campos de documentos verificando o parâmetro de desobrigação para Estrangeiros
         * Serão considerados estrangeiros usuários que tiverem o país diferente do país de origem do processo seletivo da inscrição
         *
         * @param {any} idCampo - Identificador do campo
         * @returns true or false
         */
        function campoDocumentoObrigatorio(idCampo) {

            var obrigatorio = self.campoObrigatorio(idCampo);
            if (self.isEstrangeiro() && $rootScope.objParametros.ObrigarCPFPassaporteRG) {
                obrigatorio = false;
            }
            return obrigatorio;
        }

        // Seta a variavel de controle para definicao de obrigatoriedade dos campos CPF/Passaporte/Registro Geral para estrangeiros
        function setCamposCPFPassRGObrigatorio(idCampo) {
			var obrigatorio = validaCamposCPFPassRGObrigatorio(idCampo, self.dadosPessoais);
			var blnIsEstrangeiroObrigarCPFPassaporteRG = ((angular.isDefined($rootScope.objParametros) && $rootScope.objParametros.ObrigarCPFPassaporteRG) &&
				(self.isEstrangeiro()) &&
				(angular.isArray($rootScope.objParametros.ListaParametrosPessoa)));

			if (blnIsEstrangeiroObrigarCPFPassaporteRG) {
				self.camposCPFPassRGObrigatorio['EstrangeirosRegistroGeral'] = obrigatorio;
				self.camposCPFPassRGObrigatorio['PassaporteNumero'] = obrigatorio;
				self.camposCPFPassRGObrigatorio['CPF'] = obrigatorio;
			} else {
				self.camposCPFPassRGObrigatorio[idCampo] = obrigatorio;
			}
        }

        // Valida se os campos CPF/Passaporte/Registro Geral foram preenchidos quando o parâmetro 'ObrigacaoCPFPassaporteRG' estiver marcado e os campos estiverem visiveis
        // Validação realizada para estrangeiros
        function validaCamposCPFPassRGObrigatorio(idCampo, objInscricao) {
            var result = [],
                obrigatorio = false;

            if ((angular.isDefined($rootScope.objParametros) && $rootScope.objParametros.ObrigarCPFPassaporteRG) &&
                (self.isEstrangeiro()) &&
                (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {

                result = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.IdCampo === 'EstrangeirosRegistroGeral' || e.IdCampo === 'PassaporteNumero' || e.IdCampo === 'CPF');
                });

                if (result.length > 0) {

                    var campoPreenchido = false;
                    result.forEach(function(item) {

						if (campoVisivel(item.IdCampo)) {
							if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao)) {
								campoPreenchido = true;
							}
						}
                    });

                    obrigatorio = !campoPreenchido;
                }
            } else {
                obrigatorio = campoDocumentoObrigatorio(idCampo);
            }

            return obrigatorio;
		}

        function campoPreenchidoCPFPassRG(idCampo, dadosUsuario) {

			var campoPreenchido = false;
			switch (idCampo) {
				case 'EstrangeirosRegistroGeral':
					if (angular.isDefined(dadosUsuario.NROREGGERAL) && dadosUsuario.NROREGGERAL) {
						campoPreenchido = true;
					}
					break;

				case 'PassaporteNumero':
					if (angular.isDefined(dadosUsuario.NPASSAPORTE) && dadosUsuario.NPASSAPORTE) {
						campoPreenchido = true;
					}
					break;

				case 'CPF':
					if (angular.isDefined(dadosUsuario.CPF) && dadosUsuario.CPF) {
						campoPreenchido = true;
					}
					break;
				default:
					campoPreenchido = false;
					break;
			}
			console.log(idCampo + ' - CampoPreenchido? ' + campoPreenchido);
			return campoPreenchido;
		}

        /**
         * Busca a relação de países
         *
         * @param {any} callback - função de retorno
         */
        function getListaPaises(callback) {

            EdupsUtilsFactory.getListaPaises(function(result) {
                $rootScope.contarListaCarregadas--;
                if (angular.isDefined(result) && angular.isDefined(result.GPais)) {
                    self.listaPaises = $filter('orderBy')(result.GPais, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.GPais)) {
                    callback(result.GPais);
                }
            });
        }

        /**
         * Busca a lista de estados de acordo com o pais.
         *
         * @param {any} idPais - Identificador do pais
         * @param {any} callback - função de retorno
         */
        function getListaEstados(idPais, callback) {

            if (idPais) {

                EdupsUtilsFactory.getListaEstados(idPais, function(result) {
                    $rootScope.contarListaCarregadas--;
                    if (angular.isDefined(result) && angular.isDefined(result.GEtd)) {
                        self.listaEstados = result.GEtd;

                    }

                    if (angular.isFunction(callback) && angular.isArray(result.GEtd)) {
                        callback(result.GEtd);
                    }
                });
            }
        }

        /**
         * Buscar lista de município de acordo com o estado.
         *
         * @param {any} codEtd - Código do estado
         * @param {any} callback - função de retorno
         */
        function getListaMunicipios(codEtd, callback) {

            if (codEtd) {

                EdupsUtilsFactory.getListaMunicipios(codEtd, function(result) {
                    $rootScope.contarListaCarregadas--;
                    if (angular.isDefined(result) && angular.isDefined(result.GMUNICIPIO)) {
                        self.listaMunicipios = result.GMUNICIPIO;
                    }

                    if (angular.isFunction(callback) && angular.isArray(result.GMUNICIPIO)) {
                        callback(result.GMUNICIPIO);
                    }
                });
            }
        }

        /**
         * Busca a lsita de estado cívil
         *
         * @param {any} callback - função de retorno
         */
        function getListaEstadoCivil(callback) {

            EdupsUtilsFactory.getListaEstadoCivil(function(result) {
                $rootScope.contarListaCarregadas--;
                if (angular.isDefined(result) && angular.isDefined(result.PCODESTCIVIL)) {
                    self.listaEstadoCivil = $filter('orderBy')(result.PCODESTCIVIL, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.PCODESTCIVIL)) {
                    callback(result.PCODESTCIVIL);
                }
            });
        }

        /**
         * Busca a lista de nacionalidades
         *
         * @param {any} callback - função de retorno
         */
        function getListaNacionalidade(callback) {

            EdupsUtilsFactory.getListaNacionalidade(function(result) {
                $rootScope.contarListaCarregadas--;
                if (angular.isDefined(result) && angular.isDefined(result.PCODNACAO)) {
                    self.listaNacionalidade = $filter('orderBy')(result.PCODNACAO, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.PCODNACAO)) {
                    callback(result.PCODNACAO);
                }
            });
        }

        /**
         * Busca a lista de tipo de ruas
         *
         * @param {any} callback - função de retorno
         */
        function getListaTipoRua(callback) {

            EdupsUtilsFactory.getListaTipoRua(function(result) {
                $rootScope.contarListaCarregadas--;
                if (angular.isDefined(result) && angular.isDefined(result.DTipoRua)) {
                    self.listaTipoRua = $filter('orderBy')(result.DTipoRua, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.DTipoRua)) {
                    callback(result.DTipoRua);
                }
            });
        }

        /**
         * Busca lista de tipo de bairro
         *
         * @param {any} callback  - função de retorno
         */
        function getListaTipoBairro(callback) {

            EdupsUtilsFactory.getListaTipoBairro(function(result) {
                $rootScope.contarListaCarregadas--;
                if (angular.isDefined(result) && angular.isDefined(result.DTipoBairro)) {
                    self.listaTipoBairro = $filter('orderBy')(result.DTipoBairro, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.DTipoBairro)) {
                    callback(result.DTipoBairro);
                }
            });
        }

        /**
         * Busca lista de cor/raça
         *
         * @param {any} callback - função de retorno
         */
        function getListaCorRaca(callback) {
            $rootScope.contarListaCarregadas--;
            EdupsUtilsFactory.getListaCorRaca(function(result) {

                if (angular.isDefined(result) && angular.isDefined(result.PCORRACA)) {
                    self.listaCorRaca = $filter('orderBy')(result.PCORRACA, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.PCORRACA)) {
                    callback(result.PCORRACA);
                }
            });
        }

        /**
         * Busca lista de profissões
         *
         * @param {any} callback - função de retorno
         */
        function getListaProfissao(callback) {
            EdupsUtilsFactory.getListaProfissao(function(result) {
                $rootScope.contarListaCarregadas--;
                if (angular.isDefined(result) && angular.isDefined(result.EProfiss)) {
                    self.listaProfissao = $filter('orderBy')(result.EProfiss, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.EProfiss)) {
                    callback(result.EProfiss);
                }
            });
        }

        /**
         * Busca lista de grau de instrução
         *
         * @param {any} callback - função de retorno
         */
        function getListaGrauInstrucao(callback) {
            $rootScope.contarListaCarregadas--;
            EdupsUtilsFactory.getListaGrauInstrucao(function(result) {

                if (angular.isDefined(result) && angular.isDefined(result.PCODINSTRUCAO)) {
                    self.listaGrauInstrucao = $filter('orderBy')(result.PCODINSTRUCAO, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.PCODINSTRUCAO)) {
                    callback(result.PCODINSTRUCAO);
                }
            });
        }

        /**
         * Busca lsita de tipos sanguíneo
         *
         * @param {any} callback - função de retorno
         */
        function getListaTipoSanguineo(callback) {
            $rootScope.contarListaCarregadas--;
            EdupsUtilsFactory.getListaTipoSanguineo(function(result) {
                if (angular.isDefined(result) && angular.isDefined(result.STIPOSANGUINEO)) {
                    self.listaTipoSanguineo = $filter('orderBy')(result.STIPOSANGUINEO, 'DESCRICAO');
                }

                if (angular.isFunction(callback) && angular.isArray(result.STIPOSANGUINEO)) {
                    callback(result.STIPOSANGUINEO);
                }
            });
        }

        /**
         * Busca lista de situação militar
         *
         */
        function getListaSituacaoMilitar() {
            $rootScope.contarListaCarregadas--;
            self.listaSituacaoMilitar.push({
                CODIGO: i18nFilter('l-sitmilitar-alistado', [], 'js/inscricoes'),
                DESCRICAO: i18nFilter('l-sitmilitar-alistado', [], 'js/inscricoes')
            });

            self.listaSituacaoMilitar.push({
                CODIGO: i18nFilter('l-sitmilitar-dispensado', [], 'js/inscricoes'),
                DESCRICAO: i18nFilter('l-sitmilitar-dispensado', [], 'js/inscricoes')
            });

            self.listaSituacaoMilitar.push({
                CODIGO: i18nFilter('l-sitmilitar-reservista', [], 'js/inscricoes'),
                DESCRICAO: i18nFilter('l-sitmilitar-reservista', [], 'js/inscricoes')
            });
        }

        /**
         * Valida se o candidato é estrangeiro
         *
         * @returns True or false
         */
        function isEstrangeiro() {

            var estrangeiro = false;

            // Para a visibilidade ou ele é candidato ou responsável
            if (self.campoVisivel('Pais')) {
                estrangeiro = paisDiferenteOrigemPS();
            }

            return estrangeiro;
        }

        /**
         * Valida se o país de origem é diferente do IDPAIS
         *
         * @returns True or false
         */
        function paisDiferenteOrigemPS() {
            return (self.dadosPessoais &&
                self.dadosPessoais.IDPAIS !== null &&
                self.dadosPessoais.IDPAIS !== $rootScope.objParametros.PaisOrigemPS);
        }

        // *********************************************************************************
        // *** Campos Complementares
        // *********************************************************************************

        function getModelCampo(i, grupoCampo) {
            var result = listaCamposComplementaresPorGrupo(grupoCampo);
            return result[i];
        }

        // busca os dados de endereço de um cep
        function buscarEnderecoPorCEP(numeroCEP, modeloDados) {

            if (!angular.isDefined(numeroCEP) || numeroCEP === '') {
                return;
            }

            EdupsUtilsFactory.getEnderecoByCEP(numeroCEP, function(result) {

                if (angular.isDefined(result) && result.length > 0) {
                    modeloDados.RUA = result[0].NOMELOGRADOURO;
                    modeloDados.BAIRRO = result[0].BAIRRO;

                    self.endereco.PAISESENDERECO = { 'IDPAIS': 1 };
                    self.endereco.ESTADOSENDERECO = { 'CODETD': result[0].UF };
                    self.endereco.MUNICIPIOSENDERECO = { 'CODMUNICIPIO': result[0].CODMUNICIPIO };

                } else {
                    totvsNotification.notify({
                        type: 'info',
                        title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                        detail: i18nFilter('l-msg-cep-nao-encontrado', [], 'js/inscricoes')
                    });
                }
            });
        }

        function getListaTabelaDinamicaItem(callback) {

            self.listaTabelaDinamicaItem = {};

            // Só habilita campos complementares para o candidato. Responsáveis não possuem campos complementares.
            if (!self.isResponsavel) {
                if (angular.isDefined(self.objParametros.ListaTabelaDinamicaItem) && self.objParametros.ListaTabelaDinamicaItem !== null) {
                    var tabelaDinamica = '';
                    for (var i = 0; i < self.objParametros.ListaTabelaDinamicaItem.length; i++) {
                        var item = self.objParametros.ListaTabelaDinamicaItem[i];
                        if (tabelaDinamica !== item.CodTabela) {
                            self.listaTabelaDinamicaItem[item.CodTabela] = [];
                            tabelaDinamica = item.CodTabela;
                        }
                        self.listaTabelaDinamicaItem[item.CodTabela].push(angular.copy(item));
                    }
                }

                if (angular.isFunction(callback)) {
                    callback();
                }
            }
        }

        function getTOTVSFieldTemplateCheckBox(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<field ';
            template += ' type="checkbox"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-model=controller.objCamposCompl.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</field>';
            template += '</div>';

            return template;
        }

        function getTOTVSFieldTemplateDate(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' name="$NAME_CAMPO"';
            template += ' totvs-datepicker';
            template += ' class="col-lg-4 col-md-4 col-sm-4 col-xs-12"';
            template += ' canclean';
            template += ' ng-model=controller.objCamposCompl.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', self.getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateInput(itemCampo, posicaoItemArray, grupoCampo) {
            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' name="$NAME_CAMPO"';
            template += ' totvs-input';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-model=controller.objCamposCompl.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' maxlength="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').TamanhoDaColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', self.getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateNumber(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' totvs-number';
            template += ' name="$NAME_CAMPO"';
            template += ' canclean';
            template += ' class="col-lg-4 col-md-4 col-sm-4 col-xs-12"';
            template += ' ng-model=controller.objCamposCompl.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', self.getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateDecimal(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' totvs-decimal';
            template += ' name="$NAME_CAMPO"';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' m-dec="2" a-dec=","  a-sep="." ';
            template += ' ng-model=controller.objCamposCompl.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', self.getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateSelect(itemCampo, posicaoItemArray, model, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<field';
            template += ' type="combo"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' canclean';
            template += ' ng-model=controller.objCamposCompl.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna + 'OBJ';
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';

            if (self.listaTabelaDinamicaItem && angular.isDefined(self.listaTabelaDinamicaItem)) {

                if (angular.isDefined(self.listaTabelaDinamicaItem[itemCampo.CodTabDinam]) &&
                    angular.isArray(self.listaTabelaDinamicaItem[itemCampo.CodTabDinam])) {

                    template += ' ng-options="{1} as {2} for item in {3}"';
                    template = template.replaceAllSplitAndJoin('{1}', ' item');
                    template = template.replaceAllSplitAndJoin('{2}', ' item.Descricao');
                    template = template.replaceAllSplitAndJoin('{3}', 'controller.listaTabelaDinamicaItem[\'' + itemCampo.CodTabDinam + '\'] ');

                    // seta o valor para editar
                    if (angular.isDefined(self.objCamposCompl.dadosCamposComplementaresInscricao) &&
                        angular.isDefined(self.objCamposCompl.dadosCamposComplementaresInscricao[itemCampo.NomeColuna])) {

                        for (var i = 0; i < self.listaTabelaDinamicaItem[itemCampo.CodTabDinam].length; i++) {
                            if (self.listaTabelaDinamicaItem[itemCampo.CodTabDinam][i].CodCliente === self.objCamposCompl.dadosCamposComplementaresInscricao[itemCampo.NomeColuna]) {
                                self.objCamposCompl.dadosCamposComplementaresInscricao[itemCampo.NomeColuna + 'OBJ'] = self.listaTabelaDinamicaItem[itemCampo.CodTabDinam][i];
                                break;
                            }
                        }
                    }
                }
            }

            template += '>';
            template += '</field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', self.getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;

        }

        function retornaDiretivaTOTVSPorTipoCampo(item, posicaoItemArray, model, grupoCampo) {

            var diretiva = '';

            //campo input select
            if (item.CodTabDinam) {
                diretiva = getTOTVSFieldTemplateSelect(item, posicaoItemArray, model, grupoCampo);
            } else {
                switch (item.TipoDaColuna) {
                    case 0: //campo input tipo char
                        diretiva = getTOTVSFieldTemplateInput(item, posicaoItemArray, grupoCampo);
                        break;
                    case 1: //campo input tipo data
                        diretiva = getTOTVSFieldTemplateDate(item, posicaoItemArray, grupoCampo);
                        break;
                    case 2: //campo input tipo double | decimal
                        diretiva = getTOTVSFieldTemplateDecimal(item, posicaoItemArray, grupoCampo);
                        break;
                    case 3: //campo input tipo int
                        diretiva = getTOTVSFieldTemplateNumber(item, posicaoItemArray, grupoCampo);
                        break;
                    case 4: //campo input tipo text
                        diretiva = getTOTVSFieldTemplateInput(item, posicaoItemArray, grupoCampo);
                        break;
                    case 5: //campo input tipo int16
                        diretiva = getTOTVSFieldTemplateNumber(item, posicaoItemArray, grupoCampo);
                        break;
                    case 6: //campo input tipo checkbox
                        diretiva = getTOTVSFieldTemplateCheckBox(item, posicaoItemArray, grupoCampo);
                        break;
                }
            }

            return diretiva;
        }

        /**
         * Lista os campos complementares por grupo
         *
         * @param {any} grupo
         * @returns grupo
         */
        function listaCamposComplementaresPorGrupo(grupo) {
            if ((angular.isDefined($rootScope.objParametros.ListaCampoComplFilialGrupo)) && ($rootScope.objParametros.ListaCampoComplFilialGrupo !== null)) {
                return $.grep($rootScope.objParametros.ListaCampoComplFilialGrupo, function(e) {
                    return (e.GrupoCampo === grupo);
                });
            }
        }

        function criarCamposComplementares(grupoCampos) {
            var listaCampos = listaCamposComplementaresPorGrupo(grupoCampos);

            if (angular.isArray(listaCampos)) {
                for (var i = 0; i < listaCampos.length; i++) {

                    var template = retornaDiretivaTOTVSPorTipoCampo(listaCampos[i], i, self.model, grupoCampos),
                        diretivaTotvs = $compile(template),
                        containerDiv = document.getElementById(grupoCampos);

                    diretivaTotvs = diretivaTotvs($scope);

                    angular.element(containerDiv).append(diretivaTotvs);
                }
            }
        }


        /**
         * cancela a alteração dos dados pessoais
         *
         */
        function cancelar() {

            $state.go('centralcandidato.start');

        }

        /**
         * Salva os dados pessoais
         *
         */
        function salvarDadosPessoais() {

            var objJSON = objParseToJSON();

            if (camposObrigatoriosPreenchidos()) {
                if ($state.params.isResponsavel) {

                    edupsDadosPessoaisFactory.saveDadosPessoaisResponsavel(objJSON, function(result) {
                        if (result.SPSUSUARIO != null) {
                            $state.go('centralcandidato.start');
                        }
                    });
                } else {
                    edupsDadosPessoaisFactory.saveDadosPessoaisCandidato(objJSON, function(result) {
                        if (result.SPSUSUARIO != null) {
                            $state.go('centralcandidato.start');
                        }
                    });

                }
            }
        }

        /**
         * Método responsável pela conversão do objeto para o formato JSON que será usado no POST.
         *
         * @returns Objeto no formado correto para criação do JSON utilizado no evento POST.
         */
        function objParseToJSON() {

            var modelJSON = {
                    SPSUSUARIO: [],
                    SPSINSCAREAOFERTACOMPL: [],
                    SPSINSCRICAOAREAOFERTADA: []
                },
                objDadosUsuario = angular.copy(self.dadosPessoais);

            /*Prepara os dados do usuário para enviar*/
            if (self.paisNatal.ESTADOS) {
                objDadosUsuario.ESTADONATAL = self.paisNatal.ESTADOS.CODETD;
            }
			if (self.paisNatal.MUNICIPIOS) {
				if (self.paisNatal.MUNICIPIOS.CODMUNICIPIO) {
					objDadosUsuario.CODNATURALIDADE = self.paisNatal.MUNICIPIOS.CODMUNICIPIO;
				}
				else {
                    objDadosUsuario.CODNATURALIDADE = null;
					objDadosUsuario.NATURALIDADE = self.paisNatal.MUNICIPIOS.NATURALIDADE;
				}
            }

            if (self.estadoEmissorRG.ESTADOSCARTIDENT) {
                objDadosUsuario.UFCARTIDENT = self.estadoEmissorRG.ESTADOSCARTIDENT.CODETD;
            }
            if (self.estadoEmissorTitEleitor.ESTELEITOBJ) {
                objDadosUsuario.ESTELEIT = self.estadoEmissorTitEleitor.ESTELEITOBJ.CODETD;
            }
            if (self.estadoEmissorCartTrab.UFCARTTRABOBJ) {
                objDadosUsuario.UFCARTTRAB = self.estadoEmissorCartTrab.UFCARTTRABOBJ.CODETD;
            }

            if (self.endereco.PAISESENDERECO) {
                objDadosUsuario.IDPAIS = self.endereco.PAISESENDERECO.IDPAIS;
            }
            if (self.endereco.ESTADOSENDERECO) {
                objDadosUsuario.ESTADO = self.endereco.ESTADOSENDERECO.CODETD;
            }
            if (self.endereco.MUNICIPIOSENDERECO) {
				if (self.endereco.MUNICIPIOSENDERECO.CODMUNICIPIO) {
					objDadosUsuario.CODMUNICIPIO = self.endereco.MUNICIPIOSENDERECO.CODMUNICIPIO;
				}
				else {
                    objDadosUsuario.CODMUNICIPIO = null;
					objDadosUsuario.CIDADE = self.endereco.MUNICIPIOSENDERECO.CIDADE;
				}
            }

            /*Prepara os dados para enviar*/
            trataCamposParseJson(objDadosUsuario);

            modelJSON.SPSUSUARIO.push(objDadosUsuario);

            if (self.objCamposCompl.dadosCamposComplementaresInscricao) {

                var dadosCamposComplementaresInscricaoTemp = angular.copy(self.objCamposCompl.dadosCamposComplementaresInscricao);
                dadosCamposComplementaresInscricaoTemp.CODCOLIGADA = $rootScope.CodColigada;
                dadosCamposComplementaresInscricaoTemp.IDPS = $rootScope.IdPS;

                if (self.objParametros && self.objParametros.ListaCampoComplFilialGrupo) {
                    for (var i = 0; i < self.objParametros.ListaCampoComplFilialGrupo.length; i++) {
                        var itemCampo = self.objParametros.ListaCampoComplFilialGrupo[i];

                        //campo input tipo checkbox
                        if (itemCampo.TipoDaColuna === 6) {
                            if (dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] === true) {
                                dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] = 'T';
                            } else {
                                dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] = 'F';
                            }
                        } else {
                            if (itemCampo.CodTabDinam && itemCampo.CodTabDinam !== '') {
                                var itemComplementarObj = dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna + 'OBJ'];
                                if (itemComplementarObj) {
                                    dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] = itemComplementarObj.CodCliente;
                                    delete dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna + 'OBJ'];
                                }
                            }
                        }
                    }
                }

                modelJSON.SPSINSCAREAOFERTACOMPL.push(dadosCamposComplementaresInscricaoTemp);

                var objInscricao = {};
                objInscricao.NUMEROINSCRICAO = self.numeroInscricao;
                modelJSON.SPSINSCRICAOAREAOFERTADA.push(objInscricao);

            }

            return modelJSON;
        }

        /**
         * Método para tratamento dos campos durante o parse para o objeto Json
         *
         * @param {any} dadosUsuario - Objeto do usuário que será convertido para Json
         */
        function trataCamposParseJson(dadosUsuario) {

            if (dadosUsuario.CANHOTO === true) {
                dadosUsuario.CANHOTO = 'T';
            } else {
                dadosUsuario.CANHOTO = 'F';
            }
            if (dadosUsuario.FUMANTE === true) {
                dadosUsuario.FUMANTE = 1;
            } else {
                dadosUsuario.FUMANTE = 0;
            }
            if (dadosUsuario.CONJUGEBRASIL === true) {
                dadosUsuario.CONJUGEBRASIL = 1;
            } else {
                dadosUsuario.CONJUGEBRASIL = 0;
            }
            if (dadosUsuario.NATURALIZADO === true) {
                dadosUsuario.NATURALIZADO = 1;
            } else {
                dadosUsuario.NATURALIZADO = 0;
            }
            if (dadosUsuario.NIT === true) {
                dadosUsuario.NIT = 1;
            } else {
                dadosUsuario.NIT = 0;
            }
            if (dadosUsuario.FILHOSBRASIL) {
                dadosUsuario.NROFILHOSBRASIL = dadosUsuario.FILHOSBRASIL;
            }
        }

        /**
         * Verifica o prenchimento dos campos obrigatórios
         *
         * @returns
         */
        function camposObrigatoriosPreenchidos() {
            var listaCampos = '',
                podeSalvar = true;

            if ((!$scope.frmDadosCandidato.$valid) && ($scope.frmDadosCandidato.$error.required)) {
                for (var i = 0; i < 10; i++) {
                    if ((angular.isDefined($scope.frmDadosCandidato.$error.required[i])) && ($scope.frmDadosCandidato.$error.required[i].$name !== '') && ($scope.frmDadosCandidato.$error.required[i].$name.indexOf('controller_') === -1)) {
                        listaCampos = listaCampos + $scope.frmDadosCandidato.$error.required[i].$name + ', ';
                        podeSalvar = false;
                    }
                }
            }

            if (!podeSalvar && listaCampos) {
                listaCampos = listaCampos.slice(0, -2);
                listaCampos = i18nFilter('l-campos-obrigatorios', [], 'js/inscricoes') + listaCampos;
                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                    detail: listaCampos
                });
            }

            if ($rootScope.objParametros.ExigeSobrenome)
            {
                var dados = self.dadosPessoais.NOME.split(' ');

                if (dados.length < 2)
                {
                    podeSalvar = false;
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                        detail: i18nFilter('l-exige-sobrenome', [], 'js/inscricoes')
                    });
                }
            }

            return podeSalvar;
        }

        //Recebe o texto do telefone definido nos parâmetros do RHU e concatena com qual formulário
        //esse campo pertence.
        function concatenaName(textoTelefone, nomeResource){
            var nameCampo = textoTelefone;
            nameCampo += " ";
            nameCampo += i18nFilter(nomeResource, [], 'js/inscricoes');

            return nameCampo;
        }
    }
});
