/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

define(['totvs-app.module',
        'utils/interceptors/edups-interceptors.factory'], function (){

    'use strict';

    angular
        .module('totvsApp')
        .config(totvsAppConfig);

    totvsAppConfig.$inject = ['$httpProvider', 'TotvsI18nProvider', '$compileProvider', 'TotvsDatepickerProvider'];

    function totvsAppConfig($httpProvider, TotvsI18nProvider, $compileProvider, TotvsDatepickerProvider) {

        $httpProvider.interceptors.push('totvsHttpInterceptor');
        $httpProvider.interceptors.push('EdupsInterceptors');

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|data):/);

        //Variável global 'CONST_GLOBAL_URL_BASE_APP' definida antes do carregamento da aplicação
        TotvsI18nProvider.setBaseContext(EDUPS_CONST_GLOBAL_URL_BASE_APP);

        // define o output padrão como ISO-8601
        TotvsDatepickerProvider.setOutputFormat('date');
    }
    
    angular
        .module('totvsApp')
        .run(totvsAppRun);

    totvsAppRun.$inject = ['$rootScope', '$window', '$location'];

    function totvsAppRun($rootScope, $window, $location) {
        document.eduPSNivelEnsino = $location.path().split('/')[1];
        
        var eChangeStart = $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            if (angular.isDefined(toParams.nivelEnsino)) {
                if (toParams.nivelEnsino !== 'eb' && toParams.nivelEnsino !== 'es') {
                    event.preventDefault();
                }
                
                if (toParams.nivelEnsino !== document.eduPSNivelEnsino) {
                    document.eduPSNivelEnsino = toParams.nivelEnsino;
                    eChangeStart();
                    $window.location.reload();
                }
            }
        });
    }
});
