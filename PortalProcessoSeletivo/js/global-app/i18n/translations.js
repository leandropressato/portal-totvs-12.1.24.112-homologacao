[
    {
        "l-acesso-cpf": {
            "pt": "CPF",
			"en": "CPF",
			"es": "RCPF"
        },
        "l-acesso-identidade": {
            "pt": "Carteira Identidade",
			"en": "Identity Card",
			"es": "Documento de identidad"
        },
        "l-acesso-codusuario": {
            "pt": "Código do Usuário",
			"en": "User Code",
			"es": "Código del usuario"
        },
        "l-acesso-email": {
            "pt": "E-mail",
			"en": "E-mail",
			"es": "E-mail"
        },
        "l-carregando":{
            "pt": "Carregando! Aguarde um instante!",
			"en": "Loading",
			"es": "Cargando"
        },
        "l-msg-confirmacao-alteracao-ps":{
            "pt": "A alteração do processo seletivo irá interromper a inscrição atual e os dados informados serão perdidos. Deseja continuar com a alteração?",
			"en": "The edition of the selective process interrupts the current registration and data entered are lost. Continue with editing?",
			"es": "La modificación del proceso de selección interrumpirá la inscripción actual y se perderán los datos informados. ¿Desea continuar con la modificación?"
        },
         "l-msg-confirmacao-alteracao-ps-logado":{
            "pt": "A alteração do processo seletivo não irá manter o acesso atual e será necessário realizar um novo acesso. Deseja continuar com a alteração?",
			"en": "The edition of the selective process does not interrupt the current access, you must execute a new access. Continue editing?",
			"es": "La modificación del proceso de selección no mantendrá el acceso actual y será necesario realizar un nuevo acceso. ¿Desea continuar con la modificación?"
        },
        "l-msg-atualizar-fechar-browser": {
            "pt": "Ao atualizar ou sair da página irá interromper a inscrição atual e os dados informados serão perdidos. Deseja realmente atualizar ou sair da página?",
			"en": "When updating or exiting the page, the current registration and data entered are interrupted and lost. Do you really want to update or exit page?",
			"es": "Al actualizar o salir de la página se interrumpirá la inscripción actual y se perderán los datos informados. ¿Realmente desea actualizar o salir de la página?"
        },
        "l-msg-confirmacao-logout":{
            "pt": "Tem certeza que deseja sair do Portal do Candidato?",
			"en": "Are you sure you want to exit the Candidate Portal?",
			"es": "¿Está seguro de que desea salir del Portal del candidato?"
        },
        "l-confirmacao":{
            "pt": "Confirmação",
			"en": "Confirmation",
			"es": "Confirmación"
        },
        "l-logout-success":{
            "pt": "Você deixou o portal do candidato. Para acessar as informações de candidato, realize um novo acesso através da tela de login.",
			"en": "You left the candidate portal. To access the candidate's information, execute a new access through the log in screen.",
			"es": "Usted dejó el portal del candidato. Para acceder a la información del candidato, realice un nuevo acceso por medio de la pantalla de login."
        },
        "l-msg-erro-login": {
            "pt": "Acesso ao portal expirado. É necessário fazer o Login na aplicação novamente!",
			"en": "Access to portal expired. Log in the application again!",
			"es": "Acceso al portal expirado. ¡Es necesario hacer nuevamente el Login en la aplicación!"
        },
        "l-titulo-erro-login": {
            "pt": "Acesso Expirado",
			"en": "Access Expired",
			"es": "Acceso expirado"
        },
        "l-msg-aviso-perda-dados": {
            "pt": "Utilize o botão de retorno no formulário para que as informações digitadas não sejam perdidas.",
			"en": "Use the return button in the form so information entered is not lost.",
			"es": "Utilice el botón de devolución en el formulario para que no se pierda la información digitada."
        },
        "l-aviso": {
            "pt": "Aviso",
			"en": "Warning",
			"es": "Aviso"
        }

    }
]
