/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

define(['angular-cookies'], function () {

    'use strict';

    angular
        .module('globalApp', ['ngCookies', 'ngSanitize']);
});
