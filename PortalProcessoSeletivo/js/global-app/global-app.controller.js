/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

define(['global-app/global-app.module',
    'utils/edups-utils.factory',
    'utils/edups-utils.service',
    'utils/edups-apiunis.factory',
    'login/login.factory'], function () {

        'use strict';

        angular
            .module('globalApp')
            .controller('GlobalAppController', GlobalAppController);

        GlobalAppController.$inject = [
            '$rootScope',
            '$window',
            '$timeout',
            '$location',
            '$sce',
            '$state',
            'i18nFilter',
            'edupsUtilsFactory',
            'totvs.app-notification.Service',
            'edupsLoginFactory'
        ];

        function GlobalAppController(
            $rootScope,
            $window,
            $timeout,
            $location,
            $sce,
            $state,
            i18nFilter,
            EdupsUtilsFactory,
            totvsNotification,
            edupsLoginFactory) {

            // *********************************************************************************
            // *** Variables
            // *********************************************************************************

            var self = this;
            self.listaProcessosSeletivos = [];
            self.listaArquivosProcessoSeletivo = [];
            self.model = {};
            self.slides = [];

            self.carregaParametrosProcessoSeletivo = carregaParametrosProcessoSeletivo;
            self.recarregaParametrosProcessoSeletivo = recarregaParametrosProcessoSeletivo;
            self.listaTipoIdentificacaoLogin = listaTipoIdentificacaoLogin;
            self.atualizaModel = atualizaModel;
            self.listaProcessoSeletivoSelecionado = listaProcessoSeletivoSelecionado;
            self.primeiraExecucao = true;
            self.logout = logout;
            self.carregarPaginaPrincipal = carregarPaginaPrincipal;

            // *********************************************************************************
            // *** Public Properties and Methods
            // *********************************************************************************

            if (EDUPS_CONST_GLOBAL_CUSTOM_IMAGES === false) {
                $rootScope.SrcBaseImages = EDUPS_CONST_GLOBAL_URL_BASE_APP + 'assets/img';
            }
            else {
                $rootScope.SrcBaseImages = EDUPS_CONST_GLOBAL_URL_BASE_APP + 'assets/img/custom';
            }

            if (EDUPS_CONST_GLOBAL_CUSTOM_CSS === false) {
                $rootScope.SrcBaseCSS = EDUPS_CONST_GLOBAL_URL_BASE_APP + 'assets/css/app.css';
            }
            else {
                $rootScope.SrcBaseCSS = EDUPS_CONST_GLOBAL_URL_BASE_APP + 'assets/css/custom/app.css';
            }

            init();

            // *********************************************************************************
            // *** Controller Initialize
            // *********************************************************************************

            function init() {
                //Configurar idioma padrão da tradução
                $rootScope.currentuser = { dialect: EDUPS_CONST_GLOBAL_CUSTOM_IDIOMA };

                // Recupera o nível de ensino, carregado no module principal do site (totvs-app.module.js)
                // Carrega essa informação no $rootScope para poder utilizá-la em qualquer ponto do sistema.
                $rootScope.nivelEnsino = document.eduPSNivelEnsino;

                //Parâmetros de filtro da URL.
                var codColigada = getParametrosURL('c'),
                    codFilial = getParametrosURL('f'),
                    codCategoria = getParametrosURL('ct'),
                    idPS = getParametrosURL('ps'); //Usado apenas no client para selecionar o processo seletivo passado na url.

                codColigada = angular.isDefined(codColigada) ? parseInt(codColigada) : -1;
                codFilial = angular.isDefined(codFilial) ? parseInt(codFilial) : -1;
                codCategoria = angular.isDefined(codCategoria) ? codCategoria : '';
                idPS = angular.isDefined(idPS) ? parseInt(idPS) : -1;

                carregaListaProcessosSeletivos(codColigada, codFilial, codCategoria, idPS, $rootScope.nivelEnsino);

                self.hasPendingRequests = function () {
                    return ($rootScope.pendingRequests > 0);
                };

                /**
                 * Mensagem ao usuário quando atualizar (F5 ou CTRL + R) ou fechar o browser
                 * perguntando se deseja mesmo atualizar ou ficar na págida
                 * @returns {string} Mensagem ao usuário dizendo que seus dados serão perdidos se continuar
                 */
                $window.onbeforeunload = function () {
                    if ($state.is('inscricoesWizard') || $state.is('dadospessoais.start')) {
                        return i18nFilter('l-msg-atualizar-fechar-browser', [], 'js/global-app');
                    }
                };
            }

            // *********************************************************************************
            // *** Functions
            // *********************************************************************************

            function atualizaModel(processoSeletivo) {


                if (processoSeletivo) {
                    self.model = processoSeletivo;

                    carregarBanner(self.model.CODCOLIGADA, self.model.IDPS);
                }
            }

            /* Filtra uma lista com o Processo Seletivo que foi selecionado */
            function listaProcessoSeletivoSelecionado(codColigada, idPs) {
                if ((angular.isDefined(self.listaProcessosSeletivos)) && (self.listaProcessosSeletivos !== null)) {
                    return $.grep(self.listaProcessosSeletivos, function (e) {
                        return (e.CODCOLIGADA === codColigada) && (e.IDPS === idPs);
                    });
                }
            }

            function carregaListaProcessosSeletivos(codColigada, codFilial, codCategoria, idPS, nivelEnsino) {

                var apresentacao;

                switch (nivelEnsino) {
                    case 'es':
                        apresentacao = '1';
                        break;
                    case 'eb':
                        apresentacao = '0';
                        break;
                }

                EdupsUtilsFactory.getListaProcessosSeletivos(codColigada, codFilial, codCategoria, apresentacao, function (result) {
                    if ((angular.isDefined(result)) && angular.isDefined(result.SPSProcessoSeletivo)) {

                        self.listaProcessosSeletivos = result.SPSProcessoSeletivo;

                        var index = SelecionaIdPSInformado(self.listaProcessosSeletivos, codColigada, idPS);

                        self.atualizaModel(self.listaProcessosSeletivos[index]);

                        $rootScope.codCategoria = codCategoria;
                        $rootScope.CodColigada = self.model.CODCOLIGADA;
                        $rootScope.CodFilial = self.model.CODFILIAL;
                        $rootScope.IdPS = self.model.IDPS;
                        $rootScope.NomeProcessoSeletivo = self.model.NOME;
                        $rootScope.ValorInscricao = self.model.VALORINSCRICAO;
                        $rootScope.UsaGrupoAreaInteressePS = (self.model.GRUPOAREAINTERESSE === 'T');
                        $rootScope.TextoGrupoAreaInteressePS = self.model.TEXTOGRUPOAREAINTERESSE;
                        self.carregaParametrosProcessoSeletivo(self.model.CODCOLIGADA, self.model.IDPS, self.model.ETAPAPS, 'PROCSEL|INF1|INF2|INSC|G1|G2|G3|VO|R|RS|A|GC|CTR1|CTR2|INT');
                    }
                });
            }

            function recarregaParametrosProcessoSeletivo(codigosGruposParametros) {

                var alterouPSInscricao = false;

                if ($rootScope.PeriodoInscricao && ($state.is('inscricoesWizard') || $state.is('centralcandidato.start') || $state.is('dadospessoais.start'))) {
                    alterouPSInscricao = true;
                    totvsNotification.question({
                        title: i18nFilter('l-Confirmacao'),
                        text: $state.is('centralcandidato.start') ? i18nFilter('l-msg-confirmacao-alteracao-ps-logado', [], 'js/global-app') : i18nFilter('l-msg-confirmacao-alteracao-ps', [], 'js/global-app'),
                        cancelLabel: 'l-no',
                        size: 'md', // sm = small | md = medium | lg = larger
                        confirmLabel: 'l-yes',
                        callback: function (isPositiveResult) {
                            if (isPositiveResult) {
                                atualizaModel(self.listaProcessoSeletivoSelecionado(self.model.CODCOLIGADA, self.model.IDPS)[0]);
                                carregaParametrosProcessoSeletivo(self.model.CODCOLIGADA, self.model.IDPS, self.model.ETAPAPS, codigosGruposParametros);

                                if ($state.is('centralcandidato.start') || $state.is('dadospessoais.start')) {
                                    $state.go('login.manual', { nivelEnsino: $rootScope.nivelEnsino });
                                }
                                else {
                                    $state.go('informacoes.start', { nivelEnsino: $rootScope.nivelEnsino });
                                }
                            }
                            else {
                                self.model.CODCOLIGADA = $rootScope.CodColigadaAnt;
                                self.model.IDPS = $rootScope.IdPSAnt;
                                self.model.ETAPAPS = $rootScope.etapaPSAnt;
                                self.model.NOME = $rootScope.NomeProcessoSeletivoAnt;
                                $rootScope.ValorInscricao = $rootScope.ValorInscricaoAnt;
                                carregaParametrosProcessoSeletivo(self.model.CODCOLIGADA, self.model.IDPS, self.model.ETAPAPS, codigosGruposParametros);
                            }
                        }
                    });
                }

                if (!alterouPSInscricao) {
                    atualizaModel(self.listaProcessoSeletivoSelecionado(self.model.CODCOLIGADA, self.model.IDPS)[0]);
                    carregaParametrosProcessoSeletivo(self.model.CODCOLIGADA, self.model.IDPS, self.model.ETAPAPS, codigosGruposParametros, function (){
                        if ($state.$current.self.name === 'informacoes.start') {
                            $state.reload();
                        } else {
                            $state.go('informacoes.start', { nivelEnsino: $rootScope.nivelEnsino });
                        }
                    });
                }
            }

            function carregaParametrosProcessoSeletivo(codColigada, idPS, etapaPS, codigosGruposParametros, callback) {

                EdupsUtilsFactory.getParametrosProcessoSeletivo(codColigada, idPS, codigosGruposParametros, function (result) {
                    if (angular.isDefined(result)) {

                        var parametros = result[0];
                        $rootScope.objParametros = {};

                        //Coligada e Identificador do Processo Seletivo e outros dados
                        $rootScope.CodColigada = codColigada;
                        $rootScope.IdPS = idPS;
                        $rootScope.EtapaPS = etapaPS;
                        $rootScope.NomeProcessoSeletivo = self.model.NOME;

                        // Coligada e Identificador do Processo Seletivo Anterior a alteração
                        $rootScope.CodColigadaAnt = codColigada;
                        $rootScope.IdPSAnt = idPS;
                        $rootScope.etapaPSAnt = etapaPS;
                        $rootScope.NomeProcessoSeletivoAnt = self.model.NOME;

                        // O valor da inscrição que prevalecerá será o da área ofertada da primeira opção
                        $rootScope.UsaGrupoAreaInteressePS = (self.model.GRUPOAREAINTERESSE === 'T');
                        $rootScope.TextoGrupoAreaInteressePS = self.model.TEXTOGRUPOAREAINTERESSE;

                        // Parâmetros do Cadastro do Processo Seletivo
                        $rootScope.objParametros.AceitaTreineiro = parametros.AceitaTreineiro;
                        $rootScope.objParametros.NecessidadesEspeciais = parametros.NecessidadesEspeciais;

                        // Parâmetros de Área de Informações 1/2
                        $rootScope.objParametros.NomePortalInscricoes = parametros.NomePortalInscricoes;
                        $rootScope.objParametros.UsaSenhaLogin = parametros.UsaSenhaLogin;
                        $rootScope.objParametros.LoginCpf = parametros.LoginCpf;
                        $rootScope.objParametros.LoginRG = parametros.LoginRG;
                        $rootScope.objParametros.LoginCodUsuario = parametros.LoginCodUsuario;
                        $rootScope.objParametros.LoginEmail = parametros.LoginEmail;

                        // Parâmetros de Área de Informações 2/2
                        $rootScope.objParametros.TextoAreaPublica = $sce.trustAsHtml(parametros.TextoAreaPublica);

                        // Parâmetros da Área de Inscrições
                        $rootScope.objParametros.TextoAreaInscricoes = $sce.trustAsHtml(parametros.TextoAreaInscricoes);

                        // Parâmetros da Central do candidato
                        $rootScope.objParametros.TextoLogin = $sce.trustAsHtml(parametros.TextoLogin);
                        $rootScope.objParametros.TextoRecuperarSenha = $sce.trustAsHtml(parametros.TextoRecuperarSenha);

                        // Parâmetros Geral 1/3
                        $rootScope.objParametros.UsaDadosPai = parametros.UsaDadosPai;
                        $rootScope.objParametros.UsaDadosMae = parametros.UsaDadosMae;
                        $rootScope.objParametros.UsaDadosRespAcad = parametros.UsaDadosRespAcad;
                        $rootScope.objParametros.UsaDadosRespFin = parametros.UsaDadosRespFin;
                        $rootScope.objParametros.NaoExigeDadosPai = parametros.NaoExigeDadosPai;
                        $rootScope.objParametros.NaoExigeDadosMae = parametros.NaoExigeDadosMae;
                        $rootScope.objParametros.NaoExigeDadosRespAcad = parametros.NaoExigeDadosRespAcad;
                        $rootScope.objParametros.NaoExigeDadosRespFin = parametros.NaoExigeDadosRespFin;
                        $rootScope.objParametros.ExigeSobrenome = parametros.ExigeSobrenome;
                        $rootScope.objParametros.GravarDadosMaiusculo = parametros.GravarDadosMaiusculo;
                        $rootScope.objParametros.UsaCampoComplementar = parametros.UsaCampoComplementar;
                        $rootScope.objParametros.ResponsavelCandidato = parametros.ResponsavelCandidato;
                        $rootScope.objParametros.TextoTreineiro = parametros.TextoTreineiro;
                        $rootScope.objParametros.GrupoPesquisa1 = parametros.GrupoPesquisa1;
                        $rootScope.objParametros.GrupoPesquisa2 = parametros.GrupoPesquisa2;
                        $rootScope.objParametros.GrupoPesquisa3 = parametros.GrupoPesquisa3;

                        // Parâmetros Geral 2/3
                        $rootScope.objParametros.TipoBoleto = parametros.TipoBoleto;
                        $rootScope.objParametros.CabecalhoComprovante = parametros.CabecalhoComprovante;
                        $rootScope.objParametros.BoletoComprovante = parametros.BoletoComprovante;
                        $rootScope.objParametros.TextoComprovante = parametros.TextoComprovante;

                        // Parâmetros Geral 3/3
                        $rootScope.objParametros.ObrigarCPFPassaporteRG = parametros.ObrigarCPFPassaporteRG;
                        $rootScope.objParametros.PaisOrigemPS = parametros.PaisOrigemPS;
                        $rootScope.objParametros.NaoObrigaDocCandEst = parametros.NaoObrigarDocsCandEstrangeiro;
                        $rootScope.objParametros.NaoObrigaDocRespEst = parametros.NaoObrigarDocsRespEstrangeiro;
                        $rootScope.objParametros.ServicoDeInscricao = parametros.ServicoDeInscricao;
                        $rootScope.objParametros.ServicoDeResultado = parametros.ServicoDeResultado;

                        // Parâmetros de Resultados
                        $rootScope.objParametros.TextoAreaResultados = $sce.trustAsHtml(parametros.TextoAreaResultados);
                        $rootScope.objParametros.MostraNumVagas = parametros.MostraNumVagas;
                        $rootScope.objParametros.MostraClassificacao = parametros.MostraClassificacao;
                        $rootScope.objParametros.MostraPontuacao = parametros.MostraPontuacao;
                        $rootScope.objParametros.MostrarNumInsc = parametros.MostrarNumInsc;
                        $rootScope.objParametros.MostrarSituacao = parametros.MostrarSituacao;
                        $rootScope.objParametros.MostrarCampoClassGeral = parametros.MostrarCampoClassGeral;
                        $rootScope.objParametros.MostrarPontuacaoGeral = parametros.MostrarPontuacaoGeral;
                        $rootScope.objParametros.MostraResultGeral = parametros.MostraResultGeral;
                        $rootScope.objParametros.MostraCandDesc = parametros.MostraCandDesc;
                        $rootScope.objParametros.MostrarClassGeral = parametros.MostrarClassGeral;
                        $rootScope.objParametros.MostrarChamadas = parametros.MostrarChamadas;

                        // Parâmetros de Redes Sociais
                        $rootScope.objParametros.UrlFacebook = parametros.UrlFacebook;
                        $rootScope.objParametros.UrlTwitter = parametros.UrlTwitter;
                        $rootScope.objParametros.UrlInstagram = parametros.UrlInstagram;
                        $rootScope.objParametros.NumeroWhatsApp = parametros.NumeroWhatsApp;

                        // Parâmetros de Visibilidade e Obrigatoriedade dos Campos
                        $rootScope.objParametros.ListaParametrosPessoa = parametros.ListaParametrosPessoa;

                        // Parâmetros de Arzenamento de Arquivos
                        $rootScope.objParametros.TipoArquivo = parametros.TipoArquivo;
                        $rootScope.objParametros.DiretorioArquivo = parametros.DiretorioArquivo;
                        $rootScope.objParametros.ExtensaoArquivo = parametros.ExtensaoArquivo;
                        $rootScope.objParametros.TamanhoMaximoArquivo = parametros.TamanhoMaximoArquivo;

                        //Parâmetros de Grupos de Campos Complementares
                        $rootScope.objParametros.ListaCampoComplFilialGrupo = parametros.ListaCampoComplFilialGrupo;

                        //Itens de tabela dinâmica
                        $rootScope.objParametros.ListaTabelaDinamicaItem = parametros.ListaTabelaDinamicaItem;

                        // Parâmetros de Máscaras
                        $rootScope.objParametros.MascaraTelefone = parametros.MascaraTelefone;
                        $rootScope.objParametros.MascaraCEP = parametros.MascaraCEP;

                        // Parâmetros Informações Financeiras
                        $rootScope.objParametros.PagamentoCartao = parametros.PagamentoCartao;

						// Parâmetros de Cotas de Instituições Federais
                        $rootScope.objParametros.ParamsCotaInstituicaoFederal = parametros.ParamsCotaInstituicaoFederal;

                        // Parâmetros de Nomes dos Campos de Telefone
                        $rootScope.objParametros.TextoTelefone1 = parametros.TextoTelefone1;
                        $rootScope.objParametros.TextoTelefone2 = parametros.TextoTelefone2;
                        $rootScope.objParametros.TextoTelefone3 = parametros.TextoTelefone3;

                        // Central de Notificação
                        $rootScope.objParametros.enviarEmailConfirmacaoInscricao = parametros.enviarEmailConfirmacaoInscricao;
                        
                        // Integração
                        $rootScope.objParametros.UsaIntegracaoRubeus = parametros.UsaIntegracaoRubeus;

                        configuraEtapaProcessoSeletivo($rootScope.CodColigada, $rootScope.IdPS, etapaPS);

                        self.listaTipoIdentificacaoLogin();

                        if (typeof callback === 'function') {
                            callback();
                        }
                    }
                });
            }

            function configuraEtapaProcessoSeletivo(codColigada, idPS, descricaoEtapa) {
                EdupsUtilsFactory.getListaFormaInscricao(codColigada, idPS, function(result) {
                    var possuiFormaInscricaoValida = result.SPSFormaInscricaoPS.length > 0;

                    // As datas de inscrição e resultado podem coincidir, por isso, pode ser necessário exibir as duas etapas.
                    if (descricaoEtapa === 'INSCRICAO|RESULTADOS') {
                        $rootScope.PeriodoInscricao = possuiFormaInscricaoValida;
                        $rootScope.PeriodoResultado = true;
                    } else {
                        $rootScope.PeriodoInscricao = (descricaoEtapa === 'INSCRICAO' && possuiFormaInscricaoValida);
                        $rootScope.PeriodoResultado = (descricaoEtapa === 'RESULTADOS');
                    }
                });
            }

            function listaTipoIdentificacaoLogin() {

                $rootScope.listaTipoIdentificacaoLogin = [];

                if ($rootScope.objParametros.LoginCpf === true) {
                    $rootScope.listaTipoIdentificacaoLogin.push({ CODIGO: '0', DESCRICAO: i18nFilter('l-acesso-cpf', [], 'js/global-app') });
                }

                if ($rootScope.objParametros.LoginRG === true) {
                    $rootScope.listaTipoIdentificacaoLogin.push({ CODIGO: '1', DESCRICAO: i18nFilter('l-acesso-identidade', [], 'js/global-app') });
                }

                if ($rootScope.objParametros.LoginCodUsuario === true) {
                    $rootScope.listaTipoIdentificacaoLogin.push({ CODIGO: '2', DESCRICAO: i18nFilter('l-acesso-codusuario', [], 'js/global-app') });
                }

                if ($rootScope.objParametros.LoginEmail === true) {
                    $rootScope.listaTipoIdentificacaoLogin.push({ CODIGO: '3', DESCRICAO: i18nFilter('l-acesso-email', [], 'js/global-app') });
                }
            }

            function logout() {

                totvsNotification.question({
                    title: i18nFilter('l-confirmacao', [], 'js/global-app'),
                    text: i18nFilter('l-msg-confirmacao-logout', [], 'js/global-app'),
                    cancelLabel: 'l-no',
                    size: 'sm', // sm = small | md = medium | lg = larger
                    confirmLabel: 'l-yes',
                    callback: function (isPositiveResult) {
                        if (isPositiveResult) {
                            realizarLogout();
                        }
                    }
                });

            }

            function realizarLogout() {

                edupsLoginFactory.realizarLogout(function () {

                    totvsNotification.notify({
                        type: 'success',
                        title: i18nFilter('l-titulo', [], 'js/login'),
                        detail: i18nFilter('l-logout-success', [], 'js/global-app'),
                    });
                    $state.go('informacoes.start', { nivelEnsino: $rootScope.nivelEnsino });
                });
            }

            function carregarBanner(codColigada, idPS) {
                if (idPS) {
                    EdupsUtilsFactory.getBanners(codColigada, idPS, function (result) {
                        var _slides = [];

                        result.forEach(function (item) {
                            _slides.push({
                                'img': item.IMAGEM,
                                'link': item.LINK,
                                'title': item.TITULO,
                                'isBase64': true
                            });
                        });
                        self.slides = _slides;
                    });
                }
            }

            function getParametrosURL(parametro) {

                var url = window.location.search.substring(1),
                    paramsLoginAutomatico = window.location.hash.split('?'),
                    possuiParametrosComplementares = (paramsLoginAutomatico.length > 1 && paramsLoginAutomatico[1].indexOf('token') > 0);

                if (!window.location.search && possuiParametrosComplementares) {
                    url = window.location.hash.split('?')[1];
                }

                var parametros = url.split('&');

                for (var i = 0; i < parametros.length; i++) {
                    var par = parametros[i].split('=');

                    if (par[0] === parametro) {
                        return par[1];
                    }
                }
            }

            function SelecionaIdPSInformado(listaProcessosSeletivos, codColigada, idPS) {
                var escolhido;

                for (var i = 0; i < listaProcessosSeletivos.length; i++) {
                    var element = listaProcessosSeletivos[i];

                    if (element.CODCOLIGADA == codColigada && element.IDPS == idPS) {
                        escolhido = i;
                        break;
                    } else {
                        escolhido = 0;
                    }
                }

                return escolhido;
            }

            function carregarPaginaPrincipal(){

                var urlPaginaPrincipal = '/' + document.eduPSNivelEnsino + '/informacoes';
                $location.path(urlPaginaPrincipal);
            }
        }
    });
