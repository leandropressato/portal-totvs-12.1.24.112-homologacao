define(['utils/edups-utils.module'], function () {

    'use strict';

    angular
        .module('edupsUtilsModule')
        .factory('edupsApiUnis', EdupsApiUnis);

    EdupsApiUnis.$inject = ['$http', '$q'];

    function EdupsApiUnis($http, $q) {

		//api url
		var parseUrl      = "https://api.unis.edu.br";
		var module        = "novoportaltotvs";
		var configRequest = {};
		var canceller;

		var setConfig = function() {
			canceller                           = $q.defer();
			configRequest.headers               = {
	        'Authorization': 'Basic ZGV2ZWxBcGlVbmlzOnVuMSQmbTNHVkdAITM2OQ=='
		        //'Accept': 'application/json;odata=verbose',
		        //"X-Testing" : "testing",
		        //"Accept-Language" : "pt-BR,pt;q=0.8,en;q=0.4,en-US;q=0.6"
		    };
			configRequest.headers["Authorization"] = 'Basic ZGV2ZWxBcGlVbmlzOnVuMSQmbTNHVkdAITM2OQ==';
			//configRequest.headers["test"] = "outro teste";
			//configRequest.headers["Content-Type"] = "application/json";
			configRequest.timeout               = canceller.promise;
			$http.defaults.headers.options = {"Access-Control-Request-Headers": "Accept, Origin, Authorization"}; //you probably don't need this line.  This lets me connect to my server on a different domain
	    	$http.defaults.headers.options['Authorization'] = 'Basic ' + 'ZGV2ZWxBcGlVbmlzOnVuMSQmbTNHVkdAITM2OQ==';

	    	//$http.defaults.headers.common['teste'] = 'test value';

		}

		return {

			//Read - get data in API (DB)
			get: function(method, params, callback, uri){
				setConfig();
				$http.defaults.headers.options['Authorization'] = 'Basic ZGV2ZWxBcGlVbmlzOnVuMSQmbTNHVkdAITM2OQ==';
				if ( uri != undefined )
					var url = parseUrl+'/'+uri;
				else {
					var uriParam = "";
					
					//monta a query string de parâmetros
					angular.forEach(params, function(value, key){
						uriParam += key+"="+value+"&";
					});

					//remove o último e comercial (&)
					uriParam = uriParam.substring(0, uriParam.length - 1);
	 	
	 				//monta a url completa de comunicação com a api
					var url = parseUrl+'/'+module+"?method="+method+"&"+uriParam;

					$http.get(url, configRequest).success(function(res) {
					
						//converte o retorno da API de xml para Json
						var x2js = new X2JS();
						var jsonObj = x2js.xml_str2json( res );
						var json = jsonObj.Application_Models_NovoPortalTotvs[method];
						var status = json.status;
						delete json['status'];

	    				//console.log(">>> URL: "+url);
						callback(json, status);
						
					}).error(callback);
				}
			},

			//Read - get data in API (DB)
			post: function(method, params, callback, uri){
				setConfig();
				if ( uri != undefined )
					var url = parseUrl+'/'+uri;
				else {
					var uriParam = "";
					
					//monta a query string de parâmetros
					angular.forEach(params, function(value, key){
						uriParam += key+"="+value+"&";
					});

					//remove o último e comercial (&)
					uriParam = uriParam.substring(0, uriParam.length - 1);
	 	
	 				//monta a url completa de comunicação com a api
					var url = parseUrl+'/'+module+"?method="+method+"&"+uriParam;
					
					$http.post(url, {}, configRequest).success(function(res) {
					
						//converte o retorno da API de xml para Json
						var x2js = new X2JS();
						var jsonObj = x2js.xml_str2json( res );
						var json = jsonObj.Application_Models_NovoPortalTotvs[method];
						var status = json.status;

						callback(status, json);
						
					}).error(callback);
				}
			},

			//Update - update data in API (DB)			
			update: function(method, params, callback, uri){
				setConfig();
				if ( uri != undefined )
					var url = parseUrl+'/'+uri;
				else {
					var uriParam = "";
					
					//monta a query string de parâmetros
					angular.forEach(params, function(value, key){
						uriParam += key+"="+value+"&";
					});

					//remove o último e comercial (&)
					uriParam = uriParam.substring(0, uriParam.length - 1);
	 	
	 				//monta a url completa de comunicação com a api
					var url = parseUrl+'/'+module+"?method="+method+"&"+uriParam;

					$http.put(
						url, 
						{},
						configRequest
					).success(function(res) {
					
						//converte o retorno da API de xml para Json
						var x2js = new X2JS();
						var jsonObj = x2js.xml_str2json( res );
						var json = jsonObj.Application_Models_NovoPortalTotvs[method];
						var status = json.status;

						callback(status);
						
					}).error(callback);
				}
			},

			delete: function(){

			},

			cancel: function(reason){
				canceller.resolve(reason);
				console.log(reason);
				canceller = null;
			},

			teste: function(){
				console.log("api teste");
			}

		};


    }
});
