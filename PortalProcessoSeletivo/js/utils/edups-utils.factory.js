/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['utils/edups-utils.module'], function () {

    'use strict';

    angular
        .module('edupsUtilsModule')
        .factory('edupsUtilsFactory', EdupsUtilsFactory);

    EdupsUtilsFactory.$inject = ['$totvsresource'];

    function EdupsUtilsFactory($totvsresource) {

        var self = this,
            url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS;

        self.getListaProcessosSeletivos = getListaProcessosSeletivos;
        self.getParametrosProcessoSeletivo = getParametrosProcessoSeletivo;
        self.getListaArquivosProcessoSeletivo = getListaArquivosProcessoSeletivo;
        self.getArquivoDownload = getArquivoDownload;

        self.getListaPaises = getListaPaises;
        self.getListaEstados = getListaEstados;
        self.getListaMunicipios = getListaMunicipios;
        self.getListaEstadoCivil = getListaEstadoCivil;
        self.getListaNacionalidade = getListaNacionalidade;
        self.getListaCorRaca = getListaCorRaca;
        self.getListaProfissao = getListaProfissao;
        self.getListaGrauInstrucao = getListaGrauInstrucao;
        self.getListaTipoSanguineo = getListaTipoSanguineo;
        self.getListaTipoRua = getListaTipoRua;
        self.getListaTipoBairro = getListaTipoBairro;
        self.getListaTipoDeficiencia = getListaTipoDeficiencia;
        self.getListaLocalRealizacaoProva = getListaLocalRealizacaoProva;
        self.getListaAreaOfertada = getListaAreaOfertada;
        self.getListaIdioma = getListaIdioma;
        self.getListaFormaInscricao = getListaFormaInscricao;
        self.getListaGruposAreaInteresse = getListaGruposAreaInteresse;
        self.getListaAtividadesAgendadas = getListaAtividadesAgendadas;
        self.getListaCampusLocalRealizacaoProva = getListaCampusLocalRealizacaoProva;

        self.getEnderecoByCEP = getEnderecoByCEP;

        self.getAluno = getAluno;
        self.getClienteFornecedor = getClienteFornecedor;
        self.getFuncionario = getFuncionario;

        self.getFormaInscricaoIndicacao = getFormaInscricaoIndicacao;
        self.getBanners = getBanners;
        self.verificaVagaExcedente = verificaVagaExcedente;

        return self;

        //Retorna o objeto factory a partir da URL padrão juntamento ao complemeto
        function getInstanciaFactory(urlComplemento) {
            var urlCompletaServico = url + urlComplemento;
            return $totvsresource.REST(urlCompletaServico, {}, {});
        }

        // Retorna lista de processos seletivos ativos
        function getListaProcessosSeletivos(codColigada, codFilial, codCategoria, apresentacao, callback) {
            var parametros = {},
                factory = null;

            parametros.apresentacao = apresentacao;

            if ((codColigada !== -1) && (codFilial !== -1) && (codCategoria === '')) {
                factory = getInstanciaFactory('/ListaProcessosSeletivos/:codColigada/:codFilial/:apresentacao');
                parametros.codColigada = codColigada;
                parametros.codFilial = codFilial;
            } else if ((codColigada !== -1) && (codFilial !== -1) && (codCategoria !== '')) {
                factory = getInstanciaFactory('/ListaProcessosSeletivos/:codColigada/:codFilial/:codCategoria/:apresentacao');
                parametros.codColigada = codColigada;
                parametros.codFilial = codFilial;
                parametros.codCategoria = codCategoria;
            } else {
                factory = getInstanciaFactory('/ListaProcessosSeletivos/:apresentacao');
            }

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna os parâmetros do processo seletivo de acordo com os grupos passados
        function getParametrosProcessoSeletivo(codColigada, idPS, codigosGruposParametros, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.codigosGruposParametros = codigosGruposParametros;
            var factory = getInstanciaFactory('/ParametrosProcessoSeletivo/:codColigada/:idPS/:codigosGruposParametros');
            return factory.TOTVSQuery(parametros, callback);
        }

        // Retorna a lista de arquivos do processo seletivo de acordo com os grupos passados
        function getListaArquivosProcessoSeletivo(codColigada, idPS, idCategoria, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.idCategoria = idCategoria;

            var factory = getInstanciaFactory('/ListaArquivosProcessoSeletivo/:codColigada/:idPS/:idCategoria');
            return factory.TOTVSQuery(parametros, callback);
        }

        // Retorna o arquivo do processo seletivo para download
        function getArquivoDownload(idArquivo, formaArmazenamento, pathArquivo) {
            var urlDownload = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSProcessoSeletivo/DownloadArquivo?' + 'idArquivo=' + idArquivo + '&formaArmazenamento=' + formaArmazenamento + '&pathArquivo=' + pathArquivo;
            return urlDownload;
        }

        // Retorna a lista de países da basae de dados
        function getListaPaises(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaPaises');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de estados da basae de dados com base no país passado como parâmetro
        function getListaEstados(idPais, callback) {
            var parametros = {};
            parametros.idPais = idPais;

            var factory = getInstanciaFactory('/ListaEstados/:idPais');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de estados da basae de dados com base no país passado como parâmetro
        function getListaMunicipios(codEtd, callback) {
            var parametros = {};
            parametros.codEtd = codEtd;

            var factory = getInstanciaFactory('/ListaMunicipios/:codEtd');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de estado civil
        function getListaEstadoCivil(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaEstadoCivil');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de nacionalidades
        function getListaNacionalidade(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaNacionalidade');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de cor/raça
        function getListaCorRaca(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaCorRaca');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de profissões
        function getListaProfissao(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaProfissao');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de graus de instrução
        function getListaGrauInstrucao(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaGrauInstrucao');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de tipo sanguíneo
        function getListaTipoSanguineo(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaTipoSanguineo');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de tipo de rua
        function getListaTipoRua(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaTipoRua');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de tipo de rua
        function getListaTipoBairro(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaTipoBairro');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista locais de realização da prova
        function getListaLocalRealizacaoProva(codColigada, idPS, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;

            var factory = getInstanciaFactory('/ListaLocalRealizacaoProva/:codColigada/:idPS');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista locais de realização da prova
        function getListaCampusLocalRealizacaoProva(codColigada, idPS, codMunicipio, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.codMunicipio = codMunicipio;

            var factory = getInstanciaFactory('/ListaCampusLocalRealizacaoProva/:codColigada/:idPS/:codMunicipio');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista de áreas ofertadas do processo seletivo
        function getListaAreaOfertada(codColigada, idPS, codGrupo, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.codGrupo = codGrupo;

            var factory = getInstanciaFactory('/ListaAreaOfertada/:codColigada/:idPS/:codGrupo');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista opções de idioma do processo seletivo
        function getListaIdioma(codColigada, idPS, idAreaInteresse, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.idAreaInteresse = idAreaInteresse;

            var factory = getInstanciaFactory('/ListaIdiomasAreaOfertada/:codColigada/:idPS/:idAreaInteresse');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista formas de inscrição do processo seletivo
        function getListaFormaInscricao(codColigada, idPS, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;

            var factory = getInstanciaFactory('/ListaFormaInscricao/:codColigada/:idPS');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna a lista dos grupos de área de interesse do processo seletivo
        function getListaGruposAreaInteresse(codColigada, idPS, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;

            var factory = getInstanciaFactory('/ListaGruposAreaInteresse/:codColigada/:idPS');

            return factory.TOTVSGet(parametros, callback);
        }

        // Retorna lista de atividades agendadas do processo seletivo
        function getListaAtividadesAgendadas(codColigada, idPS, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;

            var factory = getInstanciaFactory('/ListaAtividadesAgendadas');

            return factory.TOTVSQuery(parametros, callback);
        }

        //Retorna o endereço de acordo com o CEP
        function getEnderecoByCEP(numeroCEP, callback) {
            var parametros = {};
            parametros.numeroCEP = numeroCEP;

            var factory = getInstanciaFactory('/BuscaEnderecoPorCEP/:numeroCEP');

            return factory.TOTVSQuery(parametros, callback);
        }

        //Retorna o aluno
        function getAluno(codColigada, ra, callback) {

            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.ra = ra;

            var factory = getInstanciaFactory('/Aluno/:codColigada/:ra');

            return factory.TOTVSQuery(parametros, callback);
        }

        //Retorna o cliente/fornecedor
        function getClienteFornecedor(codColigada, codCfo, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.codCfo = codCfo;

            var factory = getInstanciaFactory('/ClienteFornecedor/:codColigada/:codCfo');

            return factory.TOTVSQuery(parametros, callback);
        }

        //Retorna o funcionário/professor
        function getFuncionario(codColigada, chapa, callback) {

            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.chapa = chapa;

            var factory = getInstanciaFactory('/Funcionario/:codColigada/:chapa');

            return factory.TOTVSQuery(parametros, callback);
        }

        //Verifica se a forma de inscrição selecionada utiliza indicação de aluno
        function getFormaInscricaoIndicacao(codColigada, idPS, idFormaInscricao, callback) {

            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.idFormaInscricao = idFormaInscricao;

            var factory = getInstanciaFactory('/FormaInscricaoIndicacao/:codColigada/:idPS/:idFormaInscricao');

            return factory.TOTVSQuery(parametros, callback);
        }

        // Retorna a lista de tipo de deficiências
        function getListaTipoDeficiencia(callback) {
            var parametros = {},
                factory = getInstanciaFactory('/ListaTipoDeficiencia');

            return factory.TOTVSGet(parametros, callback);
        }

        /**
         *Retorna lista de Banners cadastrados no Processo Seletivo.
         *
         * @param {Number} codColigada - Cód. Coligada
         * @param {Number} idPS - Id. Processo Seletivo
         * @param {Function} callback - Função de callback
         * @returns {Object} - Banners
         */
        function getBanners(codColigada, idPS, callback) {

            var parametros = { 'codColigada' : codColigada, 'idPS' : idPS },
                factory = getInstanciaFactory('/Banner/:codColigada/:idPS');

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * @public
         * @function Verifica se a inscrição será excedente
         * @name verificaVagaExcedente
         * @param   {int}    codColigada     Código da Coligada
         * @param   {int}    idPS            Identificador do Processo Seletivo
         * @param   {int}    idAreaInteresse Identificador da Área de Interesse
         * @callback Objeto com o número de próximo excedente
         * @returns {object} Objeto com o número de próximo excedente
         */
        function verificaVagaExcedente(codColigada, idPS, idAreaInteresse, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.idAreaInteresse = idAreaInteresse;

            var factory = getInstanciaFactory('/VerificaVagaExcedente/:codColigada/:idPS/:idAreaInteresse');

            return factory.TOTVSGet(parametros, callback);
        }
    }
});
