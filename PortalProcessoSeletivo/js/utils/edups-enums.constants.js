/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module edupsUtilsModule
* @name edupsEnumsConsts
* @object constants
*
* @created 03/10/2016 v12.1.15
* @updated
*
* @description Constantes para enums do Processo Seletivo.
*/

define(['utils/edups-utils.module'], function () {

    'use strict';

    angular
        .module('edupsUtilsModule')
        .constant('edupsEnumsConsts', {
            EdupsTipoUsuarioPortalEnum: {
                Aluno: 'A'
            },
            EdupsTipoApresentacaoEnum: {
                EnsinoBasico: 0,
                EnsinoSuperior: 1
            },
            EdupsSimOuNaoEnum: {
                Nao: 'N',
                Sim: 'S'
            },
            EdupsTipoArquivoBoletoEnum: {
                HTML: 'HTML',
                PDF: 'PDF'
            },
            EdupsTrueFalseEnum: {
                True: 'T',
                False: 'F'
            },
            EdupsTipoEstadoSemPais: {
                EstadoSemPais: -1
            }
        });
});
