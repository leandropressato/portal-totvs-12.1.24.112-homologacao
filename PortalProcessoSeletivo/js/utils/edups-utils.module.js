/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsUtilsModule
* @object module
*
* @created 03/10/2016 v12.1.15
* @updated
*
* @dependencies
*
* @description Módulo do Enums.
*/
define([], function () {

    'use strict';

    angular
        .module('edupsUtilsModule', []);
});
