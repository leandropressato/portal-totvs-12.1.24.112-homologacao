/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module edupsUtilsModule
 * @name edupsUtilsService
 * @object service
 *
 * @created 2016-10-11 v12.1.15
 * @updated
 *
 * @requires
 *
 * @description Service utils do Processo Seletivo
 */
define(['utils/edups-utils.module'], function () {

    'use strict';

    angular
        .module('edupsUtilsModule')
        .service('edupsUtilsService', EdupsUtilsService);

    EdupsUtilsService.$inject = ['edupsUtilsFactory'];

    function EdupsUtilsService() {

        var self = this;

        self.abrirJanelaDownloadArquivo = abrirJanelaDownloadArquivo;
        self.b64toBlob = b64toBlob;

        // FUNCTIONS

        function abrirJanelaDownloadArquivo(nomeArquivo, arquivoBytes) {
            var blob = b64toBlob(arquivoBytes);
            saveAs(blob, nomeArquivo);
        }

        function b64toBlob (b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data),
                byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {

                var slice = byteCharacters.slice(offset, offset + sliceSize),
                byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {
                type: contentType
            });
            return blob;
        }
    }
});
