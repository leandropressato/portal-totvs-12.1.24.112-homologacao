[
    {
        "l-titulo": {
            "pt": "Relatório",
			"en": "Report",
			"es": "Informe"
        },
        "l-msg-erro-emissao": {
            "pt": "Ocorreu um erro ao emitir o relatório!",
			"en": "Error issuing report!",
			"es": "¡Ocurrió un error al emitir el informe!"
        }
    }
]
