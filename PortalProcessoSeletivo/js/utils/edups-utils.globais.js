define(function () {
    'use strict';
    return {
        validaViewCustomizada: function (rota) {

            var viewCustomizada = false;

            switch (rota) {
                case 'informacoes.start':
                    if ((EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INFORMACOES === true && document.eduPSNivelEnsino === 'es') ||
                        (EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INFORMACOES_EB === true && document.eduPSNivelEnsino === 'eb')) {
                        viewCustomizada = true;
                    }
                    break;
                case 'resultados.start':
                    if ((EDUPS_CONST_GLOBAL_CUSTOM_VIEW_RESULTADOS === true && document.eduPSNivelEnsino === 'es') ||
                        (EDUPS_CONST_GLOBAL_CUSTOM_VIEW_RESULTADOS_EB === true && document.eduPSNivelEnsino === 'eb')) {
                        viewCustomizada = true;
                    }
                    break;
                case 'centralcandidato.start':
                    if ((EDUPS_CONST_GLOBAL_CUSTOM_VIEW_PORTAL_CANDIDATO === true && document.eduPSNivelEnsino === 'es') ||
                        (EDUPS_CONST_GLOBAL_CUSTOM_VIEW_PORTAL_CANDIDATO_EB === true && document.eduPSNivelEnsino === 'eb')) {
                        viewCustomizada = true;
                    }
                    break;
                case 'inscricoesWizard':
                    if ((EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INSCRICOES === true && document.eduPSNivelEnsino === 'es') ||
                        (EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INSCRICOES_EB === true && document.eduPSNivelEnsino === 'eb')) {
                        viewCustomizada = true;
                    }
                    break;
            }

            return viewCustomizada;
        }
    }
});
