define(['utils/interceptors/edups-interceptors.module'], function () {

    'use strict';

    angular
        .module('edupsInterceptorsModule')
        .factory('EdupsInterceptors', EdupsInterceptors);

    EdupsInterceptors.$inject = ['$q', '$injector'];

    // *********************************************************************************
    // *** Factory
    // *********************************************************************************

    /**
     * Factory para interceptar as mensagens dos serviços REST
     * @param   {object} $q Objeto para tratar as requisições e respostas
     * @param   {object} $injector Objeto para tratar o $state sem ter referência circular
     * @returns {object} Factory interceptadora
    */
    function EdupsInterceptors ($q, $injector) {
        var factory = {
            request: request,
            requestError: requestError,
            response: response,
            responseError: responseError
        };

        return factory;

        /**
         * Requisição OK
         * @param   {object} config Configurações da requisição
         * @returns {object} Objeto da requisição
         */
        function request(config) {
            return config || $q.when(config);
        }

        /**
         * Requisição com erro
         * @param   {object} rejection Requisição que ocorreu o erro
         * @returns {object} Objeto requisitado rejeitado
         */
        function requestError(rejection) {
            return $q.reject(rejection);
        }

        /**
         * Resposta OK
         * @param   {object} response Objeto de resposta
         * @returns {object} Objeto de resposta
         */
        function response(response) {
            if (response) {
                if (angular.isDefined(response['RMException:StackTrace'])) {
                    console.error(response['RMException:StackTrace']);
                }

                if (angular.isDefined(response['data']) && angular.isDefined(response['data']['RMException:StackTrace'])) {
                    console.error(response['data']['RMException:StackTrace']);
                }
            }

            return response;
        }

        /**
         * Resposta com erro
         * @param   {object} rejection Resposta que ocorreu o erro
         * @returns {object} Objeto de resposta rejeitado
         */
        function responseError(rejection) {

            /**
             * Tratamento de login expirado.
             * Retornado pelo serviço que possua o atributo [EduPSAuthorize].
             */
            if (rejection.status === 401) {
                var $state = $injector.get('$state'),
                    $rootScope = $injector.get('$rootScope'),
                    parametrosUrl = $state.params;

                if (parametrosUrl.token) {
                    $state.transitionTo('login.automatico', { token: parametrosUrl.token, action: parametrosUrl.action, nivelEnsino: $rootScope.nivelEnsino, c: parametrosUrl.c, ps: parametrosUrl.ps, insc: parametrosUrl.insc});
                }
                else {
                    /**
                     * O parâmetro expired é utilizado em EdupsLoginController para emitir mensagem ao usuário
                     * informando que a conexão expirou. Parâmetro que interage apenas com a mensagem exibida,
                     * não tendo reflexo em nenhum validação do sistema, client ou server.
                     *
                     * Este parâmetro foi alterado para 0 porque a mensagem estava sendo exibida também no 1o acesso a central do
                     * candidado antes dele efetuar o login.
                     * Deve ser encontrada outra forma de exibir esta mensagem apenas quando o token do usuário expirar no servidor.
                     */
                    $state.transitionTo('login.manual', { action: parametrosUrl.action, expired: 0, nivelEnsino: $rootScope.nivelEnsino });
                }
            }

            return $q.reject(rejection);
        }
    }
});
