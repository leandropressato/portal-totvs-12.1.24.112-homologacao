/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/*************************************************************************/
/**                                                                     **/
/**  I M P O R T A N T E                                                **/
/**  -------------------                                                **/
/**                                                                     **/
/**  Este arquivo deve conter todas as variáveis/constantes de          **/
/**  de configuração global do TOTVS Portal Processo Seletivo           **/
/**                                                                     **/
/*************************************************************************/

var EDUPS_CONST_GLOBAL_URL_BASE_APP = CONST_GLOBAL_URL_BASE_APP + 'app/Edu/PortalProcessoSeletivo/';
var EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSProcessoSeletivo/:method';

/* Indica o idioma padrão do processo seletivo | 'pt', 'es' e 'en' */
var EDUPS_CONST_GLOBAL_CUSTOM_IDIOMA = 'pt';

/*Indica que o CSS do processo seletivo será customizado*/
var EDUPS_CONST_GLOBAL_CUSTOM_CSS = false;

/*Indica que as imagens do processo seletivo serão customizadas*/
var EDUPS_CONST_GLOBAL_CUSTOM_IMAGES = false;

/*Indica que a página de informações do processo seletivo será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INFORMACOES = false;

/*Indica que a página de consulta de resultados do processo seletivo será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_RESULTADOS = false;

/*Indica que a página de portal do candidato do processo seletivo será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_PORTAL_CANDIDATO = false;

/*Indica que a página de inscrições do processo seletivo será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INSCRICOES = false;

/*Indica que a página de login do processo seletivo será customizada */
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_LOGIN = false;

/*Indica que a página de informações do processo seletivo no contexto de ensino básico será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INFORMACOES_EB = false;

/*Indica que a página de consulta de resultados do processo seletivo será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_RESULTADOS_EB = false;

/*Indica que a página de portal do candidato do processo seletivo no contexto de ensino básico será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_PORTAL_CANDIDATO_EB = false;

/*Indica que a página de inscrições do processo seletivo no contexto de ensino básico será customizada*/
var EDUPS_CONST_GLOBAL_CUSTOM_VIEW_INSCRICOES_EB = false;