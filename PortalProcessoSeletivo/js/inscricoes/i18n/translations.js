[
    {
        "l-dados-candidato":{
            "pt": "DADOS DO CANDIDATO",
			"en": "APPLICANT'S DATA",
			"es": "DATOS DEL CANDIDATO"
        },
        "l-dados-pai":{
            "pt": "DADOS DO PAI",
			"en": "FATHER'S DATA",
			"es": "DATOS DEL PADRE"
        },
        "l-dados-mae":{
            "pt": "DADOS DA MÃE",
			"en": "MOTHER'S DATA",
			"es": "DATOS DE LA MADRE"
        },
        "l-dados-resp-acad":{
            "pt": "DADOS DO RESPONSÁVEL ACADÊMICO",
			"en": "DATA OF PERSON IN CHARGE OF ACADEMICS",
			"es": "DATOS DEL RESPONSABLE ACADÉMICO"
        },
        "l-dados-resp-fin":{
            "pt": "DADOS DO RESPONSÁVEL FINANCEIRO",
			"en": "DATA OF PERSON IN CHARGE OF FINANCIALS",
			"es": "DATOS DEL RESPONSABLE FINANCIERO"
        },
        "l-dados-basicos":{
            "pt": "Dados básicos",
			"en": "Basic data",
			"es": "Datos básicos"
        },
        "l-dados-curso":{
            "pt": "Dados do Curso",
			"en": "Course data",
			"es": "Datos del curso"
        },
        "l-responsaveis":{
            "pt": "Responsáveis",
			"en": "Responsible",
			"es": "Responsable"
        },
        "l-anterior":{
            "pt": "Anterior",
			"en": "Previous",
			"es": "Anterior"
        },
        "l-proximo":{
            "pt": "Próximo",
			"en": "Next",
			"es": "Siguiente"
        },
        "l-erro-avancar-etapa": {
            "pt": "Para avançar, é necessário completar todos os itens exigidos da etapa atual!",
            "en": "To continue, complete all items required of the current stage!",
            "es": "Para avanzar, es necesario completar todos los ítems exigidos de la etapa actual."
        },
        "l-nome":{
            "pt": "Nome",
			"en": "Name",
			"es": "Nombre"
        },
        "l-nome-candidato":{
            "pt": "Nome do candidato",
			"en": "Applicant's name",
			"es": "Nombre del candidato"
        },
        "l-data-nascimento":{
            "pt": "Data de nascimento",
			"en": "Birth date",
			"es": "Fecha de nacimiento"
        },
        "l-data-nascimento-candidato":{
            "pt": "Data de nascimento do candidato",
			"en": "Applicant's birth date",
			"es": "Fecha de nacimiento del candidato"
        },
        "l-data-nascimento-responsavel":{
            "pt": "Data de nascimento do responsável",
			"en": "Birth date of person in charge",
			"es": "Fecha de nacimiento del responsable"
        },
        "l-pais-natal":{
            "pt": "País natal",
			"en": "Country of birth",
			"es": "País natal"
        },
        "l-pais-natal-candidato":{
            "pt": "País natal do candidato",
			"en": "Applicant's country of birth",
			"es": "País natal del candidato"
        },
        "l-pais-natal-responsavel":{
            "pt": "País natal do responsável",
			"en": "Country of birth of person in charge",
			"es": "País natal del responsable"
        },
        "l-nacionalidade":{
            "pt": "Nacionalidade",
			"en": "Nationality",
			"es": "Nacionalidad"
        },
        "l-estado-natal":{
            "pt": "Estado natal",
			"en": "State of birth",
			"es": "Estado/Provincia/Región natal"
        },
        "l-estado-natal-candidato":{
            "pt": "Estado natal do candidato",
			"en": "Applicant's state of birth",
			"es": "Estado/Provincia/Región natal del candidato"
        },
        "l-estado-natal-responsavel":{
            "pt": "Estado natal do responsável",
			"en": "State of birth of person in charge",
			"es": "Estado/Provincia/Región natal del responsable"
        },
        "l-naturalidade":{
            "pt": "Naturalidade",
			"en": "Place of birth",
			"es": "Lugar de nacimiento"
        },
        "l-naturalidade-candidato":{
            "pt": "Naturalidade do candidato",
			"en": "Applicant's place of birth ",
			"es": "Lugar de nacimiento del candidato"
        },
        "l-naturalidade-responsavel":{
            "pt": "Naturalidade do responsável",
			"en": "Birth place of person in charge",
			"es": "Lugar de nacimiento del responsable"
        },
        "l-cpf":{
            "pt": "CPF",
			"en": "CPF",
			"es": "RCPF"
        },
        "l-documentos":{
            "pt": "Documentos",
			"en": "Documents",
			"es": "Documentos"
        },
        "l-documentos-cpf":{
            "pt": "CPF",
			"en": "CPF",
			"es": "RCPF"
        },
        "l-documentos-rg":{
            "pt": "Carteira de identidade",
			"en": "Identity card",
			"es": "Documento de identidad"
        },
        "l-numero":{
            "pt": "Número",
			"en": "Number",
			"es": "Número"
        },
        "l-orgao-emissor":{
            "pt": "Órgão emissor",
			"en": "Issuing body",
			"es": "Organismo emisor"
        },
        "l-data-emissao":{
            "pt": "Data emissão",
			"en": "Issue date",
			"es": "Fecha de emisión"
        },
        "l-pais-orgao-emissor":{
            "pt": "País emissor",
			"en": "Issuing country",
			"es": "País emisor"
        },
        "l-uf-orgao-emissor":{
            "pt": "Estado emissor",
			"en": "Issuing state",
			"es": "Estado/Provincia/Región emisor"
        },
        "l-documentos-registro-profissional":{
            "pt": "Registro profissional",
			"en": "Profesional record",
			"es": "Registro profesional"
        },
        "l-documentos-titulo-eleitor":{
            "pt": "Título de eleitor",
			"en": "Voter registration",
			"es": "Credencial de elector"
        },
        "l-zona":{
            "pt": "Zona",
			"en": "Zone",
			"es": "Zona"
        },
        "l-secao":{
            "pt": "Seção",
			"en": "Section",
			"es": "Sección"
        },
        "l-documentos-carteira-trabalho":{
            "pt": "Carteira de trabalho",
			"en": "Employee card",
			"es": "Libreta de trabajo"
        },
        "l-serie":{
            "pt": "Série",
			"en": "Series",
			"es": "Serie"
        },
        "l-tipo-nit":{
            "pt": "Carteira tipo NIT",
			"en": "Employee card NIT type",
			"es": "Documento tipo NIT"
        },
        "l-documentos-carteira-motorista":{
            "pt": "Carteira de motorista",
			"en": "Driver's license",
			"es": "Licencia de conducir"
        },
        "l-tipo":{
            "pt": "Tipo",
			"en": "Type",
			"es": "Tipo"
        },
        "l-data-vencimento":{
            "pt": "Data de vencimento",
			"en": "Expiration date",
			"es": "Fecha de vencimiento"
        },
        "l-documentos-certificado-reservista":{
            "pt": "Certificado de reservista",
			"en": "Military discharge certificate",
			"es": "Libreta militar de reservista"
        },
        "l-categoria-militar":{
            "pt": "Categoria militar",
			"en": "Military category",
			"es": "Categoría militar"
        },
        "l-circunscricao-militar":{
            "pt": "Circunscrição militar",
			"en": "Military circuit",
			"es": "Circunscripción militar"
        },
        "l-regiao-militar":{
            "pt": "Região militar",
			"en": "Military region",
			"es": "Región militar"
        },
        "l-situacao-militar":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-numpassaporte":{
            "pt": "Número do passaporte",
			"en": "Passport number",
			"es": "Número del pasaporte"
        },
        "l-documentos-passaporte":{
            "pt": "Passaporte",
			"en": "Passport",
			"es": "Pasaporte"
        },
        "l-data-validade":{
            "pt": "Data de validade",
			"en": "Validity date",
			"es": "Fecha de validez"
        },
        "l-pais-origem":{
            "pt": "País de origem",
			"en": "Origin country",
			"es": "País de origen"
        },
        "l-informacoes-complementares":{
            "pt": "INFORMAÇÕES COMPLEMENTARES",
			"en": "ADDITIONAL INFORMATION",
			"es": "INFORMACIÓN ADICIONAL"
        },
        "l-nome-social":{
            "pt": "Nome social",
			"en": "Social name",
			"es": "Nombre social"
        },
        "l-apelido":{
            "pt": "Apelido",
			"en": "Nickname",
			"es": "Alias"
        },
        "l-estado-civil":{
            "pt": "Estado civil",
			"en": "Marital status",
			"es": "Estado civil"
        },
        "l-sexo":{
            "pt": "Sexo",
			"en": "Gender",
			"es": "Sexo"
        },
        "l-email":{
            "pt": "E-mail",
			"en": "E-mail",
			"es": "E-mail"
        },
        "l-email-candidato":{
            "pt": "E-mail do candidato",
			"en": "Applicant's e-mail",
			"es": "E-mail del candidato"
        },
        "l-email-responsavel":{
            "pt": "E-mail do responsável",
			"en": "E-mail of person in charge",
			"es": "E-mail del responsable"
        },
        "l-canhoto":{
            "pt": "Canhoto?",
			"en": "Left-handed?",
			"es": "¿Zurdo?"
        },
        "l-fumante":{
            "pt": "Fumante?",
			"en": "Smoker?",
			"es": "¿Fumador?"
        },
        "l-informacoes-adicionais":{
            "pt": "Informações adicionais",
			"en": "Additional information",
			"es": "Información adicional"
        },
        "l-cor-raca":{
            "pt": "Cor/Raça",
			"en": "Ethnic origin",
			"es": "Color/Etnia"
        },
        "l-grau-instrucao":{
            "pt": "Grau de instrução",
			"en": "Educational level",
			"es": "Grado de instrucción"
        },
        "l-profissao":{
            "pt": "Profissão",
			"en": "Profession",
			"es": "Profesión"
        },
        "l-tipo-sanguineo":{
            "pt": "Tipo sangüíneo",
			"en": "Blood type",
			"es": "Tipo sanguíneo"
        },
        "l-endereco-contato":{
            "pt": "ENDEREÇO DE CONTATO",
			"en": "CONTACT ADDRESS",
			"es": "DIRECCIÓN DE CONTACTO"
        },
        "l-endereco":{
            "pt": "Endereço",
			"en": "Address",
			"es": "Dirección"
        },
        "l-cep":{
            "pt": "CEP",
			"en": "Postal Code",
			"es": "CP"
        },
        "l-tipo-rua":{
            "pt": "Tipo da rua",
			"en": "Type of street",
			"es": "Tipo de calle"
        },
        "l-rua":{
            "pt": "Rua",
			"en": "Street",
			"es": "Calle"
        },
        "l-complemento":{
            "pt": "Complemento",
			"en": "Address line 2",
			"es": "Complemento"
        },
        "l-tipo-bairro":{
            "pt": "Tipo do bairro",
			"en": "Type of district",
			"es": "Tipo de barrio"
        },
        "l-bairro":{
            "pt": "Bairro",
			"en": "District",
			"es": "Barrio"
        },
        "l-pais":{
            "pt": "País",
			"en": "Country",
			"es": "País"
        },
        "l-estado":{
            "pt": "Estado",
			"en": "State",
			"es": "Estado/Provincia/Región"
        },
        "l-cidade":{
            "pt": "Cidade",
			"en": "City",
			"es": "Ciudad"
        },
        "l-telefone-residencial":{
            "pt": "Telefone residencial",
			"en": "Home phone number",
			"es": "Teléfono residencial"
        },
        "l-telefone-celular":{
            "pt": "Telefone celular",
			"en": "Mobile phone number",
			"es": "Teléfono celular"
        },
        "l-telefone-comercial":{
            "pt": "Telefone comercial",
			"en": "Business phone number",
			"es": "Teléfono comercial"
        },
        "l-fax":{
            "pt": "Fax",
			"en": "Fax",
			"es": "Fax"
        },
        "l-opcao-interesse":{
            "pt": "OPÇÃO DE INTERESSE",
			"en": "OPTION OF INTEREST",
			"es": "OPCIÓN DE INTERÉS"
        },
        "l-area-pretendida":{
            "pt": "Área pretendida",
			"en": "Desired area",
			"es": "Área pretendida"
        },
        "l-processo-seletivo-area-ofertada":{
            "pt": "Processo seletivo e área ofertada",
			"en": "Selective process and offered area",
			"es": "Proceso de selección y área ofrecida"
        },
        "l-processo-seletivo":{
            "pt": "Processo seletivo",
			"en": "Selective process",
			"es": "Proceso de selección"
        },
        "l-area-ofertada-1-opcao":{
            "pt": "Área ofertada - 1ª opção de curso",
			"en": "Area offered - 1st option of course",
			"es": "Área ofrecida - 1ª opción de curso"
        },
        "l-area-ofertada":{
            "pt": "Área ofertada - ",
			"en": "Area offered - ",
			"es": "Área ofrecida - "
        },
        "l-opcao-curso":{
            "pt": "ª opção de curso",
			"en": "1st option of course",
			"es": "ª opción de curso"
        },
        "l-idioma-1-opcao":{
            "pt": "Idioma - 1ª opção para prova de lígua estrangeira",
			"en": "Language - 1st option for foreigner language test",
			"es": "Idioma - 1ª opción para prueba de idioma extranjero"
        },
        "l-idioma":{
            "pt": "Idioma - ",
			"en": "Language - ",
			"es": "Idioma - "
        },
        "l-opcao-idioma":{
            "pt": "ª opção para prova de lígua estrangeira",
			"en": "1st option for foreigner language test",
			"es": "ª opción para pruba de idioma extranjero"
        },
        "l-forma-inscricao":{
            "pt": "Forma de inscrição",
			"en": "Registration method",
			"es": "Forma de inscripción"
        },
        "l-local-realizacao":{
            "pt": "Local de realização",
			"en": "Local de realização",
			"es": "Local de realização"
        },
        "l-data-inscricao":{
            "pt": "Data da inscrição",
			"en": "Date of registration",
			"es": "Fecha de la inscripción"
        },
        "l-valor-inscricao":{
            "pt": "Valor da inscrição",
			"en": "Value of registration",
			"es": "Valor de la inscripción"
        },
        "l-candidato-treineiro":{
            "pt": "Após a divulgação dos resultados você NÃO estará com o 2º grau concluído? Ou seja, você é treineiro?",
			"en": "After the publication of results, you WILL NOT have completed secondary school, which means, are you a trainee?",
			"es": "¿Después de la divulgación de los resultados aún NO habrá concluido su secundaria? Es decir, ¿usted es practicante?"
        },
        "l-campus-local-realizacao":{
            "pt": "Campus",
			"en": "Campus",
			"es": "Campus"
        },
        "l-deficiencia": {
            "pt": "Deficiências",
			"en": "Disabilities",
			"es": "Discapacidades"
        },
        "l-deficiencia-auditiva":{
            "pt": "Auditiva: ",
			"en": "Hearing Impaired: ",
			"es": "Auditiva: "
        },
        "l-deficiencia-visual":{
            "pt": "Visual: ",
			"en": "Visual Impaired: ",
			"es": "Visual: "
        },
        "l-deficiencia-multipla":{
            "pt": "Múltipla",
			"en": "Multiple",
			"es": "Múltipla"
        },
        "l-deficiencia-fisica":{
            "pt": "Física: ",
			"en": "Physical disabilities: ",
			"es": "Física: "
		},
		"l-deficiencia-fala":{
            "pt": "Fala: ",
			"en": "Speech disorder: ",
			"es": "Habla: "
		},
		"l-deficiencia-mental":{
            "pt": "Mental: ",
			"en": "Mental: ",
			"es": "Mental: "
		},
		"l-deficiencia-intelectual":{
            "pt": "Intelectual: ",
			"en": "Intellectual: ",
			"es": "Intelectual: "
		},
		"l-deficiencia-reabilitado":{
            "pt": "Reabilitado(BR): ",
			"en": "Re-habited (BR):  ",
			"es": "Rehabilitado(BR): "
		},
        "l-motivo-outras-necessidades":{
            "pt": "Motivo outras necessidades",
			"en": "Reason other needs",
			"es": "Motivo otras necesidades"
        },
        "l-senha":{
            "pt": "SENHA",
			"en": "PASSWORD",
			"es": "CONTRASEÑA"
        },
        "l-informe-senha":{
            "pt": "Informe sua senha (Mínimo 6 caracteres)",
			"en": "Enter password (Minimum of 6 characters)",
			"es": "Informe su contraseña (Mínimo 6 caracteres)"
        },
        "l-senha-1":{
            "pt": "Digite a senha",
			"en": "Enter password",
			"es": "Digite la contraseña"
        },
        "l-senha-2":{
            "pt": "Digite a senha novamente",
			"en": "Enter password again",
			"es": "Digite nuevamente la contraseña"
        },
        "l-dados-gerais-estrangeiros":{
            "pt": "Dados gerais de estrangeiros",
			"en": "General data of foreigners",
			"es": "Datos generales de extranjeros"
        },
        "l-data-chegada-brasil":{
            "pt": "Data de chegada ao Brasil",
			"en": "Date of arrival in Brazil",
			"es": "Fecha de llegada a Brasil"
        },
        "l-tipo-visto":{
            "pt": "Tipo de visto",
			"en": "Type of visa",
			"es": "Tipo de visa"
        },
        "l-carteira-modelo-19":{
            "pt": "Carteira identidade modelo 19",
			"en": "Identification card model 19",
			"es": "Documento de identidad modelo 19"
        },
        "l-numero-filhos-brasileiros":{
            "pt": "Número de filhos brasileiros",
			"en": "Number of Brazilian children",
			"es": "Número de hijos brasileños"
        },
        "l-estrangeiro-naturalizado":{
            "pt": "É estrangeiro naturalizado",
			"en": "Denizen",
			"es": "Es extranjero naturalizado"
        },
        "l-conjuge-brasil":{
            "pt": "Possui cônjuge no Brasil",
			"en": "Has spouse in Brazil",
			"es": "Su cónyuge está en Brasil"
        },
        "l-registro-geral":{
            "pt": "Número registro geral",
			"en": "Number of general record",
			"es": "Número de registro general"
        },
        "l-decreto-imigracao":{
            "pt": "Decreto de imigração",
			"en": "Immigration decree",
			"es": "Decreto de inmigración"
        },
        "l-data-vencimento-ctps":{
            "pt": "Data vencimento CTPS",
			"en": "CTPS expiration date",
			"es": "Fecha de vencimiento de la libreta de trabajo"
        },
        "l-data-vencimento-rg":{
            "pt": "Data vencimento RG",
			"en": "ID expiration date",
			"es": "Fecha de vencimiento DI"
        },
        "l-sexo-feminino":{
            "pt": "Feminino",
			"en": "Female",
			"es": "Femenino"
        },
        "l-sexo-masculino": {
            "pt": "Masculino",
			"en": "Male",
			"es": "Masculino"
        },
        "l-msg-label-nome-candidato": {
            "pt": "Nome candidato",
			"en": "Applicant's name",
			"es": "Nombre candidato"
        },
        "l-msg-label-data-nascimento-candidato": {
            "pt": "Data nascimento candidato",
			"en": "Applicant's birth date",
			"es": "Fecha de nacimiento del candidato"
        },
        "l-msg-label-data-nascimento-responsavel": {
            "pt": "Data nascimento responsável",
			"en": "Birth date of person in charge",
			"es": "Fecha de nacimiento del responsable"
        },
        "l-msg-label-pais-natal-candidato": {
            "pt": "País natal candidato",
			"en": "Applicant's country of birth",
			"es": "País natal candidato"
        },
        "l-msg-label-estado-natal-candidato": {
            "pt": "Estado natal candidato",
			"en": "Applicant's state of birth",
			"es": "Estado/Provincia/Región natal candidato"
        },
        "l-msg-label-naturalidade-candidato": {
            "pt": "Naturalidade candidato",
			"en": "Applicant's place of birth",
			"es": "Lugar de nacimiento del candidato"
        },
        "l-msg-label-nome-social-candidato": {
            "pt": "Nome social candidato",
			"en": "Applicant's social name",
			"es": "Nombre social candidato"
        },
        "l-msg-label-apelido-candidato": {
            "pt": "Apelido candidato",
			"en": "Applicant's nickname",
			"es": "Alias candidato"
        },
        "l-msg-label-estado-civil-candidato": {
            "pt": "Estado civil candidato",
			"en": "Applicant's marital status",
			"es": "Estado civil candidato"
        },
        "l-msg-label-nacionalidade-candidato": {
            "pt": "Nacionalidade candidato",
			"en": "Applicant's place of birth",
			"es": "Nacionalidad candidato"
        },
        "l-msg-label-email-candidato": {
            "pt": "E-mail candidato",
			"en": "Applicant's e-mail",
			"es": "E-mail candidato"
        },
        "l-msg-label-sexo-candidato": {
            "pt": "Sexo candidato",
			"en": "Applicant's gender",
			"es": "Sexo candidato"
        },
        "l-msg-label-canhoto-candidato": {
            "pt": "Candidato canhoto",
			"en": "Left-handed applicant",
			"es": "Candidato zurdo"
        },
        "l-msg-label-apelido-fumante-candidato": {
            "pt": "Candidato fumante",
			"en": "Smoker applicant",
			"es": "Candidato fumador"
        },
        "l-msg-label-numero-cpf-candidato":{
            "pt": "Número do CPF candidato",
			"en": "Applicant's CPF number",
			"es": "Número del RCPF candidato"
        },
        "l-msg-label-numero-cpf-responsavel":{
            "pt": "Número do CPF responsável",
			"en": "CPF number of person in charge",
			"es": "Número del RCPF responsable"
        },
        "l-msg-label-numero-rg-candidato":{
            "pt": "Número do RG candidato",
			"en": "Applicant's ID number",
			"es": "Número del DI candidato"
        },
        "l-msg-label-data-emissao-rg-candidato":{
            "pt": "Data emissão RG candidato",
			"en": "Applicant's ID issue date",
			"es": "Fecha de emisión DI candidato"
        },
        "l-msg-label-orgao-emissor-rg-candidato":{
            "pt": "Órgão emissor RG candidato",
			"en": "Applicant's ID issuing body",
			"es": "Organismo emisor DI candidato"
        },
        "l-msg-label-pais-orgao-emissor-rg-candidato":{
            "pt": "País órgão emissor RG candidato",
			"en": "Applicant's ID issuing body country",
			"es": "País organismo emisor DI candidato"
        },
        "l-msg-label-uf-orgao-emissor-rg-candidato":{
            "pt": "UF órgão emissor RG candidato",
			"en": "Applicant's ID issuing body state",
			"es": "Est/Prov/Reg organismo emisor DI candidato"
        },
        "l-msg-label-numero-registro-profossional-candidato":{
            "pt": "Número registro profissional candidato",
			"en": "Applicant's professional record number",
			"es": "Número registro profesional candidato"
        },
        "l-msg-label-numero-titulo-eleitor-candidato":{
            "pt": "Número do título de eleitor candidato",
			"en": "Applicant's voter registration card",
			"es": "Número de credencial de elector candidato"
        },
        "l-msg-label-zona-titulo-eleitor-candidato":{
            "pt": "Zona título eleitor candidato",
			"en": "Applicant's voter registration zone",
			"es": "Zona credencial elector candidato"
        },
        "l-msg-label-secao-titulo-eleitor-candidato":{
            "pt": "Seção título eleitor candidato",
			"en": "Applicant's voter registration section",
			"es": "Sección credencial elector candidato"
        },
        "l-msg-label-data-emissao-titulo-eleitor-candidato":{
            "pt": "Data emissão título eleitor candidato",
			"en": "Applicant's voter registration issue date",
			"es": "Fecha de emisión credencial elector candidato"
        },
        "l-msg-label-pais-orgao-emissor-titulo-eleitor-candidato":{
            "pt": "País órgão emissor título eleitor candidato",
			"en": "Applicant's voter registration issuing body country",
			"es": "País organismo emisor credencial elector candidato"
        },
        "l-msg-label-uf-orgao-emissor-titulo-eleitor-candidato":{
            "pt": "Uf órgão emissor título eleitor candidato",
			"en": "Applicant's voter registration issuing body state",
			"es": "Est/Prov/Reg organismo emisor credencial elector candidato"
        },
        "l-msg-label-numero-carteira-trabalho-candidato":{
            "pt": "Número carteira trabalho candidato",
			"en": "Applicant's employee card number",
			"es": "Número libreta de trabajo candidato"
        },
        "l-msg-label-serie-carteira-trabalho-candidato":{
            "pt": "Série carteira trabalho candidato",
			"en": "Applicant's employee card series",
			"es": "Serie libreta de trabajo candidato"
        },
        "l-msg-label-data-emissao-carteira-trabalho-candidato":{
            "pt": "Data emissão carteira trabalho candidato",
			"en": "Applicant's employee card issue date",
			"es": "Fecha de emisión libreta de trabajo candidato"
        },
        "l-msg-label-pais-orgao-emissor-carteira-trabalho-candidato":{
            "pt": "País órgão emissor carteira trabalho candidato",
			"en": "Applicant's employee card issuing body country",
			"es": "País organismo emisor libreta de trabajo candidato"
        },
        "l-msg-label-uf-orgao-emissor-carteira-trabalho-candidato":{
            "pt": "UF órgão emissor carteira trabalho candidato",
			"en": "Applicant's employee card issuing body state",
			"es": "Est/Prov/Reg organismo emisor libreta de trabajo candidato"
        },
        "l-msg-label-carteira-trabalho-tipo-nit-candidato":{
            "pt": "Carteira trabalho tipo NIT candidato",
			"en": "Applicant's employee card NIT type",
			"es": "Libreta de trabajo tipo NIT candidato"
        },
        "l-msg-label-carteira-motorista-candidato":{
            "pt": "Carteira motorista candidato",
			"en": "Applicant's driver's license",
			"es": "Licencia de conducir candidato"
        },
        "l-msg-label-tipo-carteira-motorista-candidato":{
            "pt": "Tipo carteira motorista candidato",
			"en": "Applicant's driver's license type",
			"es": "Tipo licencia de conducir candidato"
        },
        "l-msg-label-data-vencimento-carteira-motorista-candidato":{
            "pt": "Data vencimento carteira motorista candidato",
			"en": "Applicant's driver's license expiration date",
			"es": "Fecha de vencimiento licencia de conducir candidato"
        },
        "l-msg-label-numero-cert-reservista-candidato":{
            "pt": "Número certificado reservista candidato",
			"en": "Applicant's military discharge certificate number",
			"es": "Número libreta militar de reservista candidato"
        },
        "l-msg-label-cetegoria-militar-candidato":{
            "pt": "Categoria militar candidato",
			"en": "Applicant's military category",
			"es": "Categoría militar candidato"
        },
        "l-msg-label-circunscricao-militar-candidato":{
            "pt": "Circunscrição militar candidato",
			"en": "Applicant's military circuit",
			"es": "Circunscripción militar candidato"
        },
        "l-msg-label-regiao-militar-candidato":{
            "pt": "Região militar candidato",
			"en": "Applicant's military region",
			"es": "Región militar candidato"
        },
        "l-msg-label-data-emissao-cert-reservista-candidato":{
            "pt": "Data emissão certificado reservista candidato",
			"en": "Issue date of applicant's military discharge certificate",
			"es": "Fecha de emisión libreta militar de reservista candidato"
        },
        "l-msg-label-orgao-emissor-cert-reservista-candidato":{
            "pt": "Órgão emissor certificado reservista candidato",
			"en": "Issuing body of applicant's military discharge certificate",
			"es": "Organismo emisor libreta militar de reservista candidato"
        },
        "l-msg-label-situacao-militar-candidato":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-msg-label-cep-candidato":{
            "pt": "CEP candidato",
			"en": "Applicant's postal code",
			"es": "CP candidato"
        },
        "l-msg-label-tipo-rua-candidato":{
            "pt": "Tipo rua candidato",
			"en": "Applicant's type of street",
			"es": "Tipo calle candidato"
        },
        "l-msg-label-rua-candidato":{
            "pt": "Rua candidato",
			"en": "Applicant's street",
			"es": "Calle candidato"
        },
        "l-msg-label-numero-candidato":{
            "pt": "Número da residência do candidato",
			"en": "Applicant's number",
			"es": "Número candidato"
        },
        "l-msg-label-complemento-candidato":{
            "pt": "Complemento candidato",
			"en": "Applicant's address line 2",
			"es": "Complemento candidato"
        },
        "l-msg-label-tipo-bairro-candidato":{
            "pt": "Tipo bairro candidato",
			"en": "Applicant's type of district",
			"es": "Tipo barrio candidato"
        },
        "l-msg-label-bairro-candidato":{
            "pt": "Bairro candidato",
			"en": "Applicant's district",
			"es": "Barrio candidato"
        },
        "l-msg-label-pais-candidato":{
            "pt": "País candidato",
			"en": "Applicant's country",
			"es": "País candidato"
        },
        "l-msg-label-uf-candidato":{
            "pt": "UF candidato",
			"en": "Applicant's state",
			"es": "Est/Prov/Reg candidato"
        },
        "l-msg-label-telefone-residencial-candidato":{
            "pt": "Telefone residencial candidato",
			"en": "Applicant's home phone number",
			"es": "Teléfono residencial candidato"
        },
        "l-msg-label-telefone-calular-candidato":{
            "pt": "Telefone celular candidato",
			"en": "Applicant's mobile phone number",
			"es": "Teléfono celular candidato"
        },
        "l-msg-label-telefone-comercial-candidato":{
            "pt": "Telefone comercial candidato",
			"en": "Applicant's business phone number",
			"es": "Teléfono celular candidato"
        },
        "l-msg-label-fax-candidato":{
            "pt": "Fax candidato",
			"en": "Applicant's fax",
			"es": "Fax candidato"
         },
         "l-msg-label-cor-raca-candidato":{
             "pt": "Cor/raça candidato",
			 "en": "Applicant's ethnic origin",
		 	 "es": "Color/etnia candidato"
         },
         "l-msg-label-pofissao-candidato":{
             "pt": "Profissão candidato",
			 "en": "Applicant's profession",
			 "es": "Profesión candidato"
         },
         "l-msg-label-tipo-sanguineo-candidato":{
             "pt": "Tipo sanguíneo candidato",
			 "en": "Applicant's blood type",
			 "es": "Tipo sanguíneo candidato"
         },
         "l-msg-label-numero-passaporte-candidato":{
             "pt": "Número passaporte candidato",
			 "en": "Applicant's passport number",
			 "es": "Tipo sanguíneo candidato"
         },
         "l-msg-label-numero-passaporte-responsavel":{
            "pt": "Número passaporte responsável",
			"en": "Passport number of person in charge",
			"es": "Número pasaporte responsable"
        },
         "l-msg-label-data-emissao-passaporte-candidato":{
             "pt": "Data emissão passaporte candidato",
			 "en": "Applicant's passport issue date",
			 "es": "Fecha de emisión pasaporte candidato"
         },
         "l-msg-label-data-validade-passaporte-candidato":{
             "pt": "Data validade passaporte candidato",
			 "en": "Applicant's passport validity date",
			 "es": "Fecha de validez pasaporte candidato"
         },
         "l-msg-label-pais-origem-passaporte-candidato":{
            "pt": "País origem passaporte candidato",
			"en": "Applicant's passport origin country",
			"es": "País origen pasaporte candidato"
         },
         "l-msg-label-data-chegada-brasil-candidato":{
             "pt": "Data chegada Brasil candidato",
			 "en": "Applicant's date of arrival in Brazil",
			 "es": "Fecha de llegada Brasil candidato"
         },
         "l-msg-label-tipo-visto-candidato":{
             "pt": "Tipo visto candidato",
			 "en": "Applicant's type of visa",
			 "es": "Tipo visa candidato"
         },
         "l-msg-label-carteira-modelo-19-candidato":{
             "pt": "Carteira modelo 19 candidato",
			 "en": "Applicant's identification card model 19",
			 "es": "Documento de identidad modelo 19 candidato"
         },
         "l-msg-label-numero-filhos-brasileiros-candidato":{
             "pt": "Número filhos brasileiros candidato",
			 "en": "Applicant's number of Brazilian children",
			 "es": "Número hijos brasileños candidato"
         },
         "l-msg-label-estrangeiro-naturalizado-candidato":{
             "pt": "Estrangeiro naturalizado candidato",
			 "en": "Foreigner naturalized applicant",
			 "es": "Extranjero naturalizado candidato"
         },
         "l-msg-label-conjuge-brasil-candidato":{
             "pt": "Cônjuge Brasil candidato",
			 "en": "Spouse Brazil applicant",
			 "es": "Cónyuge Brasil candidato"
         },
        "l-msg-label-registro-geral-candidato":{
             "pt": "Registro geral candidato",
			 "en": "Applicant's general record",
			 "es": "Registro general candidato"
         },
         "l-msg-label-registro-geral-responsavel":{
             "pt": "Registro geral responsável",
			 "en": "General record person in charge",
			 "es": "Registro general responsable"
         },
         "l-msg-label-decreto-imigracao-candidato":{
             "pt": "Decreto imigração candidato",
			 "en": "Immigration decree applicant",
			 "es": "Decreto inmigración candidato"
         },
         "l-msg-label-data-vencimento-ctps-candidato":{
             "pt": "Data vencimento carteira trabalho estrangeiro candidato",
			 "en": "Expiration date employee card foreigner applicant",
			 "es": "Fecha de vencimiento libreta de trabajo extranjero candidato"
         },
         "l-msg-label-data-vencimento-rg-candidato":{
             "pt": "Data vencimento registro geral estrangeiros candidato",
			 "en": "Expiration date general record foreigner applicants",
			 "es": "Fecha de vencimiento registro general extranjeros candidato"
         },
        "l-msg-label-nome-pai": {
            "pt": "Nome pai",
			"en": "Father's name",
			"es": "Nombre padre"
        },
        "l-msg-label-data-nascimento-pai": {
            "pt": "Data nascimento pai",
			"en": "Father's birth date",
			"es": "Fecha de nacimiento padre"
        },
        "l-msg-label-pais-natal-pai": {
            "pt": "País natal pai",
			"en": "Father's country of birth",
			"es": "País natal padre"
        },
        "l-msg-label-estado-natal-pai": {
            "pt": "Estado natal pai",
			"en": "Father's state of birth",
			"es": "Estado/Provincia/Región natal padre"
        },
        "l-msg-label-naturalidade-pai": {
            "pt": "Naturalidade pai",
			"en": "Father's place of birth",
			"es": "Lugar de nacimiento padre"
        },
        "l-msg-label-nome-social-pai": {
            "pt": "Nome social pai",
			"en": "Father's social name",
			"es": "Nombre social padre"
        },
        "l-msg-label-apelido-pai": {
            "pt": "Apelido pai",
			"en": "Father's nickname",
			"es": "Alias padre"
        },
        "l-msg-label-estado-civil-pai": {
            "pt": "Estado civil pai",
			"en": "Father's marital status",
			"es": "Estado civil padre"
        },
        "l-msg-label-nacionalidade-pai": {
            "pt": "Nacionalidade pai",
			"en": "Father's nationality",
			"es": "Nacionalidad padre"
        },
        "l-msg-label-email-pai": {
            "pt": "E-mail pai",
			"en": "Father's e-mail",
			"es": "E-mail padre"
        },
        "l-msg-label-sexo-pai": {
            "pt": "Sexo pai",
			"en": "Father's gender",
			"es": "Sexo padre"
        },
        "l-msg-label-canhoto-pai": {
            "pt": "Candidato canhoto",
			"en": "Left-handed applicant",
			"es": "Candidato zurdo"
        },
        "l-msg-label-apelido-fumante-pai": {
            "pt": "Candidato fumante",
			"en": "Smoker applicant",
			"es": "Candidato fumador"
        },
        "l-msg-label-numero-cpf-pai":{
            "pt": "Número do CPF pai",
			"en": "Father's CPF number",
			"es": "Número del RCPF padre"
        },
        "l-msg-label-numero-rg-pai":{
            "pt": "Número do RG pai",
			"en": "Father's ID number",
			"es": "Número del DI padre"
        },
        "l-msg-label-data-emissao-rg-pai":{
            "pt": "Data emissão RG pai",
			"en": "Father's ID issue date",
			"es": "Fecha de emisión DI padre"
        },
        "l-msg-label-orgao-emissor-rg-pai":{
            "pt": "Órgão emissor RG pai",
			"en": "Father's ID issuing body",
			"es": "Organismo emisor DI padre"
        },
        "l-msg-label-pais-orgao-emissor-rg-pai":{
            "pt": "País órgão emissor RG pai",
			"en": "Country of father's ID issuing body",
			"es": "País organismo emisor DI padre"
        },
        "l-msg-label-uf-orgao-emissor-rg-pai":{
            "pt": "UF órgão emissor RG pai",
			"en": "State of father's ID issuing body",
			"es": "Est/Prov/Reg organismo emisor DI padre"
        },
        "l-msg-label-numero-registro-profossional-pai":{
            "pt": "Número registro profissional pai",
			"en": "Father's professional record number",
			"es": "Número registro profesional padre"
        },
        "l-msg-label-numero-titulo-eleitor-pai":{
            "pt": "Número do título de eleitor pai",
			"en": "Father's voter registration number",
			"es": "Número de credencial elector padre"
        },
        "l-msg-label-zona-titulo-eleitor-pai":{
            "pt": "Zona título eleitor pai",
			"en": "Father's voter registration zone",
			"es": "Zona credencial elector padre"
        },
        "l-msg-label-secao-titulo-eleitor-pai":{
            "pt": "Seção título eleitor candidato",
			"en": "Applicant voter registration section",
			"es": "Sección credencial elector candidato"
        },
        "l-msg-label-data-emissao-titulo-eleitor-pai":{
            "pt": "Data emissão título eleitor pai",
			"en": "Father's voter registration issue date",
			"es": "Fecha de emisión credencial elector padre"
        },
        "l-msg-label-pais-orgao-emissor-titulo-eleitor-pai":{
            "pt": "País órgão emissor título eleitor pai",
			"en": "Country of father's voter registration issuing body",
			"es": "País organismo emisor credencial elector padre"
        },
        "l-msg-label-uf-orgao-emissor-titulo-eleitor-pai":{
            "pt": "Uf órgão emissor título eleitor pai",
			"en": "State of father's voter registration issuing body",
			"es": "Est/Prov/Reg organismo emisor credencial elector padre"
        },
        "l-msg-label-numero-carteira-trabalho-pai":{
            "pt": "Número carteira trabalho pai",
			"en": "Father's employee card number",
			"es": "Número libreta de trabajo padre"
        },
        "l-msg-label-serie-carteira-trabalho-pai":{
            "pt": "Série carteira trabalho pai",
			"en": "Father's employee card series",
			"es": "Serie libreta de trabajo padre"
        },
        "l-msg-label-data-emissao-carteira-trabalho-pai":{
            "pt": "Data emissão carteira trabalho pai",
			"en": "Father's employee card issue date",
			"es": "Fecha de emisión libreta de trabajo padre"
        },
        "l-msg-label-pais-orgao-emissor-carteira-trabalho-pai":{
            "pt": "País órgão emissor carteira trabalho pai",
			"en": "Country of father's employee card issuing body",
			"es": "País organismo emisor libreta de trabajo padre"
        },
        "l-msg-label-uf-orgao-emissor-carteira-trabalho-pai":{
            "pt": "UF órgão emissor carteira trabalho pai",
			"en": "State of father's employee card issuing body",
			"es": "Est/Prov/Reg organismo emisor libreta de trabajo padre"
        },
        "l-msg-label-carteira-trabalho-tipo-nit-pai":{
            "pt": "Carteira trabalho tipo NIT pai",
			"en": "Father's employee cart NIT type",
			"es": "Libreta de trabajo tipo NIT padre"
        },
        "l-msg-label-carteira-motorista-pai":{
            "pt": "Carteira motorista pai",
			"en": "Father's driver's license",
			"es": "Licencia de conducir padre"
        },
        "l-msg-label-tipo-carteira-motorista-pai":{
            "pt": "Tipo carteira motorista pai",
			"en": "Father's driver's license type",
			"es": "Tipo licencia de conducir padre"
        },
        "l-msg-label-data-vencimento-carteira-motorista-pai":{
            "pt": "Data vencimento carteira motorista pai",
			"en": "Expiration date of father's driver's license",
			"es": "Fecha de vencimiento licencia de conducir padre"
        },
        "l-msg-label-numero-cert-reservista-pai":{
            "pt": "Número certificado reservista pai",
			"en": "Father's military discharge certificate number",
			"es": "Número libreta militar de reservista padre"
        },
        "l-msg-label-cetegoria-militar-pai":{
            "pt": "Categoria militar pai",
			"en": "Father's military category",
			"es": "Categoría militar padre"
        },
        "l-msg-label-circunscricao-militar-pai":{
            "pt": "Circunscrição militar pai",
			"en": "Father's military circuit",
			"es": "Circunscripción militar padre"
        },
        "l-msg-label-regiao-militar-pai":{
            "pt": "Região militar pai",
			"en": "Father's military region",
			"es": "Región militar padre"
        },
        "l-msg-label-data-emissao-cert-reservista-pai":{
            "pt": "Data emissão certificado reservista pai",
			"en": "Issue date of father's military discharge certificate",
			"es": "Fecha de emisión libreta militar de reservista padre"
        },
        "l-msg-label-orgao-emissor-cert-reservista-pai":{
            "pt": "Órgão emissor certificado reservista pai",
			"en": "Issuing body of father's military discharge certificate",
			"es": "Organismo emisor libreta militar de reservista padre"
        },
        "l-msg-label-situacao-militar-pai":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-msg-label-cep-pai":{
            "pt": "CEP pai",
			"en": "Father's postal code",
			"es": "CP padre"
        },
        "l-msg-label-tipo-rua-pai":{
            "pt": "Tipo rua pai",
			"en": "Father's type of street",
			"es": "Tipo calle padre"
        },
        "l-msg-label-rua-pai":{
            "pt": "Rua pai",
			"en": "Father's street",
			"es": "Calle padre"
        },
        "l-msg-label-numero-pai":{
            "pt": "Número pai",
			"en": "Father's number",
			"es": "Número padre"
        },
        "l-msg-label-complemento-pai":{
            "pt": "Complemento pai",
			"en": "Father's address line 2",
			"es": "Complemento padre"
        },
        "l-msg-label-tipo-bairro-pai":{
            "pt": "Tipo bairro pai",
			"en": "Father's type of district",
			"es": "Tipo barrio padre"
        },
        "l-msg-label-bairro-pai":{
            "pt": "Bairro pai",
			"en": "Father's district",
			"es": "Barrio padre"
        },
        "l-msg-label-pais-pai":{
            "pt": "País pai",
			"en": "Father's country",
			"es": "País padre"
        },
        "l-msg-label-uf-pai":{
            "pt": "UF pai",
			"en": "Father's state",
			"es": "Est/Prov/Reg padre"
        },
        "l-msg-label-telefone-residencial-pai":{
            "pt": "Telefone residencial pai",
			"en": "Father's home phone number",
			"es": "Teléfono residencial padre"
        },
        "l-msg-label-telefone-calular-pai":{
            "pt": "Telefone celular pai",
			"en": "Father's mobile phone number",
			"es": "Teléfono celular padre"
        },
        "l-msg-label-telefone-comercial-pai":{
            "pt": "Telefone comercial pai",
			"en": "Father's business phone number",
			"es": "Teléfono comercial padre"
        },
        "l-msg-label-fax-pai":{
            "pt": "Fax pai",
			"en": "Father's fax number",
			"es": "Fax padre"
         },
         "l-msg-label-cor-raca-pai":{
             "pt": "Cor/raça pai",
			 "en": "Father's ethnic group",
			 "es": "Color/etnia padre"
         },
         "l-msg-label-pofissao-pai":{
             "pt": "Profissão pai",
			 "en": "Father's profession",
			 "es": "Profesión padre"
         },
         "l-msg-label-tipo-sanguineo-pai":{
             "pt": "Tipo sanguíneo pai",
			 "en": "Father's blood type",
			 "es": "Tipo sanguíneo padre"
         },
         "l-msg-label-numero-passaporte-pai":{
             "pt": "Número passaporte pai",
			 "en": "Father's passport number",
			 "es": "Número pasaporte padre"
         },
         "l-msg-label-data-emissao-passaporte-pai":{
             "pt": "Data emissão passaporte pai",
			 "en": "Father's passport issue date",
			 "es": "Fecha emisión pasaporte padre"
         },
         "l-msg-label-data-validade-passaporte-pai":{
             "pt": "Data validade passaporte pai",
			 "en": "Father's passport validity date",
			 "es": "Fecha validez pasaporte padre"
         },
         "l-msg-label-pais-origem-passaporte-pai":{
            "pt": "País origem passaporte pai",
			"en": "Father's passport origin country",
			"es": "País origen pasaporte padre"
         },
         "l-msg-label-data-chegada-brasil-pai":{
             "pt": "Data chegada Brasil pai",
			 "en": "Father's date of arrival to Brazil",
			 "es": "Fecha de llegada a Brasil padre"
         },
         "l-msg-label-tipo-visto-pai":{
             "pt": "Tipo visto pai",
			 "en": "Father's type of visa",
			 "es": "Tipo visa padre"
         },
         "l-msg-label-carteira-modelo-19-pai":{
             "pt": "Carteira modelo 19 pai",
			 "en": "Father's employee card model 19",
			 "es": "Documento de identidad modelo 19 padre"
         },
         "l-msg-label-numero-filhos-brasileiros-pai":{
             "pt": "Número filhos brasileiros pai",
			 "en": "Father's number of Brazilian children",
			 "es": "Número hijos brasileños padre"
         },
         "l-msg-label-estrangeiro-naturalizado-pai":{
             "pt": "Estrangeiro naturalizado pai",
			 "en": "Father foreigner naturalized",
			 "es": "Extranjero naturalizado padre"
         },
         "l-msg-label-conjuge-brasil-pai":{
             "pt": "Cônjuge Brasil pai",
			 "en": "Father's Brazil spouse   ",
			 "es": "Cónyuge Brasil padre"
         },
         "l-msg-label-registro-geral-pai":{
             "pt": "Registro geral pai",
			 "en": "Father's general record",
			 "es": "Registro general padre"
         },
         "l-msg-label-decreto-imigracao-pai":{
             "pt": "Decreto imigração pai",
			 "en": "Father's immigration decree",
			 "es": "Decreto inmigración padre"
         },
         "l-msg-label-data-vencimento-ctps-pai":{
             "pt": "Data vencimento carteira trabalho estrangeiro pai",
			 "en": "Expiration date of father's foreigner employee card",
			 "es": "Fecha de vencimiento libreta de trabajo extranjero padre"
         },
         "l-msg-label-data-vencimento-rg-pai":{
             "pt": "Data vencimento registro geral estrangeiros pai",
			 "en": "Expiration date of father's foreigner general record",
			 "es": "Fecha de vencimiento registro general extranjeros padre"
         },
        "l-msg-label-nome-mae": {
            "pt": "Nome mãe",
			"en": "Mother's name",
			"es": "Nombre madre"
        },
        "l-msg-label-data-nascimento-mae": {
            "pt": "Data nascimento mãe",
			"en": "Mother's birth date",
			"es": "Fecha de nacimiento madre"
        },
        "l-msg-label-pais-natal-mae": {
            "pt": "País natal mãe",
			"en": "Mother's country of birth",
			"es": "País natal madre"
        },
        "l-msg-label-estado-natal-mae": {
            "pt": "Estado natal mãe",
			"en": "Mother's state of birth",
			"es": "Estado/Provincia/Región natal madre"
        },
        "l-msg-label-naturalidade-mae": {
            "pt": "Naturalidade mãe",
			"en": "Mother's place of birth",
			"es": "Lugar de nacimiento madre"
        },
        "l-msg-label-nome-social-mae": {
            "pt": "Nome social mãe",
			"en": "Mother's social name",
			"es": "Nombre social madre"
        },
        "l-msg-label-apelido-mae": {
            "pt": "Apelido mãe",
			"en": "Mother's nickname",
			"es": "Alias madre"
        },
        "l-msg-label-estado-civil-mae": {
            "pt": "Estado civil mãe",
			"en": "Mother's marital status",
			"es": "Estado civil madre"
        },
        "l-msg-label-nacionalidade-mae": {
            "pt": "Nacionalidade mãe",
			"en": "Mother's nationality",
			"es": "Nacionalidad madre"
        },
        "l-msg-label-email-mae": {
            "pt": "E-mail mãe",
			"en": "Mother's e-mail",
			"es": "E-mail madre"
        },
        "l-msg-label-sexo-mae": {
            "pt": "Sexo mãe",
			"en": "Mother's gender",
			"es": "Sexo madre"
        },
        "l-msg-label-canhoto-mae": {
            "pt": "Candidato canhoto",
			"en": "Left-handed applicant",
			"es": "Candidato zurdo"
        },
        "l-msg-label-apelido-fumante-mae": {
            "pt": "Candidato fumante",
			"en": "Smoker applicant",
			"es": "Candidato fumador"
        },
        "l-msg-label-numero-cpf-mae":{
            "pt": "Número do CPF mãe",
			"en": "Mother's CPF number",
			"es": "Número de RCPF madre"
        },
        "l-msg-label-numero-rg-mae":{
            "pt": "Número do RG mãe",
			"en": "Mother's ID number",
			"es": "Número de DI madre"
        },
        "l-msg-label-data-emissao-rg-mae":{
            "pt": "Data emissão RG mãe",
			"en": "Mother's ID issue date",
			"es": "Fecha de emisión DI madre"
        },
        "l-msg-label-orgao-emissor-rg-mae":{
            "pt": "Órgão emissor RG mãe",
			"en": "Mother's ID issuing body",
			"es": "Organismo emisor DI madre"
        },
        "l-msg-label-pais-orgao-emissor-rg-mae":{
            "pt": "País órgão emissor RG mãe",
			"en": "Country of mother's ID issuing body",
			"es": "País organismo emisor DI madre"
        },
        "l-msg-label-uf-orgao-emissor-rg-mae":{
            "pt": "UF órgão emissor RG mãe",
			"en": "State of mother's ID issuing body",
			"es": "Est/Prov/Reg organismo emisor DI madre"
        },
        "l-msg-label-numero-registro-profossional-mae":{
            "pt": "Número registro profissional mãe",
			"en": "Mother's professional record number",
			"es": "Número registro profesional madre"
        },
        "l-msg-label-numero-titulo-eleitor-mae":{
            "pt": "Número do título de eleitor mãe",
			"en": "Mother's voter registration number",
			"es": "Número de credencial elector madre"
        },
        "l-msg-label-zona-titulo-eleitor-mae":{
            "pt": "Zona título eleitor mãe",
			"en": "Mother's voter registration zone",
			"es": "Zona credencial elector madre"
        },
        "l-msg-label-secao-titulo-eleitor-mae":{
            "pt": "Seção título eleitor candidato",
			"en": "Applicant's voter registration section",
			"es": "Sección credencial elector candidato"
        },
        "l-msg-label-data-emissao-titulo-eleitor-mae":{
            "pt": "Data emissão título eleitor mãe",
			"en": "Issue date of mother's voter registration",
			"es": "Fecha de emisión credencial elector madre"
        },
        "l-msg-label-pais-orgao-emissor-titulo-eleitor-mae":{
            "pt": "País órgão emissor título eleitor mãe",
			"en": "Country of mother's voter registration issuing body",
			"es": "País organismo emisor credencial elector madre"
        },
        "l-msg-label-uf-orgao-emissor-titulo-eleitor-mae":{
            "pt": "Uf órgão emissor título eleitor mãe",
			"en": "State of mother's voter registration issuing body",
			"es": "Est/Prov/Reg organismo emisor credencial elector madre"
        },
        "l-msg-label-numero-carteira-trabalho-mae":{
            "pt": "Número carteira trabalho mãe",
			"en": "Mother's employee card number",
			"es": "Número libreta de trabajo madre"
        },
        "l-msg-label-serie-carteira-trabalho-mae":{
            "pt": "Série carteira trabalho mãe",
			"en": "Mother's employee card series",
			"es": "Serie libreta de trabajo madre"
        },
        "l-msg-label-data-emissao-carteira-trabalho-mae":{
            "pt": "Data emissão carteira trabalho mãe",
			"en": "Issue date of mother's employee card",
			"es": "Fecha de emisión libreta de trabajo madre"
        },
        "l-msg-label-pais-orgao-emissor-carteira-trabalho-mae":{
            "pt": "País órgão emissor carteira trabalho mãe",
			"en": "Country of mother's employee card issuing body",
			"es": "País organismo emisor libreta de trabajo madre"
        },
        "l-msg-label-uf-orgao-emissor-carteira-trabalho-mae":{
            "pt": "UF órgão emissor carteira trabalho mãe",
			"en": "State of mother's employee card issuing body",
			"es": "Est/Prov/Reg organismo emisor libreta de trabajo madre"
        },
        "l-msg-label-carteira-trabalho-tipo-nit-mae":{
            "pt": "Carteira trabalho tipo NIT mãe",
			"en": "Mother's employee card NIT type",
			"es": "Libreta de trabajo tipo NIT madre"
        },
        "l-msg-label-carteira-motorista-mae":{
            "pt": "Carteira motorista mãe",
			"en": "Mother's driver's license",
			"es": "Licencia de conducir madre"
        },
        "l-msg-label-tipo-carteira-motorista-mae":{
            "pt": "Tipo carteira motorista mãe",
			"en": "Mother's driver's license type",
			"es": "Tipo licencia de conducir madre"
        },
        "l-msg-label-data-vencimento-carteira-motorista-mae":{
            "pt": "Data vencimento carteira motorista mãe",
			"en": "Expiration date of mother's driver's license",
			"es": "Fecha de vencimiento licencia de conducir madre"
        },
        "l-msg-label-numero-cert-reservista-mae":{
            "pt": "Número certificado reservista mãe",
			"en": "Mother's military discharge certificate number",
			"es": "Número libreta militar de reservista madre"
        },
        "l-msg-label-cetegoria-militar-mae":{
            "pt": "Categoria militar mãe",
			"en": "Mother's military category",
			"es": "Categoría militar madre"
        },
        "l-msg-label-circunscricao-militar-mae":{
            "pt": "Circunscrição militar mãe",
			"en": "Mother's military circuit",
			"es": "Circunscripción militar madre"
        },
        "l-msg-label-regiao-militar-mae":{
            "pt": "Região militar mãe",
			"en": "Mother's military region",
			"es": "Región militar madre"
        },
        "l-msg-label-data-emissao-cert-reservista-mae":{
            "pt": "Data emissão certificado reservista mãe",
			"en": "Issue date of mother's military discharge certificate",
			"es": "Fecha de emisión libreta militar de reservista madre"
        },
        "l-msg-label-orgao-emissor-cert-reservista-mae":{
            "pt": "Órgão emissor certificado reservista mãe",
			"en": "Issuing body of mother's military discharge certificate",
			"es": "Organismo emisor libreta militar de reservista madre"
        },
        "l-msg-label-situacao-militar-mae":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-msg-label-cep-mae":{
            "pt": "CEP mãe",
			"en": "Mother's postal code",
			"es": "CP madre"
        },
        "l-msg-label-tipo-rua-mae":{
            "pt": "Tipo rua mãe",
			"en": "Mother's type of street",
			"es": "Tipo calle madre"
        },
        "l-msg-label-rua-mae":{
            "pt": "Rua mãe",
			"en": "Mother's street",
			"es": "Calle madre"
        },
        "l-msg-label-numero-mae":{
            "pt": "Número mãe",
			"en": "Mother's number",
			"es": "Número madre"
        },
        "l-msg-label-complemento-mae":{
            "pt": "Complemento mãe",
			"en": "Mother's address line 2",
			"es": "Complemento madre"
        },
        "l-msg-label-tipo-bairro-mae":{
            "pt": "Tipo bairro mãe",
			"en": "Mother's type of district",
			"es": "Tipo barrio madre"
        },
        "l-msg-label-bairro-mae":{
            "pt": "Bairro mãe",
			"en": "Mother's district",
			"es": "Barrio madre"
        },
        "l-msg-label-pais-mae":{
            "pt": "País mãe",
			"en": "Mother's country",
			"es": "País madre"
        },
        "l-msg-label-uf-mae":{
            "pt": "UF mãe",
			"en": "Mother's State",
			"es": "Est/Prov/Reg madre"
        },
        "l-msg-label-telefone-residencial-mae":{
            "pt": "Telefone residencial mãe",
			"en": "Mother's home phone number",
			"es": "Teléfono residencial madre"
        },
        "l-msg-label-telefone-calular-mae":{
            "pt": "Telefone celular mãe",
			"en": "Mother's mobile phone number",
			"es": "Teléfono celular madre"
        },
        "l-msg-label-telefone-comercial-mae":{
            "pt": "Telefone comercial mãe",
			"en": "Mother's business phone number",
			"es": "Teléfono comercial madre"
        },
        "l-msg-label-fax-mae":{
            "pt": "Fax mãe",
			"en": "Mother's fax number",
			"es": "Fax madre"
         },
         "l-msg-label-cor-raca-mae":{
             "pt": "Cor/raça mãe",
			 "en": "Mother's ethnic group",
			 "es": "Color/etnia madre"
         },
         "l-msg-label-pofissao-mae":{
             "pt": "Profissão mãe",
			 "en": "Mother's profession",
			 "es": "Profesión madre"
         },
         "l-msg-label-tipo-sanguineo-mae":{
             "pt": "Tipo sanguíneo mãe",
			 "en": "Mother's blood type",
			 "es": "Tipo sanguíneo madre"
         },
         "l-msg-label-numero-passaporte-mae":{
             "pt": "Número passaporte mãe",
			 "en": "Mother's passport number",
			 "es": "Número pasaporte madre"
         },
         "l-msg-label-data-emissao-passaporte-mae":{
             "pt": "Data emissão passaporte mãe",
			 "en": "Mother's passport issue date",
			 "es": "Fecha de emisión pasaporte madre"
         },
         "l-msg-label-data-validade-passaporte-mae":{
             "pt": "Data validade passaporte mãe",
			 "en": "Mother's passport validity date",
			 "es": "Fecha de validez pasaporte madre"
         },
         "l-msg-label-pais-origem-passaporte-mae":{
            "pt": "País origem passaporte mãe",
			"en": "Mother's passport origin country",
			"es": "País origen pasaporte madre"
         },
         "l-msg-label-data-chegada-brasil-mae":{
             "pt": "Data chegada Brasil mãe",
			 "en": "Mother's date of arrival to Brazil",
			 "es": "Fecha de llegada a Brasil madre"
         },
         "l-msg-label-tipo-visto-mae":{
             "pt": "Tipo visto mãe",
			 "en": "Mother's type of visa",
			 "es": "Tipo visa madre"
         },
         "l-msg-label-carteira-modelo-19-mae":{
             "pt": "Carteira modelo 19 mãe",
			 "en": "Mother's identification card model 19",
			 "es": "Documento de identidad modelo 19 madre"
         },
         "l-msg-label-numero-filhos-brasileiros-mae":{
             "pt": "Número filhos brasileiros mãe",
			 "en": "Mother's number of Brazilian children",
			 "es": "Número hijos brasileños madre"
         },
         "l-msg-label-estrangeiro-naturalizado-mae":{
             "pt": "Estrangeiro naturalizado mãe",
			 "en": "Mother foreigner naturalized",
			 "es": "Extranjero naturalizado madre"
         },
         "l-msg-label-conjuge-brasil-mae":{
             "pt": "Cônjuge Brasil mãe",
			 "en": "Mother Brazil spouse",
			 "es": "Cónyuge Brasil madre"
         },
         "l-msg-label-registro-geral-mae":{
             "pt": "Registro geral mãe",
			 "en": "Mother's general record",
			 "es": "Registro general madre"
         },
         "l-msg-label-decreto-imigracao-mae":{
             "pt": "Decreto imigração mãe",
			 "en": "Mother's immigration decree",
			 "es": "Decreto inmigración madre"
         },
         "l-msg-label-data-vencimento-ctps-mae":{
             "pt": "Data vencimento carteira trabalho estrangeiro mãe",
			 "en": "Expiration date of mother's foreigner employee card",
			 "es": "Fecha de vencimiento libreta de trabajo extranjero madre"
         },
         "l-msg-label-data-vencimento-rg-mae":{
             "pt": "Data vencimento registro geral estrangeiros mãe",
			 "en": "Expiration date of mother's foreigner general record",
			 "es": "Fecha de vencimiento registro general extranjeros madre"
         },
        "l-msg-label-nome-resp-acad": {
            "pt": "Nome responsável acadêmico",
			"en": "Name of Academics person in charge",
			"es": "Nombre responsable académico"
        },
        "l-msg-label-data-nascimento-resp-acad": {
            "pt": "Data nascimento responsável acadêmico",
			"en": "Expiration date Academics person in charge",
			"es": "Fecha de nacimiento responsable académico"
        },
        "l-msg-label-nome-social-resp-acad": {
            "pt": "Nome social responsável acadêmico",
			"en": "Social name Academics person in charge",
			"es": "Nombre social responsable académico"
        },
        "l-msg-label-apelido-resp-acad": {
            "pt": "Apelido responsável acadêmico",
			"en": "Nickname Academics person in charge",
			"es": "Alias responsable académico"
        },
        "l-msg-label-estado-civil-resp-acad": {
            "pt": "Estado civil responsável acadêmico",
			"en": "Marital status Academics person in charge",
			"es": "Estado civil responsable académico"
        },
        "l-msg-label-nacionalidade-resp-acad": {
            "pt": "Nacionalidade responsável acadêmico",
			"en": "Nationality Academics person in charge",
			"es": "Nacionalidad responsable académico"
        },
        "l-msg-label-email-resp-acad": {
            "pt": "E-mail responsável acadêmico",
			"en": "E-mail Academics person in charge",
			"es": "E-mail responsable académico"
        },
        "l-msg-label-sexo-resp-acad": {
            "pt": "Sexo responsável acadêmico",
			"en": "Gender Academics person in charge",
			"es": "Sexo responsable académico"
        },
        "l-msg-label-canhoto-resp-acad": {
            "pt": "Candidato canhoto",
			"en": "Left-handed applicant",
			"es": "Candidato zurdo"
        },
        "l-msg-label-fumante-resp-acad": {
            "pt": "Candidato fumante",
			"en": "Smoker applicant",
			"es": "Candidato fumador"
        },
        "l-msg-label-numero-cpf-resp-acad":{
            "pt": "Número do CPF responsável acadêmico",
			"en": "CPF Number Academics person in charge",
			"es": "Número del RCPF responsable académico"
        },
        "l-msg-label-numero-rg-resp-acad":{
            "pt": "Número do RG responsável acadêmico",
			"en": "ID Number of Academics person in charge",
			"es": "Número de DI responsable académico"
        },
        "l-msg-label-data-emissao-rg-resp-acad":{
            "pt": "Data emissão RG responsável acadêmico",
			"en": "ID issue date Academics person in charge",
			"es": "Fecha de emisión DI responsable académico"
        },
        "l-msg-label-orgao-emissor-rg-resp-acad":{
            "pt": "Órgão emissor RG responsável acadêmico",
			"en": "ID issuing body Academics person in charge",
			"es": "Organismo emisor DI responsable académico"
        },
        "l-msg-label-numero-registro-profossional-resp-acad":{
            "pt": "Número registro profissional responsável acadêmico",
			"en": "Professional record number Academics person in charge",
			"es": "Número registro profesional responsable académico"
        },
        "l-msg-label-numero-titulo-eleitor-resp-acad":{
            "pt": "Número do título de eleitor responsável acadêmico",
			"en": "Voter registration number Academics person in charge",
			"es": "Número de credencial elector responsable académico"
        },
        "l-msg-label-zona-titulo-eleitor-resp-acad":{
            "pt": "Zona título eleitor responsável acadêmico",
			"en": "Voter registration zone Academics person in charge",
			"es": "Zona credencial elector responsable académico"
        },
        "l-msg-label-secao-titulo-eleitor-resp-acad":{
            "pt": "Seção título eleitor candidato",
			"en": "Applicant's voter registration section",
			"es": "Sección credencial elector candidato"
        },
        "l-msg-label-data-emissao-titulo-eleitor-resp-acad":{
            "pt": "Data emissão título eleitor responsável acadêmico",
			"en": "Issue date of voter registration Academics person in charge",
			"es": "Fecha de emisión credencial elector responsable académico"
        },
        "l-msg-label-numero-carteira-trabalho-resp-acad":{
            "pt": "Número carteira trabalho responsável acadêmico",
			"en": "Employee card number Academics person in charge",
			"es": "Número libreta de trabajo responsable académico"
        },
        "l-msg-label-serie-carteira-trabalho-resp-acad":{
            "pt": "Série carteira trabalho responsável acadêmico",
			"en": "Employee card series Academics person in charge",
			"es": "Serie libreta de trabajo responsable académico"
        },
        "l-msg-label-data-emissao-carteira-trabalho-resp-acad":{
            "pt": "Data emissão carteira trabalho responsável acadêmico",
			"en": "Employee card issue date Academics person in charge",
			"es": "Fecha de emisión libreta de trabajo responsable académico"
        },
        "l-msg-label-carteira-trabalho-tipo-nit-resp-acad":{
            "pt": "Carteira trabalho tipo NIT responsável acadêmico",
			"en": "Employee card NIT type Academics person in charge",
			"es": "Libreta de trabajo tipo NIT responsable académico"
        },
        "l-msg-label-carteira-motorista-resp-acad":{
            "pt": "Carteira motorista responsável acadêmico",
			"en": "Driver's license Academics person in charge",
			"es": "Licencia de conducir responsable académico"
        },
        "l-msg-label-tipo-carteira-motorista-resp-acad":{
            "pt": "Tipo carteira motorista responsável acadêmico",
			"en": "Driver's license type Academics person in charge",
			"es": "Tipo licencia de conducir responsable académico"
        },
        "l-msg-label-data-vencimento-carteira-motorista-resp-acad":{
            "pt": "Data vencimento carteira motorista responsável acadêmico",
			"en": "Expiration date of driver's license Academics person in charge",
			"es": "Fecha de vencimiento licencia de conducir responsable académico"
        },
        "l-msg-label-numero-cert-reservista-resp-acad":{
            "pt": "Número certificado reservista responsável acadêmico",
			"en": "Number of military discharge certificate Academics person in charge",
			"es": "Número libreta militar de reservista responsable académico"
        },
        "l-msg-label-categoria-militar-resp-acad":{
            "pt": "Categoria militar responsável acadêmico",
			"en": "Military category Academics person in charge",
			"es": "Categoría militar responsable académico"
        },
        "l-msg-label-circunscricao-militar-resp-acad":{
            "pt": "Circunscrição militar responsável acadêmico",
			"en": "Military category circuit Academics person in charge",
			"es": "Circunscripción militar responsable académico"
        },
        "l-msg-label-regiao-militar-resp-acad":{
            "pt": "Região militar responsável acadêmico",
			"en": "Military region Academics person in charge",
			"es": "Región militar responsable académico"
        },
        "l-msg-label-data-emissao-cert-reservista-resp-acad":{
            "pt": "Data emissão certificado reservista responsável acadêmico",
			"en": "Issue date of military discharge certificate Academics person in charge",
			"es": "Fecha de emisión libreta militar de reservista responsable académico"
        },
        "l-msg-label-orgao-emissor-cert-reservista-resp-acad":{
            "pt": "Órgão emissor certificado reservista responsável acadêmico",
			"en": "Issuing body of military discharge certificate Academics person in charge",
			"es": "Organismo emisor libreta militar de reservista responsable académico"
        },
        "l-msg-label-situacao-militar-resp-acad":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-msg-label-cep-resp-acad":{
            "pt": "CEP responsável acadêmico",
			"en": "Postal code Academics person in charge",
			"es": "CP responsable académico"
        },
        "l-msg-label-tipo-rua-resp-acad":{
            "pt": "Tipo rua responsável acadêmico",
			"en": "Type of street Academics person in charge",
			"es": "Tipo calle responsable académico"
        },
        "l-msg-label-rua-resp-acad":{
            "pt": "Rua responsável acadêmico",
			"en": "Street Academics person in charge",
			"es": "Calle responsable académico"
        },
        "l-msg-label-numero-resp-acad":{
            "pt": "Número responsável acadêmico",
			"en": "Number Academics person in charge",
			"es": "Número responsable académico"
        },
        "l-msg-label-complemento-resp-acad":{
            "pt": "Complemento responsável acadêmico",
			"en": "Address line 2 Academics person in charge",
			"es": "Complemento responsable académico"
        },
        "l-msg-label-tipo-bairro-resp-acad":{
            "pt": "Tipo bairro responsável acadêmico",
			"en": "Type of district Academics person in charge",
			"es": "Tipo barrio responsable académico"
        },
        "l-msg-label-bairro-resp-acad":{
            "pt": "Bairro responsável acadêmico",
			"en": "District Academics person in charge",
			"es": "Barrio responsable académico"
        },
        "l-msg-label-pais-resp-acad":{
            "pt": "País responsável acadêmico",
			"en": "Country Academics person in charge",
			"es": "País responsable académico"
        },
        "l-msg-label-uf-resp-acad":{
            "pt": "UF responsável acadêmico",
			"en": "State Academics person in charge",
			"es": "Est/Prov/Reg responsable académico"
        },
        "l-msg-label-telefone-residencial-resp-acad":{
            "pt": "Telefone residencial responsável acadêmico",
			"en": "Home phone number Academics person in charge",
			"es": "Teléfono residencial responsable académico"
        },
        "l-msg-label-telefone-calular-resp-acad":{
            "pt": "Telefone celular responsável acadêmico",
			"en": "Mobile phone number Academics person in charge",
			"es": "Teléfono celular responsable académico"
        },
        "l-msg-label-telefone-comercial-resp-acad":{
            "pt": "Telefone comercial responsável acadêmico",
			"en": "Business phone number Academics person in charge",
			"es": "Teléfono comercial responsable académico"
        },
        "l-msg-label-fax-resp-acad":{
            "pt": "Fax responsável acadêmico",
			"en": "Fax number Academics person in charge",
			"es": "Fax responsable académico"
         },
         "l-msg-label-cor-raca-resp-acad":{
             "pt": "Cor/raça responsável acadêmico",
			 "en": "Ethnic group Academics person in charge",
			 "es": "Color/etnia responsable académico"
         },
         "l-msg-label-pofissao-resp-acad":{
             "pt": "Profissão responsável acadêmico",
			 "en": "Profession Academics person in charge",
			 "es": "Profesión responsable académico"
         },
         "l-msg-label-tipo-sanguineo-resp-acad":{
             "pt": "Tipo sanguíneo responsável acadêmico",
			 "en": "Blood type Academics person in charge",
			 "es": "Tipo sanguíneo responsable académico"
         },
         "l-msg-label-numero-passaporte-resp-acad":{
             "pt": "Número passaporte responsável acadêmico",
			 "en": "Passport number Academics person in charge",
			 "es": "Número pasaporte responsable académico"
         },
         "l-msg-label-data-emissao-passaporte-resp-acad":{
             "pt": "Data emissão passaporte responsável acadêmico",
			 "en": "Passport issue date Academics person in charge",
			 "es": "Fecha de emisión pasaporte responsable académico"
         },
         "l-msg-label-data-validade-passaporte-resp-acad":{
             "pt": "Data validade passaporte responsável acadêmico",
			 "en": "Passport validity date Academics person in charge",
			 "es": "Fecha de validez pasaporte responsable académico"
         },
         "l-msg-label-pais-origem-passaporte-resp-acad":{
            "pt": "País origem passaporte responsável acadêmico",
			"en": "Passport origin country Academics person in charge",
			"es": "País origen pasaporte responsable académico"
         },
         "l-msg-label-data-chegada-brasil-resp-acad":{
             "pt": "Data chegada Brasil responsável acadêmico",
			 "en": "Date of arrival to Brazil Academics person in charge",
			 "es": "Fecha de llegada a Brasil responsable académico"
         },
         "l-msg-label-tipo-visto-resp-acad":{
             "pt": "Tipo visto responsável acadêmico",
			 "en": "Type of visa Academics person in charge",
			 "es": "Tipo visa responsable académico"
         },
         "l-msg-label-carteira-modelo-19-resp-acad":{
             "pt": "Carteira modelo 19 responsável acadêmico",
			 "en": "Identification card model 19 Academics person in charge",
			 "es": "Documento de identidad modelo 19 responsable académico"
         },
         "l-msg-label-numero-filhos-brasileiros-resp-acad":{
             "pt": "Número filhos brasileiros responsável acadêmico",
			 "en": "Brazilian number of children Academics person in charge",
			 "es": "Número hijos brasileños responsable académico"
         },
         "l-msg-label-estrangeiro-naturalizado-resp-acad":{
             "pt": "Estrangeiro naturalizado responsável acadêmico",
			 "en": "Foreigner naturalized Academics person in charge",
			 "es": "Extranjero naturalizado responsable académico"
         },
         "l-msg-label-conjuge-brasil-resp-acad":{
             "pt": "Cônjuge Brasil responsável acadêmico",
			 "en": "Spouse Brazil Academics person in charge",
			 "es": "Cónyuge Brasil responsable académico"
         },
         "l-msg-label-registro-geral-resp-acad":{
             "pt": "Registro geral responsável acadêmico",
			 "en": "General record Academics person in charge",
			 "es": "Registro general responsable académico"
         },
         "l-msg-label-decreto-imigracao-resp-acad":{
             "pt": "Decreto imigração responsável acadêmico",
			 "en": "Immigration decree Academics person in charge",
			 "es": "Decreto inmigración responsable académico"
         },
         "l-msg-label-data-vencimento-ctps-resp-acad":{
             "pt": "Data vencimento carteira trabalho estrangeiro responsável acadêmico",
			 "en": "Expiration date of foreigner employee card Academics person in charge",
			 "es": "Fecha de vencimiento libreta de trabajo extranjero responsable académico"
         },
         "l-msg-label-data-vencimento-rg-resp-acad":{
             "pt": "Data vencimento registro geral estrangeiros responsável acadêmico",
			 "en": "Expiration date of foreigners general record Academics person in charge",
			 "es": "Fecha de vencimiento registro general extranjeros responsable académico"
         },
        "l-msg-label-nome-resp-fin": {
            "pt": "Nome responsável financeiro",
			"en": "Name Financials person in charge",
			"es": "Nombre responsable financiero"
        },
        "l-msg-label-data-nascimento-resp-fin": {
            "pt": "Data nascimento responsável financeiro",
			"en": "Birth date Financials person in charge",
			"es": "Fecha de nacimiento responsable financiero"
        },
        "l-msg-label-pais-natal-resp-fin": {
            "pt": "País natal responsável financeiro",
			"en": "Country of birth Financials person in charge",
			"es": "País natal responsable financiero"
        },
        "l-msg-label-estado-natal-resp-fin": {
            "pt": "Estado natal responsável financeiro",
			"en": "State of birth Financials person in charge",
			"es": "Estado/Provincia/Región natal responsable financiero"
        },
        "l-msg-label-naturalidade-resp-fin": {
            "pt": "Naturalidade responsável financeiro",
			"en": "Place of birth Financials person in charge",
			"es": "Lugar de nacimiento responsable financiero"
        },
        "l-msg-label-nome-social-resp-fin": {
            "pt": "Nome social responsável financeiro",
			"en": "Social name Financials person in charge",
			"es": "Nombre social responsable financiero"
        },
        "l-msg-label-apelido-resp-fin": {
            "pt": "Apelido responsável financeiro",
			"en": "Nickname Financials person in charge",
			"es": "Alias responsable financiero"
        },
        "l-msg-label-estado-civil-resp-fin": {
            "pt": "Estado civil responsável financeiro",
			"en": "Marital status Financials person in charge",
			"es": "Estado civil responsable financiero"
        },
        "l-msg-label-nacionalidade-resp-fin": {
            "pt": "Nacionalidade responsável financeiro",
			"en": "Nationality Financials person in charge",
			"es": "Nacionalidad responsable financiero"
        },
        "l-msg-label-email-resp-fin": {
            "pt": "E-mail responsável financeiro",
			"en": "E-mail Financials person in charge",
			"es": "E-mail responsable financiero"
        },
        "l-msg-label-sexo-resp-fin": {
            "pt": "Sexo responsável financeiro",
			"en": "Gender Financials person in charge",
			"es": "Sexo responsable financiero"
        },
        "l-msg-label-canhoto-resp-fin": {
            "pt": "Candidato canhoto",
			"en": "Left-handed applicant",
			"es": "Candidato zurdo"
        },
        "l-msg-label-apelido-fumante-resp-fin": {
            "pt": "Candidato fumante",
			"en": "Smoker applicant",
			"es": "Candidato fumador"
        },
        "l-msg-label-numero-cpf-resp-fin":{
            "pt": "Número do CPF responsável financeiro",
			"en": "CPF number Financials person in charge",
			"es": "Número de RCPF responsable financiero"
        },
        "l-msg-label-numero-rg-resp-fin":{
            "pt": "Número do RG responsável financeiro",
			"en": "ID number Financials person in charge",
			"es": "Número de DI responsable financiero"
        },
        "l-msg-label-data-emissao-rg-resp-fin":{
            "pt": "Data emissão RG responsável financeiro",
			"en": "ID issue date Financials person in charge",
			"es": "Fecha de emisión DI responsable financiero"
        },
        "l-msg-label-orgao-emissor-rg-resp-fin":{
            "pt": "Órgão emissor RG responsável financeiro",
			"en": "ID issuing body Financials person in charge",
			"es": "Organismo emisor DI responsable financiero"
        },
        "l-msg-label-pais-orgao-emissor-rg-resp-fin":{
            "pt": "País órgão emissor RG responsável financeiro",
			"en": "ID issuing body country Financials person in charge",
			"es": "País organismo emisor DI responsable financiero"
        },
        "l-msg-label-uf-orgao-emissor-rg-resp-fin":{
            "pt": "UF órgão emissor RG responsável financeiro",
			"en": "ID issuing body state Financials person in charge",
			"es": "Est/Prov/Reg organismo emisor DI responsable financiero"
        },
        "l-msg-label-numero-registro-profossional-resp-fin":{
            "pt": "Número registro profissional responsável financeiro",
			"en": "Professional record number Financials person in charge",
			"es": "Número registro profesional responsable financiero"
        },
        "l-msg-label-numero-titulo-eleitor-resp-fin":{
            "pt": "Número do título de eleitor responsável financeiro",
			"en": "Voter registration number Financials person in charge",
			"es": "Número de credencial de elector responsable financiero"
        },
        "l-msg-label-zona-titulo-eleitor-resp-fin":{
            "pt": "Zona título eleitor responsável financeiro",
			"en": "Voter registration zone Financials person in charge",
			"es": "Zona credencial elector responsable financiero"
        },
        "l-msg-label-secao-titulo-eleitor-resp-fin":{
            "pt": "Seção título eleitor candidato",
			"en": "Applicant's voter registration section",
			"es": "Sección credencial elector candidato"
        },
        "l-msg-label-data-emissao-titulo-eleitor-resp-fin":{
            "pt": "Data emissão título eleitor responsável financeiro",
			"en": "Voter registration issue date Financials person in charge",
			"es": "Fecha de emisión credencial elector responsable financiero"
        },
        "l-msg-label-pais-orgao-emissor-titulo-eleitor-resp-fin":{
            "pt": "País órgão emissor título eleitor responsável financeiro",
			"en": "Issuing body country of voter registration Financials person in charge",
			"es": "País organismo emisor credencial elector responsable financiero"
        },
        "l-msg-label-uf-orgao-emissor-titulo-eleitor-resp-fin":{
            "pt": "Uf órgão emissor título eleitor responsável financeiro",
			"en": "Voter registration issuing body state Financials person in charge",
			"es": "Est/Prov/Reg organismo emisor credencial elector responsable financiero"
        },
        "l-msg-label-numero-carteira-trabalho-resp-fin":{
            "pt": "Número carteira trabalho responsável financeiro",
			"en": "Employee card number Financials person in charge",
			"es": "Número libreta de trabajo responsable financiero"
        },
        "l-msg-label-serie-carteira-trabalho-resp-fin":{
            "pt": "Série carteira trabalho responsável financeiro",
			"en": "Employee card series Financials person in charge",
			"es": "Serie libreta de trabajo responsable financiero"
        },
        "l-msg-label-data-emissao-carteira-trabalho-resp-fin":{
            "pt": "Data emissão carteira trabalho responsável financeiro",
			"en": "Employee card issue date Financials person in charge",
			"es": "Fecha de emisión libreta de trabajo responsable financiero"
        },
        "l-msg-label-pais-orgao-emissor-carteira-trabalho-resp-fin":{
            "pt": "País órgão emissor carteira trabalho responsável financeiro",
			"en": "Employee card issuing body country Financials person in charge",
			"es": "País organismo emisor libreta de trabajo responsable financiero"
        },
        "l-msg-label-uf-orgao-emissor-carteira-trabalho-resp-fin":{
            "pt": "UF órgão emissor carteira trabalho responsável financeiro",
			"en": "Employee card issuing body state Financials person in charge",
			"es": "Est/Prov/Reg organismo emisor libreta de trabajo responsable financiero"
        },
        "l-msg-label-carteira-trabalho-tipo-nit-resp-fin":{
            "pt": "Carteira trabalho tipo NIT responsável financeiro",
			"en": "Employee card NIT type Financials person in charge",
			"es": "Libreta de trabajo tipo NIT responsable financiero"
        },
        "l-msg-label-carteira-motorista-resp-fin":{
            "pt": "Carteira motorista responsável financeiro",
			"en": "Driver's license Financials person in charge",
			"es": "Licencia de conducir responsable financiero"
        },
        "l-msg-label-tipo-carteira-motorista-resp-fin":{
            "pt": "Tipo carteira motorista responsável financeiro",
			"en": "Driver's license type Financials person in charge",
			"es": "Tipo licencia de conducir responsable financiero"
        },
        "l-msg-label-data-vencimento-carteira-motorista-resp-fin":{
            "pt": "Data vencimento carteira motorista responsável financeiro",
			"en": "Expiration date of driver's license Financials person in charge",
			"es": "Fecha de vencimiento licencia de conducir responsable financiero"
        },
        "l-msg-label-numero-cert-reservista-resp-fin":{
            "pt": "Número certificado reservista responsável financeiro",
			"en": "Number of military discharge certificate Financials person in charge",
			"es": "Número libreta militar de reservista responsable financiero"
        },
        "l-msg-label-cetegoria-militar-resp-fin":{
            "pt": "Categoria militar responsável financeiro",
			"en": "Military category Financials person in charge",
			"es": "Categoría militar responsable financiero"
        },
        "l-msg-label-circunscricao-militar-resp-fin":{
            "pt": "Circunscrição militar responsável financeiro",
			"en": "Military circuit Financials person in charge",
			"es": "Circunscripción militar responsable financiero"
        },
        "l-msg-label-regiao-militar-resp-fin":{
            "pt": "Região militar responsável financeiro",
			"en": "Military region Financials person in charge",
			"es": "Región militar responsable financiero"
        },
        "l-msg-label-data-emissao-cert-reservista-resp-fin":{
            "pt": "Data emissão certificado reservista responsável financeiro",
			"en": "Issue date of military discharge certificate Financials person in charge",
			"es": "Fecha de emisión libreta militar de reservista responsable financiero"
        },
        "l-msg-label-orgao-emissor-cert-reservista-resp-fin":{
            "pt": "Órgão emissor certificado reservista responsável financeiro",
			"en": "Issuing body of military discharge certificate Financials person in charge",
			"es": "Organismo emisor libreta militar de reservista responsable financiero"
        },
        "l-msg-label-situacao-militar-resp-fin":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-msg-label-cep-resp-fin":{
            "pt": "CEP responsável financeiro",
			"en": "Postal code Financials person in charge",
			"es": "CP responsable financiero"
        },
        "l-msg-label-tipo-rua-resp-fin":{
            "pt": "Tipo rua responsável financeiro",
			"en": "Type of street Financials person in charge",
			"es": "Tipo calle responsable financiero"
        },
        "l-msg-label-rua-resp-fin":{
            "pt": "Rua responsável financeiro",
			"en": "Street Financials person in charge",
			"es": "Calle responsable financiero"
        },
        "l-msg-label-numero-resp-fin":{
            "pt": "Número responsável financeiro",
			"en": "Number Financials person in charge",
			"es": "Número responsable financiero"
        },
        "l-msg-label-complemento-resp-fin":{
            "pt": "Complemento responsável financeiro",
			"en": "Address line 2 Financials person in charge",
			"es": "Complemento responsable financiero"
        },
        "l-msg-label-tipo-bairro-resp-fin":{
            "pt": "Tipo bairro responsável financeiro",
			"en": "Type of district Financials person in charge",
			"es": "Tipo barrio responsable financiero"
        },
        "l-msg-label-bairro-resp-fin":{
            "pt": "Bairro responsável financeiro",
			"en": "District Financials person in charge",
			"es": "Barrio responsable financiero"
        },
        "l-msg-label-pais-resp-fin":{
            "pt": "País responsável financeiro",
			"en": "Country Financials person in charge",
			"es": "País responsable financiero"
        },
        "l-msg-label-uf-resp-fin":{
            "pt": "UF responsável financeiro",
			"en": "State Financials person in charge",
			"es": "Est/Prov/Reg responsable financiero"
        },
        "l-msg-label-telefone-residencial-resp-fin":{
            "pt": "Telefone residencial responsável financeiro",
			"en": "Home phone number Financials person in charge",
			"es": "Teléfono residencial responsable financiero"
        },
        "l-msg-label-telefone-calular-resp-fin":{
            "pt": "Telefone celular responsável financeiro",
			"en": "Mobile phone number Financials person in charge",
			"es": "Teléfono celular responsable financiero"
        },
        "l-msg-label-telefone-comercial-resp-fin":{
            "pt": "Telefone comercial responsável financeiro",
			"en": "Business phone number Financials person in charge",
			"es": "Teléfono comercial responsable financiero"
        },
        "l-msg-label-fax-resp-fin":{
            "pt": "Fax responsável financeiro",
			"en": "Fax number Financials person in charge",
			"es": "Fax responsable financiero"
         },
         "l-msg-label-cor-raca-resp-fin":{
             "pt": "Cor/raça responsável financeiro",
			 "en": "Ethnic group Financials person in charge",
			 "es": "Colo/etnia responsable financiero"
         },
         "l-msg-label-pofissao-resp-fin":{
             "pt": "Profissão responsável financeiro",
			 "en": "Profession Financials person in charge",
			 "es": "Profesión responsable financiero"
         },
         "l-msg-label-tipo-sanguineo-resp-fin":{
             "pt": "Tipo sanguíneo responsável financeiro",
			 "en": "Blood type Financials person in charge",
			 "es": "Tipo sanguíneo responsable financiero"
         },
         "l-msg-label-numero-passaporte-resp-fin":{
             "pt": "Número passaporte responsável financeiro",
			 "en": "Passport number Financials person in charge",
			 "es": "Número pasaporte responsable financiero"
         },
         "l-msg-label-data-emissao-passaporte-resp-fin":{
             "pt": "Data emissão passaporte responsável financeiro",
			 "en": "Passport issue date Financials person in charge",
			 "es": "Fecha de emisión pasaporte responsable financiero"
         },
         "l-msg-label-data-validade-passaporte-resp-fin":{
             "pt": "Data validade passaporte responsável financeiro",
			 "en": "Passport validity date Financials person in charge",
			 "es": "Fecha de validez pasaporte responsable financiero"
         },
         "l-msg-label-pais-origem-passaporte-resp-fin":{
            "pt": "País origem passaporte responsável financeiro",
			"en": "Passport origin country Financials person in charge",
			"es": "País origen pasaporte responsable financiero"
         },
         "l-msg-label-data-chegada-brasil-resp-fin":{
             "pt": "Data chegada Brasil responsável financeiro",
			 "en": "Arrival date to Brazil Financials person in charge",
			 "es": "Fecha de llegada a Brasil responsable financiero"
         },
         "l-msg-label-tipo-visto-resp-fin":{
             "pt": "Tipo visto responsável financeiro",
			 "en": "Type of visa Financials person in charge",
			 "es": "Tipo visa responsable financiero"
         },
         "l-msg-label-carteira-modelo-19-resp-fin":{
             "pt": "Carteira modelo 19 responsável financeiro",
			 "en": "Identification card model 19 Financials person in charge",
			 "es": "Documento de identidad modelo 19 responsable financiero"
         },
         "l-msg-label-numero-filhos-brasileiros-resp-fin":{
             "pt": "Número filhos brasileiros responsável financeiro",
			 "en": "Number of Brazilian children Financials person in charge",
			 "es": "Número hijos brasileños responsable financiero"
         },
         "l-msg-label-estrangeiro-naturalizado-resp-fin":{
             "pt": "Estrangeiro naturalizado responsável financeiro",
			 "en": "Foreigner naturalized Financials person in charge",
			 "es": "Extranjero naturalizado responsable financiero"
         },
         "l-msg-label-conjuge-brasil-resp-fin":{
             "pt": "Cônjuge Brasil responsável financeiro",
			 "en": "Spouse Brazil Financials person in charge",
			 "es": "Cónyuge Brasil responsable financiero"
         },
         "l-msg-label-registro-geral-resp-fin":{
             "pt": "Registro geral responsável financeiro",
			 "en": "General record Financials person in charge",
			 "es": "Registro general responsable financiero"
         },
         "l-msg-label-decreto-imigracao-resp-fin":{
             "pt": "Decreto imigração responsável financeiro",
			 "en": "Immigration decree Financials person in charge",
			 "es": "Decreto inmigración responsable financiero"
         },
         "l-msg-label-data-vencimento-ctps-resp-fin":{
             "pt": "Data vencimento carteira trabalho estrangeiro responsável financeiro",
			 "en": "Expiration date foreigner employee card Financials person in charge",
			 "es": "Fecha de vencimiento libreta de trabajo extranjero responsable financiero"
         },
         "l-msg-label-data-vencimento-rg-resp-fin":{
             "pt": "Data vencimento registro geral estrangeiros responsável financeiro",
			 "en": "Expiration date foreigner general record Financials person in charge",
			 "es": "Fecha de vencimiento registro general extranjeros responsable financiero"
         },
        "l-campos-obrigatorios":{
            "pt": "Os campos a seguir devem ser preenchidos: ",
			"en": "The following fields must be filled out: ",
			"es": "Los siguientes campos deben cumplimentarse: "
        },
        "l-atencao":{
            "pt": "ATENÇÃO",
			"en": "ATTENTION",
			"es": "ATENCIÓN"
        },
        "l-sucesso":{
            "pt": "SUCESSO",
			"en": "SUCCESS",
			"es": "EXITO"
        },
        "l-mensagem-salvar":{
            "pt": "Essa é uma mensagem de teste. Os dados não foram salvos pois o serviço ainda não foi chamado.",
			"en": "This is a test message. Data were not saved, as service has not been called yet.",
			"es": "Este es un mensaje de prueba. Los datos no se grabaron porque aún no se ha llamado el servicio."
        },
        "l-finalizar":{
            "pt": "Finalizar",
			"en": "Finalize",
			"es": "Finalizar"
        },
        "l-voltar":{
            "pt": "Retornar ao cadastro",
			"en": "Return to register",
			"es": "Volver al archivo"
        },
        "l-li-aceito":{
            "pt": "Li e aceito os termos e condições",
			"en": "I read and accept the terms and conditions",
			"es": "Leí y acepto los términos y condiciones"
        },
        "l-recibo-inscricao": {
            "pt": "Inscrição realizada",
			"en": "Inscrição realizada",
			"es": "Inscrição realizada"
        },
        "l-btn-boleto": {
            "pt": "Pagar via boleto",
			"en": "Pay with bank bill",
			"es": "Pagar con boleta"
        },
        "l-btn-cartao": {
            "pt": "Pagar com cartão",
			"en": "Pay with card",
			"es": "Pagar con tarjeta"
        },
        "l-campos-complementares": {
            "pt": "Campos complementares",
			"en": "Additional fields",
			"es": "Campos adicionales"
        },
        "l-btn-comprovante": {
            "pt": "Comprovante",
			"en": "Receipt",
			"es": "Comprobante"
        },
        "l-msg-senha": {
            "pt": "Senha",
			"en": "Password",
			"es": "Contraseña"
        },
        "l-msg-repete-senha": {
            "pt": "Repetição da senha",
			"en": "Repeatition of password",
			"es": "Repetición de contraseña"
        },
        "l-titulo-comprovante": {
            "pt": "Comprovante",
			"en": "Receipt",
			"es": "Comprobante"
        },
        "l-mesmo-endereco-candidato":{
            "pt": " Utilizar mesmo endereço do candidato",
			"en": " Use the same address of the applicant",
			"es": " Utilizar la misma dirección del candidato"
        },
        "l-texto-termo-aceite-padrao":{
            "pt": "Ao inscrever-me, estou ciente da importância das informações aqui descritas. Sei também que devo manter meus dados sempre atualizados, através deles poderei receber notícias durante e após o término das inscrições.",
			"en": "To register, I am aware of the importance of the information described here. I also know that I must keep my data updated, through them, I could receive news during and after the end of registrations.",
			"es": "Al inscribirme, estoy conciente de la importancia de la información aquí descrita. También sé que siempre debo mantener mis datos actualizados, porque por medio de ellos podré recibir noticias durante y después del término de las inscripciones."
        },
        "l-termo-aceite":{
            "pt": "Termo de aceite",
			"en": "Term of acceptance",
			"es": "Término de aceptación"
        },
        "l-continuar":{
            "pt": "Continuar",
			"en": "Continue",
			"es": "Continuar"
        },
        "l-msg-email-invalido":{
            "pt": "E-mail inválido",
			"en": "E-mail invalid",
			"es": "E-mail no válido"
        },
        "l-senha-diferente": {
            "pt": "A confirmação da senha não confere com a senha digitada.",
			"en": "The confirmation of password does not match entered password.",
			"es": "La confirmación de la contraseña no coincide con la misma contraseña digitada."
        },
        "l-dados-enem": {
            "pt": "Dados do ENEM (Exame Nacional do Ensino Médio)",
			"en": "ENEM (National Exam of High School) data",
			"es": "Datos del ENEM (Examen Nacional de Educación Secundaria)"
        },
        "l-numero-inscricao-enem": {
            "pt": "Número de inscrição",
			"en": "Number of registration",
			"es": "Número de inscripción"
        },
        "l-ano-referencia-enem":{
            "pt": "Ano de referência",
			"en": "Reference year",
			"es": "Año de referencia"
        },
        "l-msg-cep-nao-encontrado": {
            "pt": "CEP não encontrado!",
			"en": "Postal code not found!",
			"es": "¡CP no encontrado!"
        },
        "l-dados-indicacao": {
            "pt": "Indicação",
			"en": "Indication",
			"es": "Indicación"
        },
        "l-indicacao-aluno": {
            "pt": "R.A de indicação do(a) aluno(a)",
			"en": "Enrollment number indicating student",
			"es": "R.A de indicación del alumno"
        },
        "l-indicacao-aluno-nome": {
            "pt": "Nome do(a) aluno(a)",
			"en": "Student's name",
			"es": "Nombre del alumno"
        },
        "l-indicacao-fornecedor": {
            "pt": "Código do fornecedor",
			"en": "Supplier's code",
			"es": "Código del proveedor"
        },
        "l-indicacao-fornecedor-nome": {
            "pt": "Nome do fornecedor",
			"en": "Supplier's name",
			"es": "Nombre del proveedor"
        },
        "l-indicacao-funcionario-professor": {
            "pt": "Chapa de indicação do(a) funcionário(a) / professor(a)",
			"en": "Employee Registration Number indicating employee/teacher",
			"es": "Matrícula de indicación del empleado / profesor"
        },
        "l-indicacao-funcionario-professor-nome": {
            "pt": "Nome do(a) funcionário(a) / professor(a)",
			"en": "Employee/teacher's name",
			"es": "Nombre del empleado / profesor"
        },
        "l-msg-aluno-nao-encontrado": {
            "pt": "O registro acadêmico do(a) Aluno não foi encontrado!",
			"en": "Student's academic record not found!",
			"es": "¡No se encontró el registro académico del Alumno!"
        },
        "l-msg-fornecedor-nao-encontrado": {
            "pt": "O código do fornecedor não foi encontrado!",
			"en": "Supplier's code not found",
			"es": "¡No se encontró el código del proveedor!"
        },
        "l-msg-funcionario-nao-encontrado": {
            "pt": "A chapa do(a) funcionário(a) / professor(a) não foi encontrado(a)!",
			"en": "Employee registration number of teacher/employee not found!",
			"es": "¡No se encontró la matrícula del empleado / profesor!"
        },
        "l-documentos-registrogeral": {
            "pt": "Registro geral de estrangeiros",
            "en": "General record of foreigners",
            "es": "Registro general de extranjeros"
        },
        "l-tipodocumento":{
            "pt": "Tipo de documento",
			"en": "Type of document",
			"es": "Tipo de documento"
        },
        "l-tipodocumento-candidato":{
            "pt": "Tipo de documento do candidato",
			"en": "Applicant's type of document",
			"es": "Tipo de documento del candidato"
        },
        "l-tipodocumento-responsavel":{
            "pt": "Tipo de documento do responsável",
			"en": "Type of document of person in charge",
			"es": "Tipo de documento del responsable"
        },
        "l-msg-confirmacao-utilizar-informacoes-candidato":{
            "pt": "Obrigado por finalizar a primeira parte de sua inscrição! Agora na próxima etapa insira os dados solicitados para concluí-la! Seja bem-vindo ao Grupo Unis!",
			"en": "Information found in previous registers, you will be redirected to log in and reuse data previously registered.",
			"es": "Se encontró información sobre registros anteriores, vamos a redireccionarlo para que realice el login y reaproveche los datos ya registrados."
        },
        "l-candidato":{
            "pt": "Candidato",
			"en": "Applicant",
			"es": "Candidato"
        },
        "l-pai":{
            "pt": "Pai",
			"en": "Father",
			"es": "Padre"
        },
        "l-mae":{
            "pt": "Mãe",
			"en": "Mother",
			"es": "Madre"
        },
        "l-resp-fin":{
            "pt": "Responsável Financeiro",
			"en": "Financials person in charge",
			"es": "Responsable financiero"
        } ,
        "l-utilizar-outro-usuario-como-respFin":{
            "pt": "Utilizar o usuário selecionado como responsável financeiro:",
			"en": "Use selected user as the person in charge of Financials:",
			"es": "Utilice el usuario seleccionado como responsable financiero:"
        },
        "l-utilizar-outro-usuario-como-respAcad":{
            "pt": "Utilizar o usuário selecionado como responsável acadêmico:",
			"en": "Use selected user as the person in charge of Academics:",
			"es": "Utilice el usuario seleccionado como responsable académico:"
        },
        "l-dados-basicos-candidato":{
            "pt": "Dados básicos (candidato)",
			"en": "Basic data (applicant)",
			"es": "Datos básicos (candidato)"
        },
        "l-documentos-candidato":{
            "pt": "Documentos (candidato)",
			"en": "Documents (applicant)",
			"es": "Documentos (candidato)"
        },
        "l-endereco-candidato":{
            "pt": "Endereço (candidato)",
			"en": "Address (applicant)",
			"es": "Dirección (candidato)"
        },
        "l-informacoes-adicionais-candidato":{
            "pt": "Informações adicionais (candidato)",
			"en": "Additional information (applicant)",
			"es": "Información adicional (candidato)"
        },
        "l-documentos-passaporte-candidato":{
            "pt": "Passaporte (candidato)",
			"en": "Passport (applicant)",
			"es": "Pasaporte (candidato)"
        },
        "l-dados-gerais-estrangeiros-candidato":{
            "pt": "Dados gerais de estrangeiros (candidato)",
			"en": "General data of foreigners (applicant)",
			"es": "Datos generales de extranjeros (candidato)"
        },
        "l-dados-basicos-pai":{
            "pt": "Dados básicos (pai)",
			"en": "Basic data (father)",
			"es": "Datos básicos (padre)"
        },
        "l-documentos-pai":{
            "pt": "Documentos (pai)",
			"en": "Documents (father)",
			"es": "Documentos (padre)"
        },
        "l-endereco-pai":{
            "pt": "Endereço (pai)",
			"en": "Address (father)",
			"es": "Dirección (padre)"
        },
        "l-informacoes-adicionais-pai":{
            "pt": "Informações adicionais (pai)",
			"en": "Additonal information (father)",
			"es": "Información adicional (padre)"
        },
        "l-documentos-passaporte-pai":{
            "pt": "Passaporte (pai)",
			"en": "Passport (father)",
			"es": "Pasaporte (padre)"
        },
        "l-dados-gerais-estrangeiros-pai":{
            "pt": "Dados gerais de estrangeiros (pai)",
			"en": "General data of foreigners (father)",
			"es": "Datos generales de extranjeros (padre)"
        },
        "l-dados-basicos-mae":{
            "pt": "Dados básicos (mãe)",
			"en": "Basic data (mother)",
			"es": "Datos básicos (madre)"
        },
        "l-documentos-mae":{
            "pt": "Documentos (mãe)",
			"en": "Documents (mother)",
			"es": "Documentos (madre)"
        },
        "l-endereco-mae":{
            "pt": "Endereço (mãe)",
			"en": "Address (mother)",
			"es": "Dirección (madre)"
        },
        "l-informacoes-adicionais-mae":{
            "pt": "Informações adicionais (mãe)",
			"en": "Additional information (mother)",
			"es": "Información adicional (madre)"
        },
        "l-documentos-passaporte-mae":{
            "pt": "Passaporte (mãe)",
			"en": "Passport (mother)",
			"es": "Pasaporte (madre)"
        },
        "l-dados-gerais-estrangeiros-mae":{
            "pt": "Dados gerais de estrangeiros (mãe)",
			"en": "General data of foreigners (mother)",
			"es": "Datos generales de extranjeros (madre)"
        },
        "l-dados-basicos-responsavelacademico":{
            "pt": "Dados básicos (responsável acadêmico)",
			"en": "Basic data (person in charge of Academics)",
			"es": "Datos básicos (responsable académico)"
        },
        "l-documentos-responsavelacademico":{
            "pt": "Documentos (responsável acadêmico)",
			"en": "Documents (person in charge of Academics)",
			"es": "Documentos (responsable académico)"
        },
        "l-endereco-responsavelacademico":{
            "pt": "Endereço (responsável acadêmico)",
			"en": "Address (person in charge of Academics)",
			"es": "Dirección (responsable académico)"
        },
        "l-informacoes-adicionais-responsavelacademico":{
            "pt": "Informações adicionais (responsável acadêmico)",
			"en": "Additiona information (person in charge of Academics)",
			"es": "Información adicional (responsable académico)"
        },
        "l-documentos-passaporte-responsavelacademico":{
            "pt": "Passaporte (responsável acadêmico)",
			"en": "Passport (person in charge of Academics)",
			"es": "Pasaporte (responsable académico)"
        },
        "l-dados-gerais-estrangeiros-responsavelacademico":{
            "pt": "Dados gerais de estrangeiros (responsável acadêmico)",
			"en": "General data of foreigners (person in charge of Academics)",
			"es": "Datos generales de extranjeros (responsable académico)"
        },
        "l-dados-basicos-responsavelfinanceiro":{
            "pt": "Dados básicos (responsável financeiro)",
			"en": "Basic data (person in charge of Financials)",
			"es": "Datos básicos (responsable financiero)"
        },
        "l-documentos-responsavelfinanceiro":{
            "pt": "Documentos (responsável financeiro)",
			"en": "Documents (person in charge of Financials)",
			"es": "Documentos (responsable financiero)"
        },
        "l-endereco-responsavelfinanceiro":{
            "pt": "Endereço (responsável financeiro)",
			"en": "Address (person in charge of Financials)",
			"es": "Dirección (responsable financiero)"
        },
        "l-informacoes-adicionais-responsavelfinanceiro":{
            "pt": "Informações adicionais (responsável financeiro)",
			"en": "Additional information (person in charge of Financials)",
			"es": "Información adicional (responsable financiero)"
        },
        "l-documentos-passaporte-responsavelfinanceiro":{
            "pt": "Passaporte (responsável financeiro)",
			"en": "Passport (person in charge of Financials)",
			"es": "Pasaporte (responsable financiero)"
        },
        "l-dados-gerais-estrangeiros-responsavelfinanceiro":{
            "pt": "Dados gerais de estrangeiros (responsável financeiro)",
			"en": "General data of foreigners (person in charge of Financials)",
			"es": "Datos generales de extranjeros (responsable financiero)"
        },
        "l-mensagem-confirmacao-1":{
            "pt": "Confira a seguir os dados da inscrição e caso não seja necessário realizar nenhuma alteração clique no botão 'Li e aceito os termos e condições' para finalizar sua inscrição.",
			"en": "Check the following registration data, in case you do not need to change anything, click button 'I read and accept all terms and conditions' to finish registration.",
			"es": "A continuación, verifique los datos de la inscripción y si no fuera necesario realizar alguna modificación, haga clic en el botón 'Leí y acepto los términos y condiciones' para finalizar su inscripción."
        },
        "l-mensagem-confirmacao-2":{
            "pt": "Para ocultar/exibir os grupos de informação basta clicar sobre o cabeçalho do grupo desejado.",
			"en": "To hide/display information groups, click header of desired group.",
			"es": "Para ocultar/mostrar los grupos de información basta hacer clic en el encabezado del grupo deseado."
        },
        "l-titulo-confirmacao-inscricao":{
            "pt": "Confirmação dos dados da inscrição",
			"en": "Confirmation of registration data",
			"es": "Confirmación de los datos de la inscripción"
        },
        "l-nao":{
            "pt": "Não",
			"en": "No",
			"es": "No"
        },
        "l-sim":{
            "pt": "Sim",
			"en": "Yes",
			"es": "Sí"
        },
        "l-descricao":{
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "l-subdeficiencias-auditiva":{
            "pt": "Subdeficiências (tipo: auditiva)",
			"en": "Sub-disabilities (type: hearing)",
			"es": "Subdiscapacidades (tipo: auditiva)"
        },
        "l-subdeficiencias-fisica":{
            "pt": "Subdeficiências (tipo: física)",
			"en": "Sub-disabilities (type: physical)",
			"es": "Subdiscapacidades (tipo: física)"
        },
        "l-subdeficiencias-visual":{
            "pt": "Subdeficiências (tipo: visual)",
			"en": "Sub-disability (type: visual)",
			"es": "Subdiscapacidades (tipo: visual)"
		},
		"l-subdeficiencias-fala":{
            "pt": "Subdeficiências (tipo: fala)",
			"en": "Sub-disability (type: speech)",
			"es": "Subdiscapacidades (tipo: habla)"
		},
		"l-subdeficiencias-mental":{
            "pt": "Subdeficiências (tipo: mental)",
			"en": "Sub-disabilities (type: mental)",
			"es": "Subdiscapacidades (tipo: mental)"
		},
		"l-subdeficiencias-intelectual":{
            "pt": "Subdeficiências (tipo: intelectual)",
			"en": "Sub-disabilities (type: intellectual)",
			"es": "Subdiscapacidades (tipo: intelectual)"
		},
		"l-subdeficiencias-reabilitado":{
            "pt": "Subdeficiências (tipo: reabilitado)",
			"en": "Sub-disability (type:re-habited)",
			"es": "Subdiscapacidades (tipo: rehabilitado)"
        },
        "l-no-data":{
            "pt": "Nenhuma informação foi encontrada",
			"en": "No information was found",
			"es": "No se encontró ninguna información"
        },
        "l-sitmilitar-alistado":{
            "pt": "Alistado",
			"en": "Enlisted",
			"es": "Enlistado"
        },
        "l-sitmilitar-dispensado":{
            "pt": "Dispensado",
			"en": "Discharged",
			"es": "Dispensado"
        },
        "l-sitmilitar-reservista":{
            "pt": "Reservista",
			"en": "Military reserve member",
			"es": "Reservista"
        },
        "l-dados-responsavelfinanceiro":{
            "pt": "Responsável financeiro",
			"en": "Person in charge of Financials",
			"es": "Responsable financiero"
        },
        "l-dados-responsavelacademico":{
            "pt": "Responsável acadêmico",
			"en": "Person in charge of Academics",
			"es": "Responsable académico"
        },
        "l-respfinanceiro-pai": {
            "pt": "O pai do candidato foi definido como responsável financeiro.",
			"en": "The applicant's father was defined as the person in charge of Financials.",
			"es": "El padre del candidato fue definido como responsable financiero."
        },
        "l-respfinanceiro-mae": {
            "pt": "A mãe do candidato foi definida como a responsável financeira.",
			"en": "The applicant's mother was defined as the person in charge of Financials.",
			"es": "La madre del candidato fue definida como responsable financiera."
        },
        "l-respfinanceiro-candidato": {
            "pt": "O próprio candidato foi definido como responsável financeiro.",
			"en": "The own applicant was defined as the person in charge of Financials.",
			"es": "El propio candidato fue definido como responsable financiero."
        },
        "l-respacademico-pai": {
            "pt": "O pai do candidato foi definido como responsável acadêmico.",
			"en": "The applicant's father was defined as the person in charge of Academics.",
			"es": "El padre del candidato fue definido como responsable académico."
        },
        "l-respacademico-mae": {
            "pt": "A mãe do candidato foi definida como responsável acadêmica.",
			"en": "The applicant's mother was defined as the person in charge of Academics.",
			"es": "La madre del candidato fue definida como responsable académica."
        },
        "l-respacademico-candidato": {
            "pt": "O próprio candidato foi definido como responsável acadêmico.",
			"en": "The own applicant was defined as the person in charge of Academics.",
			"es": "El propio candidato fue definido como responsable académico."
        },
        "l-respacademico-financeiro": {
            "pt": "O responsável financeiro foi definido como responsável acadêmico.",
			"en": "The person in charge of Financials was defined as the person in charge of Academics.",
			"es": "El responsable financiero fue definido como responsable académico."
        },
        "l-mensagem-aguarde-geracao-boleto": {
            "pt": "Aguarde enquanto o boleto é gerado.",
			"en": "Wait while bank bill is generated.",
			"es": "Espere mientras se genera la boleta."
        },
        "l-primeiro-nome":{
            "pt": "Primeiro nome",
			"en": "First name",
			"es": "Primer nombre"
        },
        "l-primeiro-nome-candidato":{
            "pt": "Primeiro nome do candidato",
			"en": "Applicant's firts name",
			"es": "Primer nombre del candidato"
        },
        "l-servico-altera-dados": {
            "pt": "Acesse a Central do Candidato para atualizar o seu cadastro.",
			"en": "Access the Applicant's Central to update register.",
			"es": "Acceda a la Central del candidato para actualizar su registro."
        },
        "l-servico-altera-senha": {
            "pt": "Acesse a Central do Candidato para alterar a sua senha de acesso.",
			"en": "Access Applicant's Central to change your access password.",
			"es": "Acceda a la Central del candidato para modificar su contraseña de acceso."
        },
        "l-servico-boleto-inscricao": {
            "pt": "Acesse a Central do Candidato para gerar uma nova via do boleto e não perder o prazo de pagamento da taxa de inscrição.",
			"en": "Access Applicant's Central to generate a new copy of the bank bill and do not miss the deadline to pay the registration rate.",
			"es": "Acceda a la Central del candidato para generar un nuevo ejemplar de la boleta y no perder el plazo de pago de la tasa de inscripción."
        },
        "l-servico-cartao-inscricao": {
            "pt": "Acesse a Central do Candidato para realizar o pagamento da taxa de inscrição via cartão de crédito.",
			"en": "Access Applicant's Central to pay the registration rate through credit card.",
			"es": "Acceda a la Central del candidato para realizar el pago de la tasa de inscripción por medio de la tarjeta de crédito."
        },
        "l-servico-comprovante-inscricao": {
            "pt": "Acesse a Central do Candidato para imprimir a segunda via do comprovante de inscrição.",
			"en": "Access the Applicant's Central to print the second copy of the registration receipt.",
			"es": "Acceda a la Central del candidato para imprimir el segundo ejemplar del comprobante de inscripción."
        },
        "l-servico-status": {
            "pt": "Acesse a Central do Candidato para acompanhar o seu status e andamento das etapas no Processo Seletivo.",
			"en": "Access the Applicant's Central to follow progress status of phases in the Selective Process.",
			"es": "Acceda a la Central del candidato para acompañar su estatus y progreso en las etapas del Proceso de selección."
        },
        "l-editar-inscricao": {
            "pt": "Editar dados pessoais",
			"en": "Edit personal data",
			"es": "Editar datos personales"
        },
        "l-alterar-senha": {
            "pt": "Alterar senha",
			"en": "Edit password",
			"es": "Modificar contraseña"
        },
        "l-pagamento-boleto": {
            "pt": "Pagamento boleto",
			"en": "Payment bank bill",
			"es": "Pago boleta"
        },
        "l-pagamento-cartao": {
            "pt": "Pagamento cartão",
			"en": "Payment card",
			"es": "Pago tarjeta"
        },
        "l-2a-via": {
            "pt": "2ª via comprovante",
			"en": "2nd copy receipt",
			"es": "2º ejemplar comprobante"
        },
        "l-checar-status": {
            "pt": "Checar status",
			"en": "Check status",
			"es": "Verificar estatus"
        },
        "l-atividades-agendadas-capslock": {
            "pt": "ATIVIDADES AGENDADAS",
			"en": "SCHEDULED ACTIVITIES",
			"es": "ACTIVIDADES PROGRAMADAS EN AGENDA"
        },
        "l-atividades-agendadas": {
            "pt": "Atividades Agendadas",
			"en": "Scheduled Activities",
			"es": "Actividades programadas en agenda"
        },
        "l-informe-atividades": {
            "pt": "Informe o(s) horário(s)",
			"en": "Enter schedule(s)",
			"es": "Informe el/los horario(s)"
        },
        "l-campos-obrigatorios-atividades": {
            "pt": "Os campos de atividades agendadas devem ser preenchidos",
			"en": "Enter the fields of scheduled activities",
			"es": "Los campos de actividades programadas en agenda deben completarse"
        },
        "l-atividade-sem-horario-disponivel": {
            "pt": "Prezado(a) candidato(a), não há mais disponibilidade de datas e horários para que você possa realizar na instituição, as atividades agendadas do processo seletivo escolhido. Sendo assim sua inscrição não poderá ser realizada. Qualquer dúvida, favor entrar em contato conosco.",
			"en": "Dear applicant, there is no availability of dates and times in the Institution, the scheduled activities of the selective process chosen. So your registration cannot be performed. If you have any questions, contact us.",
			"es": "Estimado(a) candidato(a), ya no hay fechas ni horarios disponibles para que pueda realizar en la institución, las actividades programadas en agenda sobre el proceso de selección seleccionado. Por ello, no podrá realizar su inscripción. Cualquier duda, por favor, entre en contacto con nosotros." 
        },
        "l-msg-inscricao-excedente": {
            "pt": "Prezado(a) candidato(a), não existem mais vagas disponíveis para esta área ofertada. Porém, você pode se inscrever como excedente. Deseja prosseguir com a inscrição e ficar como [[NUMEXCEDENTE]]º excedente?",
			"en": "Dear applicant, there is no availability for this area offered. However, you can register as an exceeding student. Proceed with registration and remain as [[NUMEXCEDENTE]]º exceeding student?",
			"es": "Estimado(a) candidato(a), ya no existen más vacantes disponibles para el área ofrecida. Sin embargo, puede inscribirse como excedente. ¿Desea continuar con la inscripción y permanecer como [[NUMEXCEDENTE]]º excedente?"
        },
        "l-msg-confirmacao-vaga-excedente": {
            "pt": "Prezado(a) candidato(a), você está se inscrevendo como excedente na área de interesse escolhida. Caso hajam desistências e/ou surjam novas vagas entraremos em contato e o boleto será gerado caso exista taxa de inscrição.",
			"en": "Dear applicant, you are registering as exceeding student in the interest area chosen. In case of dropout, or if new vacancies come up, we will contact you and the bank bill is generated if there is registration rate.",
			"es": "Estimado(a) candidato(a), está inscribiéndose como excedente en el área de su interés. Si alguien desistiera y/o surgieran nuevas vacantes entraremos en contacto y se generará una boleta si existiera alguna tasa de inscripción."
        },
        "l-msg-confirmacao-vaga-excedente-inscreveu": {
            "pt": "Prezado(a) candidato(a), você se inscreveu como excedente na área de interesse escolhida. Caso hajam desistências e/ou surjam novas vagas entraremos em contato e o boleto será gerado caso exista taxa de inscrição.",
			"en": "Dear applicant, you are registered as exceeding student in the interest area chosen. In case of dropout, or if new vacancies come up, we will contact you and the bank bill is generated if there is registration rate.",
			"es": "Estimado(a) candidato(a), usted se inscribió como excedente en el área de su interés. Si alguien desistiera y/o surgieran nuevas vacantes entraremos en contacto y se generará una boleta si existiera alguna tasa de inscripción."
        },
        "l-como-excedente": {
            "pt": " como excedente",
			"en": " as exceeding student",
			"es": " como excedente"
        },
        "l-acesso-rapido-voce-candidato": {
            "pt": "Para você que já é candidato",
			"en": "For you who is already an applicant",
			"es": "Para usted que ya es candidato"
        },
        "l-sou-candidato": {
            "pt": "Sou o candidato dessa inscrição?",
			"en": "Am I the applicant of this registration?",
			"es": "Para usted que ya es candidato"
        },
        "l-sou-candidato-sim": {
            "pt": "SIM",
			"en": "YES",
			"es": "SÍ"
        },
        "l-sou-candidato-nao": {
            "pt": "NÃO",
			"en": "NO",
			"es": "NO"
        },
        "l-dados-resp-inscricao": {
            "pt": "DADOS DO RESPONSÁVEL INSCRIÇÃO",
			"en": "DATA OF THE REGISTRATION PERSON IN CHARGE",
			"es": "DATOS DEL RESPONSABLE INSCRIPCIÓN"
        },
        "l-msg-label-nome-resp-insc": {
            "pt": "Nome responsável inscrição",
			"en": "Name registration person in charge",
			"es": "Nombre responsable inscripción"
        },
        "l-msg-label-data-nascimento-resp-insc": {
            "pt": "Data nascimento responsável inscrição",
			"en": "Birth date registration person in charge",
			"es": "Fecha nacimiento responsable inscripción"
        },
        "l-msg-label-nome-social-resp-insc": {
            "pt": "Nome social responsável inscrição",
			"en": "Social name registration person in charge",
			"es": "Nombre social responsable inscripción"
        },
        "l-msg-label-apelido-resp-insc": {
            "pt": "Apelido responsável inscrição",
			"en": "Nickname registration person in charge",
			"es": "Alias responsable inscripción"
        },
        "l-msg-label-estado-civil-resp-insc": {
            "pt": "Estado civil responsável inscrição",
			"en": "Marital status registration person in charge",
			"es": "Estado civil responsable inscripción"
        },
        "l-msg-label-nacionalidade-resp-insc": {
            "pt": "Nacionalidade responsável inscrição",
			"en": "Nationality registration person in charge",
			"es": "Nacionalidad responsable inscripción"
        },
        "l-msg-label-email-resp-insc": {
            "pt": "E-mail responsável inscrição",
			"en": "E-mail registration person in charge",
			"es": "E-mail responsable inscripción"
        },
        "l-msg-label-sexo-resp-insc": {
            "pt": "Sexo responsável inscrição",
			"en": "Gender registration person in charge",
			"es": "Sexo responsable inscripción"
        },
        "l-msg-label-canhoto-resp-insc": {
            "pt": "Candidato canhoto",
			"en": "Left-handed applicant",
			"es": "Candidato zurdo"
        },
        "l-msg-label-fumante-resp-insc": {
            "pt": "Candidato fumante",
			"en": "Smoker applicant",
			"es": "Candidato fumador"
        },
        "l-msg-label-numero-cpf-resp-insc":{
            "pt": "Número do CPF responsável inscrição",
			"en": "CPF number registration person in charge",
			"es": "Número del RCPF responsable inscripción"
        },
        "l-msg-label-numero-rg-resp-insc":{
            "pt": "Número do RG responsável inscrição",
			"en": "ID number registration person in charge",
			"es": "Número de DI responsable inscripción"
        },
        "l-msg-label-data-emissao-rg-resp-insc":{
            "pt": "Data emissão RG responsável inscrição",
			"en": "ID issue date registration person in charge",
			"es": "Fecha de emisión DI responsable inscripción"
        },
        "l-msg-label-orgao-emissor-rg-resp-insc":{
            "pt": "Órgão emissor RG responsável inscrição",
			"en": "ID issuing body registration person in charge",
			"es": "Organismo emisor DI responsable inscripción"
        },
        "l-msg-label-numero-registro-profossional-resp-insc":{
            "pt": "Número registro profissional responsável inscrição",
			"en": "Professional record number registration person in charge",
			"es": "Número registro profesional responsable inscripción"
        },
        "l-msg-label-numero-titulo-eleitor-resp-insc":{
            "pt": "Número do título de eleitor responsável inscrição",
			"en": "Voter registration number registration person in charge",
			"es": "Número de credencial de elector responsable inscripción"
        },
        "l-msg-label-zona-titulo-eleitor-resp-insc":{
            "pt": "Zona título eleitor responsável inscrição",
			"en": "Voter registration zone registration person in charge",
			"es": "Zona credencial elector responsable inscripción"
        },
        "l-msg-label-secao-titulo-eleitor-resp-insc":{
            "pt": "Seção título eleitor candidato",
			"en": "Applicant's voter registration section",
			"es": "Sección credencial elector candidato"
        },
        "l-msg-label-data-emissao-titulo-eleitor-resp-insc":{
            "pt": "Data emissão título eleitor responsável inscrição",
			"en": "Voter registration issue date registration person in charge",
			"es": "Fecha emisión credencial elector responsable inscripción"
        },
        "l-msg-label-numero-carteira-trabalho-resp-insc":{
            "pt": "Número carteira trabalho responsável inscrição",
			"en": "Employee card number registration person in charge",
			"es": "Número libreta de trabajo responsable inscripción"
        },
        "l-msg-label-serie-carteira-trabalho-resp-insc":{
            "pt": "Série carteira trabalho responsável inscrição",
			"en": "Employee card series registration person in charge",
			"es": "Serie libreta de trabajo responsable inscripción"
        },
        "l-msg-label-data-emissao-carteira-trabalho-resp-insc":{
            "pt": "Data emissão carteira trabalho responsável inscrição",
			"en": "Employee card issue date registration person in charge",
			"es": "Fecha emisión libreta de trabajo responsable inscripción"
        },
        "l-msg-label-carteira-trabalho-tipo-nit-resp-insc":{
            "pt": "Carteira trabalho tipo NIT responsável inscrição",
			"en": "Employee card NIT type registration person in charge",
			"es": "Libreta de trabajo tipo NIT responsable inscripción"
        },
        "l-msg-label-carteira-motorista-resp-insc":{
            "pt": "Carteira motorista responsável inscrição",
			"en": "Driver's license registration person in charge",
			"es": "Licencia de conducir responsable inscripción"
        },
        "l-msg-label-tipo-carteira-motorista-resp-insc":{
            "pt": "Tipo carteira motorista responsável inscrição",
			"en": "Driver's license type registration person in charge",
			"es": "Tipo licencia de conducir responsable inscripción"
        },
        "l-msg-label-data-vencimento-carteira-motorista-resp-insc":{
            "pt": "Data vencimento carteira motorista responsável inscrição",
			"en": "Driver's license expiration date registration person in charge",
			"es": "Fecha de vencimiento licencia de conducir responsable inscripción"
        },
        "l-msg-label-numero-cert-reservista-resp-insc":{
            "pt": "Número certificado reservista responsável inscrição",
			"en": "Military discharge certificate number registration person in charge",
			"es": "Número libreta militar de reservista responsable inscripción"
        },
        "l-msg-label-categoria-militar-resp-insc":{
            "pt": "Categoria militar responsável inscrição",
			"en": "Military category registration person in charge",
			"es": "Categoría militar responsable inscripción"
        },
        "l-msg-label-circunscricao-militar-resp-insc":{
            "pt": "Circunscrição militar responsável inscrição",
			"en": "Military circuit registration person in charge",
			"es": "Circunscripción militar responsable inscripción"
        },
        "l-msg-label-regiao-militar-resp-insc":{
            "pt": "Região militar responsável inscrição",
			"en": "Military region registration person in charge",
			"es": "Región militar responsable inscripción"
        },
        "l-msg-label-data-emissao-cert-reservista-resp-insc":{
            "pt": "Data emissão certificado reservista responsável inscrição",
			"en": "Military discharge certificfate issue date registration person in charge",
			"es": "Fecha de emisión libreta militar de reservista responsable inscripción"
        },
        "l-msg-label-orgao-emissor-cert-reservista-resp-insc":{
            "pt": "Órgão emissor certificado reservista responsável inscrição",
			"en": "Military discharge certificate issuing body registration person in charge",
			"es": "Organismo emisor libreta militar de reservista responsable inscripción"
        },
        "l-msg-label-situacao-militar-resp-insc":{
            "pt": "Situação militar",
			"en": "Military status",
			"es": "Situación militar"
        },
        "l-msg-label-cep-resp-insc":{
            "pt": "CEP responsável inscrição",
			"en": "Postal code registration person in charge",
			"es": "CP responsable inscripción"
        },
        "l-msg-label-tipo-rua-resp-insc":{
            "pt": "Tipo rua responsável inscrição",
			"en": "Type of street registration person in charge",
			"es": "Tipo calle responsable inscripción"
        },
        "l-msg-label-rua-resp-insc":{
            "pt": "Rua responsável inscrição",
			"en": "Street registration person in charge",
			"es": "Calle responsable inscripción"
        },
        "l-msg-label-numero-resp-insc":{
            "pt": "Número responsável inscrição",
			"en": "Number registration person in charge",
			"es": "Número responsable inscripción"
        },
        "l-msg-label-complemento-resp-insc":{
            "pt": "Complemento responsável inscrição",
			"en": "Address line 2 registration person in charge",
			"es": "Complemento responsable inscripción"
        },
        "l-msg-label-tipo-bairro-resp-insc":{
            "pt": "Tipo bairro responsável inscrição",
			"en": "Type of district registration person in charge",
			"es": "Tipo barrio responsable inscripción"
        },
        "l-msg-label-bairro-resp-insc":{
            "pt": "Bairro responsável inscrição",
			"en": "Disctrict registration person in charge",
			"es": "Barrio responsable inscripción"
        },
        "l-msg-label-pais-resp-insc":{
            "pt": "País responsável inscrição",
			"en": "Country registration person in charge",
			"es": "País responsable inscripción"
        },
        "l-msg-label-uf-resp-insc":{
            "pt": "UF responsável inscrição",
			"en": "State registration person in charge",
			"es": "Est/Prov/Reg responsable inscripción"
        },
        "l-msg-label-telefone-residencial-resp-insc":{
            "pt": "Telefone residencial responsável inscrição",
			"en": "Home phone number registration person in charge",
			"es": "Teléfono residencial responsable inscripción"
        },
        "l-msg-label-telefone-calular-resp-insc":{
            "pt": "Telefone celular responsável inscrição",
			"en": "Mobile phone number registration person in charge",
			"es": "Teléfono celular responsable inscripción"
        },
        "l-msg-label-telefone-comercial-resp-insc":{
            "pt": "Telefone comercial responsável inscrição",
			"en": "Business phone number registration person in charge",
			"es": "Teléfono comercial responsable inscripción"
        },
        "l-msg-label-fax-resp-insc":{
            "pt": "Fax responsável inscrição",
			"en": "Fax number registration person in charge",
			"es": "Fax responsable inscripción"
         },
         "l-msg-label-cor-raca-resp-insc":{
             "pt": "Cor/raça responsável inscrição",
			 "en": "Ethnic group registration person in charge",
			 "es": "Color/etnia responsable inscripción"
         },
         "l-msg-label-pofissao-resp-insc":{
             "pt": "Profissão responsável inscrição",
			 "en": "Profession registration person in charge",
			 "es": "Profesión responsable inscripción"
         },
         "l-msg-label-tipo-sanguineo-resp-insc":{
             "pt": "Tipo sanguíneo responsável inscrição",
			 "en": "Blood type registration person in charge",
			 "es": "Tipo sanguíneo responsable inscripción"
         },
         "l-msg-label-numero-passaporte-resp-insc":{
             "pt": "Número passaporte responsável inscrição",
			 "en": "Passport number registration person in charge",
			 "es": "Número pasaporte responsable inscripción"
         },
         "l-msg-label-data-emissao-passaporte-resp-insc":{
             "pt": "Data emissão passaporte responsável inscrição",
			 "en": "Passport issue date registration person in charge",
			 "es": "Fecha de emisión pasaporte responsable inscripción"
         },
         "l-msg-label-data-validade-passaporte-resp-insc":{
             "pt": "Data validade passaporte responsável inscrição",
			 "en": "Passport validity date registration person in charge",
			 "es": "Fecha de validez pasaporte responsable inscripción"
         },
         "l-msg-label-pais-origem-passaporte-resp-insc":{
            "pt": "País origem passaporte responsável inscrição",
			"en": "Passport origin country registration person in charge",
			"es": "País origen pasaporte responsable inscripción"
         },
         "l-msg-label-data-chegada-brasil-resp-insc":{
             "pt": "Data chegada Brasil responsável inscrição",
			 "en": "Date of arrival to Brazil registration person in charge",
			 "es": "Fecha de llegada a Brasil responsable inscripción"
         },
         "l-msg-label-tipo-visto-resp-insc":{
             "pt": "Tipo visto responsável inscrição",
			 "en": "Type of visa registration person in charge",
			 "es": "Tipo visa responsable inscripción"
         },
         "l-msg-label-carteira-modelo-19-resp-insc":{
             "pt": "Carteira modelo 19 responsável inscrição",
			 "en": "Identification card model 19 registration person in charge",
			 "es": "Documento de identidad modelo 19 responsable inscripción"
         },
         "l-msg-label-numero-filhos-brasileiros-resp-insc":{
             "pt": "Número filhos brasileiros responsável inscrição",
			 "en": "Number of Brazilian children registration person in charge",
			 "es": "Número hijos brasileños responsable inscripción"
         },
         "l-msg-label-estrangeiro-naturalizado-resp-insc":{
             "pt": "Estrangeiro naturalizado responsável inscrição",
			 "en": "Foreigner naturalized registration person in charge",
			 "es": "Extranjero naturalizado responsable inscripción"
         },
         "l-msg-label-conjuge-brasil-resp-insc":{
             "pt": "Cônjuge Brasil responsável inscrição",
			 "en": "Spouse Brazil registration person in charge",
			 "es": "Cónyuge Brasil responsable inscripción"
         },
         "l-msg-label-registro-geral-resp-insc":{
             "pt": "Registro geral responsável inscrição",
			 "en": "General record registration person in charge",
			 "es": "Registro general responsable inscripción"
         },
         "l-msg-label-decreto-imigracao-resp-insc":{
             "pt": "Decreto imigração responsável inscrição",
			 "en": "Immigration decree registration person in charge",
			 "es": "Decreto inmigración responsable inscripción"
         },
         "l-msg-label-data-vencimento-ctps-resp-insc":{
             "pt": "Data vencimento carteira trabalho estrangeiro responsável inscrição",
			 "en": "Foreigner employee card expiration date registration person in charge",
			 "es": "Fecha de vencimiento libreta de trabajo extranjero responsable inscripción"
         },
         "l-msg-label-data-vencimento-rg-resp-insc":{
             "pt": "Data vencimento registro geral estrangeiros responsável inscrição",
			 "en": "Foreigners general record expiration date registration person in charge",
			 "es": "Fecha de vencimiento registro general extranjeros responsable inscripción"
         },
        "l-titulo-inscricoes": {
            "pt": "Inscrições",
			"en": "Registrations",
			"es": "Inscripciones"
        },
        "l-inscricao-sem-informar-responsavel": {
            "pt": "Favor informar se você é o candidato dessa inscrição.",
			"en": "Please, enter whether you are the applicant of this registration.",
			"es": "Por favor, informe si usted es el candidato de esta inscripción."
        },
        "l-utilizar-outro-usuario-como-pai":{
            "pt": "Utilizar o usuário selecionado como pai:",
			"en": "Use selected user as father:",
			"es": "Utilice el usuario seleccionado como padre:"
        },
        "l-utilizar-outro-usuario-como-mae":{
            "pt": "Utilizar o usuário selecionado como mãe:",
			"en": "Use selected user as mother:",
			"es": "Utilice el usuario seleccionado como madre:"
        },
        "l-resp-insc":{
            "pt": "Responsável Inscrição",
			"en": "Registration person in charge",
			"es": "Responsable inscripción"
        },
        "l-dados-basicos-respinsc": {
            "pt": "Dados básicos (responsável inscrição)",
			"en": "Basic data (registration person in charge)",
			"es": "Datos básicos (responsable inscripción)"
        },
        "l-documentos-respinsc": {
            "pt": "Documentos (responsável inscrição)",
			"en": "Documents (registration person in charge)",
			"es": "Documentos (responsable inscripción)"
        },
        "l-endereco-respinsc":{
            "pt": "Endereço (responsável inscrição)",
			"en": "Address (registration person in charge)",
			"es": "Dirección (responsable inscripción)"
        },
        "l-informacoes-adicionais-respinsc":{
            "pt": "Informações adicionais (responsável inscrição)",
			"en": "Additional information (registration person in charge)",
			"es": "Información adicional (responsable inscripción)"
        },
        "l-documentos-passaporte-respinsc":{
            "pt": "Passaporte (responsável inscrição)",
			"en": "Passport (registration person in charge)",
			"es": "Pasaporte (responsable inscripción)"
        },
        "l-dados-gerais-estrangeiros-respinsc":{
            "pt": "Dados gerais de estrangeiros (responsável inscrição)",
			"en": "General data of foreigners (registration person in charge)",
			"es": "Datos generales de estranjeros (responsable inscripción)"
        },
        "l-dados-pai-confirmacao": {
            "pt": "Pai",
			"en": "Father",
			"es": "Padre"
        },
        "l-dados-mae-confirmacao": {
            "pt": "Mãe",
			"en": "Mother",
			"es": "Madre"
        },
        "l-respinscricao-pai": {
            "pt": "O pai do candidato foi definido como responsável pela inscrição.",
			"en": "The applicant's father was defined as the person in charge of registration.",
			"es": "El padre del candidato fue definido como responsable de la inscripción."
        },
        "l-respinscricao-mae": {
            "pt": "A mãe do candidato foi definida como responsável pela inscrição.",
			"en": "The applicant's mother was defined as the person in charge of registration.",
			"es": "La madre del candidato fue definida como responsable de la inscripción."
        },
        "l-respfinanceiro-inscricao": {
            "pt": "O responsável financeiro foi definido como o responsável pela inscrição.",
			"en": "The person in charge of the Financials was defined as the person in charge of registration.",
			"es": "El responsable financiero fue definido como el responsable de la inscripción."
        },
        "l-respacademico-inscricao": {
            "pt": "O responsável acadêmico foi definido como o responsável pela inscrição.",
			"en": "The person in charge of the Academics was defined as the person in charge of registration.",
			"es": "El responsable académico fue definido como el responsable de la inscripción."
        },
        "l-btn-inscricao": {
            "pt": "Inscrever outro candidato",
			"en": "Register other applicant",
			"es": "Inscribir otro candidato"
        },
        "l-nome-responsavel": {
            "pt": "Nome do responsável",
			"en": "Name of person in charge",
			"es": "Nombre del responsable"
        },
        "l-primeiro-nome-responsavel": {
            "pt": "Primeiro nome do responsável",
			"en": "First name of the person in charge",
			"es": "Primer nombre del responsable"
        },
        "l-vagas-reservadas": {
            "pt": "Vagas Reservadas",
			"en": "Vacancies Reserved",
			"es": "Vacantes reservadas"
        },        
        "l-exige-sobrenome-usuario":{
            "pt": "Deve ser informado o sobrenome do candidato no campo Nome.",
			"en": "Enter the surname of the applicant in the field Name.",
			"es": "Debe informarse el apellido del candidato en el campo Nombre."
        },        
        "l-exige-sobrenome-pai":{
            "pt": "Deve ser informado o sobrenome do pai no campo Nome.",
			"en": "Enter surname of the father in the field Name.",
			"es": "Debe informarse el apellido del padre en el campo Nombre."
        },        
        "l-exige-sobrenome-mae":{
            "pt": "Deve ser informado o sobrenome da mãe no campo Nome.",
			"en": "Enter surname of the mother in the field Name.",
			"es": "Debe informarse el apellido de la madre en el campo Nombre."
        },        
        "l-exige-sobrenome-responsavel-financeiro":{
            "pt": "Deve ser informado o sobrenome do responsável financeiro no campo Nome.",
			"en": "Enter surname of the person in charge of the Financials in the field Name.",
			"es": "Debe informarse el apellido del responsable financiero en el campo Nombre."
        },        
        "l-exige-sobrenome-responsavel-academico":{
            "pt": "Deve ser informado o sobrenome do responsável acadêmico no campo Nome.",
			"en": "Enter surname of the person in charge of the Academics in the field Name.",
			"es": "Debe informarse el apellido del responsable académico en el campo Nombre."
        },       
        "l-exige-sobrenome-responsavel-inscricao":{
            "pt": "Deve ser informado o sobrenome do responsável da inscrição no campo Nome.",
			"en": "Enter surname of the person in charge of the registration in the field Name.",
			"es": "Debe informarse el apellido del responsable de la inscripción en el campo Nombre."
        },
        "l-fechar": {
            "pt": "Fechar"
        },

        "l-nome-obrigatorio": {
            "pt": "O campo nome é obrigatório"
        },
        "l-cpf-obrigatorio": {
            "pt": "O campo cpf é obrigatório"
        },
        "l-passaporte-obrigatorio": {
            "pt": "O campo passaporte é obrigatório"
        },
        "l-registrogeral-obrigatorio": {
            "pt": "O campo registro geral é obrigatório"
        },
        "l-email-obrigatorio": {
            "pt": "O campo e-mail é obrigatório"
        },
        "l-instituição-obrigatorio": {
            "pt": "O campo instituição é obrigatório"
        },
        "l-areainteresse-obrigatorio": {
            "pt": "O campo 1ª opção de curso é obrigatório"
        },
        "l-celular-obrigatorio": {
            "pt": "O campo telefone celular é obrigatório"
        },
        "l-datanascimento-obrigatorio": {
            "pt": "O campo data de nascimento é obrigatório"
        },
        "l-selecione-polo": {
            "pt": "Selecione o polo desejado"
        },    
        "l-envio-documentos": {
            "pt": "ENVIO DE DOCUMENTOS",
            "en": "DOCUMENTS DELIVERY",
            "es": "ENVÍO DE DOCUMENTOS"
        },
        "l-info-documentos": {
            "pt": "Documentos",
            "en": "Documents",
            "es": "Documentos"
        },
        "l-label-documentos": {
            "pt": "Enviar arquivo do documento",
            "en": "Send file of document",
            "es": "Enviar arquivo del documento"
        },
        "l-qtdarquivosfirst": {
            "pt": "Devem ser enviadas ",
            "en": "Devem ser enviadas ",
            "es": "Devem ser enviadas "
        },
        "l-qtdarquivosend": {
            "pt": "do documento: ",
            "en": "do documento: ",
            "es": "do documento: "
        },
        "l-erro-save": {
            "pt": "Erro ao salvar inscrição. Motivo: ",
            "en": "Error saving subscription. Reason: ",
            "es": "Error al guardar la inscripción. Razón: "
        },
        "l-dados-oferta-online": {
            "pt": "FORMA DE PAGAMENTO",
            "es": "FORMA DE PAGO",
            "en": "FORM OF PAYMENT"
        },
        "l-detalhe-forma-pag": {
            "pt": "Forma de pagamento",
            "en": "Form of payment",
            "es": "Forma de pago"
        },
        "l-forma_pagamento": {
            "pt": "Selecione a Forma de Pagamento",
            "es": "Seleccione la Forma de Pago",
            "en": "Select the Form of Payment" 
        },
        "l-titulo-matricula": {
            "pt": "Matrícula",
            "en": "Registration",
            "es": "Registro" 
        },
        "l-warning-matricula-pendente": {
            "pt": "Sua matrícula será analisada e confirmada posteriormente. Acompanhe através da central do candidato.",
            "en": "Your enrollment will be reviewed and confirmed later. Follow through the candidate's center.",
            "es": "Su matrícula será analizada y confirmada posteriormente. Acompañe a través de la central del candidato." 
        },
        "l-matric-pending": {
            "pt": "Matrícula aguardando pagamento para efetivação.",
            "en": "Matriculation awaiting payment to effect.",
            "es": "Matrícula aguardando pago para efectivización."
        },
        "l-matric-success": {
            "pt": "Sua matrícula no TOTVS Educacional foi realizada com sucesso.",
            "en": "Your enrollment in TOTVS Educacional was successful.",
            "es": "Su matrícula en el TOTVS Educacional fue realizada con éxito."
        },
        "l-detail-pag": {
            "pt": "Ver detalhes",
            "en": "Show details",
            "es": "Ver detalhes"
        },
        "l-forma_pagamento_sel": {
            "pt": "Forma de Pagamento selecionada",
            "es": "Forma de Pago selecionada",
            "en": "Selected Payment Form" 
        }
    }
]
