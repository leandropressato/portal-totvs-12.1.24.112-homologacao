/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsInscricoesModule
 * @name edupsInscricoesFactory
 * @object factory
 *
 * @created 14/09/2016 v12.1.15
 * @updated
 *
 * @requires edupsInscricoesModule
 *
 * @description Factory utilizada na funcionalidade Inscrições do Processo Seletivo.
 */
define(['inscricoes/inscricoes.module'], function() {

    'use strict';

    angular
        .module('edupsInscricoesModule')
        .factory('edupsInscricoesFactory', EdupsInscricoesFactory);

    EdupsInscricoesFactory.$inject = ['$totvsresource'];

    function EdupsInscricoesFactory($totvsresource) {

        var self = this,
            url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS;

        self.salvarDadosUsuario = salvarDadosUsuario;
		self.getDadosUsuario = getDadosUsuario;
		self.getDadosDependente = getDadosDependente;
        self.realizarInscricao = realizarInscricao;
        self.buscarTermoAceitePS = buscarTermoAceitePS;
        self.existeUsuario = existeUsuario;
        self.getInfoAreaInteresseDocumentAndOferta = getInfoAreaInteresseDocumentAndOferta;
        self.getEduParam = getEduParam;
        self.notificaInteracao = notificaInteracao;
        self.notificaInteracaoEvento = notificaInteracaoEvento;
        self.preparaDados = preparaDados;

        return self;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        //Retorna o objeto factory a partir da URL padrão juntamento ao complemeto
        function getInstanciaFactory(urlComplemento) {
            var urlCompletaServico = url + urlComplemento;
            return $totvsresource.REST(urlCompletaServico, {}, {});
        }

        /**
         * Busca os dados pessoais do usuário
         * @param {any} callback - função de retorno
         * @returns
         */
        function getDadosUsuario(callback) {
            var factory = getInstanciaFactory('/Inscricao/BuscaUsuario');
            return factory.TOTVSGet({}, callback);
		}

        /**
         * Busca os dados pessoais do dependente
         * @param {any} callback - função de retorno
         * @returns
         */
		function getDadosDependente(codUsuarioPSDependente, callback) {
			var parametros = {};
            parametros.idUsuarioDependente = codUsuarioPSDependente;
            var factory = getInstanciaFactory('/Inscricao/v1/BuscaDependente');
            return factory.TOTVSGet(parametros, callback);
		}

        function salvarDadosUsuario(model, callback) {
            var factory = getInstanciaFactory('/Inscricao/Usuario');
            return factory.TOTVSPost({}, model, callback);
        }

        function preparaDados(model)
        {
            //Anula objetos desnecessários para não ocorrer problema na conversão para dataSet
            if (angular.isDefined(model.SPSUSUARIO) && angular.isArray(model.SPSUSUARIO)) {
                for (var i = 0; i < model.SPSUSUARIO.length; i++) {
                    if (angular.isDefined(model.SPSUSUARIO[i].CORRACAOBJ)) {
                        model.SPSUSUARIO[i].CORRACAOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].ESTADOCIVILOBJ)) {
                        model.SPSUSUARIO[i].ESTADOCIVILOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].ESTADOS)) {
                        model.SPSUSUARIO[i].ESTADOS = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].ESTADOSCARTIDENT)) {
                        model.SPSUSUARIO[i].ESTADOSCARTIDENT = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].ESTADOSENDERECO)) {
                        model.SPSUSUARIO[i].ESTADOSENDERECO = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].ESTADOSMINI)) {
                        model.SPSUSUARIO[i].ESTADOSMINI = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].ESTELEITOBJ)) {
                        model.SPSUSUARIO[i].ESTELEITOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].GRAUINSTRUCAOOBJ)) {
                        model.SPSUSUARIO[i].GRAUINSTRUCAOOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].MUNICIPIOS)) {
                        model.SPSUSUARIO[i].MUNICIPIOS = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].MUNICIPIOSENDERECO)) {
                        model.SPSUSUARIO[i].MUNICIPIOSENDERECO = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].MUNICIPIOSMINI)) {
                        model.SPSUSUARIO[i].MUNICIPIOSMINI = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].NACIONALIDADEOBJ)) {
                        model.SPSUSUARIO[i].NACIONALIDADEOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PAISCARTTRABOBJ)) {
                        model.SPSUSUARIO[i].PAISCARTTRABOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PAISES)) {
                        model.SPSUSUARIO[i].PAISES = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PAISESCARTIDENT)) {
                        model.SPSUSUARIO[i].PAISESCARTIDENT = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PAISESENDERECO)) {
                        model.SPSUSUARIO[i].PAISESENDERECO = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PAISESMINI)) {
                        model.SPSUSUARIO[i].PAISESMINI = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PAISTELEITOBJ)) {
                        model.SPSUSUARIO[i].PAISTELEITOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].PROFISSAOOBJ)) {
                        model.SPSUSUARIO[i].PROFISSAOOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].TIPOBAIRROOBJ)) {
                        model.SPSUSUARIO[i].TIPOBAIRROOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].TIPORUAOBJ)) {
                        model.SPSUSUARIO[i].TIPORUAOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].TIPOSANGOBJ)) {
                        model.SPSUSUARIO[i].TIPOSANGOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].UFCARTTRABOBJ)) {
                        model.SPSUSUARIO[i].UFCARTTRABOBJ = null;
                    }
                    if (angular.isDefined(model.SPSUSUARIO[i].SITMILITAROBJ)) {
                        model.SPSUSUARIO[i].SITMILITAROBJ = null;
                    }
                }
            }

            if (angular.isDefined(model.SPSINSCRICAOAREAOFERTADA) && angular.isArray(model.SPSINSCRICAOAREAOFERTADA)) {
                for (var j = 0; j < model.SPSINSCRICAOAREAOFERTADA.length; j++) {
                    if (angular.isDefined(model.SPSINSCRICAOAREAOFERTADA[j].LocalProvaSelecionado)) {
                        model.SPSINSCRICAOAREAOFERTADA[j].LocalProvaSelecionado = null;
                    }
                    if (angular.isDefined(model.SPSINSCRICAOAREAOFERTADA[j].FORMAINSCRICAOOBJ)) {
                        model.SPSINSCRICAOAREAOFERTADA[j].FORMAINSCRICAOOBJ = null;
                    }
                    if (angular.isDefined(model.SPSINSCRICAOAREAOFERTADA[j].CAMPUSOBJ)) {
                        model.SPSINSCRICAOAREAOFERTADA[j].CAMPUSOBJ = null;
                    }
                    if (angular.isDefined(model.SPSINSCRICAOAREAOFERTADA[j].GRUPOPSOBJ)) {
                        model.SPSINSCRICAOAREAOFERTADA[j].GRUPOPSOBJ = null;
                    }
                    if (angular.isDefined(model.SPSINSCRICAOAREAOFERTADA[j].CotaInstituicaoFederal)) {
                        model.SPSINSCRICAOAREAOFERTADA[j].CotaInstituicaoFederal = null;
                    }
                }
            }

            if (angular.isDefined(model.SPSOPCAOINSCRITO) && angular.isArray(model.SPSOPCAOINSCRITO)) {
                for (var j = 0; j < model.SPSOPCAOINSCRITO.length; j++) {
                    if (angular.isDefined(model.SPSOPCAOINSCRITO[j].AREAINTERESSEOBJ)) {
                        model.SPSOPCAOINSCRITO[j].AREAINTERESSEOBJ = null;
                    }
                    if (angular.isDefined(model.SPSOPCAOINSCRITO[j].AREAINTERESSEOPCIONALOBJ)) {
                        model.SPSOPCAOINSCRITO[j].AREAINTERESSEOPCIONALOBJ = null;
                    }
                }
            }

            if (angular.isDefined(model.SPSIDIOMAINSCRITO) && angular.isArray(model.SPSIDIOMAINSCRITO)) {
                for (var j = 0; j < model.SPSIDIOMAINSCRITO.length; j++) {
                    if (angular.isDefined(model.SPSIDIOMAINSCRITO[j].IDIOMAOBJ)) {
                        model.SPSIDIOMAINSCRITO[j].IDIOMAOBJ = null;
                    }
                    if (angular.isDefined(model.SPSIDIOMAINSCRITO[j].IDIOMAOPCIONALOBJ)) {
                        model.SPSIDIOMAINSCRITO[j].IDIOMAOPCIONALOBJ = null;
                    }
                }
            }

            //Documentos exigidos
            if (angular.isDefined(model.SPSDOCUMENTOSEXIGIDOS) && angular.isArray(model.SPSDOCUMENTOSEXIGIDOS)) {
                for (var j = 0; j < model.SPSDOCUMENTOSEXIGIDOS.length; j++) {
                    if (angular.isDefined(model.SPSDOCUMENTOSEXIGIDOS[j].NOMEARQUIVO) && model.SPSDOCUMENTOSEXIGIDOS[j].NOMEARQUIVO.length < 1) {
                        model.SPSDOCUMENTOSEXIGIDOS[j].ARQUIVO = null;
                        model.SPSDOCUMENTOSEXIGIDOS[j].NOMEARQUIVO = null;
                    }
                }
            }

            return model;
        }

        function realizarInscricao(model, callback) {
            model = preparaDados(model);

            var factory = getInstanciaFactory('/Inscricao/NovaInscricao');
            return factory.TOTVSPost({}, model, callback);
        }        

        function notificaInteracao(model, callback) {
            model = preparaDados(model);
            var factory = getInstanciaFactory('/Inscricao/NotificaInteracao');
            return factory.TOTVSPost({}, model, callback);
        }

        function notificaInteracaoEvento(codEvento, callback) {

            var parametros = {};
            parametros.codEvento = codEvento;

            var factory = getInstanciaFactory('/Inscricao/NotificaInteracaoEvento');
            return factory.TOTVSPost(parametros, {}, callback);
        }

        function buscarTermoAceitePS(codColigada, idPS, idAreaInteresse, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.idAreaInteresse = idAreaInteresse;
            var factory = getInstanciaFactory('/TermoAceitePS/:codColigada/:idPS/:idAreaInteresse');

            return factory.TOTVSQuery(parametros, callback);
        }

        function existeUsuario(codColigada, idPS, nomeCandidato, dataNascimento, estadoNatal, cidadeNatal, cpf,
            passaporte, registroGeral, email, callback) {

            var codEtd = (angular.isDefined(estadoNatal) && estadoNatal !== null) ? estadoNatal.CODETD : null,
                codNaturalidade = (angular.isDefined(cidadeNatal) && cidadeNatal !== null) ? cidadeNatal.CODMUNICIPIO : null,

                factory = getInstanciaFactory('/Inscricao/ExisteUsuario'),
                parameters = {
                    codColigada: codColigada,
                    idps: idPS,
                    nome: nomeCandidato,
                    dataNascimento: dataNascimento,
                    estadoNatal: codEtd,
                    cidadeNatal: codNaturalidade,
                    cpf: cpf,
                    passaporte: passaporte,
                    registroGeral: registroGeral,
                    email: email
                };

            return factory.TOTVSGet(parameters, callback);
        }


         /**
         * @public
         * @function Retorna a lista de documentos exigidos do processo seletivo de acordo com a área de interesse selecionada
         * @name getInfoAreaInteresseDocumentAndOferta
         * @param   {int}    codColigada     Código da Coligada
         * @param   {int}    idPS            Identificador do Processo Seletivo
         * @param   {int}    idAreaInteresse Identificador da Área de Interesse
         * @callback Objeto 
         * @returns {object} Objeto com alista de documentos
         */
        function getInfoAreaInteresseDocumentAndOferta(codColigada, idPS, idAreaInteresse, callback) {
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.idAreaInteresse = idAreaInteresse;

            var factory = getInstanciaFactory('/InfoAreaOfertadaDocumentOferta/:codColigada/:idPS/:idAreaInteresse');
            return factory.TOTVSGet(parametros, callback);
        }

        /**
         * @public
         * @function Retorna parâmetros do Educacional
         * @name    getEduParam
         * @param   {int}    codColigada     Código da Coligada
         * @param   {int}    codFilial       Código da Filial
         * @param   {int}    codTipoCurso    Código do nível de ensino
         * @callback Objeto 
         * @returns {object} Objeto com parâmetros
         */
        function getEduParam(codColigada, codFilial, codTipoCurso, callback) {
 
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.codFilial = codFilial;
            parametros.codTipoCurso = codTipoCurso;

            var factory = getInstanciaFactory('/InfoEduParam/:codColigada/:codFilial/:codTipoCurso');
            return factory.TOTVSGet(parametros, callback);
        }
}
});
