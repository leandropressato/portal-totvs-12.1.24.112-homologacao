/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsInscricoesComprovanteModule
* @object module
*
* @created 14/09/2016 v12.1.15
* @updated
*
* @dependencies totvsHtmlFramework
*
* @description Módulo da funcionalidade de Comprovante de Inscrições do Processo Seletivo.
*/
define([], function () {

    'use strict';

    angular
        .module('edupsInscricoesComprovanteModule', ['totvsHtmlFramework']);

});
