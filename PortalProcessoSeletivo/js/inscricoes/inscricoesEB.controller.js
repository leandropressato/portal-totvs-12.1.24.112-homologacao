/**
 * @license TOTVS | Portal Processo Seletivo v12.1.18
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsInscricoesModule
 * @name EdupsInscricoesEBController
 * @object controller
 *
 * @created 26/07/2017 v12.1.18
 * @updated
 *
 * @requires inscricoes.module
 *
 * @dependencies edupsInscricoesFactory
 *
 * @description Controller da funcionalidade Inscrições do Processo Seletivo (ENSINO BÁSICO).
 *              Qualquer regra específica do nível de ensino básico, relacionado a funcionalidade
 *              de inscrições, deve ser colocada aqui nesse controller.
 */
define(['inscricoes/inscricoes.module',
	'inscricoes/inscricoes.factory',
	'inscricoes/inscricoes.service',
	'totvs-custom/custom.service'
], function () {

	'use strict';

	angular
		.module('edupsInscricoesModule')
		.controller('EdupsInscricoesEBController', EdupsInscricoesEBController);

	EdupsInscricoesEBController.$inject = [
		'edupsInscricoesFactory',
		'EdupsInscricoesService',
		'edupsCustomService',
		'$rootScope',
		'$scope',
		'$state',
		'i18nFilter',
		'$compile',
		'$timeout',
		'totvs.app-notification.Service',
		'$window'
	];

	function EdupsInscricoesEBController(
		EdupsInscricoesFactory,
		EdupsInscricoesService,
		edupsCustomService,
		$rootScope,
		$scope,
		$state,
		i18nFilter,
		$compile,
		$timeout,
		totvsNotification,
		$window
	) {

		var self = this;

		self.objInscricao = new Inscricao();
		self.MostraFormularioCompleto = false;
		self.PeriodoInscricao = $rootScope.PeriodoInscricao;
		self.PeriodoResultado = $rootScope.PeriodoResultado;
		self.objParametros = null;

		self.listaDocumentoExigido = [];
		self.listaTipoDocumento = [];
		self.listaPapeisRelacPais = [];
		self.listaPapeisRelacResponsavelFin = [];
		self.listaPapeisRelacResponsavelAcad = [];
		self.listaTabelaDinamicaItem = {};

		self.TERMOACEITEPS = '';
		self.blnUtilizaVagaExcedente = false;
		self.validadorEmail = '^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]?)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$';
		self.listaSexo = [{
			value: 'F',
			label: i18nFilter('l-sexo-feminino', [], 'js/inscricoes')
		}, {
			value: 'M',
			label: i18nFilter('l-sexo-masculino', [], 'js/inscricoes')
		}];

		self.listaSouCandidato = [{
			value: false,
			label: i18nFilter('l-sou-candidato-nao', [], 'js/inscricoes')
		}, {
			value: true,
			label: i18nFilter('l-sou-candidato-sim', [], 'js/inscricoes')
		}];

		self.radioSimNao = [{
				value: false,
				label: i18nFilter('l-nao', [], 'js/inscricoes')
			},
			{
				value: true,
				label: i18nFilter('l-sim', [], 'js/inscricoes')
			}
		];

		self.carregaAreasOfertadas = carregaAreasOfertadas;
		self.campoVisivel = campoVisivel;
		self.campoObrigatorio = campoObrigatorio;
		self.campoDocumentoObrigatorio = campoDocumentoObrigatorio;
		self.campoCPFPassRGObrigatorio = campoCPFPassRGObrigatorio;
		self.continuarInscricao = continuarInscricao;
		self.efetuarLogin = efetuarLogin;
		self.exibeGrupoCampos = exibeGrupoCampos;
		self.carregarCamposFormulario = carregarCamposFormulario;
		self.setCamposCPFPassRGObrigatorio = setCamposCPFPassRGObrigatorio;
		self.setCamposCPFPassRGObrigatorioAlteracaoPais = setCamposCPFPassRGObrigatorioAlteracaoPais;
		self.carregaInfoFormaInscricao = carregaInfoFormaInscricao;
		self.criarListaAreaOfertadaOpcional = criarListaAreaOfertadaOpcional;

		self.buscaDadosUsuario = buscaDadosUsuario;
		self.inscricaoAposLogin = $state.params.inscricaoAposLogin;
		self.novoDependente = $state.params.novoDependente;
		self.codUsuarioPSDependente = $state.params.codUsuarioPSDependente;
		self.getInformacoesInscricao = getInformacoesInscricao;
		self.getListaPapeisRelacResponsavelFin = getListaPapeisRelacResponsavelFin;
		self.getListaPapeisRelacResponsavelAcad = getListaPapeisRelacResponsavelAcad;
		self.getListaPapeisRelacPais = getListaPapeisRelacPais;
		self.getListaCampusLocalRealizacaoProva = getListaCampusLocalRealizacaoProva;
		self.trataCampoComplementarParaVisualizacao = trataCampoComplementarParaVisualizacao;

		self.getModelCampo = getModelCampo;
		self.confirmarInscricao = confirmarInscricao;
		self.buscarIndicacaoPorCodigo = buscarIndicacaoPorCodigo;
		self.booleanToString = booleanToString;
		self.sexoPorExtenso = sexoPorExtenso;
		self.buscarEnderecoPorCEP = buscarEnderecoPorCEP;
		self.copiarEnderecoCandidato = copiarEnderecoCandidato;
		self.tipoResponsavelAcademico = tipoResponsavelAcademico;
		self.tipoResponsavelFinanceiro = tipoResponsavelFinanceiro;
		self.tipoResponsavelInscricao = tipoResponsavelInscricao;
		self.validaObrigatoriedadeIdiomas = validaObrigatoriedadeIdiomas;
		self.validaVisibilidadeIdiomas = validaVisibilidadeIdiomas;
		self.criarCamposComplementares = criarCamposComplementares;
		self.getParamURL = getParamURL;
		self.showDocExigidoGroup = false;
		self.getListaDocumentosExigidosAndOfertaOnline = getListaDocumentosExigidosAndOfertaOnline;
		self.getArquivosSelecionados = getArquivosSelecionados;
		self.getInfoPagamento = getInfoPagamento;
		self.showPaymentDetail = showPaymentDetail;
		self.showMsgMatric = false;
		self.showPayment = false;
		self.showPagCartao = false;
		self.isOfertaOnline = false;
		self.RA = "";
		self.msgMatriculaSuccess = "";
		
		self.salvar = salvar;
		self.doBeforeSave = doBeforeSave;
        self.doAfterSave = doAfterSave;
		self.tratarAction = tratarAction;
		self.getLabelCampoNome = getLabelCampoNome;
		self.getTextoSimNao = getTextoSimNao;
        self.concatenaName = concatenaName;
        self.chamarLogin = chamarLogin;
		self.ResponsavelCandidato = false;
		self.adequacaoDeficienciasDisponivel = false;

		self.etapas = [];
		self.frmDadosCandidato = {};
		self.frmDadosResponsavelInscricao = {};
		self.frmDadosResponsavelAcademico = {};
		self.frmDadosResponsavelFinanceiro = {};
		self.frmDadosMae = {};
		self.frmDadosPai = {};
		self.frmAtividadesAgendadas = {};
		self.frmDocumentos = {};
		self.frmOfertaOnline = {};
		self.frmDadosSenha = {};
		self.frmDadosAreaInteresse = {};

		self.retornarEtapaAtual = retornarEtapaAtual;
		self.avancarEtapa = avancarEtapa;
        self.retrocederEtapa = retrocederEtapa;
        self.redirecionar = redirecionar;
		self.aoIniciarEtapaDadosBasicos = aoIniciarEtapaDadosBasicos;
		self.aoIniciarEtapaResponsaveis = aoIniciarEtapaResponsaveis;
		self.aoIniciarEtapaDadosCurso = aoIniciarEtapaDadosCurso;

		self.documentosValidos = documentosValidos;

		// Devem ser registrados todas as funções de inicialização, quando uma etapa do wizard é acionada
		self.eventosStateEtapas = {
			'inscricoesWizard.dados-basicos': 'aoIniciarEtapaDadosBasicos',
			'inscricoesWizard.responsaveis': 'aoIniciarEtapaResponsaveis',
			'inscricoesWizard.dados-curso': 'aoIniciarEtapaDadosCurso'
		};

		// Só executa o método init após carregar o objeto com os parâmetros
		var myWatch = $scope.$watch('objParametros', function (data) {
			if (EdupsInscricoesService.isDefinedNotNull(data) && EdupsInscricoesService.isDefinedNotNull(data.NomePortalInscricoes)) {

				self.objParametros = data;

				$timeout(function () {
					init();
				});

				myWatch();
			}
		});

		// *********************************************************************************
		// *** Functions
		// *********************************************************************************

		function init() {

			//Processo customização antes inicialização
            edupsCustomService.initPre(self, $scope);
			//Fim
			
			carregarListaDadosBasicos(function () {

				//Nestes casos o usuário já existe na base, e o que já existe precisa ser recuperado da base.
				if (self.inscricaoAposLogin === true || self.novoDependente === true) {
					buscaDadosUsuario();					
				}
			});

			if (angular.isDefined($state.params) && angular.isObject($state.params.objInscricao)) {
				self.objInscricao.souCandidato = $state.params.objInscricao.souCandidato;
			}

			self.ResponsavelCandidato = $rootScope.objParametros.ResponsavelCandidato;

			if (!self.ResponsavelCandidato) {
				self.objInscricao.souCandidato = true;
			} 			

			self.objInscricao.listas.listaDefAuditiva = [];
			self.objInscricao.listas.listaDefFisica = [];
			self.objInscricao.listas.listaDefVisual = [];
			self.objInscricao.listas.listaDefFala = [];
			self.objInscricao.listas.listaDefMental = [];
			self.objInscricao.listas.listaDefIntelectual = [];
			self.objInscricao.listas.listaDefReabilitado = [];

			self.objInscricao.listas.listaSelectedDefAuditiva = [];
			self.objInscricao.listas.listaSelectedDefFisica = [];
			self.objInscricao.listas.listaSelectedDefVisual = [];
			self.objInscricao.listas.listaSelectedDefFala = [];
			self.objInscricao.listas.listaSelectedDefMental = [];
			self.objInscricao.listas.listaSelectedDefIntelectual = [];
			self.objInscricao.listas.listaSelectedDefReabilitado = [];

			self.objInscricao.listas.listaEstadoCivil = [];
			self.objInscricao.listas.listaNacionalidade = [];
			self.objInscricao.listas.listaCorRaca = [];
			self.objInscricao.listas.listaGrauInstrucao = [];
			self.objInscricao.listas.listaTipoSanguineo = [];
			self.objInscricao.listas.listaTipoRua = [];
			self.objInscricao.listas.listaTipoBairro = [];
			self.objInscricao.listas.listaSituacaoMilitar = [];
			self.objInscricao.listas.listaPaises = [];
			self.objInscricao.listas.listaEstados = [];

			self.objInscricao.listas.listaLocalRealizacaoProva = [];
			self.objInscricao.listas.listaIdioma = [];
			self.objInscricao.listas.listaFormaInscricao = [];
			self.objInscricao.listas.listaGruposAreaInteresse = [];
			self.objInscricao.listas.listaCampusLocalRealizacaoProva = [];
			self.objInscricao.listas.listaDropDownAreaOfertadaOpcional = [];
			self.objInscricao.listas.listaIdiomasOpcionais = [];
			self.objInscricao.listas.listaAreaOfertada = [];
			self.objInscricao.listas.listaAreaOfertadaOpcional = [];
			self.objInscricao.listas.listaAtividadesAgendadas = [];

			self.objInscricao.listas.listaDocumentosExigidos = [];

			self.objInscricao.exibeGrupoDadosEnem = false;
			self.objInscricao.campoEnemObrigatorio = false;

			self.objInscricao.camposCPFPassRGObrigatorio = {
				'C': [], // Candidato
				'P': [], // Pai
				'M': [], // Mãe
				'RA': [], // Responsável acadêmico
				'RF': [], // Responsável financeiro
				'RI': [] // Responsável pela inscrição
			};

			self.objInscricao.exibeIndicacaoAluno = false;
			self.objInscricao.exibeIndicacaoFornecedor = false;
			self.objInscricao.exibeIndicacaoFuncProfessor = false;

			self.objInscricao.dadosInscricaoAreaOfertada.CODCOLIGADA = $rootScope.CodColigada;
			self.objInscricao.dadosInscricaoAreaOfertada.IDPS = $rootScope.IdPS;
			self.objInscricao.dadosInscricaoAreaOfertada.DATAINSCRICAO = new Date();
			self.objInscricao.dadosInscricaoAreaOfertada.VALORINSCRICAO = $rootScope.ValorInscricao;
			self.objInscricao.dadosInscricaoAreaOfertada.NOMEPROCESSOSELETIVO = $rootScope.NomeProcessoSeletivo;
			self.objInscricao.dadosInscricaoAreaOfertada.TREINEIRO = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFAUDITIVA = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFVISUAL = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFFISICA = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFMENTAL = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFINTELECTUAL = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFFALA = false;
			self.objInscricao.dadosInscricaoAreaOfertada.BRPDH = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFMULTIPLA = false;
			self.objInscricao.dadosInscricaoAreaOfertada.DEFOUTRAS = false;

			self.objInscricao.dadosOpcaoInscrito.CODCOLIGADA = $rootScope.CodColigada;
			self.objInscricao.dadosOpcaoInscrito.IDPS = $rootScope.IdPS;

			EdupsInscricoesService.inicializaValoresDefault(self.objInscricao.dadosResponsavelInscricao);
			self.objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac = -1;
			self.objInscricao.dadosResponsavelInscricao.formularioEmBranco = true;

			EdupsInscricoesService.inicializaValoresDefault(self.objInscricao.dadosUsuario);

			if (self.objParametros.UsaDadosPai) {
				EdupsInscricoesService.inicializaValoresDefault(self.objInscricao.dadosPai);
				self.objInscricao.dadosPai.UsaDadosTipoRelac = -1;
				self.objInscricao.dadosPai.formularioEmBranco = true;
			}

			if (self.objParametros.UsaDadosMae) {
				EdupsInscricoesService.inicializaValoresDefault(self.objInscricao.dadosMae);
				self.objInscricao.dadosMae.UsaDadosTipoRelac = -1;
				self.objInscricao.dadosMae.formularioEmBranco = true;
			}

			if (self.objParametros.UsaDadosRespAcad) {
				EdupsInscricoesService.inicializaValoresDefault(self.objInscricao.dadosResponsavelAcademico);
				self.objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac = -1;
				self.objInscricao.dadosResponsavelAcademico.formularioEmBranco = true;
			}

			if (self.objParametros.UsaDadosRespFin) {
				EdupsInscricoesService.inicializaValoresDefault(self.objInscricao.dadosResponsavelFinanceiro);
				self.objInscricao.dadosResponsavelFinanceiro.UsaDadosTipoRelac = -1;
				self.objInscricao.dadosResponsavelFinanceiro.formularioEmBranco = true;
			}

			getListaTipoDocumento();

			EdupsInscricoesService.getListaGruposAreaInteresse($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao,
				function (result) {
					if (EdupsInscricoesService.isDefinedNotNull(result) && result.length === 1) {
						self.carregaAreasOfertadas($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao.dadosInscricaoAreaOfertada.GRUPOPSOBJ);
					}

					var grupo = null;
					if ($rootScope.UsaGrupoAreaInteressePS && EdupsInscricoesService.isDefinedNotNull(self.objInscricao.dadosInscricaoAreaOfertada.GRUPOPSOBJ)) {
						grupo = self.objInscricao.dadosInscricaoAreaOfertada.GRUPOPSOBJ.GRUPO;
					}

					EdupsInscricoesService.getListaAreaOfertada($rootScope.CodColigada, $rootScope.IdPS, grupo, self.objInscricao,
						function (result) {
							if (EdupsInscricoesService.isDefinedNotNull(result)){
								if (result.length === 1) {
									self.getInformacoesInscricao($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ);
								}
								else if (result.length > 1){
									let idArea = self.getParamURL('ai');
									if (idArea > 0){
										var areaUrl = self.objInscricao.listas.listaAreaOfertada.filter(function (x) { return x.IDAREAINTERESSE === parseInt(idArea, 10); });
										if (areaUrl.length > 0) {
											self.objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ = areaUrl[0];
											self.getInformacoesInscricao($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ);
										}										
									}
								}
							}
						}
					);
				}
			);

			EdupsInscricoesService.getListaAtividadesAgendadas($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao);
			EdupsInscricoesService.getListaTabelaDinamicaItem(self.objParametros, self.listaTabelaDinamicaItem, function (lista) {
				self.listaTabelaDinamicaItem = lista;
			});

			//model vagas reservadas
            self.objInscricao.dadosInscricaoAreaOfertada.CotaInstituicaoFederal = {};
            self.objInscricao.dadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhCotaFederal = null;
            self.objInscricao.dadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhCorRacaCota = null;
            self.objInscricao.dadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhDeficienciaCota = null;
            self.objInscricao.dadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhEnsinoPublico = null;
            self.objInscricao.dadosInscricaoAreaOfertada.CotaInstituicaoFederal.PossuiRendaBaixa = null;

			EdupsInscricoesService.separaBarraServico();

			//Se estiver vindo da página de login preenche o objInscrição e abre o formulátio completo
			if ($state.params.inscricaoAposLogin === true || $state.params.novoDependente === true) {
				self.MostraFormularioCompleto = true;
				$state.go('inscricoesWizard.dados-basicos');
				toggleCollapse();
			}

			$('#modalRecibo').on('hidden.bs.modal', function () {
				$state.go('informacoes.start', {
					nivelEnsino: $rootScope.nivelEnsino
				});
			});

			getListaPapeisRelacPais();
			getListaPapeisRelacResponsavelFin();
			getListaPapeisRelacResponsavelAcad();

			//Processo customização após inicialização
			edupsCustomService.initPost(self, $scope);
			//Inicializa Etapas
			carregarEtapas();
			//Inicializa o Wizard
			EdupsInscricoesService.inicializarWizardPS($scope, self.eventosStateEtapas, self.etapas);
            //Fim
		}

		function carregarEtapas() {
			self.etapas.push({
				codigo: 1,
                ordem: self.etapas.length + 1,
                nome: 'inscricoesWizard.dados-basicos',
                descricao: i18nFilter('l-dados-basicos', [], 'js/inscricoes'),
                ativo: true,
                realizado: false,
                padrao: true
			});
			if($rootScope.objParametros.UsaDadosPai || $rootScope.objParametros.UsaDadosMae || 
			   $rootScope.objParametros.UsaDadosRespAcad || $rootScope.objParametros.UsaDadosRespFin){
				self.etapas.push({
					codigo: 2,
					ordem: self.etapas.length + 1,
					nome: 'inscricoesWizard.responsaveis',
					descricao: i18nFilter('l-responsaveis', [], 'js/inscricoes'),
					ativo: false,
					realizado: false
				});
			}
			self.etapas.push({
				codigo: 3,
                ordem: self.etapas.length + 1,
                nome: 'inscricoesWizard.dados-curso',
                descricao: i18nFilter('l-dados-curso', [], 'js/inscricoes'),
                ativo: false,
				realizado: false
			});
		}

		function aoIniciarEtapaDadosBasicos() { }

		function aoIniciarEtapaResponsaveis() { }

		function aoIniciarEtapaDadosCurso() { 
			setTimeout(function() {
				angular.element(document).ready(function () {
					$("#frmDadosAreaInteresse div").eq(3).click();
				})}, 500);
		}

		function retrocederEtapa() {
			EdupsInscricoesService.retrocederEtapa(self.etapas);
		}

        function retornarEtapaAtual() {
            return EdupsInscricoesService.retornarEtapaAtual(self.etapas);
        }
		
        function redirecionar(state) {
            EdupsInscricoesService.redirecionar(self.etapas, state);
		}

		function avancarEtapa() {
			var nomeEtapaAtual = EdupsInscricoesService.retornarEtapaAtual(self.etapas).nome;

			if (EdupsInscricoesService.camposObrigatoriosCandidatoPreenchidos($scope, self.objInscricao, self.inscricaoAposLogin, self.novoDependente, nomeEtapaAtual)) {
				if (nomeEtapaAtual === 'inscricoesWizard.dados-curso') {
					if (EdupsInscricoesService.camposObrigatoriosAreaInteressePreenchidos($scope) && 
						EdupsInscricoesService.camposObrigatoriosAtividadesAgendadas($scope, self.blnUtilizaVagaExcedente)) {
							confirmarInscricao();	
					}
				}
				else {
					EdupsInscricoesService.verificaFormulariosEmBranco($scope, self.objInscricao);
					EdupsInscricoesService.liberarEtapaAtual(self.etapas);
					EdupsInscricoesService.liberarProximaEtapa(self.etapas);
					EdupsInscricoesService.notificaInteracao(self.etapas, $scope);
					EdupsInscricoesService.avancarEtapa(self.etapas);
				}
			}
		}

		function buscaDadosUsuario() {
			EdupsInscricoesService.buscaDadosUsuario(function (response) {

				// Recuperou usuário e é uma nova inscrição para um dependente existente.
				if (EdupsInscricoesService.isDefinedNotNull(response) && self.novoDependente === false && self.codUsuarioPSDependente) {
					// Recupera os dados do dependente e insere essas informações no array de responsável pela inscrição.
					EdupsInscricoesService.buscaDadosDependente(self.codUsuarioPSDependente, function (retornoDependente) {

						if (EdupsInscricoesService.isDefinedNotNull(retornoDependente)) {
							response.SPSUSUARIO.push(retornoDependente.SPSUSUARIO[0]);
							EdupsInscricoesService.atualizaInformacoesUsuario(response, self.objInscricao);
						}
					});
				}
				else {
					EdupsInscricoesService.atualizaInformacoesUsuario(response, self.objInscricao);
				}				
			});
		}

		function toggleCollapse() {
			if (self.objInscricao.souCandidato) {
				$('#dadosCandidato').collapse('show');
			} else {
				$('#dadosResponsavelInscricao').collapse('show');
			}

			$('#dadosResponsavelInscricao, #dadosCandidato, #dadosPai, #dadosMae, #dadosResponsavelFinanceiro, #dadosResponsavelAcademico, #opcaoInteresse, #senhaCandidato, #documentosCandidato')
				.on('shown.bs.collapse', function () {
					$(this).prev('.cabecalho-grupo').find('.glyphicon-chevron-down').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
				});

			$('#dadosResponsavelInscricao, #dadosCandidato, #dadosPai, #dadosMae, #dadosResponsavelFinanceiro, #dadosResponsavelAcademico, #opcaoInteresse, #senhaCandidato, #documentosCandidato')
				.on('hidden.bs.collapse', function () {
					$(this).prev('.cabecalho-grupo').find('.glyphicon-chevron-up').addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
				});
		}

		function criarCamposComplementares(grupoCampoComplementar) {
			// Só executa a criação dos campos complementares (sendo chamada diretamente pela ng-init do HTML) após carregar o objeto com os parâmetros
			var myWatch = $scope.$watch('objParametros', function (data) {
				if (EdupsInscricoesService.isDefinedNotNull(data) && EdupsInscricoesService.isDefinedNotNull(data.NomePortalInscricoes)) {
					$timeout(function () {
						if (self.listaTabelaDinamicaItem.hasOwnProperty()) {
							EdupsInscricoesService.criarCamposComplementares(grupoCampoComplementar, self.listaTabelaDinamicaItem, $scope);
							EdupsInscricoesService.preencheValorDefaultCampoComplementar(grupoCampoComplementar, self.objInscricao.dadosCamposComplementaresInscricao);
						} else {
							EdupsInscricoesService.getListaTabelaDinamicaItem(self.objParametros, self.listaTabelaDinamicaItem, function (lista) {
								self.listaTabelaDinamicaItem = lista;
								EdupsInscricoesService.criarCamposComplementares(grupoCampoComplementar, self.listaTabelaDinamicaItem, $scope);
								EdupsInscricoesService.preencheValorDefaultCampoComplementar(grupoCampoComplementar, self.objInscricao.dadosCamposComplementaresInscricao);
							});
						}
					});

					myWatch();
				}
			});
		}

		//Objeto de inscrição
		function Inscricao() {
			this.listas = {};
			this.dadosUsuario = {};
			this.dadosPai = {};
			this.dadosMae = {};
			this.dadosResponsavelInscricao = {};
			this.dadosResponsavelAcademico = {};
			this.dadosResponsavelFinanceiro = {};
			this.dadosInscricaoAreaOfertada = {};
			this.dadosOpcaoInscrito = {};
			this.dadosIdiomaInscrito = {};
			this.dadosCamposComplementaresInscricao = {};
			this.dadosDocumentosExigidos = {};
			this.dadosOfertaOnline = {};
			this.eduParam = null;
			this.dadosPagamento = null;
			this.dadosInscricaoAreaOfertada.GRUPOPS = -1;
			this.dadosInscricaoAreaOfertada.LocalProvaSelecionado = null;
			this.dadosEtapa = {};
			this.parseToJSON = EdupsInscricoesService.objParseToJSON;
		}

		function getLabelCampoNome() {
			var label = EdupsInscricoesService.getLabelCampoNome(self.objParametros, $rootScope.nivelEnsino, self.objInscricao.souCandidato);
			$('#nome_usuario label').text(label);
			var tooltip = $('#nome_usuario .tooltip-inner');
			if (tooltip.length > 0) {
				tooltip[0].innerHTML = label;
			}
			return label;
		}

		function getListaTipoDocumento() {
			var objDocumento = EdupsInscricoesService.getListaTipoDocumento();
			self.listaTipoDocumento = objDocumento.listaTipoDocumento;
			self.tipoDocumentoBuscaCandidato = objDocumento.tipoDocumentoBuscaCandidato;
		}

		function carregaAreasOfertadas(codColigada, idPS, grupoObj) {
			if (angular.isUndefined(grupoObj) || grupoObj === null) {
				EdupsInscricoesService.limpaCamposAreaInteresse(self.objInscricao);
			} else {
				EdupsInscricoesService.getListaAreaOfertada(codColigada, idPS, grupoObj.GRUPO, self.objInscricao,
					function (result) {
						if (EdupsInscricoesService.isDefinedNotNull(result) && result.length === 1) {
							self.getInformacoesInscricao($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ);
						}
					}
				);
			}
		}

		//Valida a visibilidade dos campos da tela de acordo com os parâmetros, e o tipo (Candidato ou Responsáveis)
		function campoVisivel(idCampo, tipoUsuario) {
			return EdupsInscricoesService.campoVisivel(idCampo, tipoUsuario);
		}

		//Valida a obrigatoriedade dos campos da tela de acordo com os parâmetros, e o tipo (Candidato ou Responsáveis)
		function campoObrigatorio(idCampo, tipoUsuario) {
			return EdupsInscricoesService.campoObrigatorio(idCampo, tipoUsuario, self.objInscricao);
		}

		// Valida a obrigatoriedade dos campos de documentos verificando o parâmetro de desobrigação para Estrangeiros
		// Serão considerados estrangeiros usuários que tiverem o país diferente do país de origem do processo seletivo da inscrição
		function campoDocumentoObrigatorio(idCampo, tipoUsuario) {
			return EdupsInscricoesService.campoDocumentoObrigatorio(idCampo, tipoUsuario, self.objInscricao);
		}

		// Utilizado no ng-required dos campos de CPF, Passaporte e RG Estrangeiro.
		function campoCPFPassRGObrigatorio(idCampo, tipoUsuario) {
			return EdupsInscricoesService.campoCPFPassRGObrigatorio(idCampo, tipoUsuario, self.objInscricao);
		}

		/**
		 * @public
		 * @function Obtém informações da inscrição pela área de interesse
		 * @name getInformacoesInscricao
		 * @param {int}    codColigada Coligada
		 * @param {int}    idPS Identificador Processo Seletivo
		 * @param {object} areaInteresseObj Objeto da Área de Interesse
		 */
		function getInformacoesInscricao(codColigada, idPS, areaInteresseObj) {
			EdupsInscricoesService.getInformacoesInscricao(codColigada, idPS, areaInteresseObj, self.objInscricao, self.blnUtilizaVagaExcedente);
			self.getListaDocumentosExigidosAndOfertaOnline();
		}

		function getListaPapeisRelacResponsavelFin() {
			self.listaPapeisRelacResponsavelFin = EdupsInscricoesService.getListaPapeisRelacResponsavelFin(self.objInscricao);
		}

		function getListaPapeisRelacResponsavelAcad() {
			self.listaPapeisRelacResponsavelAcad = EdupsInscricoesService.getListaPapeisRelacResponsavelAcad(self.objInscricao);
		}

		function getListaPapeisRelacPais() {
			self.listaPapeisRelacPais = EdupsInscricoesService.getListaPapeisRelacPais();
		}

		//Se não há nenhum campo para ser exibido no grupo, então o grupo não será exibido
		function exibeGrupoCampos(idGrupoCampo, tipoUsuario) {
			return EdupsInscricoesService.exibeGrupoCampos(idGrupoCampo, tipoUsuario);
		}

		function carregarCamposFormulario(dadosFormulario) {
			return EdupsInscricoesService.carregarCamposFormulario(dadosFormulario);
		}

		function setCamposCPFPassRGObrigatorioAlteracaoPais(tipoUsuario) {
			EdupsInscricoesService.setCamposCPFPassRGObrigatorioAlteracaoPais(tipoUsuario, self.objInscricao);
		}

		// Seta a variavel de controle para definicao de obrigatoriedade dos campos CPF/Passaporte/Registro Geral para estrangeiros
		function setCamposCPFPassRGObrigatorio(idCampo, tipoUsuario) {
			EdupsInscricoesService.setCamposCPFPassRGObrigatorio(idCampo, tipoUsuario, self.objInscricao);
		}

		function carregaInfoFormaInscricao() {
			EdupsInscricoesService.carregaInfoFormaInscricao(self.objInscricao);
		}

		function getListaCampusLocalRealizacaoProva(codColigada, idPS, codMunicipio) {
			EdupsInscricoesService.getListaCampusLocalRealizacaoProva(codColigada, idPS, codMunicipio, self.objInscricao);
		}

		function criarListaAreaOfertadaOpcional() {
			EdupsInscricoesService.criarListaAreaOfertadaOpcional(self.objInscricao);
		}

		// *********************************************************************************
		// *** Documentos Exigidos
		// *********************************************************************************

		function getListaDocumentosExigidosAndOfertaOnline() {
			EdupsInscricoesService.getListaDocumentosExigidosAndOfertaOnline($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao, function(result) {
				$timeout(function () {
					if (EdupsInscricoesService.isDefinedNotNull(result) && result.length > 0) {
						var containerDiv = document.getElementById('InformacaoDocumentos');

						angular.element(containerDiv).empty();

						for (var i = 0; i < result.length; i++) {
							var template = getTemplateFile(result[i], i),
								diretivaTotvs = $compile(template);
		
							diretivaTotvs = diretivaTotvs($scope);
							angular.element(containerDiv).append(diretivaTotvs);
						}
						self.showDocExigidoGroup = true;
					}
					else{
						self.showDocExigidoGroup = false;
						angular.element(document.getElementById('InformacaoDocumentos')).empty();
					}

					//Oferta Online
					getInfoOfertaOnline();
				});
			});
		}

		function documentosValidos(){
			let list = self.objInscricao.dadosDocumentosExigidos;
            for (var l = 0; l < list.length; l++){
                if ((list[l].hasOwnProperty('OBRIGATORIO') ) && (list[l].OBRIGATORIO === 'T') && (list[l].NOMEARQUIVO.length == 0) ){
					return false;
                }
			}
			
			return true;
		}

		function getInfoOfertaOnline() {
			if (EdupsInscricoesService.isDefinedNotNull(self.objInscricao.eduParam)){
				if (EdupsInscricoesService.isDefinedNotNull(self.objInscricao.dadosOfertaOnline) && self.objInscricao.dadosOfertaOnline[0].OfertaOnline){
					self.isOfertaOnline = true;
					if (self.objInscricao.dadosOfertaOnline[0].Planos.length > 0){
						self.showPayment = true;

						if (self.objInscricao.eduParam.PermitePagCartaoCredito){
							self.showPagCartao = true;
						}
					}
				}
				else{
					self.showPayment = false;
				}
			}
			else{
				self.showPayment = false;
			}
		}

		function getTemplateFile(itemCampo, k) {
			var template = '<div class="row"> \
					<div id="idArquivoFileRequerimento" class="col-md-8 col-lg-8 col-xs-9 col-sm-9"> \
						<edups-upload-file \
										name="documento_' + itemCampo.CODDOCUMENTO + '" \
										canclear="true"' +
										(itemCampo.OBRIGATORIO === 'T' ? ' required="true"' : '') +
										($rootScope.objParametros.ExtensaoArquivo !== 'undefined' && $rootScope.objParametros.ExtensaoArquivo.length > 0 ? ' accept="' + $rootScope.objParametros.ExtensaoArquivo + '"' : '') +
										($rootScope.objParametros.TamanhoMaximoArquivo !== 'undefined' && $rootScope.objParametros.TamanhoMaximoArquivo > 0 ? ' maxkbytes="' + $rootScope.objParametros.TamanhoMaximoArquivo + '"' : '') +
										' label="' + itemCampo.DESCRICAO + '" \
										nameform="' + $scope.frmDocumentos.$name + '" \
										model="controller.objInscricao.dadosDocumentosExigidos[' + k + ']" \> \
						</edups-upload-file> \
					</div> \
				</div>';

            return template;
		}

		function getArquivosSelecionados(item){
			var text = '';

			let list = self.objInscricao.dadosDocumentosExigidos;
            for (var l = 0; l < list.length; l++){
                if ((list[l].NAME === ('documento_'+item.CODDOCUMENTO)) && (list[l].NOMEARQUIVO.length > 0) ){
					for(var i = 0; i < list[l].NOMEARQUIVO.length; i++)
					{
						text += list[l].NOMEARQUIVO[i].split(/[\\|/]/).pop() + '\n';
					}
                }
			}
			return text;
		}

		function getInfoPagamento() {
			var cod = self.objInscricao.dadosPagamento;
			var infoPagamento = self.objInscricao.dadosOfertaOnline[0].Planos.filter(function (x) { return x.Codigo.trim() === cod.trim(); });
			return infoPagamento[0].Codigo + ' - ' + infoPagamento[0].Nome;
		}		

		function showPaymentDetail(codPlano){
			EdupsInscricoesService.showPaymentDetail(codPlano, self.objInscricao.dadosOfertaOnline[0].Parcelas);
		}

		function getParamURL(parametro){
			var url = window.location.search.substring(1);
                   
                if (!window.location.search && window.location.hash.indexOf('?') > -1) {
                    url = window.location.hash.split('?')[1];
                }

                var parametros = url.split('&');

                for (var i = 0; i < parametros.length; i++) {
                    var par = parametros[i].split('=');

                    if (par[0] === parametro) {
                        return par[1];
                    }
                }
		}
		// *********************************************************************************
		// *** FIM - Documentos Exigidos
		// *********************************************************************************

		function getModelCampo(i, grupoCampo) {
			return EdupsInscricoesService.getModelCampo(i, grupoCampo);
		}

		function confirmarInscricao() {
			EdupsInscricoesService.BuscaTermoAceitePS($rootScope.CodColigada, $rootScope.IdPS, self.objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE, function (result) {
				if (EdupsInscricoesService.isDefinedNotNull(result)) {
					self.TERMOACEITEPS = result;
				}
			});

			$('#modalConfirmacao').modal({
				backdrop: 'static',
				keyboard: false
			});
		}

		function continuarInscricao() {
			if ($scope.frmDadosBasicos.$valid) {
				//Faz o bind das listas de país, estado e município do formulário principal de inscrição com base no "mini-formulário" CANDIDATO
				if (self.objInscricao.dadosUsuario.PAISESMINI) {
					EdupsInscricoesService.getListaPaises(self.objInscricao, function (result) {
						self.objInscricao.dadosUsuario.PAISES = result.find(function (item) {
							if (item.IDPAIS === self.objInscricao.dadosUsuario.PAISESMINI.IDPAIS) {
								return item;
							}
						});
					});

					if (self.objInscricao.dadosUsuario.ESTADOSMINI) {
						EdupsInscricoesService.getListaEstados(self.objInscricao.dadosUsuario.PAISESMINI.IDPAIS, self.objInscricao, function (result) {
							self.objInscricao.dadosUsuario.ESTADOS = result.find(function (item) {
								if (item.CODETD === self.objInscricao.dadosUsuario.ESTADOSMINI.CODETD) {
									return item;
								}
							});
						});

						if (self.objInscricao.dadosUsuario.MUNICIPIOSMINI) {
							EdupsInscricoesService.getListaMunicipios(self.objInscricao.dadosUsuario.ESTADOSMINI.CODETD, function (result) {
								self.objInscricao.dadosUsuario.MUNICIPIOS = result.find(function (item) {
									if (item.CODMUNICIPIO === self.objInscricao.dadosUsuario.MUNICIPIOSMINI.CODMUNICIPIO &&
										item.CODETDMUNICIPIO === self.objInscricao.dadosUsuario.ESTADOSMINI.CODETD) {
										return item;
									}
								});
							});
						}
					}
				}

				//Faz o bind das listas de país, estado e município do formulário principal de inscrição com base no "mini-formulário" RESPONSÁVEL
				if (self.objInscricao.dadosResponsavelInscricao.PAISESMINI) {
					EdupsInscricoesService.getListaPaises(self.objInscricao, function (result) {
						self.objInscricao.dadosResponsavelInscricao.PAISES = result.find(function (item) {
							if (item.IDPAIS === self.objInscricao.dadosResponsavelInscricao.PAISESMINI.IDPAIS) {
								return item;
							}
						});
					});

					if (self.objInscricao.dadosResponsavelInscricao.ESTADOSMINI) {
						EdupsInscricoesService.getListaEstados(self.objInscricao.dadosResponsavelInscricao.PAISESMINI.IDPAIS, self.objInscricao, function (result) {
							self.objInscricao.dadosResponsavelInscricao.ESTADOS = result.find(function (item) {
								if (item.CODETD === self.objInscricao.dadosResponsavelInscricao.ESTADOSMINI.CODETD) {
									return item;
								}
							});
						});

						if (self.objInscricao.dadosResponsavelInscricao.MUNICIPIOSMINI) {
							EdupsInscricoesService.getListaMunicipios(self.objInscricao.dadosResponsavelInscricao.ESTADOSMINI.CODETD, function (result) {
								self.objInscricao.dadosResponsavelInscricao.MUNICIPIOS = result.find(function (item) {
									if (item.CODMUNICIPIO === self.objInscricao.dadosResponsavelInscricao.MUNICIPIOSMINI.CODMUNICIPIO &&
										item.CODETDMUNICIPIO === self.objInscricao.dadosResponsavelInscricao.ESTADOSMINI.CODETD) {
										return item;
									}
								});
							});
						}
					}
				}

				// Busca por informações já cadastradas do candidato para utilização no formulário de inscrição
				EdupsInscricoesService.buscarInformacoesCandidato(self.objInscricao);

				// Verifica as atividades agendadas antes de liberar a exibição do formulário completo.
				if (!self.blnUtilizaVagaExcedente) {
					EdupsInscricoesService.validaAtividadesAgendadas(self.objInscricao, function (result) {
						self.MostraFormularioCompleto = result;
					});
				} else {
					self.MostraFormularioCompleto = true;
				}

				if (self.MostraFormularioCompleto && $rootScope.objParametros.UsaIntegracaoRubeus)
				{
					EdupsInscricoesService.verificaFormulariosEmBranco($scope, self.objInscricao);
					EdupsInscricoesService.notificaInteracao(self.etapas, $scope);
				}

				toggleCollapse();

				getListaPapeisRelacPais();
				getListaPapeisRelacResponsavelFin();
				getListaPapeisRelacResponsavelAcad();


				//abre página intermediária de primeira etapa concluída
				var paramsUrl = "?c="+$rootScope.CodColigada+"&f=1&ct="+$rootScope.codCategoria+"&ps="+$rootScope.IdPS;
				$window.open('/web/app/edu/PortalProcessoSeletivo/primeira-etapa-concluida.html'+paramsUrl, '_blank');

			} else {
				if (!EdupsInscricoesService.isDefinedNotNull(self.objInscricao.souCandidato)) {
					totvsNotification.notify({
						type: 'error',
						title: i18nFilter('l-titulo-inscricoes', [], 'js/inscricoes'),
						detail: i18nFilter('l-inscricao-sem-informar-responsavel', [], 'js/inscricoes'),
					});
				}
			}
		}

		function trataCampoComplementarParaVisualizacao(itemCampo) {
			return EdupsInscricoesService.trataCampoComplementarParaVisualizacao(itemCampo, self.objInscricao);
		}

		function buscarEnderecoPorCEP(numeroCEP, modeloDados) {
			EdupsInscricoesService.buscarEnderecoPorCEP(numeroCEP, modeloDados, self.objInscricao);
		}

		function copiarEnderecoCandidato(modeloDadosDestino) {
			EdupsInscricoesService.copiarEnderecoCandidato(modeloDadosDestino, self.objInscricao);
		}

		function buscarIndicacaoPorCodigo(tipo, codColigada, codigo, modeloDados) {
			EdupsInscricoesService.buscarIndicacaoPorCodigo(tipo, codColigada, codigo, modeloDados);
		}

		function tipoResponsavelAcademico(dadosResponsavel) {
			return EdupsInscricoesService.tipoResponsavelAcademico(dadosResponsavel);
		}

		function tipoResponsavelFinanceiro(dadosResponsavel) {
			return EdupsInscricoesService.tipoResponsavelFinanceiro(dadosResponsavel);
		}

		function tipoResponsavelInscricao(dadosResponsavel, tipo) {
			return EdupsInscricoesService.tipoResponsavelInscricao(dadosResponsavel, tipo);
		}

		function validaObrigatoriedadeIdiomas(indiceCampo) {
			return EdupsInscricoesService.validaObrigatoriedadeIdiomas(indiceCampo, self.objInscricao);
		}

		function validaVisibilidadeIdiomas() {
			return EdupsInscricoesService.validaVisibilidadeIdiomas(self.objInscricao);
		}

		function booleanToString(valor) {
			return EdupsInscricoesService.booleanToString(valor);
		}

		function sexoPorExtenso(valor) {
			return EdupsInscricoesService.sexoPorExtenso(valor);
		}

		function efetuarLogin(tipoServico) {
			EdupsInscricoesService.efetuarLogin(tipoServico);
		}

		function salvar() {
			self.model = self.objInscricao.parseToJSON();

			self.doBeforeSave(function(callBack){
				EdupsInscricoesFactory.realizarInscricao(self.model, function (result) {
					self.doAfterSave(result);
				});
			});
		}

		// *********************************************************************************
        // Função de CallBack aberta no self para que seja possivel sobrescrever em metodos customizados
        // *********************************************************************************
		function doBeforeSave(callBack){
            if (typeof callBack === 'function') {
                callBack(true);
            }
        }

        function doAfterSave(result) {
			if (!EdupsInscricoesService.isDefinedNotNull(result)){
				let msg = ' Erro ' + arguments[1].status + ': - ' +  arguments[1].data.ExceptionMessage;
				totvsNotification.notify({
					type: 'error',
					title: i18nFilter('l-titulo-comprovante', [], 'js/inscricoes'),
					detail: i18nFilter('l-erro-save', [], 'js/inscricoes') + msg
				});
				return false;
			}
			else if (EdupsInscricoesService.isDefinedNotNull(result.exception)) {
				let sCaption = i18nFilter('l-titulo-comprovante', [], 'js/inscricoes');
				if (EdupsInscricoesService.isDefinedNotNull(result.matricula)){
					sCaption = i18nFilter('l-titulo-matricula', [], 'js/inscricoes');
				}
				totvsNotification.notify({
					type: 'error',
					title: sCaption,
					detail: result.message
				});
			} else {
				self.model = result;

				self.showPayment = false;
				self.showPagCartao = false;
				self.showMsgMatric = false;

				if (EdupsInscricoesService.isDefinedNotNull(result._TBMATRIC_)){
					self.showMsgMatric = true;
					self.showPayment = result._TBMATRIC_[0].IDLAN !== null && result._TBMATRIC_[0].IDLAN > 0;
					self.showPagCartao = self.showPayment && self.objInscricao.eduParam.PermitePagCartaoCredito;
					self.RA = result._TBMATRIC_[0].RA;

					if (!self.showPayment){
						if (result._TBMATRIC_[0].PRTMSGCONFIRMACAOMAT === null || result._TBMATRIC_[0].PRTMSGCONFIRMACAOMAT === ''){
							self.msgMatriculaSuccess = i18nFilter('l-matric-success', [], 'js/inscricoes');
						}
						else{
							self.msgMatriculaSuccess = result._TBMATRIC_[0].PRTMSGCONFIRMACAOMAT;
						}
					} else{
						if (result._TBMATRIC_[0].PRTVERIFICADEBFIN === null || result._TBMATRIC_[0].PRTVERIFICADEBFIN === '0'
							|| (result._TBMATRIC_[0].PRTVERIFICADEBFIN === '1' 
									&& (result._TBMATRIC_[0].PRTMSGMATDEBFINANC === null || result._TBMATRIC_[0].PRTMSGMATDEBFINANC === '')))
						{
							self.msgMatriculaSuccess = i18nFilter('l-matric-pending', [], 'js/inscricoes');
						}
						else{
							self.msgMatriculaSuccess = result._TBMATRIC_[0].PRTMSGMATDEBFINANC;
						}
					}
				}
				
				if (angular.isArray(self.model.SPSUSUARIO)) {
					if (self.objInscricao.souCandidato) {
						// Recupera os dados do Candidato
						self.model.objUsuario = self.model.SPSUSUARIO.find(function (item) {
							if (item.EHCANDIDATO === 'T') {
								return item;
							}
						});
					} else {
						// Recupera os dados do Responsável pela Inscrição
						self.model.objUsuario = self.model.SPSUSUARIO.find(function (item) {
							if (item.EHRESPINSC === 'T') {
								return item;
							}
						});
					}
				}

				self.model.PagamentoCartao = $rootScope.objParametros.PagamentoCartao;

				$('#modalRecibo').modal({
					backdrop: 'static',
					keyboard: false
				});
				//abre página de conclusão de inscrição
					var paramsUrl = "?c="+$rootScope.CodColigada+"&f=1&ct="+$rootScope.codCategoria+"&ps="+$rootScope.IdPS;
					$window.open('/web/app/edu/PortalProcessoSeletivo/inscricao-concluida.html'+paramsUrl, '_blank');
			}
		}

		function tratarAction(action, token) {
			$('#modalRecibo').modal('toggle');
			$timeout(function () {
				var params = {
					action: action,
					token: token,
					nivelEnsino: $rootScope.nivelEnsino,
					objInscricao: $state.params.objInscricao
				};
				$state.go('centralcandidato.start', params);
			}, 500);
		}

		function getTextoSimNao(valor) {
			return EdupsInscricoesService.getTextoSimNao(valor);
		}

		//Carregar listas com dados base
		function carregarListaDadosBasicos(callback) {
            EdupsInscricoesService.getListaPaises(self.objInscricao, function (result) {
                self.objInscricao.listas.listaPaises = result;

                EdupsInscricoesService.getListaEstadoCivil(self.objInscricao);
                EdupsInscricoesService.getListaNacionalidade(self.objInscricao);
                EdupsInscricoesService.getListaCorRaca(self.objInscricao);
                EdupsInscricoesService.getListaGrauInstrucao(self.objInscricao);
                EdupsInscricoesService.getListaTipoSanguineo(self.objInscricao);
                EdupsInscricoesService.getListaTipoRua(self.objInscricao);
                EdupsInscricoesService.getListaTipoBairro(self.objInscricao);
                EdupsInscricoesService.getListaProfissao(self.objInscricao);
                EdupsInscricoesService.getListaSituacaoMilitar(self.objInscricao);

                EdupsInscricoesService.getListaTipoDeficiencia(self.objInscricao, function (deficiencias, infoAdicionais){
                    if(angular.isDefined(infoAdicionais)){
                        self.adequacaoDeficienciasDisponivel = infoAdicionais.find(function(item){
                            if(item.Info === 'AdequacaoDeficienciasExecutada' && item.Valor === 'True'){
                                return true;
                            }
                            else {
                                return false;
                            }

                        });
                    }
                });

                if (angular.isFunction(callback)) {
                    callback();
                }
            });
		}

		//Concatena a descrição do telefone dos parâmetros com o resource
        function concatenaName(textoTelefone, nomeResource) {
            return EdupsInscricoesService.concatenaName(textoTelefone, nomeResource);
        }

        // Chama a rotina de login manual, quando for identificado que o usuário já existe no sistema.
        function chamarLogin() {
            EdupsInscricoesService.chamarLogin(self.objInscricao);
        }
	}
});
