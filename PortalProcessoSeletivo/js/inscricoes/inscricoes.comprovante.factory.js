/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsInscricoesComprovanteModule
* @name edupsInscricoesComprovanteFactory
* @object factory
*
* @created 23/01/2017 v12.1.15
* @updated
*
* @requires edupsInscricoesComprovanteModule
*
* @description Factory utilizada na funcionalidade de comprovante de Inscrições do Processo Seletivo.
*/
define(['inscricoes/inscricoes.comprovante.module'], function () {

    'use strict';

    angular
        .module('edupsInscricoesComprovanteModule')
        .factory('edupsInscricoesComprovanteFactory', edupsInscricoesComprovanteFactory);

    edupsInscricoesComprovanteFactory.$inject = ['$totvsresource'];

    function edupsInscricoesComprovanteFactory($totvsresource) {

        var self = this,
            url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS;

        self.gerarComprovante = gerarComprovante;

        return self;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        //Retorna o objeto factory a partir da URL padrão juntamento ao complemeto
        function getInstanciaFactory(urlComplemento) {
            var urlCompletaServico = url + urlComplemento;
            return $totvsresource.REST(urlCompletaServico, {}, {});
        }

        function gerarComprovante(numeroInscricao, callback) {
            var parametros = {};
            parametros.numeroInscricao = numeroInscricao;

            var factory = getInstanciaFactory('/Inscricao/Comprovante');

            return factory.TOTVSQuery(parametros, callback);
        }
   }
});
