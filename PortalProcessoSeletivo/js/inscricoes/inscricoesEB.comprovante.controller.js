/**
 * @license TOTVS | Portal Processo Seletivo v12.1.18
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsInscricoesEBComprovanteModule
 * @name EdupsInscricoesComprovanteController
 * @object controller
 *
 * @created 18/08/2017 v12.1.18
 * @updated
 *
 * @requires inscricoes.comprovante.module
 *
 * @dependencies edupsInscricoesComprovanteFactory
 *
 * @description Controller da funcionalidade Comprovante de Inscrições do Processo Seletivo (ENSINO BÁSICO)
 */
define(['inscricoes/inscricoes.comprovante.module',
        'inscricoes/inscricoes.comprovante.factory',
        'utils/reports/edups-relatorio.service'],
function () {

    'use strict';

    angular
        .module('edupsInscricoesComprovanteModule')
        .controller('EdupsInscricoesEBComprovanteController', EdupsInscricoesEBComprovanteController);

    EdupsInscricoesEBComprovanteController.$inject = [
        '$rootScope',
        '$scope',
        '$state',
        '$window',
        '$sce',
        'edupsUtilsService',
        'EduPsRelatorioService',
        'edupsInscricoesComprovanteFactory'
    ];

    function EdupsInscricoesEBComprovanteController(
        $rootScope,
        $scope,
        $state,
        $window,
        $sce,
        EdupsUtilsService,
        EduPsRelatorioService,
        EdupsInscricoesComprovanteFactory
    ) {
        var self = this;

        self.exibirLayout = true;

        // Variável utilizada na página principal para retirar cabeçalho, banners e rodapé quando
        // a página for aberta através do link de geração de comprovante
        $rootScope.TelaLimpa = true;

        EdupsInscricoesComprovanteFactory.gerarComprovante(
            $state.params.numeroInscricao,
            function (result) {
                if (angular.isDefined(result) && result[0]['TOTVSReport']) {
                    self.exibirLayout = false;
                    self.exibirObjChrome = !!window.chrome && !!window.chrome.webstore;

                    if (self.exibirObjChrome) {
                        var blob = EdupsUtilsService.b64toBlob(result[0]['TOTVSReport'], 'application/pdf');
                        var fileURL = URL.createObjectURL(blob);
                        self.pdfcontent = $sce.trustAsResourceUrl(fileURL);                    
                    } else 
                        EduPsRelatorioService.exibirOuSalvarPDFParams(result[0]['TOTVSReport'], '_self');
                }
                else if (angular.isDefined(result) && result[0]['HTMLFixo']) {
                    self.comprovante = $sce.trustAsHtml(result[0]['HTMLFixo']);
                }
                else {
                    self.exibirLayout = false;
                }
            });
    }
});

