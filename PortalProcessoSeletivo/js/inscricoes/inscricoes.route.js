/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsInscricoesModule
 * @object routeConfig
 *
 * @created 14/09/2016 v12.1.15
 * @updated
 *
 * @dependencies $stateProvider
 *
 * @description Rotas da funcionalidade Inscrições do Processo Seletivo
 */
define(['inscricoes/inscricoes.module', 'utils/edups-utils.globais'], function() {

    'use strict';

    angular
        .module('edupsInscricoesModule')
        .config(edupsInscricoesRouteConfig);

    edupsInscricoesRouteConfig.$inject = ['$stateProvider'];

    function edupsInscricoesRouteConfig($stateProvider) {
        $stateProvider.state('inscricoes', {
            abstract: true,
            template: '<ui-view/>'

        }).state('inscricoes.start', {
            url: '/:nivelEnsino/inscricoes',
            controller: 'EdupsInscricoes' + document.eduPSNivelEnsino.toUpperCase() + 'Controller',
            controllerAs: 'controller',
            templateUrl: function() {

                var utilGlobais = requirejs('utils/edups-utils.globais');
                var baseTemplate = 'js/templates/inscricoes-';

                if (utilGlobais.validaViewCustomizada('inscricoes.start')) {
                    baseTemplate = 'js/templates/custom/inscricoes-';
                }

                return baseTemplate + document.eduPSNivelEnsino + '-edit.view.html';
            },
            params: {
                retorno: false,
                confirmacao: false,
                objInscricao: null,
                inscricaoAposLogin: false,
				novoCandidato: false,
				novoDependente: false,
				codUsuarioPSDependente: null
            },
            resolve: {
                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsInscricoesModule',
                        files: ['js/inscricoes/inscricoes' + document.eduPSNivelEnsino.toUpperCase() + '.controller.js']
                    }]);
                }]
            }
        }).state('inscricoes.comprovante', {
            url: '/:nivelEnsino/inscricoes/comprovante?{numeroInscricao:int}',
            controller: 'EdupsInscricoes' + document.eduPSNivelEnsino.toUpperCase() + 'ComprovanteController',
            controllerAs: 'controller',
            templateUrl: 'js/templates/inscricoes-' + document.eduPSNivelEnsino + '-comprovante.view.html',
            params: {
                objInscricao: null
            },
            resolve: {
                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsInscricoesComprovanteModule',
                        files: ['js/inscricoes/inscricoes' + document.eduPSNivelEnsino.toUpperCase() + '.comprovante.controller.js']
                    }]);
                }]
            }
        }).state('inscricoesWizard', {
            url: '/:nivelEnsino/inscricoeswizard',
            controller: 'EdupsInscricoes' + document.eduPSNivelEnsino.toUpperCase() + 'Controller',
            controllerAs: 'controller',
            templateUrl: function() {

                    var utilGlobais = requirejs('utils/edups-utils.globais');
                    var baseTemplate = 'js/templates/inscricoes-';

                    if (utilGlobais.validaViewCustomizada('inscricoesWizard')) {
                        baseTemplate = 'js/templates/custom/inscricoes-';
                    }

                    return baseTemplate + document.eduPSNivelEnsino + '-wizard.view.html';
                },
                params: {
                    retorno: false,
                    confirmacao: false,
                    objInscricao: null,
                    inscricaoAposLogin: false,
                    novoCandidato: false,
                    novoDependente: false,
                    codUsuarioPSDependente: null
                },
                resolve: {
                    lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'edupsInscricoesModule',
                            files: ['js/inscricoes/inscricoes' + document.eduPSNivelEnsino.toUpperCase() + '.controller.js']
                        }]);
                    }]
                }
        })
        .state('inscricoesWizard.dados-basicos', {
            url: '/dados-basicos',
            templateUrl: 'js/templates/forms/frmDadosBasicos.html'
        })
        .state('inscricoesWizard.responsaveis', {
            url: '/responsaveis',
            templateUrl: 'js/templates/forms/frmDadosResponsaveis.html'
        })
        .state('inscricoesWizard.dados-curso', {
            url: '/dados-curso',
            templateUrl: 'js/templates/forms/frmDadosCurso.html'
        });
    }
});
