/**
 * @license TOTVS | Portal Processo Seletivo v12.1.18
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsInscricoesModule
 * @name EdupsInscricoesService
 * @object service
 *
 * @created 26/07/2017 v12.1.18
 * @updated 16/11/2018 v12.1.23
 *
 * @requires
 *
 * @description Service da funcionalidade Inscrições do Processo Seletivo, utilizada por todos os níveis de ensino.
 *              Todos os métodos considerados comuns entre os níveis de ensino devem ser colocados aqui e consumidos
 *              pelos controllers específicos de cada nível de ensino.
 */

define(['inscricoes/inscricoes.module',
    'inscricoes/inscricoes.factory',
    'utils/edups-utils.factory',
    'utils/edups-enums.constants'
    ], function() {

    'use strict';

    angular
        .module('edupsInscricoesModule')
        .service('EdupsInscricoesService', EdupsInscricoesService);

    EdupsInscricoesService.$inject = [
        '$rootScope',
        '$compile',
        '$state',
        '$filter',
        'i18nFilter',
        '$location',
        'totvs.app-notification.Service',
        'edupsInscricoesFactory',
        'edupsUtilsFactory',
        'edupsApiUnis',        
        'edupsEnumsConsts',
        '$timeout'
    ];

    function EdupsInscricoesService(
        $rootScope,
        $compile,
        $state,
        $filter,
        i18nFilter,
        $location,
        totvsNotification,
        EdupsInscricoesFactory,
        EdupsUtilsFactory,
        EdupsApiUnis,
        edupsEnumsConsts,
        $timeout
    ) {

        var self = this;
        self.getListaTipoDocumento = getListaTipoDocumento;
        self.getListaAreaOfertada = getListaAreaOfertada;
        self.getListaGruposAreaInteresse = getListaGruposAreaInteresse;
        self.getListaPapeisRelacResponsavelFin = getListaPapeisRelacResponsavelFin;
        self.getListaPapeisRelacResponsavelAcad = getListaPapeisRelacResponsavelAcad;
        self.getListaPapeisRelacPais = getListaPapeisRelacPais;
        self.getInformacoesInscricao = getInformacoesInscricao;
        self.getInformacoesPolo = getInformacoesPolo;
        self.getListaCampusLocalRealizacaoProva = getListaCampusLocalRealizacaoProva;
        self.getListaEstadoCivil = getListaEstadoCivil;
        self.getListaNacionalidade = getListaNacionalidade;
        self.getListaCorRaca = getListaCorRaca;
        self.getListaGrauInstrucao = getListaGrauInstrucao;
        self.getListaTipoSanguineo = getListaTipoSanguineo;
        self.getListaTipoRua = getListaTipoRua;
        self.getListaTipoBairro = getListaTipoBairro;
        self.getListaProfissao = getListaProfissao;
        self.getListaPaises = getListaPaises;
        self.getListaEstados = getListaEstados;
        self.getListaMunicipios = getListaMunicipios;
        self.getListaSituacaoMilitar = getListaSituacaoMilitar;
        self.getListaTipoDeficiencia = getListaTipoDeficiencia;
        self.getListaAtividadesAgendadas = getListaAtividadesAgendadas;
        self.getListaTabelaDinamicaItem = getListaTabelaDinamicaItem;
        self.getModelCampo = getModelCampo;
        self.getListaDocumentosExigidosAndOfertaOnline = getListaDocumentosExigidosAndOfertaOnline;
        self.getDocumentosExigidosInvalid = getDocumentosExigidosInvalid;
        self.showPaymentDetail = showPaymentDetail;
        self.inicializaValoresDefault = inicializaValoresDefault;
        self.limpaCamposAreaInteresse = limpaCamposAreaInteresse;
        self.limpaCamposFormaInscricao = limpaCamposFormaInscricao;
        self.separaBarraServico = separaBarraServico;
        self.campoVisivel = campoVisivel;
        self.campoObrigatorio = campoObrigatorio;
        self.campoDocumentoObrigatorio = campoDocumentoObrigatorio;
        self.efetuarLogin = efetuarLogin;
        self.isDefinedNotNull = isDefinedNotNull;
        self.exibeGrupoCampos = exibeGrupoCampos;
        self.carregarCamposFormulario = carregarCamposFormulario;
        self.setCamposCPFPassRGObrigatorioAlteracaoPais = setCamposCPFPassRGObrigatorioAlteracaoPais;
        self.setCamposCPFPassRGObrigatorio = setCamposCPFPassRGObrigatorio;
        self.campoCPFPassRGObrigatorio = campoCPFPassRGObrigatorio;
        self.criarListaAreaOfertadaOpcional = criarListaAreaOfertadaOpcional;
        self.criarCamposComplementares = criarCamposComplementares;
        self.preencheValorDefaultCampoComplementar = preencheValorDefaultCampoComplementar;
        self.listaCamposComplementaresPorGrupo = listaCamposComplementaresPorGrupo;
        self.buscarInformacoesCandidato = buscarInformacoesCandidato;
        self.validaAtividadesAgendadas = validaAtividadesAgendadas;
        self.buscarIndicacaoPorCodigo = buscarIndicacaoPorCodigo;
        self.trataCampoComplementarParaVisualizacao = trataCampoComplementarParaVisualizacao;
        self.booleanToString = booleanToString;
        self.sexoPorExtenso = sexoPorExtenso;
        self.camposObrigatoriosCandidatoPreenchidos = camposObrigatoriosCandidatoPreenchidos;
        self.camposObrigatoriosAreaInteressePreenchidos = camposObrigatoriosAreaInteressePreenchidos;
        self.camposObrigatoriosAtividadesAgendadas = camposObrigatoriosAtividadesAgendadas;
        self.verificaFormulariosEmBranco = verificaFormulariosEmBranco;
        self.buscarEnderecoPorCEP = buscarEnderecoPorCEP;
        self.copiarEnderecoCandidato = copiarEnderecoCandidato;
        self.tipoResponsavelAcademico = tipoResponsavelAcademico;
        self.tipoResponsavelFinanceiro = tipoResponsavelFinanceiro;
        self.tipoResponsavelInscricao = tipoResponsavelInscricao;
        self.validaObrigatoriedadeIdiomas = validaObrigatoriedadeIdiomas;
        self.validaVisibilidadeIdiomas = validaVisibilidadeIdiomas;
        self.carregaInfoFormaInscricao = carregaInfoFormaInscricao;
        self.atualizaInformacoesUsuario = atualizaInformacoesUsuario;
        self.BuscaTermoAceitePS = BuscaTermoAceitePS;
        self.getLabelCampoNome = getLabelCampoNome;
		self.buscaDadosUsuario = buscaDadosUsuario;
		self.buscaDadosDependente = buscaDadosDependente;
        self.getTextoSimNao = getTextoSimNao;
        self.concatenaName = concatenaName;
        self.chamarLogin = chamarLogin;

        self.retornarEtapaAtual = retornarEtapaAtual;
        self.avancarEtapa = avancarEtapa;
        self.retrocederEtapa = retrocederEtapa;
        self.liberarProximaEtapa = liberarProximaEtapa;
        self.liberarEtapaAtual = liberarEtapaAtual;
        self.restringirProximasEtapas = restringirProximasEtapas;
        self.verificarStatusEtapaAtualAtivo = verificarStatusEtapaAtualAtivo;
        self.verificarRedirecionamentoValido = verificarRedirecionamentoValido;
        self.redirecionar = redirecionar;
        self.inicializarWizardPS = inicializarWizardPS;
        self.notificaInteracao = notificaInteracao;
        self.notificaInteracaoEvento = notificaInteracaoEvento;

        self.objParseToJSON = objParseToJSON;

        /*flag para controlar se exibe ou não o campo polo*/
        $rootScope.criarCampoPolo = false;
        $rootScope.listaPolo = [];

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
            * Busca os dados pessoais do usuário
            * @param {any} callback - função de retorno
            * @returns
            */
        function buscaDadosUsuario(callback) {
            EdupsInscricoesFactory.getDadosUsuario(function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
		}

        /**
            * Busca os dados pessoais do dependente
            * @param {any} callback - função de retorno
            * @returns
            */
		function buscaDadosDependente(codUsuarioPSDependente, callback) {
			EdupsInscricoesFactory.getDadosDependente(codUsuarioPSDependente, function(result) {
				if (result) {
					if (angular.isFunction(callback)) {
						callback(result);
					}
				}
			});
		}

        function inicializaValoresDefault(dadosUsuario) {
            dadosUsuario.CODUSUARIOPS = null;
            dadosUsuario.CANHOTO = false;
            dadosUsuario.FUMANTE = false;
            dadosUsuario.CONJUGEBRASIL = false;
            dadosUsuario.NATURALIZADO = false;
            dadosUsuario.NIT = false;
            dadosUsuario.CODCOLIGADA = $rootScope.CodColigada;
            dadosUsuario.IDPS = $rootScope.IdPS;
            dadosUsuario.TIPORUAOBJ = {};
        }

        function getLabelCampoNome(objParametros, nivelEnsino, souCandidato) {
            var sufixo = '';

            if (nivelEnsino === 'eb') {
                if (souCandidato) {
                    sufixo = '-candidato';
                } else {
                    sufixo = '-responsavel';
                }
            }

            var label = $filter('i18n')('l-nome' + sufixo, [], 'js/inscricoes');

            return label;
        }

        function getListaTipoDocumento() {
            var objTipoDocumento = {
                listaTipoDocumento: [],
                tipoDocumentoBuscaCandidato: ''
            };

            objTipoDocumento.listaTipoDocumento.push({
                CODIGO: '1',
                DESCRICAO: i18nFilter('l-documentos-cpf', [], 'js/inscricoes')
            });
            objTipoDocumento.listaTipoDocumento.push({
                CODIGO: '2',
                DESCRICAO: i18nFilter('l-documentos-passaporte', [], 'js/inscricoes')
            });
            objTipoDocumento.listaTipoDocumento.push({
                CODIGO: '3',
                DESCRICAO: i18nFilter('l-documentos-registrogeral', [], 'js/inscricoes')
            });

            if (objTipoDocumento.listaTipoDocumento.length > 0) {
                objTipoDocumento.tipoDocumentoBuscaCandidato = objTipoDocumento.listaTipoDocumento[0].CODIGO;
            }

            return objTipoDocumento;
        }

        function getListaAreaOfertada(codColigada, idPS, codGrupo, objInscricao, callback) {

            if (angular.isUndefined(codGrupo) || codGrupo == null) {
                codGrupo = '-1';
            }

            EdupsUtilsFactory.getListaAreaOfertada(codColigada, idPS, codGrupo, function(result) {
                if (angular.isDefined(result) && angular.isDefined(result.SPSAreaOfertada)) {
                    objInscricao.listas.listaAreaOfertada = result.SPSAreaOfertada;

                    objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ = null;

                    // Se houver somente um registro ele é selecionado automaticamente
                    if (isDefinedNotNull(objInscricao.listas.listaAreaOfertada) && objInscricao.listas.listaAreaOfertada.length === 1) {
                        if ((!$rootScope.UsaGrupoAreaInteressePS) || ($rootScope.UsaGrupoAreaInteressePS && isDefinedNotNull(objInscricao.dadosInscricaoAreaOfertada.GRUPOPSOBJ))) {
                            objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ = objInscricao.listas.listaAreaOfertada[0];
                        }
                    }

                    if (angular.isFunction(callback) && angular.isObject(result.SPSAreaOfertada)) {
                        callback(result.SPSAreaOfertada);
                    }
                }
            });
        }

        function getListaGruposAreaInteresse(codColigada, idPS, objInscricao, callback) {
            EdupsUtilsFactory.getListaGruposAreaInteresse(codColigada, idPS, function(result) {

                if (isDefinedNotNull(result) && isDefinedNotNull(result.SPSAREAINTERESSE)) {
                    objInscricao.listas.listaGruposAreaInteresse = result.SPSAREAINTERESSE;

                    // Se houver somente um registro ele é selecionado automaticamente
                    if (isDefinedNotNull(objInscricao.listas.listaGruposAreaInteresse) && objInscricao.listas.listaGruposAreaInteresse.length === 1) {
                        objInscricao.dadosInscricaoAreaOfertada.GRUPOPSOBJ = objInscricao.listas.listaGruposAreaInteresse[0];
                    }

                    if (angular.isFunction(callback) && angular.isObject(result.SPSAREAINTERESSE)) {
                        callback(result.SPSAREAINTERESSE);
                    }
                } else {
                    objInscricao.listas.listaGruposAreaInteresse = [];
                }
            });
        }

        /**
            * @function Limpa os campos que dependem da área de interesse e do grupo de interesse
            * @param {any} objInscricao objeto de inscrição
            */
        function limpaCamposAreaInteresse(objInscricao) {
            objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ = null;
            objInscricao.listas.listaDropDownAreaOfertadaOpcional = [];
            objInscricao.dadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ = null;
            objInscricao.dadosIdiomaInscrito.IDIOMAOBJ = null;
            objInscricao.listas.listaIdiomasOpcionais = [];
            objInscricao.dadosIdiomaInscrito.IDIOMAOPCIONALOBJ = null;
            objInscricao.listas.listaAreaOfertadaOpcional = [];

            limpaCamposFormaInscricao(objInscricao);
        }

        /**
            * @function Limpa os campos relacionados a forma de inscrição
            * @param {any} objInscricao objeto de inscrição
            */
        function limpaCamposFormaInscricao(objInscricao) {
            objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ = null;
            objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado = null;
            objInscricao.dadosInscricaoAreaOfertada.CAMPUSOBJ = null;
            $rootScope.ValorInscricaoFormatCurrency = null;
            objInscricao.exibeGrupoDadosEnem = false;
            objInscricao.campoEnemObrigatorio = false;
            objInscricao.exibeIndicacaoAluno = false;
            objInscricao.exibeIndicacaoFornecedor = false;
            objInscricao.exibeIndicacaoFuncProfessor = false;
        }

        function separaBarraServico() {
            $rootScope.ExibeServicoAlteracaoInformacoesPessoais = ($rootScope.objParametros.ServicoDeInscricao.indexOf('0') !== -1);
            $rootScope.ExibeServicoAlteracaoSenha = ($rootScope.objParametros.ServicoDeInscricao.indexOf('1') !== -1);
            $rootScope.ExibeServicoBoletoPagtoInscricao = ($rootScope.objParametros.ServicoDeInscricao.indexOf('2') !== -1);
            $rootScope.ExibeServicoCartaoPagtoInscricao = ($rootScope.objParametros.ServicoDeInscricao.indexOf('3') !== -1);
            $rootScope.ExibeServicoSegundaViaComprovante = ($rootScope.objParametros.ServicoDeInscricao.indexOf('4') !== -1);
            $rootScope.ExibeServicoConsultaSituacaoCandidato = ($rootScope.objParametros.ServicoDeInscricao.indexOf('5') !== -1);

            $rootScope.ExibeBarraServico = $rootScope.ExibeServicoAlteracaoInformacoesPessoais ||
                $rootScope.ExibeServicoAlteracaoSenha ||
                $rootScope.ExibeServicoBoletoPagtoInscricao ||
                $rootScope.ExibeServicoCartaoPagtoInscricao ||
                $rootScope.ExibeServicoSegundaViaComprovante ||
                $rootScope.ExibeServicoConsultaSituacaoCandidato;
        }

        //Valida a visibilidade dos campos da tela de acordo com os parâmetros, e o tipo (Candidato ou Responsáveis)
        function campoVisivel(idCampo, tipoUsuario) {
            var result = [],
                mostraCampo = false;

            if ((isDefinedNotNull($rootScope.objParametros)) && (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {
                result = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.IdCampo === idCampo);
                });

                if (angular.isArray(result) && result.length > 0) {
                    switch (tipoUsuario) {
                        case 'C': //Candidato
                            mostraCampo = result[0].VisivelCandidato;
                            break;
                        case 'R': //Reponsável
                            mostraCampo = result[0].VisivelResponsavel;
                            break;
                        default:
                            mostraCampo = false;
                            break;
                    }
                }
            }
            return mostraCampo;
        }

        //Valida a obrigatoriedade dos campos da tela de acordo com os parâmetros, e o tipo (Candidato ou Responsáveis)
        function campoObrigatorio(idCampo, tipoUsuario, objInscricao) {
            var result = [],
                obrigatorio;
            if ((isDefinedNotNull($rootScope.objParametros)) && (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {
                result = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.IdCampo === idCampo);
                });
			}

            if (result.length > 0) {
				switch (tipoUsuario) {
					case 'C': //Candidato
						var obrigatorioResp = result[0].ObrigatorioResponsavel &&
							((objInscricao.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 0 && !$rootScope.objParametros.NaoExigeDadosRespFin) ||
							(objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac === 0 && !$rootScope.objParametros.NaoExigeDadosRespAcad) ||
							($rootScope.nivelEnsino === 'eb' && (objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac === 0)))
						obrigatorio = result[0].ObrigatorioCandidato || obrigatorioResp;
						break;
					case 'P': //Pai
						var validaObrigatoriedade = !$rootScope.objParametros.NaoExigeDadosPai ||
							(objInscricao.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 1 && !$rootScope.objParametros.NaoExigeDadosRespFin) ||
							(objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac === 1 && !$rootScope.objParametros.NaoExigeDadosRespAcad) ||
							($rootScope.nivelEnsino === 'eb' && (objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac === 1 && result[0].ObrigatorioResponsavel))
						obrigatorio = result[0].ObrigatorioResponsavel && validaObrigatoriedade;
						break;
					case 'M': //Mãe
						var validaObrigatoriedade = !$rootScope.objParametros.NaoExigeDadosMae ||
							(objInscricao.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 2 && !$rootScope.objParametros.NaoExigeDadosRespFin) ||
							(objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac === 2 && !$rootScope.objParametros.NaoExigeDadosRespAcad) ||
							($rootScope.nivelEnsino === 'eb' && (objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac === 2 && result[0].ObrigatorioResponsavel))
						obrigatorio = result[0].ObrigatorioResponsavel && validaObrigatoriedade;
						break;
                    case 'RA': //Responsável Acadêmico
						obrigatorio = result[0].ObrigatorioResponsavel && !$rootScope.objParametros.NaoExigeDadosRespAcad;
						break;
					case 'RF': //Responsável Financeiro
						var validaObrigatoriedade = !$rootScope.objParametros.NaoExigeDadosRespFin ||
							(objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac === 3 && !$rootScope.objParametros.NaoExigeDadosRespAcad);
						obrigatorio = result[0].ObrigatorioResponsavel && validaObrigatoriedade;
						break;
					case 'RI': //Responsável Inscrição
						obrigatorio = result[0].ObrigatorioResponsavel;
						break;
                    default:
                        obrigatorio = false;
                        break;
                }

                // Força a obrigatoriedade caso encontre os campos de login (CPF, RG, Email)
                if ((tipoUsuario === 'C' || tipoUsuario === 'RI') && isDefinedNotNull(objInscricao.souCandidato)) {
                    var acesso = [];
                    if (idCampo === 'CPF') {
                        acesso = $.grep($rootScope.listaTipoIdentificacaoLogin, function (e) {
                            return (e.CODIGO === '0')
                        });

                        if (acesso.length > 0) {
                            obrigatorio = alteraObrigatoriedade(objInscricao.souCandidato, tipoUsuario);
                        }
                    }

                    if (idCampo === 'RGNumero') {
                        acesso = $.grep($rootScope.listaTipoIdentificacaoLogin, function (e) {
                            return (e.CODIGO === '1')
                        });

                        if (acesso.length > 0) {
                            obrigatorio = alteraObrigatoriedade(objInscricao.souCandidato, tipoUsuario);
                        }
                    }

                    if (idCampo === 'Email') {
                        acesso = $.grep($rootScope.listaTipoIdentificacaoLogin, function (e) {
                            return (e.CODIGO === '3')
                        });

                        if (acesso.length > 0) {
                            obrigatorio = alteraObrigatoriedade(objInscricao.souCandidato, tipoUsuario);
                        }
                    }
                }
            }

            return obrigatorio;
        }

        function alteraObrigatoriedade(souCandidato, tipoUsuario) {
            if ($rootScope.nivelEnsino === 'eb') {
                if (souCandidato) {
                    return (tipoUsuario === 'C');
                }
                else {
                    return (tipoUsuario === 'RI')
                }
            }
            else {
                return true;
            }
        }

        function getListaPapeisRelacResponsavelFin(objInscricao) {

			var listaPapeisRelacResponsavelFin = [];

            // Carrega a opção do candidato apenas se o mesmo não possuir responsável pela inscrição.
            if ($rootScope.nivelEnsino === 'es' || objInscricao.souCandidato) {
                listaPapeisRelacResponsavelFin.push({
                    CODIGO: 0,
                    DESCRICAO: i18nFilter('l-candidato', [], 'js/inscricoes')
                });
            } else {
                listaPapeisRelacResponsavelFin.push({
                    CODIGO: 4,
                    DESCRICAO: i18nFilter('l-resp-insc', [], 'js/inscricoes')
                });
            }

            if ($rootScope.objParametros.UsaDadosPai) {
                listaPapeisRelacResponsavelFin.push({
                    CODIGO: 1,
                    DESCRICAO: i18nFilter('l-pai', [], 'js/inscricoes')
                });
            }

            if ($rootScope.objParametros.UsaDadosMae) {
                listaPapeisRelacResponsavelFin.push({
                    CODIGO: 2,
                    DESCRICAO: i18nFilter('l-mae', [], 'js/inscricoes')
                });
            }

            return listaPapeisRelacResponsavelFin;
        }

        function getListaPapeisRelacResponsavelAcad(objInscricao) {

            var listaPapeisRelacResponsavelAcad = [];

            // Carrega a opção do candidato apenas se o mesmo não possuir responsável pela inscrição.
            if ($rootScope.nivelEnsino === 'es' || objInscricao.souCandidato) {
                listaPapeisRelacResponsavelAcad.push({
                    CODIGO: 0,
                    DESCRICAO: i18nFilter('l-candidato', [], 'js/inscricoes')
                });
            } else {
                listaPapeisRelacResponsavelAcad.push({
                    CODIGO: 4,
                    DESCRICAO: i18nFilter('l-resp-insc', [], 'js/inscricoes')
                });
            }

            if ($rootScope.objParametros.UsaDadosPai) {
                listaPapeisRelacResponsavelAcad.push({
                    CODIGO: 1,
                    DESCRICAO: i18nFilter('l-pai', [], 'js/inscricoes')
                });
            }

            if ($rootScope.objParametros.UsaDadosMae) {
                listaPapeisRelacResponsavelAcad.push({
                    CODIGO: 2,
                    DESCRICAO: i18nFilter('l-mae', [], 'js/inscricoes')
                });
            }

            if ($rootScope.objParametros.UsaDadosRespFin) {
                listaPapeisRelacResponsavelAcad.push({
                    CODIGO: 3,
                    DESCRICAO: i18nFilter('l-resp-fin', [], 'js/inscricoes')
                });
            }

            return listaPapeisRelacResponsavelAcad;
        }

        function getListaPapeisRelacPais() {
            var listaPapeisRelacPais = [];

            listaPapeisRelacPais.push({
                CODIGO: 4,
                DESCRICAO: i18nFilter('l-resp-insc', [], 'js/inscricoes')
            });

            return listaPapeisRelacPais;
        }

        //Se não há nenhum campo para ser exibido no grupo, então o grupo não será exibido
        function exibeGrupoCampos(idGrupoCampo, tipoUsuario) {
            var arrayGrupos = [],
                grupoVisivel = false;

            if ((isDefinedNotNull($rootScope.objParametros)) && (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {
                arrayGrupos = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.GrupoCampo === idGrupoCampo);
                });
            }

            if (arrayGrupos.length > 0) {
                arrayGrupos.forEach(function(item) {
                    switch (tipoUsuario) {
                        case 'C': //Candidato
                            grupoVisivel = (grupoVisivel || item.VisivelCandidato || exibeGrupoCamposComplementares(idGrupoCampo));
                            break;
                        case 'R': //Responsável
                            grupoVisivel = (grupoVisivel || item.VisivelResponsavel);
                            break;
                    }
                });
            }

            return grupoVisivel;
        }

        //Se não há nenhum campo complementar para ser exibido no grupo, então o grupo não será exibido
        function exibeGrupoCamposComplementares(idGrupoCampo) {
            var arrayGrupos = [];
            arrayGrupos = $.grep(listaCamposComplementaresPorGrupo(idGrupoCampo), function(e) {
                return (e.GrupoCampo === idGrupoCampo);
            });

            return arrayGrupos.length > 0;
        }

        function listaCamposComplementaresPorGrupo(grupo) {
            if (isDefinedNotNull($rootScope.objParametros.ListaCampoComplFilialGrupo)) {
                if ($rootScope.objParametros.ListaCampoComplFilialGrupo !== null) {
                    return $.grep($rootScope.objParametros.ListaCampoComplFilialGrupo, function(e) {
                        return (e.GrupoCampo === grupo);
                    });
                }
            }
        }

        function carregarCamposFormulario(dadosFormulario) {
            var utilizaOutrosDados = true;

            if (isDefinedNotNull(dadosFormulario)) {
                if (dadosFormulario.UsaDadosTipoRelac === null ||
                    dadosFormulario.UsaDadosTipoRelac < 0) {

                    utilizaOutrosDados = false;
                }
            }

            return !utilizaOutrosDados;
        }

        function setCamposCPFPassRGObrigatorioAlteracaoPais(tipoUsuario, objInscricao) {
            setCamposCPFPassRGObrigatorio('EstrangeirosRegistroGeral', tipoUsuario, objInscricao);
            setCamposCPFPassRGObrigatorio('PassaporteNumero', tipoUsuario, objInscricao);
            setCamposCPFPassRGObrigatorio('CPF', tipoUsuario, objInscricao);
        }

        // Seta a variavel de controle para definicao de obrigatoriedade dos campos CPF/Passaporte/Registro Geral para estrangeiros
        function setCamposCPFPassRGObrigatorio(idCampo, tipoUsuario, objInscricao) {

            if (angular.isDefined(objInscricao.camposCPFPassRGObrigatorio)) {
                var obrigatorio = validaCamposCPFPassRGObrigatorio(idCampo, tipoUsuario, objInscricao);
                var blnIsEstrangeiroObrigarCPFPassaporteRG = ((isDefinedNotNull($rootScope.objParametros) && $rootScope.objParametros.ObrigarCPFPassaporteRG) &&
                    (isEstrangeiro(tipoUsuario, objInscricao)) &&
                    (angular.isArray($rootScope.objParametros.ListaParametrosPessoa)));

                if (blnIsEstrangeiroObrigarCPFPassaporteRG) {
                    objInscricao.camposCPFPassRGObrigatorio[tipoUsuario]['EstrangeirosRegistroGeral'] = obrigatorio;
                    objInscricao.camposCPFPassRGObrigatorio[tipoUsuario]['PassaporteNumero'] = obrigatorio;
                    objInscricao.camposCPFPassRGObrigatorio[tipoUsuario]['CPF'] = obrigatorio;
                } else {
                    objInscricao.camposCPFPassRGObrigatorio[tipoUsuario][idCampo] = obrigatorio;
                }
            }
        }

        // Utilizado no ng-required dos campos de CPF, Passaporte e RG Estrangeiro.
        function campoCPFPassRGObrigatorio(idCampo, tipoUsuario, objInscricao) {
            setCamposCPFPassRGObrigatorio(idCampo, tipoUsuario, objInscricao);
            if (objInscricao.camposCPFPassRGObrigatorio)
                return objInscricao.camposCPFPassRGObrigatorio[tipoUsuario][idCampo];
            else
                return null;
        }

        // Valida se os campos CPF/Passaporte/Registro Geral foram preenchidos quando o parâmetro 'ObrigacaoCPFPassaporteRG' estiver marcado e os campos estiverem visiveis
        // Validação realizada para estrangeiros
        function validaCamposCPFPassRGObrigatorio(idCampo, tipoUsuario, objInscricao) {

            var result = [],
                obrigatorio = false,
                candidatoOuResp = tipoUsuario === 'C' ? 'C' : 'R';

            if ((isDefinedNotNull($rootScope.objParametros) && $rootScope.objParametros.ObrigarCPFPassaporteRG) &&
                (isEstrangeiro(tipoUsuario, objInscricao)) &&
                (angular.isArray($rootScope.objParametros.ListaParametrosPessoa))) {

                result = $.grep($rootScope.objParametros.ListaParametrosPessoa, function(e) {
                    return (e.IdCampo === 'EstrangeirosRegistroGeral' || e.IdCampo === 'PassaporteNumero' || e.IdCampo === 'CPF');
                });

                if (result.length > 0) {

                    var campoPreenchido = false;
                    result.forEach(function(item) {

                        switch (tipoUsuario) {
                            case 'C': //Candidato
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosUsuario)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;

                            case 'P': //Pai
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosPai)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;

                            case 'M': //Mãe
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosMae)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;

                            case 'RA': //Responsável Acadêmico
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosResponsavelAcademico)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;

                            case 'RF': //Responsável Financeiro
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosResponsavelFinanceiro)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;
                            case 'RI': //Responsável Inscrição
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosResponsavelInscricao)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;
                            default:
                                if (campoVisivel(item.IdCampo, candidatoOuResp)) {
                                    if (campoPreenchidoCPFPassRG(item.IdCampo, objInscricao.dadosUsuario)) {
                                        campoPreenchido = true;
                                    }
                                }
                                break;
                        }
                    });

                    obrigatorio = !campoPreenchido;
                }
            } else {
                obrigatorio = campoDocumentoObrigatorio(idCampo, tipoUsuario, objInscricao);
            }

            return obrigatorio;
        }

        /* Serão considerados estrangeiros usuários que tiverem o país diferente do país de origem do processo seletivo da inscrição
            Se o campo não estiver visível não será realizada a validação e retornará false */
        function isEstrangeiro(tipoUsuario, objInscricao) {

            var estrangeiro = false,
                candidatoOuResp = tipoUsuario === 'C' ? 'C' : 'R';

            // Para a visibilidade ou ele é candidato ou responsável
            if (campoVisivel('Pais', candidatoOuResp)) {

                switch (tipoUsuario) {
                    case 'C': //Candidato
                        estrangeiro = paisDiferenteOrigemPS(objInscricao.dadosUsuario);
                        break;
                    case 'P': //Pai
                        estrangeiro = paisDiferenteOrigemPS(objInscricao.dadosPai);
                        break;
                    case 'M': //Mãe
                        estrangeiro = paisDiferenteOrigemPS(objInscricao.dadosMae);
                        break;
                    case 'RA': //Responsável Acadêmico
                        estrangeiro = paisDiferenteOrigemPS(objInscricao.dadosResponsavelAcademico);
                        break;
                    case 'RF': //Responsável Financeiro
                        estrangeiro = paisDiferenteOrigemPS(objInscricao.dadosResponsavelFinanceiro);
                        break;
                    case 'RI': //Responsável Inscrição
                        estrangeiro = paisDiferenteOrigemPS(objInscricao.dadosResponsavelInscricao);
                        break;
                    default:
                        estrangeiro = false;
                        break;
                }
            }

            return estrangeiro;
        }

        function campoPreenchidoCPFPassRG(idCampo, dadosUsuario) {

            var campoPreenchido = false;

            switch (idCampo) {
                case 'EstrangeirosRegistroGeral':
                    if (isDefinedNotNull(dadosUsuario.NROREGGERAL) && dadosUsuario.NROREGGERAL !== '') {
                        campoPreenchido = true;
                    }
                    break;

                case 'PassaporteNumero':
                    if (isDefinedNotNull(dadosUsuario.NPASSAPORTE) && dadosUsuario.NPASSAPORTE !== '') {
                        campoPreenchido = true;
                    }
                    break;

                case 'CPF':
                    if (isDefinedNotNull(dadosUsuario.CPF) && dadosUsuario.CPF !== '') {
                        campoPreenchido = true;
                    }
                    break;
                default:
                    campoPreenchido = false;
                    break;
            }

            return campoPreenchido;
        }

        function paisDiferenteOrigemPS(dadosUsuario) {
            return (isDefinedNotNull(dadosUsuario) &&
                isDefinedNotNull(dadosUsuario.PAISESENDERECO) &&
                dadosUsuario.PAISESENDERECO.IDPAIS !== null &&
                dadosUsuario.PAISESENDERECO.IDPAIS !== $rootScope.objParametros.PaisOrigemPS);
        }

        /* Valida a obrigatoriedade dos campos de documentos verificando o parâmetro de desobrigação para Estrangeiros
            Serão considerados estrangeiros usuários que tiverem o país diferente do país de origem do processo seletivo da inscrição */
        function campoDocumentoObrigatorio(idCampo, tipoUsuario, objInscricao) {

            var obrigatorio = campoObrigatorio(idCampo, tipoUsuario, objInscricao);

            switch (tipoUsuario) {
                case 'C': //Candidato
                    if (isEstrangeiro(tipoUsuario, objInscricao) && $rootScope.objParametros.NaoObrigaDocCandEst) {
                        obrigatorio = false;
                    }
                    break;

                default: // Responsáveis
                    if (isEstrangeiro(tipoUsuario, objInscricao) && $rootScope.objParametros.NaoObrigaDocRespEst) {
                        obrigatorio = false;
                    }
                    break;
            }

            return obrigatorio;
        }

        function getInformacoesInscricao(codColigada, idPS, areaInteresseObj, objInscricao, blnUtilizaVagaExcedente) {
            if (angular.isUndefined(areaInteresseObj) || areaInteresseObj === null) {
                limpaCamposAreaInteresse(objInscricao);
            } else {
                // Verifica se o processo seletivo trabalha com vaga excedente
                EdupsUtilsFactory.verificaVagaExcedente(codColigada, idPS, areaInteresseObj.IDAREAINTERESSE,
                    function(result) {
                        if (result && isDefinedNotNull(result.UTILIZAVAGAEXCEDENTE) && result.UTILIZAVAGAEXCEDENTE) {
                            blnUtilizaVagaExcedente = true;

                            var textMsg = i18nFilter('l-msg-inscricao-excedente', [], 'js/inscricoes');
                            textMsg = textMsg.replace('[[NUMEXCEDENTE]]', result.NUMEXCEDENTE + 1);

                            totvsNotification.question({
                                title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                                text: textMsg,
                                cancelLabel: 'l-no',
                                size: 'md', // sm = small | md = medium | lg = larger
                                confirmLabel: 'l-yes',
                                callback: function(isPositiveResult) {
                                    if (isPositiveResult) {
                                        carregaListasAreaInteresse(codColigada, idPS, areaInteresseObj, objInscricao);
                                    } else {
                                        limpaCamposAreaInteresse(objInscricao);
                                    }
                                }
                            });
                        } else {
                            blnUtilizaVagaExcedente = false;
                            carregaListasAreaInteresse(codColigada, idPS, areaInteresseObj, objInscricao);
                        }
                    });
            }
        }

        function getInformacoesPolo(codColigada, idPS, areaInteresseObj, objInscricao, blnUtilizaVagaExcedente) {

            var dadosEnvio = {
                "CODCOLIGADA": codColigada,
                "IDPS": idPS,
                "IDAREAINTERESSE": areaInteresseObj.IDAREAINTERESSE
            };

            EdupsApiUnis.get("buscaPolos", {'data': angular.toJson(dadosEnvio)}, function(data, status) {
                $rootScope.listaPolo = [];
                angular.forEach(data, function(item){
                    $rootScope.listaPolo.push(item);
                });
                // return callback(data);
            });
        }

        function carregaListasAreaInteresse(codColigada, idPS, areaInteresseObj, objInscricao) {
            getListaIdioma(codColigada, idPS, areaInteresseObj.IDAREAINTERESSE, objInscricao);

            getListaFormaInscricao(codColigada, idPS, objInscricao, function(result) {

                if (isDefinedNotNull(result) && result.length === 1) {
                    carregaInfoFormaInscricao(objInscricao);
                }
            });

            getListaAreaOfertadaOpcional(objInscricao);
            getListaIdiomaOpcional(objInscricao);
            getOutrosDadosInscricao(objInscricao);
        }

        function getListaIdioma(codColigada, idPS, idAreaInteresse, objInscricao, callback) {

            EdupsUtilsFactory.getListaIdioma(codColigada, idPS, idAreaInteresse, function(result) {

                if (isDefinedNotNull(result) && isDefinedNotNull(result.SPSIDIOMAAREAOFERTADA)) {
                    objInscricao.listas.listaIdioma = result.SPSIDIOMAAREAOFERTADA;

                    objInscricao.dadosIdiomaInscrito.IDIOMAOBJ = null;

                    // Se houver somente um registro ele é selecionado automaticamente
                    if (isDefinedNotNull(objInscricao.listas.listaIdioma) && objInscricao.listas.listaIdioma.length === 1) {
                        objInscricao.dadosIdiomaInscrito.IDIOMAOBJ = objInscricao.listas.listaIdioma[0];
                    }
                }

                if (angular.isFunction(callback) && angular.isArray(result.SPSIDIOMAAREAOFERTADA)) {
                    callback(result.SPSIDIOMAAREAOFERTADA);
                }
            });
        }

        function getListaFormaInscricao(codColigada, idPS, objInscricao, callback) {
            EdupsUtilsFactory.getListaFormaInscricao(codColigada, idPS, function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.SPSFormaInscricaoPS)) {
                    objInscricao.listas.listaFormaInscricao = result.SPSFormaInscricaoPS;
                }

                objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ = null;

                // Se houver somente um registro ele é selecionado automaticamente
                if (isDefinedNotNull(objInscricao.listas.listaFormaInscricao) && objInscricao.listas.listaFormaInscricao.length === 1) {
                    objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ = objInscricao.listas.listaFormaInscricao[0];
                }

                if (angular.isFunction(callback) && angular.isArray(result.SPSFormaInscricaoPS)) {
                    callback(result.SPSFormaInscricaoPS);
                }
            });
        }

        function carregaInfoFormaInscricao(objInscricao) {

            if (angular.isUndefined(objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ) || objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ === null) {
                limpaCamposFormaInscricao(objInscricao);
            } else {
                calculaValorInscricao(objInscricao);

                getOutrosDadosInscricao(objInscricao);

                getListaLocalRealizacaoProva($rootScope.CodColigada, $rootScope.IdPS, objInscricao, function(result) {
                    if (isDefinedNotNull(result) && result.length === 1) {
                        getListaCampusLocalRealizacaoProva($rootScope.CodColigada, $rootScope.IdPS, objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado.CODMUNICIPIO, objInscricao);
                    }
                });
            }
        }

        function getListaAreaOfertadaOpcional(objInscricao) {
            objInscricao.listas.listaDropDownAreaOfertadaOpcional = [];
            objInscricao.dadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ = null;

            if (isDefinedNotNull(objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ) && objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.NUMMAXAREASOPCIONAIS > 0) {
                for (var i = 0; i < objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.NUMMAXAREASOPCIONAIS; i++) {
                    objInscricao.listas.listaDropDownAreaOfertadaOpcional.push({});
                }
            }

            criarListaAreaOfertadaOpcional(objInscricao);
        }

        function getListaIdiomaOpcional(objInscricao) {
            objInscricao.listas.listaIdiomasOpcionais = [];
            objInscricao.dadosIdiomaInscrito.IDIOMAOPCIONALOBJ = [];

            if (isDefinedNotNull(objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ)) {
                var i = 0,
                    numMaxIdiomasInscricao = objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.NUMMAXIDIOMASINSCRICAO;

                if (numMaxIdiomasInscricao > 1) {
                    objInscricao.listas.listaIdiomasOpcionais = [{}];
                    for (i = 1; i < numMaxIdiomasInscricao - 1; i++) {
                        objInscricao.listas.listaIdiomasOpcionais.push({});
                    }
                }
            }
        }

        function getOutrosDadosInscricao(objInscricao) {

            // Verifica se irá exibir grupo "Dados do Enem"
            objInscricao.exibeGrupoDadosEnem = isDefinedNotNull(objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ) ? (objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.UTILIZAENEM === 'T') : false;
            var formaInscricaoSelecinada = objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ;

            // Verifica se a forma de inscrição obriga o preenchimento dos campos do ENEM
            if (formaInscricaoSelecinada != null && formaInscricaoSelecinada.OBRIGATORIOENEM != null) {
                objInscricao.campoEnemObrigatorio = (formaInscricaoSelecinada.OBRIGATORIOENEM === 'T');
            } else {
                objInscricao.campoEnemObrigatorio = false;
            }

            //Verifica se irá exibir indicação por "aluno"
            if (formaInscricaoSelecinada != null && formaInscricaoSelecinada.INDICACAOALUNO != null) {
                objInscricao.exibeIndicacaoAluno = (formaInscricaoSelecinada.INDICACAOALUNO === 'T');
            } else {
                objInscricao.exibeIndicacaoAluno = false;
            }

            //Verifica se irá exibir indicação por "cliente/fornecedor"
            if (formaInscricaoSelecinada != null && formaInscricaoSelecinada.INDICACAOCLIFOR != null) {
                objInscricao.exibeIndicacaoFornecedor = (formaInscricaoSelecinada.INDICACAOCLIFOR === 'T');
            } else {
                objInscricao.exibeIndicacaoFornecedor = false;
            }

            //Verifica se irá exibir indicação por "funcionário/professor"
            if (formaInscricaoSelecinada != null && formaInscricaoSelecinada.INDICACAOFUNCPROF != null) {
                objInscricao.exibeIndicacaoFuncProfessor = (formaInscricaoSelecinada.INDICACAOFUNCPROF === 'T');
            } else {
                objInscricao.exibeIndicacaoFuncProfessor = false;
            }
        }

        function calculaValorInscricao(objInscricao) {

            if (isDefinedNotNull(objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ)) {

                var percentualDesconto = 0;

                //Verifica se área ofertada "Incide desconto" e se foi selecionado "Forma de inscrição"
                if (objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.INCIDEDESCONTO === 'T' && objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ != null) {

                    percentualDesconto = objInscricao.dadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ.PERCDESCONTOMINIMO;
                }

                // Define o valor da inscrição definida na área ofertada
                $rootScope.ValorInscricao = objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.VALORINSCRICAO;

                // Calcula percentual de desconto pela forma de inscrição selecionada
                if (percentualDesconto > 0) {
                    var valorComDesconto = $rootScope.ValorInscricao * (percentualDesconto / 100);
                    $rootScope.ValorInscricao = parseFloat($rootScope.ValorInscricao) - parseFloat(valorComDesconto);
                }

                $rootScope.ValorInscricaoAnt = $rootScope.ValorInscricao;

                // Formata o valor de inscrição para exibir formatado
                $rootScope.ValorInscricaoFormatCurrency = $filter('currency')($rootScope.ValorInscricao);
            }
        }

        function getListaLocalRealizacaoProva(codColigada, idPS, objInscricao, callback) {
            objInscricao.listas.listaLocalRealizacaoProva = [];
            objInscricao.listas.listaCampusLocalRealizacaoProva = [];

            EdupsUtilsFactory.getListaLocalRealizacaoProva(codColigada, idPS, function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.SPSLOCALPROVA)) {
                    objInscricao.listas.listaLocalRealizacaoProva = result.SPSLOCALPROVA;

                    objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado = null;

                    // Se houver somente um registro ele é selecionado automaticamente
                    if (isDefinedNotNull(objInscricao.listas.listaLocalRealizacaoProva) && objInscricao.listas.listaLocalRealizacaoProva.length === 1) {
                        objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado = objInscricao.listas.listaLocalRealizacaoProva[0];
                    }

                    if (angular.isFunction(callback) && angular.isArray(result.SPSLOCALPROVA)) {
                        callback(result.SPSLOCALPROVA);
                    }
                } else if (angular.isFunction(callback)) {
                    callback(objInscricao.listas.listaLocalRealizacaoProva);
                }
            });
        }

        function getListaCampusLocalRealizacaoProva(codColigada, idPS, codMunicipio, objInscricao, callback) {
            objInscricao.dadosInscricaoAreaOfertada.CAMPUSOBJ = null;
            objInscricao.listas.listaCampusLocalRealizacaoProva = [];

            if (isDefinedNotNull(codMunicipio)) {

                EdupsUtilsFactory.getListaCampusLocalRealizacaoProva(codColigada, idPS, codMunicipio, function(result) {
                    if (isDefinedNotNull(result) && isDefinedNotNull(result.SPSCAMPUSLOCALPROVA)) {
                        objInscricao.listas.listaCampusLocalRealizacaoProva = result.SPSCAMPUSLOCALPROVA;
                    }

                    objInscricao.dadosInscricaoAreaOfertada.CAMPUSOBJ = null;

                    // Se houver somente um registro ele é selecionado automaticamente
                    if (isDefinedNotNull(objInscricao.listas.listaCampusLocalRealizacaoProva) && objInscricao.listas.listaCampusLocalRealizacaoProva.length === 1) {
                        objInscricao.dadosInscricaoAreaOfertada.CAMPUSOBJ = objInscricao.listas.listaCampusLocalRealizacaoProva[0];
                    }

                    /*Atualiza campos de local de realização da prova*/
                    if (objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado) {
                        objInscricao.dadosInscricaoAreaOfertada.CODMUNICIPIO = objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado.CODMUNICIPIO;
                        objInscricao.dadosInscricaoAreaOfertada.CODETDMUNICIPIO = objInscricao.dadosInscricaoAreaOfertada.LocalProvaSelecionado.CODETDMUNICIPIO;
                    }

                    if (angular.isFunction(callback) && angular.isArray(result.SPSCAMPUSLOCALPROVA)) {
                        callback(result.SPSCAMPUSLOCALPROVA);
                    }
                });
            }
        }

        function criarListaAreaOfertadaOpcional(objInscricao) {

            for (var j = 0; j < objInscricao.listas.listaDropDownAreaOfertadaOpcional.length; j++) {

                objInscricao.listas.listaAreaOfertadaOpcional[j] = retornaListaAreaOfertada(j, objInscricao);
            }
        }

        function retornaListaAreaOfertada(indexOfAreaOfertadaOpcional, objInscricao) {
            var lista = [];

            for (var i = 0; i < objInscricao.listas.listaAreaOfertada.length; i++) {

                if (objInscricao.listas.listaAreaOfertada[i].IDAREAINTERESSE !== objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE) {

                    if (objInscricao.dadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ) {

                        var itemValido = true;

                        for (var propertyName in objInscricao.dadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ) {

                            if ((objInscricao.dadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ[propertyName] && objInscricao.dadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ[propertyName].IDAREAINTERESSE === objInscricao.listas.listaAreaOfertada[i].IDAREAINTERESSE) &&
                                (indexOfAreaOfertadaOpcional !== parseInt(propertyName))) {
                                itemValido = false;
                                break;
                            }
                        }

                        if (itemValido) {
                            lista.push(objInscricao.listas.listaAreaOfertada[i]);
                        }
                    } else {
                        lista.push(objInscricao.listas.listaAreaOfertada[i]);
                    }
                }
            }
            return lista;
        }

        function getListaEstadoCivil(objInscricao, callback) {
            objInscricao.listas.listaEstadoCivil = [];
            EdupsUtilsFactory.getListaEstadoCivil(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.PCODESTCIVIL)) {
                    objInscricao.listas.listaEstadoCivil = result.PCODESTCIVIL;
                    if (angular.isArray(objInscricao.listas.listaEstadoCivil) && objInscricao.listas.listaEstadoCivil.length > 0) {
                        objInscricao.listas.listaEstadoCivil = $filter('orderBy')(objInscricao.listas.listaEstadoCivil, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.PCODESTCIVIL)) {
                    callback(result.PCODESTCIVIL);
                }
            });
        }

        function getListaNacionalidade(objInscricao, callback) {
            objInscricao.listas.listaNacionalidade = [];
            EdupsUtilsFactory.getListaNacionalidade(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.PCODNACAO)) {
                    objInscricao.listas.listaNacionalidade = result.PCODNACAO;
                    if (angular.isArray(objInscricao.listas.listaNacionalidade) && objInscricao.listas.listaNacionalidade.length > 0) {
                        objInscricao.listas.listaNacionalidade = $filter('orderBy')(objInscricao.listas.listaNacionalidade, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.PCODNACAO)) {
                    callback(result.PCODNACAO);
                }
            });
        }

        function getListaCorRaca(objInscricao, callback) {
            objInscricao.listas.listaCorRaca = [];
            EdupsUtilsFactory.getListaCorRaca(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.PCORRACA)) {
                    objInscricao.listas.listaCorRaca = result.PCORRACA;
                    if (angular.isArray(objInscricao.listas.listaCorRaca) && objInscricao.listas.listaCorRaca.length > 0) {
                        objInscricao.listas.listaCorRaca = $filter('orderBy')(objInscricao.listas.listaCorRaca, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.PCORRACA)) {
                    callback(result.PCORRACA);
                }
            });
        }

        function getListaGrauInstrucao(objInscricao, callback) {
            objInscricao.listas.listaGrauInstrucao = [];
            EdupsUtilsFactory.getListaGrauInstrucao(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.PCODINSTRUCAO)) {
                    objInscricao.listas.listaGrauInstrucao = result.PCODINSTRUCAO;
                    if (angular.isArray(objInscricao.listas.listaGrauInstrucao) && objInscricao.listas.listaGrauInstrucao.length > 0) {
                        objInscricao.listas.listaGrauInstrucao = $filter('orderBy')(objInscricao.listas.listaGrauInstrucao, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.PCODINSTRUCAO)) {
                    callback(result.PCODINSTRUCAO);
                }
            });
        }

        function getListaTipoSanguineo(objInscricao, callback) {
            objInscricao.listas.listaTipoSanguineo = [];
            EdupsUtilsFactory.getListaTipoSanguineo(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.STIPOSANGUINEO)) {
                    objInscricao.listas.listaTipoSanguineo = result.STIPOSANGUINEO;
                    if (angular.isArray(objInscricao.listas.listaTipoSanguineo) && objInscricao.listas.listaTipoSanguineo.length > 0) {
                        objInscricao.listas.listaTipoSanguineo = $filter('orderBy')(objInscricao.listas.listaTipoSanguineo, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.STIPOSANGUINEO)) {
                    callback(result.STIPOSANGUINEO);
                }
            });
        }

        function getListaTipoRua(objInscricao, callback) {
            objInscricao.listas.listaTipoRua = [];
            EdupsUtilsFactory.getListaTipoRua(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.DTipoRua)) {
                    objInscricao.listas.listaTipoRua = result.DTipoRua;
                    if (angular.isArray(objInscricao.listas.listaTipoRua) && objInscricao.listas.listaTipoRua.length > 0) {
                        objInscricao.listas.listaTipoRua = $filter('orderBy')(objInscricao.listas.listaTipoRua, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.DTipoRua)) {
                    callback(result.DTipoRua);
                }
            });
        }

        function getListaTipoBairro(objInscricao, callback) {
            objInscricao.listas.listaTipoBairro = [];
            EdupsUtilsFactory.getListaTipoBairro(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.DTipoBairro)) {
                    objInscricao.listas.listaTipoBairro = result.DTipoBairro;
                    if (angular.isArray(objInscricao.listas.listaTipoBairro) && objInscricao.listas.listaTipoBairro.length > 0) {
                        objInscricao.listas.listaTipoBairro = $filter('orderBy')(objInscricao.listas.listaTipoBairro, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.DTipoBairro)) {
                    callback(result.DTipoBairro);
                }
            });
        }

        function getListaProfissao(objInscricao, callback) {
            objInscricao.listaProfissao = [];
            EdupsUtilsFactory.getListaProfissao(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.EProfiss)) {
                    objInscricao.listaProfissao = result.EProfiss;
                    if (angular.isArray(objInscricao.listaProfissao) && objInscricao.listaProfissao.length > 0) {
                        objInscricao.listaProfissao = $filter('orderBy')(objInscricao.listaProfissao, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.EProfiss)) {
                    callback(result.EProfiss);
                }
            });
        }

        function getListaPaises(objInscricao, callback) {
            objInscricao.listas.listaEstados = [];

            EdupsUtilsFactory.getListaPaises(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.GPais)) {
                    objInscricao.listas.listaPaises = result.GPais;
                    if (angular.isArray(objInscricao.listas.listaPaises) && objInscricao.listas.listaPaises.length > 0) {
                        objInscricao.listas.listaPaises = $filter('orderBy')(objInscricao.listas.listaPaises, 'DESCRICAO');
                    }
                }
                if (angular.isFunction(callback) && angular.isArray(result.GPais)) {
                    callback(result.GPais);
                }
            });
        }

        function getListaEstados(idPais, objInscricao, callback) {
            if (idPais) {
                EdupsUtilsFactory.getListaEstados(idPais, function(result) {
                    if (isDefinedNotNull(result) && isDefinedNotNull(result.GEtd)) {
                        objInscricao.listas.listaEstados = result.GEtd;
                    }
                    if (angular.isFunction(callback) && angular.isArray(result.GEtd)) {
                        callback(result.GEtd);
                    }
                });
            }
        }

        function getListaMunicipios(codEtd, callback) {
            if (codEtd) {
                EdupsUtilsFactory.getListaMunicipios(codEtd, function(result) {
                    if (isDefinedNotNull(result) && isDefinedNotNull(result.GMUNICIPIO) &&
                        angular.isFunction(callback) && angular.isArray(result.GMUNICIPIO)) {
                        callback(result.GMUNICIPIO);
                    }
                });
            }
        }

        function getListaSituacaoMilitar(objInscricao) {

            objInscricao.listas.listaSituacaoMilitar = [];
            objInscricao.listas.listaSituacaoMilitar.push({
                CODIGO: i18nFilter('l-sitmilitar-alistado', [], 'js/inscricoes'),
                DESCRICAO: i18nFilter('l-sitmilitar-alistado', [], 'js/inscricoes')
            });
            objInscricao.listas.listaSituacaoMilitar.push({
                CODIGO: i18nFilter('l-sitmilitar-dispensado', [], 'js/inscricoes'),
                DESCRICAO: i18nFilter('l-sitmilitar-dispensado', [], 'js/inscricoes')
            });
            objInscricao.listas.listaSituacaoMilitar.push({
                CODIGO: i18nFilter('l-sitmilitar-reservista', [], 'js/inscricoes'),
                DESCRICAO: i18nFilter('l-sitmilitar-reservista', [], 'js/inscricoes')
            });
        }

        function getListaTipoDeficiencia(objInscricao, callback) {
            EdupsUtilsFactory.getListaTipoDeficiencia(function(result) {
                if (isDefinedNotNull(result) && isDefinedNotNull(result.VDEFICIENCIA)) {
                    objInscricao.listas.listaDefFisica = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 1);
                    });

					objInscricao.listas.listaDefAuditiva = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 2);
                    });

					objInscricao.listas.listaDefFala = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 3);
                    });

                    objInscricao.listas.listaDefVisual = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 4);
					});

					objInscricao.listas.listaDefMental = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 5);
					});

					objInscricao.listas.listaDefIntelectual = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 6);
					});

					objInscricao.listas.listaDefReabilitado = $.grep(result.VDEFICIENCIA, function(e) {
                        return (e.CODTIPODEFICIENCIA === 7);
                    });
				}

				if (angular.isFunction(callback) && angular.isArray(result.VDEFICIENCIA)) {
					callback(result.VDEFICIENCIA, result['InformacoesComplementares']);
                }
            });
        }

        function getListaAtividadesAgendadas(codColigada, idPS, objInscricao) {
            EdupsUtilsFactory.getListaAtividadesAgendadas(codColigada, idPS, function(result) {
                if (isDefinedNotNull(result)) {
                    objInscricao.listas.listaAtividadesAgendadas = result;
                } else {
                    objInscricao.listas.listaAtividadesAgendadas = [];
                }
            });
        }

        function getListaTabelaDinamicaItem(objParametros, listaTabelaDinamicaItem, callback) {
            listaTabelaDinamicaItem = {};

            if (isDefinedNotNull(objParametros.ListaTabelaDinamicaItem) && objParametros.ListaTabelaDinamicaItem !== null) {
                var tabelaDinamica = '';
                for (var i = 0; i < objParametros.ListaTabelaDinamicaItem.length; i++) {
                    var item = objParametros.ListaTabelaDinamicaItem[i];
                    if (tabelaDinamica !== item.CodTabela) {
                        listaTabelaDinamicaItem[item.CodTabela] = [];
                        tabelaDinamica = item.CodTabela;
                    }
                    listaTabelaDinamicaItem[item.CodTabela].push(angular.copy(item));
                }
            }

            if (angular.isFunction(callback)) {
                callback(listaTabelaDinamicaItem);
            }
        }

        function criarCamposComplementares(grupoCampos, listaTabelaDinamicaItem, scope) {
            var listaCampos = listaCamposComplementaresPorGrupo(grupoCampos);
            if (angular.isArray(listaCampos)) {
                for (var i = 0; i < listaCampos.length; i++) {

                     //se campo polo
                    if ( listaCampos[i].NomeColuna == "POLO_EAD" ){
                        $rootScope.criarCampoPolo = true;
                        delete listaCampos[i];
                        continue;
                    }

                    var template = retornaDiretivaTOTVSPorTipoCampo(listaCampos[i], i, listaTabelaDinamicaItem, grupoCampos),
                        diretivaTotvs = $compile(template),
                        containerDiv = document.getElementById(grupoCampos);

                    diretivaTotvs = diretivaTotvs(scope);

                    angular.element(containerDiv).append(diretivaTotvs);
                }
            }
        }

        function preencheValorDefaultCampoComplementar(grupoCampos, dadosCamposComplementaresInscricao) {
            var listaCampos = listaCamposComplementaresPorGrupo(grupoCampos);
            if (angular.isArray(listaCampos)) {
                for (var i = 0; i < listaCampos.length; i++) {
                    var valorDefault = listaCampos[i].ValorDefault;

                    if (isDefinedNotNull(valorDefault)) {

                        switch (listaCampos[i].TipoDaColuna) {
                            case 0: //campo input tipo char
                            case 4: //campo input tipo text
                                dadosCamposComplementaresInscricao[listaCampos[i].NomeColuna] = valorDefault;
                                break;
                            case 1: //campo input tipo data
                                dadosCamposComplementaresInscricao[listaCampos[i].NomeColuna] = $filter('date')(valorDefault, 'dd/MM/yyyy');
                                break;
                            case 2: //campo input tipo double | decimal
                                dadosCamposComplementaresInscricao[listaCampos[i].NomeColuna] = $filter('number')(valorDefault.replace(',','.'), 2);
                                break;
                            case 3: //campo input tipo int
                            case 5: //campo input tipo int16
                                dadosCamposComplementaresInscricao[listaCampos[i].NomeColuna] = parseInt(valorDefault);
                                break;
                            case 6: //campo input tipo checkbox
                                dadosCamposComplementaresInscricao[listaCampos[i].NomeColuna] = stringToBoolean(valorDefault);
                                break;
                        }
                    }
                }
            }
        }

        function retornaDiretivaTOTVSPorTipoCampo(item, posicaoItemArray, listaTabelaDinamicaItem, grupoCampo) {

            var diretiva = '';

            //campo input select
            if (item.CodTabDinam) {
                diretiva = getTOTVSFieldTemplateSelect(item, posicaoItemArray, listaTabelaDinamicaItem, grupoCampo);
            } else {
                switch (item.TipoDaColuna) {
                    case 0: //campo input tipo char
                        diretiva = getTOTVSFieldTemplateInput(item, posicaoItemArray, grupoCampo);
                        break;
                    case 1: //campo input tipo data
                        diretiva = getTOTVSFieldTemplateDate(item, posicaoItemArray, grupoCampo);
                        break;
                    case 2: //campo input tipo double | decimal
                        diretiva = getTOTVSFieldTemplateDecimal(item, posicaoItemArray, grupoCampo);
                        break;
                    case 3: //campo input tipo int
                        diretiva = getTOTVSFieldTemplateNumber(item, posicaoItemArray, grupoCampo);
                        break;
                    case 4: //campo input tipo text
                        diretiva = getTOTVSFieldTemplateTextArea(item, posicaoItemArray, grupoCampo);
                        break;
                    case 5: //campo input tipo int16
                        diretiva = getTOTVSFieldTemplateNumber(item, posicaoItemArray, grupoCampo);
                        break;
                    case 6: //campo input tipo checkbox
                        diretiva = getTOTVSFieldTemplateCheckBox(item, posicaoItemArray, grupoCampo);
                        break;
                }
            }

            return diretiva;
        }

        function getTOTVSFieldTemplateTextArea(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';
            template += '<div class="row form-group">';
            template += ' <field type="textarea"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' maxlength="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').TamanhoDaColuna}}"';
            template += ' >';
            template += '</field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateSelect(itemCampo, posicaoItemArray, listaTabelaDinamicaItem, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<field';
            template += ' type="combo"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' canclean';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna + 'OBJ';
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';

            /*campo confirmação de e-mail/telefone */
            /*if ( itemCampo.NomeColuna=="EMAIL_CONFIRMACAO" || itemCampo.NomeColuna=="CELULAR_CONFIRMACAO" )
                template += ' autocomplete="off"';*/

            if (listaTabelaDinamicaItem && isDefinedNotNull(listaTabelaDinamicaItem)) {

                if (isDefinedNotNull(listaTabelaDinamicaItem[itemCampo.CodTabDinam]) &&
                    angular.isArray(listaTabelaDinamicaItem[itemCampo.CodTabDinam])) {

                    template += ' ng-options="{1} as {2} for item in {3}"';
                    template = template.replaceAllSplitAndJoin('{1}', ' item');
                    template = template.replaceAllSplitAndJoin('{2}', ' item.Descricao');
                    template = template.replaceAllSplitAndJoin('{3}', 'controller.listaTabelaDinamicaItem[\'' + itemCampo.CodTabDinam + '\'] ');
                }
            }

            template += '>';
            template += '</field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;

        }

        function getTOTVSFieldTemplateInput(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';
            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' name="$NAME_CAMPO"';
            template += ' totvs-input';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' maxlength="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').TamanhoDaColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateDate(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' name="$NAME_CAMPO"';
            template += ' totvs-datepicker';
            template += ' class="col-lg-4 col-md-4 col-sm-4 col-xs-12"';
            template += ' canclean';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateDecimal(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' totvs-decimal';
            template += ' name="$NAME_CAMPO"';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' m-dec="2" a-dec=","  a-sep="." ';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateNumber(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<totvs-field';
            template += ' totvs-number';
            template += ' name="$NAME_CAMPO"';
            template += ' canclean';
            template += ' class="col-lg-4 col-md-4 col-sm-4 col-xs-12"';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').Obrigatorio"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</totvs-field>';
            template += '</div>';

            template = template.replace('$NAME_CAMPO', getModelCampo(posicaoItemArray, grupoCampo).DscNomeColuna);

            return template;
        }

        function getTOTVSFieldTemplateCheckBox(itemCampo, posicaoItemArray, grupoCampo) {

            var template = '';

            template += '<div class="row form-group">';
            template += '<field ';
            template += ' type="checkbox"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-model=controller.objInscricao.dadosCamposComplementaresInscricao.' + itemCampo.NomeColuna;
            template += ' ng-if="true"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ',\'' + grupoCampo + '\').DscNomeColuna}}"';
            template += ' >';
            template += '</field>';
            template += '</div>';

            return template;
        }

        /* Busca informações do candidato para complementar o formulário de inscrição com dados já existentes na base de dados */
        function buscarInformacoesCandidato(objInscricao) {
            if ($rootScope.objParametros.GrupoPesquisa1 || $rootScope.objParametros.GrupoPesquisa2 || $rootScope.objParametros.GrupoPesquisa3) {

                var nome, dtNascimento, estadoMini, municipioMini, CPF, passaporte, nregistro, email;

                if ($rootScope.nivelEnsino === 'eb' && !objInscricao.souCandidato) {
                    nome = objInscricao.dadosResponsavelInscricao.NOME;
                    dtNascimento = objInscricao.dadosResponsavelInscricao.DTNASCIMENTO;
                    estadoMini = objInscricao.dadosResponsavelInscricao.ESTADOSMINI;
                    municipioMini = objInscricao.dadosResponsavelInscricao.MUNICIPIOSMINI;
                    CPF = objInscricao.dadosResponsavelInscricao.CPF;
                    passaporte = objInscricao.dadosResponsavelInscricao.NPASSAPORTE;
                    nregistro = objInscricao.dadosResponsavelInscricao.NROREGGERAL;
                    email = objInscricao.dadosResponsavelInscricao.EMAIL;
                } else {
                    nome = objInscricao.dadosUsuario.NOME;
                    dtNascimento = objInscricao.dadosUsuario.DTNASCIMENTO;
                    estadoMini = objInscricao.dadosUsuario.ESTADOSMINI;
                    municipioMini = objInscricao.dadosUsuario.MUNICIPIOSMINI;
                    CPF = objInscricao.dadosUsuario.CPF;
                    passaporte = objInscricao.dadosUsuario.NPASSAPORTE;
                    nregistro = objInscricao.dadosUsuario.NROREGGERAL;
                    email = objInscricao.dadosUsuario.EMAIL;
                }

                EdupsInscricoesFactory.existeUsuario($rootScope.CodColigada, $rootScope.IdPS, nome,
                    dtNascimento, estadoMini, municipioMini, CPF, passaporte, nregistro, email,
					function(result) {


                        //salva os dados na RD
                        var idpsRd = [704, 709, 711];
                        if ( idpsRd.indexOf($rootScope.IdPS) !== -1 ){
                            salvaFormularioBasicoRD(objInscricao, function(response){});
                        }

                        // se exite o usuário, manda para a tela de login
                        if (isDefinedNotNull(result) && angular.isObject(result) && result.value) {

                            $('#modalUsuarioExistente').modal({
                                backdrop: 'static',
                                keyboard: false
                            });

                        }
                    });
            }
        }


        //salva as informações do formulário básico antes de ir para o formulário completo
        function salvaFormularioBasicoRD(objInscricao, callback){
            //prepara os dados para salvar
            var dadosPreCadastro = {
                "NOME": objInscricao.dadosUsuario.NOME,
                "tipoDocumentoBuscaCandidato": self.tipoDocumentoBuscaCandidato,
                "CPF": objInscricao.dadosUsuario.CPF,
                "NPASSAPORTE": objInscricao.dadosUsuario.NPASSAPORTE,
                "NROREGGERAL": objInscricao.dadosUsuario.NROREGGERAL,
                "EMAIL": objInscricao.dadosUsuario.EMAIL,
                "DTNASCIMENTO": $filter('date')(objInscricao.dadosUsuario.DTNASCIMENTO, "yyyy-MM-dd"),
                "GRUPOPSOBJ": objInscricao.dadosInscricaoAreaOfertada.GRUPOPSOBJ,
                "AREAINTERESSEOBJ": objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ,
                "TELEFONE2": objInscricao.dadosUsuario.TELEFONE2,
                "codCategoria": $rootScope.codCategoria
            };
            
            EdupsApiUnis.get("salvaFormularioBasicoRD", {'data': angular.toJson(dadosPreCadastro)}, 
                function(data, status) {
                    console.log(data);
                    return callback(data);
            });
        }

		// Faz validações nos usuários para determinar se é candidato em inscrições de ES e EB.
		function validaCandidato (usuarios, nivelEnsino) {
			var possuiRespInsc = false;

			if (nivelEnsino === 'eb') {
				for (var i = 0; i < usuarios.length; i++) {
					if (usuarios[i].EHRESPINSC === 'T') {
						possuiRespInsc = true;
						break;
					}
				}
			}

			return !possuiRespInsc;
		}

        // Atualiza o model de dados do candidato e responsáveis com informações obtidas na base de dados
        function atualizaInformacoesUsuario(result, objInscricao) {
            if (isDefinedNotNull(result) && isDefinedNotNull(result.SPSUSUARIO) && result.SPSUSUARIO.length > 0) {

				// A variável souCandidato determina, em um processo seletivo EB, se quem está fazendo a inscrição é ou não ou próprio candidato.
				// No caso do processo seletivo ES, o candidato sempre é quem está fazendo a inscrição.
				// Se vier da tela inicial do portal do processo seletivo, estará preenchida, caso venha da tela central do candidato, estará undefined.
				if (angular.isUndefined(objInscricao.souCandidato)) {
					objInscricao.souCandidato = validaCandidato(result.SPSUSUARIO, $rootScope.nivelEnsino);
				}

                for (var i = 0; i < result.SPSUSUARIO.length; i++) {

                    // Dados do candidato
                    if (result.SPSUSUARIO[i].EHCANDIDATO === 'T' || objInscricao.souCandidato) {

                        //Faz backup das informações que não vem do serviço REST
                        var codColigadaUsuario = objInscricao.dadosUsuario.CODCOLIGADA;
                        var idPSUsuario = objInscricao.dadosUsuario.IDPS;
                        objInscricao.dadosUsuario = result.SPSUSUARIO[i];
                        objInscricao.dadosUsuario.CODCOLIGADA = codColigadaUsuario;
                        objInscricao.dadosUsuario.IDPS = idPSUsuario;

                        trataCamposUsuario(objInscricao.dadosUsuario);

                        objInscricao.dadosUsuario.formularioEmBranco = false;

                        // Seta os valores os campos da tela que são bindados em objetos
                        setaValores(objInscricao.dadosUsuario, 'C', objInscricao);
                    }

                    // Dados do pai
                    if (result.SPSUSUARIO[i].EHPAI === 'T') {

                        objInscricao.dadosPai = result.SPSUSUARIO[i];
                        trataCamposUsuario(objInscricao.dadosPai);
                        objInscricao.dadosUsuario.formularioEmBranco = false;

                        // Seta os valores os campos da tela que são bindados em objetos
                        setaValores(objInscricao.dadosPai, 'R', objInscricao);
                    }
                    // Dados da mãe
                    if (result.SPSUSUARIO[i].EHMAE === 'T') {

                        objInscricao.dadosMae = result.SPSUSUARIO[i];
                        trataCamposUsuario(objInscricao.dadosMae);
                        objInscricao.dadosUsuario.formularioEmBranco = false;

                        // Seta os valores os campos da tela que são bindados em objetos
                        setaValores(objInscricao.dadosMae, 'R', objInscricao);
                    }

                    // Dados do responsável financeiro
                    if (result.SPSUSUARIO[i].EHRESPFIN === 'T') {
                        // Somente atribuirá os valores se o responsável não for o candidato, pai, mãe.
                        if ((result.SPSUSUARIO[i].EHCANDIDATO === 'F') && (result.SPSUSUARIO[i].EHPAI === 'F') && (result.SPSUSUARIO[i].EHMAE === 'F')) {

                            objInscricao.dadosResponsavelFinanceiro = result.SPSUSUARIO[i];

                            trataCamposUsuario(objInscricao.dadosResponsavelFinanceiro);

                            objInscricao.dadosUsuario.formularioEmBranco = false;

                            // Seta os valores os campos da tela que são bindados em objetos
                            setaValores(objInscricao.dadosResponsavelFinanceiro, 'R', objInscricao);
                        }

                        // Seta o valor do Tiporelac para o responsável
                        setaDadosTipoRelac(objInscricao.dadosResponsavelFinanceiro);
                    }

                    // Dados do responsável acadêmico
                    if (result.SPSUSUARIO[i].EHRESPACAD === 'T') {
                        // Somente atribuirá os valores se o responsável não for o candidato, pai, mãe ou financeiro.
                        if ((result.SPSUSUARIO[i].EHCANDIDATO === 'F') && (result.SPSUSUARIO[i].EHPAI === 'F') &&
                            (result.SPSUSUARIO[i].EHMAE === 'F') && (result.SPSUSUARIO[i].EHRESPFIN === 'F')) {

                            objInscricao.dadosResponsavelAcademico = result.SPSUSUARIO[i];

                            trataCamposUsuario(objInscricao.dadosResponsavelAcademico);

                            objInscricao.dadosUsuario.formularioEmBranco = false;

                            // Seta os valores os campos da tela que são bindados em objetos
                            setaValores(objInscricao.dadosResponsavelAcademico, 'R', objInscricao);
                        }

                        // Seta o valor do Tiporelac para o responsável
                        setaDadosTipoRelac(objInscricao.dadosResponsavelAcademico);
                    }

                    // Dados do responsável inscrição
                    if (result.SPSUSUARIO[i].EHRESPINSC === 'T' || (result.SPSUSUARIO.length === 1 && $rootScope.nivelEnsino === 'eb' && !objInscricao.souCandidato)) {

                        //Faz backup das informações que não vem do serviço REST
                        var codColigadaUsuario = objInscricao.dadosUsuario.CODCOLIGADA;
                        var idPSUsuario = objInscricao.dadosUsuario.IDPS;
                        objInscricao.dadosResponsavelInscricao = result.SPSUSUARIO[i];
                        objInscricao.dadosResponsavelInscricao.codColigada = codColigadaUsuario;
                        objInscricao.dadosResponsavelInscricao.idPS = idPSUsuario;
                        objInscricao.dadosResponsavelInscricao.EHCANDIDATO = 'F';

                        trataCamposUsuario(objInscricao.dadosResponsavelInscricao);

                        //inicializaValoresDefault(objInscricao.dadosResponsavelInscricao);
                        objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac = -1;
                        objInscricao.dadosResponsavelInscricao.formularioEmBranco = false;

                        // Seta os valores os campos da tela que são bindados em objetos
                        setaValores(objInscricao.dadosResponsavelInscricao, 'R', objInscricao);

                        // Seta o valor do Tiporelac para o responsável
                        setaDadosTipoRelac(objInscricao.dadosResponsavelInscricao);
                    }
                }

                //Revalida obrigatoriedade dos campos que podem ser usados como login.
                setCamposCPFPassRGObrigatorio('CPF', 'C', objInscricao);
                campoDocumentoObrigatorio('RGNumero', 'C', objInscricao);
                campoObrigatorio('Email', 'C', objInscricao);

                setCamposCPFPassRGObrigatorio('CPF', 'RI', objInscricao);
                campoDocumentoObrigatorio('RGNumero', 'RI', objInscricao);
                campoObrigatorio('Email', 'RI', objInscricao);
            }
        }

        // Seta os valores dos campos bindados em objetos
        function setaValores(dadosUsuario, tipoUsuario, objInscricao) {

            if (campoVisivel('EstadoCivil', tipoUsuario) && isDefinedNotNull(dadosUsuario.ESTADOCIVIL)) {
                dadosUsuario.ESTADOCIVILOBJ = objInscricao.listas.listaEstadoCivil.find(function(item) {
                    if (item.CODCLIENTE === dadosUsuario.ESTADOCIVIL) {
                        return item;
                    }
                });
            }

            /* País Natal */
            if (campoVisivel('EstadoNatal', tipoUsuario) && isDefinedNotNull(dadosUsuario.IDPAISESTADONATAL)) {
                dadosUsuario.PAISES = objInscricao.listas.listaPaises.find(function(item) {
                    if (item.IDPAIS === dadosUsuario.IDPAISESTADONATAL) {
                        return item;
                    }
                });
            }

            if (campoVisivel('EstadoNatal', tipoUsuario) && isDefinedNotNull(dadosUsuario.ESTADONATAL)) {
                var estadoNatal = dadosUsuario.ESTADONATAL;
                
                if (dadosUsuario.IDPAISESTADONATAL === null)
                    dadosUsuario.IDPAISESTADONATAL = edupsEnumsConsts.EdupsTipoEstadoSemPais.EstadoSemPais;

                getListaEstados(dadosUsuario.IDPAISESTADONATAL, objInscricao, function(estados) {
                    dadosUsuario.ESTADOS = estados.find(function(item) {
                        if (item.CODETD === estadoNatal) {
                            return item;
                        }
                    });
                });
            }

            if (campoVisivel('Naturalidade', tipoUsuario) && isDefinedNotNull(dadosUsuario.CODNATURALIDADE)) {
                var codNaturaliade = dadosUsuario.CODNATURALIDADE,
                    estadoNatalMunicipio = dadosUsuario.ESTADONATAL;

                getListaMunicipios(dadosUsuario.ESTADONATAL, function(municipios) {
                    dadosUsuario.MUNICIPIOS = municipios.find(function(item) {
                        if (item.CODMUNICIPIO === codNaturaliade &&
                            item.CODETDMUNICIPIO === estadoNatalMunicipio) {
                            return item;
                        }
                    });
                });
            }
            /* Fim - País Natal */

            /* Carteira de identidade */
            if (campoVisivel('RGEstadoEmissor', tipoUsuario) && isDefinedNotNull(dadosUsuario.PAISCARTIDENT)) {
                dadosUsuario.PAISESCARTIDENT = objInscricao.listas.listaPaises.find(function(item) {
                    if (item.IDPAIS === dadosUsuario.PAISCARTIDENT) {
                        return item;
                    }
                });
            }

            if (campoVisivel('RGEstadoEmissor', tipoUsuario) && isDefinedNotNull(dadosUsuario.UFCARTIDENT)) {
                var estadoCartIdent = dadosUsuario.UFCARTIDENT;
                getListaEstados(dadosUsuario.PAISCARTIDENT, objInscricao, function(estados) {
                    dadosUsuario.ESTADOSCARTIDENT = estados.find(function(item) {
                        if (item.CODETD === estadoCartIdent) {
                            return item;
                        }
                    });
                });
            }
            /* Fim Carteira de Identidade */

            /* Carteira de Trabalho */
            if (campoVisivel('CarteiraTrabalhoUFEmissao', tipoUsuario) && isDefinedNotNull(dadosUsuario.PAISCARTTRAB)) {
                dadosUsuario.PAISCARTTRABOBJ = objInscricao.listas.listaPaises.find(function(item) {
                    if (item.IDPAIS === dadosUsuario.PAISCARTTRAB) {
                        return item;
                    }
                });
            }

            if (campoVisivel('CarteiraTrabalhoUFEmissao', tipoUsuario) && isDefinedNotNull(dadosUsuario.UFCARTTRAB)) {
                var estadoCartTrab = dadosUsuario.UFCARTTRAB;
                getListaEstados(dadosUsuario.PAISCARTTRAB, objInscricao, function(estados) {
                    dadosUsuario.UFCARTTRABOBJ = estados.find(function(item) {
                        if (item.CODETD === estadoCartTrab) {
                            return item;
                        }
                    });
                });
            }
            /* Fim - Carteira de Trabalho  */

            /* Título Eleitoral */
            if (campoVisivel('TituloEleitorUFEmissao', tipoUsuario) && isDefinedNotNull(dadosUsuario.PAISTELEIT)) {
                dadosUsuario.PAISTELEITOBJ = objInscricao.listas.listaPaises.find(function(item) {
                    if (item.IDPAIS === dadosUsuario.PAISTELEIT) {
                        return item;
                    }
                });
            }

            if (campoVisivel('TituloEleitorUFEmissao', tipoUsuario) && isDefinedNotNull(dadosUsuario.ESTELEIT)) {
                var estadoCartEleit = dadosUsuario.ESTELEIT;
                getListaEstados(dadosUsuario.PAISTELEIT, objInscricao, function(estados) {
                    dadosUsuario.ESTELEITOBJ = estados.find(function(item) {
                        if (item.CODETD === estadoCartEleit) {
                            return item;
                        }
                    });
                });
            }
            /* Fim - Titulo Eleitoral  */

            /* Endereço */
            if (campoVisivel('Pais', tipoUsuario) && isDefinedNotNull(dadosUsuario.IDPAIS)) {
                dadosUsuario.PAISESENDERECO = objInscricao.listas.listaPaises.find(function(item) {
                    if (item.IDPAIS === dadosUsuario.IDPAIS) {
                        return item;
                    }
                });
            }

            if (campoVisivel('Estado', tipoUsuario) && isDefinedNotNull(dadosUsuario.ESTADO)) {
                var estadoEndereco = dadosUsuario.ESTADO;
                getListaEstados(dadosUsuario.IDPAIS, objInscricao, function(estados) {
                    dadosUsuario.ESTADOSENDERECO = estados.find(function(item) {
                        if (item.CODETD === estadoEndereco) {
                            return item;
                        }
                    });
                });
            }

            if (campoVisivel('Cidade', tipoUsuario) && isDefinedNotNull(dadosUsuario.CODMUNICIPIO)) {
                var codMunicipio = dadosUsuario.CODMUNICIPIO,
                    estadoMunicipio = dadosUsuario.ESTADO;

                getListaMunicipios(dadosUsuario.ESTADO, function(municipios) {
                    dadosUsuario.MUNICIPIOSENDERECO = municipios.find(function(item) {
                        if (item.CODMUNICIPIO === codMunicipio &&
                            item.CODETDMUNICIPIO === estadoMunicipio) {
                            return item;
                        }
                    });
                });
            }
            /* Fim - Endereco */

            if (campoVisivel('Nacionalidade', tipoUsuario) && isDefinedNotNull(dadosUsuario.NACIONALIDADE)) {
                dadosUsuario.NACIONALIDADEOBJ = objInscricao.listas.listaNacionalidade.find(function(item) {
                    if (item.CODCLIENTE === dadosUsuario.NACIONALIDADE) {
                        return item;
                    }
                });
            }

            if (campoVisivel('Rua', tipoUsuario) && isDefinedNotNull(dadosUsuario.CODTIPORUA)) {
                dadosUsuario.TIPORUAOBJ = objInscricao.listas.listaTipoRua.find(function(item) {
                    if (item.CODIGO === dadosUsuario.CODTIPORUA) {
                        return item;
                    }
                });
            }

            if (campoVisivel('Bairro', tipoUsuario) && isDefinedNotNull(dadosUsuario.CODTIPOBAIRRO)) {
                dadosUsuario.TIPOBAIRROOBJ = objInscricao.listas.listaTipoBairro.find(function(item) {
                    if (item.CODIGO === dadosUsuario.CODTIPOBAIRRO) {
                        return item;
                    }
                });
            }

            if (campoVisivel('CorRaca', tipoUsuario) && isDefinedNotNull(dadosUsuario.CORRACA)) {
                dadosUsuario.CORRACAOBJ = objInscricao.listas.listaCorRaca.find(function(item) {
                    if (item.CODCLIENTE === dadosUsuario.CORRACA) {
                        return item;
                    }
                });
            }

            if (campoVisivel('Profissao', tipoUsuario) && isDefinedNotNull(dadosUsuario.CODPROFISSAO)) {
                dadosUsuario.PROFISSAOOBJ = objInscricao.listaProfissao.find(function(item) {
                    if (item.CODCLIENTE === dadosUsuario.CODPROFISSAO) {
                        return item;
                    }
                });
            }

            if (campoVisivel('GrauIstrucao', tipoUsuario) && isDefinedNotNull(dadosUsuario.GRAUINSTRUCAO)) {
                dadosUsuario.GRAUINSTRUCAOOBJ = objInscricao.listas.listaGrauInstrucao.find(function(item) {
                    if (item.CODCLIENTE === dadosUsuario.GRAUINSTRUCAO) {
                        return item;
                    }
                });
            }

            if (campoVisivel('TipoSanguineo', tipoUsuario) && isDefinedNotNull(dadosUsuario.TIPOSANG)) {
                dadosUsuario.TIPOSANGOBJ = objInscricao.listas.listaTipoSanguineo.find(function(item) {
                    if (item.CODIGO === dadosUsuario.TIPOSANG) {
                        return item;
                    }
                });
            }

            if (campoVisivel('CertificadoReservistaSituacaoMilitar', tipoUsuario) && isDefinedNotNull(dadosUsuario.SITMILITAR)) {
                dadosUsuario.SITMILITAROBJ = objInscricao.listas.listaSituacaoMilitar.find(function(item) {
                    if (item.CODIGO === dadosUsuario.SITMILITAR) {
                        return item;
                    }
                });
            }
        }

        // Seta o valor para o tipo de relacionamento dos responsáveis
        function setaDadosTipoRelac(dadosUsuario) {

            if (dadosUsuario.EHRESPFIN === 'T' || dadosUsuario.EHRESPACAD === 'T') {

                if (dadosUsuario.EHCANDIDATO === 'T') {
                    dadosUsuario.UsaDadosTipoRelac = 0;
                } else if (dadosUsuario.EHPAI === 'T') {
                    dadosUsuario.UsaDadosTipoRelac = 1;
                } else if (dadosUsuario.EHMAE === 'T') {
                    dadosUsuario.UsaDadosTipoRelac = 2;
                } else if (dadosUsuario.EHRESPFIN === 'T' && dadosUsuario.EHRESPACAD === 'T') {
                    dadosUsuario.UsaDadosTipoRelac = 3;
                } else if (dadosUsuario.EHRESPINSC === 'T') {
                    dadosUsuario.UsaDadosTipoRelac = 4;
                } else {
                    dadosUsuario.UsaDadosTipoRelac = -1;
                }
            }
        }

        function trataCamposUsuario(dadosUsuario) {

            dadosUsuario.CANHOTO = stringToBoolean(dadosUsuario.CANHOTO);
            dadosUsuario.FUMANTE = stringToBoolean(dadosUsuario.FUMANTE);
            dadosUsuario.NIT = stringToBoolean(dadosUsuario.NIT);
            dadosUsuario.NATURALIZADO = stringToBoolean(dadosUsuario.NATURALIZADO);
            dadosUsuario.CONJUGEBRASIL = stringToBoolean(dadosUsuario.CONJUGEBRASIL);
        }

        function validaAtividadesAgendadas(objInscricao, callback) {
            var mostraFormularioCompleto;
            if (objInscricao.listas.listaAtividadesAgendadas.length > 0) {
                for (var index = 0; index < objInscricao.listas.listaAtividadesAgendadas.length; index++) {
                    var element = objInscricao.listas.listaAtividadesAgendadas[index];

                    if (element.Horarios.length === 0) {
                        mostraFormularioCompleto = false;

                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: i18nFilter('l-atividade-sem-horario-disponivel', [], 'js/inscricoes')
                        });

                        break;
                    } else {
                        mostraFormularioCompleto = true;
                    }
                }
            } else {
                mostraFormularioCompleto = true;
            }

            if (angular.isFunction(callback)) {
                callback(mostraFormularioCompleto);
            }
        }

        function buscarIndicacaoPorCodigo(tipo, codColigada, codigo, modeloDados) {

            //Inicializa todas opções de indicação
            modeloDados.RA = null;
            modeloDados.RANOME = null;
            modeloDados.CODCFO = null;
            modeloDados.CODCFONOME = null;
            modeloDados.CHAPA = null;
            modeloDados.CHAPANOME = null;

            if (codColigada && codigo) {

                //Busca por aluno
                if (tipo === 'A') {
                    EdupsUtilsFactory.getAluno(codColigada, codigo, function(result) {

                        if (isDefinedNotNull(result) && angular.isArray(result) && result.length > 0) {

                            modeloDados.RA = result[0].RA;
                            modeloDados.RANOME = result[0].NOME;
                        } else {
                            totvsNotification.notify({
                                type: 'warning',
                                title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                                detail: i18nFilter('l-msg-aluno-nao-encontrado', [], 'js/inscricoes')
                            });
                        }
                    });
                }

                //Busca por cliente/fornecedor
                if (tipo === 'C') {
                    EdupsUtilsFactory.getClienteFornecedor(codColigada, codigo, function(result) {

                        if (isDefinedNotNull(result) && angular.isArray(result) && result.length > 0) {
                            modeloDados.CODCOLIGADACFO = result[0].CODCOLIGADA;
                            modeloDados.CODCFO = result[0].CODCFO;
                            modeloDados.CODCFONOME = result[0].NOME;
                        } else {
                            totvsNotification.notify({
                                type: 'warning',
                                title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                                detail: i18nFilter('l-msg-fornecedor-nao-encontrado', [], 'js/inscricoes')
                            });
                        }
                    });
                }

                //Busca por funcionário/professor
                if (tipo === 'F') {
                    EdupsUtilsFactory.getFuncionario(codColigada, codigo, function(result) {
                        if (isDefinedNotNull(result) && angular.isArray(result) && result.length > 0) {
                            modeloDados.CODCOLIGADAFUNC = result[0].CODCOLIGADA;
                            modeloDados.CHAPA = result[0].CHAPA;
                            modeloDados.CHAPANOME = result[0].NOME;
                        } else {
                            totvsNotification.notify({
                                type: 'warning',
                                title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                                detail: i18nFilter('l-msg-funcionario-nao-encontrado', [], 'js/inscricoes')
                            });
                        }
                    });
                }
            }
        }

        /**
            * Método responsável pela conversão do objeto para o formato JSON que será usado no POST.
            *
            * @returns Objeto no formado correto para criação do JSON utilizado no evento POST.
            */
        function objParseToJSON() {

            var modelJSON = {
                    SPSUSUARIO: [],
                    SPSINSCRICAOAREAOFERTADA: [],
                    SPSOPCAOINSCRITO: [],
                    SPSIDIOMAINSCRITO: [],
                    SPSINSCAREAOFERTACOMPL: [],
                    SPSATIVIDADEAGENDADAPS: [],
                    SPSINSCRICAOAREAOFERTADASUBDEF: [],
                    SPSDOCUMENTOSEXIGIDOS: [],
                    SPSOFERTAONLINE: [],
                    SPSCUSTOM: [],
                    SPSETAPA: []
                },
                objDadosUsuario = angular.copy(this.dadosUsuario),
                objDadosInscricaoAreaOfertada = angular.copy(this.dadosInscricaoAreaOfertada),
                objDadosOpcaoInscrito = angular.copy(this.dadosOpcaoInscrito),
                objDadosIdiomaInscrito = angular.copy(this.dadosIdiomaInscrito),
                objDadosDocumentosExigidos = angular.copy(this.dadosDocumentosExigidos),
                senha = objDadosUsuario.SENHA;

            /*Prepara os dados do usuário para enviar*/

            //Se o processo seletivo utilizar senha para login, for ensino básico, e possuir responsável pela inscrição,
            //a senha será gravada para o responsável pela inscrição e não para o candidato.
            if ($rootScope.objParametros.UsaSenhaLogin && $rootScope.nivelEnsino === 'eb' && !this.souCandidato) {
                objDadosUsuario.SENHA = '';
                objDadosUsuario.REPETESENHA = '';
            }

            /*Prepara os dados do candidato para enviar*/
            trataCamposParseJson(objDadosUsuario);

            // Informa que o usuário é um candidato
            objDadosUsuario.EHCANDIDATO = 'T';
            // Informa que o usuário não é pai
            objDadosUsuario.EHPAI = 'F';
            // Informa que o usuário não é mãe
            objDadosUsuario.EHMAE = 'F';
            // Determina se ele é responsável financeiro
            objDadosUsuario.EHRESPFIN = this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 0 ? 'T' : 'F';
            // Determina se ele é responsável acadêmico
            objDadosUsuario.EHRESPACAD = this.dadosResponsavelAcademico.UsaDadosTipoRelac === 0 ? 'T' : 'F';
            // Determina se ele é responsável pela inscrição (apenas se não for candidato)
            objDadosUsuario.EHRESPINSC = 'F';

            // Caso o Academico use os dados do financeiro, mas o Financeiro usa os dados do candidato
            if (this.dadosResponsavelAcademico.UsaDadosTipoRelac >= 0) {

                objDadosUsuario.EHRESPACAD = (this.dadosResponsavelAcademico.UsaDadosTipoRelac === 3 &&
                        this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 0) ?
                    'T' : objDadosUsuario.EHRESPACAD;
            }

            modelJSON.SPSUSUARIO.push(objDadosUsuario);

            /*Prepara os dados do pai para enviar*/
            if (($rootScope.objParametros.UsaDadosPai) && (!this.dadosPai.formularioEmBranco)) {

                if (this.dadosPai.UsaDadosTipoRelac === null ||
                    this.dadosPai.UsaDadosTipoRelac < 0) {

                    // Cria uma cópia das informações do Pai
                    var objDadosPai = angular.copy(this.dadosPai);

                    /*Prepara os dados do pai para enviar*/
                    trataCamposParseJson(objDadosPai);

                    // Informa que o usuário é pai
                    objDadosPai.EHPAI = 'T';
                    // Informa que o usuário não é mãe
                    objDadosPai.EHMAE = 'F';
                    // Informa que o usuário não é candidato
                    objDadosPai.EHCANDIDATO = 'F';
                    // Determina se ele é responsável financeiro
                    objDadosPai.EHRESPFIN = this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 1 ? 'T' : 'F';
                    // Determina se ele é responsável acadêmico
                    objDadosPai.EHRESPACAD = this.dadosResponsavelAcademico.UsaDadosTipoRelac === 1 ? 'T' : 'F';
                    // Determina se ele é o responsável pela inscrição
                    objDadosPai.EHRESPINSC = this.dadosPai.UsaDadosTipoRelac === 4 ? 'T' : 'F';

                    // Caso o Academico use os dados do financeiro, mas o Financeiro usa os dados do pai
                    if (this.dadosResponsavelAcademico.UsaDadosTipoRelac >= 0) {

                        objDadosPai.EHRESPACAD = (this.dadosResponsavelAcademico.UsaDadosTipoRelac === 3 &&
                                this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 1) ?
                            'T' : objDadosPai.EHRESPACAD;
                    }

                    modelJSON.SPSUSUARIO.push(objDadosPai);
                }
            }

            /*Prepara os dados da mãe para enviar*/
            if (($rootScope.objParametros.UsaDadosMae) && (!this.dadosMae.formularioEmBranco)) {

                if (this.dadosMae.UsaDadosTipoRelac === null ||
                    this.dadosMae.UsaDadosTipoRelac < 0) {

                    // Cria uma cópia das informações da Mãe
                    var objDadosMae = angular.copy(this.dadosMae);

                    /*Prepara os dados da mae para enviar*/
                    trataCamposParseJson(objDadosMae);

                    // Informa que o usuário é mãe
                    objDadosMae.EHMAE = 'T';
                    // Informa que o usuário não é pai
                    objDadosMae.EHPAI = 'F';
                    // Informa que o usuário não é candidato
                    objDadosMae.EHCANDIDATO = 'F';
                    // Determina se ele é responsável financeiro
                    objDadosMae.EHRESPFIN = this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 2 ? 'T' : 'F';
                    // Determina se ele é responsável acadêmico
                    objDadosMae.EHRESPACAD = this.dadosResponsavelAcademico.UsaDadosTipoRelac === 2 ? 'T' : 'F';
                    // Determina se ele é o responsável pela inscrição
                    objDadosMae.EHRESPINSC = this.dadosMae.UsaDadosTipoRelac === 4 ? 'T' : 'F';

                    // Caso o Academico use os dados do financeiro, mas o Financeiro usa os dados da mãe
                    if (this.dadosResponsavelAcademico.UsaDadosTipoRelac >= 0) {

                        objDadosMae.EHRESPACAD = (this.dadosResponsavelAcademico.UsaDadosTipoRelac === 3 &&
                                this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 2) ?
                            'T' : objDadosMae.EHRESPACAD;
                    }

                    modelJSON.SPSUSUARIO.push(objDadosMae);
                }
            }

            /*Prepara os dados do responsável acadêmico para enviar*/
            if (($rootScope.objParametros.UsaDadosRespAcad) && (!this.dadosResponsavelAcademico.formularioEmBranco)) {

                if (this.dadosResponsavelAcademico.UsaDadosTipoRelac === null ||
                    this.dadosResponsavelAcademico.UsaDadosTipoRelac < 0) {

                    // Cria uma cópia das informações do responsável acadêmico
                    var objDadosRespAcad = angular.copy(this.dadosResponsavelAcademico);

                    /*Prepara os dados do responsável acadêmico para enviar*/
                    trataCamposParseJson(objDadosRespAcad);

                    // Informa que o usuário é um responsável acadêmico
                    objDadosRespAcad.EHRESPACAD = 'T';

                    modelJSON.SPSUSUARIO.push(objDadosRespAcad);
                }
            }

            /*Prepara os dados do responsável financeiro para enviar*/
            if (($rootScope.objParametros.UsaDadosRespFin) && (!this.dadosResponsavelFinanceiro.formularioEmBranco)) {

                if (this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === null ||
                    this.dadosResponsavelFinanceiro.UsaDadosTipoRelac < 0) {

                    // Cria uma cópia das informações do responsável financeiro
                    var objDadosRespFin = angular.copy(this.dadosResponsavelFinanceiro);

                    /*Prepara os dados do responsável financeiro para enviar*/
                    trataCamposParseJson(objDadosRespFin);

                    // Determina ele como responsável financeiro
                    objDadosRespFin.EHRESPFIN = 'T';

                    // Determina se ele é responsável financeiro
                    // Será responsável financeiro somente se o financeiro não estiver utilizando dados de outro usuario
                    objDadosRespFin.EHRESPACAD = (this.dadosResponsavelAcademico.UsaDadosTipoRelac === 3 &&
                            this.dadosResponsavelFinanceiro.UsaDadosTipoRelac < 0) ?
                        'T' :
                        'F';

                    modelJSON.SPSUSUARIO.push(objDadosRespFin);
                }
            }
            
            /*Prepara os dados do responsável pela inscrição, caso seja ensino básico*/
            if (($rootScope.nivelEnsino === 'eb') && (!this.souCandidato) && (!this.dadosResponsavelInscricao.formularioEmBranco)) {
                // Cria uma cópia das informações do Pai
                var objDadosRespInsc = angular.copy(this.dadosResponsavelInscricao);

                //O responsável pela inscrição que terá a senha, e não o candidato
                if ($rootScope.objParametros.UsaSenhaLogin) {
                    objDadosRespInsc.SENHA = senha;
                }

                /*Prepara os dados do responsável pela inscrição para enviar*/
                trataCamposParseJson(objDadosRespInsc);

                // Informa que o usuário é responsável pela inscrição
                objDadosRespInsc.EHRESPINSC = 'T';
                // Determina se ele é pai
                objDadosRespInsc.EHPAI = this.dadosPai.UsaDadosTipoRelac === 4 ? 'T' : 'F';
                // Determina se ele é mãe
                objDadosRespInsc.EHMAE = this.dadosMae.UsaDadosTipoRelac === 4 ? 'T' : 'F';
                // Determina se ele é responsável financeiro
                objDadosRespInsc.EHRESPFIN = this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 4 ? 'T' : 'F';
                // Determina se ele é responsável acadêmico
                objDadosRespInsc.EHRESPACAD = this.dadosResponsavelAcademico.UsaDadosTipoRelac === 4 ? 'T' : 'F';

                // Verifica se o responsável financeiro é o mesmo usuário do pai ou da mãe
                if (this.dadosResponsavelFinanceiro.UsaDadosTipoRelac >= 0) {
                    // Financeiro é o pai
                    if (this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 1 && objDadosRespInsc.EHPAI === 'T') {
                        objDadosRespInsc.EHRESPFIN = 'T';
                    }

                    // Financeiro é a mãe
                    if (this.dadosResponsavelFinanceiro.UsaDadosTipoRelac === 2 && objDadosRespInsc.EHMAE === 'T') {
                        objDadosRespInsc.EHRESPFIN = 'T';
                    }
                }

                // Verifica se o responsável acadêmico é o mesmo usuário do pai ou da mãe
                if (this.dadosResponsavelAcademico.UsaDadosTipoRelac >= 0) {
                    // Acadêmico é o pai
                    if (this.dadosResponsavelAcademico.UsaDadosTipoRelac === 1 && objDadosRespInsc.EHPAI === 'T') {
                        objDadosRespInsc.EHRESPACAD = 'T';
                    }

                    // Acadêmico é a mãe
                    if (this.dadosResponsavelAcademico.UsaDadosTipoRelac === 2 && objDadosRespInsc.EHMAE === 'T') {
                        objDadosRespInsc.EHRESPACAD = 'T';
                    }
                }

                modelJSON.SPSUSUARIO.push(objDadosRespInsc);
            }

            /*Atualiza campos de local de realização da prova*/
            if (objDadosInscricaoAreaOfertada.LocalProvaSelecionado) {
                objDadosInscricaoAreaOfertada.CODMUNICIPIO = objDadosInscricaoAreaOfertada.LocalProvaSelecionado.CODMUNICIPIO;
                objDadosInscricaoAreaOfertada.CODETDMUNICIPIO = objDadosInscricaoAreaOfertada.LocalProvaSelecionado.CODETDMUNICIPIO;
                objDadosInscricaoAreaOfertada.LocalProvaSelecionado = null; //Anula objeto para não ocorrer problema na conversão para dataSet
            }
            if (objDadosInscricaoAreaOfertada.TREINEIRO === true) {
                objDadosInscricaoAreaOfertada.TREINEIRO = 'T';
            } else {
                objDadosInscricaoAreaOfertada.TREINEIRO = 'F';
            }

            /*Prepara a lista de sub-deficiências para mandar para enviar*/
            if (objDadosInscricaoAreaOfertada.DEFAUDITIVA === true) {
                objDadosInscricaoAreaOfertada.DEFAUDITIVA = 'T';
                for (var i = 0; i < this.listas.listaSelectedDefAuditiva.length; i++) {
                    var dadosSubDeficienciaAuditiva = {};
                    dadosSubDeficienciaAuditiva.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaAuditiva.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaAuditiva.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaAuditiva.CODDEFICIENCIA = this.listas.listaSelectedDefAuditiva[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaAuditiva);
                }
            } else {
                objDadosInscricaoAreaOfertada.DEFAUDITIVA = 'F';
            }

            if (objDadosInscricaoAreaOfertada.DEFVISUAL === true) {
                objDadosInscricaoAreaOfertada.DEFVISUAL = 'T';

                for (var i = 0; i < this.listas.listaSelectedDefVisual.length; i++) {
                    var dadosSubDeficienciaVisual = {};
                    dadosSubDeficienciaVisual.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaVisual.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaVisual.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaVisual.CODDEFICIENCIA = this.listas.listaSelectedDefVisual[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaVisual);
                }
            } else {
                objDadosInscricaoAreaOfertada.DEFVISUAL = 'F';
            }

            if (objDadosInscricaoAreaOfertada.DEFFISICA === true) {
                objDadosInscricaoAreaOfertada.DEFFISICA = 'T';

                for (var i = 0; i < this.listas.listaSelectedDefFisica.length; i++) {
                    var dadosSubDeficienciaFisica = {};
                    dadosSubDeficienciaFisica.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaFisica.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaFisica.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaFisica.CODDEFICIENCIA = this.listas.listaSelectedDefFisica[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaFisica);
                }
            } else {
                objDadosInscricaoAreaOfertada.DEFFISICA = 'F';
			}

            if (objDadosInscricaoAreaOfertada.DEFFALA === true) {
                objDadosInscricaoAreaOfertada.DEFFALA = 'T';

                for (var i = 0; i < this.listas.listaSelectedDefFala.length; i++) {
                    var dadosSubDeficienciaFala = {};
                    dadosSubDeficienciaFala.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaFala.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaFala.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaFala.CODDEFICIENCIA = this.listas.listaSelectedDefFala[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaFala);
                }
            } else {
                objDadosInscricaoAreaOfertada.DEFFALA = 'F';
			}

			if (objDadosInscricaoAreaOfertada.DEFMENTAL === true) {
                objDadosInscricaoAreaOfertada.DEFMENTAL = 'T';

                for (var i = 0; i < this.listas.listaSelectedDefMental.length; i++) {
                    var dadosSubDeficienciaMental = {};
                    dadosSubDeficienciaMental.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaMental.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaMental.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaMental.CODDEFICIENCIA = this.listas.listaSelectedDefMental[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaMental);
                }
            } else {
                objDadosInscricaoAreaOfertada.DEFMENTAL = 'F';
            }

            if (objDadosInscricaoAreaOfertada.DEFINTELECTUAL === true) {
                objDadosInscricaoAreaOfertada.DEFINTELECTUAL = 'T';

                for (var i = 0; i < this.listas.listaSelectedDefIntelectual.length; i++) {
                    var dadosSubDeficienciaIntelectual = {};
                    dadosSubDeficienciaIntelectual.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaIntelectual.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaIntelectual.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaIntelectual.CODDEFICIENCIA = this.listas.listaSelectedDefIntelectual[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaIntelectual);
                }
            } else {
                objDadosInscricaoAreaOfertada.DEFINTELECTUAL = 'F';
			}

			if (objDadosInscricaoAreaOfertada.BRPDH === true) {
                objDadosInscricaoAreaOfertada.BRPDH = 'T';

                for (var i = 0; i < this.listas.listaSelectedDefReabilitado.length; i++) {
                    var dadosSubDeficienciaReabilitado = {};
                    dadosSubDeficienciaReabilitado.CODCOLIGADA = $rootScope.CodColigada;
                    dadosSubDeficienciaReabilitado.IDPS = $rootScope.IdPS;
                    dadosSubDeficienciaReabilitado.NUMEROINSCRICAO = -1;
                    dadosSubDeficienciaReabilitado.CODDEFICIENCIA = this.listas.listaSelectedDefReabilitado[i].CODDEFICIENCIA;
                    modelJSON.SPSINSCRICAOAREAOFERTADASUBDEF.push(dadosSubDeficienciaReabilitado);
                }
            } else {
                objDadosInscricaoAreaOfertada.BRPDH = 'F';
            }

            if (objDadosInscricaoAreaOfertada.DEFMULTIPLA === true) {
                objDadosInscricaoAreaOfertada.DEFMULTIPLA = 'T';
            } else {
                objDadosInscricaoAreaOfertada.DEFMULTIPLA = 'F';
            }

            if (isDefinedNotNull(objDadosInscricaoAreaOfertada.DEFMOTIVOOUTRAS) && (objDadosInscricaoAreaOfertada.DEFMOTIVOOUTRAS.length > 0)) {
                objDadosInscricaoAreaOfertada.DEFOUTRAS = 'T';
            } else {
                objDadosInscricaoAreaOfertada.DEFOUTRAS = 'F';
            }

            if (isDefinedNotNull(objDadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ)) {
                objDadosInscricaoAreaOfertada.IDFORMAINSCRICAO = objDadosInscricaoAreaOfertada.FORMAINSCRICAOOBJ.IDFORMAINSCRICAO;
            }

            if (isDefinedNotNull(objDadosOpcaoInscrito.AREAINTERESSEOBJ)) {
                objDadosInscricaoAreaOfertada.IDAREAINTERESSE = objDadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE;
            }

            if (isDefinedNotNull(objDadosInscricaoAreaOfertada.CAMPUSOBJ)) {
                objDadosInscricaoAreaOfertada.IDCAMPUS = objDadosInscricaoAreaOfertada.CAMPUSOBJ.IDCAMPUS;
            }

			// dados de cotas de instituições federais
			if (isDefinedNotNull(objDadosInscricaoAreaOfertada.CotaInstituicaoFederal)) {

				var cotaFederal = objDadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhCotaFederal === 'true',
					ensinoPublico = objDadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhEnsinoPublico === 'true',
					baixaRenda = objDadosInscricaoAreaOfertada.CotaInstituicaoFederal.PossuiRendaBaixa === 'true',
                    cotaCorRaca = objDadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhCorRacaCota === 'true',
                    cotaDeficiencia = objDadosInscricaoAreaOfertada.CotaInstituicaoFederal.EhDeficienciaCota === 'true';

				objDadosInscricaoAreaOfertada.COTAFEDERAL = cotaFederal ? 'T' : 'F';
				objDadosInscricaoAreaOfertada.COTAFEDERALENSINOPUBLICO = (cotaFederal && ensinoPublico) ? 'T' : 'F';
				objDadosInscricaoAreaOfertada.COTAFEDERALRENDA = (ensinoPublico && baixaRenda) ? 'T' : 'F';
                objDadosInscricaoAreaOfertada.COTAFEDERALCORACA = (ensinoPublico && cotaCorRaca) ? 'T' : 'F';
                objDadosInscricaoAreaOfertada.COTAFEDERALPCD = (ensinoPublico && cotaDeficiencia) ? 'T' : 'F';
			}

            /*Grava a URL do portal do processo seletivo que será incluída no link para emissão de 2ª via do boleto por e-mail*/
            objDadosInscricaoAreaOfertada.URL = $location.absUrl().substring(0, $location.absUrl().indexOf('/inscricoes')) + '/';

            modelJSON.SPSINSCRICAOAREAOFERTADA.push(objDadosInscricaoAreaOfertada);

            /*Prepara as opções de inscrição para o server (verifica se tem mais de 1 opção de inscrição e prepara)*/
            objDadosOpcaoInscrito.NUMEROOPCAO = 1;

            if (objDadosInscricaoAreaOfertada.GRUPOPSOBJ) {
                objDadosOpcaoInscrito.GRUPOPS = objDadosInscricaoAreaOfertada.GRUPOPSOBJ.GRUPO;
            }

            if (objDadosOpcaoInscrito.AREAINTERESSEOBJ) {
                objDadosOpcaoInscrito.IDAREAINTERESSE = objDadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE;
            }

            modelJSON.SPSOPCAOINSCRITO.push(objDadosOpcaoInscrito);

            if (isDefinedNotNull(objDadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ) && objDadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ !== null) {

                var numOpcao = 2;
                angular.forEach(objDadosOpcaoInscrito.AREAINTERESSEOPCIONALOBJ, function(areainteresseOpcional) {
                    var dadosOpcaoInscritoTemp = {};
                    dadosOpcaoInscritoTemp.CODCOLIGADA = objDadosOpcaoInscrito.CODCOLIGADA;
                    dadosOpcaoInscritoTemp.IDPS = objDadosOpcaoInscrito.IDPS;
                    dadosOpcaoInscritoTemp.IDAREAINTERESSE = areainteresseOpcional.IDAREAINTERESSE;
                    dadosOpcaoInscritoTemp.NUMEROOPCAO = numOpcao;

                    modelJSON.SPSOPCAOINSCRITO.push(dadosOpcaoInscritoTemp);
                    numOpcao++;
                });
            }
            /*Fim - Prepara as opções de inscrição para o server (verifica se tem mais de 1 opção de inscrição e prepara)*/

            /*Prepara as atividades agendadas da inscrição para o server */
            if (!self.blnUtilizaVagaExcedente) {
                this.listas.listaAtividadesAgendadas.forEach(function(element) {
                    var dadosAtividadesAgendadasTemp = {};
                    dadosAtividadesAgendadasTemp.CODCOLIGADA = element.CodColigada;
                    dadosAtividadesAgendadasTemp.IDPS = element.Idps;
                    dadosAtividadesAgendadasTemp.IDTIPOATIVIDADEAGENDADA = element.IdTipoAtividadeAgendada;
                    dadosAtividadesAgendadasTemp.DATAAGENDAMENTO = element.HorarioSelecionado.DataAgendamento;
                    dadosAtividadesAgendadasTemp.HORAINICIO = element.HorarioSelecionado.HoraInicio;
                    dadosAtividadesAgendadasTemp.HORAFIM = element.HorarioSelecionado.HoraFim;
                    dadosAtividadesAgendadasTemp.NOMEATIVIDADE = element.NomeAtividade;
                    dadosAtividadesAgendadasTemp.INTERVALOANTERIOR = element.IntervaloAnterior;

                    modelJSON.SPSATIVIDADEAGENDADAPS.push(dadosAtividadesAgendadasTemp);
                }, this);
            }
            /*Fim - atividades agendadas */

            if (objDadosIdiomaInscrito.IDIOMAOBJ) {

                objDadosIdiomaInscrito.CODCOLIGADA = $rootScope.CodColigada;
                objDadosIdiomaInscrito.IDPS = $rootScope.IdPS;
                objDadosIdiomaInscrito.IDAREAINTERESSE = objDadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE;
                objDadosIdiomaInscrito.CODIDIOMA = objDadosIdiomaInscrito.IDIOMAOBJ.CODIDIOMA;
                modelJSON.SPSIDIOMAINSCRITO.push(objDadosIdiomaInscrito);
            }

            if (isDefinedNotNull(objDadosIdiomaInscrito.IDIOMAOPCIONALOBJ) && objDadosIdiomaInscrito.IDIOMAOPCIONALOBJ !== null) {

                var numOpcaoIdioma = 2;
                angular.forEach(objDadosIdiomaInscrito.IDIOMAOPCIONALOBJ, function(idiomaOpcional) {
                    var dadosOpcaoIdiomaTemp = {};
                    dadosOpcaoIdiomaTemp.CODCOLIGADA = objDadosOpcaoInscrito.CODCOLIGADA;
                    dadosOpcaoIdiomaTemp.IDPS = objDadosOpcaoInscrito.IDPS;
                    dadosOpcaoIdiomaTemp.IDAREAINTERESSE = objDadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE;
                    dadosOpcaoIdiomaTemp.CODIDIOMA = idiomaOpcional.CODIDIOMA;
                    dadosOpcaoIdiomaTemp.NUMEROOPCAO = numOpcaoIdioma;

                    modelJSON.SPSIDIOMAINSCRITO.push(dadosOpcaoIdiomaTemp);
                    numOpcaoIdioma++;
                });
            }

            if (this.dadosCamposComplementaresInscricao) {

                var dadosCamposComplementaresInscricaoTemp = angular.copy(this.dadosCamposComplementaresInscricao);
                dadosCamposComplementaresInscricaoTemp.CODCOLIGADA = $rootScope.CodColigada;
                dadosCamposComplementaresInscricaoTemp.IDPS = $rootScope.IdPS;


                /*seta o polo quando existir*/
                if ( angular.isDefined(this.dadosCamposComplementaresInscricao.POLO_EAD) )
                    dadosCamposComplementaresInscricaoTemp.POLO_EAD = this.dadosCamposComplementaresInscricao.POLO_EAD;

                if ($rootScope.objParametros && $rootScope.objParametros.ListaCampoComplFilialGrupo) {
                    for (var i = 0; i < $rootScope.objParametros.ListaCampoComplFilialGrupo.length; i++) {
                        var itemCampo = $rootScope.objParametros.ListaCampoComplFilialGrupo[i];

                        //campo input tipo checkbox
                        if (itemCampo.TipoDaColuna === 6) {
                            if (dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] === true) {
                                dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] = 'T';
                            } else {
                                dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] = 'F';
                            }
                        } else {
                            if (itemCampo.CodTabDinam && itemCampo.CodTabDinam !== '') {
                                var itemComplementarObj = dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna + 'OBJ'];
                                if (itemComplementarObj) {
                                    dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna] = itemComplementarObj.CodCliente;
                                    dadosCamposComplementaresInscricaoTemp[itemCampo.NomeColuna + 'OBJ'] = null;
                                }
                            }
                        }
                    }
                }

                modelJSON.SPSINSCAREAOFERTACOMPL.push(dadosCamposComplementaresInscricaoTemp);
            }

                if (isDefinedNotNull(objDadosDocumentosExigidos) && objDadosDocumentosExigidos !== null) {
                angular.forEach(objDadosDocumentosExigidos, function(element) {
                    if (isDefinedNotNull(element.ARQUIVO)){
                        for(var f = 0; f < element.ARQUIVO.length; f++){
                            if (element.NOMEARQUIVO[f].length > 0){
                                var dadosDocumentosExigidosTemp = {};
                                dadosDocumentosExigidosTemp.CODDOCUMENTO = element.CODDOCUMENTO;
                                dadosDocumentosExigidosTemp.DETALHE = element.DESCRICAO;
                                dadosDocumentosExigidosTemp.NOMEARQUIVO = element.NOMEARQUIVO[f];
                                var model = {
                                    Arquivo: element.ARQUIVO[f]
                                }
                                dadosDocumentosExigidosTemp.ARQUIVO = [model];

                                modelJSON.SPSDOCUMENTOSEXIGIDOS.push(dadosDocumentosExigidosTemp);
                            }
                        }
                    }
                });
            }

            if (isDefinedNotNull(this.dadosOfertaOnline) && this.dadosOfertaOnline !== null
                    && this.dadosPagamento !== null)
            {
                var dadosOfertaTemp = {};
                dadosOfertaTemp.CODPLANOPGTO = this.dadosPagamento;
                modelJSON.SPSOFERTAONLINE.push(dadosOfertaTemp);
            }

            if (isDefinedNotNull(this.dadosEtapa) && this.dadosEtapa !== null)
                modelJSON.SPSETAPA.push(this.dadosEtapa);

            return modelJSON;
		}

        // Método para tratamento dos campos durante o parse para o objeto Json
        function trataCamposParseJson(dadosUsuario) {

            if (dadosUsuario.CANHOTO === true) {
                dadosUsuario.CANHOTO = 'T';
            } else {
                dadosUsuario.CANHOTO = 'F';
            }
            if (dadosUsuario.FUMANTE === true) {
                dadosUsuario.FUMANTE = 1;
            } else {
                dadosUsuario.FUMANTE = 0;
            }
            if (dadosUsuario.CONJUGEBRASIL === true) {
                dadosUsuario.CONJUGEBRASIL = 1;
            } else {
                dadosUsuario.CONJUGEBRASIL = 0;
            }
            if (dadosUsuario.NATURALIZADO === true) {
                dadosUsuario.NATURALIZADO = 1;
            } else {
                dadosUsuario.NATURALIZADO = 0;
            }
            if (dadosUsuario.NIT === true) {
                dadosUsuario.NIT = 1;
            } else {
                dadosUsuario.NIT = 0;
            }
            if (dadosUsuario.FILHOSBRASIL) {
                dadosUsuario.NROFILHOSBRASIL = dadosUsuario.FILHOSBRASIL;
			}
			if (dadosUsuario.ESTADOS) {
                dadosUsuario.ESTADONATAL = dadosUsuario.ESTADOS.CODETD;
            }
            if (dadosUsuario.MUNICIPIOS) {
                dadosUsuario.CODNATURALIDADE = dadosUsuario.MUNICIPIOS.CODMUNICIPIO;
            }
            if (dadosUsuario.ESTADOCIVILOBJ) {
                dadosUsuario.ESTADOCIVIL = dadosUsuario.ESTADOCIVILOBJ.CODCLIENTE;
            }
            if (dadosUsuario.NACIONALIDADEOBJ) {
                dadosUsuario.NACIONALIDADE = dadosUsuario.NACIONALIDADEOBJ.CODCLIENTE;
            }
            if (dadosUsuario.ESTADOSCARTIDENT) {
                dadosUsuario.UFCARTIDENT = dadosUsuario.ESTADOSCARTIDENT.CODETD;
            }
            if (dadosUsuario.ESTELEITOBJ) {
                dadosUsuario.ESTELEIT = dadosUsuario.ESTELEITOBJ.CODETD;
            }
            if (dadosUsuario.UFCARTTRABOBJ) {
                dadosUsuario.UFCARTTRAB = dadosUsuario.UFCARTTRABOBJ.CODETD;
            }
            if (dadosUsuario.TIPORUAOBJ) {
                dadosUsuario.CODTIPORUA = dadosUsuario.TIPORUAOBJ.CODIGO;
            }
            if (dadosUsuario.TIPOBAIRROOBJ) {
                dadosUsuario.CODTIPOBAIRRO = dadosUsuario.TIPOBAIRROOBJ.CODIGO;
            }
            if (dadosUsuario.PAISESENDERECO) {
                dadosUsuario.IDPAIS = dadosUsuario.PAISESENDERECO.IDPAIS;
            }
            if (dadosUsuario.ESTADOSENDERECO) {
                dadosUsuario.ESTADO = dadosUsuario.ESTADOSENDERECO.CODETD;
            }
            if (dadosUsuario.MUNICIPIOSENDERECO) {
                dadosUsuario.CODMUNICIPIO = dadosUsuario.MUNICIPIOSENDERECO.CODMUNICIPIO;
            }
            if (dadosUsuario.CORRACAOBJ) {
                dadosUsuario.CORRACA = dadosUsuario.CORRACAOBJ.CODCLIENTE;
            }
            if (dadosUsuario.GRAUINSTRUCAOOBJ) {
                dadosUsuario.GRAUINSTRUCAO = dadosUsuario.GRAUINSTRUCAOOBJ.CODCLIENTE;
            }
            if (dadosUsuario.PROFISSAOOBJ) {
                dadosUsuario.CODPROFISSAO = dadosUsuario.PROFISSAOOBJ.CODCLIENTE;
            }
            if (dadosUsuario.TIPOSANGOBJ) {
                dadosUsuario.TIPOSANG = dadosUsuario.TIPOSANGOBJ.CODIGO;
            }
            if (dadosUsuario.SITMILITAROBJ) {
                dadosUsuario.SITMILITAR = dadosUsuario.SITMILITAROBJ.CODIGO;
            }
        }

        function trataCampoComplementarParaVisualizacao(itemCampo, objInscricao) {

            var result = '';

            if (itemCampo && isDefinedNotNull(itemCampo) &&
                itemCampo.NomeColuna && isDefinedNotNull(itemCampo.NomeColuna)) {
                //campo input select
                if (itemCampo.CodTabDinam) {
                    var tabDinamObj = objInscricao.dadosCamposComplementaresInscricao[itemCampo.NomeColuna + 'OBJ'];
                    if (tabDinamObj && isDefinedNotNull(tabDinamObj.Descricao)) {
                        result = tabDinamObj.Descricao;
                    }
                } else {
                    var valor = objInscricao.dadosCamposComplementaresInscricao[itemCampo.NomeColuna];
                    //campo input tipo checkbox
                    if (itemCampo.TipoDaColuna === 6) {
                        if (isDefinedNotNull(valor)) {
                            result = booleanToString(valor);
                        } else {
                            result = booleanToString(false);
                        }
                    } else {
                        if (valor && isDefinedNotNull(valor)) {
                            if (itemCampo.TipoDaColuna === 0 || //campo input tipo char
                                itemCampo.TipoDaColuna === 4) { //campo input tipo text
                                result = valor;
                            } else if (itemCampo.TipoDaColuna === 3 || //campo input tipo int
                                itemCampo.TipoDaColuna === 5) { //campo input tipo int16
                                result = $filter('number')(valor, 0);
                            }
                            //campo input tipo data
                            else if (itemCampo.TipoDaColuna === 1) {
                                result = $filter('date')(valor, 'dd/MM/yyyy');
                            }
                            //campo input tipo double | decimal
                            else if (itemCampo.TipoDaColuna === 2) {
                                result = $filter('number')(valor, 2);
                            }
                        }
                    }
                }
            }

            return result;
        }

        function camposObrigatoriosCandidatoPreenchidos(escopo, objInscricao, inscricaoAposLogin, novoDependente, nomeEtapaAtual) {
            var listaCampos = '',
                podeSalvar = true,
                senhaIgual = true;

            if (nomeEtapaAtual === 'inscricoesWizard.dados-basicos'){
                ///DADOS DO CANDIDATO
                if (!escopo.controller.frmDadosCandidato.$valid) {
                    //Obrigatoriedade de campos
                    if (escopo.controller.frmDadosCandidato.$error.required) {
                        for (var i = 0; i < 10; i++) {
                            if ((isDefinedNotNull(escopo.controller.frmDadosCandidato.$error.required[i])) && (escopo.controller.frmDadosCandidato.$error.required[i].$name !== '') && (escopo.controller.frmDadosCandidato.$error.required[i].$name.indexOf('controller_') === -1)) {
                                listaCampos = listaCampos + escopo.controller.frmDadosCandidato.$error.required[i].$name + ', ';
                                podeSalvar = false;
                            }
                        }
                    }
                    //Máscaras inválidas de telefone
                    if (escopo.controller.frmDadosCandidato.$error.mask && escopo.controller.frmDadosCandidato.$error.mask.length > 0) {
                        listaCampos = carregaDescricao(listaCampos, escopo.controller.frmDadosCandidato.$error.mask[0], 'l-candidato');
                        podeSalvar = false;
                    }
                }
            
                //RESPONSAVEL DA INSCRIÇÃO
                if (($rootScope.nivelEnsino === 'eb') && (!escopo.controller.frmDadosResponsavelInscricao.$valid)) {
                    //Obrigatoriedade de campos
                    if (escopo.controller.frmDadosResponsavelInscricao.$error.required && escopo.controller.frmDadosResponsavelInscricao.$error.required.length > 0) {
                        for (var n = 0; n < 10; n++) {
                            if ((isDefinedNotNull(escopo.controller.frmDadosResponsavelInscricao.$error.required[n])) && (escopo.controller.frmDadosResponsavelInscricao.$error.required[n].$name !== '') && (escopo.controller.frmDadosResponsavelInscricao.$error.required[n].$name.indexOf('controller_') === -1)) {
                                listaCampos = listaCampos + escopo.controller.frmDadosResponsavelInscricao.$error.required[n].$name + ', ';
                                podeSalvar = false;
                            }
                        }
                    }
                    //Máscaras inválidas de telefone
                    if (escopo.controller.frmDadosResponsavelInscricao.$error.mask && escopo.controller.frmDadosResponsavelInscricao.$error.mask.length > 0) {
                        listaCampos = carregaDescricao(listaCampos, escopo.controller.frmDadosResponsavelInscricao.$error.mask[0], 'l-resp-insc');
                        podeSalvar = false;
                    }
                }
            }
            if (nomeEtapaAtual === 'inscricoesWizard.responsaveis'){
                //PAI
                if ($rootScope.objParametros.UsaDadosPai) {
                    if (!escopo.controller.frmDadosPai.$valid) {
                        //Obrigatoriedade de campos
                        if (escopo.controller.frmDadosPai.$error.required && escopo.controller.frmDadosPai.$error.required.length > 0) {
                            for (var j = 0; j < 10; j++) {
                                if ((isDefinedNotNull(escopo.controller.frmDadosPai.$error.required[j])) && (escopo.controller.frmDadosPai.$error.required[j].$name !== '') && (escopo.controller.frmDadosPai.$error.required[j].$name.indexOf('controller_') === -1)) {
                                    listaCampos = listaCampos + escopo.controller.frmDadosPai.$error.required[j].$name + ', ';
                                    podeSalvar = false;
                                }
                            }
                        }
                        //Máscaras inválidas de telefone
                        if (escopo.controller.frmDadosPai.$error.mask && escopo.controller.frmDadosPai.$error.mask.length > 0) {
                            listaCampos = carregaDescricao(listaCampos, escopo.controller.frmDadosPai.$error.mask[0], 'l-pai');
                            podeSalvar = false;
                        }
                    }
                }
                //MÃE
                if ($rootScope.objParametros.UsaDadosMae) {
                    if (!escopo.controller.frmDadosMae.$valid){
                        //Obrigatoriedade de campos
                        if (escopo.controller.frmDadosMae.$error.required && escopo.controller.frmDadosMae.$error.required.length > 0) {
                            for (var k = 0; k < 10; k++) {
                                if ((isDefinedNotNull(escopo.controller.frmDadosMae.$error.required[k])) && (escopo.controller.frmDadosMae.$error.required[k].$name !== '') && (escopo.controller.frmDadosMae.$error.required[k].$name.indexOf('controller_') === -1)) {
                                    listaCampos = listaCampos + escopo.controller.frmDadosMae.$error.required[k].$name + ', ';
                                    podeSalvar = false;
                                }
                            }
                        }
                        //Máscaras inválidas de telefone
                        if (escopo.controller.frmDadosMae.$error.mask && escopo.controller.frmDadosMae.$error.mask.length > 0) {
                            listaCampos = carregaDescricao(listaCampos, escopo.controller.frmDadosMae.$error.mask[0], 'l-mae');
                            podeSalvar = false;
                        }
                    }
                }
                //RESPONSAVEL ACADEMICO
                if ($rootScope.objParametros.UsaDadosRespAcad) {
                    if (!escopo.controller.frmDadosResponsavelAcademico.$valid){
                        //Obrigatoriedade de campos
                        if (escopo.controller.frmDadosResponsavelAcademico.$error.required && escopo.controller.frmDadosResponsavelAcademico.$error.required.length > 0) {
                            for (var m = 0; m < 10; m++) {
                                if ((isDefinedNotNull(escopo.controller.frmDadosResponsavelAcademico.$error.required[m])) && (escopo.controller.frmDadosResponsavelAcademico.$error.required[m].$name !== '') && (escopo.controller.frmDadosResponsavelAcademico.$error.required[m].$name.indexOf('controller_') === -1)) {
                                    listaCampos = listaCampos + escopo.controller.frmDadosResponsavelAcademico.$error.required[m].$name + ', ';
                                    podeSalvar = false;
                                }
                            }
                        }
                        //Máscaras inválidas de telefone
                        if (escopo.controller.frmDadosResponsavelAcademico.$error.mask && escopo.controller.frmDadosResponsavelAcademico.$error.mask.length > 0) {
                            listaCampos = carregaDescricao(listaCampos, escopo.controller.frmDadosResponsavelAcademico.$error.mask[0], 'l-dados-responsavelacademico');
                            podeSalvar = false;
                        }
                    }
                }
                //RESPONSAVEL FINANCEIRO
                if ($rootScope.objParametros.UsaDadosRespFin) {
                    if (!escopo.controller.frmDadosResponsavelFinanceiro.$valid){
                        //Obrigatoriedade de campos
                        if (escopo.controller.frmDadosResponsavelFinanceiro.$error.required && escopo.controller.frmDadosResponsavelFinanceiro.$error.required.length > 0) {
                            for (var p = 0; p < 10; p++) {
                                if ((isDefinedNotNull(escopo.controller.frmDadosResponsavelFinanceiro.$error.required[p])) && (escopo.controller.frmDadosResponsavelFinanceiro.$error.required[p].$name !== '') && (escopo.controller.frmDadosResponsavelFinanceiro.$error.required[p].$name.indexOf('controller_') === -1)) {
                                    listaCampos = listaCampos + escopo.controller.frmDadosResponsavelFinanceiro.$error.required[p].$name + ', ';
                                    podeSalvar = false;
                                }
                            }
                        }
                        //Máscaras inválidas de telefone
                        if (escopo.controller.frmDadosResponsavelFinanceiro.$error.mask && escopo.controller.frmDadosResponsavelFinanceiro.$error.mask.length > 0) {
                            listaCampos = carregaDescricao(listaCampos, escopo.controller.frmDadosResponsavelFinanceiro.$error.mask[0], 'l-resp-fin');
                            podeSalvar = false;
                        }
                    }
                }
            }
            if (nomeEtapaAtual === 'inscricoesWizard.dados-curso'){
                //DOCUMENTOS EXIGIDOS
                if (!escopo.controller.frmDocumentos.$valid) {
                    if (isDefinedNotNull(objInscricao.listas.listaDocumentosExigidos) && objInscricao.listas.listaDocumentosExigidos.length > 0 ) {
                        let docs = getDocumentosExigidosInvalid(escopo, objInscricao);
                        if (docs.length > 0){
                                podeSalvar = false;
                                listaCampos += docs;
                        }
                    } 
                }
                //FORMA DE PAGAMENTO
                if (!escopo.controller.frmOfertaOnline.$valid) {
                    if (isDefinedNotNull(self.objInscricao.dadosOfertaOnline) && self.objInscricao.dadosOfertaOnline.length > 0 
                            && !isDefinedNotNull(self.objInscricao.dadosPagamento)  ) {
                        podeSalvar = false;
                        listaCampos += i18nFilter('l-detalhe-forma-pag', [], 'js/inscricoes') + ', ';
                    } 
                }
                //SENHA - DEPENDENTE
                if (($rootScope.objParametros.UsaSenhaLogin && !inscricaoAposLogin && !novoDependente) && (!escopo.controller.frmDadosSenha.$valid) && (escopo.controller.frmDadosSenha.$error.required.length > 0)) {
                    for (var l = 0; l < 10; l++) {
                        if ((isDefinedNotNull(escopo.controller.frmDadosSenha.$error.required[l])) && (escopo.controller.frmDadosSenha.$error.required[l].$name !== '') && (escopo.controller.frmDadosSenha.$error.required[l].$name.indexOf('controller_') === -1)) {
                            listaCampos = listaCampos + escopo.controller.frmDadosSenha.$error.required[l].$name + ', ';
                            podeSalvar = false;
                        }
                    }
                } else if (($rootScope.objParametros.UsaSenhaLogin && !inscricaoAposLogin) && (escopo.controller.frmDadosSenha.$valid) && (objInscricao.dadosUsuario.SENHA !== objInscricao.dadosUsuario.REPETESENHA)) {
                    senhaIgual = false;
                }
            }

            if (!podeSalvar && listaCampos) {
                listaCampos = listaCampos.slice(0, -2);
                listaCampos = i18nFilter('l-campos-obrigatorios', [], 'js/inscricoes') + listaCampos;
                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                    detail: listaCampos
                });
            } else {
                if (!senhaIgual) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                        detail: i18nFilter('l-senha-diferente', [], 'js/inscricoes')
                    });
                }
            }

            if ($rootScope.objParametros.ExigeSobrenome)
            {
                if (objInscricao.dadosUsuario.NOME != '' && (isDefinedNotNull(objInscricao.dadosUsuario.NOME)))
                    var usuario = objInscricao.dadosUsuario.NOME.split(' ');
                if (objInscricao.dadosPai.NOME != '' && (isDefinedNotNull(objInscricao.dadosPai.NOME)))
                    var pai = objInscricao.dadosPai.NOME.split(' ');
                if (objInscricao.dadosMae.NOME != '' && (isDefinedNotNull(objInscricao.dadosMae.NOME)))
                    var mae = objInscricao.dadosMae.NOME.split(' ');
                if (objInscricao.dadosResponsavelFinanceiro.UsaDadosTipoRelac === null || objInscricao.dadosResponsavelFinanceiro.UsaDadosTipoRelac === -1)
                    if (isDefinedNotNull(objInscricao.dadosResponsavelFinanceiro.NOME))
                        var respAcademico = objInscricao.dadosResponsavelFinanceiro.NOME.split(' ');
                if (objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac === null || objInscricao.dadosResponsavelAcademico.UsaDadosTipoRelac === -1)
                    if (isDefinedNotNull(objInscricao.dadosResponsavelAcademico.NOME))
                        var respFinanceiro = objInscricao.dadosResponsavelAcademico.NOME.split(' ');
                if($rootScope.nivelEnsino === "eb"){
                    if (objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac === null || objInscricao.dadosResponsavelInscricao.UsaDadosTipoRelac === -1)
                        if (isDefinedNotNull(objInscricao.dadosResponsavelInscricao.NOME))
                            var respInscricao = objInscricao.dadosResponsavelInscricao.NOME.split(' ');
                }

                if (isDefinedNotNull(usuario)){
                    if (usuario.length < 2){
                        podeSalvar = false;
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: i18nFilter('l-exige-sobrenome-usuario', [], 'js/inscricoes')
                        });
                    }
                }
                if (isDefinedNotNull(pai)){
                    if (pai.length < 2){
                        podeSalvar = false;
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: i18nFilter('l-exige-sobrenome-pai', [], 'js/inscricoes')
                        });
                    }
                }
                if(isDefinedNotNull(mae)){
                    if (mae.length < 2){
                        podeSalvar = false;
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: i18nFilter('l-exige-sobrenome-mae', [], 'js/inscricoes')
                        });
                    }
                }
                if(isDefinedNotNull(respAcademico)){
                    if (respAcademico.length < 2){
                        podeSalvar = false;
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: i18nFilter('l-exige-sobrenome-responsavel-fincanceiro', [], 'js/inscricoes')
                        });
                    }
                }
                if(isDefinedNotNull(respFinanceiro)){
                    if (respFinanceiro.length < 2){
                        podeSalvar = false;
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: i18nFilter('l-exige-sobrenome-responsavel-academico', [], 'js/inscricoes')
                        });
                    }
                }
                if($rootScope.nivelEnsino === "eb"){
                    if(isDefinedNotNull(respInscricao)){
                        if (respInscricao.length < 2){
                            podeSalvar = false;
                            totvsNotification.notify({
                                type: 'warning',
                                title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                                detail: i18nFilter('l-exige-sobrenome-responsavel-inscricao', [], 'js/inscricoes')
                            });
                        }
                    }
                }
            }

            return podeSalvar && senhaIgual;
        }

        function camposObrigatoriosAreaInteressePreenchidos(escopo) {
            var listaCampos = '',
				podeSalvar = true;
            if (!escopo.controller.frmDadosAreaInteresse.$valid) {
                if (escopo.controller.frmDadosAreaInteresse.$error.required.length > 0) {
                    for (var i = 0; i < 14; i++) {
                        if ((isDefinedNotNull(escopo.controller.frmDadosAreaInteresse.$error.required[i])) &&
                            (escopo.controller.frmDadosAreaInteresse.$error.required[i].$name !== '') &&
                            (escopo.controller.frmDadosAreaInteresse.$error.required[i].$name.indexOf('controller_') === -1)) {
                            listaCampos = listaCampos + escopo.controller.frmDadosAreaInteresse.$error.required[i].$name + ', ';
                        }
                    }
                    if (listaCampos !== '') {
                        listaCampos = listaCampos.slice(0, -2);
                        listaCampos = i18nFilter('l-campos-obrigatorios', [], 'js/inscricoes') + listaCampos;
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: listaCampos
                        });
                        podeSalvar = false;
                    }
                }
            }
            return podeSalvar;
        }

        function camposObrigatoriosAtividadesAgendadas(escopo, blnUtilizaVagaExcedente) {
            var listaCampos = '',
                podeSalvar = true;

            if (!blnUtilizaVagaExcedente && !escopo.controller.frmAtividadesAgendadas.$valid) {
                if (escopo.controller.frmAtividadesAgendadas.$error.required.length > 0) {
                    for (var i = 0; i < 10; i++) {
                        if ((isDefinedNotNull(escopo.controller.frmAtividadesAgendadas.$error.required[i])) &&
                            (escopo.controller.frmAtividadesAgendadas.$error.required[i].$name !== '') &&
                            (escopo.controller.frmAtividadesAgendadas.$error.required[i].$name.indexOf('controller_') === -1)) {
                            listaCampos = listaCampos + escopo.controller.frmAtividadesAgendadas.$error.required[i].$name + ', ';
                        }
                    }
                    if (listaCampos !== '') {
                        listaCampos = i18nFilter('l-campos-obrigatorios-atividades', [], 'js/inscricoes');
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                            detail: listaCampos
                        });
                        podeSalvar = false;
                    }
                }
            }
            return podeSalvar;
        }
        
        function carregaDescricao(listaCampos, campo, sufixo) {
            if (campo.$name.indexOf('_telefone1') > 0) {
                listaCampos = listaCampos + concatenaName($rootScope.objParametros.TextoTelefone1, sufixo) + ', ';
            }
            if (campo.$name.indexOf('_telefone2') > 0) {
                listaCampos = listaCampos + concatenaName($rootScope.objParametros.TextoTelefone2, sufixo) + ', ';
            }
            if (campo.$name.indexOf('_telefone3') > 0) {
                listaCampos = listaCampos + concatenaName($rootScope.objParametros.TextoTelefone3, sufixo) + ', ';
            }
            if (campo.$name.indexOf('_fax') > 0) {
                listaCampos = listaCampos + i18nFilter('l-fax', [], 'js/inscricoes') + ', ';
            }
            return listaCampos;
        }

        // Verifica os formulários dos responsáveis para saber se os campos estão em branco.
        // Serão enviados somente usuários que tiveram no mínimo um campo preenchido
        function verificaFormulariosEmBranco(escopo, objInscricao) {

            if (($rootScope.objParametros.UsaDadosPai) && (escopo.controller.frmDadosPai) && (escopo.controller.frmDadosPai.$valid) && (!escopo.controller.frmDadosPai.$pristine)) {

                objInscricao.dadosPai.formularioEmBranco = formularioEmBranco(escopo.controller.frmDadosPai);
            }

            if (($rootScope.objParametros.UsaDadosMae) && (escopo.controller.frmDadosMae) && (escopo.controller.frmDadosMae.$valid) && (!escopo.controller.frmDadosMae.$pristine)) {

                objInscricao.dadosMae.formularioEmBranco = formularioEmBranco(escopo.controller.frmDadosMae);
            }

            if (($rootScope.objParametros.UsaDadosRespAcad) && (escopo.controller.frmDadosResponsavelAcademico) && (escopo.controller.frmDadosResponsavelAcademico.$valid) && (!escopo.controller.frmDadosResponsavelAcademico.$pristine)) {

                objInscricao.dadosResponsavelAcademico.formularioEmBranco = formularioEmBranco(escopo.controller.frmDadosResponsavelAcademico);
            }

            if (($rootScope.objParametros.UsaDadosRespFin) && (escopo.controller.frmDadosResponsavelFinanceiro) && (escopo.controller.frmDadosResponsavelFinanceiro.$valid) && (!escopo.controller.frmDadosResponsavelFinanceiro.$pristine)) {

                objInscricao.dadosResponsavelFinanceiro.formularioEmBranco = formularioEmBranco(escopo.controller.frmDadosResponsavelFinanceiro);
            }

            if (($rootScope.nivelEnsino === 'eb') && (escopo.controller.frmDadosResponsavelInscricao) && (escopo.controller.frmDadosResponsavelInscricao.$valid)) {

                objInscricao.dadosResponsavelInscricao.formularioEmBranco = formularioEmBranco(escopo.controller.frmDadosResponsavelInscricao);
            }
        }

        // Percorre o formulário procurando se houve algum campo preenchido
        function formularioEmBranco(form) {

            var formEmBranco = true;

            if (isDefinedNotNull(form)) {
                angular.forEach(form, function(value) {
                    if (angular.isObject(value) && value.hasOwnProperty('$modelValue')) {
                        if (value.$valid && value.$modelValue && value.$modelValue !== '') {
                            formEmBranco = false;
                            return false;
                        }
                    }
                });
            }

            return formEmBranco;
        }

        function buscarEnderecoPorCEP(numeroCEP, modeloDados, objInscricao) {

            if (!isDefinedNotNull(numeroCEP) || numeroCEP === '') {
                return;
            }

            EdupsUtilsFactory.getEnderecoByCEP(numeroCEP, function(result) {

                if (isDefinedNotNull(result) && result.length > 0) {

                    modeloDados.TIPORUAOBJ = objInscricao.listas.listaTipoRua.find(function(item) {
                        if (isDefinedNotNull(result[0].TIPOLOGRADOURO) && item.DESCRICAO === result[0].TIPOLOGRADOURO.toUpperCase()) {
                            return item;
                        }
                    });
                    modeloDados.RUA = result[0].NOMELOGRADOURO;
                    modeloDados.BAIRRO = result[0].BAIRRO;
                    modeloDados.PAISESENDERECO = { 'IDPAIS': 1 };
                    modeloDados.ESTADOSENDERECO = { 'CODETD': result[0].UF };
                    modeloDados.MUNICIPIOSENDERECO = { 'CODMUNICIPIO': result[0].CODMUNICIPIO };

                } else {
                    totvsNotification.notify({
                        type: 'info',
                        title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                        detail: i18nFilter('l-msg-cep-nao-encontrado', [], 'js/inscricoes')
                    });
                }
            });
        }

        function copiarEnderecoCandidato(modeloDadosDestino, objInscricao) {

            modeloDadosDestino.CODTIPORUA = objInscricao.dadosUsuario.CODTIPORUA;
            modeloDadosDestino.RUA = objInscricao.dadosUsuario.RUA;
            modeloDadosDestino.COMPLEMENTO = objInscricao.dadosUsuario.COMPLEMENTO;
            modeloDadosDestino.CODTIPOBAIRRO = objInscricao.dadosUsuario.CODTIPOBAIRRO;
            modeloDadosDestino.NOMETIPOBAIRRO = objInscricao.dadosUsuario.NOMETIPOBAIRRO;
            modeloDadosDestino.IDPAIS = objInscricao.dadosUsuario.IDPAIS;
            modeloDadosDestino.ESTADO = objInscricao.dadosUsuario.ESTADO;
            modeloDadosDestino.CODMUNICIPIO = objInscricao.dadosUsuario.CODMUNICIPIO;
            modeloDadosDestino.NUMERO = objInscricao.dadosUsuario.NUMERO;
            modeloDadosDestino.CEP = objInscricao.dadosUsuario.CEP;

            modeloDadosDestino.TELEFONE1 = objInscricao.dadosUsuario.TELEFONE1;
            modeloDadosDestino.TELEFONE2 = objInscricao.dadosUsuario.TELEFONE2;
            modeloDadosDestino.TELEFONE3 = objInscricao.dadosUsuario.TELEFONE3;
            modeloDadosDestino.FAX = objInscricao.dadosUsuario.FAX;

            modeloDadosDestino.TIPORUAOBJ = objInscricao.dadosUsuario.TIPORUAOBJ;
            modeloDadosDestino.TIPOBAIRROOBJ = objInscricao.dadosUsuario.TIPOBAIRROOBJ;
            modeloDadosDestino.BAIRRO = objInscricao.dadosUsuario.BAIRRO;

            modeloDadosDestino.PAISESENDERECO = angular.copy(objInscricao.dadosUsuario.PAISESENDERECO);
            modeloDadosDestino.ESTADOSENDERECO = angular.copy(objInscricao.dadosUsuario.ESTADOSENDERECO);
            modeloDadosDestino.MUNICIPIOSENDERECO = angular.copy(objInscricao.dadosUsuario.MUNICIPIOSENDERECO);
            modeloDadosDestino.CIDADE = angular.copy(objInscricao.dadosUsuario.CIDADE);
        }

        function tipoResponsavelAcademico(dadosResponsavel) {
            if (dadosResponsavel.UsaDadosTipoRelac === 0) {
                return i18nFilter('l-respacademico-candidato', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 1) {
                return i18nFilter('l-respacademico-pai', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 2) {
                return i18nFilter('l-respacademico-mae', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 3) {
                return i18nFilter('l-respacademico-financeiro', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 4) {
                return i18nFilter('l-respacademico-inscricao', [], 'js/inscricoes');
            } else {
                return '';
            }
        }

        function tipoResponsavelFinanceiro(dadosResponsavel) {
            if (dadosResponsavel.UsaDadosTipoRelac === 0) {
                return i18nFilter('l-respfinanceiro-candidato', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 1) {
                return i18nFilter('l-respfinanceiro-pai', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 2) {
                return i18nFilter('l-respfinanceiro-mae', [], 'js/inscricoes');
            } else if (dadosResponsavel.UsaDadosTipoRelac === 4) {
                return i18nFilter('l-respfinanceiro-inscricao', [], 'js/inscricoes');
            } else {
                return '';
            }
        }

        function tipoResponsavelInscricao(dadosResponsavel, tipo) {
            if (tipo === 1 && dadosResponsavel.UsaDadosTipoRelac === 4) {
                return i18nFilter('l-respinscricao-pai', [], 'js/inscricoes');
            } else if (tipo === 2 && dadosResponsavel.UsaDadosTipoRelac === 4) {
                return i18nFilter('l-respinscricao-mae', [], 'js/inscricoes');
            } else {
                return '';
            }
        }

        function validaObrigatoriedadeIdiomas(indiceCampo, objInscricao) {
            var campoObrigatorio = false;

            if (isDefinedNotNull(objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ)) {
                var numMinIdiomasInscricao = objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.NUMMINIDIOMASINSCRICAO;

                if (numMinIdiomasInscricao > 0) {
                    if (numMinIdiomasInscricao >= indiceCampo + 2) {
                        campoObrigatorio = true;
                    }
                }
            }

            return campoObrigatorio;
        }

        function validaVisibilidadeIdiomas(objInscricao) {
            var campoVisivel = false;

            if (isDefinedNotNull(objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ)) {
                var numMaxIdiomasInscricao = objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.NUMMAXIDIOMASINSCRICAO;

                if (numMaxIdiomasInscricao > 0) {
                    campoVisivel = true;
                }
            }

            return campoVisivel;
        }

        function stringToBoolean(valor) {
            return (valor === 'T') || (valor === 1) || (valor === 'True') || (valor === 'Sim') || (valor === '1');
        }

        function booleanToString(valor) {
            if ((valor === 'F') || (valor === 0) || (valor === false)) {
                return $filter('i18n')('l-nao', [], 'js/inscricoes');
            } else if ((valor === 'T') || (valor === 1) || (valor === true)) {
                return $filter('i18n')('l-sim', [], 'js/inscricoes');
            }
        }

        function sexoPorExtenso(valor) {
            if (valor === 'F') {
                return $filter('i18n')('l-sexo-feminino', [], 'js/inscricoes');
            } else if (valor === 'M') {
                return $filter('i18n')('l-sexo-masculino', [], 'js/inscricoes');
            } else {
                return '';
            }
        }

        function getModelCampo(i, grupoCampo) {
            var result = listaCamposComplementaresPorGrupo(grupoCampo);
            return result[i];
        }

        function efetuarLogin(tipoServico) {
            var params = { action: tipoServico };

            $state.go('centralcandidato.start', params);
        }

        function BuscaTermoAceitePS(codColigada, idPS, idAreaInteresse, callback) {
            var termoAceite;
            EdupsInscricoesFactory.buscarTermoAceitePS(codColigada, idPS, idAreaInteresse, function(result) {
                if (angular.isDefined(result) && angular.isArray(result) && result[0].TERMOACEITE !== null) {
                    termoAceite = result[0].TERMOACEITE;
                } else {
                    termoAceite = i18nFilter('l-texto-termo-aceite-padrao', [], 'js/inscricoes');
                }

                if (typeof callback === 'function') {
                    callback(termoAceite);
                }
            });
        }

        /**
            * Verifica se o objeto isDefined e diferente de nulo
            * @param {any} objeto objeto a ser validado
            * @returns
            */
        function isDefinedNotNull(objeto) {
            return (angular.isDefined(objeto) && objeto !== null);
		}

		function getTextoSimNao(valor) {
            if (valor === 'true') {
                return i18nFilter('l-sim', [], 'js/inscricoes');
            } else {
                return i18nFilter('l-nao', [], 'js/inscricoes');
            }
        }

        //Recebe o texto do telefone definido nos parâmetros do RHU e concatena com qual formulário
        //esse campo pertence.
        function concatenaName(textoTelefone, nomeResource){
            var nameCampo = textoTelefone;
            nameCampo += " ";
            nameCampo += i18nFilter(nomeResource, [], 'js/inscricoes');

            return nameCampo;
        }

        // Chama a rotina de login manual, quando for identificado que o usuário já existe no sistema.
        function chamarLogin(objInscricao) {
            $('#modalUsuarioExistente').modal('toggle');
            $timeout(function () {
                var params = {
                    action: 'inscricoesWizard',
                    nivelEnsino: $rootScope.nivelEnsino,
                    objInscricao: objInscricao
                };

                $state.go('login.manual', params);
            }, 200);
        }

        /**
            * Busca a lista de documentos da área ofertada
            * @param {any} callback - função de retorno
            * @returns
            */
		function getListaDocumentosExigidosAndOfertaOnline(codColigada, idPS, objInscricao, callback) {
            var documentos;

            EdupsInscricoesFactory.getInfoAreaInteresseDocumentAndOferta(codColigada, idPS, objInscricao.dadosOpcaoInscrito.AREAINTERESSEOBJ.IDAREAINTERESSE, function(result) {
                //Documentos exigidos
                if (angular.isDefined(result) && angular.isDefined(result.SPSDOCUMENTOEXIGIDO)){
                    let codFilial = -1;
                    let codTipoCurso = -1;

                    if (angular.isDefined(result.SPSPARAMETROSAREAOFERTADA) && angular.isArray(result.SPSPARAMETROSAREAOFERTADA) && result.SPSPARAMETROSAREAOFERTADA.length > 0){
                        codFilial = result.SPSPARAMETROSAREAOFERTADA[0].CODFILIAL;
                        codTipoCurso = result.SPSPARAMETROSAREAOFERTADA[0].CODTIPOCURSO;
                    }

                    EdupsInscricoesFactory.getEduParam(codColigada, codFilial, codTipoCurso, function(resultParam) {
                    
                        if (angular.isArray(result.SPSDOCUMENTOEXIGIDO) && result.SPSDOCUMENTOEXIGIDO.length > 0) {
                            documentos = result.SPSDOCUMENTOEXIGIDO;
                            
                            if (objInscricao.listas.listaDocumentosExigidos == null || objInscricao.listas.listaDocumentosExigidos.length == 0) {
                                objInscricao.listas.listaDocumentosExigidos = [];
                            }
                            
                            if (!angular.isArray(objInscricao.dadosDocumentosExigidos)) {
                                objInscricao.dadosDocumentosExigidos = [];
                                
                                for(let x = 0; x < documentos.length; x++){
                                    objInscricao.listas.listaDocumentosExigidos.push(documentos[x]);
                                    objInscricao.dadosDocumentosExigidos.push({
                                        CODDOCUMENTO: documentos[x].CODDOCUMENTO,
                                        DESCRICAO: documentos[x].DESCRICAO,
                                        OBRIGATORIO: documentos[x].OBRIGATORIO,
                                        NOMEARQUIVO: '',
                                        ARQUIVO: {}
                                    });
                                    }
                            }
                        }
                        else{
                            objInscricao.listas.listaDocumentosExigidos = [];
                            objInscricao.dadosDocumentosExigidos = [];
                        } 

                        //dadosOfertaOnline
                        if (angular.isArray(result.SPSPARAMETROSAREAOFERTADA) && result.SPSPARAMETROSAREAOFERTADA.length > 0){
                            //Parâmetros Educacional
                            if (angular.isDefined(resultParam)){
                                objInscricao.eduParam = resultParam;
                            }
                            else{
                                objInscricao.eduParam = null;
                            }

                            objInscricao.dadosOfertaOnline = [];

                            let params = result.SPSPARAMETROSAREAOFERTADA;
                            let arrPlanos = [];
                            let arrParcelas = [];
                            let canChange = false;

                            if (angular.isDefined(result.SPLANOPGTO)){ 
                                if (result.SPLANOPGTO.length > 0){
                                    canChange = true;

                                    for(let x = 0; x < result.SPLANOPGTO.length; x++){
                                        arrPlanos.push({
                                            Codigo: result.SPLANOPGTO[x].CODPLANOPGTO,
                                            Nome: result.SPLANOPGTO[x].NOME
                                        });
                                    }
                                }

                                if (angular.isDefined(result.SPARCPLANO) && result.SPARCPLANO.length > 0){ 
                                    for(let x = 0; x < result.SPARCPLANO.length; x++){
                                        let valor = result.SPARCPLANO[x].VALOR;
                                        arrParcelas.push({
                                            CodigoPlano: result.SPARCPLANO[x].CODPLANOPGTO,
                                            Parcela: result.SPARCPLANO[x].PARCELA,
                                            Vencimento: $filter('date')(result.SPARCPLANO[x].DTVENCIMENTO, 'dd/MM/yyyy'),
                                            Valor: $filter('number')(valor, 2)
                                        });
                                    }
                                }
                            }

                            objInscricao.dadosOfertaOnline.push({
                                OfertaOnline: true,
                                PlanoDefault: params[0].CODPLANOPGTO,
                                PodeAlterarPlano: canChange,
                                Planos: arrPlanos,
                                Parcelas: arrParcelas
                            });
                            objInscricao.dadosPagamento = params[0].CODPLANOPGTO;
                        }
                        else{
                            objInscricao.eduParam = null;
                            objInscricao.dadosOfertaOnline = [];
                            objInscricao.dadosOfertaOnline.push({
                                OfertaOnline: false,
                                Planos: null,
                                Parcelas: null
                            });
                        }

                        if (typeof callback === 'function') {
                            callback(documentos);
                        }
                    });
                }
            });
        }
        
        function getDocumentosExigidosInvalid(escopo, objInscricao) {
            let list = objInscricao.listas.listaDocumentosExigidos;
            let frm = $('#' + escopo.controller.frmDocumentos.$name);
            let campos = '';
            for (var l = 0; l < list.length; l++){
                if (list[l].OBRIGATORIO === 'T'){

                    let t = frm.find('input[name=documento_' + list[l].CODDOCUMENTO + ']');
                    if (t.length > 0 && t[0].files.length === 0 ){
                        campos += list[l].DESCRICAO + ', ';
                    }
                }
            }

            return campos;
        }

        /**
            * Monta mensagem de parcelas do plano
            * @param {string} codPlano - código do plano
            * @param {object} parcelas - parcelas do plano
            */
        function showPaymentDetail(codPlano, parcelas) {
            var parcela = parcelas.filter(function (x) { return x.CodigoPlano.trim() === codPlano.trim(); });
            var detalhes = "<table class=\"table\">\n    <thead><tr>\n      <th>Parcela</th>\n      <th>Vencimento</th>\n      <th>Valor</th>\n      </tr></thead><tbody>";
            for (var x = 0; x < parcela.length; x++) {
                detalhes += "<tr>\n          <th>" + parcela[x].Parcela + "</th>\n          <th>" + parcela[x].Vencimento + "</th>\n          <th>" + parcela[x].Valor + "</th>\n        </tr>";
            }
            detalhes += "</tbody></table>";
            totvsNotification.message({
                title: i18nFilter('l-detalhe-forma-pag', [], 'js/inscricoes'),
                text: detalhes,
                size: 'sm'
            });
        }
        
        // FUNÇÕES DA INSCRIÇÃO WIZARD

        function inicializarWizardPS(scope, eventosStateEtapas, etapas, callback) {

            //Habilita evento de escuta, que verificará se a mudança de um passo do wizard é válido
            $rootScope.$on('$stateChangeStart',
                function (event, toState) {
                    if (!verificarRedirecionamentoValido(etapas, toState.name)) {
                        event.preventDefault();
                    }
                });

            inicializarWizard(etapas, scope, eventosStateEtapas, callback);
        }
        
        function inicializarWizard(etapas, scope, eventosStateEtapas, callback) {

            if ($state.current.name !== 'incricoes.dados-basicos') {

                verificarStatusEtapaAtualAtivo(etapas);

                registrarEscutaEventoVerificacaoEtapa(etapas, eventosStateEtapas, scope);

                if (angular.isFunction(callback)) {
                    callback(etapas);
                }
            }
        }

        function registrarEscutaEventoVerificacaoEtapa(etapas, eventosStateEtapas, scope) {

            scope.$on('$stateChangeStart',
                function (event, toState) {

                    event.defaultPrevented = false;

                    if (!verificarRedirecionamentoValido(etapas, toState.name)) {
                        event.preventDefault();
                    }
                }
            );

            scope.$on('$stateChangeSuccess',
                function (event, toState) {

                    if (eventosStateEtapas.hasOwnProperty(toState.name)) {

                        var funcaoEventoNome = eventosStateEtapas[toState.name];

                        if (angular.isFunction(scope.controller[funcaoEventoNome])) {
                            scope.controller[funcaoEventoNome]();
                        }
                    }
                }
            );
        }      

        function notificaInteracao(etapas, scope)
        {           
            if ($rootScope.objParametros.UsaIntegracaoRubeus)
            {            
                scope.controller.objInscricao.dadosEtapa = retornarEtapaAtual(etapas);    
                var model = scope.controller.objInscricao.parseToJSON();

                EdupsInscricoesFactory.notificaInteracao(model, null);	
            }	
        }

        function notificaInteracaoEvento(codigoEtapa)
        {           
            if ($rootScope.objParametros.UsaIntegracaoRubeus)
            {    
                EdupsInscricoesFactory.notificaInteracaoEvento(codigoEtapa, null);	
            }	
        }
        
        function retornarEtapaAtual(etapas) {
            return etapas.find(function (item) {
                if (item.nome === $state.current.name)
                    return item;
            });
        }
		
		function retornaEtapaPorOrdem(etapas, ordem) {
            return etapas.find(function (item) {

                if (item.ordem === ordem) {
                    return item;
                }
            });
        }

        function retornaEtapaPorNome(etapas, nome) {
            return etapas.find(function (item) {

                if (item.nome === nome) {
                    return item;
                }
            });
		}
		
		function retornaEtapaPadrao(etapas) {
            return etapas.find(function (item) {

                if (item.padrao) {
                    return item;
                }
            });
        }

        function retornaTextoi18n(key) {
            return i18nFilter(key, [], 'js/inscricoes');
        }

        function avancarEtapa(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);

            if (etapaAtual.ordem < etapas.length) {

                var proximaEtapa = retornaEtapaPorOrdem(etapas, etapaAtual.ordem + 1);

                if (proximaEtapa.ativo) {
                    etapaAtual.realizado = true;
                    $state.go(proximaEtapa.nome);    
                }
            }
        }

        function retrocederEtapa(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);

            if (etapaAtual.ordem > 1) {
                var proximaEtapa = retornaEtapaPorOrdem(etapas, etapaAtual.ordem - 1);

                if (proximaEtapa.ativo) {
                    $state.go(proximaEtapa.nome);
                }
            }
		}
		
        function liberarProximaEtapa(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);
            if (etapaAtual) {
                if (etapas.length > etapaAtual.ordem) {
                    var proximaEtapa = etapas[etapaAtual.ordem];
                    if (!proximaEtapa.ativo) {
                        proximaEtapa.ativo = true;
                        etapaAtual.realizado = true;
                    }
                }
            }
        }
		
        function liberarEtapaAtual(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);
            if (etapaAtual) {
                etapaAtual.ativo = true;
                etapaAtual.realizado = true;
            }
        }
		
        function restringirProximasEtapas(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);
            angular.forEach(etapas, function (etapa) {
                if (etapa.ordem > etapaAtual.ordem) {
                    etapa.ativo = false;
                    etapa.realizado = false;
                }
            }, self);
        }

        function verificarRedirecionamentoValido(etapas, stateRedirecionamento) {
            if (stateRedirecionamento !== 'inscricoesWizard.dados-basicos')
            {
                var stateEtapaRedirecionamento = retornaEtapaPorNome(etapas, stateRedirecionamento);
                var etapaPadrao = retornaEtapaPadrao(etapas);

                var nomeStatePadraoPaginaMatricula = etapaPadrao.nome.split('.')[0];

                if ((stateEtapaRedirecionamento && stateEtapaRedirecionamento.ativo) || (!stateEtapaRedirecionamento && stateRedirecionamento !== nomeStatePadraoPaginaMatricula)) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                return true;
            }
		}

        function redirecionar(etapas, stateRedirecionamento) {

            if (verificarRedirecionamentoValido(etapas, stateRedirecionamento)) {
                $state.go(stateRedirecionamento);
            } else {

                totvsNotification.notify({
                    type: 'error',
                    title: retornaTextoi18n('l-atencao'),
                    detail: retornaTextoi18n('l-erro-avancar-etapa')
                });
            }
        }
        		
		function verificarStatusEtapaAtualAtivo(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);
            var etapaPadrao = retornaEtapaPadrao(etapas);
            if (!etapaAtual || !etapaAtual.ativo) {
                $state.go(etapaPadrao.nome);
            }
        }
    }
});
