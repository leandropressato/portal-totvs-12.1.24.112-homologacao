/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */
 (function () {

    'use strict';

    var DependenciasGlobais = [];

    //Módulos: AngularAMD Totvs Html Framework (THF)
    DependenciasGlobais.push('angularAMD', 'totvs-html-framework');

    //Módulo: TOTVS Desktop
    DependenciasGlobais.push
        (
        'global-app/global-app.module',
        'global-app/global-app.controller'
        );

    //Controllers, Diretivas, Módulos, Providers, Factorys e Services Globais da aplicação
    DependenciasGlobais.push
        (
            'utils/edups-utils.module',

            'totvs-custom/custom.module',

            'informacoes/informacoes.module',
            'informacoes/informacoes.route',

            'centralcandidato/centralcandidato.module',
            'centralcandidato/centralcandidato.route',

            'inscricoes/inscricoes.module',
            'inscricoes/inscricoes.comprovante.module',
            'inscricoes/inscricoes.route',

            'resultados/resultados.module',
            'resultados/resultados.route',

            'login/login.module',
            'login/login.route',

            'financeiro/financeiro.module',
            'financeiro/financeiro.route',

            'dados-pessoais/dados-pessoais.module',
            'dados-pessoais/dados-pessoais.route',

            'diretivas/diretivas.module',
            'diretivas/edups-pais-uf-cidade.directive',
            'diretivas/edups-file-model.directive',
            'diretivas/edups-upload-file.directive',

            'utils/reports/edups-relatorio.module',

            'utils/interceptors/edups-interceptors.module'
        );

    define(DependenciasGlobais, function (angularAMD) {

        var app =
            angular.module('totvsApp', [
                'ngSanitize',
                'ui.router',
                'ui.mask',
                'ui.select',
                'oc.lazyLoad',
                'totvsHtmlFramework',
                'edupsInterceptorsModule',
                'globalApp',
                'ngMask',
                'edupsUtilsModule',
                'edupsCustomModule',
                'edupsDiretivasModule',
                'edupsInformacoesModule',
                'edupsCentralCandidatoModule',
                'edupsInscricoesModule',
                'edupsResultadosModule',
                'edupsLoginModule',
                'edupsFinanceiroModule',
                'edupsRelatorioModule',
                'edupsDadosPessoaisModule',
                'elif'
            ]);

        // Busca os arquivos de configuração e rota do módulo principal 'totvsApp'
        // antes antes de realizar o bootstrapping(inicialização) da aplicação
        requirejs(['totvs-app.config', 'totvs-app.route', 'totvs-app.polyfills.config'], function () {
            if (app != null) {

                // Recupera o nível de ensino a partir da url.
                // O primeiro parâmetro, logo após o #, tem que ser o nível de ensino.
                document.eduPSNivelEnsino = document.location.hash.split('/')[1];

                //Módulos do angularJS que precisam ser instanciados antes do bootstrapping da aplicação

                //Inicializa a aplicação
                return angularAMD.bootstrap(app);
            }
        });
    });

} ());
