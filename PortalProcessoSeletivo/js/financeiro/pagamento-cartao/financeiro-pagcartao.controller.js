/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
 * @module edupsFinanceiroModule
 * @name EdupsPagCartaoController
 * @object controller
 *
 * @created 2017-01-11 v12.1.15
 * @updated
 *
 * @requires Financeiro.module
 *
 * @dependencies EdupsFinanceiroFactory
 *
 * @description Controller do pagamento de boleto com cartão
 */
define([
    'financeiro/financeiro.module',
    'financeiro/financeiro.route',
    'financeiro/financeiro.factory',
], function () {

    'use strict';

    angular.module('edupsFinanceiroModule')
        .controller('EdupsPagCartaoController', EdupsPagCartaoController);

    EdupsPagCartaoController.$inject = ['$scope',
        '$filter',
        '$rootScope',
        '$state',
        '$location',
        '$cookies',
        'EdupsFinanceiroFactory'
    ];

    function EdupsPagCartaoController($scope,
        $filter,
        $rootScope,
        $state,
        $location,
        $cookies,
        EdupsFinanceiroFactory) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;

        self.pagamento = null;
        self.pagCredito = true;
        self.bandeira = '1';
        self.srcVisa = EDUPS_CONST_GLOBAL_URL_BASE_APP + '/assets/img/visa.png';
        self.srcMaster = EDUPS_CONST_GLOBAL_URL_BASE_APP + '/assets/img/mastercard.png';
        self.srcVisaElectron = EDUPS_CONST_GLOBAL_URL_BASE_APP + '/assets/img/visaElectron.png';
        self.parcelaSelecionada = '01';
        self.showParcelas = true;
        self.opcoesParcelamento = [];

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.retornarOpcoesParcelamento = retornarOpcoesParcelamento;
        self.pagarCartao = pagarCartao;
        self.formataOpcaoParcela = formataOpcaoParcela;
        self.retornaValorTotal = retornaValorTotal;

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        init();

        function init() {
            // Variável utilizada na página principal para retirar cabeçalho, banners e rodapé quando
            // a página for aberta através do link de pagamento via cartão
            $rootScope.TelaLimpa = true;

            self.numeroInscricao = $state.params.numeroInscricao;

            if (!window.location.href.includes("ofertaOnline")){
                buscarDadosPagamento();
            }
            else{
                buscarDadosPagamentoEdu();
            }

            $scope.$watch('controller.bandeira', function () {
                self.opcoesParcelamento = retornarOpcoesParcelamento();
            });
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function buscarDadosPagamento() {
            EdupsFinanceiroFactory.retornarDadosPagCartao(
                self.numeroInscricao,
                function (result) {
                if (result) {

                    self.pagamento = result;
                    self.opcoesParcelamento = retornarOpcoesParcelamento();

                    if (self.pagamento.CartaoCreditoParams.AtivarBandeiraVisa === 1) {
                        self.bandeira = '1';
                    }
                    else if (self.pagamento.CartaoCreditoParams.AtivarBandeiraMaster === 1) {
                        self.bandeira = '2';
                    }
                    else {
                        self.bandeira = '3';
                        self.pagCredito = false;
                    }
                }
            });

        }

        /**
         * Retorna informações sobre pagamento gerados no Educacional
         * */
        function buscarDadosPagamentoEdu() {
            EdupsFinanceiroFactory.retornarDadosPagCartaoEdu(
                self.numeroInscricao,
                function (result) {
                if (result) {

                    self.pagamento = result;
                    self.pagamento.DadosComprador = result;
                    self.opcoesParcelamento = retornarOpcoesParcelamento();

                    if (self.pagamento.CartaoCreditoParams.AtivarBandeiraVisa === 1) {
                        self.bandeira = '1';
                    }
                    else if (self.pagamento.CartaoCreditoParams.AtivarBandeiraMaster === 1) {
                        self.bandeira = '2';
                    }
                    else {
                        self.bandeira = '3';
                        self.pagCredito = false;
                    }
                }
            });

        }

        function retornarOpcoesParcelamento () {
            if (self.pagamento !== null && self.pagamento !== undefined) {
                if (self.bandeira === '1') {
                    return self.pagamento.OpcoesParcelamentoVisa.OpcoesParcelamento;
                }
                else {
                    return self.pagamento.OpcoesParcelamentoMasterCard.OpcoesParcelamento;
                }
            }
        }

        function pagarCartao () {
            var precoProduto = Number(self.pagamento.PrecoProduto).toFixed(2);

            // BuyPage
            if (self.pagamento.CartaoCreditoParams.ModeloECommerceCielo === 1) {
                var produto = self.bandeira === '3' ? 'A' : self.parcelaSelecionada !== '01' ? '2' : '1',
                urlRetorno = $location.absUrl().substring(0, $location.absUrl().indexOf('cartao?')) + 'statuspagcartao';

                // Se for débito a bandeira será Visa
                var bandeiraCartao = self.bandeira === '3' && produto === 'A' ? '1' : self.bandeira;

                EdupsFinanceiroFactory.retornarUrlPagBuyPage(
                    self.pagamento.IdBoleto,
                    precoProduto,
                    bandeiraCartao,
                    produto,
                    self.parcelaSelecionada,
                    urlRetorno,
                    self.codUsuarioPS,
                    function (result) {
                        if (result.IdTransacao !== undefined) {
                            var transacao = {};
                            transacao.codColigada = self.codColigada;
                            transacao.idTransacao = result.IdTransacao;
                            transacao.idTransacaoCielo = result.IdTransacaoCielo;
                            transacao.idBoleto = self.pagamento.IdBoleto;
                            transacao.descricaoBoleto = self.pagamento.DescProduto;
                            transacao.valorBoleto = precoProduto;
                            transacao.nomeRespFinanceiro = self.pagamento.DadosComprador.NomeComprador;
                            transacao.emailRespFinanceiro = self.pagamento.DadosComprador.EmailComprador;
                            transacao.numParcelas = self.parcelaSelecionada;
                            transacao.isCredito = self.bandeira !== '3';
                            transacao.codUsuarioPS = self.codUsuarioPS;
                            transacao.token = self.token;

                            $cookies['EduPSTransacaoCielo'] = angular.toJson(transacao);
                            abrirPaginaCielo(result.Url);
                        }
                    }
                );

            }
            //Checkout cielo
            else {
                EdupsFinanceiroFactory.retornarUrlPagCheckoutCielo(
                self.pagamento.IdBoleto,
                self.pagamento.DescProduto,
                precoProduto,
                self.pagamento.DadosComprador.NomeComprador,
                self.pagamento.DadosComprador.EmailComprador,
                self.pagamento.DadosComprador.CpfComprador,
                self.pagamento.DadosComprador.TelefoneComprador,
                function (result) {
                    if (result.IdTransacao !== undefined) {
                        var transacao = {};
                        transacao.codColigada = result.codColigada;
                        transacao.idTransacao = result.IdTransacao;
                        transacao.idBoleto = self.pagamento.IdBoleto;
                        transacao.codUsuarioPS = self.codUsuarioPS;
                        transacao.token = self.token;

                        $cookies['EduPSTransacaoCielo'] = transacao;
                        abrirPaginaCielo(result.Url);
                    }
                });
            }
        }

        function formataOpcaoParcela(key, value) {
            return key + ' x ' + value;
        }

        function retornaValorTotal() {
            if (self.opcoesParcelamento === undefined) {
                return;
            }

            // visa débito
            if (self.bandeira === '3') {
                return $filter('currency')(self.pagamento.PrecoProduto);
            } else { // crédito
                var parcelas = Number(self.parcelaSelecionada),
                    valorParcela = self.opcoesParcelamento[self.parcelaSelecionada].replace('.', '').replace(',', '.'),
                    valorTotal = parcelas * parseFloat(valorParcela);

                return $filter('currency')(valorTotal) + ' (' + parcelas + ' x ' + $filter('currency')(valorParcela) + ')';
            }
        }

        function abrirPaginaCielo(url) {
            // Janela da Cielo
            var winCielo = window;

            // Monta e abre a URL
            winCielo.open(
                url,
                '_blank', 'Height=653px,Width=981px,Top=50%,Left=50%,Position=absolute,help=no,status=yes,resizable=0,scrollbars=1');
        }
    }

});
