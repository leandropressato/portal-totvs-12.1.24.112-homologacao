/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
 * @module edupsFinanceiroModule
 * @name EdupsStatusPagBoletoController
 * @object controller
 *
 * @created 2016-11-23 v12.1.15
 * @updated
 *
 * @requires Financeiro.module
 *
 * @dependencies EdupsFinanceiroFactory
 *
 * @description Controller do status de pagamento do boleto com cartão
 */
define([
    'financeiro/financeiro.module',
    'financeiro/financeiro.route',
    'financeiro/financeiro.factory',
], function () {

    'use strict';

    angular.module('edupsFinanceiroModule')
        .controller('EdupsStatusPagCartaoController', EdupsStatusPagCartaoController);

    EdupsStatusPagCartaoController.$inject = ['$scope',
        '$filter',
        '$state',
        '$cookies',
        '$rootScope',
        'EdupsFinanceiroFactory',
        '$sce'
    ];

    function EdupsStatusPagCartaoController($scope,
        $filter,
        $state,
        $cookies,
        $rootScope,
        EdupsFinanceiroFactory,
        $sce) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;
        self.status = null;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.retornarStatusPagamento = retornarStatusPagamento;
        self.closeBtn = closeBtn;

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        init();

        function init() {

            // Variável utilizada na página principal para retirar cabeçalho, banners e rodapé quando 
            // a página for aberta através da urlRetorno do serviço da CIELO.
            $rootScope.TelaLimpa = true;

            retornarStatusPagamento();
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function retornarStatusPagamento() {
            var transacao = angular.fromJson($cookies['EduPSTransacaoCielo']);
            EdupsFinanceiroFactory.retornarStatusPagamento(
                transacao.idTransacao,
                transacao.idTransacaoCielo,
                transacao.idBoleto,
                transacao.descricaoBoleto,
                transacao.valorBoleto,
                transacao.nomeRespFinanceiro,
                transacao.emailRespFinanceiro,
                transacao.numParcelas,
                transacao.isCredito, 
                function (result) {
                if (result) {
                    self.status = result;
                }
            });

        }

        function closeBtn() {
            return $sce.trustAsHtml('<p><a class="btn btn-default btn-md" style="float:right" href="#" role="button" onclick="window.close()">Fechar Janela</a></p>');
        }
    }

});
