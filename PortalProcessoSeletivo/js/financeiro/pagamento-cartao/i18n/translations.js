[{
    "l-titulo": {
        "pt": "Lançamentos",
		"en": "Entries",
		"es": "Registros"
    },
    "l-data-vencimento": {
        "pt": "Data vencimento",
		"en": "Expiration date",
		"es": "Fecha de vencimiento"
    },
    "l-valor-bruto": {
        "pt": "Valor bruto",
		"en": "Gross value",
		"es": "Valor bruto"
    },
    "l-linha-digitavel": {
        "pt": "Linha digitável",
		"en": "Digitable row",
		"es": "Línea digitable"
    },    
    "l-situacao": {
        "pt": "Situação",
		"en": "Status",
		"es": "Situación"
    },
    "l-valorboleto": {
        "pt": "Valor bruto",
		"en": "Gross value",
		"es": "Valor bruto"
    },
    "l-vencimento": {
        "pt": "Vencimento",
		"en": "Expiration",
		"es": "Vencimiento"
    },
    "l-responsavel": {
        "pt": "Responsável",
		"en": "Responsible party",
		"es": "Responsable"
    },
    "l-servico": {
        "pt": "Serviço",
		"en": "Service",
		"es": "Servicio"
    },
    "l-parcela": {
        "pt": "Parcela",
		"en": "Installment",
		"es": "Cuota"
    },
    "l-cota": {
        "pt": "Cota",
		"en": "Quota",
		"es": "Cota"
    },
    "l-competencia": {
        "pt": "Competência",
		"en": "Reference",
		"es": "Competencia"
    },
    "l-dtbaixa": {
        "pt": "Dt. Baixa",
		"en": "Issue Date",
		"es": "Fch. Baja"
    },
    "l-valorbruto": {
        "pt": "Vlr bruto",
		"en": "Gross value",
		"es": "Val. Bruto"
    },
    "l-valordesconto": {
        "pt": "Valor de desconto",
		"en": "Discount value",
		"es": "Valor de descuento"
    },
    "l-juros": {
        "pt": "Juros",
		"en": "Insterests",
		"es": "Intereses"
    },
    "l-multa": {
        "pt": "Multa",
		"en": "Fine",
		"es": "Multa"
    },
    "l-desconto": {
        "pt": "Desconto",
		"en": "Discount",
		"es": "Descuento"
    },
    "l-baixado": {
        "pt": "Baixado",
		"en": "Issued",
		"es": "Dado de baja"
    },
    "l-bolsa": {
        "pt": "Bolsa",
		"en": "Schorlarship",
		"es": "Beca"
    },
    "l-filtrarpor": {
        "pt": "Filtrar por",
		"en": "Filter by",
		"es": "Filtrar por"
    },
    "l-pago": {
        "pt": "Pago",
		"en": "Paid",
		"es": "Pagado"
    },
    "l-aberto": {
        "pt": "Em aberto",
		"en": "Outstanding",
		"es": "Pendiente"
    },
    "l-pago-parcialmente": {
        "pt": "Pago parcialmente",
		"en": "Partially paid",
		"es": "Pagado parcialmente"
    },
    "l-cancelado": {
        "pt": "Cancelado",
		"en": "Canceled",
		"es": "Anulado"
    },
    "l-produto": {
        "pt": "Produto",
		"en": "Product",
		"es": "Producto"
    },
    "l-redirecionarbanco": {
        "pt": "Você será redirecionado para o site do banco da instituição para emissão do seu boleto. Deseja continuar?",
		"en": "You will be redictered to the site of the institution bank to issue your banking slip. Continue?",
		"es": "Usted será reorientado al sitio del banco de la institución para la emisión de la boleta. ¿Desea continuar?"
    },
    "l-pagar": {
        "pt": "Pagar",
		"en": "Pay",
		"es": "Pagar"
    },
    "l-valorop1": {
        "pt": "Bolsa",
		"en": "Scholarship",
		"es": "Beca"
    },
    "l-valorop2": {
        "pt": "Pago a maior",
		"en": "Overstated pay",
		"es": "Pago a más"
    },
    "l-valorop3": {
        "pt": "Pago a menor",
		"en": "Understated pay",
		"es": "Pago a menos"
	},
    "l-valorop4": {
        "pt": "Desconta no líquido",
		"en": "Discounts in the net",
		"es": "Descuenta en el neto"
    },
    "l-valorop5": {
        "pt": "Acrescenta no líquido",
		"en": "Increases in the net",
		"es": "Aumenta en el neto"
    },
    "l-valorop6": {
        "pt": "ACRÉSCIMO (+)",
		"en": "INCREASE (+)",
		"es": "AUMENTO (+)"
    },
    "l-valorop7": {
        "pt": "DECRÉSCIMO (-)",
		"en": "DECREASE (-)",
		"es": "DECREMENTO (-)"
    },
    "l-valorop8": {
        "pt": "Valor opcional 8",
		"en": "Optional value 8",
		"es": "Valor opcional 8"
    },    
    "l-acrescimo-acordo": {
        "pt": "Acréscimo do acordo",
		"en": "Institutional Assessment",
		"es": "Aumento del acuerdo"
    },
    "l-juros-acordo": {
        "pt": "Juros do acordo",
		"en": "Interests of the agreement",
		"es": "Intereses del acuerdo"
    },
    "l-desconto-acordo": {
        "pt": "Desconto do acordo",
		"en": "Discount of the agreement",
		"es": "Descuento del acuerdo"
    },
    "l-pagamento": {
        "pt": "Pagamento",
		"en": "Payment",
		"es": "Pago"
    },
    "l-dadoscadastrais": {
        "pt": "Dados cadastrais",
		"en": "Register data",
		"es": "Datos de registro"
    },
    "l-nome": {
        "pt": "Nome",
		"en": "Name",
		"es": "Nombre"
    },
    "l-email": {
        "pt": "E-Mail",
		"en": "E-Mail",
		"es": "E-Mail"
    },
    "l-bairro": {
        "pt": "Bairro",
		"en": "District",
		"es": "Barrio"
    },
    "l-uf": {
        "pt": "UF",
		"en": "State",
		"es": "Estado/Prov/Reg"
    },
    "l-telefone": {
        "pt": "Telefone",
		"en": "Phone number",
		"es": "Teléfono"
    },
    "l-cpf": {
        "pt": "CPF",
		"en": "CPF",
		"es": "RCPF"
    },
    "l-endereco": {
        "pt": "Endereço",
		"en": "Address",
		"es": "Dirección"
    },
    "l-cidade": {
        "pt": "Cidade",
		"en": "City",
		"es": "Ciudad"
    },
    "l-cep": {
        "pt": "CEP",
		"en": "Zip code",
		"es": "CP"
    },
    "l-dadospagamento": {
        "pt": "Dados do pagamento",
		"en": "Payment data",
		"es": "Datos del pago"
    },
    "l-referentea": {
        "pt": "Referente a",
		"en": "Related to",
		"es": "Referente a"
    },
    "l-valor": {
        "pt": "Valor",
		"en": "Value",
		"es": "Valor"
    },
    "l-valortotal": {
        "pt": "Valor total",
		"en": "Total value",
		"es": "Valor total"
    },
    "l-numparcelas": {
        "pt": "Nº de parcelas",
		"en": "Number of installments",
		"es": "Nº de cuotas"
    },
    "l-formapagamento": {
        "pt": "Forma de pagamento",
		"en": "Payment term",
		"es": "Forma de pago"
    },
    "l-credito": {
        "pt": "Cartão de Crédito / Débito",
		"en": "Credit Card",
		"es": "Tarjeta de crédito"
    },
    "l-debito": {
        "pt": "Cartão de Débito",
		"en": "Debit Card",
		"es": "Tarjeta de débito"
    },
    "l-parcelamento": {
        "pt": "Parcelas",
		"en": "Installments",
		"es": "Cuotas"
    },
    "l-erro": {
        "pt": "Ocorreu erro na consulta/captura da transação",
		"en": "Error in query/capture of transaction",
		"es": "Ocurrió un erro en la consulta/captura de la transacción"
    },
    "l-codigo": {
        "pt": "Código",
		"en": "Code",
		"es": "Código"
    },
    "l-mensagem": {
        "pt": "Mensagem",
		"en": "Message",
		"es": "Mensaje"
    },
    "l-pagnaoautenticado": {
        "pt": "Pagamento não autenticado",
		"en": "Payment not authenticated",
		"es": "Pago no autenticado"
    },
    "l-pagnaoautorizado": {
        "pt": "Pagamento não autorizado",
		"en": "Payment not authorized",
		"es": "Pago no autorizado"
    },
    "l-pagcapturado": {
        "pt": "Pagamento realizado com sucesso",
		"en": "Payment successfully made",
		"es": "Pago realizado con éxito"
    },
    "l-pagemautenticacao": {
        "pt": "Pagamento em autenticação",
		"en": "Payment without authentication",
		"es": "Pago en autenticación"
    },
    "l-respfinanceiro": {
        "pt": "Resp. financeiro",
		"en": "Financial Resp.",
		"es": "Resp. Financiero"
    },
    "l-datahora": {
        "pt": "Data/hora",
		"en": "Date/time",
		"es": "Fecha/hora"
    },
    "l-tid": {
        "pt": "Código de Verificação",
		"en": "Verification Code",
		"es": "Código de verificación"
    },
    "l-numpedido": {
        "pt": "Número do pedido",
		"en": "Number of order",
		"es": "Número del pedido"
    },
    "l-numautor": {
        "pt": "Número de autorização",
		"en": "Number of authorization",
		"es": "Número de autorización"
    },
    "l-nsu": {
        "pt": "Número Sequencial Único",
		"en": "Single Sequential Number",
		"es": "Número secuencial único"
    }
}]
