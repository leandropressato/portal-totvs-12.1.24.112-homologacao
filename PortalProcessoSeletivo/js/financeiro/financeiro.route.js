/**
* @license TOTVS | Portal Processo Seletivo v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

define(['financeiro/financeiro.module'], function () {

    'use strict';

    angular
        .module('edupsFinanceiroModule')
        .config(EduPSFinanceiroRouteConfig);

    EduPSFinanceiroRouteConfig.$inject = ['$stateProvider'];

    function EduPSFinanceiroRouteConfig($stateProvider) {

        $stateProvider.state('financeiro', {
            abstract: true,
            template: '<ui-view/>'
        }).state('financeiro.boleto', {
            url: '/:nivelEnsino/financeiro/boleto?{numeroInscricao:int}',
            controller: 'EdupsStatusPagBoletoController',
            controllerAs: 'controller',
            templateUrl: 'js/templates/financeiro-pagboleto.view.html',
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsFinanceiroModule',
                        files: ['js/financeiro/pagamento-boleto/financeiro-pagboleto.controller.js']
                    }]);
                }]
            }
        }).state('financeiro.cartao', {
            url: '/:nivelEnsino/financeiro/cartao?{numeroInscricao:int}',
            controller: 'EdupsPagCartaoController',
            controllerAs: 'controller',
            templateUrl: 'js/templates/financeiro-pagcartao.view.html',
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsFinanceiroModule',
                        files: ['js/financeiro/pagamento-cartao/financeiro-pagcartao.controller.js']
                    }]);
                }]
            }
        }).state('financeiro.statuspagcartao', {
            url: '/:nivelEnsino/financeiro/statuspagcartao',
            controller: 'EdupsStatusPagCartaoController',
            controllerAs: 'controller',
            templateUrl: 'js/templates/financeiro-pagcartao-status.view.html',
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsFinanceiroModule',
                        files: ['js/financeiro/pagamento-cartao/financeiro-pagcartao-status.controller.js']
                    }]);
                }]
            }
        }).state('financeiro.eduboleto', {
            url: '/:nivelEnsino/financeiro/boleto?{numeroInscricao:int}&{ofertaOnline:int}',
            controller: 'EdupsStatusPagBoletoController',
            controllerAs: 'controller',
            templateUrl: 'js/templates/financeiro-pagboleto.view.html',
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsFinanceiroModule',
                        files: ['js/financeiro/pagamento-boleto/financeiro-pagboleto.controller.js']
                    }]);
                }]
            }
        }).state('financeiro.educartao', {
            url: '/:nivelEnsino/financeiro/cartao?{numeroInscricao:int}&{ofertaOnline:int}',
            controller: 'EdupsPagCartaoController',
            controllerAs: 'controller',
            templateUrl: 'js/templates/financeiro-pagcartao.view.html',
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsFinanceiroModule',
                        files: ['js/financeiro/pagamento-cartao/financeiro-pagcartao.controller.js']
                    }]);
                }]
            }
        });
    }
});
