[{
    "l-boleto": {
        "pt": "Boleto",
		"en": "Bank slip",
		"es": "Boleta"
    },
    "l-redirecionar": {
        "pt": "Você será redirecionado para a geração do boleto. Deseja continuar?",
		"en": "You will be redirected to bank slip generation. Continue?",
		"es": "Usted será reorientado para la generación de la boleta. ¿Desea continuar?"
    },
    "l-redirecionarbanco": {
        "pt": "Você será redirecionado para o site do banco da instituição para emissão do seu boleto. Deseja continuar?",
		"en": "You will be redirected to the site of the institution bank for the issue of your bank slip. Continue?",
		"es": "Usted será reorientado al sitio del banco de la institución para la emisión de su boleta. ¿Desea continuar?"
    },
    "l-yes": {
        "pt": "Sim",
		"en": "Yes",
		"es": "Sí"
    },
    "l-no": {
        "pt": "Não",
		"en": "No",
		"es": "No"
    },
    "l-erro-geracao-boleto": {
        "pt": "Não foi possível realizar a geração do boleto",
		"en": "Unable to generate bank slip",
		"es": "No fue posible realizar la generación de la boleta"
    },
    "l-boleto-pagamento": {
        "pt": "Boleto de pagamento de inscrição",
		"en": "Bank slip of registration payment",
		"es": "Boleta de pago de inscripción"
    },
    "l-erro-boleto-naoregistrado": {
        "pt": "Não foi possível realizar a geração do boleto. O boleto não foi registrado!",
		"en": "Unable to generate bank slip. The bank slip was not registered!",
		"es": "No fue posible realizar la generación de la boleta. ¡La boleta no se registró!"
    }
}]
