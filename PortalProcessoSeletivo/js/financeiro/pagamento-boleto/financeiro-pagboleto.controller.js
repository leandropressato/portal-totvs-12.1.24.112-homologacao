/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduLancamentosModule
 * @name EduLancamentosController
 * @object controller
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @requires Lancamentos.module
 *
 * @dependencies eduLancamentosFactory
 *
 * @description Controller do Lancamentos
 */
define(['financeiro/financeiro.module',
    'financeiro/financeiro.factory',
    'utils/edups-utils.service',
    'utils/edups-utils.factory',
    'utils/edups-utils.service',
    'utils/reports/edups-relatorio.module',
    'utils/reports/edups-relatorio.service'
], function () {

    'use strict';

    angular
        .module('edupsFinanceiroModule')
        .controller('EdupsStatusPagBoletoController', EdupsStatusPagBoletoController);

    EdupsStatusPagBoletoController.$inject = [
        '$rootScope',
        '$window',
        '$filter',
        '$state',
        '$sce',
        'EdupsFinanceiroFactory',
        'EduPsRelatorioService',
        'edupsUtilsService',
        'totvs.app-notification.Service'
    ];

    /**
     * Controller Pagamento de Boleto
     * @param {object} $rootScope - Objeto rootScope
     * @param {object} $window - Objeto window
     * @param {object} $filter - Objeto filter
     * @param {object} $state - Objeto state
     * @param {object} $sce - Objeto state
     * @param {object} EdupsFinanceiroFactory - Factory Financeiro
     * @param {object} EduPsRelatorioService - Service Relatório
     * @param {object} EdupsUtilsService - Service Util
     * @param {object} totvsNotification - Totvs notification
     */
    function EdupsStatusPagBoletoController($rootScope, $window, $filter, $state, $sce, EdupsFinanceiroFactory, EduPsRelatorioService, EdupsUtilsService, totvsNotification) {

        var self = this;

        // Variáveis
        self.infoBoletoObj = [];
        self.parametros = $state.params;

        //Métodos
        self.geraBoletoPDF = geraBoletoPDF;
        self.retornaInfoBoleto = retornaInfoBoleto;
        self.redirecionaBoletoFixo = redirecionaBoletoFixo;
        self.redirecionaUrlRegOnline = redirecionaUrlRegOnline;

        // Variável utilizada na página principal para retirar cabeçalho, banners e rodapé quando
        // a página for aberta através do link de geração de comprovante
        $rootScope.TelaLimpa = true;

        init();

        /**
         * Inicialia o controller
         */
        function init() {
            if (!window.location.href.includes("ofertaOnline")){
                retornaInfoBoleto(imprimeBoleto);
            }
            else{
                retornaInfoBoletoEdu(imprimeBoletoEdu);
            }
        }

        /**
         * Retorna as informações do boleto de inscrição
         *
         * @param {function} callback - Função de retorno
         */
        function retornaInfoBoleto(callback) {
            EdupsFinanceiroFactory.retornaInfoBoletoInscricao(
                self.parametros.numeroInscricao,
                function (result) {
                    if (result) {
                        self.infoBoletoObj = result[0];

                        if (typeof callback === 'function') {
                            callback(result[0]);
                        }
                    }
                });
        }

         /**
         * Retorna as informações do boleto de inscrição
         *
         * @param {function} callback - Função de retorno
         */
        function retornaInfoBoletoEdu(callback) {
            EdupsFinanceiroFactory.retornaInfoBoletoMatricula(
                self.parametros.numeroInscricao,
                function (result) {
                    if (result) {
                        self.infoBoletoObj = result[0];

                        if (typeof callback === 'function') {
                            callback(result[0]);
                        }
                    }
                });
        }

        /**
         * Imprime o boleto, depedendo das condições
         *
         * @param {object} dadosBoleto - Objeto que contêm as informações do boleto
         */
        function imprimeBoleto(dadosBoleto) {
            if (angular.isDefined(dadosBoleto)) {
                if (dadosBoleto.BOLETOREGISTRADO === 'T' ||
                    dadosBoleto.SHOWBOLETONAOREGPORTAL === 'T') {
                    if (dadosBoleto.TIPOBOLETO === 'HTML') {
                        redirecionaBoletoFixo(dadosBoleto.URLBOLETOFIXO);
                    }
                    else {
                        geraBoletoPDF(dadosBoleto.IDBOLETO);
                    }
                } else {
                    totvsNotification.notify({
                                type: 'error',
                                title: $filter('i18n')('l-boleto-pagamento', [], 'js/financeiro/pagamento-boleto'),
                                detail: $filter('i18n')('l-erro-boleto-naoregistrado', [], 'js/financeiro/pagamento-boleto'),
                            });
                }
            }
        }

        /**
         * Imprime o boleto, depedendo das condições
         *
         * @param {object} dadosBoleto - Objeto que contêm as informações do boleto
         */
        function imprimeBoletoEdu(dadosBoleto) {
            if (dadosBoleto && dadosBoleto.Bytes) {

                EdupsFinanceiroFactory.getEduParam(self.parametros.numeroInscricao, function (paramEdu) {
                
                    //Para boleto registrado, abre o boleto em PDF normalmente.
                    if (dadosBoleto.BOLETOREGISTRADO === true ||
                        paramEdu.ShowBoletoNaoRegistradoPortal) {

                            var bytes = dadosBoleto.Bytes;
                            self.exibirObjChrome = !!window.chrome && !!window.chrome.webstore;

                            if (self.exibirObjChrome) {
                                var blob = EdupsUtilsService.b64toBlob(bytes, 'application/pdf');
                                var fileURL = URL.createObjectURL(blob);
                                self.pdfcontent = $sce.trustAsResourceUrl(fileURL);
                            }
                            EduPsRelatorioService.exibirOuSalvarPDFParams(bytes, '_self');
                    } else if (dadosBoleto.PERMITEREGONLINE === true) {

                        //O usuário é informado que será redirecionado para o site do banco,
                        //e confirma se deseja continuar ou não.
                        totvsNotification.question({
                            title: i18nFilter('l-boleto-pagamento', [], 'js/financeiro/lancamentos'),
                            text: i18nFilter('l-redirecionarbanco', [], 'js/financeiro/lancamentos'),
                            size: 'sm',
                            cancelLabel: 'l-no',
                            confirmLabel: 'l-yes',
                            callback: function(opcaoEscolhida) {
                                if (opcaoEscolhida) {
                                    $window.open(dadosBoleto.URLREGONLINE);
                                }
                            }
                        });
                    } else if (!paramEdu.ShowBoletoNaoRegistradoPortal) {
                        totvsNotification.notify({
                            type: 'warning',
                            title: $filter('i18n')('l-boleto-pagamento', [], 'js/financeiro/pagamento-boleto'),
                            detail: $filter('i18n')('l-erro-boleto-naoregistrado', [], 'js/financeiro/pagamento-boleto')
                        });
                    } else {
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-boleto-pagamento', [], 'js/financeiro/lancamentos'),
                            detail: i18nFilter('l-erro-gerar-boleto', [], 'js/financeiro/lancamentos')
                        });
                    }
                });
            }
        }

        /**
         * Realiza o redirecionamento para o boleto fixo
         *
         * @param {string} urlBoletoFixo - URL a qual o sistema irá redirecionar
         */
        function redirecionaBoletoFixo(urlBoletoFixo) {
            $window.open(urlBoletoFixo, '_self');
        }

        /**
         * Realiza o redirecionamento para o boleto de registro online
         *
         * @param {string} urlRegOnline - URL a qual o sistema irá redirecionar
         */
        function redirecionaUrlRegOnline(urlRegOnline) {

            totvsNotification.question({
                title: $filter('i18n')('l-boleto', [], 'js/financeiro/pagamento-boleto'),
                text: $filter('i18n')('l-redirecionarbanco', [], 'js/financeiro/pagamento-boleto'),
                size: 'sm',
                cancelLabel: $filter('i18n')('l-no', [], 'js/financeiro/pagamento-boleto'),
                confirmLabel: $filter('i18n')('l-yes', [], 'js/financeiro/pagamento-boleto'),
                callback: function (opcaoEscolhida) {
                    if (opcaoEscolhida) {
                        $window.open(urlRegOnline, '_self');
                    }
                }
            });
        }

        /**
         * Gera o boleto a partir do array de bytes
         *
         * @param {int} idBoleto - Identificador do boleto
         */
        function geraBoletoPDF(idBoleto) {
            EdupsFinanceiroFactory.retornaBoletoCandidato(
                idBoleto,
                function (result) {
                    if (angular.isDefined(result) && result !== null && result.length > 0) {
                        var bytes = result[0].BYTES;
                        self.exibirObjChrome = !!window.chrome && !!window.chrome.webstore;

                        if (self.exibirObjChrome) {
                            var blob = EdupsUtilsService.b64toBlob(bytes, 'application/pdf');
                            var fileURL = URL.createObjectURL(blob);
                            self.pdfcontent = $sce.trustAsResourceUrl(fileURL);                    
                        } else 
                        EduPsRelatorioService.exibirOuSalvarPDFParams(bytes, '_self');
                    }
                    else {
                        totvsNotification.notify({
                            type: 'error',
                            title: $filter('i18n')('l-boleto-pagamento', [], 'js/financeiro/pagamento-boleto'),
                            detail: $filter('i18n')('l-erro-geracao-boleto', [], 'js/financeiro/pagamento-boleto'),
                        });
                    }
                });
        }
    }
});
