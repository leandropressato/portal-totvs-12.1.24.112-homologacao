/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

define(['financeiro/financeiro.module'], function () {

    'use strict';

    angular
        .module('edupsFinanceiroModule')
        .factory('EdupsFinanceiroFactory', EdupsFinanceiroFactory);

    EdupsFinanceiroFactory.$inject = ['$totvsresource'];

    // *********************************************************************************
    // *** Factory
    // *********************************************************************************
    function EdupsFinanceiroFactory($totvsresource) {

        var factory = this,
            url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS + '/Financeiro';

        factory.retornarDadosPagCartao = retornarDadosPagCartao;
        factory.retornarDadosPagCartaoEdu = retornarDadosPagCartaoEdu;
        factory.retornarUrlPagBuyPage = retornarUrlPagBuyPage;
        factory.retornarUrlPagCheckoutCielo = retornarUrlPagCheckoutCielo;
        factory.retornarStatusPagamento = retornarStatusPagamento;
        factory.retornaInfoBoletoInscricao = retornaInfoBoletoInscricao;
        factory.retornaBoletoCandidato = retornaBoletoCandidato;
        factory.retornaInfoBoletoMatricula = retornaInfoBoletoMatricula;
        factory.getEduParam = getEduParam;
        return factory;

        /**
         * Buscar registros dos dados para pagamento do boleto com cartão.
         *
         * @param {any} numeroInscricao - Número de inscrição do aluno.
         * @param {any} callback - Função de callback, se necessário.
         * @returns Dados para pagamento do boleto com cartão.
         */
        function retornarDadosPagCartao(numeroInscricao, callback) {

            var parameters = {
                numeroInscricao: numeroInscricao
            },
            factory = getInstanciaFactory('/DadosPagCartao');

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Buscar registros dos dados para pagamento do boleto com cartão do Educacional.
         *
         * @param {any} numeroInscricao - Número de inscrição do aluno.
         * @param {any} callback - Função de callback, se necessário.
         * @returns Dados para pagamento do boleto com cartão Educacional.
         */
        function retornarDadosPagCartaoEdu(numeroInscricao, callback) {

            var parameters = {
                numeroInscricao: numeroInscricao
            },
            factory = getInstanciaFactory('/DadosPagCartaoMatric');

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Buscar url para pagamento BuyPage.
         *         
         * @param {any} idBoleto - Id do boleto para pagamento.
         * @param {any} valorBoleto - Id do boleto para pagamento.
         * @param {any} bandeira - Id do boleto para pagamento.
         * @param {any} produto - Id do boleto para pagamento.
         * @param {any} parcelas - Id do boleto para pagamento.
         * @param {any} urlRetorno - Id do boleto para pagamento.
         * @param {any} codUsuarioPS - Código do usuário do processo seletivo.         
         * @param {any} callback - Função de callback, se necessário.
         * @returns Dados para pagamento do boleto com cartão.
         */
        function retornarUrlPagBuyPage(
            idBoleto,
            valorBoleto,
            bandeira,
            produto,
            parcelas,
            urlRetorno,
            codUsuarioPS,
            callback) {

            var parameters = {                
                idBoleto: idBoleto,
                valorBoleto: valorBoleto,
                bandeira: bandeira,
                produto: produto,
                parcelas: parcelas,
                urlRetorno: urlRetorno
            },
            factory = getInstanciaFactory('/UrlBuyPageCielo');

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Buscar url para pagamento Checkout Cielo.
         *         
         * @param {any} idBoleto - Id do boleto para pagamento.
         * @param {any} descricaoBoleto - Id do boleto para pagamento.
         * @param {any} valorBoleto - Id do boleto para pagamento.
         * @param {any} nomeRespFinanceiro - Id do boleto para pagamento.
         * @param {any} emailRespFinanceiro - Id do boleto para pagamento.
         * @param {any} cpfRespFinanceiro - Id do boleto para pagamento.
         * @param {any} telefoneRespFinanceiro - Id do boleto para pagamento.         
         * @param {any} callback - Função de callback, se necessário.
         * @returns Dados para pagamento do boleto com cartão.
         */
        function retornarUrlPagCheckoutCielo(
            idBoleto,
            descricaoBoleto,
            valorBoleto,
            nomeRespFinanceiro,
            emailRespFinanceiro,
            cpfRespFinanceiro,
            telefoneRespFinanceiro,
            callback) {

            var parameters = {
                idBoleto: idBoleto,
                descricaoBoleto: descricaoBoleto,
                valorBoleto: valorBoleto,
                nomeRespFinanceiro: nomeRespFinanceiro,
                emailRespFinanceiro: emailRespFinanceiro,
                cpfRespFinanceiro: cpfRespFinanceiro,
                telefoneRespFinanceiro: telefoneRespFinanceiro
            },
            factory = getInstanciaFactory('/UrlCheckOutCielo');

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         *  Retorna o status do pagamento Buy Page cielo.
         *         
         * @param {any} idTransacaoFin - Identificador transação financeiro.
         * @param {any} idTransacaoCielo - Identificador transação Cielo.
         * @param {any} idBoleto - Id do boleto.
         * @param {any} descricaoBoleto - Descrição do boleto.
         * @param {any} valorBoleto - Valor do boleto.
         * @param {any} nomeRespFinanceiro - Nome do resp. financeiro.
         * @param {any} emailRespFinanceiro - Email do resp. financeiro.
         * @param {any} numParcelas - Número de parcelas.
         * @param {any} isCredito - Se é pagamento por cartão de crédito.
         * @param {any} callback - Função de callback, se necessário.
         * @returns Status do pagamento Buy Page cielo.
         */
        function retornarStatusPagamento(
            idTransacaoFin,
            idTransacaoCielo,
            idBoleto,
            descricaoBoleto,
            valorBoleto,
            nomeRespFinanceiro,
            emailRespFinanceiro,
            numParcelas,
            isCredito,
            callback) {
                
            var parameters = {                
                idTransacaoFin: idTransacaoFin,
                idTransacaoCielo: idTransacaoCielo,
                idBoleto: idBoleto,
                descricaoBoleto: descricaoBoleto,
                valorBoleto: valorBoleto,
                nomeRespFinanceiro: nomeRespFinanceiro,
                emailRespFinanceiro: emailRespFinanceiro,
                numParcelas: numParcelas,
                isCredito: isCredito
            },
            factory = getInstanciaFactory('/StatusBuyPageCielo');

            return factory.TOTVSGet(parameters, callback);
        }

        function retornaInfoBoletoInscricao(numeroInscricao, callback) {
            var parameters = {
                    numeroInscricao: numeroInscricao
                },
                factory = getInstanciaFactory('/InfoBoletoInscricao');

            return factory.TOTVSQuery(parameters, callback);

        }

        /**
         *  Retorna informações do boleto Educacional.
         *         
         * @param {any} numeroInscricao - Identificadorda inscrição.
         * @param {any} callback - Função de callback, se necessário.
         * @returns Status do pagamento Buy Page cielo.
         */
        function retornaInfoBoletoMatricula(numeroInscricao, callback) {
            var parameters = {
                    numeroInscricao: numeroInscricao
                },
                factory = getInstanciaFactory('/InfoBoletoMatricula');

            return factory.TOTVSQuery(parameters, callback);

        }

        function retornaBoletoCandidato(idBoleto, callback) {
            var parameters = {
                    idBoleto: idBoleto
                },
                factory = getInstanciaFactory('/2aviaBoletoCandidato');

            return factory.TOTVSQuery(parameters, callback);
        }

        //Retorna o objeto factory a partir da URL padrão juntamento ao complemeto
        function getInstanciaFactory(urlComplemento) {
            var urlCompletaServico = url + urlComplemento;
            return $totvsresource.REST(urlCompletaServico, {}, {});
        }

        /**
         * @public
         * @function Retorna parâmetros do Educacional
         * @name  getEduParam
         * @param   {int}    numeroInscricao     Número da inscrição
         * @callback Objeto 
         * @returns {object} Objeto com parâmetros
         */
        function getEduParam(numeroInscricao, callback) {
           
            var parameters = {
                numeroInscricao: numeroInscricao
            },
            factory = getInstanciaFactory('/InfoEduParam');

        return factory.TOTVSGet(parameters, callback);
        }
    }
});
