[{
    "l-Titulo-Arquivos": {
        "pt": "ARQUIVOS",
		"en": "FILES",
		"es": "ARCHIVOS"
    },
    "l-candidato": {
        "pt": "Candidato",
		"en": "Applicant",
		"es": "Candidato"
    },
    "l-email": {
        "pt": "Email",
		"en": "E-mail",
		"es": "Email"
    },
    "l-areainteresse": {
        "pt": "Área de interesse",
		"en": "Area of interest",
		"es": "Área de interés"
    },
    "l-areainteresse-popup": {
        "pt": "Você possui mais de uma inscrição neste processo seletivo. Selecione abaixo uma delas:",
		"en": "You have more than a registration in this selective process. Select one of them below:",
		"es": "Usted tiene más de una inscripción en este proceso de selección. A continuación, seleccione una de estas:"
    },
    "l-statusinscricao": {
        "pt": "Status da inscrição",
		"en": "Registration status",
		"es": "Estatus de la inscripción"
    },
    "l-visualizarboleto": {
        "pt": "Visualizar boleto",
		"en": "View bank slip",
		"es": "Visualizar boleta"
    },
    "l-processoseletivo": {
        "pt": "Processo seletivo",
		"en": "Selective process",
		"es": "Proceso de selección"
    },
    "l-inscrito": {
        "pt": "Inscrito",
		"en": "Registered",
		"es": "Inscrito"
    },
    "l-inscricaoconfirmada": {
        "pt": "Candidato (inscrição confirmada)",
		"en": "Applicant (registration confirmed)",
		"es": "Candidato (inscripción confirmada)"
    },
    "l-inscricao": {
        "pt": "Inscrição",
		"en": "Registration",
		"es": "Inscripción"
    },
    "l-dependente": {
        "pt": "Dependente(s)",
		"en": "Dependent(s)",
		"es": "Dependiente(s)"
    },
    "l-pagamento": {
        "pt": "Pagamento",
		"en": "Payment",
		"es": "Pago"
    },
    "l-confirmado": {
        "pt": "Confirmado",
		"en": "Confirmed",
		"es": "Confirmado"
    },
    "l-aguardandopagamento": {
        "pt": "Aguardando pagamento",
		"en": "Waiting payment",
		"es": "Esperando pago"
    },
    "l-vencimento": {
        "pt": "Vencimento",
		"en": "Expiration",
		"es": "Vencimiento"
    },
    "l-selecao": {
        "pt": "Seleção",
		"en": "Selection",
		"es": "Selección"
    },
    "l-resultado": {
        "pt": "Resultado",
		"en": "Result",
		"es": "Resultado"
    },
    "l-boleto": {
        "pt": "Boleto",
		"en": "Bank slip",
		"es": "Boleta"
    },
    "l-cartaodecredito": {
        "pt": "Cartão de Crédito / Débito",
		"en": "Credit card",
		"es": "Tarjeta de crédito"
    },
    "l-seminscricao": {
        "pt": "Você não possui inscrições neste processo seletivo",
		"en": "You do not have registrations in this selective process",
		"es": "Usted no tiene inscripciones en este proceso de selección"
    },
    "l-naoprevisto": {
        "pt": "Não previsto",
		"en": "Not estimated",
		"es": "No previsto"
    },
    "l-formainscricao": {
        "pt": "Forma de inscrição",
		"en": "Registration method",
		"es": "Forma de inscripción"
    },
    "l-localrealizacao": {
        "pt": "Local de realização",
		"en": "Venue",
		"es": "Lugar de realización"
    },
    "l-campus": {
        "pt": "Campus",
		"en": "Campus",
		"es": "Campus"
    },
    "l-datainscricao": {
        "pt": "Data da inscrição",
		"en": "Registration date",
		"es": "Fecha de la inscripción"
    },
    "l-datapagamento": {
        "pt": "Data pagamento",
		"en": "Payment date",
		"es": "Fecha de pago"
    },
    "l-areasinteresseselecionada": {
        "pt": "Áreas de interesse selecionadas",
		"en": "Areas of interest selected",
		"es": "Áreas de interés seleccionadas"
    },
    "l-numeroinscricao": {
        "pt": "Número da inscrição",
		"en": "Number of registration",
		"es": "Número de inscripción"
    },
    "l-opcoesidioma": {
        "pt": "Opções de idiomas selecionados",
		"en": "Options of selected languages",
		"es": "Opciones de idiomas seleccionados"
    },
    "l-opcao": {
        "pt": "º opção",
		"en": "st option",
		"es": "º opción"
    },
    "l-opcao-a": {
        "pt": "ª opção",
		"en": "nd option",
		"es": "ª opción"
    },
    "l-sair": {
        "pt": "Sair",
		"en": "Exit",
		"es": "Salir"
    },
    "l-alterar-senha": {
        "pt": "Alterar senha",
		"en": "Change password",
		"es": "Modificar contraseña"
    },
    "l-titulo": {
        "pt": "Processo Seletivo",
		"en": "Selective Process",
		"es": "Proceso de selección"
    },
    "l-alterar-senha-titulo": {
        "pt": "Alterar senha",
		"en": "Edit password",
		"es": "Modificar contraseña"
    },
    "l-btn-salvar": {
        "pt": "Salvar",
		"en": "Save",
		"es": "Grabar"
    },
    "l-senha-atual": {
        "pt": "Digite a senha atual",
		"en": "Enter current password",
		"es": "Digite la contraseña actual"
    },
    "l-msg-senha-atual": {
        "pt": "Senha atual",
		"en": "Current password",
		"es": "Contraseña actual"
    },
    "l-senha-nova": {
        "pt": "Digite a nova senha",
		"en": "Enter new password",
		"es": "Digite la nueva contraseña"
    },
    "l-msg-senha-nova": {
        "pt": "Nova senha",
		"en": "New password",
		"es": "Nueva contraseña"
    },
    "l-repetir-senha": {
        "pt": "Digite a nova senha novamente",
		"en": "Enter new password again",
		"es": "Digite la nueva contraseña nuevamente"
    },
    "l-msg-repetir-senha": {
        "pt": "Repetição da senha",
		"en": "Repetition of password",
		"es": "Repetición de la contraseña"
    },
    "l-campos-obrigatorios": {
        "pt": "Os campos a seguir devem ser preenchidos: ",
		"en": "The following fields must be completed:",
		"es": "Deben completarse los siguientes campos: "
    },
    "l-atencao": {
        "pt": "ATENÇÃO",
		"en": "ATTENTION",
		"es": "ATENCIÓN"
    },
    "l-senha-diferente": {
        "pt": "A confirmação da senha não confere com a senha digitada.",
		"en": "The confirmation of the password does not match the password entered.",
		"es": "La confirmación de la contraseña no coincide con la contraseña digitada."
    },
    "l-mostrar-senha": {
        "pt": "Mostrar senha",
		"en": "Display password",
		"es": "Mostrar contraseña"
    },
    "l-senha-nova-igual-atual": {
        "pt": "A nova senha deve ser diferente da atual.",
		"en": "The new password must be different than the current one.",
		"es": "La nueva contraseña debe ser diferente de la actual."
    },
    "l-comprovante": {
        "pt": "Comprovante",
		"en": "Receipt",
		"es": "Comprobante"
    },
    "l-opcao-bloqueada": {
        "pt": "Opção bloqueada. Entre em contato com a instituição para atualizar seus dados pessoais.",
		"en": "Option blocked. Contact institution to update your personal data.",
		"es": "Opción bloqueada. Entre en contacto con la institución para actualizar sus datos personales."
    },
    "l-editar-dados": {
        "pt": "Editar dados",
		"en": "Edit data",
		"es": "Editar datos"
    },
    "l-atencao-candidato-excedente": {
        "pt": "Atenção! Você está inscrito como ",
		"en": "Attention! You are registered as ",
		"es": "¡Atención! Usted está inscrito como "
    },
    "l-excedente": {
        "pt": "excedente.",
		"en": "surplus.",
		"es": "excedente."
    },
    "l-bloqueadorpopup": {
        "pt": "ATENÇÃO: Para que você consiga utilizar o portal corretamente, favor desabilitar o bloqueador de pop-up.",
		"en": "ATTENTION: To use the portal properly, disable the pop-up blocker, please.",
		"es": "ATENCIÓN: para que consiga utilizar el portal correctamente, por favor desactive el bloqueador de pop-up."
    },
    "l-msg-inscricao-paga": {
        "pt": "A taxa de inscrição já foi paga!",
		"en": "The registration fee has already been paid!",
		"es": "¡La tasa de inscripción ya se pagó!"
    },
    "l-msg-inscricao-sem-cartao": {
        "pt": "A inscrição não pode ser paga através de cartão!",
		"en": "The registration cannot be paid with card!",
		"es": "¡La inscripción no se puede pagar por medio de la tarjeta!"
    },
    "l-msg-inscricao-nao-possui-taxa": {
        "pt": "Inscrição não possui taxa!",
		"en": "Registration does not have fee!",
		"es": "¡Esta inscripción no tiene tasa!"
    },
    "l-atividades-agendadas": {
        "pt": "Atividades agendadas",
		"en": "Scheduled activities",
		"es": "Actividades programadas en agenda"
    },
    "l-local-prova": {
        "pt": "Local da prova",
		"en": "Test location",
		"es": "Lugar de la prueba"
    },
    "l-local-endereco": {
        "pt": "Endereço: ",
		"en": "Address: ",
		"es": "Dirección: "
    },
    "l-local-predio": {
        "pt": "Local: ",
		"en": "Location: ",
		"es": "Lugar: "
    },
    "l-local-horario": {
        "pt": "Data/Horário: ",
		"en": "Date/Schedule: ",
		"es": "Fecha/Horario: "
    },
    "l-novoDependente": {
        "pt": "Novo dependente",
		"en": "New dependent",
		"es": "Nuevo dependiente"
    },
    "l-btn-fechar": {
        "pt": "Fechar",
        "en": "Close",
        "es": "Cerrar"
    },
    "l-situacao-matricula":{
        "pt": "Situação atual",
        "en": "Current status",
        "es": "Situación actual"
    },    
    "l-etapa-realizada":{
        "pt": "Etapa realizada",
		"en": "Phase executed",
		"es": "Etapa realizada"
    },
    "l-pontuacao":{
        "pt": "Pontuação",
		"en": "Score",
		"es": "Puntaje"
    },
    "l-pontuacao-geral":{
        "pt": "Pontuação geral",
		"en": "General Score",
		"es": "Puntaje general"
    },
    "l-classificacao":{
        "pt": "Classificação",
		"en": "Classification",
		"es": "Clasificación"
    },
    "l-classificacao-geral":{
        "pt": "Classificação geral",
		"en": "General classification",
		"es": "Clasificación general"
    },    
    "l-numero-vagas":{
        "pt": "Nº de vagas",
		"en": "Number of vacancies",
		"es": "Nº de vacantes"
    },
    "l-inscrito-em": {
        "pt": "Inscrito em",
		"en": "Registered in",
		"es": "Inscrito el"
    },
    "l-areas-opcionais": {
        "pt": "Áreas opcionais",
		"en": "Optional areas",
		"es": "Áreas opcionales"
    },
    "l-data-cancelamento": {
        "pt": "Cancelado em",
		"en": "Canceled in",
		"es": "Anulado el"
    },
    "l-motivo-cancelamento": {
        "pt": "Motivo do cancelamento",
		"en": "Reason of cancellation",
		"es": "Motivo de la anulación"
    },
	"l-nova-inscricao": {
        "pt": "Nova inscrição",
		"en": "New registration",
		"es": "Nueva inscripción"
    },
	"l-documentosexigidos": {
        "pt": "Documentos/Arquivos solicitados",
		"en": "Requested documents/files",
		"es": "Documentos/Archivos solicitados"
    },
	"l-excluir": {
        "pt": "Excluir arquivo enviado",
		"en": "Delete uploaded file",
		"es": "Eliminar archivo enviado"
    },
	"l-download": {
        "pt": "Fazer download de arquivo",
		"en": "Download file",
		"es": "Descargar archivo"
    },
    "l-arq-enviados": {
        "pt": "Arquivos enviados",
		"en": "Uploaded Files",
		"es": "Archivos enviados"
    },
    "l-arq-pendentes": {
        "pt": "Arquivos pendentes/não validados",
		"en": "Pending files/not validated",
		"es": "Archivos pendientes/no validados"
    },
    "l-btn-save-files": {
        "pt": "Enviar arquivos selecionados",
		"en": "Send selected files",
		"es": "Enviar archivos seleccionados" 
    },
    "l-upload-success": {
        "pt": "Arquivos enviados com sucesso",
		"en": "Uploaded Files successfully",
		"es": "Archivos enviados con éxito"
    },
    "l-upload-error": {
        "pt": "Erro ao salvar arquivos",
		"en": "Error saving files",
		"es": "Error al guardar archivos"
    },
    "l-edups-upload-file": {
        "pt": "Envio de arquivos",
		"en": "Upload Files",
		"es": "Envío de archivos"
    },
    "l-edups-no-file": {
        "pt": "Não foi selecionado nenhum arquivo",
		"en": "No file selected",
		"es": "No se ha seleccionado ningún archivo"
    },
    "l-warning-matricula-pendente": {
        "pt": "Matrícula em processo de analise para posterior confirmação.",
        "en": "Enrollment in process of analysis for later confirmation.",
        "es": "Matrícula en proceso de análisis para posterior confirmación." 
    },
    "l-matricula":{
        "pt": "Matrícula",
        "en": "Enrollment",
        "es": "Matrícula"
    },
    "l-matric-pending": {
        "pt": "Matrícula aguardando pagamento para efetivação.",
        "en": "Matriculation awaiting payment to effect.",
        "es": "Matrícula aguardando pago para efectivización."
    }    
}]
