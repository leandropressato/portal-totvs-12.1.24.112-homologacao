/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsCentralCandidatoModule
 * @object routeConfig
 *
 * @created 20/04/2017 v12.1.17
 * @updated
 *
 * @dependencies $stateProvider
 *
 * @description Rotas da funcionalidade Informações da Central do Candidato
 */
define(['centralcandidato/centralcandidato.module', 'utils/edups-utils.globais'], function() {
    'use strict'

    angular
        .module('edupsCentralCandidatoModule')
        .config(edupsCentralCandidatoRouteConfig)

    edupsCentralCandidatoRouteConfig.$inject = ['$stateProvider']

    function edupsCentralCandidatoRouteConfig($stateProvider) {
        $stateProvider.state('centralcandidato', {
            abstract: true,
            template: '<ui-view/>'
        }).state('centralcandidato.start', {

            url: '/' + document.eduPSNivelEnsino + '/centralcandidato?{action:string}&{token:string}&{c:string}&{ps:string}&{insc:string}',
            controller: 'edupsCentralCandidato' + document.eduPSNivelEnsino.toUpperCase() + 'Controller',
            controllerAs: 'controller',
            params: {
                action: '',
                token: ''
            },
            templateUrl: function() {

                var utilGlobais = requirejs('utils/edups-utils.globais');
                var baseTemplate = 'js/templates/centralcandidato-';

                if (utilGlobais.validaViewCustomizada('centralcandidato.start')) {
                    baseTemplate = 'js/templates/custom/centralcandidato-';
                }

                return baseTemplate + document.eduPSNivelEnsino + '-list.view.html';
            },
            resolve: {
                lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsCentralCandidatoModule',
                        files: ['js/centralcandidato/centralcandidato' + document.eduPSNivelEnsino.toUpperCase() + '.controller.js']
                    }]);
                }]
            }
        })
    }
})
