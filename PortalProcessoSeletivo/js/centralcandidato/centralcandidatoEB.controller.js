/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsCentralCandidatoModule
 * @name edupsCentralCandidatoEBController
 * @object controller
 *
 * @created 20/04/2017 v12.1.17
 * @updated 05/12/2018 v12.1.23
 *
 * @requires centralcandidato.module
 *
 * @dependencies edupsCentralCandidatoFactory
 *
 * @description Controller da funcionalidade Informações da Central do Candidato do Ensino Básico
 */
define(['centralcandidato/centralcandidato.module',
    'centralcandidato/centralcandidato.service',
    'financeiro/pagamento-cartao/financeiro-pagcartao.controller',
    'utils/edups-utils.service',
    'login/login.factory'
], function() {
    'use strict'

    angular
        .module('edupsCentralCandidatoModule')
        .controller('edupsCentralCandidatoEBController', edupsCentralCandidatoEBController)

    edupsCentralCandidatoEBController.$inject = [
        '$rootScope',
        '$scope',
        '$state',
        '$modal',
        '$window',
        '$sce',
        '$timeout',
		'$compile',
        'totvs.app-notification.Service',
        'i18nFilter',
        'edupsLoginFactory',
        'edupsCentralCandidatoService'
    ]

    function edupsCentralCandidatoEBController(
        $rootScope,
        $scope,
        $state,
        $modal,
        $window,
        $sce,
        $timeout,
        $compile,
        totvsNotification,
        i18nFilter,
        edupsLoginFactory,
        edupsCentralCandidatoService
    ) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.alteraInscricao = alteraInscricao;
        self.alteraInscricaoPopUp = alteraInscricaoPopUp;
        self.pagamentoBoleto = pagamentoBoleto;
        self.pagamentoBoletoEdu = pagamentoBoletoEdu;
        self.tratarAction = tratarAction;
        self.pagamentoCartaoCredito = pagamentoCartaoCredito;
        self.pagamentoCartaoCreditoEdu = pagamentoCartaoCreditoEdu;
        self.pagamentoBoleto = pagamentoBoleto;
        self.validaInscricaoSelecionada = validaInscricaoSelecionada;
        self.comprovante = comprovante;
        self.realizarLogout = realizarLogout;
        self.novoDependente = novoDependente;
        self.alterarSenha = alterarSenha;
        self.abrirModalAlterarSenha = abrirModalAlterarSenha;
        self.abrirModalResultado = abrirModalResultado;
        self.alteraDependente = alteraDependente;
        self.alteraInscricao = alteraInscricao;
        self.editarDadosCandidato = editarDadosCandidato;
        self.editarDadosResponsavel = editarDadosResponsavel;
        self.novaInscricaoDependenteSelecionado = novaInscricaoDependenteSelecionado;
        self.novaInscricaoCandidato = novaInscricaoCandidato;
        self.intervaloResultadoValido = intervaloResultadoValido;
        self.intervaloSelecaoValido = intervaloSelecaoValido;
        self.NomeUsuario = '';
        self.InfoInscricao = {};
        self.Inscricoes = [];
        self.CandidatosDependentes = [];
        self.OpcoesCurso = [];
        self.OpcoesIdioma = [];
        self.LocalProvaEtapas = [];
        self.HorariosEtapas = [];
        self.AtividadesAgendadas = [];
        self.StatusCadastro = [];
        self.StatusPagamento = [];
        self.NumeroInscricao = 0;
        self.IdAreaInteresseAtual = 0;
        self.QuantBoletos = 0;
        self.CodUsuarioPS = 0;
        self.IdUsuario = 0;
        self.IsResponsavel = true;
        self.ResultadoAreaInteresse = [];
        self.ResultadoAreaOpcionais = [];

        self.HasSolicitacaoDoc = false;
        self.HasPendenciaDoc = false;
        self.listaDocumentoExigido = [];
        self.dadosDocumentosExigidos = [];
        self.buscarDocumentosExigidos = buscarDocumentosExigidos;
        self.saveSelectedFiles = saveSelectedFiles;
        $scope.downloadFile = downloadFile;
        self.getInfoAlunoEdu = getInfoAlunoEdu;
        self.dadosAluno = {};

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        // Só executa o método init após carregar o objeto com os parâmetros
        var myWatch = $scope.$watch('objParametros', function(data) {
            if (edupsCentralCandidatoService.isDefinedNotNull(data) &&
                edupsCentralCandidatoService.isDefinedNotNull(data.NomePortalInscricoes)) {
                $timeout(function() {
                    init();
                });

                myWatch();
            }
        });

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            buscarNomeUsuario();

            if ($rootScope.objParametros.ResponsavelCandidato) {
                buscarCandidatosDependentes(buscarInscricoesDependente);        
            } else {
                buscarInscricoesCandidato();
            }

            /**
             * Evento necessário, pois ao redirecinar para outro escopo
             * o overlay do modal de inscrições continuava aberto.
             * @param {event} '$destroy' Evento ao sair do escopo da central de canidato
             */
            $scope.$on('$destroy', function() {
                $('#modalPopUpInscricoes').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            });
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************
        function buscarNomeUsuario() {
            edupsCentralCandidatoService.buscarNomeUsuario(function(result) {
                self.NomeUsuario = result;
            });
        }

        function novoDependente() {
            edupsCentralCandidatoService.novoDependente();
        }

        function novaInscricaoDependenteSelecionado() {
            edupsCentralCandidatoService.novaInscricaoDependente(self.IdUsuario);
        }

        function pagamentoBoleto() {
            edupsCentralCandidatoService.tratarAction('financeiro.boleto', self.NumeroInscricao);
        }

        function pagamentoBoletoEdu() {
            edupsCentralCandidatoService.tratarAction('financeiro.eduboleto', self.NumeroInscricao);
        }

        function pagamentoCartaoCredito() {
            edupsCentralCandidatoService.tratarAction('financeiro.cartao', self.NumeroInscricao);
        }

        function pagamentoCartaoCreditoEdu() {
            edupsCentralCandidatoService.tratarAction('financeiro.educartao', self.NumeroInscricao);
        }

        function comprovante() {
            edupsCentralCandidatoService.tratarAction('inscricoes.comprovante', self.NumeroInscricao);
        }

        function alteraDependente() {
            buscarInscricoesDependente();
            buscaTodasInformacoes();
        }

        function alteraInscricao() {
            buscaTodasInformacoes();
        }

        function alteraInscricaoPopUp() {
            buscaTodasInformacoes(validaInscricaoSelecionada);
        }

        function validaInscricaoSelecionada() {
            var status = null;

            if (edupsCentralCandidatoService.isDefinedNotNull(self.StatusPagamento[0])) {
                status = self.StatusPagamento[0].STATUS;
            }

            edupsCentralCandidatoService.validaInscricaoSelecionada(self.NumeroInscricao,
                self.QuantBoletos,
                status);

            escondePopUpInscricoes();
        }

        function escondePopUpInscricoes() {
            $('#modalPopUpInscricoes').modal('hide');
        }

        function exibePopUpInscricoes() {
            $('#modalPopUpInscricoes').modal({ backdrop: 'static' });
        }

        function tratarAction(action, inscricao) {
            edupsCentralCandidatoService.tratarAction(action, inscricao);
        }

        /**
         * Realizar logout
         */
        function realizarLogout() {
            edupsCentralCandidatoService.realizarLogout();
        }

        /**
         * Alterar senha
         *
         * @param {any} infoUsuario - objeto com as senhas do usuario
         * @param {any} _valid - $valid do Form
         * @param {any} _error - $error do Form
         */
        function alterarSenha(infoUsuario, _valid, _error) {
            edupsCentralCandidatoService.alterarSenha(infoUsuario, _valid, _error);
        }

        /**
         * Abre o modal para alteração de senha
         */
        function abrirModalAlterarSenha() {
            if ($rootScope.objParametros.UsaSenhaLogin) {
                self.InfoUsuario = {};
                $('#modalAlterarSenha').modal('show');
            }
        }

        /**
         * Envia para a pagina de edição de dados pessoais
         */
        function editarDadosCandidato() {
            edupsCentralCandidatoService.editarDados(false, self.NumeroInscricao);
        }

        function editarDadosResponsavel() {
            edupsCentralCandidatoService.editarDados(true);
        }

        function buscarCandidatosDependentes(callback) {
            edupsCentralCandidatoService.buscarCandidatosDependentes(function(result) {
                self.CandidatosDependentes = result;

                if (result.length > 0) {
                    self.IdUsuario = self.CandidatosDependentes[0].IDUSUARIO;

                    // Parâmetro retornado pelo link enviado por e-mail ao candidato e/ou responsável pela isncrição
                    if ($state.params.insc) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].NUMEROINSCRICAO === parseInt($state.params.insc)) {
                                self.IdUsuario = result[i].IDUSUARIO;
                                break;
                            }
                        }
                    }

                    //Se possuir algum candidato, busca as demais informaçoes.
                    if (angular.isFunction(callback)) {
                        callback();
                    }                    
                } else {
                    buscaTodasInformacoes();
                }
            });
        }

        function buscarInscricoesDependente() {
            edupsCentralCandidatoService.buscarInscricoesDependente(self.IdUsuario, function(result) {
                if ($state.params.action === 'inscricoesWizard') {
                    novoDependente();
                }
                else {
                    self.Inscricoes = result;

                    if (edupsCentralCandidatoService.isDefinedNotNull(self.Inscricoes)) {
                        self.NumeroInscricao = self.Inscricoes[0].NUMEROINSCRICAO;
                        self.IdAreaInteresseAtual = self.Inscricoes[0].IDAREAINTERESSE;
                        buscaTodasInformacoes(validaInscricaoSelecionada);

                        if (self.Inscricoes.length !== 0) {
                            if (self.Inscricoes.length > 1 && $state.params.action !== '' &&
                                $state.params.action !== 'alterar.senha' &&
                                $state.params.action !== 'dadospessoais.start') {
                                exibePopUpInscricoes();
                            }
                        }
                    }
                }
            });
        }

        function buscarInscricoesCandidato() {
            edupsCentralCandidatoService.buscarInscricoesCandidato(self.NumeroInscricao, function(result) {
                self.Inscricoes = result;

                if (edupsCentralCandidatoService.isDefinedNotNull(self.Inscricoes)) {
                    if (self.Inscricoes.length !== 0) {
                        if (self.Inscricoes.length > 1 && $state.params.action !== '' &&
                            $state.params.action !== 'alterar.senha' &&
                            $state.params.action !== 'dadospessoais.start') {
                            exibePopUpInscricoes();
                        } else {
                            // Preenche com a ultima inscrição do candidato
                            self.NumeroInscricao = self.Inscricoes[0].NUMEROINSCRICAO;
                            self.IdAreaInteresseAtual = self.Inscricoes[0].IDAREAINTERESSE;
                            buscaTodasInformacoes(validaInscricaoSelecionada);
                        }
                    }
                }
            })
        }        

        function buscarInformacaoInscricao() {
            edupsCentralCandidatoService.buscarInformacaoInscricao(self.NumeroInscricao, function(result) {
                self.InfoInscricao = result;
            });
        }

        function buscarOpcoesCurso() {
            edupsCentralCandidatoService.buscarOpcoesCurso(self.NumeroInscricao, function(result) {
                self.OpcoesCurso = result;
            });
        }

        function buscarOpcoesIdioma() {
            edupsCentralCandidatoService.buscarOpcoesIdioma(self.NumeroInscricao, function(result) {
                self.OpcoesIdioma = result;
            });
        }

        function buscarLocalProvaEtapas() {
            edupsCentralCandidatoService.buscarLocalProvaEtapas(self.NumeroInscricao, function(result) {
                self.LocalProvaEtapas = result;
            });
        }

        function buscarHorariosEtapas() {
            edupsCentralCandidatoService.buscarHorariosEtapas(self.NumeroInscricao, function(result) {
                self.HorariosEtapas = result;
            });
        }

        function buscarAtividadesAgendadas() {
            edupsCentralCandidatoService.buscarAtividadesAgendadas(self.NumeroInscricao, function(result) {
                self.AtividadesAgendadas = result;
            });
        }

        function buscarStatusCadastro() {
            edupsCentralCandidatoService.buscarStatusCadastro(self.NumeroInscricao, function(result) {
                self.StatusCadastro = result;
            });
        }

        function buscarStatusPagamento(callback) {
            var funcCallback = function(result) {
                self.StatusPagamento = result;
                self.QuantBoletos = result.length;
            }

            edupsCentralCandidatoService.buscarStatusPagamento(self.NumeroInscricao, funcCallback, callback);
        }

        function buscarResultadoAreaInteresse() {
            edupsCentralCandidatoService.buscarResultadoAreaInteresse(self.NumeroInscricao, function(result) {
                self.ResultadoAreaInteresse = result;
            });
        }

        function buscarResultadoAreaOpcionais() {
            edupsCentralCandidatoService.buscarResultadoAreaOpcionais(self.NumeroInscricao, function(result) {
                self.ResultadoAreaOpcionais = result;
            });
        }
        
        function buscaTodasInformacoes(callback) {
            buscarInformacaoInscricao();
            buscarOpcoesCurso();
            buscarOpcoesIdioma();
            buscarLocalProvaEtapas();
            buscarHorariosEtapas();
            buscarAtividadesAgendadas();
            buscarStatusCadastro();
            buscarStatusPagamento(callback);
            buscarResultadoAreaInteresse();
            buscarResultadoAreaOpcionais();

            //Documentos exigidos
            buscarDocumentosExigidos();
            getInfoAlunoEdu();
        }

       /**
        * Abre o modal para apresentar o resultado da seleção
        */
        function abrirModalResultado() {
            $('#modalPopUpResultado').modal('show');            
        }

        /**
         * Está em um período válido de seleção.
         */
        function intervaloSelecaoValido() {            

            return edupsCentralCandidatoService.intervaloSelecaoValido(self.StatusCadastro);
        }

        /**
         * Está em um período válido de resultado.
         */
        function intervaloResultadoValido() {

            return edupsCentralCandidatoService.intervaloResultadoValido(self.StatusCadastro);
        }

        /**
         * Realiza nova inscrição
         */
        function novaInscricaoCandidato() {
            edupsCentralCandidatoService.novaInscricaoCandidato();
        }

        /**
         * Carrega área de documentos exigidos
         */
        function buscarDocumentosExigidos(){
            var pesq = self.Inscricoes.filter(function (x) { return x.NUMEROINSCRICAO === self.NumeroInscricao; });
            
            if (pesq.length > 0){
                self.IdAreaInteresseAtual = pesq[0].IDAREAINTERESSE;
                self.IdPS = pesq[0].IDPS;
            }
            edupsCentralCandidatoService.buscarDocumentosExigidos(self.IdAreaInteresseAtual, self, $scope);
        }

        /**
         * Realiza download do arquivo selecionado
         *
         * @param {any} id - pk do arquivo
         */
        function downloadFile(id){
            edupsCentralCandidatoService.abrirJanelaDownloadArquivo(id);
        }

        /**
         * Salva arquivos selecionados
         */
        function saveSelectedFiles(){
            edupsCentralCandidatoService.saveSelectedFiles(self.IdAreaInteresseAtual, self, $scope, this.dadosDocumentosExigidos); 
        }

        /**
         * Retorna informação de Aluno
         */
        function getInfoAlunoEdu(){
            edupsCentralCandidatoService.getInfoAlunoEdu(self.IdPS, self.NumeroInscricao, function(result){
                if (edupsCentralCandidatoService.isDefinedNotNull(result)){
                    self.dadosAluno = result;
                }
            });
        }
    }
})
