/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsCentralCandidatoModule
 * @name edupsCentralCandidatoFactory
 * @object factory
 *
 * @created 20/04/2017 v12.1.17
 * @updated 05/12/2018 v12.1.23
 *
 * @requires edupsCentralCandidatoModule
 *
 * @description Factory utilizada na funcionalidade Central do Candidato.
 */
define(['centralcandidato/centralcandidato.module'], function() {
    'use strict'

    angular
        .module('edupsCentralCandidatoModule')
        .factory('edupsCentralCandidatoFactory', edupsCentralCandidatoFactory);

    edupsCentralCandidatoFactory.$inject = ['$totvsresource', '$http'];

    function edupsCentralCandidatoFactory($totvsresource, $http) {
        var factory,
            url = EDUPS_CONST_GLOBAL_URL_BASE_SERVICOS;

        factory = $totvsresource.REST(url, {}, {});
        factory.getInformacaoInscricao = getInformacaoInscricao;
        factory.getInscricoesCandidato = getInscricoesCandidato;
        factory.getInscricoesDependente = getInscricoesDependente;
        factory.getOpcoesCurso = getOpcoesCurso;
        factory.getOpcoesIdioma = getOpcoesIdioma;
        factory.getLocalProvaEtapas = getLocalProvaEtapas;
        factory.getHorariosEtapas = getHorariosEtapas;
        factory.getAtividadesAgendadas = getAtividadesAgendadas;
        factory.getStatusCadastro = getStatusCadastro;
        factory.getStatusPagamento = getStatusPagamento;
        factory.getCandidatosDependentes = getCandidatosDependentes;
        factory.getNomeUsuario = getNomeUsuario;
        factory.getResultadoAreaInteresse = getResultadoAreaInteresse;
        factory.getResultadoAreaOpcionais = getResultadoAreaOpcionais;

        factory.getDocumentosExigidos = getDocumentosExigidos;
        factory.getArquivosCandidato = getArquivosCandidato;
        factory.getArquivoDownload = getArquivoDownload;
        factory.postFiles = postFiles;
        factory.getInfoAlunoEdu = getInfoAlunoEdu;

        return factory;

        /**
         * Retorna o nome do usuário
         *
         * @param {callback} callback - Retorna o nome do usuário
         */
        function getNomeUsuario(callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/NomeUsuario';

            return factory.TOTVSGet(parametros, callback);
        }

        /**
         * Retorna as informações de uma determinada inscrição do candidato
         *
         * @param {NumeroInscricao} NumeroInscricao - Informações da Inscrição do candidato
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getInscricoesCandidato(NumeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/InscricoesCandidato';
            parametros.numeroInscricao = NumeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as informações de uma determinada inscrição do candidato
         *
         * @param {IdUsuario} IdUsuario - Id do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getInscricoesDependente(IdUsuario, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/InscricoesDependente';
            parametros.idUsuario = IdUsuario;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna todas as inscrições do candidato em um determinado processo seletivo
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getInformacaoInscricao(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/InformacaoInscricao';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de curso selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getOpcoesCurso(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/OpcoesCurso';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de idiomas selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getOpcoesIdioma(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/OpcoesIdioma';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de idiomas selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getLocalProvaEtapas(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/LocalProvaEtapas';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de idiomas selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getHorariosEtapas(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/HorariosEtapas';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as atividades agendadas do candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getAtividadesAgendadas(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/AtividadesAgendadas';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de idiomas selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getStatusCadastro(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/StatusCadastro';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de idiomas selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getStatusPagamento(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/StatusPagamento';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna as opções de idiomas selecionadas pelo candidato em uma determinada inscrição
         *
         * @param {callback} callback - Informações da Inscrição do candidato
         */
        function getCandidatosDependentes(callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/CandidatosDependentes';

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna o resultado do processo seletivo para um candidato em uma área de interesse.
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações do resultado do candidato em uma área de interesse
         */
        function getResultadoAreaInteresse(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/ResultadoAreaInteresse';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna o resultado de áreas opcionais em um processo seletivo para um candidato.
         *
         * @param {numeroInscricao} numeroInscricao - Número de Inscrição do candidato no Processo Seletivo
         * @param {callback} callback - Informações do resultado do candidato em uma área de interesse
         */
        function getResultadoAreaOpcionais(numeroInscricao, callback) {
            var parametros = {};
            parametros.method = 'CentralCandidato/v1/ResultadoAreaOpcionais';
            parametros.numeroInscricao = numeroInscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

         /**
         * Retorna os arquivos solicitados na área ofertada (obrigatórios ou não).
         *
         * @param {codcoligada} codcoligada - código da coligada
         * @param {idps} idps - Id. do processo seletivo
         * @param {idAreaOfertada} idAreaOfertada - Id. da área ofertada
         * @param {idinscricao} idinscricao - Id. inscrição
          * @param {callback} callback - Informações dos arquivos localizados
         */
        function getDocumentosExigidos(codcoligada, idps, idAreaOfertada, idinscricao, callback) {
            let complurl = 'CentralCandidato/v1/SelectionProcesses';
            var parametros = {};
            parametros.method = complurl;
            parametros.id = codcoligada + '|' + idps + '|' + idAreaOfertada + '|' +  idinscricao;
            parametros.query = 'expand=RequiredDocument';

            return factory.TOTVSQuery(parametros, callback);
        }

         /**
         * Retorna os arquivos que o candidato fez upload.
         *
         * @param {codcoligada} codcoligada - código da coligada
         * @param {idps} idps - Id. do processo seletivo
         * @param {idAreaOfertada} idAreaOfertada - Id. da área ofertada
         * @param {idinscricao} idinscricao - Id. da inscrição
         * @param {callback} callback - Informações dos arquivos localizados
         */
        function getArquivosCandidato(codcoligada, idps, idAreaOfertada, idinscricao, callback) {
            let complurl = 'CentralCandidato/v1/ApplicantRegistries';
            var parametros = {};
            parametros.method = complurl;
            parametros.id = codcoligada + '|' + idps + '|' + idAreaOfertada;
            parametros.query = 'inscriptionNumber=' + idinscricao;

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Retorna um arquivo para download.
         *
         * @param {id} id - Identificador do arquivo
         * @param {callback} callback - Informações do arquivo para download
         */
        function getArquivoDownload(id, callback) {
            let complurl = 'CentralCandidato/v1/ApplicantFiles';
            var parametros = {};
            parametros.method = complurl;
            parametros.id = id;
            parametros.query = '';

            return factory.TOTVSQuery(parametros, callback);
        }

        /**
         * Faz um upload de arquivo.
         *
         * @param {model} model -Model com os dados do arquivo
         * @param {callback} callback - Informações do upload
         */
        function postFiles(model, callback) {
            let complurl = 'CentralCandidato/v1/ApplicantFilesUpload';
            var parametros = {};
            parametros.method = complurl;

            return factory.TOTVSPost(parametros, model, callback);
        }

        /**
         * @public
         * @function Retorna informações do Aluno matriculado no Educacional
         * @name getInfoAlunoEdu
         * @param   {int}    codColigada     Código da Coligada
         * @param   {int}    idPS            Identificador do Processo Seletivo
         * @param   {int}    numeroInscricao Número da Inscrição
         * @callback Objeto 
         * @returns {object} Objeto com os dados do aluno
         */
        function getInfoAlunoEdu(codColigada, idPS, numeroInscricao, callback) {
            let complurl = 'CentralCandidato/v1/InfoAlunoEducacional';
            var parametros = {};
            parametros.codColigada = codColigada;
            parametros.idPS = idPS;
            parametros.numeroInscricao = numeroInscricao;
            parametros.method = complurl;

            return factory.TOTVSQuery(parametros, callback);
        }
    }
})