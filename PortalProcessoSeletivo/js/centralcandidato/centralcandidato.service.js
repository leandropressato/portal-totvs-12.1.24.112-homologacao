/**
 * @license TOTVS | Portal Processo Seletivo v12.1.17
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsCentralCandidatoModule
 * @name edupsCentralCandidatoService
 * @object Service
 *
 * @created 20/04/2017 v12.1.17
 * @updated
 *
 * @requires centralcandidato.module
 *
 * @dependencies edupsCentralCandidatoFactory
 *
 * @description Service da funcionalidade Informações da Central do Candidato
 */
define(['centralcandidato/centralcandidato.module',
    'centralcandidato/centralcandidato.factory',
    'financeiro/pagamento-cartao/financeiro-pagcartao.controller',
    'utils/edups-utils.service',
    'login/login.factory',
	'inscricoes/inscricoes.service'
], function() {
    'use strict'

    angular
        .module('edupsCentralCandidatoModule')
        .service('edupsCentralCandidatoService', edupsCentralCandidatoService)

    edupsCentralCandidatoService.$inject = [
        '$rootScope',
        '$state',
        '$modal',
        '$window',
        '$sce',
        'edupsCentralCandidatoFactory',
        'totvs.app-notification.Service',
        'i18nFilter',
        'edupsLoginFactory',
        '$compile',
        'EdupsInscricoesService'
    ]

    function edupsCentralCandidatoService(
        $rootScope,
        $state,
        $modal,
        $window,
        $sce,
        edupsCentralCandidatoFactory,
        totvsNotification,
        i18nFilter,
        edupsLoginFactory,
        $compile,
		EdupsInscricoesService
    ) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.isDefinedNotNull = isDefinedNotNull;
        self.pagamentoBoleto = pagamentoBoleto;
        self.pagamentoCartaoCredito = pagamentoCartaoCredito;
        self.buscarInscricoesCandidato = buscarInscricoesCandidato;
        self.buscarInscricoesDependente = buscarInscricoesDependente;
        self.buscarInformacaoInscricao = buscarInformacaoInscricao;
        self.buscarOpcoesCurso = buscarOpcoesCurso;
        self.buscarOpcoesIdioma = buscarOpcoesIdioma;
        self.buscarLocalProvaEtapas = buscarLocalProvaEtapas;
        self.buscarHorariosEtapas = buscarHorariosEtapas;
        self.buscarAtividadesAgendadas = buscarAtividadesAgendadas;
        self.buscarStatusCadastro = buscarStatusCadastro;
        self.buscarStatusPagamento = buscarStatusPagamento;
        self.buscarCandidatosDependentes = buscarCandidatosDependentes;
        self.buscarResultadoAreaInteresse = buscarResultadoAreaInteresse;
        self.buscarResultadoAreaOpcionais = buscarResultadoAreaOpcionais;        
        self.buscarNomeUsuario = buscarNomeUsuario;
        self.novoDependente = novoDependente;
        self.validaInscricaoSelecionada = validaInscricaoSelecionada;
        self.tratarAction = tratarAction;
        self.novaInscricaoCandidato = novaInscricaoCandidato;
        self.novaInscricaoDependente = novaInscricaoDependente;
        self.intervaloSelecaoValido = intervaloSelecaoValido;
        self.intervaloResultadoValido = intervaloResultadoValido;

        self.comprovante = comprovante;
        self.realizarLogout = realizarLogout;
        self.alterarSenha = alterarSenha;
        self.abrirModalAlterarSenha = abrirModalAlterarSenha;
        self.editarDados = editarDados;

        self.abrirJanelaDownloadArquivo = abrirJanelaDownloadArquivo;
        self.postFiles = postFiles;
        self.getTemplateFileExists = getTemplateFileExists;
        self.getTemplateFile = getTemplateFile;
        self.buscarDocumentosExigidos = buscarDocumentosExigidos;
        self.saveSelectedFiles = saveSelectedFiles;
        self.notifyNoFile = notifyNoFile;
        self.getInfoAlunoEdu = getInfoAlunoEdu;

        // *********************************************************************************
        // *** Service Initialize
        // *********************************************************************************

        function init() {
            /**
             * Evento necessário, pois ao redirecinar para outro escopo
             * o overlay do modal de inscrições continuava aberto.
             * @param {event}    '$destroy' Evento ao sair do escopo da central de canidato
             */
            $rootScope.$on('$destroy', function() {
                $('#modalPopUpInscricoes').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            })
        }

        /**
         * Verifica se o objeto isDefined e diferente de nulo
         * @param {any} objeto - Objeto de parâmetro
         * @returns
         */
        function isDefinedNotNull(objeto) {
            return (angular.isDefined(objeto) && objeto !== null);
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************
        // Realiza a inscrição para um NOVO dependente.
        function novoDependente() {
            var params = {
                nivelEnsino: $rootScope.nivelEnsino,
                action: $state.params.action,
                objInscricao: {},
                ps: $rootScope.IdPS,
                inscricaoAposLogin: true,
                novoDependente: true
            };

            $state.go('inscricoesWizard.dados-basicos', params);
        }

        // Realiza uma NOVA inscrição para um candidato existente.
        function novaInscricaoCandidato() {
            var params = {
                nivelEnsino: $rootScope.nivelEnsino,
                action: $state.params.action,
                objInscricao: {},
                ps: $rootScope.IdPS,
                inscricaoAposLogin: true,
                novoDependente: false
            };

			EdupsInscricoesService.notificaInteracaoEvento('4'); //4a etapa do Rubeus

            $state.go('inscricoesWizard.dados-basicos', params);
        }

        // Realiza uma NOVA inscrição para um dependente existente.
        function novaInscricaoDependente(idUsuario) {
            var params = {
                nivelEnsino: $rootScope.nivelEnsino,
                action: $state.params.action,
                objInscricao: {},
                ps: $rootScope.IdPS,
                inscricaoAposLogin: true,
                novoDependente: false,
                codUsuarioPSDependente: idUsuario
            };

            $state.go('inscricoesWizard.dados-basicos', params);
        }

        function pagamentoBoleto() {
            tratarAction('financeiro.boleto', self.NumeroInscricao);
        }

        function pagamentoCartaoCredito() {
            tratarAction('financeiro.cartao', self.NumeroInscricao);
        }

        function comprovante() {
            tratarAction('inscricoes.comprovante', self.NumeroInscricao);
        }

        function validaInscricaoSelecionada(numeroInscricao, quantBoletos, statusBoleto) {

            // Inscrição possui taxa
            if (quantBoletos > 0 && ($state.params.action && $state.params.action !== '')) {
                // Boleto já foi baixado (0-Em aberto; 1-baixado; 2-cancelado; 4-baixado parcialmente)
                if (statusBoleto == 1) {
                    totvsNotification.notify({
                        type: 'error',
                        title: i18nFilter('l-atencao', [], 'js/centralcandidato'),
                        detail: i18nFilter('l-msg-inscricao-paga', [], 'js/centralcandidato')
                    });
                } else {
                    if ($state.params.action == 'financeiro.cartao' && !$rootScope.objParametros.PagamentoCartao) {
                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-atencao', [], 'js/centralcandidato'),
                            detail: i18nFilter('l-msg-inscricao-sem-cartao', [], 'js/centralcandidato')
                        });
                    } else {
                        tratarAction($state.params.action, numeroInscricao);
                    }
                }
            }
            else if (($state.params.action && $state.params.action !== '') 
                      && ($state.params.action === 'financeiro.eduboleto' || $state.params.action === 'financeiro.educartao'))
            {
                tratarAction($state.params.action, numeroInscricao);
            } 
            else {
                if ($state.params.action === 'inscricoes.comprovante' ||
                    $state.params.action === 'alterar.senha' ||
                    $state.params.action === 'dadospessoais.start') {
                    tratarAction($state.params.action, numeroInscricao);
                } else if ($state.params.action === 'financeiro.boleto' || $state.params.action === 'financeiro.cartao') {
                    totvsNotification.notify({
                        type: 'error',
                        title: i18nFilter('l-atencao', [], 'js/centralcandidato'),
                        detail: i18nFilter('l-msg-inscricao-nao-possui-taxa', [], 'js/centralcandidato')
                    });
                }
            }
        }

        function buscarNomeUsuario(callback) {
            edupsCentralCandidatoFactory.getNomeUsuario(function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result.NomeUsuario);
                    }
                }
            });
        }

        function buscarInscricoesCandidato(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getInscricoesCandidato(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarInscricoesDependente(idUsuario, callback) {
            edupsCentralCandidatoFactory.getInscricoesDependente(idUsuario, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarInformacaoInscricao(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getInformacaoInscricao(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result[0]);
                    }
                }
            });
        }

        function buscarOpcoesCurso(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getOpcoesCurso(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarOpcoesIdioma(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getOpcoesIdioma(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarLocalProvaEtapas(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getLocalProvaEtapas(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarHorariosEtapas(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getHorariosEtapas(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarAtividadesAgendadas(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getAtividadesAgendadas(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarStatusCadastro(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getStatusCadastro(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarStatusPagamento(numeroInscricao, callback, callbackValidacao) {
            edupsCentralCandidatoFactory.getStatusPagamento(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }

                if (angular.isFunction(callbackValidacao)) {
                    callbackValidacao();
                }
            });
        }

        function buscarCandidatosDependentes(callback) {
            edupsCentralCandidatoFactory.getCandidatosDependentes(function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function buscarResultadoAreaInteresse(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getResultadoAreaInteresse(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }
                 
        function buscarResultadoAreaOpcionais(numeroInscricao, callback) {
            edupsCentralCandidatoFactory.getResultadoAreaOpcionais(numeroInscricao, function(result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function tratarAction(action, inscricao) {
            if (action !== '') {
                var nomeJanela = 'meuPopUp'

                switch (action) {
                    case 'inscricoes.comprovante':
                    case 'financeiro.boleto':
                    case 'financeiro.cartao':
                        var url = $state.href(action, { nivelEnsino: $rootScope.nivelEnsino, numeroInscricao: inscricao, ps: $rootScope.IdPS }),
                            janela = window.open(url, nomeJanela);
                        break;
                    case 'dadospessoais.start':
                        $state.go(action, { nivelEnsino: $rootScope.nivelEnsino, numeroInscricao: inscricao });
                        break;
                    case 'alterar.senha':
                        abrirModalAlterarSenha();
                        break;
                    case 'financeiro.eduboleto':
                    case 'financeiro.educartao':
                        var url = $state.href(action, { nivelEnsino: $rootScope.nivelEnsino, numeroInscricao: inscricao, ofertaOnline: 1 }),
                            janela = window.open(url, nomeJanela);
                        break;
                }
            }
        }

        /**
         * Realizar logout
         */
        function realizarLogout() {
            edupsLoginFactory.realizarLogout(function() {
                totvsNotification.notify({
                    type: 'success',
                    title: i18nFilter('l-titulo', [], 'js/centralcandidato'),
                    detail: i18nFilter('l-logout-success', [], 'js/global-app')
                });

                $state.go('login.manual', { nivelEnsino: $rootScope.nivelEnsino});
            });
        }

        /**
         * Alterar senha
         *
         * @param {any} infoUsuario - Informações do usuário vindo do formulário
         * @param {any} _valid - Informa se o formulário é válido ou não
         * @param {any} _error - Erro retornado pelo formulário
         */
        function alterarSenha(infoUsuario, _valid, _error) {
            if (validarCamposAlterarSenha(infoUsuario, _valid, _error)) {
                edupsLoginFactory.alterarSenha(infoUsuario.SENHAATUAL, infoUsuario.NOVASENHA, infoUsuario.CONFIRMANOVASENHA,
                    function(result) {
                        totvsNotification.notify({
                            type: result.type,
                            title: i18nFilter('l-titulo', [], 'js/centralcandidato'),
                            detail: result.detail
                        });

                        if (result.type === 'success') {
                            self.InfoUsuario = {};
                            $('#modalAlterarSenha').modal('hide');
                        }
                    });
            }
        }

        /**
         * Abre o modal para alteração de senha
         */
        function abrirModalAlterarSenha() {
            if ($rootScope.objParametros.UsaSenhaLogin) {
                self.InfoUsuario = {};
                $('#modalAlterarSenha').modal('show');
            }
        }

        /**
         *  Envia para a pagina de edição de dados pessoais
         *
         * @param {any} numeroInscricao - Informações da Inscrição do candidato
         */
        function editarDados(isResponsavel, numeroInscricao) {
            if ($rootScope.objParametros.UsaSenhaLogin) {
                var params = { isResponsavel: isResponsavel,
							   numeroInscricao: numeroInscricao,
							   nivelEnsino: $rootScope.nivelEnsino
                             };

                $state.go('dadospessoais.start', params);
            } else {
                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-editar-dados', [], 'js/centralcandidato'),
                    detail: i18nFilter('l-opcao-bloqueada', [], 'js/centralcandidato')
                });
            }
        }

        /**
         * Valida campos obrigatórios
         *
         * @param {any} infoUsuario - Informações do usuário vindo do formulário
         * @param {any} _valid - Informa se o formulário é válido ou não
         * @param {any} _error - Erro retornado pelo formulário
         */
        function validarCamposAlterarSenha(infoUsuario, _valid, _error) {
            var listaCampos = '',
                podeSalvar = true,
                novaSenhaValida = true,
                confirmacaoNovaSenhavalida = true;

            if ((!_valid) && (_error.required.length > 0)) {
                for (var l = 0; l < 10; l++) {
                    if ((angular.isDefined(_error.required[l])) &&
                        (_error.required[l].$name !== '') &&
                        (_error.required[l].$name.indexOf('controller_') === -1)) {
                        listaCampos = listaCampos + _error.required[l].$name + ', '
                        podeSalvar = false;
                    }
                }
            } else if (_valid) {
                // valida se a confirmação da nova senha é igual a nova senha
                if (infoUsuario.NOVASENHA !== infoUsuario.CONFIRMANOVASENHA) {
                    confirmacaoNovaSenhavalida = false;
                }
                // valida se a senha atual é diferente da nova senha
                if (infoUsuario.SENHAATUAL === infoUsuario.NOVASENHA) {
                    novaSenhaValida = false;
                }
            }

            if (!podeSalvar && listaCampos) {
                listaCampos = listaCampos.slice(0, -2)
                listaCampos = i18nFilter('l-campos-obrigatorios', [], 'js/centralcandidato') + listaCampos
                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                    detail: listaCampos
                });
            } else if (!confirmacaoNovaSenhavalida) {
                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                    detail: i18nFilter('l-senha-diferente', [], 'js/centralcandidato')
                });
            } else if (!novaSenhaValida) {
                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-atencao', [], 'js/inscricoes'),
                    detail: i18nFilter('l-senha-nova-igual-atual', [], 'js/centralcandidato')
                });
            }

            return podeSalvar && confirmacaoNovaSenhavalida && novaSenhaValida;
        }

        /**
         * Está em um período válido de seleção.
         */
        function intervaloSelecaoValido(StatusCadastro) {

            if ((StatusCadastro.length > 0))
            {
                var dataInicioSelecao = new Date(StatusCadastro[0].DTINISELECAO);
                var hoje = new Date();

                return (hoje >=  dataInicioSelecao);
            }
            else
                return false;
        }

        /**
         * Está em um período válido de resultado.
         */
        function intervaloResultadoValido(StatusCadastro) {

            if ((StatusCadastro.length > 0))
            {
                var dataInicioResultado = new Date(StatusCadastro[0].DTINIRESULTADO);
                var hoje = new Date();

                return (hoje >=  dataInicioResultado);
            }
            else
                return false;
        }

        /**
         * Abre a janela de download do arquivo.
         */
        function abrirJanelaDownloadArquivo(chavePesquisa) {

            edupsCentralCandidatoFactory.getArquivoDownload(chavePesquisa, function(result){
                if (result) {
                    let arquivoBytes = result[0].file;
                    let nomeArquivo = result[0].fileName;
                    var blob = b64toBlob(arquivoBytes);
                    let idx = nomeArquivo.lastIndexOf('.');
                    nomeArquivo = nomeArquivo.substr(0, nomeArquivo.lastIndexOf('¶')) + nomeArquivo.substr(idx, nomeArquivo.length - idx)
                    saveAs(blob, nomeArquivo);
                }
            });
        }

        /**
         * Converte o arquivo para de blob.
         */
        function b64toBlob (b64Data, contentType, sliceSize) {

            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data),
                byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {

                var slice = byteCharacters.slice(offset, offset + sliceSize),
                byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {
                type: contentType
            });
            return blob;
        }

        /**
         * Faz um upload de arquivo.
         */
        function postFiles(model, callback){
            edupsCentralCandidatoFactory.postFiles(model , callback);
        }

        /**
         * Monta um teplante de download File.
         */
        function getTemplateFileExists(item, nomeArquivo, allowDelete, key) {
            let idx = nomeArquivo.lastIndexOf('.');
			var template = '<div class="row"> \
                    <div class="col-md-10 col-lg-10 col-xs-10 col-sm-10"> \
                    <div style="padding: 8px"> \
                    <label class="field-label" style="font-family:\'Arial\'; font-size: 12px; color: gray"; \
                        tooltip="' + item.description + '" tooltip-placement="top">' + item.description + '</label> \
                        <div class="input-group" style="max-width: 500px;"> \
                        <span class="input-group-btn"> \
                                <span class="form-control" style="width: 100%; min-width:120px; position:absolute; display: block; text-overflow: clip; overflow: hidden;" \
                                   tooltip="' + item.description + '">' + nomeArquivo.substr(0, nomeArquivo.lastIndexOf('¶')) + nomeArquivo.substr(idx, nomeArquivo.length - idx) + '</span> ';
               if (allowDelete)
                    template += ' <span class="btn btn-default" tooltip="{{ ::\'l-excluir\' | i18n : [] : \'js/centralcandidato\' }}" ng-click="downloadFile(\'' + key + nomeArquivo + '\');"><span class="glyphicon glyphicon-trash"></span></span> ';

               template += ' <span class="btn btn-default" style="float: right; margin-left:0px;" tooltip="{{ ::\'l-download\' | i18n : [] : \'js/centralcandidato\' }}" ng-click="downloadFile(\'' + key + nomeArquivo + '\');"><span class="glyphicon glyphicon-cloud-download"></span></span> \
                            </span> \
                        </div> \
                    </div> \
                    </div> \
				</div>';

            return template;
        }

        /**
         * Monta um teplante de imput type File.
         */
        function getTemplateFile(item, k, key, nomeArquivo) {
            let idx = isDefinedNotNull(nomeArquivo) ? nomeArquivo.lastIndexOf('.') : 0;
			var template = '<div class="row"> \
					<div class="col-md-10 col-lg-10 col-xs-10 col-sm-10"> \
						<edups-upload-file \
										name="documento_' + item.fileCode + '" \
										canclear="true"' +
										(item.required === 'T' ? ' required="true"' : '') +
										($rootScope.objParametros.ExtensaoArquivo !== 'undefined' && $rootScope.objParametros.ExtensaoArquivo.length > 0 ? ' accept="' + $rootScope.objParametros.ExtensaoArquivo + '"' : '') +
										($rootScope.objParametros.TamanhoMaximoArquivo !== 'undefined' && $rootScope.objParametros.TamanhoMaximoArquivo > 0 ? ' maxkbytes="' + $rootScope.objParametros.TamanhoMaximoArquivo + '"' : '') +
                                        ' label="' + item.description + '" ' +
                                        (isDefinedNotNull(nomeArquivo) ? ' keydownload="' + key + nomeArquivo + '" filename="' + nomeArquivo.substr(0, nomeArquivo.lastIndexOf('¶')) + nomeArquivo.substr(idx, nomeArquivo.length - idx) + '"' : '') +
                                        ' model="controller.dadosDocumentosExigidos[' + k + ']" \> \
						</edups-upload-file> \
					</div> \
				</div>';

            return template;
        }

        /**
         * Monta os componentes de arquivos da tela.
         */
        function buscarDocumentosExigidos(IdAreaInteresseAtual, _self, _scope){
            if (!isDefinedNotNull(IdAreaInteresseAtual) || IdAreaInteresseAtual < 1)
                return;
            var inscriptionNumber = _self.NumeroInscricao;
            var arquivosCandidato = [];
            _self.HasPendenciaDoc = false;
            _self.HasSolicitacaoDoc = false;

            var containerPre = document.getElementById('InformacaoDocumentos');
            angular.element(containerPre).empty();
            edupsCentralCandidatoFactory.getDocumentosExigidos($rootScope.CodColigada, _self.IdPS, IdAreaInteresseAtual, _self.NumeroInscricao, function(result) {
                
                if (isDefinedNotNull(result) && result.length > 0){
                    edupsCentralCandidatoFactory.getArquivosCandidato($rootScope.CodColigada, _self.IdPS, IdAreaInteresseAtual, inscriptionNumber, function(resultFile) {
                        if (isDefinedNotNull(resultFile) && resultFile.length > 0){
                            arquivosCandidato = resultFile;
                        }
                        var containerDiv = document.getElementById('InformacaoDocumentos');
                        let exibe = false;
                        let hasFiles = false;
                        let hasRequest = false;
                        let contDoc = 0;
                        let key = $rootScope.CodColigada + '|' + _self.IdPS + '|';
                        _self.listaDocumentoExigido = [];
                        _self.dadosDocumentosExigidos = [];
                        var template = '<legend ng-if="true"><h5>{{ ::\'l-arq-pendentes\' | i18n : [] : \'js/centralcandidato\' }}</h5></legend>';
                        var templateFiles = '<legend ng-if="true"><h5>{{ ::\'l-arq-enviados\' | i18n : [] : \'js/centralcandidato\' }}</h5></legend>';

                        for(let x = 0; x < result.length; x++){
                            if (result[x].requiresEnrollment === 'T'){
                                exibe = true;
                                //IE 
                                let descricao = result[x].description;
                                var pesq = arquivosCandidato.filter(function (x) { return x.detail.trim() === descricao.trim(); });
                                let item = result[x];

                                if (pesq.length > 0 && pesq[0].allowupdate !== 'T')
                                {
                                    hasFiles = true;
                                    templateFiles += getTemplateFileExists(item, pesq[0].fileName, false, key + pesq[0].inscriptionNumber + '|');
                                }
                                else{
                                    if (pesq.length > 0 && pesq[0].allowupdate == 'H')
                                        continue;

                                    hasRequest = true;
                                    _self.listaDocumentoExigido.push(item);
                                    _self.dadosDocumentosExigidos.push({
                                        CODDOCUMENTO: item.fileCode,
                                        DESCRICAO: item.description,
                                        NOMEARQUIVO: '',
                                        NOMEORIGINAL: (pesq.length > 0 ? pesq[0].fileName : ''),
                                        ARQUIVO: {}
                                    });
            
                                    let key1 = (pesq.length > 0 ? key + pesq[0].inscriptionNumber + '|' : '');
                                    template += getTemplateFile(result[x], contDoc, key1, (pesq.length > 0 ? pesq[0].fileName : ''));
                                    contDoc++;
                                }
                            }
                        }

                        if (hasFiles){
                            let diretivaTotvs = $compile(templateFiles + '</br>');
                            diretivaTotvs = diretivaTotvs(_scope);
                            angular.element(containerDiv).append(diretivaTotvs);
                        }

                        if (hasRequest){
                            let diretivaTotvs = $compile(template);
                            diretivaTotvs = diretivaTotvs(_scope);
                            angular.element(containerDiv).append(diretivaTotvs);
                            _self.HasPendenciaDoc = true;
                        }

                        _self.HasSolicitacaoDoc = exibe;
                    });    
                        
                }
                else{
                    _self.HasSolicitacaoDoc = false;
                    _self.listaDocumentoExigido = [];
                } 
                
            });
        }

        /**
         * Salva arquivos selecionados
         */
        function saveSelectedFiles(IdAreaInteresseAtual, _self, _scope, dadosDocumentosExigidos){
            if (isDefinedNotNull(dadosDocumentosExigidos) 
                && dadosDocumentosExigidos !== null) {

                var modelJSON = {
                    SPSDOCUMENTOSEXIGIDOS: []
                }
    
                var count = 0;
                angular.forEach(dadosDocumentosExigidos, function(xFile) {
                    
                    for(var f = 0; f < xFile.ARQUIVO.length; f++){
                        if (xFile.NOMEARQUIVO[f].length > 0){
                            var dadosDocumentosExigidosTemp = {};
                            dadosDocumentosExigidosTemp.CODCOLIGADA = $rootScope.CodColigada;
                            dadosDocumentosExigidosTemp.IDPS =  _self.IdPS;
                            dadosDocumentosExigidosTemp.NUMEROINSCRICAO = _self.NumeroInscricao;
                            dadosDocumentosExigidosTemp.DETALHE = xFile.DESCRICAO;
                            dadosDocumentosExigidosTemp.NOMEARQUIVO = xFile.NOMEARQUIVO[f];
                            dadosDocumentosExigidosTemp.ARQUIVO = xFile.ARQUIVO[f];
                            dadosDocumentosExigidosTemp.NOMEORIGINAL = xFile.NOMEORIGINAL;

                            modelJSON.SPSDOCUMENTOSEXIGIDOS.push(dadosDocumentosExigidosTemp);
                            count++;
                        }
                    }
                });

                if (count > 0){
                    postFiles(modelJSON, function(result){
                        if(result && isDefinedNotNull(result.exception)){
                            totvsNotification.notify({
                                type: 'error',
                                title: i18nFilter('l-edups-upload-file', [], 'js/centralcandidato'),
                                detail: i18nFilter('l-upload-error', [], 'js/centralcandidato') + ': ' + result.message
                            });
                        }
                        else if(result){
                            totvsNotification.notify({
                                type: 'success',
                                title: i18nFilter('l-edups-upload-file', [], 'js/centralcandidato'),
                                detail: i18nFilter('l-upload-success', [], 'js/centralcandidato')
                            });

                            buscarDocumentosExigidos(IdAreaInteresseAtual, _self, _scope);
                        }
                        else{
                            let msg = ' Erro ' + arguments[1].status + ': - ' +  arguments[1].data.ExceptionMessage;
                            totvsNotification.notify({
                                type: 'error',
                                title: i18nFilter('l-edups-upload-file', [], 'js/centralcandidato'),
                                detail: i18nFilter('l-upload-error', [], 'js/centralcandidato') + ': ' + msg
                            });
                        }
                    });
                }
                else{
                    notifyNoFile();
                }
            }
            else{
                notifyNoFile();
            }
        }

        /**
         * Exibe notificações.
         */
        function notifyNoFile(){
            totvsNotification.notify({
                type: 'error',
                title: i18nFilter('l-edups-upload-file', [], 'js/centralcandidato'),
                detail: i18nFilter('l-edups-no-file', [], 'js/centralcandidato')
            });
        }

        /**
         * Retorna informações de Aluno
         */
        function getInfoAlunoEdu(IdPS, NumeroInscricao, callback){
            let retorno = {};
            retorno.ofertaOnline = false;
            retorno.showPagamento = false;
            retorno.showPagCartao = false;
            retorno.alunoMatriculado = false;
            retorno.boletoPago = false;
            retorno.statusMatric = '';
            retorno.DataBaixa;
            retorno.Vencimento;

            edupsCentralCandidatoFactory.getInfoAlunoEdu($rootScope.CodColigada, IdPS, NumeroInscricao, function(result) {
                if (isDefinedNotNull(result) && result.length > 0 && result[0].OFERTAONLINE === 'T'){
                    retorno.ofertaOnline =  true;
                    retorno.alunoMatriculado = result[0].MATRICULAPROVISORIA !== 'S';
                    if (result[0].STATUSMATRIC !== null){
                        retorno.statusMatric = result[0].DESCRICAO;
                    }
                    else{
                        retorno.statusMatric = i18nFilter('l-warning-matricula-pendente', [], 'js/centralcandidato');
                        retorno.alunoMatriculado = false;
                    }
                    retorno.showPagamento = result[0].IDBOLETO > 0;
                    if (result[0].STATUSLAN !== 1){
                        retorno.showPagCartao = result[0].PRTPERMITEPAGCARTAO === 'S';
                        retorno.Vencimento = result[0].VENCIMENTO;
                    }
                    else{
                        retorno.boletoPago = true;
                        retorno.DataBaixa = result[0].DATABAIXA;
                    }
                }

                if (angular.isFunction(callback)) {
                    callback(retorno);
                }
            });
        }
    }
})
