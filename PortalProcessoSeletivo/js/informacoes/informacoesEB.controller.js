/**
 * @license TOTVS | Portal Processo Seletivo v12.1.18
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsInformacoesModule
* @name EdupsInformacoesEBController
* @object controller
*
* @created 24/07/2017 v12.1.18
* @updated
*
* @requires informacoes.module
*
* @dependencies EdupsInformacoesService
*
* @description Controller da funcionalidade Informações do Processo Seletivo (ENSINO BÁSICO)
*              Qualquer regra específica do nível de ensino básico, relacionado a funcionalidade
*              de informações, deve ser colocada aqui nesse controller.
*/
define(['informacoes/informacoes.module',
        'informacoes/informacoes.service'], function () {

    'use strict';

    angular
        .module('edupsInformacoesModule')
        .controller('EdupsInformacoesEBController', EdupsInformacoesEBController);

    EdupsInformacoesEBController.$inject = ['EdupsInformacoesService', '$rootScope', '$scope'];

    function EdupsInformacoesEBController(EdupsInformacoesService, $rootScope, $scope) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.listaArquivosProcessoSeletivo = [];
        self.PeriodoInscricao = $rootScope.PeriodoInscricao;
        self.PeriodoResultado = $rootScope.PeriodoResultado;

        self.baixarArquivo = baixarArquivo;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        // Só executa o método init após carregar o objeto com os parâmetros
        var myWatch = $scope.$watch('objParametros', function (data) {
            if (EdupsInformacoesService.isDefinedNotNull(data) && EdupsInformacoesService.isDefinedNotNull(data.NomePortalInscricoes)) {

                self.objParametros = data;

                init();
                myWatch();
            }
        });

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            EdupsInformacoesService.listarArquivosPS($rootScope.CodColigada, $rootScope.IdPS, 1, function (result) {
                self.listaArquivosProcessoSeletivo = result;
            });
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function baixarArquivo(idArquivo, formaArmazenamento, pathArquivo, nomeArquivo) {
            EdupsInformacoesService.baixarArquivo(idArquivo,
                                                  formaArmazenamento,
                                                  pathArquivo,
                                                  nomeArquivo);
        }
    }
});
