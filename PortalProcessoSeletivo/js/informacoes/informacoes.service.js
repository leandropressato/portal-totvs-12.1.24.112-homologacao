/**
 * @license TOTVS | Portal Processo Seletivo v12.1.18
 * (c) 2015-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module edupsInformacoesModule
 * @name EdupsInformacoesService
 * @object service
 *
 * @created 24/07/2017 v12.1.18
 * @updated
 *
 * @requires
 *
 * @description Service da funcionalidade Informações do Processo Seletivo, utilizada por todos os níveis de ensino.
 *              Todos os métodos considerados comuns entre os níveis de ensino devem ser colocados aqui e consumidos
 *              pelos controllers específicos de cada nível de ensino.
 */

define(['informacoes/informacoes.module',
        'utils/edups-utils.factory'], function () {

    'use strict';

    angular
        .module('edupsInformacoesModule')
        .service('EdupsInformacoesService', EdupsInformacoesService);

    EdupsInformacoesService.$inject = ['$rootScope', 'edupsUtilsFactory'];

    function EdupsInformacoesService($rootScope, EdupsUtilsFactory) {

        var self = this;

        self.listarArquivosPS = listarArquivosPS;
        self.baixarArquivo = baixarArquivo;
        self.isDefinedNotNull = isDefinedNotNull;

        function listarArquivosPS(codColigada, idPS, idCategoria, callback) {
            var listaArquivosProcessoSeletivo = [];

            EdupsUtilsFactory.getListaArquivosProcessoSeletivo(codColigada, idPS, idCategoria, function (result) {
                if ((angular.isDefined(result)) && (result.length > 0)) {
                    listaArquivosProcessoSeletivo = result;
                }

                if (typeof callback === 'function') {
                    callback(listaArquivosProcessoSeletivo);
                }
            });
        }

        function baixarArquivo(idArquivo, formaArmazenamento, pathArquivo, nomeArquivo) {
            var link = document.createElement('a');
            link.download = nomeArquivo;
            link.href = EdupsUtilsFactory.getArquivoDownload(idArquivo, formaArmazenamento, pathArquivo);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        /**
         * Verifica se o objeto isDefined e diferente de nulo
         * @param {any} objeto
         * @returns
         */
        function isDefinedNotNull (objeto) {
            return (angular.isDefined(objeto) && objeto !== null);
        }
    }
});
