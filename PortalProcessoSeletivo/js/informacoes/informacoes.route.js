/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsInformacoesModule
* @object routeConfig
*
* @created 14/09/2016 v12.1.15
* @updated
*
* @dependencies $stateProvider
*
* @description Rotas da funcionalidade Informações do Processo Seletivo
*/
define(['informacoes/informacoes.module', 'utils/edups-utils.globais'],function () {

    'use strict';

    angular
        .module('edupsInformacoesModule')
        .config(edupsInformacoesRouteConfig);

    edupsInformacoesRouteConfig.$inject = ['$stateProvider'];

    function edupsInformacoesRouteConfig($stateProvider) {
        $stateProvider.state('informacoes', {
            abstract: true,
            template: '<ui-view/>'

        }).state('informacoes.start', {
            url: '/:nivelEnsino/informacoes',
            controller: 'EdupsInformacoes' + document.eduPSNivelEnsino.toUpperCase() + 'Controller',
            controllerAs: 'controller',
            templateUrl: function () {

                var utilGlobais = requirejs('utils/edups-utils.globais');
                var baseTemplate = 'js/templates/informacoes-';

                if (utilGlobais.validaViewCustomizada('informacoes.start')) {
                    baseTemplate = 'js/templates/custom/informacoes-';
                }

                return baseTemplate + document.eduPSNivelEnsino + '-list.view.html';
            },
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'edupsInformacoesModule',
                        files: ['js/informacoes/informacoes' + document.eduPSNivelEnsino.toUpperCase() + '.controller.js']
                    }]);
                }]
            }
        });
    }
});
