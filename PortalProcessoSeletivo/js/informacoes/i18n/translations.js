[
    {
        "l-Titulo-Arquivos": {
            "pt": "ARQUIVOS",
			"en": "FILES",
			"es": "ARCHIVOS"
        },
        "l-Download-Arquivos":{
            "pt": "Faça o download do(s) arquivo(s) para imprimir",
			"en": "Download the file(s) to print",
			"es": "Haga el download de el(los) archivo(s) para imprimir"
        }
    }
]