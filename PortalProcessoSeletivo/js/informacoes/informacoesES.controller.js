/**
 * @license TOTVS | Portal Processo Seletivo v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module edupsInformacoesModule
* @name EdupsInformacoesESController
* @object controller
*
* @created 14/09/2016 v12.1.15
* @updated
*
* @requires informacoes.module
*
* @dependencies EdupsInformacoesService
*
* @description Controller da funcionalidade Informações do Processo Seletivo (ENSINO SUPERIOR).
*              Qualquer regra específica do nível de ensino superior, relacionado a funcionalidade
*              de informações, deve ser colocada aqui nesse controller.
*/
define(['informacoes/informacoes.module',
        'informacoes/informacoes.service'], function () {

    'use strict';

    angular
        .module('edupsInformacoesModule')
        .controller('EdupsInformacoesESController', EdupsInformacoesESController);

    EdupsInformacoesESController.$inject = ['EdupsInformacoesService', '$rootScope', '$scope'];

    function EdupsInformacoesESController(EdupsInformacoesService, $rootScope, $scope) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.listaArquivosProcessoSeletivo = [];
        self.PeriodoInscricao = $rootScope.PeriodoInscricao;
        self.PeriodoResultado = $rootScope.PeriodoResultado;

        self.baixarArquivo = baixarArquivo;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        // Só executa o método init após carregar o objeto com os parâmetros
        var myWatch = $scope.$watch('objParametros', function (data) {
            if (EdupsInformacoesService.isDefinedNotNull(data) && EdupsInformacoesService.isDefinedNotNull(data.NomePortalInscricoes)) {

                self.objParametros = data;

                init();
                myWatch();
            }
        });

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            EdupsInformacoesService.listarArquivosPS($rootScope.CodColigada, $rootScope.IdPS, 1, function (result) {
                self.listaArquivosProcessoSeletivo = result;
            });
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function baixarArquivo(idArquivo, formaArmazenamento, pathArquivo, nomeArquivo) {
            EdupsInformacoesService.baixarArquivo(idArquivo,
                                                  formaArmazenamento,
                                                  pathArquivo,
                                                  nomeArquivo);
        }
    }
});
