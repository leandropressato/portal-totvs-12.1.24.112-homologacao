/**
 * @license TOTVS | Portal - TOTVS Processo Seletivo v12.1.23
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module edupsDiretivasModule
 * @name edupsUploadFile
 * @object directive
 *
 * @created 08/11/2018 v12.1.23
 * @updated
 *
 * @requires diretivas.module
 *
 * @dependencies i18nFilter
 *
 * @restrict E
 * Parâmetros:
 * {string} label                Texto a ser apresentado no botão
 * {string}  name                Nome para o campo de input tipo file
 * {string}  keydownload         Chave para download
 * {string}  filename            Nome do arquivo de download
 * {boolean} multiple            Se verdadeiro irá deixar selecionar mais de um arquivo para upload
 * {boolean} required            Se verdadeiro campo é obrigatório
 * {boolena} canclear            Habilita botão para limpar campo
 * {boolean} checkmaxfiles       Informa se a quantidade de arquivos requeridos deve ser validada
 * {int}     numfilesreq         Quantidade de arquivos esperados
 * {int}     maxkbytes           Tamanho máximo do arquivo inserido (em bytes)
 * {string}  accept              Formatos permitididos (separados por ;)
 * {string}  nameForm            Nome do formulário pai
 * {object}  model               Equivalente a ng-model
 *
 * @example
 * <edups-upload-file
 *      name="campoFile"
 *      required="true"
 *      model="controller.Arquivos.File"
 *      label="{{ ::'l-enviar-arquivo' | i18n : [] : 'js/inscricao' }}">
 * </edu-upload-file>
 *
 * @description Diretiva para Upload de arquivos (para download de arquivo deve-se eimplementar uma função $scope.downloadFile na controller)
 *
 */
define(['diretivas/diretivas.module'], function () {

    'use strict';

    angular.module('edupsDiretivasModule').directive('edupsUploadFile', EduPsUploadFile);

    EduPsUploadFile.$inject = ['i18nFilter', 'totvs.app-notification.Service'];
    /**
     * Diretiva eduPsUploadFile
     *
     * @param {any} i18nFilter - Filtro de tradução
     * @param {any} totvsNotification - Notificação toast
     * @returns
     */
    function EduPsUploadFile(i18nFilter, totvsNotification) {

        var edupsUploadFileDirective = {
            restrict: 'E',
            scope: {
                label: '@',
                name: '@',
                accept: '@',
                nameform: '@',
                keydownload: '@',
                filename: '=',
                multiple: '=',
                required: '=',
                canclear: '=',
                checkmaxfiles: '=',
                numfilesreq: '=',
                maxkbytes: '=',
                model: '='
                
            },
            bindToController: true,
            template: '<div style="padding: 10px">' +
            '<legend ng-if="true"> \
               <h5>{{label}}</h5> \
             </legend> ' +
             '<label class="field-label" style="font-family:\'Arial\'; font-size: 12px; color: gray"; \
             tooltip="{{label}}" tooltip-placement="top">{{ ::\'l-label-documentos\' | i18n : [] : \'js/diretivas\' }} </label> ' +
 
           ' <div class="input-group" style="max-width: 500px;">' +
            ' <span class="input-group-btn"> ' +
            '  <input ' +
                    'type="file" ' +
                    'name="{{name}}" ' +
                    'edups-file-model="model" ' +
                    'ng-model="model" ' +
                    'class="hidden" /> \
                    <span class="form-control" tooltip="{{label}}" style="width: 100%; min-width:120px; position:absolute; display: block; text-overflow: clip; overflow: hidden;"></span> \
                    <span class="btn btn-default btnDownload" style="float: right; margin-left:0px;" tooltip="{{ ::\'l-download\' | i18n : [] : \'js/diretivas\' }}"><span class="glyphicon glyphicon-cloud-download"></span></span> \
                    <span class="btn btn-default btnClean" style="float: right; margin-left:0px;" tooltip="{{ ::\'l-limpar\' | i18n : [] : \'js/diretivas\' }}"><span class="glyphicon glyphicon-erase"></span></span> \
                    <span class="btn btn-default" style="float: right; margin-left:0px;" onclick="$(this).parent().find(\'input[type=file]\').click();" tooltip="{{ ::\'l-select-file\' | i18n : [] : \'js/diretivas\' }}"><span class="glyphicon glyphicon-file"></span></span> \
                </span>' +
            '</div>' +
            '<ul class="list-unstyled">' +
                '<li ng-if="extensionsPermitted !== null && extensionsPermitted !== undefined">' +
                    '<span class="text-warning">' +
                        '{{::"l-ext-permitidas" | i18n : [] : "js/diretivas"}}{{extensionsPermitted}}</span>' +
                '</li>' +
                '<li ng-if="maxFileSizeKbytes !== null && maxFileSizeKbytes !== undefined">' +
                    '<span class="text-warning">' +
                        '{{::"l-max-file-size" | i18n : [] : "js/diretivas"}}{{maxFileSizeKbytes | number}} KB</span>' +
                '</li>' +
            '</ul>' +
          '</div>',
            link: link
        };

        return edupsUploadFileDirective;

        function link(scope, element) {

            scope.$watch('model', function(value) {

                if ((value.hasOwnProperty('NOMEARQUIVO')) && (value.NOMEARQUIVO.length > 0))
                {
                    let input = element.find('input[type=file]');
                    input.parent().parent().find('.form-control').html(value.name);
                }
              });

            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.replace(new RegExp(search, 'g'), replacement);
            };

            //Monta arquivos aceitos
            if (angular.isDefined(scope.accept) && scope.accept.length > 0) {
                let t =  scope.accept.replaceAll(';', ',').split(',');
                let s = '';
                for(var i = 0; i < t.length; ++i){
                    s += '.' + t[i] + ',';
                }
                s = s.slice(0, -1);
                element.find('input[type=file]').attr('accept', s);
            }

            if (scope.multiple) {
                element.find('input[type=file]').attr('multiple', 'multiple');
            }

            if (scope.required){
                element.find('input[type=file]').attr('required', 'required');
                element.find('label').append('<label class="field-label" style="font-family:\'Arial\'; font-size: 12px; color: red"; tooltip="{{ ::\'l-label-obrigatorio\' | i18n : [] : \'js/diretivas\' }}" tooltip-placement="top">&nbsp&nbsp*</label>');
            }

            if (!scope.canclear){
                element.find('.btnClean').remove();
            }

            if (!scope.keydownload){
                element.find('.btnDownload').remove();
            }
            else{
                let input = element.find('input[type=file]');
                input.parent().parent().find('.form-control').html(scope.filename);
            }

            element.on('change', 'input[type=file]', function () {
            
                var input = angular.element(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

                    var nomes = '';
                    for( var x = 0; x < input.get(0).files.length; x++)
                    {
                        nomes += input.get(0).files[x].name.split(/[\\|/]/).pop() + ', ';
                    }
                    input.parent().parent().find('.form-control').html(nomes.slice(0, -2));

                checkFiles(input.get(0).files, scope.maxkbytes, scope.accept, scope.numfilesreq, scope.required, scope.checkmaxfiles, function (result) {
                    if (result) {
                        input.trigger('fileselect', [numFiles, label]);

                        var files = input.get(0).files;

                            scope.model.ARQUIVO = [];
                            scope.model.NOMEARQUIVO = [];

                            scope.model.OBRIGATORIO = "F";
                            if (scope.required)
                                scope.model.OBRIGATORIO = "T";

                            scope.model.NAME = scope.name;
                            scope.model.DESCRICAO = scope.label;
                            scope.model.NOMEORIGINAL = scope.filename;
                            for(var p = 0; p < files.length; p++){
                                var reader = new FileReader();

                                reader.onload = function (e) {
                                    var temp = e.target.result.split(',')[1];
                                    scope.model.ARQUIVO.push(temp);
                                };

                                reader.readAsDataURL(files[p]);
                                scope.model.NOMEARQUIVO.push(files[p].name);
                            }
                    } else {
                        input.val(''); //limpa o input
                        input.parent().parent().find('.form-control').html('');
                    }
                    scope.$apply();
                });
            });

            element.find('input[type=file]').on('fileselect', function (event, numFiles, label) {

                var input = angular.element(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' ' + i18nFilter('l-files selected', [], 'js/diretivas') : label;

                if (input.length) {
                    input.val(log);
                } 
            });

            element.find('span').on('click', function () {
                if (this.className.indexOf('btnClean') > 0){
                    var input = angular.element(this);
                    input.parent().parent().find('input[type=file]').val('');
                    input.parent().parent().find('.form-control').html('');
                    
                    scope.model.ARQUIVO = [];
                    scope.model.NOMEARQUIVO = [];
                    scope.model.NAME = scope.name;
                    scope.model.DESCRICAO = scope.label;
                    scope.model.NOMEORIGINAL = scope.filename;
                    scope.model.OBRIGATORIO = "F";
                    if (scope.required)
                        scope.model.OBRIGATORIO = "T";                    
                }
                else if (this.className.indexOf('btnDownload') > 0){
                    scope.$parent.downloadFile(scope.keydownload);
                }
            });
        }

        function checkFiles (arrayFiles, maxFileSizeKbytes, extensionsPermitted, maxFiles, obrigatorio, checkNumFiles, callback) {
            var result = true;
           
            if (angular.isDefined(arrayFiles)) {
                angular.forEach(arrayFiles, function (value) {

                    if ((angular.isDefined(maxFileSizeKbytes) && maxFileSizeKbytes > 0) && Math.round(value.size / 1024) > maxFileSizeKbytes) {
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-edups-upload-file', [], 'js/diretivas'),
                            detail: value.name + i18nFilter('l-edupsuploadfile-valida-file-size', [], 'js/diretivas') + maxFileSizeKbytes + ' KB'
                        });
                        result = false;
                    }

                    if (angular.isDefined(extensionsPermitted) && extensionsPermitted !== '') {
                        var extFile = value.name.split('.').pop();
                        if (extensionsPermitted.toLowerCase().split(';').indexOf(extFile.toLowerCase()) === -1) {
                            totvsNotification.notify({
                                type: 'warning',
                                title: i18nFilter('l-edups-upload-file', [], 'js/diretivas'),
                                detail: value.name + i18nFilter('l-edupsuploadfile-valida-file-extension', [], 'js/diretivas') + extensionsPermitted
                            });
                            result = false;
                        }
                    }
                });

                //Validação para quantidade de arquivos
                if (checkNumFiles){
                    if ((obrigatorio && maxFiles > 1 && maxFiles !== arrayFiles.length) || (maxFiles > 1 && maxFiles < arrayFiles.length))
                    {
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-edups-maxfiles', [], 'js/diretivas'),
                            detail: i18nFilter('l-edups-maxfilesmsg', [], 'js/diretivas') + maxFiles + i18nFilter('l-edups-maxfilesmsgend', [], 'js/diretivas')
                        });
                        result = false;
                    }
                }
            }

            if (typeof callback === 'function') {
                callback(result);
            }
        }        
    }
});
