/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.23
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module edupsDiretivasModule
 * @name edupsFileModelDirective
 * @object directive
 *
 * @created 2018-11-14 v12.1.23
 * @updated
 *
 * @requires diretivas.module
 *
 * @dependencies $totvsresource, $compile
 *
 * @description Diretiva edupsFileModelDirective
 */
define(['diretivas/diretivas.module'], function () {

    'use strict';

    angular.module('edupsDiretivasModule').directive('edupsFileModel', EdupsFileModelDirective);

    EdupsFileModelDirective.$inject = ['$parse'];

    function EdupsFileModelDirective($parse) {

        var edupsSelecaoDirective = {
            restrict: 'A',
            link: link
        };

        return edupsSelecaoDirective;

        //Função para manipulação do DOM (atributos, eventos, observadores)
        function link(scope, element, attrs) {

            var model = $parse(attrs.edupsFileModel),
                modelSetter = model.assign;

            element.bind('change', function () {

                scope.$apply(function () {

                    modelSetter(scope, element[0].files[0]);

                });
            });
        }
    }
});
