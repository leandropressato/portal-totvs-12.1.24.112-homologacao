/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module edupsDiretivasModule
 * @name eduPaisUfCidade
 * @object directive
 *
 * @created 31/01/2017 v12.1.15
 * @updated
 *
 * @requires diretivas.module
 *
 * @dependencies
 *
 * @restrict E
 *
 *
 * @description Diretiva para Combos de Países, Estados e Cidades
 *
 */
define(['diretivas/diretivas.module', 'utils/edups-utils.factory', 'utils/edups-enums.constants'], function () {

    'use strict';

    angular.module('edupsDiretivasModule').directive('edupsPaisUfCidade', EdupsPaisUfCidadeDirective);

    EdupsPaisUfCidadeDirective.$inject = [];

    /********************************************************************
                        Diretiva EduPaisUfCidade
    *********************************************************************/
    function EdupsPaisUfCidadeDirective() {

        var edupsPaisUfCidadeDirective = {
            restrict: 'E',
            scope: {
                cssClass: '@',
                cssClassPais: '=?',
                cssClassEstado: '=?',
                cssClassCidade: '=?',

                labelPais: '@',
                labelEstado: '@',
                labelCidade: '@',

                tVisible: '=?',
                campoPaisVisible: '=?',
                campoEstadoVisible: '=?',
                campoCidadeVisible: '=?',

                campoPaisRequired: '=?',
                campoEstadoRequired: '=?',
                campoCidadeRequired: '=?',

                listaPaises: '=?',
                listaEstados: '=?',

                ngModelPais: '=?',
                ngModelPaisDesc: '=?',
                ngModelEstado: '=?',
                ngModelEstadoDesc: '=?',
                ngModelCidade: '=?',
                ngModelCidadeDesc: '=?',

                ngModelCidadeEstrangeiro: '=?',

                possuiCidade: '@?',

                paisChange: '&?'
            },
            template:   '<div class="{{objController.cssClass}}" ng-if="objController.tVisible"> ' +
                            '<field ' +
                                'label="{{objController.labelPais}}" ' +
                                'class="{{objController.cssClassPais}}" ' +
                                'type="combo" ' +
                                'ng-model="objController.ngModelPais" ' +
                                'ng-options="paises as paises.DESCRICAO for paises in objController.objPaisList track by paises.IDPAIS" ' +
                                'ng-show="objController.campoPaisVisible" ' +
                                'name-id="pais_{{objController.scopeId}}"' +
                                'name="{{objController.labelPais}}" ' +
                                'canclean> ' +
                                '</field> ' +
                            '<field ' +
                                'label="{{objController.labelEstado}}" ' +
                                'class="{{objController.cssClassEstado}}" ' +
                                'name-id="estado_{{objController.scopeId}}"' +
                                'type="combo" ' +
                                'ng-model="objController.ngModelEstado" ' +
                                'ng-options="estados as estados.NOME for estados in objController.objEstadoList" ' +
                                'ng-show="objController.campoEstadoVisible" ' +
                                'ng-required="objController.campoEstadoRequired" ' +
                                'name="{{objController.labelEstado}}" ' +
                                'canclean> ' +
                            '</field> ' +
                            '<div ng-switch="objController.possuiCidade"> ' +
                                '<div ng-switch-when="true"> ' +
                                    '<field ' +
                                        'name-id="cidade_{{objController.scopeId}}"' +
                                        'label="{{objController.labelCidade}}" ' +
                                        'class="{{objController.cssClassCidade}}" ' +
                                        'type="combo" ' +
                                        'ng-disabled="!objController.ngModelEstado && objController.campoEstadoVisible" ' +
                                        'ng-model="objController.ngModelCidade" ' +
                                        'ng-options="municipios as municipios.NOMEMUNICIPIO for municipios in objController.objMunicipiosList" ' +
                                        'ng-show="objController.campoCidadeVisible" ' +
                                        'ng-required="objController.campoCidadeRequired" ' +
                                        'name="{{objController.labelCidade}}" ' +
                                        'canclean> ' +
                                    '</field> ' +
                                '</div> ' +
                                '<div ng-switch-when="false"> ' +
                                    '<field ' +
                                        'name-id="cidade_estrangeiro_{{objController.scopeId}}"' +
                                        'label="{{objController.labelCidade}}" ' +
                                        'class="{{objController.cssClassCidade}}" ' +
                                        'type="input" ' +
                                        'ng-disabled="!objController.ngModelEstado && objController.campoEstadoVisible" ' +
                                        'ng-model="objController.ngModelCidadeEstrangeiro" ' +
                                        'ng-show="objController.campoCidadeVisible" ' +
                                        'ng-required="objController.campoCidadeRequired" ' +
                                        'name="{{objController.labelCidade}}" ' +
                                        'canclean> ' +
                                    '</field> ' +
                                '</div>' +
                            '</div>' +
                        '</div>',
            controller: ['$scope', '$filter', '$timeout', '$interval', 'edupsUtilsFactory', 'edupsEnumsConsts', function EdupsPaisUfCidadeController($scope, $filter, $timeout, $interval, edupsUtilsFactory, edupsEnumsConsts) {

                var self = this;
                self.objPaisList = [];
                self.objEstadoList = [];
                self.objMunicipiosList = [];
                self.scopeId = $scope.$id;

                self.setDefaultValues = setDefaultValues;
                self.getListaPaises = getListaPaises;
                self.getListaEstados = getListaEstados;
                self.getListaEstadosSemPais = getListaEstadosSemPais;
                self.getListaMunicipios = getListaMunicipios;

                var cargaListaEstados = undefined;
                self.encerrarCargaEstado = function () {
                    if (angular.isDefined(cargaListaEstados)) {

                        $interval.cancel(cargaListaEstados);
                        cargaListaEstados = undefined;
                    }
                };

                var cargaListaMunicipios = undefined;
                self.encerrarCargaMunicipios = function () {
                    if (angular.isDefined(cargaListaMunicipios)) {

                        $interval.cancel(cargaListaMunicipios);
                        cargaListaMunicipios = undefined;
                    }
                };

                var timeout = 250,
                    tentativas = 100;

                init();

                function init() {

                    self.setDefaultValues();
                    self.getListaPaises();
                    self.getListaEstadosSemPais();

                    var myWatchPaisList = $scope.$watch('objController.listaPaises', function (newValue) {
                        if (!!newValue) {
                            self.objPaisList = angular.copy(self.listaPaises);
                            if (self.ngModelPais && angular.isArray(self.objPaisList) && self.objPaisList.length > 0) {
                                var paisSelecionado = self.objPaisList.find(function (item) {
                                    if (item.IDPAIS == self.ngModelPais.IDPAIS) {
                                        return item;
                                    }
                                });

                                if (angular.isDefined(paisSelecionado)) {
                                    self.ngModelPais = paisSelecionado;
                                }

                                myWatchPaisList();
                            }
                        }
                    });

                    $scope.$watch('objController.ngModelPais', function (newValue, oldValue) {

                        if (!!newValue && (!oldValue || newValue.IDPAIS != oldValue.IDPAIS)) {

                            self.setPais(newValue);
                        }
                        else if (!!oldValue && !newValue) {

                            self.setPais(null);
                        }
                    }, true);

                    $scope.$watch('objController.ngModelEstado', function (newValue, oldValue) {

                        if (!!newValue && (!oldValue || newValue.CODETD != oldValue.CODETD) && angular.isUndefined(cargaListaEstados)) {
                            cargaListaEstados = $interval(function () {self.setEstado(newValue);}, timeout, tentativas);
                        }
                        else if (!!oldValue && !newValue) {

                            self.setEstado(null);
                        }
                    }, true);

                    $scope.$watch('objController.ngModelCidade', function (newValue, oldValue) {

                        if (!!newValue && (!oldValue || newValue.CODMUNICIPIO != oldValue.CODMUNICIPIO) && angular.isUndefined(cargaListaMunicipios)) {

                            cargaListaMunicipios = $interval(function () {self.setCidade(newValue);}, timeout, tentativas);

                        }
                        else if (!!oldValue && !newValue) {

                            self.setCidade(null);
                        }
                    }, true);

                    $scope.$watch('objController.ngModelCidadeEstrangeiro', function (newValue) {

                        if (!!newValue) {

                            self.ngModelCidadeEstrangeiro = newValue;
                        }
                    });
                }

                function setDefaultValues() {
                    if (angular.isUndefined(self.tVisible)) {
                        self.tVisible = false;
                    }

                    if (angular.isUndefined(self.campoPaisVisible)) {
                        self.campoPaisVisible = true;
                    }

                    if (angular.isUndefined(self.campoEstadoVisible)) {
                        self.campoEstadoVisible = true;
                    }

                    if (angular.isUndefined(self.campoCidadeVisible)) {
                        self.campoCidadeVisible = true;
                    }

                    if (angular.isUndefined(self.campoPaisRequired)) {
                        self.campoPaisRequired = false;
                    }

                    if (angular.isUndefined(self.campoEstadoRequired)) {
                        self.campoEstadoRequired = false;
                    }

                    if (angular.isUndefined(self.campoCidadeRequired)) {
                        self.campoCidadeRequired = false;
                    }

                    if (angular.isUndefined(self.cssClassPais)) {
                        self.cssClassPais = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
                    }

                    if (angular.isUndefined(self.cssClassEstado)) {
                        self.cssClassEstado = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
                    }

                    if (angular.isUndefined(self.cssClassCidade)) {
                        self.cssClassCidade = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
                    }

                    if (angular.isUndefined(self.possuiCidade)) {
                        self.possuiCidade = true;
                    }

                    self.cssClass = self.cssClass || '';
                    self.labelPais = self.labelPais || '';
                    self.labelEstado = self.labelEstado || '';
                    self.labelCidade = self.labelCidade || '';

                    self.ngModelPais = self.ngModelPais || null;
                    self.ngModelEstado = self.ngModelEstado || null;
                    self.ngModelCidade = self.ngModelCidade || null;

                    self.ngModelPaisDesc = self.ngModelPaisDesc || null;
                    self.ngModelEstadoDesc = self.ngModelEstadoDesc || null;
                    self.ngModelCidadeDesc = self.ngModelCidadeDesc || null;
                }

                function getListaPaises() {
                    if (self.objPaisList.length === 0) {

                        edupsUtilsFactory.getListaPaises(function (result) {
                            if (angular.isDefined(result) && angular.isDefined(result.GPais)) {

                                self.objPaisList = result.GPais;

                                if (angular.isArray(self.objPaisList) && self.objPaisList.length > 0) {
                                    self.objPaisList = $filter('orderBy')(self.objPaisList, 'DESCRICAO');
                                }
                            }
                        });
                    }
                }

                function getListaEstados(objPais) {
                    if (!!objPais && objPais.IDPAIS) {

                        edupsUtilsFactory.getListaEstados(objPais.IDPAIS, function (result) {
                            if (angular.isDefined(result) && angular.isDefined(result.GEtd)) {
                                self.objEstadoList = result.GEtd;
                            }
                        });
                    }
                }

                function getListaEstadosSemPais() {
                    edupsUtilsFactory.getListaEstados(edupsEnumsConsts.EdupsTipoEstadoSemPais.EstadoSemPais, function (result) {
                        if (angular.isDefined(result) && angular.isDefined(result.GEtd)) {
                            self.objEstadoList = result.GEtd;
                        }
                    });
                }

                function getListaMunicipios(objUF) {

                    if (angular.isDefined(objUF) && angular.isDefined(objUF.CODETD)) {

                        edupsUtilsFactory.getListaMunicipios(objUF.CODETD, function (result) {

                            if (angular.isDefined(result) && angular.isDefined(result.GMUNICIPIO)) {
                                self.objMunicipiosList = result.GMUNICIPIO;

                                self.possuiCidade = self.objMunicipiosList.length > 0;

                                if (!self.possuiCidade && !!self.ngModelCidade) {
                                    self.ngModelCidade['CODMUNICIPIO'] = null;
                                }
                            }
                        });
                    }
                }

                self.setPais = function (objPais) {

                    if (!!objPais) {

                        if (self.objPaisList.length > 0) {
                            var paisSelecionado = self.objPaisList.find(function (item) {
                                if (item.IDPAIS == objPais.IDPAIS) {
                                    return item;
                                }
                            });

                            if (!!paisSelecionado) {
                                self.ngModelPais = paisSelecionado;
                                self.ngModelPaisDesc = paisSelecionado.DESCRICAO;

                                if (angular.isFunction(self.paisChange)) {
                                    $timeout(function () { self.paisChange(); }, 0, true);
                                }

                                self.getListaEstados(paisSelecionado);
                            }
                        }
                    }
                    else {
                        self.ngModelPais = null;
                        self.ngModelPaisDesc = null;

                        self.objEstadoList = self.getListaEstadosSemPais();
                        self.ngModelEstado = false;

                        self.setCidade(null);
                        self.ngModelCidade = null;
                        self.ngModelCidadeDesc = null;
                        self.ngModelCidadeEstrangeiro = null;
                    }
                };

                self.setEstado = function (objEstado) {

                    if (!!objEstado) {

                        if (self.objEstadoList.length > 0) {
                            self.objMunicipiosList = [];

                            var estadoSelecionado = self.objEstadoList.find(function (item) {
                                if (item.CODETD == objEstado.CODETD) {
                                    return item;
                                }
                            });

                            if (!!estadoSelecionado) {

                                self.ngModelEstado = estadoSelecionado;
                                self.ngModelEstadoDesc = estadoSelecionado.NOME;

                                self.encerrarCargaEstado();
                                self.getListaMunicipios(estadoSelecionado);
                            }
                        }
                    }
                    else {

                        self.ngModelEstado = null;
                        self.ngModelEstadoDesc = null;

                        self.setCidade(null);

                        self.ngModelCidadeEstrangeiro = null;
                    }
                };

                self.setCidade = function (objCidade) {

                    if (!!objCidade) {

                        if (self.objMunicipiosList.length > 0) {
                            var cidadeSelecionada = self.objMunicipiosList.find(function (item) {
                                if (item.CODMUNICIPIO == objCidade.CODMUNICIPIO) {
                                    return item;
                                }
                            });

                            if (!!cidadeSelecionada) {

                                self.ngModelCidade = cidadeSelecionada;
                                self.ngModelCidadeDesc = cidadeSelecionada.NOMEMUNICIPIO;

                                self.encerrarCargaMunicipios();
                            }
                        }
                    }
                    else {
                        self.ngModelCidade = null;
                        self.ngModelCidadeDesc = null;
                    }
                };
            }],
            controllerAs: 'objController',
            bindToController: true
        };

        return edupsPaisUfCidadeDirective;
    }
});
