[
    {
        "l-edups-upload-file": {
            "pt": "Upload de Arquivos",
			"en": "Upload of Files",
			"es": "Upload de archivos"
        },
        "l-edupsuploadfile-valida-file-size": {
            "pt": " ultrapassou o tamanho máximo permitido. Tamanho máximo permitido: ",
			"en": " the maximum size allowed exceeded. Maximum size allowed:",
			"es": " sobrepasó el tamaño máximo permitido. Tamaño máximo permitido: "
        },
        "l-edupsuploadfile-valida-file-extension": {
            "pt": " não é válido. As extensões permitidas são: ",
			"en": " is not valid. The extensions allowed are:",
			"es": " no es válido. Las extensiones permitidas son: "
        },
        "l-ext-permitidas": {
            "pt": "Extensões permitidas: ",
			"en": "Extensions allowed:",
			"es": "Extensiones permitidas: "
        },
        "l-max-file-size": {
            "pt": "Tamanho máximo do arquivo: ",
			"en": "Maximum size of file:",
			"es": "Tamaño máximo del archivo: "
        },
        "l-label-documentos": {
            "pt": "Enviar arquivo do documento",
            "en": "Send file of document",
            "es": "Enviar archivo del documento"
        },
        "l-label-obrigatorio": {
            "pt": "Obrigatório",
            "en": "Required",
            "es": "Obligatorio"
        },
        "l-edups-maxfiles": {
            "pt": "Quantidade de arquivos",
            "en": "Quantidade de arquivos",
            "es": "Quantidade de arquivos"
        },
        "l-edups-maxfilesmsg": {
            "pt": "São esperados o envio de ",
            "en": "Are expected to send ",
            "es": "Se esperan los envíos de "
        },
        "l-edups-maxfilesmsgend": {
            "pt": " arquivos.",
            "en": " files.",
            "es": " archivos."
        },
        "l-download": {
            "pt": "Fazer download de arquivo",
            "en": "Download file",
            "es": "Descargar archivo"
        },
        "l-select-file": {
            "pt": "Selecionar arquivo",
            "en": "Select file",
            "es": "Selecionar archivo"
        },
        "l-limpar": {
            "pt": "Limpar campo",
            "en": "Clean field",
            "es": "Limpiar campo"
        }
    }
]