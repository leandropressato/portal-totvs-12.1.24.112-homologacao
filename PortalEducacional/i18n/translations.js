[
    {
        "recents": {
            "pt": "Recentes",
            "en": "Recents",
			"es": "Recientes"
        },
        "favorites": {
            "pt": "Favoritos",
            "en": "Favoutites",
			"es": "Favoritos"
        },
        "favorite": {
            "pt": "Favorito",
            "en": "Favourite",
			"es": "Favorito"
        },
        "applications": {
            "pt": "Aplicações",
            "en": "Applications",
			"es": "Aplicaciones"
        },
        "processes": {
            "pt": "Processos",
            "en": "Processes",
			"es": "Procesos"
        },
        "tasks": {
            "pt": "Tarefas",
            "en": "Tasks",
			"es": "Tareas"
        },
        "reports": {
            "pt": "Relatórios",
            "en": "Reports",
			"es": "Informes"
        },
        "queries": {
            "pt": "Consultas",
            "en": "Queries",
			"es": "Consultas"
        },
        "records": {
            "pt": "Cadastros",
            "en": "Registers",
			"es": "Archivos"
        },
        "search": {
            "pt": "Pesquisar...",
            "en": "Search...",
			"es": "Buscar..."
        },
        "program": {
            "pt": "Programa",
            "en": "Program",
			"es": "Programa"
        },
        "name": {
            "pt": "Nome",
            "en": "Name",
			"es": "Nombre"
        },
        "module": {
            "pt": "Módulo",
            "en": "Module",
			"es": "Módulo"
        },
        "filter-program": {
            "pt": "Filtrar resultado",
            "en": "Filter result",
			"es": "Filtrar resultado"
        },
        "execute": {
            "pt": "Executar",
            "en": "Execute",
			"es": "Ejecutar"
        },
        "add-favorite": {
            "pt": "Adicionar aos favoritos",
            "en": "Add to favourites",
			"es": "Agregar a favoritos"
        },
        "remove-favorite": {
            "pt": "Remover dos favoritos",
            "en": "Remove from favourites",
			"es": "Retirar de favoritos"
        },
        "l-to": {
            "pt": "a",
            "en": "to",
			"es": "a"
        },
        "btn-close": {
            "pt": "Fechar",
            "en": "Close",
			"es": "Cerrar"
        },
        "btn-advanced-search": {
            "pt": "Busca Avançada",
            "en": "Advanced Search",
			"es": "Búsqueda avanzada"

        },
        "l-search": {
            "pt": "Pesquisa",
            "en": "Search",
			"es": "Búsqueda"
        },
        "l-filter-by": {
            "pt": "Filtrado por",
            "en": "Filtered by",
			"es": "Filtrado por"
        },
        "l-more-results": {
            "pt": "Mais Resultados",
            "en": "More Results",
            "es": "Más resultados"
        },
        "btn-cancel": {
            "pt": "Cancelar",
            "en": "Cancel",
            "es": "Anular"
        },
        "btn-ok": {
            "pt": "Ok",
            "en": "Ok",
            "es": "Ok"
        },
        "btn-select": {
            "pt": "Selecionar",
            "en": "Select",
            "es": "Seleccionar"
        },
        "btn-back": {
            "pt": "Voltar",
            "en": "Back",
			"es": "Volver"
        },
        "btn-add": {
            "pt": "Adicionar",
            "en": "Add",
            "es": "Agregar"
        },
        "btn-save": {
            "pt": "Salvar",
            "en": "Save",
            "es": "Grabar"
        },
        "btn-save-new": {
            "pt": "Salvar e novo",
            "en": "Save and new",
            "es": "Grabar y nuevo"
        },
        "btn-update": {
            "pt": "Atualizar",
            "en": "Update",
            "es": "Actualizar"
        },
        "btn-update-new": {
            "pt": "Atualizar e novo",
            "en": "Update and new",
            "es": "Actualizar y nuevo"
        },
        "btn-edit": {
            "pt": "Editar",
            "en": "Edit",
            "es": "Editar"
        },
        "btn-remove": {
            "pt": "Excluir",
            "en": "Delete",
            "es": "Borrar"
        },
        "btn-confirm": {
            "pt": "Confirmar",
            "en": "Confirm",
			"es": "Confirmar"
        },
        "btn-execution": {
            "pt": "Executar",
            "en": "Execute",
			"es": "Ejecutar"
        },
        "btn-actions": {
            "pt": "Outras ações",
            "en": "Other actions",
            "es": "Otras acciones"
        },
        "btn-apply": {
            "pt": "Aplicar",
            "en": "Apply",
			"es": "Aplicar"
        },
        "l-yes": {
            "pt": "Sim",
            "en": "Yes",
            "es": "Sí"
        },
        "l-no": {
            "pt": "Não",
            "en": "No",
            "es": "No"
        },
        "l-start": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-end": {
            "pt": "Fim",
			"en": "End",
			"es": "Final"
        },
        "l-cancel-operation": {
            "pt": "Deseja cancelar a operação?",
            "en": "Cancel the operation?",
            "es": "¿Desea anular la operación?"
        },
        "l-confirm-delete-operation": {
            "pt": "Você confirma a remoção deste registro?",
            "en": "Confirm the removal of this record?",
            "es": "¿Confirma la eliminación de este registro?"
        },
        "close-info-more": {
            "pt": "Ocultar detalhes...",
            "en": "Hide details...",
            "es": "Ocultar detalles..."
        },
        "open-info-more": {
            "pt": "Exibir detalhes...",
            "en": "Show details...",
            "es": "Mostrar detalles..."
        },
        "loading": {
            "pt": "Carregando",
            "en": "Loading",
            "es": "Cargando"
        },
        "l-question": {
            "pt": "Pergunta",
            "en": "Question",
            "es": "Pregunta"
        },
        "l-msg-not-found": {
            "pt": "Nenhum registro encontrado!",
            "en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },
        "l-initial-date": {
            "pt": "Início",
            "en": "Start",
			"es": "Inicio"
        },
        "l-final-date": {
            "pt": "Fim",
            "en": "End",
			"es": "Final"
        },
        "l-ola": {
            "pt": "Olá",
            "en": "Hello",
			"es": "Hola"
        },
        "btn-confirmar": {
            "pt": "Confirmar",
            "en": "Confirm",
			"es": "Confirmar"
        },
        "l-selecione-curso": {
            "pt": "Selecione o CURSO para acessar o Portal",
            "en": "Select the COURSE to access the Portal",
			"es": "Seleccione la ESPECIALIDAD para acceder al Portal"
        },
        "l-selecione-aluno": {
            "pt": "Selecione o ALUNO para acessar o Portal",
            "en": "Select the STUDENT to access the Portal",
			"es": "Seleccione el ALUMNO para acceder al Portal"

        },
        "l-nivel": {
            "pt": "Nível",
            "en": "Level",
			"es": "Nivel"
        },
        "l-periodo": {
            "pt": "Período",
            "en": "Period",
			"es": "Período"
        },
        "l-serie": {
            "pt": "Série",
            "en": "Series",
			"es": "Serie"
        },
        "l-grade-curricular": {
            "pt": "Grade Curricular",
			"en": "Curriculum Grid",
			"es": "Mapa curricular"
        },
        "l-filial": {
            "pt": "Filial",
            "en": "Branch",
			"es": "Sucursal"
        },
        "l-habilitacao": {
            "pt": "Habilitação",
            "en": "Graduation",
			"es": "Graduación"
        },
        "l-menu": {
            "pt": "Menu",
            "en": "Menu",
			"es": "Menú"
        },
        "l-ra": {
            "pt": "RA",
            "en": "ER",
			"es": "RA"
        },
        "l-curso": {
            "pt": "Curso",
            "en": "Course",
			"es": "Especialidad"
        },
        "l-alterar-aluno": {
            "pt": "Alterar Aluno",
            "en": "Change Student",
			"es": "Modificar alumno"
        },
        "l-alterar-curso": {
            "pt": "Alterar Curso",
            "en": "Change Cost",
			"es": "Alterar especialidad"
        },
        "l-config": {
            "pt": "Config",
            "en": "Config",
			"es": "Config"
        },
        "l-help": {
            "pt": "Help",
            "en": "Help",
			"es": "Help"
        },
        "l-logoff": {
            "pt": "Logoff",
            "en": "Logoff",
			"es": "Logoff"
        },
        "l-nenhum-curso": {
            "pt": "Não foi encontrado nenhum curso para este usuário",
            "en": "No course for this user",
			"es": "No se encontró ninguna especialidad para este usuario"
        },
        "l-portal-aluno": {
            "pt": "Portal do Aluno",
            "en": "Student's Portal",
			"es": "Portal del alumno"
        },
        "l-urls-externas": {
            "pt": "Url's Externas",
            "en": "External Url's",
			"es": "Urls externas"
        },
        "l-entrar-como": {
            "pt": "Entrar como",
            "en": "Enter as",
			"es": "Entrar como"
        },
        "l-entrar-como-aluno": {
            "pt": "Aluno",
            "en": "Student",
			"es": "Alumno"
        },
        "l-entrar-como-responsavel": {
            "pt": "Responsável",
            "en": "Responsible party",
			"es": "Responsable"
        },
        "l-todas-etapas": {
            "pt": "TODAS",
            "en": "ALL",
			"es": "TODAS"
        },
        "l-etapas": {
            "pt": "Etapas",
            "en": "Stages",
			"es": "Etapas"
        },
        "l-todas-disciplinas": {
            "pt": "TODAS",
            "en": "ALL",
			"es": "TODAS"
        },
        "l-disciplinas": {
            "pt": "Disciplinas",
            "en": "Course Subject",
			"es": "Materias"
        },
        "l-turma": {
            "pt": "Turma",
            "en": "Class",
			"es": "Grupo"
        },
        "l-CadastroAcademico":{
            "pt": "Cadastro Acadêmico",
            "en": "Academic Register",
			"es": "Registro académico"
        },
        "l-Atencao":{
            "pt": "Atenção",
            "en": "Attention",
			"es": "Atención"
        },
        "l-Informacao":{
            "pt": "Informação",
            "en": "Information",
			"es": "Información"
        },
        "l-Sucesso":{
            "pt": "Sucesso",
            "en": "Success",
			"es": "Éxito"
        },
        "l-Erro":{
            "pt": "Erro",
            "en": "Error",
			"es": "Error"
        },
         "l-msg-campos-preenchimento-obrigatorio":{
            "pt": "Campos marcados com (*) de preenchimento obrigatório!",
            "en": "Fields selected with (*) of required completion!",
			"es": "¡Campos marcados con (*) de cumplimentación obligatoria!"
        },
        "l-Confirmacao":{
            "pt": "Confirmação",
            "en": "Confirmation",
			"es": "Confirmación"
        },
        "grid-agrupamento-vazio":{
            "pt": "Arraste o cabeçalho de uma coluna e solte aqui para agrupar",
            "en": "Drag the header of a column and drop here to group",
			"es": "Arrastre el encabezado de una columna y suelte aquí para agrupar"
        },
        "l-msg-nenhum-registro":{
            "pt": "Nenhum registro encontrado!",
            "en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },

        "l-scheduler-messages.today":{
            "pt": "Hoje",
            "en": "Today",
			"es": "Hoy"
        },

        "l-scheduler-messages.save":{
            "pt": "Salvar",
            "en": "Salve",
			"es": "Grabar"
        },

        "l-scheduler-messages.cancel":{
            "pt": "Cancelar",
            "en": "Cancel",
			"es": "Anular"
        },

        "l-scheduler-messages.destroy":{
            "pt": "Apagar",
            "en": "Delete",
			"es": "Borrar"
        },

        "l-scheduler-messages.event":{
            "pt": "Evento",
            "en": "Event",
			"es": "Evento"
        },

        "l-scheduler-messages.date":{
            "pt": "Data",
            "en": "Date",
			"es": "Fecha"
        },

        "l-scheduler-messages.time":{
            "pt": "Hora",
            "en": "Time",
			"es": "Hora"
        },

        "l-scheduler-messages.allDay":{
            "pt": "Dia todo",
            "en": "Whole day",
			"es": "Día completo"
        },

        "l-scheduler-messages.showFullDay":{
            "pt": "Exibir dia completo",
            "en": "Display the whole day",
			"es": "Mostrar día completo"
        },

        "l-scheduler-messages.showWorkDay":{
            "pt": "Exibir horário comercial",
            "en": "Display business hours",
			"es": "Mostrar horario comercial"
        },

        "l-scheduler-messages.defaultRowText":{
            "pt": "Todos os eventos",
            "en": "All events",
			"es": "Todos los eventos"
        },

        "l-scheduler-messages.deleteWindowTitle":{
            "pt": "Excluir Evento",
            "en": "Delete Event",
			"es": "Borrar evento"
        },

        "l-scheduler-messages.ariaEventLabel":{
            "pt": "{0} em {1:D} às {2:t}",
            "en": "{0} on {1:D} at {2:t}",
			"es": "{0} el {1:D} a las {2:t}"
        },

        "l-scheduler-messages.ariaSlotLabel":{
            "pt": "Selecionado de {0:t} a {1:t}",
            "en": "Selected from {0:t} to {1:t}",
			"es": "Seleccionado de {0:t} a {1:t}"
        },

        "l-scheduler-messages.views.day":{
            "pt": "Dia",
            "en": "Day",
			"es": "Día"
        },

        "l-scheduler-messages.views.week":{
            "pt": "Semana",
            "en": "Week",
			"es": "Semana"
        },

        "l-scheduler-messages.views.month":{
            "pt": "Mês",
            "en": "Month",
			"es": "Mes"
        },

        "l-scheduler-messages.views.timeline":{
            "pt": "Linha do Tempo",
            "en": "Timeline",
			"es": "Línea de tiempo"
        },

        "l-scheduler-messages.views.workWeek":{
            "pt": "Semana de Trabalho",
            "en": "Work week",
			"es": "Semana de trabajo"
        },

        "l-scheduler-messages.editable.confirmation":{
            "pt": "Tem certeza de que deseja excluir este evento?",
            "en": "Are you sure you want to delete this event?",
			"es": "¿Tiene seguridad que desea borrar este evento?"
        },

        "l-scheduler-messages.editor.allDayEvent":{
            "pt": "Evento o dia todo",
            "en": "Event the whole day",
			"es": "Evento el día completo"
        },

        "l-scheduler-messages.editor.description":{
            "pt": "Descrição",
            "en": "Description",
			"es": "Descripción"
        },

        "l-scheduler-messages.editor.editorTitle":{
            "pt": "Editar Evento",
            "en": "Edit Event",
			"es": "Editar evento"
        },

        "l-scheduler-messages.editor.end":{
            "pt": "Fim",
            "en": "End",
			"es": "Final"
        },

        "l-scheduler-messages.editor.endTimezone":{
            "pt": "Evento o dia todo",
            "en": "Event the whole day",
			"es": "Evento el día completo"
        },

        "l-scheduler-messages.editor.repeat":{
            "pt": "Repetir",
            "en": "Repeat",
			"es": "Repetir"
        },

        "l-scheduler-messages.editor.separateTimezones":{
            "pt": "Definir diferentes fusos horários de início e fim",
            "en": "Define different start and end timezones",
			"es": "Definir diferentes husos horarios de inicio y final"
        },

        "l-scheduler-messages.editor.start":{
            "pt": "Início",
            "en": "Start",
			"es": "Inicio"
        },

        "l-scheduler-messages.editor.startTimezone":{
            "pt": "Data inicial fuso horário",
            "en": "Start Date timezone",
			"es": "Fecha inicial huso horario"
        },

        "l-scheduler-messages.editor.timezone":{
            "pt": "Fuso horário do evento",
            "en": "Event timezone",
			"es": "Huso horario del evento"
        },

        "l-scheduler-messages.editor.timezoneEditorButton":{
            "pt": "Fuso Horário",
            "en": "Timezone",
			"es": "Huso horario"
        },

        "l-scheduler-messages.editor.timezoneEditorTitle":{
            "pt": "Fusos horários",
            "en": "Timezones",
			"es": "Husos horarios"
        },

        "l-scheduler-messages.editor.title":{
            "pt": "Título",
            "en": "Title",
			"es": "Título"
        },

        "l-scheduler-messages.recurrenceEditor.daily.interval":{
            "pt": "dia(s)",
            "en": "day(s)",
			"es": "día(s)"
        },

        "l-scheduler-messages.recurrenceEditor.daily.repeatEvery":{
            "pt": "Repetir a cada:",
            "en": "Repeat at each:",
			"es": "Repetir a cada:"
        },

        "l-scheduler-messages.recurrenceEditor.end.after":{
            "pt": "Após",
            "en": "After",
			"es": "Después"
        },

        "l-scheduler-messages.recurrenceEditor.end.occurrence":{
            "pt": "ocorrências",
            "en": "occurrences",
			"es": "ocurrencias"
        },

        "l-scheduler-messages.recurrenceEditor.end.label":{
            "pt": "Fim",
            "en": "End",
			"es": "Final"
        },

        "l-scheduler-messages.recurrenceEditor.end.never":{
            "pt": "Nunca",
            "en": "Never",
			"es": "Nunca"
        },

        "l-scheduler-messages.recurrenceEditor.end.mobileLabel":{
            "pt": "Termina",
            "en": "Ends",
			"es": "Termina"
        },

        "l-scheduler-messages.recurrenceEditor.end.on":{
            "pt": "Em",
            "en": "In",
			"es": "En"
        },

        "l-scheduler-messages.recurrenceEditor.frequencies.daily":{
            "pt": "Diariamente",
            "en": "Daily",
			"es": "Diariamente"
        },

        "l-scheduler-messages.recurrenceEditor.frequencies.monthly":{
            "pt": "Mensal",
            "en": "Monthly",
			"es": "Mensual"
        },

        "l-scheduler-messages.recurrenceEditor.frequencies.never":{
            "pt": "Nunca",
            "en": "Never",
			"es": "Nunca"
        },

        "l-scheduler-messages.recurrenceEditor.frequencies.weekly":{
            "pt": "Semanalmente",
            "en": "Weekly",
			"es": "Semanalmente"
        },

        "l-scheduler-messages.recurrenceEditor.frequencies.yearly":{
            "pt": "Anualmente",
            "en": "Annually",
			"es": "Anualmente"

        },

        "l-scheduler-messages.recurrenceEditor.monthly.day":{
            "pt": "Dia ",
            "en": "Day ",
			"es": "Día "
        },

        "l-scheduler-messages.recurrenceEditor.monthly.interval":{
            "pt": "mês(es)",
            "en": "month(s)",
			"es": "mes(es)"
        },

        "l-scheduler-messages.recurrenceEditor.monthly.repeatEvery":{
            "pt": "Repetir a cada: ",
            "en": "Repeat at each: ",
			"es": "Repetir a cada: "
        },

        "l-scheduler-messages.recurrenceEditor.monthly.repeatOn":{
            "pt": "Repetir em: ",
            "en": "Repeat in: ",
			"es": "Repetir en: "

        },

        "l-scheduler-messages.recurrenceEditor.offsetPositions.first":{
            "pt": "primeiro",
            "en": "first",
			"es": "primero"
        },

        "l-scheduler-messages.recurrenceEditor.offsetPositions.second":{
            "pt": "segundo",
            "en": "second",
			"es": "segundo"
        },

        "l-scheduler-messages.recurrenceEditor.offsetPositions.third":{
            "pt": "terceiro",
            "en": "third",
			"es": "tercero"
        },

        "l-scheduler-messages.recurrenceEditor.offsetPositions.fourth":{
            "pt": "quarto",
            "en": "fourth",
			"es": "cuarto"
        },

        "l-scheduler-messages.recurrenceEditor.offsetPositions.last":{
            "pt": "último",
            "en": "last",
			"es": "último"
        },

        "l-scheduler-messages.recurrenceEditor.weekly.interval":{
            "pt": " semana(s)",
            "en": " week(s)",
			"es": " semana(s)"
        },

        "l-scheduler-messages.recurrenceEditor.weekly.repeatEvery":{
            "pt": "Repetir a cada: ",
            "en": "Repeat at each: ",
			"es": "Repetir a cada: "
        },

        "l-scheduler-messages.recurrenceEditor.weekly.repeatOn":{
            "pt": "Repetir em: ",
            "en": "Repeat in: ",
			"es": "Repetir en: "
        },

        "l-scheduler-messages.recurrenceEditor.weekdays.day":{
            "pt": "dia",
            "en": "day",
			"es": "día"
        },

        "l-scheduler-messages.recurrenceEditor.weekdays.weekday":{
            "pt": "semana",
            "en": "week",
			"es": "semana"
        },

        "l-scheduler-messages.recurrenceEditor.weekdays.weekend":{
            "pt": "final de semana",
            "en": "weekend",
			"es": "final de semana"
        },

        "l-scheduler-messages.recurrenceEditor.yearly.of":{
            "pt": "de ",
            "en": "from ",
			"es": "de "
        },

        "l-scheduler-messages.recurrenceEditor.yearly.repeatEvery":{
            "pt": "Repetir a cada: ",
            "en": "Repeat at each: ",
			"es": "Repetir a cada: "
        },

        "l-scheduler-messages.recurrenceEditor.yearly.repeatOn":{
            "pt": "Repetir em: ",
            "en": "Repeat in: ",
			"es": "Repetir en: "
        },

        "l-scheduler-messages.recurrenceEditor.yearly.interval":{
            "pt": " ano(s)",
            "en": " year(s)",
			"es": " año(s)"
        },

        "l-scheduler-messages.recurrenceMessages.deleteRecurring":{
            "pt": "Deseja excluir somente esta ocorrência de evento ou toda a série?",
            "en": "Do you want to delete only this occurrence of event or the whole series?",
			"es": "¿Desea borrar solamente esta ocurrencia de evento o toda la serie?"
        },

        "l-scheduler-messages.recurrenceMessages.deleteWindowOccurrence":{
            "pt": "Excluir ocorrência atual",
            "en": "Delete current occurrence",
			"es": "Borrar ocurrencia actual"
        },

        "l-scheduler-messages.recurrenceMessages.deleteWindowSeries":{
            "pt": "Excluir a série",
            "en": "Delete series",
			"es": "Borrar la serie"
        },

        "l-scheduler-messages.recurrenceMessages.deleteWindowTitle":{
            "pt": "Excluir item recorrente",
            "en": "Delete recurrent item",
			"es": "Borrar ítem recurrente"
        },

        "l-scheduler-messages.recurrenceMessages.editRecurring":{
            "pt": "Deseja editar apenas esta ocorrência de evento ou toda a série?",
            "en": "Do you want to edit only this occurrence of event or the whole series?",
			"es": "¿Desea editar solamente esta ocurrencia de evento o toda la serie?"
        },

        "l-scheduler-messages.recurrenceMessages.editWindowOccurrence":{
            "pt": "Editar ocorrência atual",
            "en": "Edit current occurrence",
			"es": "Editar ocurrencia actual"
        },

        "l-scheduler-messages.recurrenceMessages.editWindowSeries":{
            "pt": "Editar a série",
            "en": "Edit series",
			"es": "Editar la serie"
        },

        "l-scheduler-messages.recurrenceMessages.editWindowTitle":{
            "pt": "Editar item recorrente",
            "en": "Edit recurrent item",
			"es": "Editar ítem recurrente"
        },

        "btn-imprimir":{
            "pt": "Imprimir",
            "en": "Print",
			"es": "Imprimir"
        },

        "l-imprimir":{
            "pt": "Imprimir",
            "en": "Print",
			"es": "Imprimir"
        },

        "l-confirma-impressao":{
            "pt": "Confirma solicitação de impressão?",
            "en": "Confirm printing request?",
			"es": "¿Confirma solicitud de impresión?"
        },
        "l-sair": {
            "pt": "Sair",
            "en": "Exit",
			"es": "Salir"
        },
         "l-titulo-erro-login": {
            "pt": "Acesso Expirado",
            "en": "Expired Access",
			"es": "Acceso expirado"
        },
        "l-titulo-meu-pergamum": {
            "pt": "Meu PERGAMUM",
            "en": "Meu PERGAMUM",
            "es": "Meu PERGAMUM"
        },
        "l-msg-erro-login": {
            "pt": "Acesso ao portal expirado. É necessário fazer o Login na aplicação novamente!",
            "en": "Access to expired portal. Login the application again!",
			"es": "Accesso al portal expirado. ¡Es necesario realizar el Login en la aplicación nuevamente!"
        },
        "l-bussiness-error": {
            "pt": "Erro de negócio",
            "en": "Business error",
			"es": "Error de negocio"
        },
        "l-usuario": {
            "pt": "Usuário",
            "en": "User",
			"es": "Usuario"
        },
        "l-senha": {
            "pt": "Senha",
            "en": "Password",
			"es": "Contraseña"
        },
        "l-alias": {
            "pt": "Alias",
            "en": "Alias",
			"es": "Alias"
        },
        "l-acessar": {
            "pt": "Acessar",
            "en": "Access",
			"es": "Acceder"
        },
        "l-esqueceu-senha": {
            "pt": "Esqueceu sua senha?",
            "en": "Forgot your password?",
			"es": "¿Olvidó su contraseña?"
        },
        "l-email": {
            "pt": "E-mail",
            "en": "E-mail",
			"es": "E-mail"
        },
        "l-voltar-realizar-login": {
            "pt": "Voltar para o formulário de login",
            "en": "Bach to login form",
			"es": "Volver al formulario de login"
        },
        "l-continuar": {
            "pt": "Continuar",
            "en": "Continue",
			"es": "Continuar"
        },
        "l-senha-antiga": {
            "pt": "Digite a senha antiga!",
            "en": "Enter former password!",
			"es": "¡Digite la contraseña antigua!"
        },
        "l-senha1": {
            "pt": "Digite a nova senha!",
            "en": "Enter new password!",
			"es": "¡Digite la nueva contraseña!"
        },
        "l-senha2": {
            "pt": "Redigite a nova senha!",
            "en": "Re-enter new password!",
			"es": "¡Redigite la nueva contraseña!"
        },
        "l-msg-usuario-obrigatorio": {
            "pt": "O Campo \"Usuário\" é de preenchimento obrigatório!",
            "en": "The Field \"User\" is of required completion!",
			"es": "¡El Campo \"Usuario\" es de cumplimentación obligatoria!"
        },
        "l-msg-senha-obrigatorio": {
            "pt": "O Campo \"Senha\" é de preenchimento obrigatório!",
            "en": "The Field \"Password\" is of required completion!",
			"es": "El Campo \"Contraseña\" es de cumplimentación obligatoria!"
        },
        "l-msg-email-obrigatorio": {
            "pt": "O Campo \"E-mail\" é de preenchimento obrigatório! Preencha o campo com um e-mail válido.",
            "en": "The Field \"E-mail\" is of required completion! Enter the field with valid e-mail",
			"es": "El Campo \"E-mail\" es de cumplimentación obligatoria! Complete el campo con un e-mail válido."
        },
        "l-msg-senhas-nao-coincidem": {
            "pt": "As senhas informadas não coincidem. Favor revisar os dados informados e digite novamente!",
            "en": "Passwords entered do not match. Please review data informed and enter again!",
			"es": "Las contraseñas informadas no coinciden. ¡Por favor revise los datos informados y digite nuevamente!"
        },
        "l-alias-invalido": {
            "pt": "As configurações de acesso ao Portal do Aluno não foram definidas corretamente para o banco de dados '*ALIAS*'.",
            "en": "The configurations of access to the Student's Portal were not properly defined for'*ALIAS*' database.",
			"es": "Las configuraciones de acceso al Portal del alumno no se definieron correctamente para la base de datos '*ALIAS*'."
        },
        "l-usuario-nao-associado": {
            "pt": "Identificamos que sua conta não está associada à nenhum usuário do Portal do Aluno. Por favor, informe seus dados de login (usuário e senha) para que possamos vincular as contas.",
            "en": "We identified that your account is not associated to any user of the Student's Portal. Please, enter your login database (user and password) so they can bind the accounts",
			"es": "Identificamos que su cuenta de no está vinculada a ningún usuario del Portal del alumno. Por favor, informe sus datos de login (usuario y contraseña) para que podamos vincular las cuentas."
        },
        "l-vincular-contas": {
            "pt": "Unificar acesso ao Portal do Aluno com uma conta externa",
            "en": "Unify the access to the Student's Portal with an external account",
			"es": "Unificar acceso al Portal del alumno con una cuenta externa"
        },
        "l-ou-acesse-via-external": {
            "pt": "Ou utilize outra conta",
            "en": "Or use another account",
			"es": "O utilice otra cuenta"
        },
        "l-botao-acesse-facebook": {
            "pt": "Acessar usando Facebook",
            "en": "Access using Facebook",
			"es": "Acceder utilizando Facebook"
        },
        "l-associar-contas": {
            "pt": "Associar contas",
            "en": "Associate accounts",
			"es": "Vincular cuentas"
        },
        "l-fique-tranquilo": {
            "pt": "Fique tranquilo, o Portal Educacional não armazena suas informações de login e não iremos pedir essas informações novamente no futuro.",
            "en": "Do not worry, the Educational Portal does not store your login information and we will not ask this information again in the future.",
			"es": "Quede tranquilo, el Portal Educación no almacena su información de login y no pediremos esta información nuevamente en el futuro."
        },
        "l-senha-expirada-title": {
            "pt": "Senha expirada!",
            "en": "Expired password!",
			"es": "¡Contraseña expirada!"
        },
        "l-senha-expirada": {
            "pt": "A senha digitada não é mais válida para login. É necessário que você troque sua senha por uma outra diferente.",
            "en": "The password entered is no longer valid for login. You must exchange your password by other different.",
			"es": "La contraseña digitada no más es válida para login. Es necesario que cambie su contraseña por otra diferente."
        },
        "l-usuario-sem-permissao": {
            "pt": "Você não está autorizado a acessar essa funcionalidade",
            "en": "You don't have permission to access this page",
            "es": "No tienes permiso para acceder a esta página"
        },
        "l-todos": {
            "pt": "Todos",
			"en": "All",
			"es": "Todos"
        },
        "l-botao-acesse-Google": {
            "pt": "Acessar usando Google",
            "en": "Sign in with Google",
			"es": "Acceder utilizando Google"
        },
        "l-portal-diploma": {
            "pt": "Consulta diploma"
        },
        "l-pesquisar": {
            "pt": "Pesquisar",
			"en": "Pesquisar",
			"es": "Pesquisar"
        },
        "l-CPF": {
            "pt": "CPF",
			"en": "CPF",
			"es": "CPF"
        },
        "l-Nome": {
            "pt": "Nome",
			"en": "Nome",
			"es": "Nome"
        },        
        "l-instrucao-pesquisa": {
            "pt": "Informe o ",
			"en": "Informe o ",
			"es": "Informe o "
        },
        "l-Registro": {
            "pt": "Número de registro",
			"en": "Número de registro",
			"es": "Número de registro"
        },
        "l-Processo": {
            "pt": "Número de processo",
			"en": "Número de processo",
			"es": "Número de processo"
        },
        "l-dispositivo-mobile": {
            "pt": "Dispositivo mobile",
            "en": "Dispositivo mobile",
            "es": "Dispositivo mobile"
        },
        "l-msg-mobile-1": {
            "pt": "Identificamos que você está acessando o portal de um dispositivo mobile.",
            "en": "Identificamos que você está acessando o portal de um dispositivo mobile.",
            "es": "Identificamos que você está acessando o portal de um dispositivo mobile."
        },
        "l-msg-mobile-2": {
            "pt": "Para uma melhor experiência de uso, recomendamos que você acesse a versão mobile.",
            "en": "Para uma melhor experiência de uso, recomendamos que você acesse a versão mobile.",
            "es": "Para uma melhor experiência de uso, recomendamos que você acesse a versão mobile."
        },
        "l-btn-acessar-versao-mobile": {
            "pt": "Acessar versão mobile",
            "en": "Acessar versão mobile",
            "es": "Acessar versão mobile"
        },
        "l-btn-continuar-site": {
            "pt": "Continuar no site",
            "en": "Continuar no site",
            "es": "Continuar no site"
        }
    }
]
