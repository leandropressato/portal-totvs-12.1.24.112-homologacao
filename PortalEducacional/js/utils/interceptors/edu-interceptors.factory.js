define(['utils/interceptors/edu-interceptors.module'], function () {

    'use strict';

    angular
        .module('eduInterceptorsModule')
        .factory('EduInterceptors', EduInterceptors);

    EduInterceptors.$inject = ['$q'];

    // *********************************************************************************
    // *** Factory
    // *********************************************************************************
    
    /**
     * Factory para interceptar as mensagens dos serviços REST
     * @param   {object} $q Objeto para tratar as requisições e respostas
     * @returns {object} Factory interceptadora
     */
    function EduInterceptors ($q) {
        var factory = {
            request: request,
            requestError: requestError,
            response: response,
            responseError: responseError
        };

        return factory;

        /**
         * Requisição OK
         * @param   {object} config Configurações da requisição
         * @returns {object} Objeto da requisição
         */
        function request(config) {
            return config || $q.when(config);
        }

        /**
         * Requisição com erro
         * @param   {object} rejection Requisição que ocorreu o erro
         * @returns {object} Objeto requisitado rejeitado
         */
        function requestError(rejection) {
            return $q.reject(rejection);
        }

        /**
         * Resposta OK
         * @param   {object} response Objeto de resposta
         * @returns {object} Objeto de resposta
         */
        function response(response) {
            if (response) {                
                if (angular.isDefined(response['RMException:StackTrace'])) {
                    console.error(response['RMException:StackTrace']);
                }

                if (angular.isDefined(response['data']) && angular.isDefined(response['data']['RMException:StackTrace'])) {
                    console.error(response['data']['RMException:StackTrace']);
                }
            }

            return response;
        }

        /**
         * Resposta com erro
         * @param   {object} rejection Resposta que ocorreu o erro
         * @returns {object} Objeto de resposta rejeitado
         */
        function responseError(rejection) {
            return $q.reject(rejection);
        }
    }
});
