/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module dashboard
* @object module
*
* @created 2016-08-24 v12.1.15
* @updated 2016-08-24 v12.1.15
*
* @dependencies
*
* @description Modulo dashboard
*/

define(['utils/speech/speech.module'],function () {
    'use strict';

    angular
        .module('speech')
        .service('SpeechService', SpeechService);


    SpeechService.$inject = ['$state', '$rootScope'];

    function SpeechService($state, $rootScope) {

        var self = this;

        self.speak = function(texto){
            meSpeak.loadConfig("js/libs/mespeak/mespeak_config.json");
            meSpeak.loadVoice("js/libs/mespeak/voices/pt.json");
            meSpeak.speak(texto, {amplitude:80, speed:150, wordgap:5, pitch:30});
        };

        // COMMANDS
        self.commands = {};

        self.init = function() {
            self.clearResults();

            self.addCommand('dashboard', function(allSpeech) {
                $state.go("dashboard.start");
            });

            self.addCommand('cadastro aluno', function(allSpeech) {
                $state.go("customers.start");
            });

            self.start();
        };

        self.addCommand = function(phrase, callback) {
            var command = {};

            // Wrap annyang command in scope apply
            command[phrase] = function(args) {
                $rootScope.$apply(callback(args));
            };

            // Extend our commands list
            angular.extend(self.commands, command);

            // Add the commands to annyang
            annyang.addCommands(self.commands);
            console.debug('added command "' + phrase + '"', self.commands);
        };

        self.start = function() {
            annyang.setLanguage('pt-BR')
            annyang.addCommands(self.commands);
            annyang.debug(true);
            annyang.start();
        };

        self.addResult = function(result) {
            self.results.push({
                content: result,
                date: new Date()
            });
        };

        self.clearResults = function() {
            self.results = [];
        };
    }

});
