/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module dashboard
* @object module
*
* @created 2016-08-24 v12.1.15
* @updated 2016-08-24 v12.1.15
*
* @dependencies
*
* @description Modulo dashboard
*/

define(['annyang'],function () {
    'use strict';

    angular
        .module('speech', ['totvsHtmlFramework']);

}());
