var tests = [
            'ng-load',
            'angular-scroll',
            'angular-animate',
            'angular-bootstrap',
            'angular-hotkeys',
            'angular-i18n',
            'angular-nestable',
            'angular-resource',
            'angular-sanitize',
            'angular-ui-mask',
            'angular-ui-router',
            'angular-ui-select',
            'angular-cookies',

            'bootstrap-datepicker',
            'bootstrap-datepicker.pt-BR',
            'bootstrap-switch',

            'AngularJS-Toaster',

            'ngDraggable',

            'ngMask',

            'telerik.kendoui',
            'telerik.kendoui-en',
            'telerik.kendoui-es',
            'telerik.kendoui-pt',

            'oclazyload',

            'lightbox2'
];

for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
        if (/.*\.spec\.js$/.test(file)) {
            tests.push(file);
        }
    }
}

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/app/Edu/PortalEducacional/js',

    paths: {

        'jquery':                       '../../../../js/libs/jquery/dist/jquery',

        'bootstrap':                    '../../../../js/libs/bootstrap/dist/js/bootstrap',
        'bootstrap-datepicker':         '../../../../js/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker',
        'bootstrap-datepicker.pt-BR':   '../../../../js/libs/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min',
        'bootstrap-switch':             '../../../../js/libs/bootstrap-switch/dist/js/bootstrap-switch.min',

        'angular' :                     '../../../../js/libs/angular/angular',
        'angular-animate' :             '../../../../js/libs/angular-animate/angular-animate',
        'angular-bootstrap' :           '../../../../js/libs/angular-bootstrap/ui-bootstrap-tpls',
        'angular-hotkeys' :             '../../../../js/libs/angular-hotkeys/build/hotkeys',
        'angular-i18n' :                '../../../../js/libs/angular-i18n/angular-locale_pt-br',
        'angular-nestable' :            '../../../../js/libs/angular-nestable/src/angular-nestable',
        'angular-resource' :            '../../../../js/libs/angular-resource/angular-resource',
        'angular-sanitize' :            '../../../../js/libs/angular-sanitize/angular-sanitize',
        'angular-scroll':               '../../../../js/libs/angular-scroll/angular-scroll.min',
        'angular-ui-mask' :             '../../../../js/libs/angular-ui-mask/dist/mask',
        'angular-ui-router' :           '../../../../js/libs/angular-ui-router/release/angular-ui-router',
        'angular-ui-select' :           '../../../../js/libs/angular-ui-select/dist/select',
        'angular-cookies' :             '../../../../js/libs/angular-cookies/angular-cookies',
        'angular-mocks' :               '../../../../js/libs/angular-mocks/angular-mocks',

        'angularAMD':                   '../../../../js/libs/angularAMD/angularAMD',
        'ng-load':                      '../../../../js/libs/angularAMD/ngload',

        'AngularJS-Toaster':            '../../../../js/libs/AngularJS-Toaster/toaster',
        'ngDraggable':                  '../../../../js/libs/ngDraggable/ngDraggable',
        'ngMask':                      '../../../../js/libs/ngMask/dist/ngMask.min',

        'telerik.kendoui':              '../../../../js/libs/telerik.kendoui/js/kendo.all.min',
        'telerik.kendoui-pt':           '../../../../js/libs/telerik.kendoui/js/cultures/kendo.culture.pt.min',
        'telerik.kendoui-en':           '../../../../js/libs/telerik.kendoui/js/cultures/kendo.culture.en.min',
        'telerik.kendoui-es':           '../../../../js/libs/telerik.kendoui/js/cultures/kendo.culture.es.min',

        'annyang':                      '../../../../js/libs/annyang/annyang.min',

        'totvs-html-framework':         '../../../../js/libs/totvs-html-framework/totvs-html-framework',

        'oclazyload':                   '../../../../js/libs/oclazyload/dist/ocLazyLoad.require',

        'lightbox2':                    '../../../../js/libs/lightbox2/dist/js/lightbox.min'
    },
    shim: {
        'angular':                      {exports: 'angular'},

        'angularAMD':                   ['angular'],
        'ng-load':                      ['angularAMD'],

        'bootstrap':                    ['jquery'],
        'bootstrap-datepicker':         ['bootstrap'],
        'bootstrap-datepicker.pt-BR':   ['bootstrap', 'bootstrap-datepicker'],
        'bootstrap-switch':             ['bootstrap'],

        'angular':                      ['jquery', 'bootstrap'],
        'angular-scroll':               ['angular'],
        'angular-animate':              ['angular'],
        'angular-bootstrap':            ['angular'],
        'angular-hotkeys' :             ['angular'],
        'angular-i18n' :                ['angular'],
        'angular-nestable':             ['jquery', 'angular'],
        'angular-resource' :            ['angular'],
        'angular-sanitize' :            ['angular'],
        'angular-ui-mask' :             ['angular'],
        'angular-ui-router' :           ['angular'],
        'angular-ui-select' :           ['angular'],
        'angular-cookies' :             ['angular','angularAMD'],
        'angular-mocks':                {deps: ['angular'], exports: 'ngMock'},

        'AngularJS-Toaster':            ['angular', 'angular-animate', 'jquery'],
        'ngDraggable':                  ['angular'],

        'telerik.kendoui':              ['jquery', 'angular'],
        'telerik.kendoui-en':           ['telerik.kendoui'],
        'telerik.kendoui-es':           ['telerik.kendoui'],
        'telerik.kendoui-pt':           ['telerik.kendoui'],

        'totvs-app.module':             ['angular'],
        'totvs-app.route':              ['angular', 'totvs-app.module'],
        'totvs-app.config':             ['angular', 'totvs-app.module'],

        'totvs-html-framework':         ['angular', 'angular-i18n', 'angular-resource', 'telerik.kendoui'],
    },

    priority:   ['angular'],

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});
