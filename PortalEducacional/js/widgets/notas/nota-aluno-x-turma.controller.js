/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module eduWidgetsModule
* @name eduWidgetsNotaAlunoXTurma
* @object controller
*
* @created 02/03/2017 v12.1.16
* @updated 02/03/2017 v12.1.16
*
* @requires
*
* @dependencies
*
* @description
*/
define(['widgets/widget.module', 
        'widgets/widget.service',
        'widgets/widget.constants'],function(){

    'use strict';

    angular
        .module('eduWidgetsModule')
        .controller('EduWidgetsNotaAlunoXTurmaController', EduWidgetsNotaAlunoXTurmaController);

    EduWidgetsNotaAlunoXTurmaController.$inject = [
        '$scope',
        '$state',
        '$rootScope', 
        'eduWidgetsService', 
        'eduWidgetsConsts', 
        'i18nFilter'
    ];

    /**
     * Controller do widget de Notas do Aluno pela Média da Turma
     * Gráfico de linha utilizando a diretiva totvs-chart
     * @param {object} $scope           Escopo do controller
     * @param {object} $rootscope       Root scope do controller
     * @param {object} objWidgetService Serviços para widgets
     * @param {object} objWidgetConst   Constantes de definição dos widgets
     * @param {object} i18nFilter       Objeto para tradução dos campos
     */
    function EduWidgetsNotaAlunoXTurmaController($scope, $state, $rootScope, objWidgetService, objWidgetConst, i18nFilter) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;
        self.objWidget = {};            // Dados do widgets
        self.objDisciplinaList = [];    // Disciplinas para o combo
        self.selectedDisciplina = null; // Disciplina selecionada no combo
        self.objNotaList = [];          // Notas para as linhas do gráfico
        self.notasAlunoDisc = [];       // Notas do aluno na disciplina selecionada
        self.hasNota = false;           // Guarda se já possui alguma nota inserida
        self.valueMax = 0;              // Valor máximo para o eixo Y do gráfico
        self.isNotaConceitual = false;  // Verdadeiro se a disciplina usar nota conceitual
        self.permissaoDesempenho = false; //Utilizado para exibir o link para tela de Desempenho. True para exibir o link da tela para o usuário. 
        self.valueAxis = [];            // Parâmetros do eixo de valor do gráfico

        // *********************************************************************************
        // *** Public Methods
        // *********************************************************************************        
        self.getWidgetDados = getWidgetDados;
        self.setChartData = setChartData;
        self.changeDisciplina = changeDisciplina;
        self.goDesempenho = goDesempenho;
        self.setFnExport = setFnExport;

        /**
         * Valida se o usuário tem permissão ao menu
         */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {

            if ($rootScope.objPermissions) {
                if ($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_DESEMPENHO) {
                    self.permissaoDesempenho = true;
                } else {
                    self.permissaoDesempenho = false;
                }

                //destroy watch
                permissionsWatch();
            }
        });        


        // *********************************************************************************
        // *** Initialize
        // *********************************************************************************
        init();
        
        /**
         * @private
         * @function Função de inicialização do controller
         * @name init
         */
        function init() {
            self.getWidgetDados($scope);
        }

        function goDesempenho () {
            $state.go('desempenho.iniciar');
        }        

        // *********************************************************************************
        // *** Public functions
        // *********************************************************************************
        
        /**
         * @public
         * @function Obtém os dados do widget
         * @name getWidgetDados
         * @param {object} scope Escopo do controller
         */
        function getWidgetDados(scope) {
            self.objWidget = objWidgetService.getWidgetDataByController(scope);
            
            if (!self.objWidget || !self.objWidget.DadosWidget) {
                loadWidgetDados(objWidgetConst.EduWidgetsFuncionalidade.Notas,
                                objWidgetConst.EduWidgets.NotaAlunoXTurma.ID);
            }
            else {
                self.setChartData();
            }
        }
        
        /**
         * @public
         * @function Realiza a montagem do gráfico
         * @name setChartData
         */
        function setChartData() {
            setDisciplinas();
            setNotas();
        }
        
        /**
         * @public
         * @function Alteração da disciplina na lista
         * @name changeDisciplina
         * @param {int} selectedDisciplina Identificador da disciplina
         */
        function changeDisciplina() {
            setNotas();
        }
        
        /**
         * @public
         * @function Seta as funções do gráfico para exportar para PDF ou PNG
         * @name setFnExport
         * @param {object} objChart Objeto do gráfico
         */
        function setFnExport(objChart) {
            self.exportToPdf = objChart.pdf;
            self.exportToPng = objChart.png;
        }

        // *********************************************************************************
        // *** Private functions
        // *********************************************************************************
        
        /**
         * @private
         * @function Carrega os dados de um determinado widget
         * @name loadWidgetDados
         * @param {int} idFuncionalidade Identificador da Funcionalidade
         * @param {int} idWidget         Identificador do Widget
         */
        function loadWidgetDados(idFuncionalidade, idWidget) {
            objWidgetService.getWidgetDataByFactory(idFuncionalidade, idWidget, 
                                                   function (objResult) {
                if (objResult) {
                    self.objWidget = objResult;
                    self.setChartData();
                }
            });
        }
        
        /**
         * @private 
         * @function Carregam as disciplinas
         * @name setDisciplinas
         */
        function setDisciplinas() {
            var objWidgetDados = self.objWidget.DadosWidget;
            self.objDisciplinaList = [];

            if (angular.isDefined(objWidgetDados) && objWidgetDados !== null && angular.isArray(objWidgetDados.DISCIPLINAS)) {
                angular.forEach(objWidgetDados.DISCIPLINAS, function (rowDisciplina) {

                    var objDisciplina = {
                        label: rowDisciplina.DISCIPLINA,
                        value: rowDisciplina.CODDISC
                    };

                    self.objDisciplinaList.push(objDisciplina);
                });

            }

            if (!self.selectedDisciplina && self.objDisciplinaList.length > 0) {
                self.selectedDisciplina = self.objDisciplinaList[0].value;
            }
        }

        /**
         * @private
         * @function Calcula a média das notas de todos os alunos na disciplina selecionada, em todas as etapas.
         * @name getDadosDiscSelecionada
         */        
        function getDadosDiscSelecionada() {
            var NotasDisciplinasList = [];
            var resultadoFinal = [];

            self.notasAlunoDisc = $.grep(self.objWidget.DadosWidget.NOTAS, function( objNota ) {
                return objNota.CODDISC === self.selectedDisciplina &&
                       objNota.RA === self.objWidget.DadosWidget.ALUNO[0];
            });

            angular.forEach(self.notasAlunoDisc, function (rowEtapa) {
                
                NotasDisciplinasList = $.grep(self.objWidget.DadosWidget.NOTAS, function( objNota ) {
                    return objNota.CODDISC === self.selectedDisciplina &&
                           objNota.CODETAPA === rowEtapa.CODETAPA;
                });

                if (NotasDisciplinasList.length > 0) {

                    var notaTurmaItem = {
                        CODDISC: self.selectedDisciplina,
                        DISCIPLINA: NotasDisciplinasList[0].DISCIPLINA,
                        CODETAPA: rowEtapa.CODETAPA,
                        ETAPA: rowEtapa.ETAPA,
                        NOTA: null,
                        PONTDIST: NotasDisciplinasList[0].PONTDIST,
                        CODCONCEITO: NotasDisciplinasList[0].CODCONCEITO
                    };
                    
                    var notasSomadas = 0
                    for ( var i = 0, _len = NotasDisciplinasList.length; i < _len; i++ ) {
                        notasSomadas += NotasDisciplinasList[i]["NOTA"];
                    }

                    notaTurmaItem.NOTA = parseFloat(notasSomadas / NotasDisciplinasList.length).toFixed(2);
                    resultadoFinal.push(notaTurmaItem);
                };
            });

            return resultadoFinal;
        }

        /**
         * @private 
         * @function Carregam as notas
         * @name setNotas
         */
        function setNotas() {

            self.valueMax = 0;

            // Verifica se tem alguma nota lançada para uma etapa e disciplina
            if (angular.isArray(self.objDisciplinaList) && self.objDisciplinaList.length > 0) {
                
                var objWidgetDados = self.objWidget.DadosWidget;
                var objNotasTurma = getDadosDiscSelecionada();
                self.objNotaList = [];
                var objNotaServiceList = [];

                // Média das notas da turma
                if (angular.isDefined(objNotasTurma) && objNotasTurma !== null && angular.isArray(objNotasTurma)) {
                    if (!verificaNotaConceitual(objNotasTurma)) { // Se não for nota conceitual
                        self.objNotaList.push(setChartObject(objNotasTurma, 
                                                            i18nFilter('l-media-turma', [], 'js/widgets'), 
                                                            '#ff6800'));
                        
                        self.hasNota = true;
                    }
                }

                // Notas do aluno
                if (angular.isDefined(objWidgetDados) && objWidgetDados !== null && 
                    angular.isArray(objWidgetDados.NOTAS)) {

                    if (!verificaNotaConceitual(self.notasAlunoDisc)) { // Se não for nota conceitual
                        self.objNotaList.push(setChartObject(self.notasAlunoDisc,
                                                            i18nFilter('l-aluno', [], 'js/widgets'), 
                                                            '#a0a700'));
                        
                        self.hasNota = true;
                    }
                }
            }
            else {
                self.hasNota = false;
            }
        }
        
        /**
         * @private
         * @function Verifica se a nota para a disciplina é conceitual
         * @name verificaNotaConceitual
         * @param   {Array}   objNotasList Lista com as notas para a disciplina corrente
         * @returns {boolean} Verdadeiro se as notas forem conceituais
         */
        function verificaNotaConceitual(objNotaList) {
            if (angular.isArray(objNotaList) && objNotaList.length > 0) {
                if (objNotaList[0].CODCONCEITO !== null && objNotaList[0].CODCONCEITO.length > 0) {
                    self.isNotaConceitual = true;
                    return true;
                }
            }
            else {
                return false;
            }
        }
        
        /**
         * @private
         * @function Monta o objeto para o totvs-chart
         * @name setChartObject
         * @param   {Array}  objNotaList  Lista das notas
         * @param   {string} strLinhaNome Nome relativo a linha no gráfico
         * @param   {string} strCor       Hexadecimal da cor da linha no gráfico
         * @returns {object} Objeto do gráfico para o totvs-chart
         */
        function setChartObject(objNotaList, strLinhaNome, strCor) {
            var objChart = {};
            
            if (angular.isArray(objNotaList) && objNotaList.length > 0) {
                objChart = {
                    name: strLinhaNome,
                    color: strCor,
                    categoryField: 'etapa',
                    data: []
                };

                angular.forEach(objNotaList, function (objNota) {
                    
                    var objData = {
                        etapa: objNota.ETAPA,
                        value: (objNota.NOTA != null) ? objNota.NOTA : 0
                    };

                    // Valor máximo para o eixo Y do gráfico
                    if (objNota.PONTDIST > self.valueMax) {
                        self.valueMax = objNota.PONTDIST;
                    }
                    
                    objChart.data.push(objData);
                });

                self.valueAxis = {
                    min: 0,
                    max: self.valueMax + (self.valueMax * 0.2),
                    line: {
                        visible: true
                    },
                    labels: {
                        visible: true
                    },
                    minorGridLines: {
                        visible: true,
                        step: 1
                    },
                    majorGridLines: {
                        visible: true
                    }
                };                
                
                // Valor da nota acima da coluna do gráfico
                objChart.labels = {
                    visible: true,
                    template: '#:value#'
                };
            }
            
            return objChart;
        }
    }
});
