/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.16
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['widgets/widget.module',
    'widgets/widget.service',
    'aluno/financeiro/financeiro.factory',
    'aluno/financeiro/financeiro.service',
    'widgets/widget.constants',
    'utils/reports/edu-relatorio.service',
    'utils/edu-utils.factory',
    'utils/edu-enums.constants'], function () {

        'use strict';

        angular
            .module('eduWidgetsModule')
            .controller('eduWidgetsProxVencimentoController', EduWidgetsProxVencimentoController);

        EduWidgetsProxVencimentoController.$inject = [
            '$scope',
            'eduWidgetsService',
            'eduWidgetsConsts',
            'i18nFilter',
            '$state',
            'EduFinanceiroFactory',
            'EduFinanceiroService',
            'eduRelatorioService',
            'eduUtilsFactory',
            'eduEnumsConsts',
            '$window',
            'totvs.app-notification.Service',
            '$filter'
        ];

        function EduWidgetsProxVencimentoController(
            $scope,
            objWidgetService,
            objWidgetConst,
            i18nFilter,
            $state,
            EduFinanceiroFactory,
            EduFinanceiroService,
            eduRelatorioService,
            eduUtilsFactory,
            eduEnumsConsts,
            $window,
            appNotificationService,
            $filter) {

            // variáveis
            var self = this,
            paramsEdu = null;
            self.objWidget = {};
            self.dadosBoleto = {};
            self.objInfoBoleto = [];
            self.hasValue = false;
            self.PermitePagCartaoCredito = false;
            self.IntegradoProtheus = false;
            self.LegadoIntegracaoProtheus = false;
            self.TipoGeradorRelatorio = false;
            self.IdRelatBoletosDotNetExe = 0;
            self.IdRelatBoletosDelphiExe = 0;
            self.exibeBtnBoleto = false;
            self.exibeBtnCartao = false;
            self.exibeLinkFinanceiro = true;

            // metódos
            self.getWidgetDados = getWidgetDados;
            self.imprimeBoleto = imprimeBoleto;
            self.exibirDadosPagCartao = exibirDadosPagCartao;
            self.goFinanceiro = goFinanceiro;

            // inicializador
            init();

            /**
             * Inicializa o controller.
             *
             */
            function init() {
                getWidgetDados($scope);
                carregarParametrosEdu();
                verificaExibicaoLink();
            }

            /**
             * Recupera os dados do Widget de acordo com o ID do widget.
             *
             * @param {any} scope - scopo da tela a qual o widget estará
             */
            function getWidgetDados(scope) {

                self.objWidget = objWidgetService.getWidgetDataByController(scope);
                if (!self.objWidget) {
                    loadWidgetDados(objWidgetConst.EduWidgetsFuncionalidade.Disciplinas,
                        objWidgetConst.EduWidgets.ProximoVencimento.ID);
                }
                if (self.objWidget.DadosWidget !== null && self.objWidget.DadosWidget.SBoletos.length > 0) {
                    self.dadosBoleto = self.objWidget.DadosWidget.SBoletos[0];
                }

                self.hasValue = self.objWidget.DadosWidget !== null && self.objWidget.DadosWidget.SBoletos.length > 0;
            }

            // Realiza o carregamento dos dados do widget
            function loadWidgetDados(idFuncionalidade, idWidget) {
                objWidgetService.getWidgetDataByFactory(
                    idFuncionalidade,
                    idWidget,
                    function (objResult) {
                        if (objResult) {
                            self.objWidget = objResult;
                        }
                    }
                );
            }

            /**
             * Imprime o boleto de acordo com o parâmetro.
             *
             * @param {any} idBoleto - identificador do boleto
             * @param {any} nossoNumero - nosso número
             * @param {any} dtVencimento - data de vencimento
             */
            function imprimeBoleto(idBoleto, nossoNumero, dtVencimento) {
                EduFinanceiroFactory.geraSegundaVia(idBoleto, nossoNumero, dtVencimento, function (result) {
                    if (result && result[0]['Bytes']) {
                        angular.forEach(result, function (value) {
                            self.objInfoBoleto.push(value);
                        });

                        //Para boleto registrado, abre o boleto em PDF normalmente.
                        if (self.objInfoBoleto[0].BOLETOREGISTRADO === true ||
                            self.ShowBoletoNaoRegistradoPortal) {

                            eduRelatorioService.exibirOuSalvarPDF(result[0].Bytes);
                        } else if (self.objInfoBoleto[0].PERMITEREGONLINE === true) {

                            //O usuário é informado que será redirecionado para o site do banco,
                            //e confirma se deseja continuar ou não.
                            appNotificationService.question({
                                title: $filter('i18n')('l-boleto', [], 'js/aluno/financeiro/lancamentos'),
                                text: $filter('i18n')('l-redirecionarbanco', [], 'js/aluno/financeiro/lancamentos'),
                                size: 'sm',
                                cancelLabel: 'l-no',
                                confirmLabel: 'l-yes',
                                callback: function (opcaoEscolhida) {
                                    if (opcaoEscolhida) {
                                        $window.open(self.objInfoBoleto[0].URLREGONLINE);
                                    }
                                }
                            });
                        }
                    }
                });
            }

            /**
             * Mostra o modal com os dados de pagamento para cartão de crédito
             *
             * @param {any} idBoleto - identificador do boleto
             */
            function exibirDadosPagCartao(idBoleto) {
                EduFinanceiroFactory.exibirDadosPagCartao(idBoleto);
            }

            /**
             * Define se serão exibidos os botões de boleto e cartão de crédito
             *
             */
            function defineRegraBtnPagto() {

                self.exibeBtnBoleto = ((self.TipoGeradorRelatorio === eduEnumsConsts.TipoGeradorRelatorio.DotNet && self.IdRelatBoletosDotNetExe != null) ||
                    (self.TipoGeradorRelatorio === eduEnumsConsts.TipoGeradorRelatorio.Delphi && self.IdRelatBoletosDelphiExe != null));

                self.exibeBtnCartao = EduFinanceiroService.permitePagamentoCartao(self.dadosBoleto, paramsEdu);
            }

            /**
             * Carrega os parâmetros do educacional.
             *
             */
            function carregarParametrosEdu() {
                eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (params) {
                    paramsEdu = params;
                    self.PermitePagCartaoCredito = params.PermitePagCartaoCredito;
                    self.IntegradoProtheus = params.IntegradoProtheus;
                    self.LegadoIntegracaoProtheus = params.LegadoIntegracaoProtheus;
                    self.TipoGeradorRelatorio = params.TipoGeradorRelatorio;
                    self.IdRelatBoletosDotNetExe = params.IdRelatBoletosDotNetExe;
                    self.IdRelatBoletosDelphiExe = params.IdRelatBoletosDelphiExe;
                    // define a regra de acordo com os parâmetros
                    defineRegraBtnPagto();
                });
            }

            /**
             * Função que redireciona para a tela de financeiro.
             *
             */
            function goFinanceiro() {

                var params = {
                    status: 0
                };

                $state.go('financeiro.start', params);
            }

            /**
             * Valida exibição do link 'Ver financeiro'
              */
            function verificaExibicaoLink() {
                if ($state.current.name === 'financeiro.start') {
                    self.exibeLinkFinanceiro = false;
                }
            }
        }
    });
