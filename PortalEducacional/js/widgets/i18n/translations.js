[
    {
        "l-titulo": {
            "pt": "Widgets",
			"en": "Widgets",
			"es": "Widgets"
        },
        "l-notas-x-turma": {
            "pt": "Notas por etapa",
			"en": "Score by stage",
			"es": "Notas por etapa"
        },
        "l-notas-x-turma-avaliacao": {
            "pt": "Notas por avaliação",
			"en": "Score by evaluation",
			"es": "Notas por evaluación"
        },
        "l-disciplina-em-curso": {
            "pt": "Disciplinas em Curso",
			"en": "Course Subjects in Course",
			"es": "Materias en curso"
        },
        "l-media-turma": {
            "pt": "Média da turma",
			"en": "Average score of the class",
			"es": "Promedio del grupo"
        },
        "l-aluno": {
            "pt": "Aluno",
			"en": "Student",
			"es": "Alumno"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Course Subject",
			"es": "Materia"
        },
        "l-etapa": {
            "pt": "Etapa",
			"en": "Stage",
			"es": "Etapa"
        },
        "l-horarios": {
            "pt": "Horários",
			"en": "Schedules",
			"es": "Horarios"
        },
        "l-exportar": {
            "pt": "Exportar",
			"en": "Export",
			"es": "Exportar"
        },
        "l-go-quadro-horario": {
            "pt": "ver quadro de horários",
			"en": "see timetable",
			"es": "ver cuadro de horarios"
        },
        "l-Nenhuma-Nota-Inserida": {
            "pt": "Nenhuma nota inserida.",
			"en": "No score added.",
			"es": "No se incluyó ninguna nota."
        },
        "l-Nenhuma-Falta-Inserida": {
            "pt": "Nenhuma falta inserida.",
			"en": "No absence added.",
			"es": "No se incluyó ninguna falta."
        },
        "l-nenhuma-info-disponivel": {
            "pt": "Nenhuma informação disponível.",
			"en": "No information available.",
			"es": "Ninguna información disponible."
        },
        "l-Nota-tipo-concentual": {
            "pt": "Não é possível gerar o gráfico para notas conceituais.",
			"en": "Unable to generate the chart for conceptual score.",
			"es": "No es posible generar el gráfico para notas conceptuales."
        },
        "l-de": {
            "pt": "de",
			"en": "from",
			"es": "de"
        },
        "l-acontece-hoje": {
            "pt": "Acontecendo hoje",
			"en": "Happening today",
			"es": "Ocurriendo hoy"
        },
        "l-go-calendario": {
            "pt": "ver calendário",
			"en": "see calendar",
			"es": "ver calendario"
        },
         "l-go-financeiro": {
            "pt": "ver financeiro",
			"en": "see Financials",
			"es": "ver financiero"
        },
        "l-dia-todo": {
            "pt": "Dia todo",
			"en": "The whole day",
			"es": "Día todo"
        },
        "l-ate": {
            "pt": "até",
			"en": "to",
			"es": "a"
        },
        "l-proximo-vencimento": {
            "pt": "Próximo vencimento de boleto",
			"en": "Bank slip next expiration",
			"es": "Próximo vencimiento de la boleta"
        },
        "l-gerar-boleto": {
            "pt": "Boleto",
			"en": "Bank slip",
			"es": "Boleta"
        },
        "l-cartao": {
            "pt": "Cartão",
			"en": "Card",
			"es": "Tarjeta"
        },
        "l-valor": {
            "pt": "Valor",
			"en": "Value",
			"es": "Valor"
        },
        "l-vencimento": {
            "pt": "Vencimento",
			"en": "Expiration",
			"es": "Vencimiento"
        },
        "l-proximo-vencimento-boleto": {
            "pt": "Próximo vencimento de boleto",
			"en": "Bank slip next expiration",
			"es": "Próximo vencimiento de la boleta"
        },
        "l-frequencia": {
            "pt": "Frequência",
			"en": "Frequency",
			"es": "Frecuencia"
        },
        "l-limite-faltas": {
            "pt": "Limite de faltas",
			"en": "Limit of absences",
			"es": "Límite de faltas"
        },
        "l-qtd-faltas": {
            "pt": "Qtd. de faltas",
			"en": "Number of absences",
			"es": "Cant. de faltas"
        },
        "l-desempenho": {
            "pt": "ver desempenho",
			"en": "see performance",
			"es": "ver desempeño"
        },        
        "l-percentual-faltas": {
            "pt": "Percentual de faltas",
			"en": "Percentage of absences",
			"es": "Porcentaje de faltas"
        },
        "l-go-aulas-do-dia": {
            "pt": "Detalhar aulas do dia",
			"en": "Detalhar aulas do dia",
			"es": "Detalhar aulas do dia"
        }        
    }
]
