/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.16
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['widgets/widget.module',
        'widgets/widget.service',
        'widgets/widget.constants'],function () {

    'use strict';

    angular
        .module('eduWidgetsModule')
        .controller('eduWidgetsDiscEmCursoController', EduWidgetsDiscEmCursoController);

    EduWidgetsDiscEmCursoController.$inject = [
        '$scope',
        'eduWidgetsService',
        'eduWidgetsConsts',
        'i18nFilter',
        'MatriculaDisciplinaService',
        'TotvsDesktopContextoCursoFactory',
        '$state'
    ];

    function EduWidgetsDiscEmCursoController(
        $scope,
        objWidgetService,
        objWidgetConst,
        i18nFilter,
        MatriculaDisciplinaService,
        TotvsDesktopContextoCursoFactory,
        $state) {

        // variáveis
        var self = this;
        self.objWidget = {};
        self.hasValue = false;

        // metódos
        self.getWidgetDados = getWidgetDados;
        self.setFnExport = setFnExport;
        self.getTemplateHorario = getTemplateHorario;
        self.abrirDetalhesDisciplina = abrirDetalhesDisciplina;
        self.goQuadroHorario = goQuadroHorario;

        // inicializador
        init();

        // public
        function init() {
            getWidgetDados($scope);
        }

        function getWidgetDados(scope) {
            self.objWidget = objWidgetService.getWidgetDataByController(scope);
            if (!self.objWidget) {
                loadWidgetDados(objWidgetConst.EduWidgetsFuncionalidade.Disciplinas,
                                objWidgetConst.EduWidgets.DisciplinaEmCurso.ID);
            }

            self.hasValue = self.objWidget.DadosWidget !== null && self.objWidget.DadosWidget.length > 0;
        }

        function setFnExport(objChart) {
            self.exportToPdf = objChart.pdf;
            self.exportToPng = objChart.png;
        }

        function getTemplateHorario (item) {
            return item.join('/');
        }

        function abrirDetalhesDisciplina(idTurmaDisc) {
            var contexto, codColigada, ra;

            contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
            codColigada = contexto.cursoSelecionado.CODCOLIGADA;
            ra = contexto.cursoSelecionado.RA;

            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(codColigada, idTurmaDisc, ra);
        }

        function goQuadroHorario () {
            $state.go('quadro-horario.iniciar');
        }

        // private
        function loadWidgetDados(idFuncionalidade, idWidget) {
            objWidgetService.getWidgetDataByFactory(
                idFuncionalidade,
                idWidget,
                function (objResult) {
                    if (objResult) {
                        self.objWidget = objResult;
                    }
                }
            );
        }
    }
});
