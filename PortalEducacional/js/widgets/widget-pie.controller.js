/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module widget
* @name WidgetPieController
* @object controller
*
* @created 2016-08-24 v12.1.15
* @updated 2016-08-24 v12.1.15
*
* @requires
*
* @dependencies
*
* @description
*/
define(['widgets/widget.module'],function(){

    'use strict';

    angular
        .module('eduWidgetsModule')
        .controller('WidgetPieController', WidgetPieController);

    WidgetPieController.$inject = ['$scope'];

    function WidgetPieController($scope) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        self.series = [];
        self.setFnExport = setFnExport;

        init();

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            var parent = $scope.$parent;
            
            self.series = [{
                data: [
                    {category: "Exemplo #1", value: 150, color: "#1E90FF"},
                    {category: "Exemplo #2", value: 300, color: "#87CEFA"}
                ]
            }];
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function setFnExport(obj) {
            self.exportToPdf = obj.pdf;
            self.exportToPng = obj.png;
        }
    }


});
