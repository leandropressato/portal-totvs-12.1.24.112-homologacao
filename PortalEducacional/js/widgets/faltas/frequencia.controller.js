/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.18
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module eduWidgetsModule
* @name eduWidgetsFrequenciaController
* @object controller
*
* @created 10/07/2017 v12.1.18
* @updated 10/07/2017 v12.1.18
*
* @requires
*
* @dependencies
*
* @description
*/
define(['widgets/widget.module', 
        'widgets/widget.service',
        'widgets/widget.constants'],function(){

    'use strict';

    angular
        .module('eduWidgetsModule')
        .controller('EduWidgetsFrequenciaController', EduWidgetsFrequenciaController);

    EduWidgetsFrequenciaController.$inject = ['$scope', 'eduWidgetsService', 'eduWidgetsConsts', 'i18nFilter'];

    /**
     * @public
     * @function Controller do widget de Frenquência (limite de faltas)
     * @name EduWidgetsFrequenciaController
     * Gráfico de rosca utilizando a diretiva totvs-chart
     * @param {object} $scope           Escopo do controller
     * @param {object} objWidgetService Serviços para widgets
     * @param {object} objWidgetConst   Constantes de definição dos widgets
     * @param {object} i18nFilter       Objeto para tradução dos campos
     */
    function EduWidgetsFrequenciaController($scope, objWidgetService, objWidgetConst, i18nFilter) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;
        self.objWidget = {};            // Dados do widgets
        self.objDisciplinaList = [];    // Disciplinas para o combo
        self.objFalta = {};             // Falta para o gráfico
        self.selectedDisciplina = null; // Disciplina selecionada no combo
        self.hasFaltas = false;         // Guarda se já possui alguma falta inserida

        // *********************************************************************************
        // *** Public Methods
        // *********************************************************************************        
        self.getWidgetDados = getWidgetDados;
        self.setChartData = setChartData;
        self.changeDisciplina = changeDisciplina;
        self.setFnExport = setFnExport;

        // *********************************************************************************
        // *** Initialize
        // *********************************************************************************
        init();

        /**
         * @private
         * @function Função de inicialização do controller
         * @name init
         */
        function init() {
            self.getWidgetDados($scope);
        }

        // *********************************************************************************
        // *** Public functions
        // *********************************************************************************

        /**
         * @public
         * @function Obtém os dados do widget
         * @name getWidgetDados
         * @param {object} scope Escopo do controller
         */
        function getWidgetDados(scope) {
            self.objWidget = objWidgetService.getWidgetDataByController(scope);

            if (!self.objWidget || !self.objWidget.DadosWidget) {
                loadWidgetDados(objWidgetConst.EduWidgetsFuncionalidade.Faltas,
                                objWidgetConst.EduWidgets.FrequenciaLimiteFaltas.ID);
            }
            else {
                self.setChartData();
            }
        }

        /**
         * @public
         * @function Realiza a montagem do gráfico
         * @name setChartData
         */
        function setChartData() {
            setDisciplinas();
            setFaltas();
        }

        /**
         * @public
         * @function Alteração da disciplina na lista
         * @name changeDisciplina
         */
        function changeDisciplina() {
            setFaltas();
        }

        /**
         * @public
         * @function Seta as funções do gráfico para exportar para PDF ou PNG
         * @name setFnExport
         * @param {object} objChart Objeto do gráfico
         */
        function setFnExport(objChart) {
            self.exportToPdf = objChart.pdf;
            self.exportToPng = objChart.png;
        }

        // *********************************************************************************
        // *** Private functions
        // *********************************************************************************

        /**
         * @private
         * @function Seta o limite de faltas
         * @name setLimiteFaltas
         */
        function setLimiteFaltas() {
            if (angular.isArray(self.objWidget.DadosWidget) && self.objWidget.DadosWidget.length > 0) {
                self.limiteFaltas = self.objWidget.DadosWidget[0].LIMITEFALTAS;    
            }
        }
        
        /**
         * @private
         * @function Carrega os dados de um determinado widget
         * @name loadWidgetDados
         * @param {int} idFuncionalidade Identificador da Funcionalidade
         * @param {int} idWidget         Identificador do Widget
         */
        function loadWidgetDados(idFuncionalidade, idWidget) {
            objWidgetService.getWidgetDataByFactory(idFuncionalidade, idWidget, 
                                                    function (objResult) {
                if (objResult) {
                    self.objWidget = objResult;
                    self.setChartData();
                }
            });
        }

        /**
         * @private 
         * @function Carregam as disciplinas
         * @name setDisciplinas
         */
        function setDisciplinas() {
            var objWidgetDados = self.objWidget.DadosWidget;
            self.objDisciplinaList = [];

            if (angular.isDefined(objWidgetDados) && objWidgetDados !== null && angular.isArray(objWidgetDados)) {
                angular.forEach(objWidgetDados, function (row) {

                    var objDisciplina = {
                        label: row.NOME,
                        value: row.CODDISC,
                        id: row.IDTURMADISC
                    };

                    self.objDisciplinaList.push(objDisciplina);
                });

            }

            if (!self.selectedDisciplina && self.objDisciplinaList.length > 0) {
                self.selectedDisciplina = self.objDisciplinaList[0];
            }
        }

        /**
         * @private 
         * @function Carregam as notas
         * @name setFaltas
         */
        function setFaltas() {
            self.hasFaltas = false;

            // Verifica se tem alguma nota lançada para uma etapa e disciplina
            if (angular.isArray(self.objDisciplinaList) && self.objDisciplinaList.length > 0) {

                var objWidgetDados = self.objWidget.DadosWidget;
                self.objFaltasList = [];
                var objFaltaServiceList = [];

                // Média das notas da turma
                if (angular.isDefined(objWidgetDados) && objWidgetDados !== null && angular.isArray(objWidgetDados)) {
                    // Obtém as notas da disciplina selecionada
                    for (var i = 0; i < objWidgetDados.length; i++) {
                        if (objWidgetDados[i].CODDISC === self.selectedDisciplina.value) {
                            self.objFaltasList.push(setChartObject(objWidgetDados[i]));
                            break;
                        }
                    }
                }
            }
        }

        /**
         * @private
         * @function Monta o objeto para o totvs-chart
         * @name setChartObject
         * @param   {Array}  objFalta  Objeto com as faltas
         * @returns {object} Objeto do gráfico para o totvs-chart
         */
        function setChartObject(objFalta) {
            self.objFalta = objFalta;
            var objChart = {};
            
            if (objFalta.QTDFALTAS > 0) {
                self.hasFaltas = true;
                var strCor = getCorGrafico(objFalta.NIVELLIMITE)

                objChart = {
                    legend: {
                        visible: false
                    },
                    labels: {
                        visible: true,
                        background: "transparent",
                        position: "outsideEnd",
                        template: function(e) {
                            if (e.dataItem.category !== '') {
                                return e.dataItem.category + ': ' + e.dataItem.value + '%';
                            }
                            else
                                return '';
                        }
                    },
                    tooltip: {
                        visible: false
                    },
                    data: [
                        {
                            category: i18nFilter('l-percentual-faltas', [], 'js/widgets'),
                            color: strCor,
                            value: (objFalta.PERCFALTAS != null) ? objFalta.PERCFALTAS : 0
                        },
                        {
                            category: '',
                            color: "#d3d3d3", // cinza
                            value: 100 - objFalta.PERCFALTAS
                        }
                    ]
                }; 
            }
            
            return objChart;  
        }
        
        /**
         * @private
         * @function Obtém a cor de acordo com o limite de faltas
         * @name getCorGrafico
         * @param   {int}    intNivelLimite Nível do limite de faltas
         * @returns {string} Hexadecimal da cor
         */
        function getCorGrafico(intNivelLimite) {
            var strCor = "#1e90ff"; // azul e abaixo de 80% do limite de faltas
            
            if (intNivelLimite === 0) { // ultrapassou limite de faltas
                strCor = "#d9534f"; // vermelho
            }
            else if (intNivelLimite === 1) { // 80% do limite de faltas
                strCor = "#f0ad4e"; // laranja
            }
            
            return strCor;
        }
    }
});
