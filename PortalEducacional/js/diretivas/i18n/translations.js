[
    {
        "l-files selected": {
            "pt": "arquivos selecionados",
			"en": "selected files",
			"es": "archivos seleccionados"
        },
        "l-scheduler-messages.today": {
            "pt": "Hoje",
			"en": "Today",
			"es": "Hoy"
        },
        "l-scheduler-messages.save": {
            "pt": "Salvar",
			"en": "Save",
			"es": "Grabar"
        },
        "l-scheduler-messages.cancel": {
            "pt": "Cancelar",
			"en": "Cancel",
			"es": "Anular"
        },
        "l-scheduler-messages.destroy": {
            "pt": "Apagar",
			"en": "Delete",
			"es": "Cortar"
        },
        "l-scheduler-messages.event": {
            "pt": "Evento",
			"en": "Event",
			"es": "Evento"
        },
        "l-scheduler-messages.date": {
            "pt": "Data",
			"en": "Date",
			"es": "Fecha"
        },
        "l-scheduler-messages.time": {
            "pt": "Hora",
			"en": "Time",
			"es": "Hora"
        },
        "l-scheduler-messages.allDay": {
            "pt": "Dia todo",
			"en": "The whole day",
			"es": "Día todo"
        },
        "l-scheduler-messages.showFullDay": {
            "pt": "Exibir dia completo",
			"en": "Display the whole day",
			"es": "Mostrar día completo"
        },
        "l-scheduler-messages.showWorkDay": {
            "pt": "Exibir horário comercial",
			"en": "Display business hours",
			"es": "Mostrar horario comercial"
        },
        "l-scheduler-messages.defaultRowText": {
            "pt": "Todos os eventos",
			"en": "All events",
			"es": "Todos los eventos"
        },
        "l-scheduler-messages.deleteWindowTitle": {
            "pt": "Excluir Evento",
			"en": "Delete Event",
			"es": "Mostrar evento"
        },
        "l-scheduler-messages.ariaEventLabel": {
            "pt": "{0} em {1:D} às {2:t}",
			"en": "{0} on {1:D} at {2:t}",
			"es": "{0} en {1:D} a las {2:t}"
        },
        "l-scheduler-messages.ariaSlotLabel": {
            "pt": "Selecionado de {0:t} a {1:t}",
			"en": "Selected from {0:t} at {1:t}",
			"es": "Seleccionado de {0:t} a {1:t}"
        },
        "l-scheduler-messages.views.day": {
            "pt": "Dia",
			"en": "Day",
			"es": "Día"
        },
        "l-scheduler-messages.views.week": {
            "pt": "Semana",
			"en": "Week",
			"es": "Semana"
        },
        "l-scheduler-messages.views.month": {
            "pt": "Mês",
			"en": "Month",
			"es": "Mes"
        },
        "l-scheduler-messages.views.timeline": {
            "pt": "Linha do Tempo",
			"en": "Timeline",
			"es": "Línea del tiempo"
        },
        "l-scheduler-messages.views.workWeek": {
            "pt": "Semana de Trabalho",
			"en": "Work Week",
			"es": "Semana de trabajo"
        },
        "l-scheduler-messages.editable.confirmation": {
            "pt": "Tem certeza de que deseja excluir este evento?",
			"en": "Are you sure you want to delete this event?",
			"es": "¿Está seguro de que desea borrar este evento?"
        },
        "l-scheduler-messages.editor.allDayEvent": {
            "pt": "Evento o dia todo",
			"en": "Event the whole day",
			"es": "Evento el día todo"
        },
        "l-scheduler-messages.editor.description": {
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "l-scheduler-messages.editor.editorTitle": {
            "pt": "Editar Evento",
			"en": "Edit Event",
			"es": "Editar evento"
        },
        "l-scheduler-messages.editor.end": {
            "pt": "Fim",
			"en": "End",
			"es": "Final"
        },
        "l-scheduler-messages.editor.endTimezone": {
            "pt": "Evento o dia todo",
			"en": "Event the whole day",
			"es": "Evento el día todo"
        },
        "l-scheduler-messages.editor.repeat": {
            "pt": "Repetir",
			"en": "Repeat",
			"es": "Repetir"
        },
        "l-scheduler-messages.editor.separateTimezones": {
            "pt": "Definir diferentes fusos horários de início e fim",
			"en": "Define different timezones from beginning to end",
			"es": "Definir diferentes husos horarios de inicio y final"
        },
        "l-scheduler-messages.editor.start": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-scheduler-messages.editor.startTimezone": {
            "pt": "Data inicial fuso horário",
			"en": "Timezone start date",
			"es": "Fecha inicial huso horario"
        },
        "l-scheduler-messages.editor.timezone": {
            "pt": "Fuso horário do evento",
			"en": "Event timezone",
			"es": "Huso horario del evento"
        },
        "l-scheduler-messages.editor.timezoneEditorButton": {
            "pt": "Fuso Horário",
			"en": "Timezone",
			"es": "Huso horario"
        },
        "l-scheduler-messages.editor.timezoneEditorTitle": {
            "pt": "Fusos horários",
			"en": "Timezones",
			"es": "Husos horarios"
        },
        "l-scheduler-messages.editor.title": {
            "pt": "Título",
			"en": "Title",
			"es": "Título"
        },
        "l-scheduler-messages.recurrenceEditor.daily.interval": {
            "pt": "dia(s)",
			"en": "day(s)",
			"es": "día(s)"
        },
        "l-scheduler-messages.recurrenceEditor.daily.repeatEvery": {
            "pt": "Repetir a cada:",
			"en": "Repeat at each:",
			"es": "Repetir cada:"
        },
        "l-scheduler-messages.recurrenceEditor.end.after": {
            "pt": "Após",
			"en": "After",
			"es": "Después"
        },
        "l-scheduler-messages.recurrenceEditor.end.occurrence": {
            "pt": "ocorrências",
			"en": "occurrences",
			"es": "ocurrencias"
        },
        "l-scheduler-messages.recurrenceEditor.end.label": {
            "pt": "Fim",
			"en": "End",
			"es": "Final"
        },
        "l-scheduler-messages.recurrenceEditor.end.never": {
            "pt": "Nunca",
			"en": "Never",
			"es": "Nunca"
        },
        "l-scheduler-messages.recurrenceEditor.end.mobileLabel": {
            "pt": "Termina",
			"en": "Finalizes",
			"es": "Finaliza"
        },
        "l-scheduler-messages.recurrenceEditor.end.on": {
            "pt": "Em",
			"en": "On",
			"es": "En"
        },
        "l-scheduler-messages.recurrenceEditor.frequencies.daily": {
            "pt": "Diariamente",
			"en": "Daily",
			"es": "Diariamente"
        },
        "l-scheduler-messages.recurrenceEditor.frequencies.monthly": {
            "pt": "Mensal",
			"en": "Monthly",
			"es": "Mensual"
        },
        "l-scheduler-messages.recurrenceEditor.frequencies.never": {
            "pt": "Nunca",
			"en": "Never",
			"es": "Nunca"
        },
        "l-scheduler-messages.recurrenceEditor.frequencies.weekly": {
            "pt": "Semanalmente",
			"en": "Weekly",
			"es": "Semanalmente"
        },
        "l-scheduler-messages.recurrenceEditor.frequencies.yearly": {
            "pt": "Anualmente",
			"en": "Annually",
			"es": "Anualmente"
        },
        "l-scheduler-messages.recurrenceEditor.monthly.day": {
            "pt": "Dia ",
			"en": "Day",
			"es": "Día "
        },
        "l-scheduler-messages.recurrenceEditor.monthly.interval": {
            "pt": "mês(es)",
			"en": "month(s)",
			"es": "mes(es)"
        },
        "l-scheduler-messages.recurrenceEditor.monthly.repeatEvery": {
            "pt": "Repetir a cada: ",
			"en": "Repeat at each:",
			"es": "Repetir cada: "
        },
        "l-scheduler-messages.recurrenceEditor.monthly.repeatOn": {
            "pt": "Repetir em: ",
			"en": "Repeat in:",
			"es": "Repetir en: "
        },
        "l-scheduler-messages.recurrenceEditor.offsetPositions.first": {
            "pt": "primeiro",
			"en": "first",
			"es": "primero"
        },
        "l-scheduler-messages.recurrenceEditor.offsetPositions.second": {
            "pt": "segundo",
			"en": "second",
			"es": "segundo"
        },
        "l-scheduler-messages.recurrenceEditor.offsetPositions.third": {
            "pt": "terceiro",
			"en": "third",
			"es": "tercero"
        },
        "l-scheduler-messages.recurrenceEditor.offsetPositions.fourth": {
            "pt": "quarto",
			"en": "fourth",
			"es": "cuarto"
        },
        "l-scheduler-messages.recurrenceEditor.offsetPositions.last": {
            "pt": "último",
			"en": "last",
			"es": "último"
        },
        "l-scheduler-messages.recurrenceEditor.weekly.interval": {
            "pt": " semana(s)",
			"en": " week(s)",
			"es": " semana(s)"
        },
        "l-scheduler-messages.recurrenceEditor.weekly.repeatEvery": {
            "pt": "Repetir a cada: ",
			"en": "Repeat at each:",
			"es": "Repetir a cada: "
        },
        "l-scheduler-messages.recurrenceEditor.weekly.repeatOn": {
            "pt": "Repetir em: ",
			"en": "Repeat in:",
			"es": "Repetir en: "
        },
        "l-scheduler-messages.recurrenceEditor.weekdays.day": {
            "pt": "dia",
			"en": "day",
			"es": "día"
        },
        "l-scheduler-messages.recurrenceEditor.weekdays.weekday": {
            "pt": "semana",
			"en": "week",
			"es": "semana"
        },
        "l-scheduler-messages.recurrenceEditor.weekdays.weekend": {
            "pt": "final de semana",
			"en": "weekend",
			"es": "final de semana"
        },
        "l-scheduler-messages.recurrenceEditor.yearly.of": {
            "pt": "de ",
			"en": "from",
			"es": "de "
        },
        "l-scheduler-messages.recurrenceEditor.yearly.repeatEvery": {
            "pt": "Repetir a cada: ",
			"en": "Repeat at each:",
			"es": "Repetir cada: "
        },
        "l-scheduler-messages.recurrenceEditor.yearly.repeatOn": {
            "pt": "Repetir em: ",
			"en": "Repeat in:",
			"es": "Repetir en: "
        },
        "l-scheduler-messages.recurrenceEditor.yearly.interval": {
            "pt": " ano(s)",
			"en": "year(s)",
			"es": " año(s)"
        },
        "l-scheduler-messages.recurrenceMessages.deleteRecurring": {
            "pt": "Deseja excluir somente esta ocorrência de evento ou toda a série?",
			"en": "Do you want to delete only for this event occurrence or the whole series?",
			"es": "¿Solo desea borrar esta ocurrencia de evento o toda la serie?"
        },
        "l-scheduler-messages.recurrenceMessages.deleteWindowOccurrence": {
            "pt": "Excluir ocorrência atual",
			"en": "Delete current occurrence",
			"es": "Borrar ocurrencia actual"
        },
        "l-scheduler-messages.recurrenceMessages.deleteWindowSeries": {
            "pt": "Excluir a série",
			"en": "Delete series",
			"es": "Borrar la serie"
        },
        "l-scheduler-messages.recurrenceMessages.deleteWindowTitle": {
            "pt": "Excluir item recorrente",
			"en": "Delete recurrent item",
			"es": "Borrar ítem recurrente"
        },
        "l-scheduler-messages.recurrenceMessages.editRecurring": {
            "pt": "Deseja editar apenas esta ocorrência de evento ou toda a série?",
			"en": "Do you want to edit only this event occurrence of the whole series?",
			"es": "¿Solo desea editar esta ocurrencia de evento o toda la serie?"
        },
        "l-scheduler-messages.recurrenceMessages.editWindowOccurrence": {
            "pt": "Editar ocorrência atual",
			"en": "Edit current occurrence",
			"es": "Editar ocurrencia actual"
        },
        "l-scheduler-messages.recurrenceMessages.editWindowSeries": {
            "pt": "Editar a série",
			"en": "Edit series",
			"es": "Editar la serie"
        },
        "l-scheduler-messages.recurrenceMessages.editWindowTitle": {
            "pt": "Editar item recorrente",
			"en": "Edit recurrent item",
			"es": "Editar ítem recurrente"
        },
        "l-edit": {
            "pt": "Editar",
			"en": "Edit",
			"es": "Editar"
        },
        "l-save": {
            "pt": "Salvar",
			"en": "Save",
			"es": "Grabar"
        },
        "l-back": {
            "pt": "Voltar",
			"en": "Back",
			"es": "Volver"
        },
        "l-minimize": {
            "pt": "Minimizar",
			"en": "Minimize",
			"es": "Minimizar"
        },
        "l-maximize": {
            "pt": "Maximizar",
			"en": "Maximize",
			"es": "Maximizar"
        },
        "l-move-up": {
            "pt": "Mover para cima",
			"en": "Move up",
			"es": "Mover hacia arriba"
        },
        "l-move-down": {
            "pt": "Mover para baixo",
			"en": "Move down",
			"es": "Mover hacia abajo"
        },
        "l-move-left": {
            "pt": "Mover para a esquerda",
			"en": "Move left",
			"es": "Mover a la izquierda"
        },
        "l-move-right": {
            "pt": "Mover para a direita",
			"en": "Move right",
			"es": "Mover a la derecha"
        },
        "l-delete": {
            "pt": "Remover",
			"en": "Remove",
			"es": "Eliminar"
        },
        "l-cancel": {
            "pt": "Cancelar",
			"en": "Cancel",
			"es": "Anular"
        },
        "l-add-favorito": {
            "pt": "Adicionar aos favoritos",
			"en": "Add to favorite",
			"es": "Agregar a los favoritos"
        },
        "l-favorito": {
            "pt": "Favorito",
			"en": "Favorite",
			"es": "Favorito"
        },
        "l-change-width": {
            "pt": "Alterar a largura",
			"en": "Edit width",
			"es": "Modificar el ancho"
        },
        "l-msg-not-array": {
            "pt": "A lista de widgets está vazia ou não é um array.",
			"en": "The list of widgets is emplty or is not an array",
			"es": "La lista de widgets está vacía o no es un array."
        },
        "l-edu-upload-file": {
            "pt": "Upload de Arquivos",
			"en": "Upload of Files",
			"es": "Upload de archivos"
        },
        "l-eduuploadfile-valida-file-size": {
            "pt": " ultrapassou o tamanho máximo permitido. Tamanho máximo permitido: ",
			"en": " the maximum size allowed exceeded. Maximum size allowed:",
			"es": " sobrepasó el tamaño máximo permitido. Tamaño máximo permitido: "
        },
        "l-eduuploadfile-valida-file-extension": {
            "pt": " não é válido. As extensões permitidas são: ",
			"en": " is not valid. The extensions allowed are:",
			"es": " no es válido. Las extensiones permitidas son: "
        },
        "l-ext-permitidas": {
            "pt": "Extensões permitidas: ",
			"en": "Extensions allowed:",
			"es": "Extensiones permitidas: "
        },
        "l-max-file-size": {
            "pt": "Tamanho máximo do arquivo: ",
			"en": "Maximum size of file:",
			"es": "Tamaño máximo del archivo: "
        },
        "l-info-mural": {
            "pt": "Informações",
			"en": "Information",
			"es": "Información"
        }
    }
]
