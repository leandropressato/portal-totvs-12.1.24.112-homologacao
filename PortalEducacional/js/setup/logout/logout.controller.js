/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['setup/setup.module', 'setup/logout/logout.factory'], function () {

    'use strict';

    angular
        .module('eduSetupModule')
        .controller('EduLogoutController', EduLogoutController);

    EduLogoutController.$inject = ['$window', '$rootScope', 'eduLogoutFactory'];

    function EduLogoutController($window, $rootScope, eduLogoutFactory) {

        var self = this;
        self.logout = logout;
        init();

        /**
         * Função de inicialização
         */
        function init() {
            self.logout();
        }

        /**
         * Realiza o logout no sistema
         */
        function logout() {

            eduLogoutFactory.logout(function (result) {
                if (result) {
                    $window.location.href = $window.location.href.split('#')[0] + 'login';
                }
            });
        }
    }
});
