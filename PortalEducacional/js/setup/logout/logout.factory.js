/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduSetupModule
* @name eduLogoutFactory
* @object factory
*
* @created 10/01/2017 v12.1.15
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada para o Logout.
*/
define(['setup/setup.module'], function () {

    'use strict';

    angular
        .module('eduSetupModule')
        .factory('eduLogoutFactory', EduLogoutFactory);

    EduLogoutFactory.$inject = ['$totvsresource'];

    /**
     * Factory para os serviços de Logout
     * @param   {object} $totvsresource Objeto facilitador para REST
     * @returns {object} Factory para serviços do Logout
     */
    function EduLogoutFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.logout = logout; // Logout no sistema

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Realiza o Logout no sistema
         * @param   {function} callback Função de retorno
         * @returns {object} Obejto de retorno do serviço
         */
        function logout(callback) {
            return factory.TOTVSSave({ method: 'Logout' }, {}, callback);
        }
    }
});
