[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Mural",
			"en": "Mural",
			"es": "Mural"
        },
        "l-titulo-erro-auto-login": {
            "pt": "Acesso Expirado",
			"en": "Expired Access",
			"es": "Acceso expirado"
        },
        "l-msg-erro-auto-login": {
            "pt": "Acesso ao portal expirado. É necessário fazer o Login na aplicação novamente!",
			"en": "Access to portal expired. Log in the application again!",
			"es": "Acceso al portal expirado. Es necesario hacer nuevamente el Login en la aplicación."
        },
        "l-info-portal-antigo": {
            "pt": "Aqui serão listadas todas as funcionalidades para acesso ao antigo Portal.",
			"en": "All functionalities to access the former Portal are listed here.",
			"es": "Aquí se listarán todas las funcionalidades para acceder al antiguo Portal."
        },
        "l-acessar": {
            "pt": "Acessar",
			"en": "Access",
			"es": "Acceder"
        }
    }
]
