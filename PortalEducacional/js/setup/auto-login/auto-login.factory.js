/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduSetupModule
* @name eduLogoutFactory
* @object factory
*
* @created 10/01/2017 v12.1.15
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada para o Logout.
*/
define(['setup/setup.module'], function () {

    'use strict';

    angular
        .module('eduSetupModule')
        .factory('eduAutoLoginFactory', EduAutoLoginFactory);

    EduAutoLoginFactory.$inject = ['$totvsresource'];

    /**
     * Factory para os serviços de Logout
     * @param   {object} $totvsresource Objeto facilitador para REST
     * @returns {object} Factory para serviços do Logout
     */
    function EduAutoLoginFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'user/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.autoLoginAsync = autoLoginAsync;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function autoLoginAsync(key, callback) {

            var parameters = {
                method: 'AutoLoginPortal',
                key: key
            };

            return factory.TOTVSGet(parameters, callback);
        }
    }
});
