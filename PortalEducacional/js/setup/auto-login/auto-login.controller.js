/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['setup/setup.module', 'setup/auto-login/auto-login.factory'], function () {

    'use strict';

    angular
        .module('eduSetupModule')
        .controller('EduAutoLoginController', EduAutoLoginController);

    EduAutoLoginController.$inject = ['eduAutoLoginFactory', '$state', '$location', '$window', 'totvs.app-notification.Service', 'i18nFilter'];

    function EduAutoLoginController(eduAutoLoginFactory, $state, $location, $window, totvsNotification, i18nFilter) {

        var self = this;
        self.mensagem = 'Login sendo processado. Aguarde...';

        init();

        function init() {

            if (angular.isDefined($state.params.key)) {
                realizaLoginFrameHTML($state.params.key);
            } else {
                $window.history.back();
            }
        }

        function realizaLoginFrameHTML(key) {

            eduAutoLoginFactory.autoLoginAsync(key, function (result) {

                if (result && !result.Excessao) {
                    var urlLogin = $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + '' + EDU_CONST_GLOBAL_URL_BASE_APP;
                    $window.location.href = urlLogin;
                }
                else {
                    exibirMensagemErroAoLogar(function () {
                        $window.history.back();
                    });
                }
            });
        }

        function exibirMensagemErroAoLogar (callback) {

            totvsNotification.message({
                title: i18nFilter('l-titulo-erro-auto-login', [], 'js/setup'),
                text: i18nFilter('l-msg-erro-auto-login', [], 'js/setup'),
                size: 'md', // sm = small | md = medium | lg = larger,
                callback: callback
            });
        }
    }
});
