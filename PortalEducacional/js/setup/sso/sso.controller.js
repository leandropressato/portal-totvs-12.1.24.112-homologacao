/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['setup/setup.module', 'setup/sso/sso.factory'], function () {

    'use strict';

    angular
        .module('eduSetupModule')
        .controller('EduSSOController', EduSSOController);

    EduSSOController.$inject = ['eduSSOFactory','$window'];

    function EduSSOController(eduSSOFactory, $window) {

        var self = this;
        self.titulo = EDU_CONST_GLOBAL_NAME_MENU_PORTAL_CORPORERM;
        self.redirect = redirect;

        init();

        function init() {
            buscarMenusCorporeRMAsync();
        }
        function buscarMenusCorporeRMAsync() {

            self.model = [];
            eduSSOFactory.getMenuCorporeRMAsync(function (result) {

                if (result) {
                    self.model = result;
                }
            });
        }

        function redirect(url) {
            $window.open(url);
        }
    }
});
