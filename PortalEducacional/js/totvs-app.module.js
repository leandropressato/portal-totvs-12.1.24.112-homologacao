/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */
(function() {

    'use strict';

    var DependenciasGlobais = [];

    //Módulos: AngularAMD Totvs Html Framework (THF)
    DependenciasGlobais.push('angularAMD', 'totvs-html-framework');

    //Módulo: TOTVS Desktop
    DependenciasGlobais.push(
        'totvs-desktop/totvs-desktop.module',
        'totvs-desktop/totvs-desktop.factory',
        'totvs-desktop/totvs-desktop.service',
        'totvs-desktop/totvs-desktop.controller',

        'totvs-desktop/totvs-desktop-contexto/totvs-desktop-contexto-curso.factory',
        'totvs-desktop/totvs-desktop-contexto/totvs-desktop-contexto-curso.controller'
    );

    //Controllers, Diretivas, Módulos, Providers, Factorys e Services Globais da aplicação
    DependenciasGlobais.push(
        'widgets/widget.module',

        'aluno/main/main.module',
        'aluno/main/main.route',        

        'aluno/mural/mural.module',
        'aluno/mural/mural.route',

        'aluno/faltas/faltas.module',
        'aluno/faltas/faltas.route',

        'aluno/notas/notas.module',
        'aluno/notas/notas.route',

        'aluno/notafalta-unificada/notafalta-unificada.module',
        'aluno/notafalta-unificada/notafalta-unificada.route',

        'aluno/desempenho/desempenho.module',
        'aluno/desempenho/desempenho.route',

        'aluno/plano-aula/plano-aula.module',
        'aluno/plano-aula/plano-aula.route',        

        'aluno/avaliacoes/avaliacoes.module',
        'aluno/avaliacoes/avaliacoes.route',

        'aluno/calendario/calendario.module',
        'aluno/calendario/calendario.route',

        'aluno/ocorrencias/ocorrencias.module',
        'aluno/ocorrencias/ocorrencias.route',

        'aluno/grade-curricular/grade-curricular.module',
        'aluno/grade-curricular/grade-curricular.route',

        'aluno/historico-eb/historico-eb.module',
        'aluno/historico-eb/historico-eb.route',

        'aluno/disciplinas-eb/disciplinas-eb.module',
        'aluno/disciplinas-eb/disciplinas-eb.route',

        'aluno/entregas/entregas.module',
        'aluno/entregas/entregas.route',

        'aluno/quadro-horario/quadro-horario.module',
        'aluno/quadro-horario/quadro-horario.route',

        'aluno/disciplina/disciplina.module',
        'aluno/disciplina/disciplina.route',

        'aluno/aulas/aulas.module',
        'aluno/aulas/aulas.route',

        'aluno/tcc/tcc.module',
        'aluno/tcc/tcc.route',

        'aluno/matricula/matricula-disciplina.module',
        'aluno/matricula/matricula-disciplina-detalhes.factory',
        'aluno/matricula/matricula-disciplina-detalhes.controller',
        'aluno/matricula/matricula-disciplina.service',

        'aluno/dados-pessoais/dados-pessoais.module',
        'aluno/dados-pessoais/responsaveis/responsaveis.module',
        'aluno/dados-pessoais/documentos/documentos.module',
        'aluno/dados-pessoais/movimentacao-academica/movimentacao-academica.module',
        'aluno/dados-pessoais/ficha-medica/ficha-medica.module',
        'aluno/dados-pessoais/dados-pessoais.route',

        'aluno/financeiro/financeiro.module',
        'aluno/financeiro/financeiro.route',

        'aluno/relatorios/relatorios.module',
        'aluno/relatorios/relatorios.route',        

        'aluno/atividades-extras/atividades-extras.module',
        'aluno/atividades-extras/atividades-extras.route',
        'aluno/atividades-extras/inscritas/atividades-extras-inscritas-detalhes.controller',

        'aluno/requerimentos/requerimentos.module',
        'aluno/requerimentos/requerimentos.route',

        'aluno/arquivos/arquivo.module',
        'aluno/arquivos/arquivo.route',
        'aluno/arquivos/arquivo.controller',
        'aluno/arquivos/arquivo.factory',

        'aluno/matricula/matricula.module',
        'aluno/matricula/matricula.route',

        'setup/setup.module',
        'setup/setup.route',

        'diretivas/diretivas.module',
        'diretivas/edu-selecao-periodoletivo.directive',
        'diretivas/edu-selecao-turmadisciplina.directive',
        'diretivas/edu-turma-disciplina.directive',
        'diretivas/edu-selecao-etapa.directive',
        'diretivas/edu-img-foto.directive',
        'diretivas/edu-selecao-grupoatendimento.directive',
        'diretivas/edu-file-model.directive',
        'diretivas/edu-upload-file.directive',
        'diretivas/edu-scheduler.directive',
        'diretivas/edu-widgets-container.directive',

        'utils/edu-utils.module',
        'utils/edu-enums.constants',

        'utils/reports/edu-relatorio.module',
        'utils/reports/edu-relatorio.service',

        'utils/interceptors/edu-interceptors.module',

        'cst-customizacao/cst-customizacao.module',
        'cst-customizacao/cst-customizacao.route'

    );

    define(DependenciasGlobais, function(angularAMD) {

        var app =
            angular.module('totvsApp', [
                'ngSanitize',
                'ui.router',
                'ui.mask',
                'ui.select',
                'oc.lazyLoad',
                'totvsHtmlFramework',
                'totvsDesktop',
                'ngMask',
                'ngAnimate',
                'eduWidgetsModule',
                'eduMainModule',                
                'eduMuralModule',
                'eduNotasModule',
                'eduRelatoriosModule',
                'eduNotaFaltaUnificadaModule',
                'eduDesempenhoModule',
                'eduPlanoAulaModule',
                'eduOcorrenciasModule',
                'eduQuadroHorarioModule',
                'eduFaltasModule',
                'eduDiretivasModule',
                'eduMatriculaDisciplinaModule',
                'eduDadosPessoaisModule',
                'eduResponsaveisModule',
                'eduDocumentosModule',
                'eduMovimentacaoAcademicaModule',
                'eduAvaliacoesModule',
                'eduCalendarioModule',
                'eduUtilsModule',
                'eduInterceptorsModule',
                'eduFinanceiroModule',
                'eduGradeCurricularModule',
                'eduHistoricoEBModule',
                'eduDisciplinasEBModule',
                'eduDisciplinaModule',
                'eduAulasModule',
                'eduAtividadesExtrasModule',
                'eduRelatorioModule',
                'eduRequerimentosModule',
                'eduArquivoModule',
                'eduSetupModule',
                'eduTccModule',
                'eduEntregasModule',
                'eduFichaMedicaModule',
                'eduMatriculaModule',
                'cstCustomizacaoModule'
            ]);

        // Busca os arquivos de configuração e rota do módulo principal 'totvsApp'
        // antes antes de realizar o bootstrapping(inicialização) da aplicação
        requirejs(['totvs-app.config', 'totvs-app.route', 'totvs-app.polyfills.config'], function() {
            if (app != null) {

                //Inicializa a aplicação
                return angularAMD.bootstrap(app);
            }
        });
    });

}());
