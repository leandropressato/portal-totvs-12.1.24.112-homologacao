[{
    "l-home": {
        "pt": "Início",
		"en": "Start",
		"es": "Inicio"
    },
    "l-titulo": {
        "pt": "Arquivos",
		"en": "Files",
		"es": "Archivos"
    },
    "l-material-disciplinas": {
        "pt": "Materiais das disciplinas",
		"en": "Material of the course subjects",
		"es": "Materiales de las materias"
    },
    "l-materiais-institucionais": {
        "pt": "Materiais da Instituição",
		"en": "Material of the Institution",
		"es": "Materiales de la institución"
    },
    "l-widget-title": {
      "pt": "Painel do Aluno",
	  "en": "Student's Panel",
	  "es": "Panel del alumno"
    }
}]
