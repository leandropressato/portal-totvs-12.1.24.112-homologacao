/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/arquivos/arquivo.module',
        'aluno/arquivos/arquivo.factory',
        'aluno/aulas/aulas.factory',
        'aluno/matricula/matricula-disciplina.service'],
        function () {

    'use strict';

    angular
        .module('eduArquivoModule')
        .controller('eduMaterialController', EduMaterialController);

    EduMaterialController.$inject = ['$scope',
                                    '$state',
                                    'eduMaterialFactory',
                                    'eduAulasFactory',
                                    'MatriculaDisciplinaService',
                                    'eduUtilsFactory'];

    function EduMaterialController($scope,
                                    $state,
                                    EduMaterialFactory,
                                    EduAulasFactory,
                                    MatriculaDisciplinaService,
                                    eduUtilsFactory) {

        // *********************************************************************************
        // *** Variáveis
        // *********************************************************************************

        var self = this;

        // *********************************************************************************
        // *** Propriedades públicas e métodos
        // *********************************************************************************

        self.downloadMaterial = downloadMaterial;
        self.getTamanhoAquivo = getTamanhoAquivo;
        self.filtrarMateriais = filtrarMateriais;
        self.filtroDisciplina = filtroDisciplina;
        self.exibirDetalhesAula = exibirDetalhesAula;
        self.exibirDetalhesDisciplina = exibirDetalhesDisciplina;

        self.disciplina = 0;
        self.telaDisciplina = false;
        self.refresh = false;
        self.ignoraDiscEmCurso;
        self.listaMateriaisDisciplina = [];

        // *********************************************************************************
        // *** Inicialização do controller
        // *********************************************************************************
        init();

        /**
         * Metodo de inicialização do controller
         */
        function init() {
            //Oculta header na visão de detalhes da disciplina
            if ($state.params.idTurmaDisc && $state.params.idTurmaDisc !== -1) {
                self.disciplina = $state.params.idTurmaDisc;
                self.telaDisciplina = true;
                filtrarMateriais(self.disciplina);
            }

            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function(params){
                self.ignoraDiscEmCurso = !params.VerDisUploadMaterial;
            });
        }

        /**
         * Evento de filtro da turma/disciplina
         *
         * @param {Number} idTurmaDisc Id. da Turma/Disciplina
         */
        function filtrarMateriais(idTurmaDisc) {
            if (idTurmaDisc === undefined) {
                return;
            }
            
            EduMaterialFactory.materiaisTurmaDisc(idTurmaDisc, function (materiais) {
                self.listaMateriaisExibicao = materiais;
            });
        }

        /**
         * Download do material
         *
         * @param {Object} material Material para download
         */
        function downloadMaterial(material) {
            var link = document.createElement('a');
            link.download = material.PATHARQUIVO;
            link.href = EduMaterialFactory.urlDownloadArquivo(material.IDMATERIALSEC, material.PATHARQUIVO);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        /**
         * Trata tamanho do arquivo
         *
         * @param {Object} material - Material de download
         * @returns String - Tamanho do arquivo em KB
         */
        function getTamanhoAquivo(material) {
            if (material) {
                var mb = material.TAMANHO / 1024;
                return mb > 1 ? mb.toFixed(2) + 'Mb' : material.TAMANHO + 'Kb';
            }
        }

        /**
         * Exibe detalhes da aula
         *
         * @param {Object} objMaterial - Material associado a aula
         */
        function exibirDetalhesAula (objMaterial) {
            EduAulasFactory.exibirResumoAula(objMaterial.IDTURMADISC, objMaterial.IDPLANOAULA, objMaterial.CODDISC);
        }

        /**
         * Exibe detalhes da matrícula na disciplina
         *
         * @param {Object} objMaterial - Material associado a disciplina
         */
        function exibirDetalhesDisciplina (objMaterial) {
            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(objMaterial.CODCOLIGADA, objMaterial.IDTURMADISC);
        }

        /**
         * Função utilizada como filtro de arquivos
         *
         * @param {Object} item Material
         * @returns Boolean - Material deve ser exibido ou não
         */
        function filtroDisciplina(item) {
            return self.disciplina === -1 || item.IDTURMADISC === self.disciplina;
        }

        // Assina o evento de alteração de período
        $scope.$on('alteraPeriodoLetivoEvent', function () {  
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function(params){
                self.ignoraDiscEmCurso = !params.VerDisUploadMaterial;
                self.refresh = true;
            });
        });
    }
});
