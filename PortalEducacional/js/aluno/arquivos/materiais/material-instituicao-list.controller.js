/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/arquivos/arquivo.module',
    'aluno/arquivos/arquivo.factory'],
    function () {

        'use strict';

        angular
            .module('eduArquivoModule')
            .controller('EduMaterialInstitucionalController', EduMaterialInstitucionalController);

        EduMaterialInstitucionalController.$inject = ['$scope',
            '$rootScope',
            '$state',
            'eduMaterialFactory'];

        function EduMaterialInstitucionalController($scope,
            $rootScope,
            $state,
            EduMaterialFactory) {

            // *********************************************************************************
            // *** Variáveis
            // *********************************************************************************

            var self = this;

            // *********************************************************************************
            // *** Propriedades públicas e métodos
            // *********************************************************************************

            self.listaMateriais = [];
            self.listaMateriaisExibicao = [];
            self.disciplina = 0;
            self.downloadMaterial = downloadMaterial;
            self.getTamanhoAquivo = getTamanhoAquivo;
            self.refresh = false;

            // *********************************************************************************
            // *** Inicialização do controller
            // *********************************************************************************
            init();

            /**
             * Metodo de inicialização do controller
             */
            function init() {

                EduMaterialFactory.materiaisInstitucionais(function (materiais) {
                    self.listaMateriais = self.listaMateriaisExibicao = materiais;
                });
            }

            /**
             * Download do material
             *
             * @param {Object} material Material para download
             */
            function downloadMaterial(material) {
                var link = document.createElement('a');
                link.download = material.PATHARQUIVO;
                link.href = EduMaterialFactory.urlDownloadArquivo(material.IDMATERIALSEC, material.PATHARQUIVO);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }

            /**
             * Trata tamanho do arquivo
             *
             * @param {Object} material - Material de download
             * @returns String - Tamanho do arquivo em KB
             */
            function getTamanhoAquivo(material) {
                if (material) {
                    var mb = material.TAMANHO / 1024;

                    return mb > 1 ? mb.toFixed(2) + 'Mb' : material.TAMANHO + 'Kb';
                }
            }
        }
    });
