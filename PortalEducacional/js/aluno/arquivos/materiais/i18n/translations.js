[{
    "l-titulo": {
        "pt": "Materiais",
		"en": "Material",
		"es": "Materiales"
    },
    "btn-download": {
        "pt": "Download",
		"en": "Download",
		"es": "Descarga"
    },
    "l-data-publicacao": {
        "pt": "Publicação",
		"en": "Publication",
		"es": "Publicación"
    },
    "l-disciplina": {
        "pt": "Disciplina",
		"en": "Course Subject",
		"es": "Materia"
    },
    "l-tipo-material": {
        "pt": "Tipo do material",
		"en": "Type of material",
		"es": "Tipo de material"
    },
    "l-tamanho-arquivo": {
        "pt": "Arquivo",
		"en": "File",
		"es": "Archivo"
    },
    "l-aula-vinculada": {
        "pt": "Aula vinculada",
		"en": "Class bound",
		"es": "Clase vinculada"
    },
    "l-nome-arquivo": {
        "pt": "Nome do arquivo",
		"en": "File name",
		"es": "Nombre del archivo"
    },
    "l-titulo-institucional":{
        "pt": "Materiais da Instituição",
		"en": "Material of the Institution",
		"es": "Materiales de la institución"
    }
}]
