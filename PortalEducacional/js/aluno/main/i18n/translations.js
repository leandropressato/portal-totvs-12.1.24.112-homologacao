[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Mural",
			"en": "Message board",
			"es": "Mural"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student board",
		  "es": "Panel del alumno"
        }
    }
]
