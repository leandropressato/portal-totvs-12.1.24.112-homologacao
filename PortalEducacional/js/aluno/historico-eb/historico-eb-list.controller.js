/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduHistoricoEBModule
 * @name EduHistoricoEBController
 * @object controller
 *
 * @created 14/12/2016 v12.1.15
 * @updated
 *
 * @requires historico-eb.module
 *
 * @dependencies eduGradeCurricularFactory
 *
 * @description Controller do Histórico do Ensino Básico
 */
define(['aluno/historico-eb/historico-eb.module',
    'aluno/grade-curricular/grade-curricular.factory',
    'aluno/matricula/matricula-disciplina.service',
    'utils/edu-enums.constants',
    'utils/edu-utils.factory',
    'utils/reports/edu-relatorio.service',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'
], function () {

    'use strict';

    angular
        .module('eduHistoricoEBModule')
        .controller('EduHistoricoEBController', EduHistoricoEBController);

    EduHistoricoEBController.$inject = [
        'TotvsDesktopContextoCursoFactory',
        'eduGradeCurricularFactory',
        'MatriculaDisciplinaService',
        'eduEnumsConsts',
        'eduUtilsFactory',
        '$scope',
        '$state',
        'i18nFilter',
        '$timeout',
        '$compile',
        'eduRelatorioService',
        'eduWidgetsConsts',
        '$rootScope',
        'eduConstantesGlobaisConsts',
        'totvs.app-notification.Service'
    ];

    /**
     * Controller do Histórico do Ensino Básico
     * @param   {object} objContextoCursoFactory    Objeto do contexto Educacional
     * @param   {object} eduGradeCurricularFactory  Factory para os serviços REST
     * @param   {object} MatriculaDisciplinaService Service para exibir os dados da disciplinas
     * @param   {object} EduEnumsConsts             Objeto de Constantes do Educacional
     * @param   {object} objUtilsFactory            Factory de serviços comuns
     * @param   {object} $scope                     Escopo View-Model
     * @param   {object} $state                     Objeto de rotas
     * @param   {object} i18nFilter                 Objeto para traduções
     * @param   {object} $timeout                   Objeto setTimeout
     * @param   {object} $compile                   Objeto para compilar html
     * @param   {object} eduRelatorioService        Serviço para geração de relatórios
     * @param   {object} eduWidgetsConsts           Constantes dos Widgets
     * @returns {string} Objeto Controller angular
     */
    function EduHistoricoEBController(objContextoCursoFactory, eduGradeCurricularFactory, MatriculaDisciplinaService,
        EduEnumsConsts, objUtilsFactory, $scope, $state, i18nFilter, $timeout, $compile, eduRelatorioService,
        eduWidgetsConsts, $rootScope, eduConstantesGlobaisConsts, totvsNotification) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this,
        codRelatorioImpressao = null;

        self.objContexto = {};
        self.objHistorico = [];
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Historico;
        self.permiteImprimir = false;

        // *********************************************************************************
        // *** Public Methods
        // *********************************************************************************
        self.getContexto = getContexto;
        self.getParametrosEducacional = getParametrosEducacional;
        self.verificaTipoEnsino = verificaTipoEnsino;
        self.loadHistorico = loadHistorico;
        self.clickCollapse = clickCollapse;
        self.expandirTodos = expandirTodos;
        self.toggleSeries = toggleSeries;
        self.getImageStatus = getImageStatus;
        self.configurarLegenda = configurarLegenda;
        self.expandirCurso = expandirCurso;
        self.imprimir = imprimir;

        // *********************************************************************************
        // *** Initialize
        // *********************************************************************************
        init();

        /**
         * @private
         * @function Função de inicialização do controller
         * @name init
         */
        function init() {
            self.getContexto();
            self.getParametrosEducacional();
            self.verificaTipoEnsino();
            self.loadHistorico();
        }

        /**
         * Valida se o usuário tem permissão no Menu
         */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_HISTORICO) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        // *********************************************************************************
        // *** Public functions
        // *********************************************************************************

        /**
         * @public
         * @function Obtém o contexto Educacional
         * @name getContexto
         */
        function getContexto() {
            self.objContexto = objContextoCursoFactory.getCursoSelecionado();
        }

        /**
         * @public
         * @function Carrega parâmetros do Educacional
         * @name getParametrosEducacional
         */
        function getParametrosEducacional() {
            objUtilsFactory.getParametrosTOTVSEducacionalAsync(function (params) {
                codRelatorioImpressao = params.CodRelatorioGradeCurricularReports;
                self.permiteImprimir = !!codRelatorioImpressao;
            });
        }

        /**
         * @public
         * @function Verifica se é Ensino Superior, caso contrário retorna para a página inicial
         * @name verificaTipoEnsino
         */
        function verificaTipoEnsino() {
            // Verifica se o tipo de ensino é Ensino Superior
            if (parseInt(self.objContexto.cursoSelecionado.APRESENTACAO) !== EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico) {
                $state.go('mural.start');
            }
        }

        /**
         * @public
         * @function Busca os dados do Histórico
         * @name loadHistorico
         */
        function loadHistorico() {
            eduGradeCurricularFactory.historicoEnsinoBasico(
                function (result) {
                    if (result) {

                        self.objHistorico = criarIDUnico(result);

                        // configuração inicial da tela
                        $timeout(function () {
                            toggleElement(true, false);
                        }, 0);

                    }
                });
        }

        /**
         * @public
         * @function Evento click do header do collapse séries
         * @name clickCollapse
         * @param {object} serie Objeto da série
         */
        function clickCollapse(serie) {

            $timeout(function () {
                $('#' + serie.id).collapse('toggle');
                serie.expandido = !serie.expandido;

                if (serie.expandido) {
                    $('#sinal' + serie.id).attr('class', 'glyphicon glyphicon-minus');
                } else {
                    $('#sinal' + serie.id).attr('class', 'glyphicon glyphicon-plus');
                }

            }, 0);

            verificaExpandirRecolherTodos(serie);
        }

        /**
         * @public
         * @function Expande/Recolhe todos os itens
         * @name expandirTodos
         * @param {boolean} blnToggleElement Verdadeiro para o toggle dos elementos
         */
        function expandirTodos(blnToggleElement) {
            if (angular.isUndefined(blnToggleElement)) {
                blnToggleElement = true;
            }

            $timeout(function () {

                var i;

                if ($('#chkExpandirTodos:checked').length > 0) {

                    // configurar botão
                    $('#lblExpandirTodos').parent().attr('class', 'btn btn-success');
                    $('#txtExpandirTodos').html(' ' + i18nFilter('l-recolher-todos', [], 'js/aluno/historico-eb'));
                    $('#lblExpandirTodos').attr('class', 'glyphicon glyphicon-zoom-out');

                    // expandir grupos
                    if (blnToggleElement) {
                        toggleElement(true, true);
                    }


                    // marcar botões de grupos
                    for ( i = 0; i < self.objHistorico.length; i++) {
                        marcarGrupoExpandido(self.objHistorico[i], true);
                    }

                } else {

                    // configurar botão
                    $('#lblExpandirTodos').parent().attr('class', 'btn btn-info');
                    $('#txtExpandirTodos').html(' ' + i18nFilter('l-expandir-todos', [], 'js/aluno/historico-eb'));
                    $('#lblExpandirTodos').attr('class', 'glyphicon glyphicon-zoom-in');

                    // expandir grupos
                    if (blnToggleElement) {
                        toggleElement(false, false);
                    }

                    // marcar botões de grupos
                    for ( i = 0; i < self.objHistorico.length; i++) {
                        marcarGrupoExpandido(self.objHistorico[i], false);
                    }

                }
            }, 0);

        }

        /**
         * @public
         * @function Expandir/Recolher séries
         * @name toggleSeries
         * @param {object}  curso           Objeto do curso
         * @param {boolean} showDisciplinas Exibir disciplinas
         */
        function toggleSeries(curso, showDisciplinas) {

            // expande/recolhe as séries
            var j, serie;

            for (j = 0; j < curso.series.length; j++) {

                serie = curso.series[j];

                if ((showDisciplinas && !serie.expandido) ||
                    (!showDisciplinas && serie.expandido)) {
                    clickCollapse(serie);
                }
            }
        }

        /**
         * @public
         * @function Tag relativa ao status
         * @name getImageStatus
         * @param   {object} objItem Objeto da linha corrente da disciplina
         * @returns {string} HTML com a tag do status
         */
        function getImageStatus(objItem) {

            if (angular.isDefined(objItem.codParteHist)) {
                return '<span class="tag legend legenda-' + objItem.codLegendaParteHist + ' legenda-content-' + objItem.codLegendaParteHist + ' "></span>';

            } else {
                return '';
            }
        }

        /**
         *
         *
         * @param {any} serie
         * @param {any} legendas
         */


        /**
         * @public
         * @function Cria a legenda dinâmica de cada grid
         * @name configurarLegenda
         * @param {object} serie    Objetos séries
         * @param {Array}  legendas Lista de legendas
         */
        function configurarLegenda(serie, legendas) {

            var legenda = '<totvs-page-tags>',
                i, itemLegenda;

            for (i = 0; i < legendas.length; i++) {
                itemLegenda = legendas[i];
                legenda += '<tag class="legenda-item legenda-' + itemLegenda.codigoLegenda + ' legenda-content-' + itemLegenda.codigoLegenda + '">' + itemLegenda.descricao + '</tag>';
            }

            legenda += '</totvs-page-tags>';

            var idLegenda = 'leg' + serie.id;

            $('#' + idLegenda).empty();
            // cria o elemento através do angular.
            var elmLegenda = angular.element(legenda);

            // insere o elemento na div
            elmLegenda = $('#' + idLegenda).append(elmLegenda);

            // compila o elemento na tela.
            elmLegenda = $compile(elmLegenda)($scope);

        }

        /**
         * @public
         * @function Expandir área de cursos
         * @name expandirCurso
         * @param {object} curso Objeto curso
         */
        function expandirCurso(curso) {

            var nomeElm = 'ExpandirGrupo' + curso.id;

            $timeout(function () {
                if ($('#chk' + nomeElm + ':checked').length > 0) {
                    // altera propriedades dos botões
                    $('#lbl' + nomeElm).parent().attr('class', 'btn btn-success');
                    $('#lbl' + nomeElm).attr('class', 'glyphicon glyphicon-zoom-out');
                    // expande as séries
                    toggleSeries(curso, true);
                } else {
                    // altera propriedades dos botões
                    $('#lbl' + nomeElm).parent().attr('class', 'btn btn-info');
                    $('#lbl' + nomeElm).attr('class', 'glyphicon glyphicon-zoom-in');
                    // recolhe as séries
                    toggleSeries(curso, false);
                }
            }, 0);
        }

        /**
         * @public
         * @function Executa emissão do relatório associado a visão(Parametrizado no sistema).
         * @name imprimir
         */
        function imprimir () {
            if (codRelatorioImpressao) {
                eduRelatorioService.emitirRelatorio(codRelatorioImpressao);
            }
        }

        // *********************************************************************************
        // *** Private functions
        // *********************************************************************************

        /**
         * @private
         * @function Cria um id único para cada série
         * @name criarIDUnico
         * @param   {Array} cursos Lista de cursos
         * @returns {Array} Lista de cursos com id único
         */
        function criarIDUnico(cursos) {

            var i, j, curso, serie;

            for (i = 0; i < cursos.length; i++) {

                curso = cursos[i];

                curso.id = curso.codCursoHist.toString().replace(' ', '');

                for (j = 0; j < curso.series.length; j++) {

                    serie = curso.series[j];

                    // cria um id único pro item, para ser adicionado ao id do header do collapse
                    serie.id = curso.codCursoHist.toString().replace(' ', '') +
                        serie.ano.toString().replace(' ', '') +
                        serie.codSerieHist.toString();
                }
            }

            return cursos;
        }

        /**
         * @private
         * @function Marcação para grupos expandidos
         * @name marcarGrupoExpandido
         * @param {object}  curso           Objeto curso
         * @param {boolean} marcarExpandido Marcar expandido
         */
        function marcarGrupoExpandido(curso, marcarExpandido) {

            var nomeElm = 'ExpandirGrupo' + curso.id;

            $timeout(function () {
                if (marcarExpandido) {
                    $('#lbl' + nomeElm).parent().attr('class', 'btn btn-success');
                    $('#lbl' + nomeElm).attr('class', 'glyphicon glyphicon-zoom-out');
                    $('#chk' + nomeElm).prop('checked', true );
                } else {
                    $('#lbl' + nomeElm).parent().attr('class', 'btn btn-info');
                    $('#lbl' + nomeElm).attr('class', 'glyphicon glyphicon-zoom-in');
                    $('#chk' + nomeElm).prop('checked', false );
                }
            }, 0);

        }

        /**
         * @private
         * @function Mostra ou esconde todas as séries
         * @name toggleElement
         * @param {boolean} showSeries      Mostrar séries
         * @param {boolean} showDisciplinas Mostrar disciplinas
         */
        function toggleElement(showSeries, showDisciplinas) {

            angular.forEach(angular.element('span.content'), function (value) {
                var group = angular.element(value);
                if ((showSeries && group.css('display') === 'none')) {

                    $timeout(function () {
                        group.prev().children().children().click();
                    }, 0);

                }
            });

            // expande/recolhe as séries
            var i, curso;

            for (i = 0; i < self.objHistorico.length; i++) {
                curso = self.objHistorico[i];
                toggleSeries(curso, showDisciplinas);
            }
        }

        /**
         * @private
         * @function Verifica se estão todos os campos expandidos ou recolhidos
         * @name verificaExpandirRecolherTodos
         * @param {object} serie Objeto serie
         */
        function verificaExpandirRecolherTodos(serie) {
            $timeout(function () {
                // verifica se todos estão expandidos/recolhidos
                var countPanels = $('.panel-collapse').length;
                var countCollapsing = $('.panel-collapse.collapsing').length;
                if (serie.expandido) {
                    var countExp = $('.panel-collapse.collapse.in').length;
                    if ((countExp + countCollapsing) === countPanels) {
                        $scope.vmExpandirTodos = true;
                        self.expandirTodos(false);
                    }
                }
                else {
                    var countRec = $('.panel-collapse.collapse').not('.in').length;
                    if ((countRec + countCollapsing) === countPanels) {
                        $scope.vmExpandirTodos = false;
                        self.expandirTodos(false);
                    }
                }
            }, 0);
        }
    }
});
