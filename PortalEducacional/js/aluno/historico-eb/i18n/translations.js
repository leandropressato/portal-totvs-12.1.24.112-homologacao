[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Histórico",
			"en": "History",
			"es": "Historial"
        },
        "l-Nenhum-Registro-Encontrado": {
            "pt": "Nenhum Registro Encontrado!",
			"en": "No Record Found",
			"es": "¡No se encontró ningún registro!"
        },
        "l-disciplinas": {
            "pt": "Disciplinas",
			"en": "Course Subjects",
			"es": "Materias"
        },
        "l-codigo": {
            "pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-carga-horaria": {
            "pt": "CH",
			"en": "CH",
			"es": "CH"
        },
        "l-faltas": {
            "pt": "Faltas",
			"en": "Absences",
			"es": "Faltas"
        },
        "l-notas-conceito": {
            "pt": "Notas/Conceito",
			"en": "Score/Concept",
			"es": "Notas/Concepto"
        },
        "l-ciclo-ano": {
            "pt": "Ciclo/Ano",
			"en": "Cycle/Year",
			"es": "Ciclo/Año"
        },
        "l-dias-letivos": {
            "pt": "Dias letivos",
			"en": "School days",
			"es": "Días lectivos"
        },
        "l-minimo-aprov": {
            "pt": "Min. para aprovação",
			"en": "Min. to be approved",
			"es": "Mín. para aprobación"
        },
        "l-carga-horaria": {
            "pt": "CH",
			"en": "CH",
			"es": "CH"
        },
        "l-resultado": {
            "pt": "Resultado",
			"en": "Result",
			"es": "Resultado"
        },
        "l-expandir-todos": {
            "pt": "Expandir todos",
			"en": "Expand all",
			"es": "Expandir todos"
        },
        "l-recolher-todos": {
            "pt": "Recolher todos",
			"en": "Collect all",
			"es": "Pagar todos"
        },
        "l-estabelecimento": {
            "pt": "Instituição",
			"en": "Institution",
			"es": "Institución"
        },
        "l-cidade": {
            "pt": "Cidade",
			"en": "City",
			"es": "Ciudad"
        },
        "l-estado": {
            "pt": "Estado",
			"en": "State",
			"es": "Estado/Prov/Reg"
        },
        "l-obs": {
            "pt": "Observação",
			"en": "Note",
			"es": "Observación"
        },
        "l-msg-nenhum-registro": {
            "pt": "Não foi cadastrado nenhuma disciplina para este ciclo/ano",
			"en": "No course subject registered for this cycle/year",
			"es": "No se registró ninguna materia para este ciclo/año"
        },
        "l-ano": {
            "pt": "Ano",
			"en": "Year",
			"es": "Año"
        },
        "l-situacao": {
            "pt": "Situação",
			"en": "Status",
			"es": "Situación"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        }
    }
]
