/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.20
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduNotaFaltaUnificadaModule
 * @name EduFaltasUnificadaFactory
 * @object factory
 *
 * @created 2018-01-05 v12.1.20
 * @updated
 *
 * @requires eduNotaFaltaUnificadaModule
 *
 * @dependencies eduNotaFaltaUnificadaFactory
 *
 * @description Factory utilizada para Nota/Falta unificada.
 */

define(['aluno/notafalta-unificada/notafalta-unificada.module'], function () {

    'use strict';

    angular
        .module('eduNotaFaltaUnificadaModule')
        .factory('eduNotaFaltaUnificadaFactory', EduNotaFaltaUnificadaFactory);

    EduNotaFaltaUnificadaFactory.$inject = ['$totvsresource'];

    function EduNotaFaltaUnificadaFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.NotaFaltaUnificada = NotaFaltaUnificada;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function NotaFaltaUnificada(callback) {
            var parameters = [];
            parameters['method'] = 'v1/NotaFaltaUnificada';; // método;

            factory.TOTVSGet(parameters, callback);
        }
    }
});
