[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Nota/Falta unificada",
			"en": "Unified Score/Absence",
			"es": "Nota/Falta unificada"
        },
        "l-detalhes": {
            "pt": "detalhes",
			"en": "details",
			"es": "detalles"
        },
        "l-comentario": {
            "pt": "OBS",
			"en": "NOTES",
			"es": "OBS"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Course subject",
			"es": "Materia"
        },
        "l-percentual": {
            "pt": "Percentual de faltas",
			"en": "Percentage of absences",
			"es": "Porcentaje de faltas"
        },
        "l-msg-nenhum-registro": {
            "pt": "Nenhum registro de nota/falta unificada foi localizado!",
			"en": "No record of unified score/absence found!",
			"es": "¡No se localizó ningún registro de nota/falta unificada!"
        }        
    }
]
