/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.20
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduNotaFaltaUnificadaModule
 * @name eduNotaFaltaUnificadaController
 * @object controller
 *
 * @created 2018-01-05 v12.1.20
 * @updated
 *
 * @requires notafalta-unificada.module
 *
 * @dependencies
 *
 * @description Controller de Nota/Falta unificada
 */
define(['aluno/notafalta-unificada/notafalta-unificada.module',
    'aluno/notafalta-unificada/notafalta-unificada.route',
    'aluno/notafalta-unificada/notafalta-unificada.factory',
    'utils/edu-utils.factory',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'
], function() {

    'use strict';

    angular.module('eduNotaFaltaUnificadaModule')
        .controller('eduNotaFaltaUnificadaController', eduNotaFaltaUnificadaController);

    eduNotaFaltaUnificadaController.$inject = [
        '$scope',
        '$filter',
        '$compile',
        '$rootScope',
        'i18nFilter',
        'TotvsDesktopContextoCursoFactory',
        'MatriculaDisciplinaService',
        'eduUtilsFactory',
        'eduUtilsService',
        'eduNotaFaltaUnificadaFactory',
        'eduWidgetsConsts',
        'eduConstantesGlobaisConsts',
        '$state',
        'totvs.app-notification.Service'
    ];

    function eduNotaFaltaUnificadaController(
        $scope,
        $filter,
        $compile,
        $rootScope,
        i18nFilter,
        TotvsDesktopContextoCursoFactory,
        MatriculaDisciplinaService,
        eduUtilsFactory,
        eduUtilsService,
        eduNotaFaltaUnificadaFactory,
        eduWidgetsConsts,
        eduConstantesGlobaisConsts,
        $state,
        totvsNotification
    ) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.NotaFaltaUnificada;
        self.notaFaltaUnificada;
        self.periodoLetivo;
        self.myGrid;
        self.parametrosEdu;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.abrirDetalhesDisciplina = abrirDetalhesDisciplina;
        self.buscaNotaFaltaUnificada = buscaNotaFaltaUnificada;
        self.periodoLetivoAlterado = periodoLetivoAlterado;

        /**
         * Assina o evento de alteração de período
         */
        $scope.$on('alteraPeriodoLetivoEvent', buscaNotaFaltaUnificada);

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        init();

        function init() {
            carregarParametrosEdu();
        }

        /**
         * Valida se o usuário tem permissão no Menu
         */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_NOTAFALTAUNIFICADA) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        /**
         * Evento disparado ao quando está ocorrendo a vinculação dos dados a partir da fonte de dados.
         *
         * @param {any} event - atributos do evento
         */
        function onDataBound(event) {

            var grid = self.myGrid;
            if (grid) {
                for (var i = 0; i < grid.columns.length; i++) {
                    grid.showColumn(i);
                }
                // Percorremos as colunas que estão agrupadas, que são identificadas pela
                // class k-group-indicator, dessa forma aplicamos o hide.
                $('div.k-group-indicator').each(function(i, v) {
                    var fieldName = $(v).data('field').toString().replace(/'/g, "\"");
                    grid.hideColumn(fieldName);
                });
            }
        }

        /**
         * Evento disparado ao realizar o carregamento dos dados.
         *
         * @param {any} data - atributos do evento.
         */
        function onData(data) {
            var grid = self.myGrid;
            if (grid) {
                grid.bind('dataBound', onDataBound);
            }
        }

        /**
         * Carrega os parâmetros do educacional
         *
         */
        function carregarParametrosEdu() {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function(params) {
                self.parametrosEdu = params;
            });
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

         function periodoLetivoAlterado() {
             $scope.$broadcast('alteraPeriodoLetivoEvent');
         }

        /**
         * Realiza a busca pelos dados de falta do aluno no contexto selecionado
         */
        function buscaNotaFaltaUnificada() {

            var defColumns = [];
            eduNotaFaltaUnificadaFactory.NotaFaltaUnificada(function(result) {

                self.notaFaltaUnificada = result.NotaFaltaUnificada;
                self.etapasOrdenadas = result.Etapas;

                if (self.notaFaltaUnificada.length > 0) {
                    defColumns = configuraColunasGrid();
                }

                self.gridOptions = {
                    columns: defColumns,
                    dataSource: self.notaFaltaUnificada,
                    groupable: false,
                    pageable: false,
                    sortable: false,
                    resizable: false,
                    scrollable: true,
                    selectable: false
                };

                criaGrid();
            });
        }

        /**
         * Cria o elemento grid na tela
         */
        function criaGrid() {
            $('#notaFaltaUnificadaGrid').empty();
            // cria o elemento através do angular.
            var grid = angular.element('<totvs-grid></totvs-grid>');

            // adiciona o atributo grid-options com as configurações do grid
            grid.attr('grid-options', 'controller.gridOptions');
            grid.attr('grid', 'controller.myGrid');
            grid.attr('on-data', 'controller.onData(data)');

            $('#notaFaltaUnificadaGrid').css('padding-top', '10px');
            $('#notaFaltaUnificadaGrid tr').addClass('GridRow');

            // insere o elemento na div
            grid = $('#notaFaltaUnificadaGrid').append(grid);

            // compila o elemento na tela.
            grid = $compile(grid)($scope);
        }

        /**
         * Realiza a configuração das colunas que serão apresentadas no grid
         *
         * @param {any} valor - Conteúdo retornado pelo REST. Listagem de Notas por etapa.
         * @returns
         */
        function configuraColunasGrid() {
            var colunaConfig = [];
            colunaConfig = defineColunasGrid(colunaConfig);
            return colunaConfig;
        }

        /**
         * Configura as colunas com valores fixos.
         *
         * @param {any} etapasOrdenadas - .
         * @param {any} colunaConfig - coluna configurada.
         *
         * @returns coluna configurada
         */
        function defineColunasGrid(colunaConfig) {

            var AgrupadorAtual = '';
            var Grupos = [];
            var colunasGrupo = [];
            var columnsNotaFalta = self.notaFaltaUnificada[0];

            //Primeira coluna
            colunaConfig.push(retornaColunaConfig(
                                    'DISCIPLINA',
                                    templateDisciplina(),
                                    $filter('i18n')('l-disciplina', [], 'js/aluno/notafalta-unificada/'),
                                    'left',
                                    '200px',
                                    false));

            self.etapasOrdenadas.forEach(function(item) {
                if (Grupos.indexOf(item.AGRUPADOR) === -1) {
                    Grupos.push(item.AGRUPADOR);
                }
            });

            //Adiciona as colunas no GRID que possuem AGRUPADORES primeiro, de acordo com os critérios de ordenação
            Grupos.forEach(function(grupo) {
                for (var colunaAtual in columnsNotaFalta) {
                    var nomeColuna = colunaAtual.split('|');
                    if (nomeColuna.length > 1) {
                        if (nomeColuna[1] === grupo) {
                            if (nomeColuna[4] == 'COMENTARIO') {
                                colunasGrupo.push(retornaColunaConfig(
                                    colunaAtual,
                                    templateComentario(colunaAtual),
                                    $filter('i18n')('l-comentario', [], 'js/aluno/notafalta-unificada/'),
                                    'center',
                                    '50px',
                                    true));
                            } else {
                                colunasGrupo.push(retornaColunaConfig(
                                    colunaAtual,
                                    formatarTemplateNotaFalta(colunaAtual),
                                    nomeColuna[0],
                                    'center',
                                    '70px',
                                    true));
                            }
                        }
                    }
                }

                //Adiciona no grid todas as colunas com Agrupadores, já em suas ordens
                colunaConfig.push({
                    title: grupo,
                    columns: colunasGrupo
                });

                colunasGrupo = [];
            });

            //Adiciona as colunas no GRID quem NÃO possuem AGRUPADORES, 
            //logo após as colunas que possuem agrupadores.
            for (var colunaAtual in columnsNotaFalta) {
                var nomeColuna = colunaAtual.split('|');
                if (nomeColuna.length > 1) {
                    if (nomeColuna[1] == null || nomeColuna[1] == '') {
                        if (nomeColuna[4] == 'COMENTARIO') {
                            colunaConfig.push(retornaColunaConfig(
                                    colunaAtual,
                                    templateComentario(colunaAtual),
                                    $filter('i18n')('l-obs', [], 'js/aluno/notafalta-unificada/'),
                                    'center',
                                    '50px',
                                    true));
                        } else {
                            colunaConfig.push(retornaColunaConfig(
                                    colunaAtual,
                                    formatarTemplateNotaFalta(colunaAtual),
                                    nomeColuna[0],
                                    'center',
                                    '70px',
                                    true));
                        }
                    }
                }
            }

            //Adiciona a ultima coluna no Grid. Percentual de faltas, caso esteja parametrizado para exibir.
            if (self.parametrosEdu.PercentualLimiteFaltas != null) {
                if (self.parametrosEdu.PercentualLimiteFaltas > 0) {
                colunaConfig.push(retornaColunaConfig(
                                    'PERCENTUAL',
                                    '<span>#=data["PERCENTUAL"]#%</span>',
                                    $filter('i18n')('l-percentual', [], 'js/aluno/notafalta-unificada/'),
                                    'center',
                                    '70px',
                                    true));
                }
            }

            return colunaConfig;
        }

        /**
         * Retorna uma coluna parametrizavel para o Grid de Nota/Falta unificada
         *
         * @returns Coluna formatada.
         */
        function retornaColunaConfig(_field, _template, _title, textAlign, _width, fontMenor) {

            var fonte = '';

            if (fontMenor)
                fonte = 'font-size: 11px; ';

            var coluna = {
                field: '["' + _field + '"]',
                template: _template,
                title: _title,
                attributes: {
                    'style': 'text-align:' + textAlign
                },
                width: _width,
                headerAttributes: {
                    'style': fonte + 'white-space: normal; vertical-align: middle; padding:0;'
                },
            }
            return coluna;
        }

        /**
         * Template utilizado para a coluna de Comentário de notas
         *
         * @returns Coluna formatada.
         */
        function templateComentario(nomeCampo) {
            var conteudo = '<span class="ico-info" ng-if="(\'#:data["' + nomeCampo + '"]#\' != \'null\' )" tooltip-append-to-body=\'appendToBody=true\' data-tooltip=\'#:data["' + nomeCampo + '"]#\' tooltip-placement="left"></span>';
            return conteudo;
        }

        /**
         * Formatar o template de notas e faltas
         *
         * @param {any} nomeCampo - Nome da coluna
         * @returns Coluna formatada
         */
        function formatarTemplateNotaFalta(nomeCampo) {
            return ('#=data["' + nomeCampo + '"] ? data["' + nomeCampo + '"] : \'\'#');
        }

        /**
         * Formata coluna de Disciplina, já contendo o link para o Modal.
         *
         * @returns Coluna formatada.
         */
        function templateDisciplina() {
            var conteudo = '<a ng-click="controller.abrirDetalhesDisciplina(#:data.IDTURMADISC#)" ' +
                'tooltip-append-to-body=\'appendToBody=true\' data-tooltip="#:data.DISCIPLINA#" tooltip-placement="right">' +
                '#:data.DISCIPLINA#</a>';
            return conteudo;
        }

        /**
         * Função que abre o modal de Detalhes da Disciplina
         *
         * @param {any} idTurmaDisc - Identificador da turma disciplina
         */
        function abrirDetalhesDisciplina(idTurmaDisc) {
            var contexto, codColigada, ra;

            contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
            codColigada = contexto.cursoSelecionado.CODCOLIGADA;
            ra = contexto.cursoSelecionado.RA;

            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(codColigada, idTurmaDisc, ra);
        }
    }
});
