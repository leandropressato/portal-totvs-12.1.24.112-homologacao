/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module eduQuadroHorarioModule
* @name QuadroHorarioFactory
* @object factory
*
* @created 2016-09-05 v12.1.15
* @updated
*
* @requires eduPeriodoMatriculaDetalhes
*
* @description Factory utilizada nos detalhes da matrícula do aluno na turma/disciplina.
*/

define(['aluno/periodo-matricula/periodo-matricula.module',
        'aluno/periodo-matricula/periodo-matricula-modal.controller'], function () {

    'use strict';

    angular
        .module('eduPeriodoMatriculaModule')
        .service('eduPeriodoMatriculaService', eduPeriodoMatriculaService);

    eduPeriodoMatriculaService.$inject = ['$modal'];

    function eduPeriodoMatriculaService($modal) {

        this.exibirDetalhesPeriodoMatricula = function () {

            $modal.open({
                templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/periodo-matricula/periodo-matricula-modal.view.html',
                controller: 'eduPeriodoMatriculaController',
                controllerAs: 'controller',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return '';
                    }
                },
                backdrop: 'true'
            });
        };

        // this.exibirDetalhesPeriodoMatriculaComHorario = function (idTurmaDisc, idHorarioTurma) {

        //     var params = {idTurmaDisc: idTurmaDisc, idHorarioTurma: idHorarioTurma};

        //     $modal.open({
        //         templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/matricula/matricula-disciplina-detalhes.view.html',
        //         controller: 'PeriodoMatriculaDetalhesController',
        //         controllerAs: 'controller',
        //         size: 'md',
        //         resolve: {
        //             parametros: function () {
        //                 return params;
        //             }
        //         },
        //         backdrop: 'true'
        //     });
        // };
    }
});
