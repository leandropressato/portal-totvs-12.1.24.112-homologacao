/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduPeriodoMatriculaModule
 * @name eduAcompanhamentoPeriodoMatriculaController
 * @object controller
 *
 * @created 2016-11-23 v12.1.15
 * @updated
 *
 * @requires PeriodoMatricula.module
 *
 * @dependencies eduPeriodoMatriculaFactory
 *
 * @description Controller do modal de Período de Matrícula exibodo a partir do PeriodoMatricula.
 */
define([
    'aluno/periodo-matricula/periodo-matricula.module',
    'aluno/periodo-matricula/periodo-matricula-modal.factory'], function() {
    'use strict';

    angular
        .module('eduPeriodoMatriculaModule')
        .controller('eduPeriodoMatriculaController', EduPeriodoMatriculaController);

    EduPeriodoMatriculaController.$inject = ['$scope',
        'eduPeriodoMatriculaFactory',
        '$filter',
        'parametros',
        '$state',
        '$modalInstance',
        'totvs.app-notification.Service',
        'i18nFilter'
    ];

    function EduPeriodoMatriculaController($scope,
        eduPeriodoMatriculaFactory,
        $filter,
        parametros,
        $state,
        $modalInstance,
        appNotificationService,
        i18nFilter) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;

        //Contém a modelo de dados dos campos disponíveis no formulário
        self.model = {};
        self.modalInstance = $modalInstance;
        self.dataSetPeriodoMatricula =[];

        //self.salvaAlteracao = salvaAlteracao;
        //self.retornaAcompanhamentoAluno = retornaAcompanhamentoAluno;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        init();

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        function init() {            
            self.dataSetPeriodoMatricula = parametros.dataSetPeriodoMatricula;
            retornaInfoPeriodoLetivo();
        }

        function retornaInfoPeriodoLetivo() {
            eduPeriodoMatriculaFactory.retornarPeriodoMatricula(function(result) {
                if (result) {
                    self.dataSetPeriodoMatricula = result["0"];

                    self.model.DTINICIOMATRICULA = self.dataSetPeriodoMatricula.DTINICIOMATRICULA;
                    self.model.DTFINMATRICULA = self.dataSetPeriodoMatricula.DTFINMATRICULA;
                    self.model.HRINICIOMATRICULA = self.dataSetPeriodoMatricula.HRINICIOMATRICULA;
                    self.model.HRFINMATRICULA = self.dataSetPeriodoMatricula.HRFINMATRICULA;

                    self.model.DTINICIOALTERACAOPROGRAMA = self.dataSetPeriodoMatricula.DTINICIOALTERACAOPROGRAMA;
                    self.model.DTFIMALTERACAOPROGRAMA = self.dataSetPeriodoMatricula.DTFIMALTERACAOPROGRAMA;
                    self.model.HRINICIOALTERACAOPROGRAMA = self.dataSetPeriodoMatricula.HRINICIOALTERACAOPROGRAMA;
                    self.model.HRFIMALTERACAOPROGRAMA = self.dataSetPeriodoMatricula.HRFIMALTERACAOPROGRAMA;

                    self.model.DTLIMITETRANCAMENTO = self.dataSetPeriodoMatricula.DTLIMITETRANCAMENTO;
                }
            });
        }

        /**
         * Redireciona para a página da turma/disciplina
         */
        function onclickCancelar() {
            $modalInstance.dismiss();
        }
    }
});
