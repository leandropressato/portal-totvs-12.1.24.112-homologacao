/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module eduQuadroHorarioModule
* @name QuadroHorarioFactory
* @object factory
*
* @created 2016-09-05 v12.1.15
* @updated
*
* @requires eduPeriodoMatricula
*
* @description Factory utilizada nos detalhes da matrícula do aluno na turma/disciplina.
*/

define(['aluno/periodo-matricula/periodo-matricula.module'], function () {

    'use strict';

    angular
        .module('eduPeriodoMatriculaModule')
        .factory('eduPeriodoMatriculaFactory', eduPeriodoMatriculaFactory);

    eduPeriodoMatriculaFactory.$inject = ['$totvsresource'];

    function eduPeriodoMatriculaFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.retornarPeriodoMatricula = retornarPeriodoMatricula;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function retornarPeriodoMatricula(callback) {
            var parameters = {};
            parameters['method'] = 'Aluno/PeriodoMatricula';

            factory.TOTVSQuery(parameters, callback);
        }
    }
});
