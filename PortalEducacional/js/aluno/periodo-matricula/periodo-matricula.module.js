/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module eduPeriodoMatriculaModule
* @object module
*
* @created 2016-09-05 v12.1.15
* @updated
*
* @dependencies totvsHtmlFramework
*
* @description Módulo do modal de Período de Matrícula, utilizado na tela de calendário.
*/
define([], function () {

    'use strict';

    angular
        .module('eduPeriodoMatriculaModule', ['totvsHtmlFramework']);
});
