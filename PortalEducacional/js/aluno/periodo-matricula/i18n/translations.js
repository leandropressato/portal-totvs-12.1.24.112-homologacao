[
    {
        "l-titulo": {
            "pt": "Informações de matrícula",
			"en": "Information of enrollment",
			"es": "Información de matrícula"
        },
        "btn-fechar": {
            "pt": "Fechar",
			"en": "Close",
			"es": "Cerrar"
        },        
        "l-periodo-matricula-presencial": {
            "pt": "Período matrícula presencial",
			"en": "Enrollment period by attendance",
			"es": "Período matrícula presencial"
        },        
        "l-periodo-matricula-online": {
            "pt": "Período matrícula online",
			"en": "Online enrollment period",
			"es": "Período matrícula online"
        },  
        "l-trancamento": {
            "pt": "Trancamento da matrícula",
			"en": "Leave of absence",
			"es": "Aplazamiento de la matrícula"
        },         
        "l-data-limite-trancamento": {
            "pt": "Data limite de trancamento",
			"en": "Limit date of Leave of absence",
			"es": "Fecha límite de aplazamiento"
        },                                      
        "l-inicio-matricula-presencial": {
            "pt": "Início matrícula presencial",
			"en": "Start of enrollment by attendance",
			"es": "Inicio matrícula presencial"
        },
        "l-final-matricula-presencial": {
            "pt": "Final matrícula presencial",
			"en": "End of enrollment by attendance",
			"es": "Final matrícula presencial"
        },
        "l-inicio-horario-presencial": {
            "pt": "Horário inicial",
			"en": "Start time",
			"es": "Horario inicial"
        },
        "l-final-horario-presencial": {
            "pt": "Horário final",
			"en": "End time",
			"es": "Horario final"
        },
        "l-inicio-matricula-online": {
            "pt": "Início da matrícula online",
			"en": "Start of online enrollment",
			"es": "Inicio de la matrícula online"
        },
        "l-final-matricula-online": {
            "pt": "Final da matrícula online",
			"en": "End of online enrollment",
			"es": "Final de la matrícula online"
        },
        "l-inicio-horario-matricula-online": {
            "pt": "Horário início",
			"en": "Start time",
			"es": "Horario inicio"
        },
        "l-final-horario-matricula-online": {
            "pt": "Horário final",
			"en": "End time",
			"es": "Horario final"
        }
    }
]
