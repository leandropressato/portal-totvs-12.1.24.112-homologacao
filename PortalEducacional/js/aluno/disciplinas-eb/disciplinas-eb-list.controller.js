/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduGradeCurricularModule
* @name EduDisciplinasEBController
* @object controller
*
* @created 06/12/2016 v12.1.15
* @updated
*
* @requires disciplinas-eb.module
*
* @dependencies eduGradeCurricularFactory
*
* @description Controller das Disciplinas (Séries) do Ensino Básico
*/
define(['aluno/disciplinas-eb/disciplinas-eb.module',
        'aluno/grade-curricular/grade-curricular.factory',
        'aluno/matricula/matricula-disciplina.service',
        'utils/edu-utils.factory',
        'utils/edu-enums.constants',
        'widgets/widget.constants',
        'utils/edu-constantes-globais.constants'], function () {

    'use strict';

    angular
        .module('eduDisciplinasEBModule')
        .controller('EduDisciplinasEBController', EduDisciplinasEBController);

    EduDisciplinasEBController.$inject = [
        'TotvsDesktopContextoCursoFactory',
        'eduGradeCurricularFactory',
        'MatriculaDisciplinaService',
        'eduUtilsFactory',
        'eduEnumsConsts',
        'i18nFilter',
        '$scope',
        '$state',
        '$timeout',
        'eduWidgetsConsts',
        '$rootScope',
        'eduConstantesGlobaisConsts',
        'totvs.app-notification.Service'
    ];

    /**
     * Controller da Grade Curricular
     * @param   {object}    objContextoCursoFactory        Factory do Contexto Educacional
     * @param   {object}    objGradeCurricularFactory      Factory da Grade Curricular
     * @param   {object}    MatriculaDisciplinaService     Serviço para exibir o modal de disciplina
     * @param   {object}    objUtilsFactory                Factory para serviços dos Parametros do Educacional
     * @param   {constants} EduEnumsConsts                 Constantes Enums do Educacional
     * @param   {object}    i18nFilter                     Objeto para aplicar filtro de tradução
     * @param   {object}    $scope                         Objeto de Escopo
     * @param   {object}    $state                         Objeto de estado de rotas
     * @param   {object}    $timeout                       Objeto timeout angular
     * @param   {object}    eduWidgetsConsts               Constantes Widgets
     */
    function EduDisciplinasEBController(objContextoCursoFactory, objGradeCurricularFactory, MatriculaDisciplinaService,
                                         objUtilsFactory, EduEnumsConsts, i18nFilter, $scope, $state, $timeout,
                                         eduWidgetsConsts, $rootScope, eduConstantesGlobaisConsts, totvsNotification) {

        var self = this;

        self.objContexto = {};
        self.objCicloAnoList = [];
        self.periodoLetivo = 0;
        self.intNumCasasDecimaisCH = 0;
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Disciplinas;

        self.widgetUrl = 'js/aluno/widgets/widget-pie.view.html';

        self.getContexto = getContexto;
        self.verificaTipoEnsino = verificaTipoEnsino;
        self.loadNumCasasDecimais = loadNumCasasDecimais;
        self.periodoLetivoAlterado = periodoLetivoAlterado;
        self.loadGradeCurricular = loadGradeCurricular;
        self.expandirTodos = expandirTodos;
        self.detalhesDisciplina = detalhesDisciplina;
        self.getColunaDisciplina = getColunaDisciplina;

        self.retornaWidgetUrl = retornaWidgetUrl;

        init();

        /**
         * Inicialização
         */
        function init() {
            self.getContexto();
            self.verificaTipoEnsino();
            self.loadNumCasasDecimais();
            self.loadGradeCurricular();

            /**
             * Ao mudar da aba verifica se marcou para
             * expandir ou recolher todos e aplica
             */
            $scope.$watch('controller.objGradeCurricular', function () {
                self.expandirTodos();
            });
        }

        /**
         * Valida se o usuário tem permissão no Menu
         */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_DISCIPLINASENSINOBASICO) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }
                //destroy watch
                permissionsWatch();
            }
        });

        /**
         * Verifica se é Ensino Básico, caso contrário retorna para a página inicial
         */
        function verificaTipoEnsino() {
            // Verifica se o tipo de ensino é Ensino Básico
            if (parseInt(self.objContexto.cursoSelecionado.APRESENTACAO) !== EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico) {
                $state.go('mural.start');
            }
        }

        /**
         * Obtém o contexto Educacional
         */
        function getContexto() {
            self.objContexto = objContextoCursoFactory.getCursoSelecionado();
        }

        /**
         * Carrega o número de casas decimais
         */
        function loadNumCasasDecimais() {
            objUtilsFactory.getParametrosTOTVSEducacionalAsync(function (params) {
                self.intNumCasasDecimaisCH = params.NumCasasDecimaisCH;
                self.intNumCasasDecimaisCredAcad = params.NumCasasDecimaisCredAcad;
                self.blnExibirDiscExtra = params.ExibirDiscExtra;
            });
        }

        /**
         * Obtém a Grade curricular do curso do contexto
         */
        function loadGradeCurricular() {
            self.objCicloAnoList = [];

            objGradeCurricularFactory.gradeCurricularEnsinoBasico(
                function (result) {
                    if (result && angular.isArray(result)) {
                        angular.forEach(result, function (objResult, key) {
                            var objGradeCurricular = objResult.Value;
                            if (angular.isArray(objGradeCurricular) && objGradeCurricular.length > 0) {
                                var codTurma = objResult.Key,
                                    objDisciplina = objGradeCurricular[0];

                                // Titulo concatenando o código da turma e o nome da habilitação
                                var titulo = i18nFilter('l-turma', [], 'js/aluno/disciplinas-eb') + ': ' +
                                        codTurma + ' | ' +
                                        i18nFilter('l-ciclo-ano', [], 'js/aluno/disciplinas-eb') + ': ' +
                                        objDisciplina.NOMEHABILITACAO,

                                    objCicloAno = {
                                        INDEX: key,
                                        IDPERLET: objDisciplina.IDPERLET,
                                        CODPERLET: objDisciplina.CODPERLET,
                                        CODHABILITACAO: objDisciplina.CODHABILITACAO,
                                        NOMEHABILITACAO: objDisciplina.NOMEHABILITACAO,
                                        CODTURMA: codTurma,
                                        TITULO: titulo
                                    };

                                self.objCicloAnoList[key] = objCicloAno;
                                self.objCicloAnoList[key].objDisciplinasList = objGradeCurricular;
                            }
                        });
                    }
                });
        }

        /**
         * Método quando altera o período letivo pela diretiva
         */
        function periodoLetivoAlterado() {
            self.loadGradeCurricular();

            $('#chkExpandirTodos').prop('checked', false);
            $('#lblExpandirTodos').parent().attr('class', 'btn btn-info');
            $('#txtExpandirTodos').html(' ' + i18nFilter('l-expandir-todos', [], 'js/aluno/disciplinas-eb'));
            $('#lblExpandirTodos').attr('class', 'glyphicon glyphicon-zoom-in');
        }

        /**
         * Expande/Recolhe as informações dos períodos
         */
        function expandirTodos() {
            $timeout(function () {
                if ($('#chkExpandirTodos:checked').length > 0) {
                    $('#lblExpandirTodos').parent().attr('class', 'btn btn-success');
                    $('#txtExpandirTodos').html(' ' + i18nFilter('l-recolher-todos', [], 'js/aluno/disciplinas-eb'));
                    $('#lblExpandirTodos').attr('class', 'glyphicon glyphicon-zoom-out');
                    toggleElement(true);
                }
                else {
                    $('#lblExpandirTodos').parent().attr('class', 'btn btn-info');
                    $('#txtExpandirTodos').html(' ' + i18nFilter('l-expandir-todos', [], 'js/aluno/disciplinas-eb'));
                    $('#lblExpandirTodos').attr('class', 'glyphicon glyphicon-zoom-in');
                    toggleElement(false);
                }
            }, 0);
        }

        /**
         * Mostra ou esconde todas as disciplinas de todos os períodos
         * @param {boolean} blnShow Verdadeiro se é para mostrar todos
         */
        function toggleElement(blnShow) {
            angular.forEach(angular.element('span.content'), function (value) {
                var group = angular.element(value);
                if ((blnShow && group.css('display') === 'none') ||
                    (!blnShow && group.css('display') !== 'none')) {

                    $timeout(function () {
                        group.prev().children().children().click();
                    }, 0);

                }
            });
        }

        /**
         * Template da coluna de Disciplinas
         * @param   {object} dataItem Objeto corrente do grdi
         * @returns {string} HTML com o código e nome da disciplina
         */
        function getColunaDisciplina(dataItem) {
            var strToReturn = '';

            strToReturn = dataItem.NOMEDISC;

            if (dataItem.IDTURMADISC && dataItem.IDTURMADISC !== '' && dataItem.IDTURMADISC !== 0) {
                strToReturn = '<a ng-style="{\'cursor\': \'pointer\'}" ng-click="controller.detalhesDisciplina(dataItem.IDTURMADISC)">{{dataItem.NOMEDISC}}</a>';
            }

            return strToReturn;
        }

        /**
         * Mostra os Detalhes da Disciplina no Modal
         * @param {int} idTurmaDisc Turma/Disciplina
         */
        function detalhesDisciplina(idTurmaDisc) {
            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(self.objContexto.cursoSelecionado.CODCOLIGADA,
                                                                         idTurmaDisc, self.objContexto.cursoSelecionado.RA);
        }

        /**
         * View com o Widget
         * @returns {string} View do Widget
         */
        function retornaWidgetUrl() {
            return 'js/aluno/widgets/widget-area.view.html';
        }

    }
});
