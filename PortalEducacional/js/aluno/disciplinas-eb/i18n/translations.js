[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Disciplinas",
			"en": "Subjects",
			"es": "Materias"
        },
        "l-Nenhum-Registro-Encontrado": {
            "pt": "Nenhum registro encontrado!",
			"en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },
        "l-concluida": {
            "pt": "Concluída",
			"en": "Finished",
			"es": "Concluida"
        },
        "l-nao-concluida": {
            "pt": "Não Concluída",
			"en": "Not finished",
			"es": "No concluida"
        },
        "l-pendente": {
            "pt": "Pendente",
			"en": "Pending",
			"es": "Pendiente"
        },
        "l-todos": {
            "pt": "Todos",
			"en": "All",
			"es": "Todos"
        },
        "l-pendentes": {
            "pt": "Pendentes",
			"en": "Pending",
			"es": "Pendientes"
        },
        "l-em-curso": {
            "pt": "Em curso",
			"en": "In course",
			"es": "En curso"
        },
        "l-concluidas": {
            "pt": "Concluídas",
			"en": "Finished",
			"es": "Concluidas"
        },
        "l-concluidos": {
            "pt": "Concluídos",
			"en": "Finished",
			"es": "Concluidos"
        },
        "l-numero": {
            "pt": "Número",
			"en": "Number",
			"es": "Número"
        },
        "l-todas": {
            "pt": "Todas",
			"en": "All",
			"es": "Todas"
        },
        "l-situacao": {
            "pt": "Situação",
			"en": "Status",
			"es": "Situación"
        },
        "l-periodo-letivo": {
            "pt": "Período Letivo",
			"en": "School term",
			"es": "Período lectivo"
        },
        "l-cod-disciplina": {
            "pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Subject",
			"es": "Materia"
        },
        "l-creditos": {
            "pt": "Créditos",
			"en": "Credit",
			"es": "Créditos"
        },
        "l-carga-horaria": {
            "pt": "CH",
			"en": "CH",
			"es": "CH"
        },
        "l-tipo": {
            "pt": "Tipo",
			"en": "Type",
			"es": "Tipo"
        },
        "l-frequencia": {
            "pt": "Frequência",
			"en": "Attendance",
			"es": "Frecuencia"
        },
        "l-nota-final": {
            "pt": "Nota Final",
			"en": "Invoice",
			"es": "Nota final"
        },
        "l-conceito": {
            "pt": "Conceito",
			"en": "Concept",
			"es": "Concepto"
        },
        "l-periodo": {
            "pt": "Período",
			"en": "Period",
			"es": "Período"
        },
        "l-resumo": {
            "pt": "Resumo",
			"en": "Summary",
			"es": "Resumen"
        },
        "l-expandir-todos": {
            "pt": "Expandir Todos",
			"en": "Expand all",
			"es": "Expandir todos"
        },
        "l-recolher-todos": {
            "pt": "Recolher Todos",
			"en": "Collapse all",
			"es": "Pagar todos"
        } ,
        "l-turma": {
            "pt": "Turma",
			"en": "Class",
			"es": "Grupo"
        },
        "l-ciclo-ano": {
            "pt": "Ciclo/Ano",
			"en": "Cycle/Year",
			"es": "Ciclo/Año"
        },
        "l-codigo": {
            "pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student board",
		  "es": "Panel del alumno"
        }
    }
]
