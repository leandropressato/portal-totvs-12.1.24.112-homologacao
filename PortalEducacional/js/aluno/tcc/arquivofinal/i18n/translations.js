[{
    "l-home": {
        "pt": "Início",
		"en": "Start",
		"es": "Inicio"
    },
    "l-arquivo-final": {
        "pt": "Arquivo final",
		"en": "End file",
		"es": "Archivo final"
    },
    "l-comentario": {
        "pt": "Comentário do arquivo",
		"en": "File comment",
		"es": "Comentario del archivo"
    },
    "l-data-envio": {
        "pt": "Data de envio do arquivo",
		"en": "File submission date",
		"es": "Fecha de envío del archivo"
    },
    "l-nome-arquivo": {
        "pt": "Nome do arquivo",
		"en": "File name",
		"es": "Nombre del archivo"
    },
    "l-comentario-arquivo": {
        "pt": "Comentário sobre o arquivo",
		"en": "Comment about the file",
		"es": "Comentario sobre el archivo"
    },
    "btn-upload-arquivo": {
        "pt": "Enviar arquivo final",
		"en": "Send end file",
		"es": "Enviar archivo final"
    }
}]