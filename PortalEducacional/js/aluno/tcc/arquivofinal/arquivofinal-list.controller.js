/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/tcc/tcc.module',
        'aluno/tcc/tcc.factory',
        'aluno/tcc/acompanhamento/acompanhamento-upload.controller',
        'utils/edu-enums.constants',
        'utils/edu-utils.factory'
    ],
    function() {

        'use strict';

        angular
            .module('eduDocumentosModule')
            .controller('eduArquivoFinalListController', EduArquivoFinalListController);

        EduArquivoFinalListController.$inject = [
            'TotvsDesktopContextoCursoFactory',
            'eduTccFactory',
            'eduEnumsConsts',
            'eduUtilsFactory',
            'i18nFilter',
            '$scope',
            '$state',
            '$modal',
            'eduUtilsService',
            'totvs.app-notification.Service',
            '$filter'
        ];

        function EduArquivoFinalListController(TotvsDesktopContextoCursoFactory,
            eduTccFactory,
            EduEnumsConsts,
            eduUtilsFactory,
            i18nFilter,
            $scope,
            $state,
            $modal,
            eduUtilsService,
            appNotificationService,
            $filter) {

            var self = this;

            self.objArquivosList = [];
            self.periodoLetivo = 0;
            self.idTCC = 0;
            self.podeEnviarArquivoFinal = false;

            self.getArquivosFinais = getArquivosFinais;
            self.getInfoTcc = getInfoTcc;
            self.retornaNomeArquivo = retornaNomeArquivo;
            self.ModalUploadArquivo = ModalUploadArquivo;
            self.onclickDownloadArquivo = onclickDownloadArquivo;
            self.onclickExcluiArquivo = onclickExcluiArquivo;

            init();

            function init() {
                self.getInfoTcc();
            }


            /**
             * Busca qual é o Id do Tcc e posteriormente verifica se existe arquifo final entregue.
             */
            function getInfoTcc() {

                eduTccFactory.retornaInformacoesTcc($state.params.idTurmaDisc, function(result) {
                    if (result) {
                        self.objInfoTcc = result;

                        if (self.objInfoTcc.STCC.length > 0 &&
                            self.objInfoTcc.STCC[0].IDTCC != null) {
                            self.idTCC = self.objInfoTcc.STCC[0].IDTCC;

                            eduTccFactory.getPodeEnviarArquivoFinal(self.idTCC, function(resultPodeEnviar) {
                                if (resultPodeEnviar) {
                                    self.podeEnviarArquivoFinal = resultPodeEnviar.value;
                                }
                            });

                            self.getArquivosFinais(self.idTCC);
                        }
                    }
                });
            }

            /**
             * Retorna o arquivo final.
             */
            function getArquivosFinais(idTcc) {
                self.objArquivosList = [];

                eduTccFactory.getListaArquivos(idTcc, true, function(result) {
                    if (result) {
                        self.objArquivosList = result;
                    }
                });

            }

            /**
             * Exibe o nome do arquivo.
             */
            function retornaNomeArquivo(arquivo) {
                return arquivo.PATHARQUIVO.split('#')[0];
            }

            /**
             * Faz a chamada do Modal para Uploado do arquivo Final
             */
            function ModalUploadArquivo() {

                var params = {
                    idTcc: self.idTCC = self.idTCC,
                    arquivoFinal: true
                };

                $modal.open({
                    templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/tcc/acompanhamento/acompanhamento-upload.view.html',
                    controller: 'eduAcompanhamentoUploadController',
                    controllerAs: 'controller',
                    size: 'md',
                    scope: $scope,
                    resolve: {
                        parametros: function() {
                            return params;
                        }
                    },
                    backdrop: 'true'
                });
            }

            /**
             * Faz a chamada do download do arquivo.
             */
            function onclickDownloadArquivo(idArquivo, integradoECM) {
                eduTccFactory.getDetalheArquivo(idArquivo, integradoECM, function(result) {
                    if (result) {

                        eduUtilsService.abrirJanelaDownloadArquivo(
                            result.SARQUIVOS["0"].PATHARQUIVO,
                            result.SARQUIVOS["0"].ARQUIVO);
                    }
                });
            }

            /**
             * Faz a chamada da Exclusão do arquivo Final.
             */
            function onclickExcluiArquivo(idArquivo, integracaoECM) {
                //O usuário é informado que será redirecionado para o site do banco,
                //e confirma se deseja continuar ou não.
                appNotificationService.question({
                    title: $filter('i18n')('l-excluir-arquivo', [], 'js/aluno/tcc/acompanhamento'),
                    text: $filter('i18n')('l-texto-excluir-arquivo', [], 'js/aluno/tcc/acompanhamento'),
                    size: 'sm',
                    cancelLabel: 'l-no',
                    confirmLabel: 'l-yes',
                    callback: function(opcaoEscolhida) {
                        if (opcaoEscolhida) {

                            eduTccFactory.setExcluiArquivoAsync(idArquivo, integracaoECM, function(result) {
                                if (result) {
                                    getInfoTcc();
                                }
                            });
                        }
                    }
                });
            }
        }
    });