/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduTccModule
 * @name eduTccFactory
 * @object factory
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @dependencies $totvsresource
 *
 * @description Factory utilizada nas Tcc.
 */
define(['aluno/tcc/tcc.module'], function() {

    'use strict';

    angular
        .module('eduTccModule')
        .factory('eduTccFactory', EduTccFactory);

    EduTccFactory.$inject = ['$totvsresource'];

    function EduTccFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.getListaAcompanhamentos = getListaAcompanhamentos; // Busca os acompanhamentos do TCC
        factory.getAcompanhamentoAluno = getAcompanhamentoAluno;
        factory.getDetalheArquivo = getDetalheArquivo;
        factory.setAcompanhamentoAluno = setAcompanhamentoAluno;
        factory.getListaArquivos = getListaArquivos;
        factory.setUploadArquivoAsync = setUploadArquivoAsync;
        factory.setExcluiArquivoAsync = setExcluiArquivoAsync;
        factory.findRecords = findRecords; // Busca todas as Tcc
        factory.updateRecord = updateRecord; // Atualiza uma ocorrencia
        factory.getTccBanca = getTccBanca;
        factory.retornaInformacoesTcc = retornaInformacoesTcc;
        factory.retornaLinhaPesquisa = retornaLinhaPesquisa;
        factory.retornaMatriculaVinculadas = retornaMatriculaVinculadas;
        factory.retornaProfessoresOrientadores = retornaProfessoresOrientadores;
        factory.retornaTipo = retornaTipo;
        factory.salvarDadosTCC = salvarDadosTCC;
        factory.retornaOrientadorTCC = retornaOrientadorTCC;
        factory.convidaOrientador = convidaOrientador;
        factory.retornaAlunoGrupo = retornaAlunoGrupo;
        factory.retornaAlunoDisponiveisGrupo = retornaAlunoDisponiveisGrupo;
        factory.incluiAlunoGrupo = incluiAlunoGrupo;
        factory.getPodeEnviarArquivoFinal = getPodeEnviarArquivoFinal;
        factory.getPodeEnviarArquivoDeAcompanhamento = getPodeEnviarArquivoDeAcompanhamento;
        factory.gerarRelatorioAutorizacaoPublicacaoTCCAsync = gerarRelatorioAutorizacaoPublicacaoTCCAsync;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************
        /**
         * Busca todas as TCC
         *
         * @param {int} idTurmaDisc
         * @param {int} startAt
         * @param {int} limitAt
         * @param {function} callback
         * @returns {object}
         */
        function findRecords(idTurmaDisc, startAt, limitAt, callback) {
            var pStart = startAt || 0;
            var pLimit = limitAt || 0;
            var pIdTurmaDisc = idTurmaDisc || 0;

            var parameters = {};
            parameters['method'] = 'Tcc';
            parameters['start'] = pStart;
            parameters['limit'] = pLimit;
            parameters['idTurmaDisc'] = pIdTurmaDisc;

            return factory.TOTVSQuery(parameters, callback);
        }

        function updateRecord(id, model, callback) {
            var parameters = {
                method: 'ResponsavelCiente',
                id: id
            };

            return factory.TOTVSUpdate(parameters, model, callback);
        }

        function getListaAcompanhamentos(idTurmaDisc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/RetornaListaAcompanhamentos',
                idTurmaDisc: idTurmaDisc
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getAcompanhamentoAluno(idAcompanhamentoTcc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/RetornaAcompanhamentoAluno',
                idAcompanhamentoTcc: idAcompanhamentoTcc
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getDetalheArquivo(idArquivo, integradoECM, callback) {
            var parameters = {
                method: 'Arquivo/RetornaArquivoAcompanhamentoTcc',
                idArquivo: idArquivo,
                integracaoECM: integradoECM
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getPodeEnviarArquivoFinal(idTcc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/PodeEnviarArquivoFinal',
                idTcc: idTcc
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getPodeEnviarArquivoDeAcompanhamento(idAcompanhamentoTcc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/PodeEnviarArquivoDeAcompanhamento',
                idAcompanhamentoTcc: idAcompanhamentoTcc
            };

            return factory.TOTVSQuery(parameters, callback);
        }

        function setAcompanhamentoAluno(idAcompanhamentoTcc, model, callback) {
            var parameters = {
                method: 'Disciplina/TCC/SalvaAcompanhamentoAluno',
                idAcompanhamentoTcc: idAcompanhamentoTcc
            };

            return factory.TOTVSSave(parameters, model, callback);
        }

        function setUploadArquivoAsync(idTcc, arquivo, nomeArquivo, pathArquivo, arquivofinal, callback) {

            var parameters = {
                    method: 'Arquivo/UploadArquivoAcompanhamentoTcc',
                    idTcc: idTcc, //Id do TCC para arquivo final e IdAcompnhamentoTcc para arquivo de acompanhamento.
                    arquivoFinal: arquivofinal
                },
                model = {
                    Arquivo: arquivo,
                    NomeArquivo: nomeArquivo,
                    PathArquivo: pathArquivo
                };

            return factory.TOTVSSave(parameters, [model], callback);
        }

        function setExcluiArquivoAsync(idArquivo, integracaoECM, callback) {
            var parameters = {
                method: 'Arquivo/ExcluiArquivoAcompanhamentoTcc',
                idArquivo: idArquivo,
                integracaoECM: 0
            };

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna a lista de arquivos de um acompanhamento
         * @param   {int} id Identificador do acompanhamento do TCC ou do TCC (arquivo final)
         * @param   {bool} arquivoFinal Indica se o arquivo é do Acompanhamento ou do Arquivo final
         * @param   {function} callback    Função de retorno
         * @returns {object} Objeto de lista de arquivos de um acompanhamento
         */
        function getListaArquivos(id, arquivoFinal, callback) {
            var parameters = {};

            parameters = {
                method: 'Arquivo/RetornaArquivosTcc',
                idTcc: id,
                arquivoFinal: arquivoFinal
            };

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Obtém os dados da banca de TCC
         * @param   {int} idTurmaDisc Identificador da Turma/Disciplina
         * @param   {function} callback    Função de retorno
         * @returns {object} Objeto com os dados da banca de TCC
         */
        function getTccBanca(idTurmaDisc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/Banca',
                idTurmaDisc: idTurmaDisc
            };

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna as informações do TCC
         *
         * @param {int} idTurmaDisc
         * @param {function} callback
         * @returns
         */
        function retornaInformacoesTcc(idTurmaDisc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/{idTurmaDisc}',
                idTurmaDisc: idTurmaDisc
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getTcc(idTurmaDisc, callback) {
            var parameters = {
                method: 'Disciplina/TCC/RetornaTcc',
                idTurmaDisc: idTurmaDisc
            };

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna os professoes/orientadores disponíveis
         *
         * @param {int} idLinhaPesquisa
         * @param {int} idTcc
         * @param {function} callback
         * @returns
         */
        function retornaProfessoresOrientadores(idTcc, idLinhaPesquisa, callback) {
            var parameters = {
                method: 'TCC/Professor/Orientadores',
                idLinhaPesquisa: idLinhaPesquisa,
                idTcc: idTcc
            }

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorna os orientadores do TCC
         *
         * @param {int} idTcc
         * @param {function} callback
         * @returns
         */
        function retornaOrientadorTCC(idTcc, callback) {
            var parameters = {
                method: 'TCC/{idTcc}/Orientadores',
                idTcc: idTcc
            }

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorna as informações de linha de pesquisa
         *
         * @param {function} callback
         * @returns
         */
        function retornaLinhaPesquisa(callback) {
            var parameters = {
                method: 'TCC/LinhaPesquisa',
            }

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorna as matrículas vínculadas ao TCC
         *
         * @param {function} callback
         * @returns
         */
        function retornaMatriculaVinculadas(callback) {
            var parameters = {
                method: 'TCC/MatriculasVinculadas'
            }

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorna o tipo de TCC
         *
         * @param {function} callback
         * @returns
         */
        function retornaTipo(callback) {
            var parameters = {
                method: 'TCC/Tipo'
            }
            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Salva os dados de TCC
         *
         * @param {int} idTurmaDisc
         * @param {bool} novoRegistro
         * @param {array} model
         * @param {function} callback
         * @returns
         */
        function salvarDadosTCC(idTurmaDisc, novoRegistro, model, callback) {
            var parameters = {
                method: 'Disciplina/TCC/{idTurmaDisc}',
                idTurmaDisc: idTurmaDisc
            }

            if (novoRegistro) {
                return factory.TOTVSSave(parameters, model, callback);
            } else {
                return factory.TOTVSUpdate(parameters, model, callback);
            }
        }

        /**
         * Realiza o cadastro do convite ao professor/orientador
         *
         * @param {int} idTcc
         * @param {string} codProf
         * @param {function} callback
         * @returns
         */
        function convidaOrientador(idTcc, codProf, callback) {
            var parameters = {
                method: 'TCC/{idTcc}/Orientadores',
                idTcc: idTcc,
                codProf: codProf
            }

            return factory.TOTVSSave(parameters, {}, callback);
        }

        /**
         * Retorna os alunos pertencentes ao grupo de TCC
         *
         * @param {int} idTcc
         * @param {function} callback
         * @returns
         */
        function retornaAlunoGrupo(idTcc, callback) {
            var parameters = {
                method: 'TCC/{idTcc}/Grupo',
                idTcc: idTcc,
            }

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorna a lista de alunos disponíveis para o grupo
         *
         * @param {int} idTcc
         * @param {function} callback
         * @returns
         */
        function retornaAlunoDisponiveisGrupo(idTcc, callback) {
            var parameters = {
                method: 'TCC/{idTcc}/GrupoAlunoTurma',
                idTcc: idTcc,
            };

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Inclui aluno ao grupo de TCC
         *
         * @param {int} idTcc
         * @param {int} idTurmaDisc
         * @param {string} ra
         * @param {function} callback
         * @returns
         */
        function incluiAlunoGrupo(idTcc, idTurmaDisc, ra, callback) {

            var parameters = {
                method: 'TCC/{idTcc}/GrupoAlunoTurma',
                idTcc: idTcc,
                idTurmaDisc: idTurmaDisc,
                ra: ra
            };

            return factory.TOTVSSave(parameters, {}, callback);
        }

        function gerarRelatorioAutorizacaoPublicacaoTCCAsync(idTcc, callback) {

            var parameters = {
                method: 'TCC/{idTcc}/RelatorioAutorizacaoPublicacaoTCC',
                idTcc: idTcc,
            };

            return factory.TOTVSQuery(parameters, callback);
        }
    }
});
