/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduTccModule
 * @name EduTccController
 * @object controller
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @requires tcc.module
 *
 * @dependencies eduTccFactory
 *
 * @description Controller do Tcc
 */
define(['aluno/tcc/tcc.module',
    'aluno/tcc/tcc.factory',
    'aluno/tcc/acompanhamento/acompanhamento-upload.controller',
    'aluno/tcc/acompanhamento/acompanhamento-observacoes.controller',
    'utils/edu-enums.constants',
    'utils/edu-utils.service'
], function() {

    'use strict';

    angular
        .module('eduTccModule')
        .controller('eduAcompanhamentoTccController', EduAcompanhamentoTccController);

    EduAcompanhamentoTccController.$inject = [
        'TotvsDesktopContextoCursoFactory',
        'eduTccFactory',
        'eduUtilsFactory',
        'eduEnumsConsts',
        'i18nFilter',
        '$state',
        '$modal',
        '$filter',
        '$scope',
        'eduUtilsService',
        'totvs.app-notification.Service'
    ];

    function EduAcompanhamentoTccController(
        objContextoCursoFactory,
        eduTccFactory,
        eduUtilsFactory,
        EduEnumsConsts,
        i18nFilter,
        $state,
        $modal,
        $filter,
        $scope,
        eduUtilsService,
        appNotificationService
    ) {

        var self = this;

        self.objContexto = {};
        self.objAcompanhamentoList = [];
        self.objArquivosList = [];
        self.periodoLetivo = 0;
        self.PermiteAceiteAlunoAcompanhamento = false;

        self.getContexto = getContexto;
        self.getSituacaoAcompanhamento = getSituacaoAcompanhamento;
        self.permiteExcluirArquivo = permiteExcluirArquivo;
        self.periodoLetivoAlterado = periodoLetivoAlterado;
        self.onclickModalUploadArquivo = onclickModalUploadArquivo;
        self.onclickModalObservacaoAluno = onclickModalObservacaoAluno;
        self.onclickDownloadArquivo = onclickDownloadArquivo;
        self.onclickExcluiArquivo = onclickExcluiArquivo;
        self.retornaListaAcompanhamentos = retornaListaAcompanhamentos;
        self.retornaListaArquivos = retornaListaArquivos;
        self.retornaNomeArquivo = retornaNomeArquivo;
        self.retornaSimNao = retornaSimNao;
        self.retornaOrigemArquivo = retornaOrigemArquivo;

        init();

        function init() {
            self.getContexto();
            carregarParametrosEdu();
        }

        /**
         * Atualiza a listagem de acompanhamentos ao alterar o período letivo.
         */
        function periodoLetivoAlterado() {
            self.retornaListaAcompanhamentos();

        }

        /**
         * Retorna o contexto ao carregar a página.
         */
        function getContexto() {
            self.objContexto = objContextoCursoFactory.getCursoSelecionado();
        }

        /**
         * Retorna o nome do arquivo
         */
        function retornaNomeArquivo(arquivo) {
            return arquivo.PATHARQUIVO.split('#')[0];
        }

        /**
         * Recarrega a listagem de uma tab específica
         */
        function atualizarListaArquivos() {
            var itemExpandido = $('.item-details.open');

            if (itemExpandido.length === 0) {
                $timeout(function() { $('.info-more a').click(); }, 100);
            }
        }

        function carregarParametrosEdu() {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function(params) {
                self.permiteAceiteAlunoAcompanhamento = params.PermiteAceiteAlunoAcompanhamento;
                //Realiza a chamada da listagem apenas após retornar os parâmatros para exibir ou não
                //as os botões de acordo com as permissões.
                self.retornaListaAcompanhamentos();
            });
        }

        /**
         * Retorna o texto "Sim" ou "Não" para exibição do usuário
         */
        function retornaSimNao(valor) {
            var conteudo;
            if (valor === 'S') {
                conteudo = $filter('i18n')('l-sim', [], 'js/aluno/tcc/acompanhamento');
            } else {
                conteudo = $filter('i18n')('l-nao', [], 'js/aluno/tcc/acompanhamento');
            }

            return conteudo;
        }

        /**
         * Função que valida se o aluno pode ou não excluir o arquivo
         * O aluno não pode excluir arquivos que são postados pelo professor.
         */
        function permiteExcluirArquivo(chaveRM, objAcompanhamento) {
            //ChaveRM é divida em 3 partes: CODCOLIGADA;IDACOMPANHAMENTOTCC;ORIGEM (P: Professor / A: Aluno)
            var origem = chaveRM.split(';')[2];
            if (origem === EduEnumsConsts.EduTipoAvalInstEnum.Professor) {
                return false; //O aluno não pode excluir arquivos que o professor posta!
            } else if (origem === EduEnumsConsts.EduTipoAvalInstEnum.Aluno) {

                if (objAcompanhamento.REUNIAOENCERRADA === 'S') {
                    return false; //Se estiver encerrado, não deve mais excluir o arquivo.
                } else {

                    return true;
                }
            }
        }

        /**
         * Retorna se o arquivo foi enviado pelo Aluno ou Professor
         */
        function retornaOrigemArquivo(chaveRM) {
            //ChaveRM é divida em 3 partes: CODCOLIGADA;IDACOMPANHAMENTOTCC;ORIGEM (P: Professor / A: Aluno)
            var origem = chaveRM.split(';')[2];
            if (origem === EduEnumsConsts.EduTipoAvalInstEnum.Professor) {
                return $filter('i18n')('l-professor', [], 'js/aluno/tcc/acompanhamento');
            } else if (origem === EduEnumsConsts.EduTipoAvalInstEnum.Aluno) {
                return $filter('i18n')('l-aluno', [], 'js/aluno/tcc/acompanhamento');
            }
        }

        /**
         * Função de definição do status do acompanhamento
         *
         * @param {Object} item - Verifica o status do Acompanhamento
         * @returns classe CSS do status
         */
        function getSituacaoAcompanhamento(item) {
            var classeLegenda = 'legenda-verde legenda-content-1';

            if (item.STATUS === 1) {
                classeLegenda = 'legenda-verde legenda-content-1'; //Em andamento
            } else if (item.STATUS === 2) {
                classeLegenda = 'legenda-vermelho legenda-content-2'; //Aceite pendente
            } else if (item.STATUS === 3) {
                classeLegenda = 'legenda-laranja legenda-content-3'; //Encerramento pendente
            } else if (item.STATUS === 4) {
                classeLegenda = 'legenda-cinza legenda-content-4'; //Encerrado
            }

            return 'legenda ' + classeLegenda;
        }

        /**
         * Faz a chamada ao Modal de Upload de arquivo
         *
         * @param {int} idTcc - Id do Acompanhamento que será enviado o arquivo no Modal.
         */
        function onclickModalUploadArquivo(idTcc) {

            var params = {
                idTcc: idTcc,
                arquivoFinal: false
            };

            $modal.open({
                templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/tcc/acompanhamento/acompanhamento-upload.view.html',
                controller: 'eduAcompanhamentoUploadController',
                controllerAs: 'controller',
                size: 'md',
                scope: $scope,
                resolve: {
                    parametros: function() {
                        return params;
                    }
                },
                backdrop: 'true'
            });
        }

        /**
         * Faz a chamada ao modal de Observações do aluno
         */
        function onclickModalObservacaoAluno(objAcompanhamento) {

            var params = {
                idAcompanhamentoTcc: objAcompanhamento.IDACOMPANHAMENTOTCC,
                encerrado: objAcompanhamento.REUNIAOENCERRADA,
                permiteAceiteAlunoAcompanhamento: self.permiteAceiteAlunoAcompanhamento
            };

            $modal.open({
                templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/tcc/acompanhamento/acompanhamento-observacoes.view.html',
                controller: 'eduAcompanhamentoObservacoesController',
                controllerAs: 'controller',
                size: 'lg',
                scope: $scope,
                resolve: {
                    parametros: function() {
                        return params;
                    }
                },
                backdrop: 'true'
            });
        }

        /**
         * Realiza o download de um arquivo específico.
         */
        function onclickDownloadArquivo(idArquivo, integradoECM) {
            eduTccFactory.getDetalheArquivo(idArquivo, integradoECM, function(result) {
                if (result) {

                    eduUtilsService.abrirJanelaDownloadArquivo(
                        result.SARQUIVOS["0"].PATHARQUIVO,
                        result.SARQUIVOS["0"].ARQUIVO);
                }
            });
        }

        /**
         * Exclui um arquivo de Acompanhamento específico
         */
        function onclickExcluiArquivo(idArquivo, integracaoECM, descricaoArquivo) {
            //O usuário é informado que será redirecionado para o site do banco,
            //e confirma se deseja continuar ou não.
            appNotificationService.question({
                title: $filter('i18n')('l-excluir-arquivo', [], 'js/aluno/tcc/acompanhamento'),
                text: $filter('i18n')('l-texto-excluir-arquivo', [], 'js/aluno/tcc/acompanhamento') + descricaoArquivo + '?',
                size: 'md',
                cancelLabel: 'l-no',
                confirmLabel: 'l-yes',
                callback: function(opcaoEscolhida) {
                    if (opcaoEscolhida) {
                        eduTccFactory.setExcluiArquivoAsync(idArquivo, integracaoECM, function(result) {
                            if (result) {
                                retornaListaAcompanhamentos(function() {
                                    atualizarListaArquivos();
                                });
                            }
                        });
                    }
                }
            });
        }

        /**
         * Retorna a listagem de acompanhamentos do Aluno
         */
        function retornaListaAcompanhamentos(callback) {
            eduTccFactory.getListaAcompanhamentos($state.params.idTurmaDisc, function(result) {
                if (result) {
                    self.objAcompanhamentoList = result.STCCACOMPANHAMENTO;
                }
            });
        }

        /**
         * Retorna a listagem de arquivos de um acompanhamento de TCC.
         */
        function retornaListaArquivos(objAcompanhamento) {
            if (!angular.isDefined(objAcompanhamento.ListaArquivos)) {
                eduTccFactory.getListaArquivos(objAcompanhamento.IDACOMPANHAMENTOTCC, false, function(result) {
                    objAcompanhamento.ListaArquivos = [];
                    objAcompanhamento.TemArquivos = false;
                    if (result) {
                        objAcompanhamento.ListaArquivos = result;
                        objAcompanhamento.TemArquivos = true;
                    }
                });
            }
        }
    }
});