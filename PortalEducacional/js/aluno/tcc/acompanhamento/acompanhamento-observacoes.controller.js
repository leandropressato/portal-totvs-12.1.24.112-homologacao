/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduTccModule
 * @name eduAcompanhamentoTccController
 * @object controller
 *
 * @created 2016-11-23 v12.1.15
 * @updated
 *
 * @requires Tcc.module
 *
 * @dependencies eduTccFactory
 *
 * @description Controller do modal de envio de comentários do aluno de um acompanhamento do TCC.
 */
define([
    'aluno/tcc/tcc.module',
    'aluno/tcc/tcc.route',
    'aluno/tcc/tcc.factory'
], function() {
    'use strict';

    angular
        .module('eduTccModule')
        .controller('eduAcompanhamentoObservacoesController', EduAcompanhamentoObservacoesController);

    EduAcompanhamentoObservacoesController.$inject = ['$scope',
        'eduTccFactory',
        '$filter',
        'parametros',
        '$state',
        '$modalInstance',
        'totvs.app-notification.Service',
        'i18nFilter'
    ];

    function EduAcompanhamentoObservacoesController($scope,
        eduTccFactory,
        $filter,
        parametros,
        $state,
        $modalInstance,
        appNotificationService,
        i18nFilter) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;

        //Contém a modelo de dados dos campos disponíveis no formulário
        self.model = {};
        self.objAcompanhamentoTcc = [];
        self.encerrado = false;
        self.permiteAceiteAlunoAcompanhamento = true;

        self.salvaAlteracao = salvaAlteracao;
        self.retornaAcompanhamentoAluno = retornaAcompanhamentoAluno;
        self.modalInstance = $modalInstance;



        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        init();

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        function init() {
            self.encerrado = parametros.encerrado;
            self.permiteAceiteAlunoAcompanhamento = parametros.permiteAceiteAlunoAcompanhamento;
            retornaAcompanhamentoAluno();
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************
        function salvaAlteracao() {
            if (self.model.COMENTARIOALUNO == null) {
                appNotificationService.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-comentario', '[]', 'js/aluno/tcc/acompanhamento')
                });
            } else {
                eduTccFactory.setAcompanhamentoAluno(parametros.idAcompanhamentoTcc, [self.model], function(result) {
                    if (result) {
                        reload();
                        appNotificationService.notify({
                            type: 'success',
                            title: i18nFilter('l-observacao-aluno-sucesso', '[]', 'js/aluno/tcc/acompanhamento'),
                            detail: i18nFilter('l-observacao-aluno-salvo', '[]', 'js/aluno/tcc/acompanhamento')
                        });
                    }
                });
            }
        }

        function retornaAcompanhamentoAluno() {
            eduTccFactory.getAcompanhamentoAluno(parametros.idAcompanhamentoTcc, function(result) {
                if (result) {
                    self.objAcompanhamentoTcc = result;

                    self.model.COMENTARIOALUNO = self.objAcompanhamentoTcc.STCCACOMPANHAMENTO[0].COMENTARIOALUNO;
                    self.model.ORIENTACOESPROF = self.objAcompanhamentoTcc.STCCACOMPANHAMENTO[0].ORIENTACOESPROF;
                    if (self.objAcompanhamentoTcc.STCCACOMPANHAMENTO[0].ACEITEALUNO === 'S')
                        self.model.ACEITEALUNO = true;
                    else
                        self.model.ACEITEALUNO = false;
                }
            });
        }

        /**
         * Redireciona para a página da turma/disciplina
         */
        function onclickCancelar() {
            $modalInstance.dismiss();
        }

        /**
         * Recarrega a rota
         */
        function reload() {
            $scope.$parent.controller.retornaListaAcompanhamentos();
            $modalInstance.dismiss();
        }

    }
});