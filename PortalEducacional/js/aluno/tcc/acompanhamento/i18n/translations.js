[{
    "l-home": {
        "pt": "Início",
		"en": "Start",
		"es": "Inicio"
    },
    "l-titulo": {
        "pt": "Acompanhamentos",
		"en": "Follow-up",
		"es": "Acompañamientos"
    },
    "l-aluno": {
        "pt": "Aluno",
		"en": "Student",
		"es": "Alumno"
    },
    "l-professor": {
        "pt": "Professor",
		"en": "Teacher",
		"es": "Profesor"
    },
    "l-origem-arquivo": {
        "pt": "Origem do arquivo",
		"en": "File origin",
		"es": "Origen del archivo"
    },
    "l-observacoes-aluno": {
        "pt": "Observações do aluno",
		"en": "Student's notes",
		"es": "Observaciones del alumno"
    },
    "l-observacao-aluno-salvo": {
        "pt": "O comentário do aluno foi salvo com sucesso.",
		"en": "The student's comment was successfully saved.",
		"es": "El comentario del alunmo se grabó con éxito."
    },
    "l-observacao-aluno-sucesso": {
        "pt": "Comentário do aluno",
		"en": "Student's comment",
		"es": "Comentario del alumno"
    },
    "l-Nenhum-Registro-Encontrado": {
        "pt": "Comentário do aluno",
		"en": "Student's comment",
		"es": "Comentario del alumno"
    },
    "l-titulo-modal-upload": {
        "pt": "Upload de arquivo",
		"en": "File upload",
		"es": "Upload de archivo"
    },
    "l-msg-campo-obrigatorio-comentario": {
        "pt": "É obrigatório o preenchimento do campo Comentário do aluno.",
		"en": "The completion of student's comment is required.",
		"es": "Es obligatorio cumplimentar el campo Comentario del alumno."
    },
    "l-titulo-modal-observacao": {
        "pt": "Comentário do acompanhamento",
		"en": "Follow-up comment",
		"es": "Comentario del acompañamiento"
    },
    "l-salvar": {
        "pt": "Salvar",
		"en": "Save",
		"es": "Grabar"
    },
    "l-cancelar": {
        "pt": "Cancelar",
		"en": "Cancel",
		"es": "Anular"
    },
    "l-enviar-arquivo": {
        "pt": "Enviar arquivo",
		"en": "Send file",
		"es": "Enviar archivo"
    },
    "l-sim": {
        "pt": "Sim",
		"en": "Yes",
		"es": "Sí"
    },
    "l-nao": {
        "pt": "Não",
		"en": "No",
		"es": "No"
    },
    "l-arquivo-enviado": {
        "pt": "Arquivo enviado com sucesso.",
		"en": "File successfully sent.",
		"es": "Archivo enviado con éxito."
    },
    "l-msg-campo-obrigatorio": {
        "pt": "É obrigatório o preenchimento do campo Observação.",
		"en": "The completion of the field Note is required.",
		"es": "Es obligatorio cumplimentar el campo Observación."
    },
    "l-erro-envio": {
        "pt": "Não foi possível realizar Upload do arquivo.",
		"en": "Unable to upload file.",
		"es": "No fue posible realizar Upload del archivo."
    },
    "l-comentario-arquivo": {
        "pt": "Comentário sobre o arquivo",
		"en": "Comment about file",
		"es": "Comentario sobre el archivo"
    },
    "l-editar-acompanhamento": {
        "pt": "Editar acompanhamento",
		"en": "Edit follow-up",
		"es": "Editar acompañamiento"
    },
    "l-orientacao-professor": {
        "pt": "Orientação do professor",
		"en": "Teacher's supervision",
		"es": "Asesoría del profesor"
    },
    "l-comentario-aluno": {
        "pt": "Comentário do aluno",
		"en": "Student's comment",
		"es": "Comentario del alumno"
    },
    "btn-download-arquivo": {
        "pt": "Download",
		"en": "Download",
		"es": "Descarga"
    },
    "btn-excluir-arquivo": {
        "pt": "Excluir",
		"en": "Delete",
		"es": "Borrar"
    },
    "l-texto-excluir-arquivo": {
        "pt": "Deseja realmente excluir o arquivo",
		"en": "Do you really want to delete the file",
		"es": "Realmente desea borrar el archivo"
    },
    "l-excluir-arquivo": {
        "pt": "Excluir arquivo",
		"en": "Delete file",
		"es": "Borrar archivo"
    },
    "l-data-prevista": {
        "pt": "Data prevista",
		"en": "Estimated date",
		"es": "Fecha prevista"
    },
    "l-data-limite-entrega": {
        "pt": "Data limite entrega arquivo",
		"en": "Limit date to hand file in",
		"es": "Fecha límite entrega archivo"
    },
    "l-data-efetiva": {
        "pt": "Data efetiva",
		"en": "Effective date",
		"es": "Fecha efectiva"
    },
    "l-aceite-aluno": {
        "pt": "Aceite do aluno",
		"en": "Student's acceptance",
		"es": "Aceptación del alumno"
    },
    "l-encerrado": {
        "pt": "Encerrado",
		"en": "Closed",
		"es": "Finalizado"
    },
    "l-data-cadastro": {
        "pt": "Data de cadastro",
		"en": "Register date",
		"es": "Fecha del registro"
    },
    "l-nome-arquivo": {
        "pt": "Nome do arquivo",
		"en": "File name",
		"es": "Nombre del archivo"
    },
    "l-arquivos": {
        "pt": "Lista de arquivos anexados",
		"en": "List of files attached",
		"es": "Lista de archivos adjuntos"
    },
    "l-data-confirmada": {
        "pt": "Data confirmada",
		"en": "Confirmed date",
		"es": "Fecha confirmada"
    },
    "l-aceite-pendente": {
        "pt": "Aceite pendente (aluno)",
		"en": "Pending acceptance (student)",
		"es": "Aceptación pendiente (alumno)"
    },
    "l-encerramento-pendente": {
        "pt": "Encerramento pendente (orientador)",
		"en": "Pending closing (supervisor)",
		"es": "Finalización pendiente (asesor)"
    },    
    "l-em-aberto": {
        "pt": "Em aberto",
		"en": "Pending",
		"es": "Pendiente"
    }
}]