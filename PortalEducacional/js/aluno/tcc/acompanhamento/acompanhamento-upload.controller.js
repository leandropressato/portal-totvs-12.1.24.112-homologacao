/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduTccModule
 * @name eduAcompanhamentoTccController
 * @object controller
 *
 * @created 2016-11-23 v12.1.15
 * @updated
 *
 * @requires Tcc.module
 *
 * @dependencies eduTccFactory
 *
 * @description Controller do modal de envio de comentários do aluno de um acompanhamento do TCC.
 */
define([
    'aluno/tcc/tcc.module',
    'aluno/tcc/tcc.route',
    'aluno/tcc/tcc.factory'
], function() {
    'use strict';

    angular
        .module('eduTccModule')
        .controller('eduAcompanhamentoUploadController', EduAcompanhamentoUploadController);

    EduAcompanhamentoUploadController.$inject = ['$scope',
        'eduTccFactory',
        '$resource',
        'i18nFilter',
        'parametros',
        '$state',
        '$modalInstance',
        'totvs.app-notification.Service'
    ];

    function EduAcompanhamentoUploadController($scope,
        eduTccFactory,
        $resource,
        i18nFilter,
        parametros,
        $state,
        $modalInstance,
        totvsNotification) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.model = {};
        self.model.Arquivo = {};
        self.ArrayArquivo = [];
        self.PatchArquivo = '';

        self.modalInstance = $modalInstance;
        self.sendArquivo = sendArquivo;


        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        init();

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        function init() {

        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Cancela o envio de arquivo
         */
        function onclickCancelar() {
            $modalInstance.dismiss();
        }

        /**
         * Faz a chamada do envio do arquivo para o servidor.
         */
        function sendArquivo() {
            var reader = new FileReader(),
                file = self.model.Arquivo;

            if (self.model.Arquivo.name != null) {

                reader.onload = function(e) {
                    var stringBinario = e.target.result.split(',')[1];

                    eduTccFactory.setUploadArquivoAsync(parametros.idTcc,
                        stringBinario,
                        self.model.NOMEARQUIVO, //comentário do aluno
                        file.name,
                        parametros.arquivoFinal,
                        function(result) {
                            if (result) {
                                if (result[0].Arquivo != null) {
                                    reload(parametros.arquivoFinal);
                                    totvsNotification.notify({
                                        type: 'success',
                                        title: i18nFilter('l-arquivo-enviado', [], 'js/aluno/tcc/acompanhamento'),
                                        detail: result.message
                                    });
                                }
                            } else {
                                totvsNotification.notify({
                                    type: 'error',
                                    title: i18nFilter('l-erro-envio', [], 'js/aluno/tcc/acompanhamento'),
                                    detail: result.message
                                });
                            }
                        });
                };
                reader.readAsDataURL(file);
            } else {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-erro-envio', [], 'js/aluno/tcc/acompanhamento'),
                    detail: 'Selecione um arquivo.'
                });
            }
        }

        /**
         * Recarrega a rota
         */
        function reload(arquivoFinal) {
            if (arquivoFinal) {
                $scope.$parent.controller.getInfoTcc();
            } else {
                $scope.$parent.controller.retornaListaAcompanhamentos();
            }

            $modalInstance.dismiss();
        }
    }
});