[{
    "l-home": {
        "pt": "Início",
		"en": "Start",
		"es": "Inicio"
    },
    "l-titulo": {
        "pt": "TCC",
		"en": "Final paper",
		"es": "Tesis"
    },
    "l-tcc-arquivo-final": {
        "pt": "Arquivo final",
		"en": "End file",
		"es": "Archivo final"
    },
    "l-identificador": {
        "pt": "Cadastro do TCC",
		"en": "Register of file paper",
		"es": "Archivo de la tesis"
    },
    "btn-CIENTE": {
        "pt": "Ciente",
		"en": "Aware",
		"es": "Informado"
    },
    "l-situacao-tcc": {
        "pt": "Situação do TCC",
		"en": "Status of final paper",
		"es": "Situación de la tesis"
    },
    "l-autoriza-publicacao": {
        "pt": "Autoriza publicação do TCC?",
		"en": "Do you authorize the publication of the final paper?",
		"es": "¿Autoriza publicación de la tesis?"
    },
    "l-DATAOCORRENCIA": {
        "pt": "Data da Ocorrência",
		"en": "Occurrence date",
		"es": "Fecha de la ocurrencia"
    },
    "l-DESCGRUPOOCOR": {
        "pt": "Grupo",
		"en": "Group",
		"es": "Grupo"
    },
    "l-DISCIPLINA": {
        "pt": "Disciplina",
		"en": "Course subject",
		"es": "Materia"
    },
    "l-NOMEPROF": {
        "pt": "Professor",
		"en": "Teacher",
		"es": "Profesor"
    },
    "l-CODTURMA": {
        "pt": "Turma",
		"en": "Class",
		"es": "Grupo"
    },
    "l-ETAPA": {
        "pt": "Etapa",
		"en": "Stage",
		"es": "Etapa"
    },
    "l-DTRESPONSAVELCIENTE": {
        "pt": "Data do Aceite",
		"en": "Acceptance Date",
		"es": "Fecha de la aceptación"
    },
    "l-NOMEUSUARIOCIENTE": {
        "pt": "Responsável Ciente",
		"en": "Responsible Party Aware",
		"es": "Responsable informado"
    },
    "l-OBSERVACOES": {
        "pt": "Observação",
		"en": "Note",
		"es": "Observación"
    },
    "l-AceiteValidado": {
        "pt": "Ciente",
		"en": "Aware",
		"es": "Informado"
    },
    "l-AguardandoAceite": {
        "pt": "Aguardando Aceite",
		"en": "Waiting Acceptance",
		"es": "Esperando aceptación"
    },
    "l-success-updated": {
        "pt": "salvo com sucesso",
		"en": "successfully saved",
		"es": "grabado con éxito"
    },
    "l-titulo-tcc-cadastro": {
        "pt": "Cadastro do TCC",
		"en": "Register of final paper",
		"es": "Archivo de la tesis"
    },
    "l-tema": {
        "pt": "Tema do TCC",
		"en": "Final paper theme",
		"es": "Tema de la tesis"
    },
    "l-publicar": {
        "pt": "Autoriza a publicação do TCC?",
		"en": "Do you authorize the publication of the final paper?",
		"es": "¿Autoriza la publicación de la tesis?"
    },
    "l-matricula-vinculada": {
        "pt": "Matrícula vinculada ao TCC",
		"en": "Enrollment bound to final paper",
		"es": "Matrícula vinculada a la tesis"
    },
    "l-linha-pesquisa": {
        "pt": "Linha de pesquisa",
		"en": "Research line",
		"es": "Línea de búsqueda"
    },
    "l-tipo": {
        "pt": "Tipo",
		"en": "Type",
		"es": "Tipo"
    },
    "l-TEMA": {
        "pt": "Tema",
		"en": "Theme",
		"es": "Tema"
    },
    "l-LOCAL": {
        "pt": "Local",
		"en": "Location",
		"es": "Local"
    },
    "l-NOMEPARTICIPANTE": {
        "pt": "Participantes",
		"en": "Participants",
		"es": "Participantes"
    },
    "l-TIPOPARTICIPANTE": {
        "pt": "Tipo",
		"en": "Type",
		"es": "Tipo"
    },
    "l-incluir": {
        "pt": "Incluir registro",
		"en": "Add record",
		"es": "Incluir registro"
    },
    "l-editar": {
        "pt": "Editar registro",
		"en": "Edit record",
		"es": "Editar registro"
    },
    "l-title-tcc-disciplina": {
        "pt": "Informações sobre o TCC",
		"en": "Information on final paper",
		"es": "Información sobre la tesis"
    },
    "l-professor": {
        "pt": "Professor",
		"en": "Teacher",
		"es": "Profesor"
    },
    "l-situacao": {
        "pt": "Situação",
		"en": "Status",
		"es": "Situación"
    },
    "l-justificativa": {
        "pt": "Justificativa",
		"en": "Justification",
		"es": "Justificación"
    },
    "l-aceito": {
        "pt": "Convite aceito",
		"en": "Invitation accepted",
		"es": "Invitación aceptada"
    },
    "l-rejeitado": {
        "pt": "Convite rejeitado",
		"en": "Invitation rejected",
		"es": "Invitación rechazada"
    },
    "l-aguardando": {
        "pt": "Aguardando resposta",
		"en": "Waiting answer",
		"es": "Esperando respuesta"
    },
    "l-convidar-professor": {
        "pt": "Convidar orientador",
		"en": "Invite supervisor",
		"es": "Invitar asesor"
    },
    "l-cadastrar-aluno": {
        "pt": "Cadastrar aluno",
		"en": "Register student",
		"es": "Registrar alumno"
    },
    "l-orientadores": {
        "pt": "Orientadores",
		"en": "Supervisors",
		"es": "Asesores"
    },
    "l-tcc-acompanhamento": {
        "pt": "Acompanhamento",
		"en": "Follow-up",
		"es": "Acompañamiento"
    },
    "l-Nenhum-Registro-Encontrado": {
        "pt": "Nenhum registro encontrado!",
		"en": "No record foun",
		"es": "¡No se encontró ningún registro!"
    },
    "l-grupo-aluno": {
        "pt": "Grupo de alunos",
		"en": "Student's group",
		"es": "Grupo de alumnos"
    },
    "l-tcc-banca": {
        "pt": "Banca",
		"en": "Examination board",
		"es": "Mesa examinadora"
    },
    "l-nome": {
        "pt": "Nome",
		"en": "Name",
		"es": "Nombre"
    },
    "l-btn-convidar-professor": {
        "pt": "Convidar professor",
		"en": "Invite teacher",
		"es": "Invitar profesor"
    },
    "l-incluir-aluno": {
        "pt": "Incluir aluno",
		"en": "Add student",
		"es": "Incluir alumno"
    },
    "l-aluno": {
        "pt": "Aluno",
		"en": "Student",
		"es": "Alumno"
    },
    "l-msg-cancelar": {
        "pt": "Confirma o cancelamento da operação?",
		"en": "Confirm cancellation of operation?",
		"es": "¿Confirma la anulación de la operación?"
    },
    "l-incluir-aluno-sucesso": {
        "pt": "Aluno incluído ao grupo de TCC com sucesso!",
		"en": "Student successfully added to final paper group!",
		"es": "¡Alumno incluido al grupo de tesis con éxito!"
    },
    "l-convite-professor-sucesso": {
        "pt": "Convite cadastrado com sucesso!",
		"en": "Invitation successfully registered!",
		"es": "¡Invitación registrada con éxito!"
    },
    "l-msg-aluno-nao-encontrado": {
        "pt": "Não existem alunos para adicionar ao grupo de TCC.",
		"en": "No students to add to final paper group.",
		"es": "No existen alumnos para agregar al grupo de tesis."
    },
    "l-msg-professor-nao-encontrado": {
        "pt": "Não existem professores para envio de convite de orientação.",
		"en": "No teachers to submit supervision invitation.",
		"es": "No existen profesores para enviar invitación para asesoría."
    },
    "l-tcc-informacoes": {
        "pt": "Informações",
		"en": "Information",
		"es": "Información"
    },
    "l-msg-campo-obrigatorio": {
      "pt": "Campo de preenchimento obrigatório",
	  "en": "Completion field required",
	  "es": "Campo de cumplimentación obligatoria"
    }
}]