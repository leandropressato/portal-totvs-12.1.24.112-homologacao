/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduTccModule
 * @name EduTccController
 * @object controller
 *
 * @created 04/01/2016 v12.1.15
 * @updated
 *
 * @requires tcc.module
 *
 * @dependencies eduTccFactory
 *
 * @description Controller da Banca do Tcc
 */
define(['aluno/tcc/tcc.module',
        'aluno/tcc/tcc.factory',
        'utils/edu-enums.constants'
       ], function() {

    'use strict';

    angular
        .module('eduTccModule')
        .controller('EduTccBancaController', EduTccBancaController);

    EduTccBancaController.$inject = [
        'TotvsDesktopContextoCursoFactory',
        'eduTccFactory',
        'eduEnumsConsts',
        '$state',
        '$filter'
    ];

    /**
     * Controller da Banca do TCC
     * @param {object}    objContextoCursoFactory Objeto do contexto Educacional
     * @param {object}    eduTccFactory           Objeto da factory com serviços para o TCC
     * @param {constants} EduEnumsConsts          Constantes Enums do Educacional
     * @param {object}    $state                  Objeto com parâmetros da rota
     * @param {object}    $filter                 Objeto para aplicação de filtros
     */
    function EduTccBancaController(objContextoCursoFactory, eduTccFactory, EduEnumsConsts, $state, $filter) {

        var self = this;
        
        self.objContexto = {};
        self.objTccBancaList = [];
        
        self.getContexto = getContexto;
        self.loadTccBanca = loadTccBanca;

        init();

        /**
         * Função de inicialização do controller
         */
        function init() {
            self.getContexto();
            self.loadTccBanca();
        }

        /**
         * Obtém o contexto educacional
         */
        function getContexto() {
            self.objContexto = objContextoCursoFactory.getCursoSelecionado();
        }
        
        /**
         * Carrega as informações da banca de TCC
         */
        function loadTccBanca() {
            eduTccFactory.getTccBanca($state.params.idTurmaDisc, function (result) {
                self.objTccBancaList = [];
                if (angular.isArray(result.STCCBANCA)) {
                    var objBancaList = $filter('orderBy')(result.STCCBANCA, 'DATAHORABANCA'); 
                    var objParticipantesList = [];
                    if (angular.isArray(result.STCCPARTICIPANTESBANCA)) {
                        objParticipantesList = result.STCCPARTICIPANTESBANCA;
                    }
                    
                    angular.forEach(objBancaList, function (objBanca) {
                        objBanca.objParticipantesList = makeTccBancaParticipante(objParticipantesList, objBanca.IDBANCA);                        
                        self.objTccBancaList.push(objBanca);                        
                    });
                }                    
            });
        }
        
        /**
         * Obtém os participantes de uma banca de TCC
         * @param   {array} objParticipantesList Lista de todos os participantes
         * @param   {int}   idBanca              Identificador da Banca de TCC
         * @returns {array} Lista com os participantes de uma determinada banca de TCC
         */
        function makeTccBancaParticipante(objParticipantesList, idBanca) {
            var objToReturnList = [];
            
            angular.forEach(objParticipantesList, function (objParticipante) {
               if (objParticipante.IDBANCA === idBanca)  {
                   objToReturnList.push(objParticipante);
               }
            });
            
            return objToReturnList;
        }
    }
});