/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduTccModule
 * @name EduTccInformacoesController
 * @object controller
 *
 * @created 2017-01-06 v12.1.15 - luis.marques
 * @updated
 *
 * @requires tcc.module, tcc.factory. edu-enums.constants
 *
 * @dependencies $filter, eduTccFactory, $state, $window, totvsNotification, i18nFilter, EduEnumsConsts
 *
 * @description Controller das informações de TCC
 */

define(['aluno/tcc/tcc.module',
    'aluno/tcc/tcc.factory',
    'utils/edu-enums.constants',
    'utils/reports/edu-relatorio.service',
    'utils/edu-utils.factory'
], function () {

    'use strict';

    angular.module('eduTccModule')
        .controller('EduTccInformacoesController', EduTccInformacoesController);

    EduTccInformacoesController.$inject = [
        '$filter',
        'eduTccFactory',
        '$state',
        '$window',
        'totvs.app-notification.Service',
        'i18nFilter',
        'eduEnumsConsts',
        '$rootScope',
        'eduRelatorioService',
        'eduUtilsFactory'
    ];

    /**
     * Controller das informações do TCC
     */
    function EduTccInformacoesController($filter, eduTccFactory, $state, $window, totvsNotification, i18nFilter, EduEnumsConsts, $rootScope, eduRelatorioService, eduUtilsFactory) {

        var self = this;

        // Variáveis
        self.idTCC = 0;
        self.idTurmaDisc = $state.params.idTurmaDisc;
        self.codDisc = $state.params.codDisc;
        self.permiteImpressao = false;

        // Array e Objetos
        self.objGrupoAlunos = [];
        self.objGrupoAlunosDisponiveis = [];
        self.objOrientadores = [];
        self.objProfessoresDisponiveis = [];
        self.objTCCLista = [];

        // Métodos
        self.abrirModalAluno = abrirModalAluno;
        self.abrirModalProfessor = abrirModalProfessor;
        self.carregaAlunosDisponiveis = carregaAlunosDisponiveis;
        self.carregaAlunosGrupo = carregaAlunosGrupo;
        self.carregaInformacoesTCC = carregaInformacoesTCC;
        self.carregaOrientadores = carregaOrientadores;
        self.carregaProfessoresDisponiveis = carregaProfessoresDisponiveis;
        self.convidaOrientador = convidaOrientador;
        self.consultaPermissao = consultaPermissao;
        self.editarTCC = editarTCC;
        self.incluiAlunoGrupo = incluiAlunoGrupo;
        self.incluirTCC = incluirTCC;
        self.formataSituacao = formataSituacao;
        self.imprimir = imprimir;
        self.carregarParametrosEducacional = carregarParametrosEducacional;

        init();

        /**
         * Inicializa o carregamento dos dados
         */
        function init() {
            self.carregarParametrosEducacional();
            self.carregaInformacoesTCC();
        }

        ///////////////////////////////////////////////////////////////////////
        // CARREGAMENTO DE DADOS
        //////////////////////////////////////////////////////////////////////

        /**
         * Carrega os alunos disponíveis para inclusão ao grupo de TCC
         */
        function carregaAlunosDisponiveis() {
            eduTccFactory.retornaAlunoDisponiveisGrupo(self.idTCC,
                function (result) {
                    self.objGrupoAlunosDisponiveis = [];
                    if (result && result.Table1) {
                        angular.forEach(result.Table1, function (value) {
                            self.objGrupoAlunosDisponiveis.push(value);

                        });
                    }
                });
        }

        /**
         * Carrega os alunos do grupo de TCC
         */
        function carregaAlunosGrupo() {
            eduTccFactory.retornaAlunoGrupo(self.idTCC,
                function (result) {
                    self.objGrupoAlunos = [];
                    if (result) {
                        self.objGrupoAlunos = result;
                    }
                });
        }

        /**
         *  Carrega todos os dados para o funcionamento da tela de TCC
         */
        function carregaDadosTCC() {
            self.carregaOrientadores();
            self.carregaProfessoresDisponiveis();
            self.carregaAlunosGrupo();
            self.carregaAlunosDisponiveis();
        }

        /**
         * Carrega as informações de TCC
         */
        function carregaInformacoesTCC() {
            eduTccFactory.retornaInformacoesTcc(self.idTurmaDisc, function (result) {
                if (result && result.STCC) {
                    result.STCC.forEach(function (value) {
                        self.objTCCLista.push(value);
                    });
                }

                if (self.objTCCLista.length > 0) {
                    self.idTCC = self.objTCCLista[0].IDTCC;
                    carregaDadosTCC();
                }
            });
        }

        /**
         * Carrega a lista de orientadores do TCC
         */
        function carregaOrientadores() {
            eduTccFactory.retornaOrientadorTCC(self.idTCC, function (result) {
                self.objOrientadores = [];
                if (result) {
                    self.objOrientadores = result;
                }
            });
        }

        /**
         * Carrega a lista de professores disponíveis para orientação
         */
        function carregaProfessoresDisponiveis() {
            eduTccFactory.retornaProfessoresOrientadores(self.idTCC,
                self.objTCCLista[0].IDLINHAPESQUISA,
                function (result) {
                    self.objProfessoresDisponiveis = [];
                    if (result) {
                        self.objProfessoresDisponiveis = result;
                    }
                });
        }

        ///////////////////////////////////////////////////////////////////////
        // AÇÕES DE BOTÕES
        //////////////////////////////////////////////////////////////////////

        /**
         * Abri o modal para a inclusão de aluno ao grupo de TCC
         */
        function abrirModalAluno() {
            if (self.objGrupoAlunosDisponiveis.length > 0) {
                $('#modalGrupoAluno').modal('show');
            } else {
                totvsNotification.notify({
                    type: 'info',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-aluno-nao-encontrado', [], 'js/aluno/tcc')
                });
            }
        }

        /**
         * Abri o modal para envio de convite ao professor/orientador
         */
        function abrirModalProfessor() {
            if (self.objProfessoresDisponiveis.length > 0) {
                $('#modalOrientadores').modal('show');
            } else {
                totvsNotification.notify({
                    type: 'info',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-professor-nao-encontrado', [], 'js/aluno/tcc')
                });
            }
        }

        /**
         * Realiza o cadastro do convite do orientador
         */
        function convidaOrientador() {
            eduTccFactory.convidaOrientador(self.idTCC, self.professor, function (result) {
                if (result) {
                    if (angular.isDefined(result.exception)) {
                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-titulo', [], 'js/aluno/Tcc'),
                            detail: result.message
                        });
                    } else {
                        totvsNotification.notify({
                            type: 'success',
                            title: i18nFilter('l-titulo', [], 'js/aluno/Tcc'),
                            detail: i18nFilter('l-convite-professor-sucesso', [], 'js/aluno/Tcc') + '!'
                        });

                        $('#modalOrientadores').modal('hide');
                        self.carregaOrientadores();
                        self.carregaProfessoresDisponiveis();
                    }
                }
            });
        }

        /**
         * Carrega a tela de Edição de TCC
         */
        function editarTCC() {
            $state.go('tcc.cadastro-edit', {
                idTurmaDisc: self.idTurmaDisc,
                codDisc: self.codDisc
            });
        }

        /**
         * Inclui aluno no grupo de TCC
         */
        function incluiAlunoGrupo() {
            eduTccFactory.incluiAlunoGrupo(self.idTCC, self.idTurmaDisc, self.aluno, function (result) {
                if (result) {
                    if (angular.isDefined(result.exception)) {
                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-titulo', [], 'js/aluno/Tcc'),
                            detail: result.message
                        });
                    } else {
                        totvsNotification.notify({
                            type: 'success',
                            title: i18nFilter('l-titulo', [], 'js/aluno/Tcc'),
                            detail: i18nFilter('l-incluir-aluno-sucesso', [], 'js/aluno/Tcc') + '!'
                        });

                        $('#modalGrupoAluno').modal('hide');
                        self.carregaAlunosDisponiveis();
                        self.carregaAlunosGrupo();
                    }
                }
            });
        }

        /**
         * Carrega a tela de Inclusão de TCC
         */
        function incluirTCC() {
            $state.go('tcc.cadastro-novo', {
                idTurmaDisc: self.idTurmaDisc,
                codDisc: self.codDisc
            });
        }

        ///////////////////////////////////////////////////////////////////////
        // UTÉIS
        //////////////////////////////////////////////////////////////////////

        /**
         * Formata a situação do convite ao orientador
         *
         * @param {object} orientador objeto orientador
         * @returns {string} Campo justificativa formatado
         */
        function formataSituacao(orientador) {
            if (orientador.CONVITEACEITO === EduEnumsConsts.EduSimOuNaoEnum.Sim) {
                return i18nFilter('l-aceito', [], 'js/aluno/tcc');
            } else if (orientador.CONVITEACEITO === EduEnumsConsts.EduSimOuNaoEnum.Nao &&
                orientador.JUSTIFICATIVA) {
                return i18nFilter('l-rejeitado', [], 'js/aluno/tcc');
            } else {
                return i18nFilter('l-aguardando', [], 'js/aluno/tcc');
            }
        }

        function consultaPermissao() {
            return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_TCCALUNO_CONVORIENTADOR);
        }

        function imprimir() {

            totvsNotification.question({
                title: $filter('i18n')('l-imprimir'),
                text: $filter('i18n')('l-confirma-impressao'),
                size: 'sm',
                cancelLabel: 'l-no',
                confirmLabel: 'l-yes',
                callback: function (imprimir) {

                    if (imprimir) {

                        eduTccFactory.gerarRelatorioAutorizacaoPublicacaoTCCAsync(self.idTCC, function (result) {

                            if (result) {
                                eduRelatorioService.exibirOuSalvarPDF(result[0].Bytes);
                            }
                        });
                    }
                }
            });
        }

        function carregarParametrosEducacional() {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (parametros) {

                self.permiteImpressao = parametros.CodRelatorioAceitePublicacaoTrab ? true : false;

            });
        }

    }
});
