/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2016-2017 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduTccModule
 * @name EduTccInformacoesEditController
 * @object Controller
 *
 * @created 06-01-2017 - luis.marques
 * @updated
 *
 * @requires tcc.module, tcc.factory
 *
 * @dependencies $filter, eduTccFactory, $state, totvsNotification, i18nFilter
 *
 * @description Controller da edição das informações de TCC
 */

define(['aluno/tcc/tcc.module',
    'aluno/tcc/tcc.factory'
], function () {

    'use strict';

    angular.module('eduTccModule')
        .controller('EduTccInformacoesEditController', EduTccInformacoesEditController);

    EduTccInformacoesEditController.$inject = [
        '$filter',
        'eduTccFactory',
        '$state',
        'totvs.app-notification.Service',
        'i18nFilter'
    ];

    /**
     * Controller da edição das informações de TCC
     *
     * @param {object} $filter - Objeto filtro
     * @param {object} eduTccFactory - Objeto factory
     * @param {object} $state - objeto state
     * @param {object} totvsNotification - Totvs Notification
     * @param {object} i18nFilter - Filtro tradução
     */
    function EduTccInformacoesEditController($filter, eduTccFactory, $state, totvsNotification, i18nFilter) {

        var self = this;

        // Variáveis
        self.model = {};
        self.blnEditMode = false;
        self.campos = {};
        self.listMatVinculadas = [];
        self.listlinhaPesquisa = [];
        self.listTipo = [];

        // Métodos e Eventos
        self.carregaMatVinculadas = carregaMatVinculadas;
        self.carregaLinhaPesquisa = carregaLinhaPesquisa;
        self.carregaTipo = carregaTipo;
        self.carregaInformacoesTCC = carregaInformacoesTCC;
        self.cancel = cancel;
        self.save = save;
        self.carregaDadosModel = carregaDadosModel;

        init();

        /**
         *  Inicializa os carregamento de dados
         */
        function init() {
            self.carregaMatVinculadas();
            self.carregaLinhaPesquisa();
            self.carregaTipo();

            // carrega os parâmetros
            self.idTurmaDisc = $state.params.idTurmaDisc;
            self.codDisc = $state.params.codDisc;

            if (self.idTurmaDisc) {
                self.carregaInformacoesTCC();
            }
        }

        /**
         * Carrega os dados do model acessados via parâmetro
         *
         * @param {any} objTCC - Objeto de TCC
         */
        function carregaDadosModel(objTCC) {

            self.model['TEMA'] = objTCC['TEMA'];
            self.model['AUTORIZAPUBLICACAO'] = (objTCC['AUTORIZAPUBLICACAO'] === 'S') ? true : false;
            self.model['IDTURMADISC'] = objTCC['IDTURMADISC'];
            self.model['IDLINHAPESQUISA'] = objTCC['IDLINHAPESQUISA'];
            self.model['IDTIPOTCC'] = objTCC['IDTIPOTCC'];
            self.model['IDTCC'] = objTCC['IDTCC']
        }

        /**
         * Carrega as informações de TCC com base no idturmadisc
         */
        function carregaInformacoesTCC() {
            eduTccFactory.retornaInformacoesTcc(self.idTurmaDisc, function (result) {
                var objTcc = [];
                if (result && result.STCC && result.STCC.length > 0) {
                    result.STCC.forEach(function (value) {
                        objTcc = value;
                    });

                    if (objTcc && angular.isDefined(objTcc.IDTCC)) {
                        self.blnEditMode = true;
                    }

                    self.carregaDadosModel(objTcc);
                    self.desabilitaCampo = true;
                } else {
                    self.desabilitaCampo = false;
                }
            });
        }

        /**
         * Carrega a lista de matrículas vínculadas à disciplina
         */
        function carregaMatVinculadas() {
            eduTccFactory.retornaMatriculaVinculadas(function (result) {
                self.listMatVinculadas = result;

                if (result.length === 1) {
                    self.model.IDTURMADISC = result[0].IDTURMADISC;
                }
            });
        }

        /**
         * Carrega as linhas de pesquisa
         */
        function carregaLinhaPesquisa() {
            eduTccFactory.retornaLinhaPesquisa(function (result) {
                self.listlinhaPesquisa = result;

                if (result.length === 1) {
                    self.model.IDLINHAPESQUISA = result[0].IDLINHAPESQUISA;
                }
            });
        }

        /**
         * Carrega o tipo de TCC
         */
        function carregaTipo() {
            eduTccFactory.retornaTipo(function (result) {
                self.listTipo = result;

                if (result.length === 1){
                    self.model.IDTIPOTCC = result[0].IDTIPOTCC;
                }
            });
        }

        /**
         * Valida o formulário
         *
         * @returns True/False
         */
        function validarFormulario() {

            if (self.formulario.$valid) {
                return true;
            } else {

                for (var nomeCampo in self.formulario) {

                    if (nomeCampo.startsWith('campo') && self.formulario[nomeCampo].$invalid) {
                        setFocusCampo(nomeCampo);
                        break;
                    }
                }

                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-campos-preenchimento-obrigatorio')
                });
            }
        }

        /**
         * Seta o foco no campo obrigatório
         *
         * @param {string} nomeCampo - nome do campo
         */
        function setFocusCampo(nomeCampo) {
            var seletor = '[name=' + nomeCampo + '] input, [name=' + nomeCampo + '] select';

            $(seletor).focus();
        }

        /**
         * Cancela a ação sobre o formulário e retorna para a tela de informações do TCC
         */
        function cancel() {
            totvsNotification.question({
                title: i18nFilter('l-Confirmacao'),
                text: i18nFilter('l-msg-cancelar', [], 'js/aluno/tcc'),
                cancelLabel: 'l-no',
                confirmLabel: 'l-yes',
                callback: function (isPositiveResult) {
                    if (isPositiveResult) {
                        $state.go('disciplina.start', {
                            idTurmaDisc: self.idTurmaDisc,
                            codDisc: self.codDisc
                        });
                    }
                }
            });
        }

        /**
         * Salva os dados de TCC após realizar a validação
         */
        function save() {
            var validacaoOK = validarFormulario();
            self.model.IDTCC = self.model.IDTCC ? self.model.IDTCC : 0;

            if (validacaoOK) {
                self.model.AUTORIZAPUBLICACAO = (self.model.AUTORIZAPUBLICACAO) ? 'S' : 'N';
                eduTccFactory.salvarDadosTCC(self.idTurmaDisc, !self.blnEditMode, new Array(self.model), function (result) {
                    if (result) {
                        if (angular.isDefined(result.exception)) {
                            totvsNotification.notify({
                                type: 'error',
                                title: i18nFilter('l-titulo', [], 'js/aluno/Tcc'),
                                detail: result.message
                            });
                        } else {
                            totvsNotification.notify({
                                type: 'success',
                                title: i18nFilter('l-titulo', [], 'js/aluno/Tcc'),
                                detail: i18nFilter('l-success-updated', [], 'js/aluno/Tcc') + '!'
                            });

                            $state.go('disciplina.start', {
                                idTurmaDisc: self.idTurmaDisc,
                                codDisc: self.codDisc
                            });
                        }
                    }
                });
            }
        }

    }

});
