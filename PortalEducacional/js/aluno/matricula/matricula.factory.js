define(['aluno/matricula/matricula.module'], function () {

    'use strict';

    angular
        .module('eduMatriculaModule')
        .factory('eduMatriculaFactory', EduMatriculaFactory);

    EduMatriculaFactory.$inject = ['$totvsresource', 'TotvsDesktopContextoCursoFactory'];

    function EduMatriculaFactory($totvsresource, TotvsDesktopContextoCursoFactory) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method';
        var factory = $totvsresource.REST(url, {}, {});

        factory.retornaValidacoesPLMatriculaAlunoAsync = retornaValidacoesPLMatriculaAlunoAsync;
        factory.retornaHabilitacoesPLDisponveisMatriculaAlunoAsync = retornaHabilitacoesPLDisponveisMatriculaAlunoAsync;
        factory.retornaParametrosMatriculaOnlineAlunoAsync = retornaParametrosMatriculaOnlineAlunoAsync;
        factory.retornaDisciplinasMatriculaAlunoAsync = retornaDisciplinasMatriculaAlunoAsync;
        factory.retornaPlanosPagamentoDisponiveisAsync = retornaPlanosPagamentoDisponiveisAsync;
        factory.retornaPlanoPagamentoDefaultAsync = retornaPlanoPagamentoDefaultAsync;
        factory.retornaContratoMatriculaAsync = retornaContratoMatriculaAsync;
        factory.retornaComprovanteMatriculaAsync = retornaComprovanteMatriculaAsync;
        factory.retornaDiscplinasMatriculaESAsync = retornaDiscplinasMatriculaESAsync;
        factory.retornaPesquisaDisciplinasExtrasAsync = retornaPesquisaDisciplinasExtrasAsync;
        factory.retornaTurmasDisciplinasExtrasAsync = retornaTurmasDisciplinasExtrasAsync;
        factory.retornaInfoTurmaDisc = retornaInfoTurmaDisc;
        factory.retornaInfoTurmaDisc = retornaInfoTurmaDisc;
        factory.retornaValidacoesReqDisciplinas = retornaValidacoesReqDisciplinas;
        factory.executaFVSelecaoDisc = executaFVSelecaoDisc;
        factory.efetuarEnsinoBasicoMatriculaAsync = efetuarEnsinoBasicoMatriculaAsync;
        factory.executarMatriculaES = executarMatriculaES;
        factory.permitirAlterarPlanoPagamentoAsync = permitirAlterarPlanoPagamentoAsync;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Realiza a validação para a matrícula do perído letivo do aluno
         * @param {any} idPerLet idPerLet
         * @param {any} filtroDisciplina filtroDisciplina
         * @param {Function} callback  Função de callback, se necessário.
         * @returns Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         */
        function retornaPesquisaDisciplinasExtrasAsync(idPerLet, filtroDisciplina, codDiscSDD, callback) {
            var parameters = {};
            parameters['idContextoAluno'] = TotvsDesktopContextoCursoFactory.getIdContextoEncode();
            parameters['idPerLet'] = idPerLet;
            parameters['filtroDisciplina'] = filtroDisciplina;
            parameters['listaCodDiscSDD'] = codDiscSDD;
            
            parameters['method'] = 'v1/Matricula/Habilitacao/EnsinoSuperior/DisciplinasExtras';
            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Realiza a validação para a matrícula do perído letivo do aluno
         * @param {any} idPerLet idPerLet
         * @param {any} codDisc codDisc
         * @param {Function} callback  Função de callback, se necessário.
         * @returns Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         */
        function retornaTurmasDisciplinasExtrasAsync(idPerLet, codDisc, callback) {
            var parameters = {};
            parameters['idContextoAluno'] = TotvsDesktopContextoCursoFactory.getIdContextoEncode();
            parameters['idPerLet'] = idPerLet;
            parameters['codDisc'] = codDisc;
            
            parameters['method'] = 'v1/Matricula/Habilitacao/EnsinoSuperior/TurmaDisciplinasExtras';
            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Realiza a validação para a matrícula do perído letivo do aluno
         * @param {any} idHabilitacaoFilialMatricula IdHabilitacaoFilial
         * @param {Function} callback  Função de callback, se necessário.
         * @returns Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         */
        function retornaValidacoesPLMatriculaAlunoAsync(idHabilitacaoFilialMatricula, idPerLet, callback) {
            var parameters = {};
            parameters['idHabilitacaoFilialMatricula'] = idHabilitacaoFilialMatricula;
            parameters['idPerLet'] = idPerLet;
            parameters['method'] = 'Matricula/Habilitacao/Validacao';
            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Realiza a validação para a matrícula do perído letivo do aluno
         * @param {Function} callback - Função de callback, se necessário.
         * @returns Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         */
        function retornaHabilitacoesPLDisponveisMatriculaAlunoAsync(callback) {
            var parameters = {};
            parameters['method'] = 'Matricula/Habilitacao';

            return factory.TOTVSGet(parameters, callback);
        }
        /**
         * Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         *
         * @param {any} callback função callback
         * @returns
         */
        function retornaParametrosMatriculaOnlineAlunoAsync(callback) {
            var parameters = {};
            parameters['method'] = 'Matricula/Parametros';

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna os dados das disciplinas da habilitação na matrículas
         *
         * @param {any} idHabilitacaoFilialMatricula IdHabilitacaoFilial
         * @param {any} callback Retorna os dados das disciplinas da habilitação na matrículas
         * @returns
         */
        function retornaDisciplinasMatriculaAlunoAsync(idHabilitacaoFilialMatricula, idPerLet, callback) {
            var parameters = {};
            parameters.idHabilitacaoFilialMatricula = idHabilitacaoFilialMatricula;
            parameters.idPerLet = idPerLet;
            parameters.method = 'Matricula/Habilitacao/Disciplinas';

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorna as disciplinas que o aluno pode matricular no ensino superior baseado na SDD
         *
         * @param {any} callback função callback
         * @returns Lista de disciplinas
         */
        function retornaDiscplinasMatriculaESAsync(idPerLet, callback) {
            var parameters = {};
            parameters.idContextoAluno = TotvsDesktopContextoCursoFactory.getIdContextoEncode();
            parameters.idPerLet = idPerLet;
            parameters.method = 'v1/Matricula/Habilitacao/EnsinoSuperior/Disciplinas';

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna as informações da turma/disciplina
         *
         * @param {any} idTurmaDisc Id turma/disc
         * @param {any} codDisc Cód disciplina
         * @param {any} callback função callback
         */
        function retornaInfoTurmaDisc(idTurmaDisc, codDisc, callback) {
            var parameters = {};
            parameters.idContextoAluno = TotvsDesktopContextoCursoFactory.getIdContextoEncode();
            parameters.idTurmaDisc = idTurmaDisc;
            parameters.codDisc = codDisc;
            parameters.method = 'v1/Matricula/Habilitacao/EnsinoSuperior/TurmaDisciplina';

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna os planos de pagamento da habilitação na matrícula
         *
         * @param {any} idHabilitacaoFilialMatricula Habilitação Filial da Matrícula
         * @param {any} idPerLet Período Letivo
         * @param {any} idsTurmaDisc Ids das turma/disciplinas
         * @param {any} callback Retorna os dados das disciplinas da habilitação na matrículas
         * @returns Retorna os planos de pagamento da habilitação na matrícula
         */
        function retornaPlanosPagamentoDisponiveisAsync(idHabilitacaoFilialMatricula, idPerLet, listaIdTurmaDisc, callback) {
            var parameters = {};
            parameters.idHabilitacaoFilialMatricula = idHabilitacaoFilialMatricula;
            parameters.idPerLet = idPerLet;
            parameters.listaIdTurmaDisc = listaIdTurmaDisc;
            parameters.method = 'Matricula/Habilitacao/PlanoPagamento';

            return factory.TOTVSQuery(parameters, callback);
        }

        function retornaPlanoPagamentoDefaultAsync(idHabilitacaoFilialMatricula, idPerLet, callback) {
            var parameters = {};
            parameters.idHabilitacaoFilialMatricula = idHabilitacaoFilialMatricula;
            parameters.idPerLet = idPerLet;
            parameters.method = 'Matricula/Habilitacao/PlanoPagamento/Padrao';

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Valida se o aluno pode alterar o plano de pagamento
         * 
         * @param {any} idHabilitacaoFilial Habilitação filial
         * @param {any} idPerLet Id. do período letivo
         * @param {any} callback Callback
         * @returns Retorna true se puder alterar o plano de pagamento
         */
        function permitirAlterarPlanoPagamentoAsync(idHabilitacaoFilial, idPerLet, callback) {
            var parameters = {};
            parameters.idContextoAluno = TotvsDesktopContextoCursoFactory.getIdContextoEncode();
            parameters.idPerLet = idPerLet;
            parameters.idHabilitacaoFilial = idHabilitacaoFilial;
            
            parameters.method = 'Matricula/Habilitacao/PlanoPagamento/PermiteAlterar';

            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna o contrato para o plano de pagamento e habilitação da matrícula
         *
         * @param {any} idHabilitacaoFilial Habilitação Filial
         * @param {any} codPlanoPagamento Informar plano de pagamento caso necessário
         * @param {any} callback Retorna os dados das disciplinas da habilitação na matrículas
         * @returns Retorna os planos de pagamento da habilitação na matrícula
         */
        function retornaContratoMatriculaAsync(idHabilitacaoFilial, idPerLet, codPlanoPagamento, listaIdTurmaDisc, callback) {
            var parameters = {};
            parameters.idHabilitacaoFilialMatricula = idHabilitacaoFilial;
            parameters.idPerLet = idPerLet;
            parameters.method = 'Matricula/Habilitacao/Contrato';
            parameters.codPlanoPagamento = codPlanoPagamento;
            parameters.listaIdTurmaDisc = listaIdTurmaDisc;
            
            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Retorna o contrato para o plano de pagamento e habilitação da matrícula
         *
         * @param {any} idHabilitacaoFilial Habilitação Filial
         * @param {any} callback Retorna os dados das disciplinas da habilitação na matrículas
         * @returns Retorna os planos de pagamento da habilitação na matrícula
         */
        function retornaComprovanteMatriculaAsync(idHabilitacaoFilial, idPerLet, callback) {
            var parameters = {};
            parameters.idHabilitacaoFilialMatricula = idHabilitacaoFilial;
            parameters.idPerLet = idPerLet;
            parameters.method = 'Matricula/Habilitacao/Comprovante';
            return factory.TOTVSGet(parameters, callback);
        }

        /**
         * Realiza a efetuação da matrícula para o ensino básico
         *
         * @param {any} idHabilitacaoFilial Habilitação Filial
         * @param {any} codPlanoPagamento Informar plano de pagamento caso necessário
         * @param {any} callback Retorna os dados das disciplinas da habilitação na matrículas
         * @returns Retorna os planos de pagamento da habilitação na matrícula
         */
        function efetuarEnsinoBasicoMatriculaAsync(idHabilitacaoFilial, codPlanoPagamento, idPerLet, callback) {
            var parameters = {};
            parameters.method = 'Matricula/Habilitacao/EnsinoBasico/Confirmacao';

            var objDadosMatricula = { idHabilitacaoFilial: idHabilitacaoFilial, codPlanoPagamento: codPlanoPagamento, idPerLet: idPerLet };

            return factory.TOTVSSave(parameters, objDadosMatricula, callback);
        }

        /**
         * Realiza a validação de requisitos das disciplinas
         * @param {any} matricItensList Lista de disciplinas matrículadas
         * @param {any} matricItensListDel Lista de disciplinas excluídas da matrícula
         * @param {Function} callback  Função de callback, se necessário.
         * @returns Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         */
        function retornaValidacoesReqDisciplinas(matricItensList, matricItensListDel, callback) {
            var parameters = {};
            var ObjListMatricDisc = { matricItensList: matricItensList, matricItensListDel: matricItensListDel };
            parameters['method'] = 'v1/Matricula/Habilitacao/EnsinoSuperior/ValidaReqDisciplinas';
            return factory.TOTVSSave(parameters, ObjListMatricDisc, callback);
        }

         /**
         * Realiza a execução de fórmula visual após seleção de disciplinas
         * @param {any} matricItensList Lista de disciplinas matrículadas
         * @param {Function} callback  Função de callback, se necessário.
         * @returns Retorna um objeto com os itens encontrados na validação do período letivo do aluno.
         */
        function executaFVSelecaoDisc(matricItensList, codcoligada, codtipocurso, codfilial, callback) {
            var parameters = {};
            parameters['codcoligada'] = codcoligada;
            parameters['codtipocurso'] = codtipocurso;
            parameters['codfilial'] = codfilial;
            parameters['method'] = 'v1/Matricula/Habilitacao/EnsinoSuperior/ExecutaFVSelecaoDisc';
            return factory.TOTVSSave(parameters, matricItensList, callback);
        }

        /**
         * Realiza matricula do ES
         * @param {any} listaDiscAlteradas Lista de disciplinas alteradas
         * @param {any} listaDiscAdd Lista de disciplinas adicionadas
         * @param {any} listaDiscRemovidas Lista de disciplinas removidas
         * @param {any} listaDiscSubstituicao Lista de troca de disciplina por substituição
         * @param {any} codPlanoPgtoSelecionado Cód plano de pagamento selecionado
         * @param {any} idPeriodoLetivo Id. do período letivo
         * @param {function} callback Callback
         */
        function executarMatriculaES(listaDiscAlteradas, listaDiscAdd, listaDiscRemovidas, listaDiscSubstituicao, codPlanoPgtoSelecionado, idPeriodoLetivo, callback) {

            var objConfirmaMatricula = {
                                            idContextoAluno: TotvsDesktopContextoCursoFactory.getIdContextoEncode(),
                                            MatriculaToAltList: listaDiscAlteradas,
                                            MatriculaToInsertList: listaDiscAdd,
                                            MatriculaToRemoveList: listaDiscRemovidas,
                                            MatriculasToSwapList: listaDiscSubstituicao,
                                            codPlanoPagamento: codPlanoPgtoSelecionado,
                                            idPerLet: idPeriodoLetivo
                                    };

            var parameters = {};
            parameters['method'] = 'v1/Matricula/Habilitacao/EnsinoSuperior/ConfirmarMatriculaES';

            return factory.TOTVSSave(parameters, objConfirmaMatricula, callback);
        }
    }
});
