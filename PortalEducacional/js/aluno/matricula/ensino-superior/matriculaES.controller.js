define(['aluno/matricula/matricula.module', 
        'aluno/matricula/matricula.service',
        'aluno/financeiro/financeiro.service',
        'aluno/financeiro/financeiro.factory',
        'aluno/financeiro/lancamentos/lancamentos-pagcartao.controller',
        'utils/edu-enums.constants'], function () {

    'use strict';

    angular
        .module('eduMatriculaModule')
        .controller('EduMatriculaESController', EduMatriculaESController)
        .filter('unique', function() {
            return function(collection, keyname) {
                var output = [], 
                    keys = [];
                
                angular.forEach(collection, function(item) {
                    var key = item[keyname];
                    if(keys.indexOf(key) === -1) {
                        keys.push(key); 
                        output.push(item);
                    }
                });
                return output;
             };
          });

    EduMatriculaESController.$inject = ['$scope', '$sce', '$filter', 'i18nFilter', 'eduMatriculaService', 'totvs.app-notification.Service',
    'eduEnumsConsts', 'EduFinanceiroService', 'eduUtilsFactory', 'EduFinanceiroFactory', '$rootScope'];

    function EduMatriculaESController($scope, $sce, $filter, i18nFilter, eduMatriculaService, totvsNotification, eduEnumsConsts, eduFinanceiroService, eduUtilsFactory, eduFinanceiroFactory, $rootScope) {

        var self = this,
        parametrosEducacional = null;

        self.etapas = [];
        self.parametrosMatricula = {};
        self.habilitacoesPLDisponiveis = [];
        self.disciplinasMatriculadas = [];
        self.disciplinaSelecionadaMatricula = [];
        self.disciplinasSDD = [];
        self.requisitos = [];
        self.horariosTurmaDisc = [];
        self.horariosMatriculados = [];
        self.discDisponiveisHorario = [];
        self.habilitacaoSelecionada = null;
        self.instrucoesMatriculaHTML = '';
        self.MensagemMatriculaNaoDisponivelHTML = '';
        self.tituloColunaHabilitacaoSerie = i18nFilter('l-habilitacao');
        self.planosPagamentoDisponiveis = [];
        self.planoPagamentoSelecionado = null;
        self.paramsEdu = null;
        self.somaCreditos = 0;
        self.subTurmaSelecionada = null;
        self.exibePainelValidacoes = false;
        self.exibePainelInformacoes = false;
        self.erroMaxCredito = false;
        self.erroMinCredito = false;
        self.possuiDisciplinaAdicionada = false;
        self.totalDiscObrigatoria = 0;
        self.totalDiscOptEletiva = 0;
        self.totalDiscExtra = 0;
        self.totalDiscEquivalente = 0;
        self.somaCreditosFinanceiros = 0;
        self.possuiHorarioSabado = false;
        self.possuiHorarioDomingo = false;
        self.modoEdicaoQuadroHorario = false;
        self.quadroHorarioMinimizado = false;
        self.resumoMinimizado = true;
        self.tooltipMinMaxCreditos = '';
        self.listaIdTurmaDisc = [];
        self.disciplinasExtras = [];
        self.turmasDisciplinasExtras = [];
        self.matricItensList = [];
        self.matricItensListDel = [];
        self.listaDiscAdd = [];
        self.listaDiscAlteradas = [];
        self.listaDiscSubstituicao = [];
        self.erroValidacaoPreCoRequisito = '';
        self.tituloContratoFinanceiro = '';
        self.tituloComprovanteMatricula = '';
        self.htmlParaImpressao = '';
        self.textoContratoFinanceiro = '';
        self.boleto = null;
        self.maiorQuantidadeCaracterCodDisciplina = 0;
        self.maiorQuantidadeCaracterNomeDisciplina = 0;
        self.quantidadeErrosValidacao = 0;

        self.diasSemana = ['1', '2', '3', '4', '5', '6', '7'];
        self.larguraColQuadroHorario = 20;
        self.renderizarSlideOut = false;
        self.permiteAlterarPlanoPagamento = true;
        self.larguraGridCreditosAcademicos = 4;

        self.retornarEtapaAtual = retornarEtapaAtual;
        self.avancarEtapa = avancarEtapa;
        self.retrocederEtapa = retrocederEtapa;
        self.redirecionar = redirecionar;
        self.realizarMatricula = realizarMatricula;
        self.removerDisciplina = removerDisciplina;
        self.matricularDisciplina = matricularDisciplina;
        self.exibirModalContratoMatricula = exibirModalContratoMatricula;
        self.exibirModalComprovante = exibirModalComprovante;
        self.formataMoeda = formataMoeda;

        self.aoIniciarEtapaApresentacao = aoIniciarEtapaApresentacao;
        self.aoIniciarEtapaPeriodoLetivo = aoIniciarEtapaPeriodoLetivo;
        self.aoIniciarEtapaDisciplinas = aoIniciarEtapaDisciplinas;
        self.aoIniciarEtapaPlanosPagamento = aoIniciarEtapaPlanosPagamento;
        self.aoIniciarEtapaFinalizacao = aoIniciarEtapaFinalizacao;
        self.disciplinaSelecionada = disciplinaSelecionada;
        self.ocultaPainelInformacoesEValidacoes = ocultaPainelInformacoesEValidacoes;
        self.exibePainelErros = exibePainelErros;
        self.adicionarDisciplinaDoHorario = adicionarDisciplinaDoHorario;
        self.minimizaPainelQuadroHorario = minimizaPainelQuadroHorario;
        self.minimizaPainelResumo = minimizaPainelResumo;
        self.possuiDiscNoHorario = possuiDiscNoHorario;
        self.possuiErroValicaoHorario = possuiErroValicaoHorario;
        self.retornaCorFundoQuadroHorario = retornaCorFundoQuadroHorario;
        self.retornaLarguraColunaDisciplinaAdicionada = retornaLarguraColunaDisciplinaAdicionada;
        self.disciplinasDoHorarioModal = disciplinasDoHorarioModal;
        self.disciplinasExtrasModal = disciplinasExtrasModal;
        self.pesquisaDisciplinasExtras = pesquisaDisciplinasExtras;
        self.turmasDisciplinasExtras = turmasDisciplinasExtras;
        self.emitirBoleto = emitirBoleto;
        self.exibirOpcoesPagamento = exibirOpcoesPagamento;
        self.exibeBtnPagamentoCartao = exibeBtnPagamentoCartao;
        self.exibeBtnPagamentoBoleto = exibeBtnPagamentoBoleto;
        self.exibirDadosPagCartao = exibirDadosPagCartao;
        self.calculaPlanoPagamento = calculaPlanoPagamento;
        self.executaTutorialMatricula = executaTutorialMatricula;

        self.HabilitacaoPLSelecionado = HabilitacaoPLSelecionado;

        //Devem ser registrados todas as funções de inicialização, quando uma etapa do wizard é acionada
        self.eventosStateEtapas = {
            'matriculaES.apresentacao': aoIniciarEtapaApresentacao.name,
            'matriculaES.periodo-letivo': 'aoIniciarEtapaPeriodoLetivo',
            'matriculaES.disciplinas': 'aoIniciarEtapaDisciplinas',
            'matriculaES.planos-pagamento': 'aoIniciarEtapaPlanosPagamento',
            'matriculaES.finalizacao': 'aoIniciarEtapaFinalizacao'
        }

        init();

        function init() {

            carregarParametrosEducacional();

            self.tituloContratoFinanceiro = i18nFilter('l-titulo-contrato', '[]', 'js/aluno/matricula');

            eduMatriculaService.inicializarWizardMatriculaEnsinoSuperiorAsync($scope, self.eventosStateEtapas, function (etapas, parametrosMatricula) {
                self.etapas = etapas;
                self.parametrosMatricula = parametrosMatricula;
                self.instrucoesMatriculaHTML = $sce.trustAsHtml(parametrosMatricula.InstrucoesMatricula);

                if (parametrosMatricula.MensagemMatriculaNaoDisponivel) {
                    self.MensagemMatriculaNaoDisponivelHTML = $sce.trustAsHtml(parametrosMatricula.MensagemMatriculaNaoDisponivel);
                }
                else {
                    self.MensagemMatriculaNaoDisponivelHTML = i18nFilter('l-msg-matricula-indisponivel', '[]', 'js/aluno/matricula');
                }

                if (parametrosMatricula.TituloContratoFinanceiro) {
                    self.tituloContratoFinanceiro = parametrosMatricula.TituloContratoFinanceiro;
                }

                if (parametrosMatricula.TituloComprovanteMatricula) {
                    self.tituloComprovanteMatricula = parametrosMatricula.TituloComprovanteMatricula;
                }
            });
        }

        function carregarParametrosEducacional () {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (paramEdu) {
                parametrosEducacional = paramEdu;
            });
        }

        function retornarEtapaAtual() {
            return eduMatriculaService.retornarEtapaAtual(self.etapas);
        }

        function avancarEtapa() {

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaES.apresentacao') {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
                eduMatriculaService.avancarEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaES.periodo-letivo') {
                self.bloquearMatricula = false;
                self.validacoesPLSelecionado.forEach(function (item) {
                    if (item.bloquearMatricula) {
                        self.bloquearMatricula = true;
                    }
                }, this);

                if (!self.bloquearMatricula && !self.habilitacaoSelecionada) {
                    self.bloquearMatricula = true;
                }

                if (!self.bloquearMatricula) {
                    eduMatriculaService.liberarProximaEtapa(self.etapas);                    
                }

                eduMatriculaService.avancarEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaES.dados-pessoais') {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
                eduMatriculaService.avancarEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaES.ficha-medica') {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
                eduMatriculaService.avancarEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaES.disciplinas') {
                self.bloquearMatricula = false;

                if (self.disciplinasSDD) {

                    if (verificaExistenciaErrosValidacao() == true) {
                        return;
                    }

                    criaListaDisciplinas(self.disciplinasSDD, function() {
                        validaPreCoRequisitos(function() {
                            validaFormulaVisual(function() {
                                if (self.bloquearMatricula) {
                                    return;
                                }
                                else {
                                    liberaEtapaPlanoPagto();
                                }
                            });

                        }); 
                    });
                }
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaES.planos-pagamento') {
                if ((self.planoPagamentoSelecionado && self.planoPagamentoSelecionado !== '0') || !self.permiteAlterarPlanoPagamento) {
                    executarContratoMatricula();

                    return;
                }
            }
        }

        function verificaExistenciaErrosValidacao() {
            if ($scope.permiteAvancarEtapaDisc == false) {
                exibePainelErros();

                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-erro-avancar-etapa-disciplina', '[]', 'js/aluno/matricula')
                });
                
                return true;
            }

            return false;
        }

        function validaPreCoRequisitos(callback) {
            if (self.paramsEdu.DESCONSIDERARREQDISC) {
                var validacoesPreCoRequisitoMatric = [];
                var validacoesPreCoRequisitoMatricDel = [];

                eduMatriculaService.retornaValidacoesReqDisciplinas(self.matricItensList, self.matricItensListDel, 
                    function(result) {
                        validacoesPreCoRequisitoMatric = result.matricItensList;
                        validacoesPreCoRequisitoMatricDel = result.matricItensListDel;
                        self.erroValidacaoPreCoRequisito = '';

                        //Verifica se ocorreu algum erro na validação dos pré/corequisito na lista de disciplinas matriculadas
                        validacoesPreCoRequisitoMatric.forEach(function (item) {
                            if (item.LogExcecoes.Texto) {
                                self.bloquearMatricula = true;
                                montaErroValidacaoPreCoRequisito(item.CodDisc, item.NomeDisc, item.CodTurma, item.LogExcecoes.Texto);
                            }
                        });

                        //Verifica se ocorreu algum erro na validação dos pré/corequisito na lista de disciplinas excluídas
                        validacoesPreCoRequisitoMatricDel.forEach(function (item) {
                            if (item.LogExcecoes.Texto) {
                                self.bloquearMatricula = true;
                                montaErroValidacaoPreCoRequisito(item.CodDisc, item.NomeDisc, item.CodTurma, item.LogExcecoes.Texto);
                            }
                        });

                        if (self.bloquearMatricula) {
                            totvsNotification.notify({
                                type: 'error',
                                title: i18nFilter('l-Atencao'),
                                detail: self.erroValidacaoPreCoRequisito
                                });
                        }

                        if (angular.isFunction(callback)) {
                            callback();
                        }
                });
            }
            else {
                if (angular.isFunction(callback)) {
                    callback();
                }
            }
        }

        function validaFormulaVisual(callback) {
            if(self.paramsEdu.FVAPOSSELECTDISC > 0) {
                eduMatriculaService.executaFVSelecaoDisc(self.matricItensList, 
                    self.habilitacaoSelecionada.CODCOLIGADA, 
                    self.habilitacaoSelecionada.CODTIPOCURSO, 
                    self.disciplinasSDD[0].CODFILIAL, function(result) {
                    
                    if (result.textovalidacao) {
                        self.bloquearMatricula = true;

                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-Atencao'),
                            detail: result.textovalidacao
                        });

                    }

                    if (angular.isFunction(callback)) {
                        callback();
                    }
                });
            }
            else {
                if (angular.isFunction(callback)) {
                    callback();
                }
            }
        }

        function liberaEtapaPlanoPagto() {
            if (self.parametrosMatricula.VisualizarPlanoPgto) {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
                eduMatriculaService.avancarEtapa(self.etapas);
            } else {
                executarContratoMatricula();
                return;
            }
        }        

        function retrocederEtapa() {
            eduMatriculaService.retrocederEtapa(self.etapas);
        }

        function redirecionar(state) {
            eduMatriculaService.redirecionar(self.etapas, state);
        }

        //* Função chamada quando um período letivo é selecionado pelo usuário *//
        function HabilitacaoPLSelecionado(plSelecionado) {
            if (plSelecionado == []) {
                return;
            }

            eduMatriculaService.retornaValidacoesPLMatriculaAlunoAsync(plSelecionado.IDHABILITACAOFILIAL, plSelecionado.IDPERLET,
                function (data) {
                    self.validacoesPLSelecionado = data;
                });
        }

        function aoIniciarEtapaApresentacao() {
            //Evento disparado quando é iniciallizado a etapa de apresentação
        }        

        //Evento disparado quando é iniciallizado a etapa de seleção de período letivo
        function aoIniciarEtapaPeriodoLetivo() {

            if (self.habilitacoesPLDisponiveis.length === 0) {

                eduMatriculaService.retornaHabilitacoesPLDisponveisMatriculaAlunoAsync(function (data) {
                    self.habilitacoesPLDisponiveis = data.HABILITACAOPLDISPONIVEIS;

                    //Se não tiver habilitação disponível mostra mensagem de matrícula indisponível
                    if (self.habilitacoesPLDisponiveis.length === 0) {
                        self.parametrosMatricula.MatriculaDiscponivel = false;
                    }

                    //Se tiver apenas um registro de habilitações exibidas para o usuário, já seleciona automaticamente e realiza as validações.
                    if (self.habilitacoesPLDisponiveis.length === 1) {
                        self.habilitacaoSelecionada = self.habilitacoesPLDisponiveis[0];
                        HabilitacaoPLSelecionado(self.habilitacaoSelecionada);
                    }
                });
            }

            var myWatch = $scope.$watch('controller.habilitacaoSelecionada', function (newValue, oldValue) {
                // Se o usuário trocar o período letivo durante a navegação da tela (anterior/próximo), então será
                // necessário reiniciar as etapas posteriores, para evitar que o usuário avance, com as regras
                // do período letivo selecionado anteriormente.
                if (newValue && oldValue && newValue.IDHABILITACAOFILIAL !== oldValue.IDHABILITACAOFILIAL) {

                    eduMatriculaService.restringirProximasEtapas(self.etapas);

                    // Limpa a lista de planos de pagamento disponíveis, pois ela precisará ser
                    // carregada novamente, devido a alteração do período letivo selecionado.
                    // Carga realizada na function aoIniciarEtapaPlanosPagamento
                    self.planosPagamentoDisponiveis = [];

                    myWatch();
                }
            });
        }

        function aoIniciarEtapaPlanosPagamento() {
            carregaPlanosPgto();
        }
        
        function carregaPlanoPgtoDefault(callback) {
            eduMatriculaService.retornaPlanoPagamentoDefaultAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, 
                self.habilitacaoSelecionada.IDPERLET, function (planoDefault) {
                    eduMatriculaService.permitirAlterarPlanoPagamentoAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET,
                        function(permite) {
                            self.permiteAlterarPlanoPagamento = permite.value;
                            
                            if (planoDefault && planoDefault.CODPLANOPGTO !== "0") {
                                self.planoPagamentoSelecionado = planoDefault.CODPLANOPGTO;
                            }

                            if (!permite.value && planoDefault.CODPLANOPGTO === "0")
                                self.planoPagamentoSelecionado = '';
        
                    });
                
            });

            if (callback) {
                callback();
            }            
        }

        function carregaPlanosPgto() {
            preparaListaIdTurmaDisc(function() {
                eduMatriculaService.retornaPlanosPagamentoDisponiveisAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, 
                    self.habilitacaoSelecionada.IDPERLET, 
                    self.listaIdTurmaDisc, function (listaPlanos) {
                        self.planosPagamentoDisponiveis = listaPlanos;
                        self.renderizarSlideOut = true;

                        if (listaPlanos.length == 0 || listaPlanos == null) {
                            self.renderizarSlideOut = false;
                        } else {
                            if (self.permiteAlterarPlanoPagamento && (!self.planoPagamentoSelecionado || self.planoPagamentoSelecionado == null)) {
                                self.planoPagamentoSelecionado = listaPlanos[0].codPlanoPgto;
                            }
                        }
                });
            });    
        }

        function preparaListaIdTurmaDisc(callback) {
            self.listaIdTurmaDisc = [];
            self.disciplinasSDD.forEach(function(disciplinaSDD) {
                if (disciplinaSDD.ADICIONADO) {
                    self.listaIdTurmaDisc.push(disciplinaSDD.IDTURMADISC);
                }
            });

            if (callback) {
                callback();
            }
        }

        function aoIniciarEtapaFinalizacao() {

            //Etapa de finallização será realizada automaticamente
            eduMatriculaService.liberarEtapaAtual(self.etapas);

            if (!self.textoComprovante) {
                eduMatriculaService.retornaComprovanteMatriculaAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET,
                    function (result) {
                        if (result && result.value) {
                            self.textoComprovante = $sce.trustAsHtml(result.value);
                        }
                    });
            }
        }        

        //Evento disparado quando é iniciallizado a etapa de seleção de disciplinas
        function aoIniciarEtapaDisciplinas() {

            if (self.disciplinasSDD.length == 0) {
                // Limpa a lista de planos de pagamento disponíveis, pois ela precisará ser
                // carregada novamente, devido a alteração das disciplinas.
                // Carga realizada na function aoIniciarEtapaPlanosPagamento
                self.planosPagamentoDisponiveis = [];
                self.renderizarSlideOut = false;
                self.matricItensList = [];
                self.matricItensListDel = [];
                $scope.permiteAvancarEtapaDisc = true;
                                
                if (self.habilitacaoSelecionada) {
                    
                    eduMatriculaService.retornaDiscplinasMatriculaESAsync(self.habilitacaoSelecionada.IDPERLET,
                        function (data) {

                        self.disciplinasMatriculadas = resolveTipoDisciplina(data.TURMASMATRICULADAS);
                        self.disciplinasSDD = resolveTipoDisciplina(data.TURMADISC);
                        self.horariosTurmaDisc = resolveDiasSemana(data.HORARIOTURMADISC);

                        self.requisitos = data.REQUISITO;
                        self.paramsEdu = data.MATRICPARAMS[0];

                        preencheDadosDisciplinas(data, function() {
                            sincronizaMatriculadasComSDD();
                            carregaPlanoPgtoDefault(executaValidacoes);
                            if (self.paramsEdu.VISUALIZARSIMULACAOPAGTO == true) {
                                carregaPlanosPgto();
                            }
                            defineLarguraGridCreditosAcademicos();
                        });

                        //Caso não esteja parametrizado no sistema, será ocultado o campo TURMA da listagm de disciplinas
                        if (!self.parametrosMatricula.ExibirCampoTurmaNaListagemDisciplinasRematricula) {
                            self.gridInstance.hideColumn(4);
                        }

                        angular.element(document).ready(function () {
                            funcoesJqueryInicializacao();
                            fechaSlideOutAoClicarFora();
                            minimizaPainelResumo();

                            var codUsuarioPulaTutorial = 'pulaTutorial-' + $rootScope.InformacoesLogin.login;

                            if (angular.isDefined(window.localStorage.getItem(codUsuarioPulaTutorial)) && 
                            window.localStorage.getItem(codUsuarioPulaTutorial) != null) 
                            {
                                var usuarioPulaTutorial = window.localStorage.getItem(codUsuarioPulaTutorial);                                
                            }
                            if(!usuarioPulaTutorial)
                                executaTutorialMatricula();

                        });
                    });
            } else {
                self.disciplinasMatriculadas = [];
            }
          }
        }

        function funcoesJqueryInicializacao() {
            $(".totvs-group").removeAttr("title");
            $(".totvs-group a").eq(0).click();
            $("#painel-validacoes").slideToggle();
            $("#painel-informacoes-turma-disciplina").slideToggle();

            $('#filtroDisciplina').keyup(function(e){
                if(e.keyCode == 13)
                {
                    pesquisaDisciplinasExtras();  
                }
            });                             
        }

        function fechaSlideOutAoClicarFora() {
            $(window).click(function(elementoClicado) {

                var $elementoClicado = $(elementoClicado.target);

                if (($elementoClicado.closest('.slideout_img').length == 0 && 
                     $elementoClicado.closest('.slideout_inner').length == 0 &&
                     $elementoClicado.closest('.controller_planopagamentoselecionado').length == 0) &&
                    $('.slideout').hasClass('slideoutOpen') == true) {
                    $('.slideout').toggleClass('slideoutOpen');
                }
            });
        }

        function defineLarguraGridCreditosAcademicos() {
            if ((self.habilitacaoSelecionada.MINCREDPERIODO != null && self.habilitacaoSelecionada.MINCREDPERIODO > 0) &&
                (self.habilitacaoSelecionada.MAXCREDPERIODO != null && self.habilitacaoSelecionada.MAXCREDPERIODO > 0)) {
                self.larguraGridCreditosAcademicos = 3;
            }

            if ((self.habilitacaoSelecionada.MINCREDPERIODO == null || self.habilitacaoSelecionada.MINCREDPERIODO == 0) ||
                (self.habilitacaoSelecionada.MAXCREDPERIODO == null || self.habilitacaoSelecionada.MAXCREDPERIODO == 0)) {
                self.larguraGridCreditosAcademicos = 4;
            }

            if ((self.habilitacaoSelecionada.MINCREDPERIODO == null || self.habilitacaoSelecionada.MINCREDPERIODO == 0) &&
                (self.habilitacaoSelecionada.MAXCREDPERIODO == null || self.habilitacaoSelecionada.MAXCREDPERIODO == 0)) {
                self.larguraGridCreditosAcademicos = 6;
            }
        }

        /**
         * Ao iniciar a tela de matrícula. Esta função sincroniza a lista de discplinas já matriculadas/pré-matriculadas do aluno,
         * com a lista de todas as disciplinas exibidas no SDD.
         *
         * ATENÇÃO: Centralizamos em todos os pontos da tela de seleção de disciplinas da matrícula do ensino superior a lista "self.disciplinasSDD".
         */
        function sincronizaMatriculadasComSDD() {
            self.disciplinasSDD.forEach(function(disciplinaSDD) {

                self.disciplinasMatriculadas.forEach(function(matriculada) {
                    if (matriculada.CODCOLIGADA === disciplinaSDD.CODCOLIGADA &&
                        matriculada.IDTURMADISC === disciplinaSDD.IDTURMADISC) {
                            
                            //indica que esta Turma/Disciplina já estava matriculada ao iniciar a matrícula.
                            disciplinaSDD.MATRICULADO = true;

                            //indica que a disciplina foi incluída no grupo de disciplinas adicionadas.
                            disciplinaSDD.ADICIONADO = true;

                            //Se possui mais de uma Turma/Disc e uma delas estiver matriculado,
                            //Define a propriedade para exibir o ícone de matriculado em qualquer uma delas
                            //Pois qualquer uma destas Turma/Disc pode ser listadas na sugestão de disciplinas.
                            self.disciplinasSDD.forEach(function(item) {
                                if (item.CODCOLIGADA === disciplinaSDD.CODCOLIGADA && 
                                    item.CODDISC === disciplinaSDD.CODDISC)
                                {
                                    item.EXIBEMATRICULADO = true;
                                    //indica que alguma turma/disciplina com esta disciplina já estava matriculada ao iniciar a matrícula (necessário para a troca de turma).
                                    item.DISCIPLINAMATRICULADA = true;
                                }
                            });
                    }
                });
            });

            self.disciplinasSDD.forEach(function(disciplina) {
                /* Só permite incluir as disciplinas que forem adicionadas durante o processo de matrícula
                   As que já estiverem previamente matriculadas não poderão ser excluídas */
                if ((self.paramsEdu.BLOQUEIAINCLUSAODISCIPLINASMATRICULADAS === true &&
                     disciplina.DISCIPLINAMATRICULADA === true) ||
                     self.paramsEdu.BLOQUEIAINCLUSAODISCIPLINASMATRICULADAS == false) {
                    disciplina.PERMITEINCLUIR = true;
                } else {
                    disciplina.PERMITEINCLUIR = false;
                }

                /* Só permite excluir as disciplinas que forem adicionadas durante o processo de matrícula
                   As que já estiverem previamente matriculadas não poderão ser excluídas */
                if (self.paramsEdu.BLOQUEIAEXCLUSAODISCIPLINASMATRICULADAS === true &&
                    disciplina.DISCIPLINAMATRICULADA === true) {
                    disciplina.PERMITEEXCLUIR = false;
                } else {
                    disciplina.PERMITEEXCLUIR = true;
                }

                if (disciplina.PERIODO == i18nFilter('l-periodoDiscExtra', '[]', 'js/aluno/matricula')) {
                    disciplina.CODPERIODO = 0;
                }
            })
        }

        function adicionarDisciplinaDoHorario(turmaDisc) {
            matricularDisciplina(turmaDisc)
            $('#modalDisciplinasDoHorario').modal('hide');
        }

        /**
         * Adiciona uma disciplina na lista de disciplinas matriculadas ou a serem matriculadas.
         * Também é a função para a TROCA de Turma/disciplinas
         */
        function matricularDisciplina(turmaDisc, disciplinaExtra) {
            if (validaInclusao(turmaDisc) == false)
                return;
            //Verifica se já está "adicionado" e o caso é a troca de turma.
            var discParaTrocaTurma = self.disciplinasSDD.filter(function (x) { return x.ADICIONADO === true &&
                x.CODCOLIGADA === turmaDisc.CODCOLIGADA &&
                x.CODDISC === turmaDisc.CODDISC; });
            if (discParaTrocaTurma.length > 0) {
                removerDisciplina(discParaTrocaTurma[0]);
            }
            atualizaListaDiscMatriculadas(turmaDisc, true, disciplinaExtra);
        }        

        /**
         * Remove uma disciplina da lista de disciplinas matriculadas ou a serem matriculadas.
         */
        function removerDisciplina(turmaDisc) {         
            atualizaListaDiscMatriculadas(turmaDisc, false);
        }

        /**
         * Responsável por atualizar a lista de disciplinas do SDD após qualquer tipo de ação.
         * INCLUSÃO de disciplinas. EXCLUSÃO ou TROCA de turma/disc.
         */
        function atualizaListaDiscMatriculadas(turmaDisc, matriculado, disciplinaExtra) {
            if (disciplinaExtra === true) {
                turmaDisc.ADICIONADO = true;
                turmaDisc.PERMITEEXCLUIR = true;
                turmaDisc.EXIBEMATRICULADO = true;
                self.disciplinasSDD = appendObjTo(self.disciplinasSDD, turmaDisc);

                $('#modalDisciplinasExtras').modal('hide');
            } else {

                if (turmaDisc.TIPODISC == 'EXT' && matriculado == false) {
                    self.disciplinasSDD = self.disciplinasSDD.filter(function (x) { return x.IDTURMADISC != turmaDisc.IDTURMADISC; });
                }
                                
                self.disciplinasSDD.forEach(function(item) {
                    if (item.CODCOLIGADA === turmaDisc.CODCOLIGADA &&
                        item.IDTURMADISC === turmaDisc.IDTURMADISC) {
                        
                        /*recebe o valor passado para a função. 
                            True quando orinundo de uma inclusão ou troca de turma/disc. 
                            False quando removido da lista de disciplinas matriculadas. */
                        item.ADICIONADO = matriculado;
                        self.disciplinaSelecionadaMatricula = item;
                    }
                });
    
                //A propriedade EXIBEMATRICULADO ficou responsável por exibir o ícone de disciplina já incluída nas disciplinas do SDD.
                //Ficou necessário em casos de disciplina que possuem mais de uma turma/Disc.
                self.disciplinasSDD.forEach(function(item) {
                    if (item.CODCOLIGADA === turmaDisc.CODCOLIGADA && 
                        item.CODDISC === turmaDisc.CODDISC)
                    {
    
                        /* Só permite excluir as disciplinas que forem adicionadas durante o processo de matrícula
                           As que já estiverem previamente matriculadas não poderão ser excluídas */
                        if (self.paramsEdu.BLOQUEIAEXCLUSAODISCIPLINASMATRICULADAS === true &&
                            item.DISCIPLINAMATRICULADA === true) {
                            item.PERMITEEXCLUIR = false;
                        } else {
                            item.PERMITEEXCLUIR = true;
                        }
    
                        item.EXIBEMATRICULADO = matriculado;
                    }
                });
            }

            executaValidacoes();

            AbreAccordionDisciplinas();
        }        

        /**
         * Função que centraliza a chamada de todas as validações. Executada no momento da abertura da tela de seleção de disciplinas da matrícula,
         * ao adicionar, remover ou trocar de turma/disciplina.
         */
        function executaValidacoes() {
            limpaErrosTodasDisc();
            validaHorariosMatriculados();
            validaCoRequisitoTodasDisc();
            somaCreditosDiscMatriculadas();
            criaMensagemInformacaoUsuario();
            geraQuadroHorario();
        }

        function minimizaPainelQuadroHorario() {
            $("#bodyPainelQuadroHorario").slideToggle();
            self.quadroHorarioMinimizado = !self.quadroHorarioMinimizado;
        }

        function minimizaPainelResumo() {
            $("#bodyPainelResumo").slideToggle();
            self.resumoMinimizado = !self.resumoMinimizado;
        }        

        /**
         * Função que recebe a disciplina selecionada pelo usuário na lista de SDD ou disciplinas matriculadas.
         * Exibe as informações de todas as turmas/disciplinas para o usuário.
         */
        function disciplinaSelecionada(disciplina) {

            var disciplinaSelecionada = self.disciplinasSDD.filter(function (x) { return x.CODCOLIGADA === disciplina.CODCOLIGADA &&
                x.CODDISC === disciplina.CODDISC &&
                x.ADICIONADO === true; });

            if (disciplinaSelecionada.length > 0) {
                self.disciplinaSelecionadaMatricula = disciplinaSelecionada[0];
            } else {
                self.disciplinaSelecionadaMatricula = disciplina;
            }

            $(".clickable-row").each(function() {
                $(this).removeClass('active');
            });

            $('.table').on('click', '.clickable-row', function(event) {
                $(this).addClass('active').siblings().removeClass('active');
            });

            exibePainelInformacoes();
        }

        /**
         * Responsável por exibir o painel de erros de validações de todas as disciplinas.
         */
        function exibePainelErros() {

            if (self.exibePainelValidacoes === false) {
                self.exibePainelValidacoes = true;

                if (self.exibePainelInformacoes === false) {
                    setTimeout(function wait(){
                        $("#painel-validacoes").hide();
                        $("#painel-validacoes").slideToggle();

                        defineScrollParaPaienel('painel-validacoes');
                    }, 200);
                } else {
                    defineScrollParaPaienel('painel-validacoes');
                }

            } else {
                defineScrollParaPaienel('painel-validacoes');
            }

            self.exibePainelInformacoes = false;
        }        

        function exibePainelInformacoes() {
            if (self.exibePainelInformacoes === false) {
                self.exibePainelInformacoes = true;
                
                if (self.exibePainelValidacoes == false) {
                    setTimeout(function wait(){
                        $("#painel-informacoes-turma-disciplina").hide();
                        $("#painel-informacoes-turma-disciplina").slideToggle();

                        defineScrollParaPaienel('painel-informacoes-turma-disciplina');
                    }, 200);
                } else {
                    defineScrollParaPaienel('painel-informacoes-turma-disciplina');
                }
            } else {
                defineScrollParaPaienel('painel-informacoes-turma-disciplina');
            }
            
            self.exibePainelValidacoes = false;
        }

        /**
         * Responsável por ocultar o painel de informações de todas as turmas/disciplinas
         */
        function ocultaPainelInformacoesEValidacoes() {
            $("#painel-validacoes").slideToggle();
            $("#painel-informacoes-turma-disciplina").slideToggle();

            self.disciplinaSelecionadaMatricula = [];

            self.exibePainelValidacoes = false;
            self.exibePainelInformacoes = false;
        }        

        function preencheDadosDisciplinas(dataSet, callback) {
            self.disciplinasSDD.forEach(function (turmaDisc) {
                turmaDisc.HORARIOS = dataSet.HORARIOTURMADISC.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.IDTURMADISC === turmaDisc.IDTURMADISC; });
                turmaDisc.REQUISITOS = dataSet.REQUISITO.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.CODDISC === turmaDisc.CODDISC || (x.TIPO === 'C' && x.CODDISCREQ === turmaDisc.CODDISC); });
                turmaDisc.SUBTURMAS = dataSet.SUBTURMAS.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.IDTURMADISC === turmaDisc.IDTURMADISC; });
                turmaDisc.PROFESSORTURMADISC = dataSet.PROFESSORTURMADISC.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.IDTURMADISC === turmaDisc.IDTURMADISC; });
                turmaDisc.POSSUICHOQUEHORARIO = false;
                turmaDisc.DISCIPLINASCHOQUE = [];
                turmaDisc.POSSUIERROCOREQUISITO = false;
                turmaDisc.DISCERROCORREQUISITO = [];
                turmaDisc.POSSUIERROVALIDACAO = false;
            });
            self.disciplinasMatriculadas.forEach(function (turmaDisc) {
                turmaDisc.HORARIOS = dataSet.HORARIOTURMADISC.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.IDTURMADISC === turmaDisc.IDTURMADISC; });
                turmaDisc.REQUISITOS = dataSet.REQUISITO.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.CODDISC === turmaDisc.CODDISC; });
                turmaDisc.SUBTURMAS = dataSet.SUBTURMAS.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.IDTURMADISC === turmaDisc.IDTURMADISC; });
                turmaDisc.PROFESSORTURMADISC = dataSet.PROFESSORTURMADISC.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA && x.IDTURMADISC === turmaDisc.IDTURMADISC; });
                turmaDisc.POSSUICHOQUEHORARIO = false;
                turmaDisc.DISCIPLINASCHOQUE = [];
                turmaDisc.POSSUIERROCOREQUISITO = false;
                turmaDisc.DISCERROCORREQUISITO = [];
                turmaDisc.POSSUIERROVALIDACAO = false;
            });
            callback();
        }        

        function validaSubTurma(turmaDisc) {
            if ((self.paramsEdu.MATRICULARSUBTURMAS == eduEnumsConsts.EduMatricularSubTurmasEnum.PermitidoObrigatorio) &&
                (turmaDisc.SUBTURMAS !== null && turmaDisc.SUBTURMAS.length > 0) &&
                (turmaDisc.CODSUBTURMA == null || turmaDisc.CODSUBTURMA == ''))
            {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-subturma', [], 'js/aluno/matricula')
                });

                return false;
            }
            else
                return true;
        }

        /**
         * Responsável por realizar validações, antes de incluir uma disciplina na lista para ser matriculada.
         */
        function validaInclusao(turmaDisc) {
            var inclusaoValida = false;

            inclusaoValida = validaSubTurma(turmaDisc);
            return inclusaoValida;
        }

        /**
         * Função responsável por criar um Tooltip com todos os erros de validação de uma disciplina.
         * São mensagens diferentes das exibidas no painel de validações.
         */
        function criaMensagemInformacaoUsuario() {
            self.possuiDisciplinaAdicionada = false;
            var possuiAlgumErro = false;
            var totalErrosValidacao = 0;

            self.disciplinasSDD.forEach(function(item) {
                var mensagemInformacao = '';
                var numMensagens = 0;

                if (item.ADICIONADO) {
                    self.possuiDisciplinaAdicionada = true;
                }

                if (item.POSSUIERROCOREQUISITO) {
                    mensagemInformacao = i18nFilter('l-requerCorequisito', '[]', 'js/aluno/matricula');
                    numMensagens++;

                    item.POSSUIERROVALIDACAO = true;
                }

                if (item.erroMaxCredito) {
                    if (numMensagens > 0) {
                        mensagemInformacao += '\n';
                    }

                    mensagemInformacao += i18nFilter('l-maxCredito', '[]', 'js/aluno/matricula');
                    numMensagens++;
                    item.POSSUIERROVALIDACAO = true;
                }

                if (item.erroMinCredito) {
                    if (numMensagens > 0) {
                        mensagemInformacao += '\n';
                    }

                    mensagemInformacao += i18nFilter('l-minCredito', '[]', 'js/aluno/matricula');
                    numMensagens++;
                    item.POSSUIERROVALIDACAO = true;
                }

                if (item.POSSUICHOQUEHORARIO) {
                    if (numMensagens > 0) {
                        mensagemInformacao += '\n';
                    }

                    if (self.paramsEdu.PERMITECHOQUEHORARIO) {
                        item.ICONEDEINFORMACAO = true;
                        mensagemInformacao += i18nFilter('l-alertaPossuiChoqueHorario', '[]', 'js/aluno/matricula');

                        if (numMensagens > 0) {
                            item.ICONEDEINFORMACAO = false;
                        }

                    } else {
                        item.ICONEDEINFORMACAO = false;
                        mensagemInformacao += i18nFilter('l-possuiChoqueHorario', '[]', 'js/aluno/matricula');
                    }

                    numMensagens++;
                    item.POSSUIERROVALIDACAO = true;
                }

                if (numMensagens > 0) {
                    possuiAlgumErro = true;
                    totalErrosValidacao += numMensagens;
                }

                item.InfoValidacao = mensagemInformacao;
            });

            if (possuiAlgumErro == false && (self.erroMinCredito == false && self.erroMinCredito == false)) {
                self.exibePainelValidacoes = false;
                $scope.permiteAvancarEtapaDisc = true;
                self.quantidadeErrosValidacao = 0;
            } else {
                self.quantidadeErrosValidacao = totalErrosValidacao;
                $scope.permiteAvancarEtapaDisc = false;
            }
        }

        function limpaErrosTodasDisc() {
            self.disciplinasSDD.forEach(function(turmaDisc) {
                turmaDisc.POSSUICHOQUEHORARIO = false;
                turmaDisc.DISCIPLINASCHOQUE = [];
                turmaDisc.POSSUIERROCOREQUISITO = false;
                turmaDisc.DISCERROCORREQUISITO = [];

                turmaDisc.POSSUIERROVALIDACAO = false;
            });
        }

        function validaHorariosMatriculados() {
            if (self.disciplinasSDD.length > 0) {
                self.horariosMatriculados = [];
                self.disciplinasSDD.filter(function (x) { return x.ADICIONADO === true; }).forEach(function (turmaDisc) {
                    adicionaHorariosTurmaDisc(turmaDisc);
                });
            }
        }

        function adicionaHorariosTurmaDisc(turmaDisc) {
            var horariosTurmaDisc;
            if (!turmaDisc.CODSUBTURMA) {
                horariosTurmaDisc = turmaDisc.HORARIOS.filter(function (x) { return x.IDTURMADISC == turmaDisc.IDTURMADISC; });
            }
            else {
                horariosTurmaDisc = turmaDisc.HORARIOS.filter(function (x) { return x.IDTURMADISC == turmaDisc.IDTURMADISC &&
                    x.CODSUBTURMA == turmaDisc.CODSUBTURMA; });
            }
            if (horariosTurmaDisc.length > 0) {
                self.horariosMatriculados = self.horariosMatriculados.concat(horariosTurmaDisc);
                horariosTurmaDisc.forEach(function (horario) {
                    validaChoqueHorario(horario);
                });
            }
        }

        function validaChoqueHorario(horario) {
            var horariosMesmoDia = self.horariosMatriculados.filter(function (x) { return x.IDTURMADISC != horario.IDTURMADISC && x.DIASEMANA === horario.DIASEMANA; });

            horariosMesmoDia.forEach(function (horarioMesmoDia) {
                // Verifica choque de horarios
                if (horario.HORAINICIAL === horarioMesmoDia.HORAINICIAL && horario.HORAFINAL === horarioMesmoDia.HORAFINAL) {
                    validaChoqueDatas(horario, horarioMesmoDia);
                }
                // Verifica se a hora inical da disciplina a ser inserida está no intervalo de alguma outra disciplina
                else if (horario.HORAINICIAL >= horarioMesmoDia.HORAINICIAL && horario.HORAINICIAL < horarioMesmoDia.HORAFINAL) {
                    validaChoqueDatas(horario, horarioMesmoDia);
                }
                // Verifica se a hora final da disciplina a ser inserida está no intervalo de alguma outra disciplina
                else if (horario.HORAFINAL > horarioMesmoDia.HORAINICIAL && horario.HORAFINAL <= horarioMesmoDia.HORAFINAL) {
                    validaChoqueDatas(horario, horarioMesmoDia);
                }
                //Verifica se o horário da disciplina corrente possui a hora incial e final maiores. (Ex. disciplina corrente de 7 as 11 verificando horario com
                //uma disciplina de 8 as 10)
                else if (horario.HORAINICIAL < horarioMesmoDia.HORAINICIAL && horario.HORAFINAL > horarioMesmoDia.HORAFINAL) {
                    validaChoqueDatas(horario, horarioMesmoDia);
                }
            });
        }

        function validaChoqueDatas(horario, horarioMesmoDia) {
            var choqueHorario = false;
            var dataInicialHorario = null;
            var dataFinalHorario = null;
            var dataInicialMesmoDia = null;
            var dataFinalMesmoDia = null;
            var maxDate = new Date(8640000000000000);
            var minDate = new Date(-8640000000000000);

            if (horario.DATAINICIAL !== null && horarioMesmoDia !== null) {

                if (horario.DATAINICIAL) {
                    dataInicialHorario = new Date(horario.DATAINICIAL);
                }
                else {
                    dataInicialHorario = minDate;
                }

                if (horario.DATAFINAL) {
                    dataFinalHorario = new Date(horario.DATAFINAL);
                }
                else {
                    dataFinalHorario = maxDate;
                }

                if (horarioMesmoDia.DATAINICIAL) {
                    dataInicialMesmoDia = new Date(horarioMesmoDia.DATAINICIAL);
                }
                else {
                    dataInicialMesmoDia = minDate;
                }
                
                if (horarioMesmoDia.DATAFINAL) {
                    dataFinalMesmoDia = new Date(horarioMesmoDia.DATAFINAL);
                }
                else {
                    dataFinalMesmoDia = maxDate;
                }

                if (((dataInicialHorario >= dataInicialMesmoDia) && (dataInicialHorario <= dataFinalMesmoDia)) ||
                    ((dataFinalHorario >= dataInicialMesmoDia) && (dataFinalHorario <= dataFinalMesmoDia)) ||
                    ((dataInicialMesmoDia >= dataInicialHorario) && (dataInicialMesmoDia <= dataFinalHorario)) ||
                    ((dataFinalMesmoDia >= dataInicialHorario) && (dataFinalMesmoDia <= dataFinalHorario)))
                    choqueHorario = true;                    
            }
            else
                choqueHorario = true;

            if (choqueHorario) {
                var turmaDiscAdicionando = self.disciplinasSDD.find(function (x) { return x.IDTURMADISC === horario.IDTURMADISC; });
                var turmaDiscChoque = self.disciplinasSDD.find(function (x) { return x.IDTURMADISC === horarioMesmoDia.IDTURMADISC; });
                turmaDiscAdicionando.POSSUICHOQUEHORARIO = true;

                if (!turmaDiscAdicionando.DISCIPLINASCHOQUE.includes(turmaDiscChoque.CODDISC + ' - ' + turmaDiscChoque.DISCIPLINA))
                    turmaDiscAdicionando.DISCIPLINASCHOQUE.push(turmaDiscChoque.CODDISC + ' - ' + turmaDiscChoque.DISCIPLINA);

                turmaDiscChoque.POSSUICHOQUEHORARIO = true;
                if (!turmaDiscChoque.DISCIPLINASCHOQUE.includes(turmaDiscAdicionando.CODDISC + ' - ' + turmaDiscAdicionando.DISCIPLINA))
                    turmaDiscChoque.DISCIPLINASCHOQUE.push(turmaDiscAdicionando.CODDISC + ' - ' + turmaDiscAdicionando.DISCIPLINA);
            }                
        }

        function validaCoRequisitoTodasDisc() {
            self.disciplinasSDD.filter(function (x) { return x.ADICIONADO === true; }).forEach(function (turmaDisc) {
                validaCoRequisitoDisc(turmaDisc);
            });
        }

        function validaCoRequisitoDisc(turmaDisc) {
            if (angular.isDefined(turmaDisc.REQUISITOS) && turmaDisc.REQUISITOS.length > 0) {
                turmaDisc.REQUISITOS.forEach(function (requisito) {
                    if (requisito.TIPO === 'C') {
                        var turmaDiscReq = self.disciplinasSDD.filter(function (x) { return x.ADICIONADO === true && (x.CODDISC === requisito.CODDISCREQ ||
                            x.CODDISC == requisito.CODDISC); });
                        if (turmaDiscReq.length < 2) {
                            turmaDisc.POSSUIERROCOREQUISITO = true;
                            if (requisito.CODDISCREQ !== turmaDisc.CODDISC)
                                turmaDisc.DISCERROCORREQUISITO.push(requisito.CODDISCREQ + ' - ' + requisito.DISCIPLINA);
                        }
                    }
                });
            }
        }

        function somaCreditosDiscMatriculadas(){
            self.somaCreditos = 0;
            self.totalDiscExtra = 0;
            self.totalDiscObrigatoria = 0;
            self.totalDiscOptEletiva = 0;
            self.somaCreditosFinanceiros = 0;
            self.totalDiscEquivalente = 0;

            self.disciplinasSDD.forEach(function (item) {
                if (item.ADICIONADO) {
                    self.somaCreditos += item.CREDITOS;
                    self.somaCreditosFinanceiros += item.CREDITOSCOB;

                    if (item.TIPODISC === 'B') {
                        self.totalDiscObrigatoria += 1;
                    } else if (item.TIPODISC === 'O' || item.TIPODISC === 'E') {
                        self.totalDiscOptEletiva += 1;
                    } else if ((item.TIPODISC === null && !item.ISEQUIVALENTE) || item.TIPODISC == 'EXT') {
                        self.totalDiscExtra += 1;
                    } else if (item.ISEQUIVALENTE){
                        self.totalDiscEquivalente += 1;
                    }
                }
            });

            verificaErrosDeCredito();
        }

        function verificaErrosDeCredito() {
            if (self.habilitacaoSelecionada.MAXCREDPERIODO > 0) {
                self.erroMaxCredito = self.somaCreditos > self.habilitacaoSelecionada.MAXCREDPERIODO;

                if (self.erroMaxCredito) {
                    self.tooltipMinMaxCreditos = i18nFilter('l-excedeuCreditos', '[]', 'js/aluno/matricula');
                    self.tooltipMinMaxCreditos = self.tooltipMinMaxCreditos.replace('@', self.habilitacaoSelecionada.MAXCREDPERIODO);
                }
            }
            
            if (self.habilitacaoSelecionada.MINCREDPERIODO > 0) {
                self.erroMinCredito = self.somaCreditos < self.habilitacaoSelecionada.MINCREDPERIODO;

                if (self.erroMinCredito) {
                    self.tooltipMinMaxCreditos = i18nFilter('l-quantMinCreditos', '[]', 'js/aluno/matricula');
                    self.tooltipMinMaxCreditos = self.tooltipMinMaxCreditos.replace('@', self.habilitacaoSelecionada.MINCREDPERIODO);
                }
            }
        }

        function adicionaHorariosMatriculados(horarios) {
            if (horarios.length > 0) {
                self.horariosMatriculados = self.horariosMatriculados.concat(horarios);
            }
        }

        function executarContratoMatricula() {

            if (!eduMatriculaService.retornarEtapaAtual(self.etapas).realizado) {
                if (self.parametrosMatricula.ContratoDisponivelImpressao) {
                    preparaListaIdTurmaDisc(function() {
                        eduMatriculaService.retornaContratoMatriculaAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL,
                            self.habilitacaoSelecionada.IDPERLET, self.planoPagamentoSelecionado, self.listaIdTurmaDisc,
                            function (result) {

                            if (result.Contrato === null) {
                                result.Contrato = '';
                            }

                            if (result.Contrato || result.Contrato === '') {
                                self.textoContratoFinanceiro = $sce.trustAsHtml(result.Contrato);
                                exibirModalContratoMatricula(true);
                            }
                        });
                    });
                } else {
                    self.AceiteContrato = true;
                    realizarMatricula();
                }
            } else {
                eduMatriculaService.avancarEtapa(self.etapas);
            }
        }

        function exibirModalContratoMatricula(exibir) {
            if (exibir) {
                self.htmlParaImpressao = self.textoContratoFinanceiro;
                $('#modalContratoMatricula').modal('show');
            } else {
                $('#modalContratoMatricula').modal('hide');
            }
        }

        function exibirModalComprovante(exibir) {
            if (exibir) {
                self.htmlParaImpressao = self.textoComprovante;
                $('#modalComprovanteMatricula').modal('show');
            } else {
                $('#modalComprovanteMatricula').modal('hide');
            }
        }

        /**
         * Executar processo de confirmação de matrícula EB.
         */
        function realizarMatricula() {

            if (self.AceiteContrato) {
                exibirModalContratoMatricula(false);

                eduMatriculaService.efetuarEnsinoSuperiorMatriculaAsync(
                    self.listaDiscAlteradas,
                    self.listaDiscAdd,
                    self.matricItensListDel,
                    self.listaDiscSubstituicao,
                    self.planoPagamentoSelecionado, 
                    self.habilitacaoSelecionada.IDPERLET, function (result) {
                    if (result.$messages[0].type === 'success') {

                        if (self.parametrosMatricula.MensagemConfirmacaoMatricula) {
                            self.mensagemConfirmacaoMatricula = self.parametrosMatricula.MensagemConfirmacaoMatricula;
                        }
                        else {
                            self.mensagemConfirmacaoMatricula = i18nFilter('l-msg-default-matricula-confirmada', '[]', 'js/aluno/matricula');
                        }

                        self.contratoDisponivelImpressao = self.parametrosMatricula.ContratoDisponivelImpressao;
                        self.comprovanteDisponivelImpressao = self.parametrosMatricula.ComprovanteDisponivelImpressao;

                        eduMatriculaService.liberarProximaEtapa(self.etapas);
                        eduMatriculaService.avancarEtapa(self.etapas);

                        self.processoFinalizado = true;

                        if (result['Lancamentos'].data && result['Lancamentos'].data.BOLETOS.SBoletos.length > 0) {
                            self.boleto = result['Lancamentos'].data.BOLETOS.SBoletos[0];
                        }
                    }
                });
            } else {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-necessario-aceite-contrato', '[]', 'js/aluno/matricula')
                });
            }
        }

        function geraQuadroHorario() {

            if (self.horariosMatriculados.length == 0) 
                return;
            else {
                self.horariosMatriculados = resolveDiasSemana(self.horariosMatriculados);
            }

            self.horariosMatriculados.sort(function(a, b) {
                var database = '01/01/1984 ';

                if (new Date(database + a.HORAINICIAL) > new Date(database + b.HORAINICIAL))
                  return 1;
                else if (new Date(database + a.HORAINICIAL) < new Date(database + b.HORAINICIAL))
                  return -1;
                else return 0;
            });

            var itemAnterior;

            self.horariosMatriculados.forEach(function(item, i) {
                if (i == 0) {
                    item.ROW = 1;
                    itemAnterior = item;
                } else if (i > 0) {
                    if (item.HORAINICIAL == itemAnterior.HORAINICIAL) {
                        item.ROW = itemAnterior.ROW;
                    } else if (item.HORAINICIAL > itemAnterior.HORAINICIAL) {
                        item.ROW = itemAnterior.ROW+1;
                        itemAnterior = item;
                    }
                }

                var disciplina = self.disciplinasSDD.filter(function (x) { return x.CODCOLIGADA == item.CODCOLIGADA && x.IDTURMADISC == item.IDTURMADISC; });

                if (disciplina.length > 0) {
                    item.TURMADISC = disciplina[0];
                }
            });

            self.diasSemana = ['1', '2', '3', '4', '5', '6', '7'];
            self.possuiHorarioDomingo = self.horariosMatriculados.filter(function (x) { return x.CODDIA === 1; }).length > 0;
            self.possuiHorarioSabado = self.horariosMatriculados.filter(function (x) { return x.CODDIA === 7; }).length > 0;

            if (self.possuiHorarioDomingo == false) {
                self.diasSemana.shift();
            }

            if (self.possuiHorarioSabado == false) {
                self.diasSemana.pop();
            }

            self.diasSemana.sort();
            self.larguraColQuadroHorario =  90 / self.diasSemana.length;
        }

        function possuiDiscNoHorario(horario, codDia) {

            var disciplinasDoHorario = self.horariosMatriculados.filter(function (x) { return x.CODDIA == codDia &&
                x.HORAINICIAL == horario.HORAINICIAL &&
                x.HORAFINAL == horario.HORAFINAL; });

            if (disciplinasDoHorario.length > 0) {
                return true;
            } else return false;
        }

        function possuiErroValicaoHorario(horario, codDia) {

            var possuiErroValidacao = self.horariosMatriculados.filter(function (x) { return x.CODDIA == codDia &&
                x.HORAINICIAL == horario.HORAINICIAL &&
                x.HORAFINAL == horario.HORAFINAL &&
                x.TURMADISC.POSSUIERROVALIDACAO == true; });

            if (possuiErroValidacao.length > 0) {
                return true;
            } else return false;
        }

        function retornaCorFundoQuadroHorario(horario, codDia) {

            var possuiErroValidacao = self.horariosMatriculados.filter(function (x) { return x.CODDIA == codDia &&
                x.HORAINICIAL == horario.HORAINICIAL &&
                x.HORAFINAL == horario.HORAFINAL &&
                x.TURMADISC.POSSUIERROVALIDACAO == true; });            

            if (possuiErroValidacao.length > 0) {
                
                if (possuiErroValidacao[0].TURMADISC.ICONEDEINFORMACAO == true) {
                    return '#f7f5bf';
                } else {
                    return '#ffe4e4';
                }
                
            } else return '';
        }

        function retornaTurmasDisciplinasExtras(disciplina) {
            eduMatriculaService.retornaTurmasDisciplinasExtrasAsync(self.habilitacaoSelecionada.IDPERLET, 
                disciplina.TURMADISC[0].CODDISC,
                function (result) {
                    self.turmasDisciplinasExtras = result;
                });
        }

        function disciplinasDoHorarioModal(horario, codDia) {
            $('#modalDisciplinasDoHorario').modal('show');

            self.discDisponiveisHorario = [];

            self.disciplinasSDD.forEach(function(turmaDisc) {

                if (!turmaDisc.ADICIONADO) {
                    turmaDisc.HORARIOS.forEach(function(horarioItem) {

                        if (horarioItem.CODDIA == codDia &&
                            horario.HORAINICIAL >= horarioItem.HORAINICIAL &&
                            horario.HORAFINAL <= horarioItem.HORAFINAL) {

                            //Verifica se a disciplina já está matriculada. Se estiver, não exibe como sugestão
                            var disciplinaMatriculada = self.disciplinasSDD.filter(function (x) { return x.CODCOLIGADA === turmaDisc.CODCOLIGADA &&
                                x.CODDISC === turmaDisc.CODDISC &&
                                x.ADICIONADO === true; });

                            if (disciplinaMatriculada.length == 0) {
                                self.discDisponiveisHorario.push(turmaDisc);
                            }
                        }
                    });
                }
            });
        }

        function disciplinasExtrasModal() {
            self.disciplinasExtras = [];

            $('#modalDisciplinasExtras').modal('show');
            $('#modalDisciplinasExtras').on('shown.bs.modal', function () {
                $('#filtroDisciplina').val('');
                $('#filtroDisciplina').focus();
            });
        }

        function pesquisaDisciplinasExtras() {
            if ($('#filtroDisciplina').val().length > 0) {
                var codDiscSDD = '';

                self.disciplinasSDD.forEach(function(discSDD) {
                    codDiscSDD += discSDD.CODDISC + '*@';
                });

                codDiscSDD = codDiscSDD.slice(0, -2);

                eduMatriculaService.retornaPesquisaDisciplinasExtrasAsync(self.habilitacaoSelecionada.IDPERLET, $('#filtroDisciplina').val(), codDiscSDD,
                    function (result) {
                        self.disciplinasExtras = result;
                        self.disciplinasExtras.SDISCIPLINA.forEach(function(disciplina) {

                            disciplina.TURMADISC = [];

                            var turmaDiscExtra = self.disciplinasExtras.TURMASDISCEXTRAS.filter(function (x) { return x.CODCOLIGADA === disciplina.CODCOLIGADA &&
                                x.CODDISC === disciplina.CODDISC; });

                            if (turmaDiscExtra) {
                                disciplina.TURMADISC = appendObjTo(disciplina.TURMADISC, turmaDiscExtra);
                            }

                            if (disciplina.TURMADISC.length > 0) {

                                disciplina.TURMADISC.forEach(function(turmaDiscAtual) {
                                    turmaDiscAtual.IDHABILITACAOFILIAL = self.habilitacaoSelecionada.IDHABILITACAOFILIAL;
                                    turmaDiscAtual.CODPERIODO = 0;
                                    turmaDiscAtual.PERIODO = i18nFilter('l-periodoDiscExtra', '[]', 'js/aluno/matricula');

                                    if (!turmaDiscAtual.SUBTURMAS) {
                                        turmaDiscAtual.SUBTURMAS = [];

                                        var subturmas = self.disciplinasExtras.SUBTURMAS.filter(function (x) { return x.CODCOLIGADA == turmaDiscAtual.CODCOLIGADA &&
                                            x.IDTURMADISC == turmaDiscAtual.IDTURMADISC; });
                
                                        if (subturmas) {
                                            turmaDiscAtual.SUBTURMAS = appendObjTo(turmaDiscAtual.SUBTURMAS, subturmas);
                                        }
                                    }

                                    if (turmaDiscAtual.HORARIOS == undefined) {
                                        turmaDiscAtual.HORARIOS = [];

                                        var horarios = self.disciplinasExtras.HORARIOS.filter(function (x) { return x.CODCOLIGADA == turmaDiscAtual.CODCOLIGADA &&
                                            x.IDTURMADISC == turmaDiscAtual.IDTURMADISC; });
                
                                        if (horarios) {
                                            turmaDiscAtual.HORARIOS = appendObjTo(turmaDiscAtual.HORARIOS, horarios);
                                            turmaDiscAtual.HORARIOS = resolveDiasSemana(turmaDiscAtual.HORARIOS);
                                        }
                                    }

                                    if (turmaDiscAtual.PROFESSORES == undefined) {
                                        turmaDiscAtual.PROFESSORES = [];

                                        var professores = self.disciplinasExtras.PROFESSORES.filter(function (x) { return x.CODCOLIGADA == turmaDiscAtual.CODCOLIGADA &&
                                            x.IDTURMADISC == turmaDiscAtual.IDTURMADISC; });
                
                                        if (professores) {
                                            turmaDiscAtual.PROFESSORES = appendObjTo(turmaDiscAtual.PROFESSORES, professores);
                                        }
                                    }
                                });

                                disciplina.TURMADISC = resolveTipoDisciplina(disciplina.TURMADISC);
                            }
                        });
                    }
                );
            }
        }

        function appendObjTo(meuArray, novoObjeto) {
            if (meuArray == undefined) {
                return novoObjeto;
            }

            return meuArray.concat(novoObjeto);
        }

        function turmasDisciplinasExtras(disciplina) {
            if (disciplina != null) {
                eduMatriculaService.retornaTurmasDisciplinasExtrasAsync(self.habilitacaoSelecionada.IDPERLET, disciplina.CODDISC, 
                    function (result) {
                        disciplina.turmasDisciplinasExtras = result;
                    });
            }
        }

        function resolveDiasSemana(horarios) {
            horarios.forEach(function(item) {
                switch (parseInt(item.DIASEMANA)) {
                    case 1:
                        item.DIASEMANA = $filter('i18n')('l-dom', [], 'js/aluno/matricula');
                        item.CODDIA = 1;
                        break;                                                                       
                    case 2:
                        item.DIASEMANA = $filter('i18n')('l-seg', [], 'js/aluno/matricula');
                        item.CODDIA = 2;
                        break;                        
                    case 3:
                        item.DIASEMANA = $filter('i18n')('l-ter', [], 'js/aluno/matricula');
                        item.CODDIA = 3;
                        break;               
                    case 4:
                        item.DIASEMANA = $filter('i18n')('l-qua', [], 'js/aluno/matricula');
                        item.CODDIA = 4;
                        break;
                    case 5:
                        item.DIASEMANA = $filter('i18n')('l-qui', [], 'js/aluno/matricula');
                        item.CODDIA = 5;
                        break;
                    case 6:
                        item.DIASEMANA = $filter('i18n')('l-sex', [], 'js/aluno/matricula');
                        item.CODDIA = 6;
                        break;
                    case 7:
                        item.DIASEMANA = $filter('i18n')('l-sab', [], 'js/aluno/matricula');
                        item.CODDIA = 7;
                        break;                                                                           
                    default:
                        break;
                }
            });

            return horarios;
        }

        function resolveTipoDisciplina(turmaDisc) {

            turmaDisc.forEach(function(item) {
                switch (item.TIPO) {
                    case 'P':
                        item.TIPO = $filter('i18n')('l-presencial', [], 'js/aluno/matricula');
                        break;                                                                       
                    case 'D':
                        item.TIPO = $filter('i18n')('l-distancia', [], 'js/aluno/matricula');
                        break;
                    case 'S':
                        item.TIPO = $filter('i18n')('l-semipresencial', [], 'js/aluno/matricula');
                        break;
                    default:
                        break;
                }
            });

            return turmaDisc;
        }

        function criaListaDisciplinas(disciplinasSDD, callback) {
            self.matricItensList = [];
            self.matricItensListDel = [];
            self.listaDiscAdd = [];
            self.listaDiscAlteradas = [];
            self.listaDiscSubstituicao = [];
            disciplinasSDD.forEach(function(item){

                // Copia a disciplina para poder deletar alguns dados desnecessários para validações e confirmação de matrícula
                var itemCopy = angular.copy(item);

                itemCopy['RA'] = self.habilitacaoSelecionada.RA;
                itemCopy['CODTIPOCURSO'] = self.habilitacaoSelecionada.CODTIPOCURSO;
                delete itemCopy.PROFESSORTURMADISC;
                delete itemCopy.HORARIOS;
                delete itemCopy.SUBTURMAS;

                //Popula lista de disciplinas matrículadas
                if(itemCopy.ADICIONADO){
                    //Disciplinas a serem matrículadas devem assumir o status definido nos parâmetros
                    itemCopy.CODSTATUS = self.paramsEdu.STATUSDISCIPLINA;
                    self.matricItensList.push(itemCopy);

                    if (self.disciplinasMatriculadas.find(function (x) { return x.IDTURMADISC === itemCopy.IDTURMADISC; })) {
                        self.listaDiscAlteradas.push(itemCopy);
                    }
                    else {
                        self.listaDiscAdd.push(itemCopy);
                    }
                }
                
                //Popula lista de disciplinas excluídas
                if (itemCopy.ADICIONADO == false && self.disciplinasMatriculadas.find(function (x) { return x.IDTURMADISC === itemCopy.IDTURMADISC; })) {
                    self.matricItensListDel.push(itemCopy);
                }
            });

            if (angular.isFunction(callback)) {
                callback();
            }
        }

        function montaErroValidacaoPreCoRequisito(codisc, nomedisc, turma, logexcecao) {
            self.erroValidacaoPreCoRequisito += i18nFilter('l-msg-precorequisito-inicio', '[]', 'js/aluno/matricula') + codisc + ' - ' + nomedisc + '(' + turma + ')' + '<br/>';
            self.erroValidacaoPreCoRequisito +=  i18nFilter('l-msg-precorequisito-final', '[]', 'js/aluno/matricula');
            self.erroValidacaoPreCoRequisito += logexcecao + '<br/><br/>';

            exibePainelErros();
        }

        function calculaPlanoPagamento() {
            if ($('.slideout').hasClass('slideoutOpen') == false) {

                if (self.paramsEdu.VISUALIZARSIMULACAOPAGTO) {
                    carregaPlanosPgto();
                } else {
                    self.renderizarSlideOut = false;
                }
            }

            $('.slideout').toggleClass('slideoutOpen');
        }        
        
        // Função utilizada para formatar o campo 'Valor simulado' do grid de detalhamento das parcelas.
        function formataMoeda(item) {
            return $filter('currency')(item.valorLiquido);
        }

        /**
         * Exibir opção de pagamento na confirmação da matrícula
         *
         * @returns {Boolean}
         */
        function exibirOpcoesPagamento() {
            return self.boleto !== null && (exibeBtnPagamentoBoleto() || exibeBtnPagamentoCartao());
        }

        /**
         *
         * Realizar o pagamento via cartão.
         */
        function exibirDadosPagCartao() {
            eduFinanceiroFactory.exibirDadosPagCartao(self.boleto.IDBOLETO);
        }

        /**
         * Exibe opção de pagamento via cartão
         *
         * @returns {Boolean}
         */
        function exibeBtnPagamentoCartao () {
            return eduFinanceiroService.permitePagamentoCartao(self.boleto, parametrosEducacional);
        }

        /**
         * Exibe opção de pagamento via impressão do boleto
         *
         * @returns
         */
        function exibeBtnPagamentoBoleto() {
            return eduFinanceiroService.permitePagamentoBoleto(self.boleto, parametrosEducacional);
        }

        /**
         * Emite boleto
         */
        function emitirBoleto() {
            if (self.boleto) {
                eduFinanceiroService.visualizarBoleto(self.boleto.IDBOLETO, self.boleto.NOSSONUMERO, self.boleto.DATAVENCIMENTO, parametrosEducacional, function (objInfoBoleto) {
                    self.objInfoBoleto = objInfoBoleto;
                });
            }

        }

        function retornaLarguraColunaDisciplinaAdicionada(colunaCodigoDisciplina) {
            var disciplinasEncontradas = self.disciplinasSDD.filter(function (x) { return x.ADICIONADO == true &&
                x.TURMA.length > 11 ||
                x.CODDISC.length > 7; });            

            if (disciplinasEncontradas.length > 0) {
                if (colunaCodigoDisciplina) {
                    return "'col-xs-4 col-sm-3'";
                } else {
                    return "'col-xs-5 col-sm-7'";
                }
            } else {
                if (colunaCodigoDisciplina) {
                    return "'col-xs-3 col-sm-2'";
                } else {
                    return "'col-xs-6 col-sm-8'";
                }
            }
        }

        function defineScrollParaPaienel(painelASerExibido) {
            setTimeout(function() {
                $('html, body').animate({
                        scrollTop: $("#" + painelASerExibido).offset().top - 48 
                    }, 500);
                }, 210, function(){ isScrolling = false; }
            );
        }

        function redirecionaScrollPara (idComponente) {
             defineScrollParaPaienel(idComponente);
        };
        
        function executaTutorialMatricula() {
            var passosTutorial = [
                {
                    selector:'[data-tutorial="quadro-horario"]',
                    event_type:'next',
                    description: i18nFilter('l-msg-tutorial-quadro-horarios', '[]', 'js/aluno/matricula'),
                    timeout:500,
                    onBeforeStart: function() {
                        window.scrollTo(0, 0);
                        AbreAccordionDisciplinas();
                    }
                },
                {
                    selector:'[data-tutorial="quantidade-erros"]',
                    event:'click',
                    description: i18nFilter('l-msg-tutorial-quantidade-erros', '[]', 'js/aluno/matricula'),
                    showSkip: false,
                    timeout: 500,
                    onBeforeStart: function () {
                        redirecionaScrollPara('quadro-quantidade-erros')
                    }
                },
                {
                    selector:'[data-tutorial="painel-validacoes"]',
                    event_type:'next',
                    description: i18nFilter('l-msg-tutorial-painel-validacoes', '[]', 'js/aluno/matricula'),
                    timeout:600
                },
                {
                    selector:'.tableMatricula .clickable-row',
                    event:'click',
                    description: i18nFilter('l-msg-tutorial-disciplinas-sdd', '[]', 'js/aluno/matricula'),
                    showSkip: false,
                    scrollAnimationSpeed : 500,
                    onBeforeStart: function () {
                        var rowDiscMatricula = $('.tableMatricula .clickable-row');
                        redirecionaScrollPara(rowDiscMatricula[0].id);
                    }
                },
                {
                    selector:'[data-tutorial="informacoes-turma-disciplina"]',
                    event_type:'next',
                    description: i18nFilter('l-msg-tutorial-informacoes-turma-disciplina', '[]', 'js/aluno/matricula'),
                    scrollAnimationSpeed : 900
                }
              ];

            var tutorialDisciplinaExtra = {
                selector:'[data-tutorial="disciplinas-extras"]',
                description: i18nFilter('l-msg-tutorial-disciplinas-extras', '[]', 'js/aluno/matricula'),
                skipButton: {text: "Finalizar"},
                showNext: false,
                onBeforeStart: function () {
                    redirecionaScrollPara('add-disc-extra')
                },
                scrollAnimationSpeed : 900
            };
            var tutorialSdd = {
                selector:'[data-tutorial="sugestao-disciplina"]',
                description: i18nFilter('l-msg-tutorial-sugestao-disciplina', '[]', 'js/aluno/matricula'),
                onBeforeStart: function () {
                    redirecionaScrollPara('panel-sugestao-disciplinas')
                },
                timeout: 700
            };

              if(self.paramsEdu.VISUALIZAROPCAOMATDISCEXTRA == true){
                
                tutorialSdd.event_type = 'next';
                passosTutorial.push(tutorialSdd);
                passosTutorial.push(tutorialDisciplinaExtra);
              }
              else
              {
                tutorialSdd.skipButton = {text: "Finalizar"};
                tutorialSdd.showNext = false;
                passosTutorial.push(tutorialSdd);
              }

              var enjoyhint_instance = new EnjoyHint({});
              enjoyhint_instance.setScript(passosTutorial, $rootScope.InformacoesLogin.login);
              enjoyhint_instance.runScript();
        }
    }

    function AbreAccordionDisciplinas(){
        //Abre o primeiro item do Accordion de Disciplinas matriculadas, caso esteja fechada.
        setTimeout(function wait(){
            if (!$(".totvs-group span").eq(1).hasClass("open")) {
                $(".totvs-group a").eq(0).click();
            }
        }, 100);
    }
});
