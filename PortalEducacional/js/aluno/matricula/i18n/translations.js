[{
    "l-modalidade": {
        "pt": "Modalidade",
		"en": "Modality",
		"es": "Modalidad"
    },
    "l-disciplinasExtras": {
        "pt": "Disciplinas extras",
		"en": "Disciplinas extras",
		"es": "Disciplinas extras"
    },    
    "l-dataVencimento": {
        "pt": "Data vencimento",
		"en": "Data vencimento",
		"es": "Data vencimento"
    },    
    "l-creditos": {
        "pt": "Créditos",
		"en": "Credits",
		"es": "Créditos"
    },
    "l-pesquisar": {
        "pt": "Pesquisar",
		"en": "Pesquisar",
		"es": "Pesquisar"
    },    
    "l-pesquisarDisciplina": {
        "pt": "Pesquisar por disciplinas",
		"en": "Pesquisar por disciplinas",
		"es": "Pesquisar por disciplinas"
    },
    "l-adicionarDisciplina": {
        "pt": "Adicionar disciplina",
		"en": "Adicionar disciplina",
		"es": "Adicionar disciplina"
    },
    "l-adicionarDisciplinaExtra": {
        "pt": "Adicionar disciplina extra",
		"en": "Adicionar disciplina extra",
		"es": "Adicionar disciplina extra"
    },    
    "l-detalhesDisciplina": {
        "pt": "Detalhes da Turma/Disciplina",
		"en": "Detalhes da Turma/Disciplina",
		"es": "Detalhes da Turma/Disciplina"
    },
    "l-periodoDiscExtra": {
        "pt": "Disc. Equivalentes / Optativas / Eletivas / Extras",
		"en": "Disc. Equivalentes / Optativas / Eletivas / Extras",
		"es": "Disc. Equivalentes / Optativas / Eletivas / Extras"
    },    
    "l-excedeuCreditos": {
        "pt": "Excedeu a quantidade máxima de @ créditos",
		"en": "Excedeu a quantidade máxima de @ créditos",
		"es": "Excedeu a quantidade máxima de @ créditos"
    },
    "l-quantMinCreditos": {
        "pt": "Quantidade mínima de @ créditos não atingida",
		"en": "Quantidade mínima de @ créditos não atingida",
		"es": "Quantidade mínima de @ créditos não atingida"
    },    
    "l-maximoCredito": {
        "pt": "Máxido de créditos",
        "en": "Máxido de créditos",
        "es": "Máxido de créditos"
    },
    "l-minimoCredito": {
        "pt": "Mínimo de créditos",
        "en": "Mínimo de créditos",
        "es": "Mínimo de créditos"
    },
    "l-creditosAcademicos": {
        "pt": "Créd. acadêmicos",
		"en": "Créditos acadêmicos",
		"es": "Créditos acadêmicos"
    },
    "l-creditosFinanceiros": {
        "pt": "Créd. financeiros",
		"en": "Créditos financeiros",
		"es": "Créditos financeiros"
    },
    "l-tipoDisciplina": {
        "pt": "Tipo",
		"en": "Tipo",
		"es": "Tipo"
    },
    "l-disciplina": {
        "pt": "Disciplina",
		"en": "Disciplina",
		"es": "Disciplina"
    },    
    "l-cargaHoraria": {
        "pt": "Carga horária - CH",
		"en": "Course load - CL",
		"es": "Carga horaria - CH"
    },
    "l-ch": {
        "pt": "CH",
		"en": "CL",
		"es": "CH"
    },
    "l-requerCorequisito": {
        "pt": "Requer disciplina de corequisito.",
        "en": "Requer disciplina de corequisito.",
        "es": "Requer disciplina de corequisito."
    },
    "l-possuiChoqueHorario": {
        "pt": "Possui choque de horário.",
        "en": "Possui choque de horário.",
        "es": "Possui choque de horário."
    },
    "l-alertaPossuiChoqueHorario": {
        "pt": "Atenção ao concluir a matrícula! Possui choque de horário.",
        "en": "Atenção ao concluir a matrícula! Possui choque de horário.",
        "es": "Atenção ao concluir a matrícula! Possui choque de horário."
    },
    "l-maxCredito": {
        "pt": "Excedeu o número de créditos.",
        "en": "Excedeu o número de créditos.",
        "es": "Excedeu o número de créditos."
    },
    "l-minCredito": {
        "pt": "Número mínimo de créditos não matriculados.",
        "en": "Número mínimo de créditos não matriculados.",
        "es": "Número mínimo de créditos não matriculados."
    },
    "l-campus": {
        "pt": "Campus",
		"en": "Campus",
		"es": "Campus"
    },
    "l-matriz": {
        "pt": "Matriz",
		"en": "Matriz",
		"es": "Matriz"
    },
    "l-datas": {
        "pt": "Datas",
		"en": "Datas",
		"es": "Datas"
    },  
    "l-adicionar": {
        "pt": "Adicionar",
		"en": "Adicionar",
		"es": "Adicionar"
    },     
    "l-removerDisciplina": {
        "pt": "Remover disciplina",
		"en": "Remover disciplina",
		"es": "Remover disciplina"
    },     
    "l-ate": {
        "pt": "até",
		"en": "até",
		"es": "até"
    },
    "l-presencial": {
        "pt": "Presencial",
		"en": "Presencial",
		"es": "Presencial"
    },
    "l-semipresencial": {
        "pt": "Semipresencial",
		"en": "Semipresencial",
		"es": "Semipresencial"
    },
    "l-distancia": {
        "pt": "A distância",
		"en": "A distância",
		"es": "A distância"
    },
    "l-vagas": {
        "pt": "Vagas",
		"en": "Vagas",
		"es": "Vagas"
    },
    "l-vagasRestantes": {
        "pt": "Vagas restantes",
		"en": "Vagas restantes",
		"es": "Vagas restantes"
    },
    "l-listaEspera": {
        "pt": "Lista de espera",
		"en": "Lista de espera",
		"es": "Lista de espera"
    },
    "l-vagasListaEspera": {
        "pt": "Vagas por lista de espera",
		"en": "Vagas por lista de espera",
		"es": "Vagas por lista de espera"
    },
    "l-painelValidacoes": {
        "pt": "Painel de validações",
		"en": "Painel de validações",
		"es": "Painel de validações"
    },
    "l-disciplinaAdicionada": {
        "pt": "Disciplina adicionada",
		"en": "Disciplina adicionada",
		"es": "Disciplina adicionada"
    },
   "l-trocar": {
        "pt": "Trocar turma",
		"en": "Trocar turma",
		"es": "Trocar turma"
    },   
    "l-sugestaoDisciplina": {
        "pt": "Sugestão de disciplina",
		"en": "Sugestão de disciplina",
		"es": "Sugestão de disciplina"
    },
    "l-disciplinasMatriculadas": {
        "pt": "Disciplinas para matrícula",
		"en": "Disciplinas para matrícula",
		"es": "Disciplinas para matrícula"
    },
    "l-disciplinasDoHorario": {
        "pt": "Disciplinas do horário",
		"en": "Disciplinas do horário",
		"es": "Disciplinas do horário"
    },
    "l-erros-validacao": {
        "pt": "Erros de validação",
		"en": "Erros de validação",
		"es": "Erros de validação"
    },    
    "l-editar": {
        "pt": "Editar",
		"en": "Editar",
		"es": "Editar"
    },
    "l-maximizar": {
        "pt": "Maximizar",
		"en": "Maximizar",
		"es": "Maximizar"
    },
    "l-cod": {
        "pt": "Cód.",
		"en": "Cód.",
		"es": "Cód."
    },    
    "l-emEdicao": {
        "pt": "Em edição",
		"en": "Em edição",
		"es": "Em edição"
    },
    "l-editarQuadro": {
        "pt": "Editar quadro de horário",
		"en": "Editar quadro de horário",
		"es": "Editar quadro de horário"
    },    
    "l-minimizar": {
        "pt": "Minimizar",
		"en": "Minimizar",
		"es": "Minimizar"
    },
    "l-minimo-credito": {
        "pt": "Mínimo créditos",
		"en": "Mínimo créditos",
		"es": "Mínimo créditos"
    },
    "l-maximo-credito": {
        "pt": "Máximo créditos",
		"en": "Máximo créditos",
		"es": "Máximo créditos"
    },
    "l-quadroHorario": {
        "pt": "Quadro de horário",
		"en": "Quadro de horário",
		"es": "Quadro de horário"
    },    
    "l-possuiChoqueHorarioDisciplina": {
        "pt": "Possui choque de horário com a disciplina",
		"en": "Possui choque de horário com a disciplina",
		"es": "Possui choque de horário com a disciplina"
    },
    "l-semDisciplinaHorario": {
        "pt": "Não possui disciplinas disponíveis para este horário",
		"en": "Não possui disciplinas disponíveis para este horário",
		"es": "Não possui disciplinas disponíveis para este horário"
    },
    "l-btn-fechar": {
        "pt": "Fechar",
		"en": "Fechar",
		"es": "Fechar"
    },
    "l-segunda": {
        "pt": "Segunda",
		"en": "Segunda",
		"es": "Segunda"
    },
    "l-seg": {
        "pt": "Seg.",
		"en": "Seg.",
		"es": "Seg."
    },
    "l-terca": {
        "pt": "Terça",
		"en": "Terça",
		"es": "Terça"
    },
    "l-ter": {
        "pt": "Ter.",
		"en": "Ter.",
		"es": "Ter."
    },
    "l-quarta": {
        "pt": "Quarta",
		"en": "Quarta",
		"es": "Quarta"
    },
    "l-qua": {
        "pt": "Qua.",
		"en": "Qua.",
		"es": "Qua."
    },
    "l-quinta": {
        "pt": "Quinta",
		"en": "Quinta",
		"es": "Quinta"
    },
    "l-qui": {
        "pt": "Qui.",
		"en": "Qui.",
		"es": "Qui."
    },
    "l-sexta": {
        "pt": "Sexta",
		"en": "Sexta",
		"es": "Sexta"
    },
    "l-sex": {
        "pt": "Sex.",
		"en": "Sex.",
		"es": "Sex."
    },
    "l-sabado": {
        "pt": "Sábado",
		"en": "Sábado",
		"es": "Sábado"
    },
    "l-sab": {
        "pt": "Sáb.",
		"en": "Sáb.",
		"es": "Sáb."
    },    
    "l-domingo": {
        "pt": "Domingo",
		"en": "Domingo",
		"es": "Domingo"
    },
    "l-dom": {
        "pt": "Dom.",
		"en": "Dom.",
		"es": "Dom."
    },    
    "l-desmatricular": {
        "pt": "Desmatricular",
		"en": "Desmatricular",
		"es": "Desmatricular"
    },
    "l-matricular": {
        "pt": "Matricular",
		"en": "Matricular",
		"es": "Matricular"
    },
    "l-turma": {
        "pt": "Turma",
		"en": "Class",
		"es": "Grupo"
    },
    "l-subturma": {
        "pt": "Subturma",
		"en": "Sub-class",
		"es": "Subgrupo"
    },
    "l-professor": {
        "pt": "Professor(es)",
		"en": "Teacher(s)",
		"es": "Profesor(es)"
    },
    "l-periodo": {
        "pt": "Período",
		"en": "Period",
		"es": "Período"
    },
    "l-situacao": {
        "pt": "Situação",
		"en": "Status",
		"es": "Situación"
    },
    "l-infoHorario": {
        "pt": "Informações do horário",
		"en": "Schedule information",
		"es": "Información del horario"
    },
    "l-predio": {
        "pt": "Prédio",
		"en": "Building",
		"es": "Edificio"
    },
    "l-bloco": {
        "pt": "Bloco",
		"en": "Block",
		"es": "Block"
    },
    "l-sala": {
        "pt": "Sala",
		"en": "Room",
		"es": "Sala"
    },
    "l-dataHorario": {
        "pt": "Data do horário",
		"en": "Schedule date",
		"es": "Fecha del horario"
    },
    "l-horario": {
        "pt": "Horário",
		"en": "Schedule",
		"es": "Horario"
    },
    "l-maisInfomacoesTurmaDisc": {
        "pt": "Ver mais informações",
		"en": "See more information",
		"es": "Ver más información"
    },
    "l-codigo-disc": {
        "pt": "Código",
		"en": "Code",
		"es": "Código"
    },
    "l-filial": {
        "pt": "Filial",
		"en": "Branch",
		"es": "Sucursal"
    },
    "l-titulo-matricula-superior": {
        "pt": "Matrícula",
		"en": "Enrollment",
		"es": "Matrícula"
    },
    "l-titulo-matricula-basico": {
        "pt": "Matrícula",
		"en": "Enrollment",
		"es": "Matrícula"
    },
    "l-btn-proximo": {
        "pt": "Próximo",
		"en": "Next",
		"es": "Siguiente"
    },
    "l-btn-anterior": {
        "pt": "Anterior",
		"en": "Previous",
		"es": "Anterior"
    },
    "l-btn-cancelar": {
        "pt": "Cancelar",
		"en": "Cancel",
		"es": "Anular"
    },
    "l-btn-concluir": {
        "pt": "Concluir",
		"en": "Conclude",
		"es": "Concluir"
    },
    "l-etapa-apresentacao": {
        "pt": "Apresentação",
		"en": "Presentation",
		"es": "Presentación"
    },
    "l-etapa-selecao-periodo-letivo": {
        "pt": "Período Letivo",
		"en": "School Period",
		"es": "Período lectivo"
    },
    "l-etapa-confirmacao-dados-aluno": {
        "pt": "Confirmação Dados do Aluno",
		"en": "Student's Data Confirmation",
		"es": "Confirmación de datos del alumno"
    },
    "l-etapa-ficha-medica": {
        "pt": "Ficha Médica",
		"en": "Medical Form",
		"es": "Ficha médica"
    },
    "l-etapa-selecao-disciplinas": {
        "pt": "Disciplinas",
		"en": "Course Subjects",
		"es": "Materias"
    },
    "l-etapa-planos-pagamento": {
        "pt": "Plano de Pagamento",
		"en": "Payment Plan",
		"es": "Plan de pago"
    },
    "l-etapa-finalizacao": {
        "pt": "Finalização",
		"en": "Finalization",
		"es": "Finalización"
    },
    "l-etapa-atual": {
        "pt": "Etapa atual",
		"en": "Current state",
		"es": "Etapa actual"
    },
    "l-curso": {
        "pt": "Curso",
		"en": "Course",
		"es": "Curso"
    },
    "l-pldisponivel-cursodisponivel": {
        "pt": "Períodos letivos e Cursos disponíveis",
		"en": "School periods and courses available",
		"es": "Períodos lectivos y cursos disponibles"
    },
    "l-habilitacao-serie": {
        "pt": "Habilitação/Série",
		"en": "Specialization/Series",
		"es": "Especialidad/Serie"
    },
    "l-turno": {
        "pt": "Turno",
		"en": "Shift",
		"es": "Turno"
    },
    "l-erro-avancar-etapa": {
        "pt": "Para avançar, é necessário completar todos os itens exigidos da etapa atual!",
		"en": "To continue, complete all items required of the current stage!",
		"es": "Para avanzar, es necesario completar todos los ítems exigidos de la etapa actual."
    },
    "l-nenhuma-matricula-disciplina-disponivel": {
        "pt": "Não foram encontradas disciplinas vinculadas à este processo de matrícula.",
		"en": "No course subjects bound to this process of enrollment",
		"es": "No se encontraron materias vinculadas a este proceso de matrícula."
    },
    "l-matricula-codigo-disciplina": {
        "pt": "Cód. Disciplina",
		"en": "Course subject code",
		"es": "Cód. Materia"
    },
    "l-matricula-descricao-disciplina": {
        "pt": "Disciplina",
		"en": "Course subject",
		"es": "Materia"
    },
    "l-matricula-periodo-disciplina": {
        "pt": "Período",
		"en": "Period",
		"es": "Período"
    },
    "l-matricula-situacao-disciplina": {
        "pt": "Situação de Matrícula",
		"en": "Status of Enrollment",
		"es": "Situación de la matrícula"
    },
    "l-matricula-turma": {
        "pt": "Turma",
		"en": "Class",
		"es": "Grupo"
    },
    "l-etapa-disciplinas": {
        "pt": "Disciplinas",
		"en": "Course subjects",
		"es": "Materias"
    },
    "l-msg-necessario-aceite-contrato": {
        "pt": "Para finalizar a matrícula, é necessário ler e realizar o aceite do contrato. Após a leitura do mesmo, marque o campo \"Li e aceito os termos do contrato \".",
		"en": "To finalize enrollment, read and accept the contract. After reading it, select the field \"I read and accept the terms of the contract\".",
		"es": "Para finalizar la matrícula, es necesario leer y realizar la aceptación del contrato. Después de leerlo, marque el campo \"Leí y acepto los términos del contrato \"."
    },
    "l-aceite-contrato-matricula": {
        "pt": "Li e aceito os termos do contrato",
		"en": "I read and accept the terms of the contract",
		"es": "Leí y acepto los términos del contrato"
    },
    "l-btn-cancelar-matricula": {
        "pt": "Cancelar",
		"en": "Cancel",
		"es": "Anular"
    },
    "l-btn-finalizar-matricula": {
        "pt": "Finalizar matrícula",
		"en": "Finalize enrollment",
		"es": "Finalizar matrícula"
    },
    "l-titulo-contrato": {
        "pt": "Contrato",
		"en": "Contract",
		"es": "Contrato"
    },
    "l-numero-parcela": {
        "pt": "Número da parcela",
		"en": "Installment number",
		"es": "Número de la cuota"
    },
    "l-parcela": {
        "pt": "Parcela",
		"en": "Parcela",
		"es": "Parcela"
    },
    "l-valor": {
        "pt": "Valor",
		"en": "Valor",
		"es": "Valor"
    },    
    "l-servico-parcela": {
        "pt": "Serviço"
    },
    "l-valor-simulado": {
        "pt": "Valor simulado",
		"en": "Simulated value",
		"es": "Valor simulado"
    },
    "l-parcela-fixa": {
        "pt": "Parcela fixa?",
		"en": "Fixed installment?",
		"es": "¿Cuota fija?"
    },
    "l-titulo-comprovante": {
        "pt": "Comprovante de Matrícula",
		"en": "Receipt of Enrollment",
		"es": "Comprobante de matrícula"
    },
    "l-btn-fechar-matricula": {
        "pt": "Fechar",
		"en": "Close",
		"es": "Cerrar"
    },
    "l-matricula-eb-sucesso": {
        "pt": "Matrícula realizada com sucesso!",
		"en": "Enrollment successful!",
		"es": "¡Matrícula realizada con éxito!"
    },
    "l-matricula-eb-info adicionais": {
        "pt": "Imprima seu comprovante de matrícula atráves do link abaixo \"Comprovante de Matrícula\"",
		"en": "Print your receipt of enrollment through the link below \"Receipt of Enrollment\"",
		"es": "Imprima su comprobante de matrícula por medio del siguiente enlace \"Comprobante de matrícula\""
    },
    "l-btn-download-comprovante": {
        "pt": "Comprovante de Matrícula",
		"en": "Registration Receipt",
		"es": "Comprobante de matrícula"
    },
    "l-habilitacao": {
        "pt": "Habilitação",
		"en": "Specialization",
		"es": "Especialidad"
    },
    "l-pagamento": {
        "pt": "Pagamento",
		"en": "Payment",
		"es": "Pago"
    },
    "l-boleto": {
        "pt": "Boleto",
		"en": "Bank slip",
		"es": "Boleta"
    },
    "l-cartao": {
        "pt": "Cartão",
		"en": "Card",
		"es": "Tarjeta"        
    },
    "l-titulo-planos-pagamento": {
        "pt": "Selecione o plano de pagamento preterido:",
		"en": "Select desired payment plan",
		"es": "Seleccione el plan de pago preferido:"
    },
    "l-detalhar-parcelamento": {
        "pt": "Detalhar parcelamento",
		"en": "Detail installment",
		"es": "Detallar financiación"
    },
    "l-msg-matricula-indisponivel": {
        "pt": "Matrícula indisponível",
        "en": "Enrollment unavailable",
		"es": "Matrícula indisponible"
    },
    "l-msg-desmatricular": {
        "pt": "Para realmente cancelar a matrícula é necessário finalizar a matrícula.",
		"en": "You need to finalize enrollment in order to cancel it",
		"es": "Para realmente anular la matrícula es necesario finalizarla."
    },
    "l-msg-subturma": {
        "pt": "Favor informar a subturma a ser matriculada",
        "en": "Please inform the 'subturma' to be enrolled",
		"es": "Por favor, indique el subturma a ser matriculado"
    },
    "l-extras": {
        "pt": "Extras",
        "en": "Extras",
		"es": "Extras"
    },
    "l-creditos-academicos": {
        "pt": "Créditos acadêmicos",
        "en": "Academic credit",
		"es": "Créditos académicos"
    },
    "l-minimo": {
        "pt": "Minimo",
        "en": "Maximum",
		"es": "Mínimo"
    },
    "l-maximo": {
        "pt": "Máximo",
        "en": "Maximum",
		"es": "Máximo"
    },
    "l-creditos-selecionados": {
        "pt": "Créditos selecionados",
        "en": "Créditos selecionados",
		"es": "Créditos selecionados"
    },
    "l-cred-selecionados": {
        "pt": "Créd. selecionados",
        "en": "Créd. selecionados",
		"es": "Créd. selecionados"
    },    
    "l-disciplinas": {
        "pt": "Disciplinas",
        "en": "Disciplina",
		"es": "Disciplina"
    },
    "l-disciplinas-disponiveis-horario": {
        "pt": "Disciplinas disponíveis para o horário",
        "en": "Disciplinas disponíveis para o horário",
		"es": "Disciplinas disponíveis para o horário"
    },    
    "l-obrigatorias": {
        "pt": "Obrigatórias",
        "en": "Requireds",
        "es": "Obligatorio"
    },
    "l-optativas-eletivas": {
        "pt": "Optativas/eletivas",
        "en": "Optativas/eletivas",
        "es": "Optativas/eletivas"
    },
    "l-creditos-financeiros": {
        "pt": "Créditos financeiros",
        "en": "Financial credit",
        "es": "Credito financiero"
    },
    "l-total": {
        "pt": "Total",
        "en": "Total",
        "es": "Total"
    },
    "l-equivalentes": {
        "pt": "Equivalentes",
        "en": "Equivalentes",
        "es": "Equivalentes"
    },
    "l-msg-precorequisito-inicio": {
        "pt": "Disciplinas possuem requisitos: <br/>",
        "en": "Disciplinas possuem requisitos: <br/>",
        "es": "Disciplinas possuem requisitos: <br/>"
    },
    "l-msg-precorequisito-final": {
        "pt": "Requesitos não atendidos: <br/>",
        "en": "Requesitos não atendidos: <br/>",
        "es": "Requesitos não atendidos: <br/>"
    },
    "l-msg-erro-avancar-etapa-disciplina": {
        "pt": "Você não pode avançar a etapa da matrícula. Verifique os erros informados no Painel de validações.",
        "en": "Você não pode avançar a etapa da matrícula. Verifique os erros informados no Painel de validações.",
        "es": "Você não pode avançar a etapa da matrícula. Verifique os erros informados no Painel de validações."
    },    
    "l-msg-default-matricula-confirmada": {
        "pt": "Matrícula confirmada com sucesso",
        "en": "Matrícula confirmada com sucesso",
        "es": "Matrícula confirmada com sucesso"
    },     
    "l-semErros": {
        "pt": "Sua matrícula não possui erros.",
        "en": "Sua matrícula não possui erros.",
        "es": "Sua matrícula não possui erros."
    },   
    "l-msg-tutorial-quadro-horarios": {
        "pt": "Quadro de horários, aqui você pode adicionar ou excluir disciplinas",
        "en": "Quadro de horários, aqui você pode adicionar ou excluir disciplinas",
        "es": "Quadro de horários, aqui você pode adicionar ou excluir disciplinas"
    },   
    "l-msg-tutorial-quantidade-erros": {
        "pt": "Aqui será exibido o número de erros <br>que ocorreram na sua matrícula.<br> Clique sobre a caixa de erros para abrir o painel de validações<br>e prosseguir com o tutorial",
        "en": "Aqui será exibido o número de erros <br>que ocorreram na sua matrícula.<br> Clique sobre a caixa de erros para abrir o painel de validações<br>e prosseguir com o tutorial",
        "es": "Aqui será exibido o número de erros <br>que ocorreram na sua matrícula.<br> Clique sobre a caixa de erros para abrir o painel de validações<br>e prosseguir com o tutorial"
    },   
    "l-msg-tutorial-painel-validacoes": {
        "pt": "Caso exista algum erro na sua matrícula,<br>você pode verificar os detalhes aqui",
        "en": "Caso exista algum erro na sua matrícula,<br>você pode verificar os detalhes aqui",
        "es": "Caso exista algum erro na sua matrícula,<br>você pode verificar os detalhes aqui"
    },   
    "l-msg-tutorial-sugestao-disciplina": {
        "pt": "Aqui se encontra a lista de disciplinas<br> em que você pode se matricular",
        "en": "Aqui se encontra a lista de disciplinas<br> em que você pode se matricular",
        "es": "Aqui se encontra a lista de disciplinas<br> em que você pode se matricular"
    },   
    "l-msg-tutorial-disciplinas-sdd": {
        "pt": "Clique na disciplina, para exibir seus detalhes",
        "en": "Clique na disciplina, para exibir seus detalhes",
        "es": "Clique na disciplina, para exibir seus detalhes"
    },   
    "l-msg-tutorial-informacoes-turma-disciplina": {
        "pt": "Aqui você pode verificar<br>todos os detalhes importantes<br>sobre a disciplina selecionada,<br>por exemplo as turmas e professores",
        "en": "Aqui você pode verificar<br>todos os detalhes importantes<br>sobre a disciplina selecionada",
        "es": "Aqui você pode verificar<br>todos os detalhes importantes<br>sobre a disciplina selecionada"
    },   
    "l-msg-tutorial-disciplinas-extras": {
        "pt": "Caso queira adicionar disciplinas que não estão na sua grade,<br>utilize a opção de Adicionar disciplinas extras",
        "en": "Caso queira adicionar disciplinas que não estão na sua grade,<br>utilize a opção de Adicionar disciplinas extras",
        "es": "Caso queira adicionar disciplinas que não estão na sua grade,<br>utilize a opção de Adicionar disciplinas extras"
    },   
    "l-verTutorial": {
        "pt": "Ver o tutorial novamente",
        "en": "Ver o tutorial novamente",
        "es": "Ver o tutorial novamente"
    }
}]
