define(['aluno/matricula/matricula.module', 'aluno/matricula/matricula.factory', 'utils/edu-constantes-globais.constants'], function () {

    'use strict';

    angular
        .module('eduMatriculaModule')
        .factory('eduMatriculaService', EduMatriculaService);

    EduMatriculaService.$inject = ['$state', 'i18nFilter', '$rootScope', 'totvs.app-notification.Service', 'eduMatriculaFactory', 'eduConstantesGlobaisConsts'];

    function EduMatriculaService($state, i18nFilter, $rootScope, totvsNotification, eduMatriculaFactory, eduConstantesGlobaisConsts) {
        var self = this;

        self.inicializarWizardMatriculaEnsinoSuperiorAsync = inicializarWizardMatriculaEnsinoSuperiorAsync;
        self.inicializarWizardMatriculaEnsinoBasicoAsync = inicializarWizardMatriculaEnsinoBasicoAsync;
        self.retornarEtapaAtual = retornarEtapaAtual;
        self.avancarEtapa = avancarEtapa;
        self.retrocederEtapa = retrocederEtapa;
        self.liberarProximaEtapa = liberarProximaEtapa;
        self.liberarEtapaAtual = liberarEtapaAtual;
        self.restringirProximasEtapas = restringirProximasEtapas;
        self.verificarStatusEtapaAtualAtivo = verificarStatusEtapaAtualAtivo;
        self.verificarRedirecionamentoValido = verificarRedirecionamentoValido;
        self.redirecionar = redirecionar;

        self.retornaParametrosMatriculaOnlineAlunoAsync = retornaParametrosMatriculaOnlineAlunoAsync;
        self.retornaValidacoesPLMatriculaAlunoAsync = retornaValidacoesPLMatriculaAlunoAsync;
        self.retornaHabilitacoesPLDisponveisMatriculaAlunoAsync = retornaHabilitacoesPLDisponveisMatriculaAlunoAsync;
        self.retornaDisciplinasMatriculaAlunoAsync = retornaDisciplinasMatriculaAlunoAsync;
        self.retornaPlanosPagamentoDisponiveisAsync = retornaPlanosPagamentoDisponiveisAsync;
        self.retornaPlanoPagamentoDefaultAsync = retornaPlanoPagamentoDefaultAsync;
        self.retornaContratoMatriculaAsync = retornaContratoMatriculaAsync;
        self.retornaComprovanteMatriculaAsync = retornaComprovanteMatriculaAsync;
        self.efetuarEnsinoBasicoMatriculaAsync = efetuarEnsinoBasicoMatriculaAsync;
        self.retornaDiscplinasMatriculaESAsync = retornaDiscplinasMatriculaESAsync;
        self.retornaPesquisaDisciplinasExtrasAsync = retornaPesquisaDisciplinasExtrasAsync;
        self.retornaTurmasDisciplinasExtrasAsync = retornaTurmasDisciplinasExtrasAsync;
        self.retornaInfoTurmaDisc = retornaInfoTurmaDisc;
        self.retornaValidacoesReqDisciplinas = retornaValidacoesReqDisciplinas;
        self.executaFVSelecaoDisc = executaFVSelecaoDisc;
        self.efetuarEnsinoSuperiorMatriculaAsync = efetuarEnsinoSuperiorMatriculaAsync;
        self.permitirAlterarPlanoPagamentoAsync = permitirAlterarPlanoPagamentoAsync;

        return self;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function inicializarWizardMatriculaEnsinoSuperiorAsync(scope, eventosStateEtapas, callback) {

            carregarParametrosMatriculaAsync(function (parametrosMatricula) {

                var etapas = carregarEtapasES(parametrosMatricula);

                //Habilita evento de escuta, que verificará se a mudança de um passo do wizard é válido
                $rootScope.$on('$stateChangeStart',
                    function (event, toState) {
                        if (!verificarRedirecionamentoValido(etapas, toState.name)) {
                            event.preventDefault();
                        }
                    });

                inicializarWizard(etapas, scope, eventosStateEtapas, parametrosMatricula, callback);
            });
        }

        function inicializarWizardMatriculaEnsinoBasicoAsync(scope, eventosStateEtapas, callback) {

            carregarParametrosMatriculaAsync(function (parametrosMatricula) {

                var etapas = carregarEtapasEB(parametrosMatricula);

                //Habilita evento de escuta, que verificará se a mudança de um passo do wizard é válido
                $rootScope.$on('$stateChangeStart',
                    function (event, toState) {
                        if (!verificarRedirecionamentoValido(etapas, toState.name)) {
                            event.preventDefault();
                        }
                    });

                inicializarWizard(etapas, scope, eventosStateEtapas, parametrosMatricula, callback);
            });
        }

        function inicializarWizard(etapas, scope, eventosStateEtapas, parametrosMatricula, callback) {

            if ($state.current.name !== eduConstantesGlobaisConsts.EduStateViewPadrao) {

                verificarStatusEtapaAtualAtivo(etapas);

                registrarEscutaEventoVerificacaoEtapa(etapas, eventosStateEtapas, scope);

                if (angular.isFunction(callback)) {
                    callback(etapas, parametrosMatricula);
                }
            }
        }

        function carregarParametrosMatriculaAsync(callback) {

            eduMatriculaFactory.retornaParametrosMatriculaOnlineAlunoAsync(function (result) {

                if (result && result.MATRICPARAMS) {

                    var parametrosMatricula = {};

                    parametrosMatricula.InstrucoesMatricula = result.MATRICPARAMS[0].INSTRUCOESMATRICULA;
                    parametrosMatricula.MensagemMatriculaNaoDisponivel = result.MATRICPARAMS[0].MENSAGEMMATRICULANAODISPONIVEL;
                    parametrosMatricula.TituloMatricula = result.MATRICPARAMS[0].NOMEROTINAMATRICULA;
                    parametrosMatricula.MatriculaDiscponivel = result.MATRICPARAMS[0].MATRICDISPONIVEL;
                    parametrosMatricula.MensagemDebitosFinanc = result.MATRICPARAMS[0].MENSAGEMDEBITOSFINANC;
                    parametrosMatricula.AlterarDadosCadastraisNaMatricula = result.MATRICPARAMS[0].ALTERARDADOSCADMATRICULA;
                    parametrosMatricula.AlterarFichaMedicaNaMatricula = result.MATRICPARAMS[0].PERMITEALTERARFICHAMEDICA;
                    parametrosMatricula.ExibirCampoTurmaNaListagemDisciplinasRematricula = result.MATRICPARAMS[0].EXIBIRTURMAREMATRICULA;
                    parametrosMatricula.VisualizarPlanoPgto = result.MATRICPARAMS[0].VISUALIZARPLANOPGTO;
                    parametrosMatricula.VisualizarSimulacaoPlanoPgto = result.MATRICPARAMS[0].VISUALIZARSIMULACAOPAGTO;
                    parametrosMatricula.TituloContratoFinanceiro = result.MATRICPARAMS[0].NOMECONTRATOFINANC;
                    parametrosMatricula.ContratoDisponivelImpressao = result.MATRICPARAMS[0].CONTRATODISPONIVELIMPRESSAO;
                    parametrosMatricula.ComprovanteDisponivelImpressao = result.MATRICPARAMS[0].COMPROVANTEDISPONIVELIMPRESSAO;
                    parametrosMatricula.TituloComprovanteMatricula = result.MATRICPARAMS[0].TITULOCOMPMATRICULA;
                    parametrosMatricula.MensagemConfirmacaoMatricula = result.MATRICPARAMS[0].MENSAGEMCONFIRMACAOMATRICULA;

                    if (angular.isFunction(callback)) {
                        callback(parametrosMatricula);
                    }

                } else {
                    if (angular.isFunction(callback)) {
                        callback(null);
                    }
                }
            });
        }

        function registrarEscutaEventoVerificacaoEtapa(etapas, eventosStateEtapas, scope) {

            scope.$on('$stateChangeStart',
                function (event, toState) {

                    event.defaultPrevented = false;

                    if (!verificarRedirecionamentoValido(etapas, toState.name)) {
                        event.preventDefault();
                    }
                }
            );

            scope.$on('$stateChangeSuccess',
                function (event, toState) {

                    if (eventosStateEtapas.hasOwnProperty(toState.name)) {

                        var funcaoEventoNome = eventosStateEtapas[toState.name];

                        if (angular.isFunction(scope.controller[funcaoEventoNome])) {
                            scope.controller[funcaoEventoNome]();
                        }
                    }
                }
            );
        }

        function retornarEtapaAtual(etapas) {

            return etapas.find(function (item) {

                if (item.nome === $state.current.name) {
                    return item;
                }
            });
        }

        function retornaEtapaPorOrdem(etapas, ordem) {

            return etapas.find(function (item) {

                if (item.ordem === ordem) {
                    return item;
                }
            });
        }

        function retornaEtapaPorNome(etapas, nome) {

            return etapas.find(function (item) {

                if (item.nome === nome) {
                    return item;
                }
            });
        }

        function retornaEtapaPadrao(etapas) {

            return etapas.find(function (item) {

                if (item.padrao) {
                    return item;
                }
            });
        }

        function retornaTextoi18n(key) {
            return i18nFilter(key, [], 'js/aluno/matricula');
        }

        function avancarEtapa(etapas) {

            var etapaAtual = retornarEtapaAtual(etapas);

            if (etapaAtual.ordem < etapas.length) {

                var proximaEtapa = retornaEtapaPorOrdem(etapas, etapaAtual.ordem + 1);

                if (proximaEtapa.ativo) {
                    etapaAtual.realizado = true;
                    $state.go(proximaEtapa.nome);
                } else {
                    totvsNotification.notify({
                        type: 'error',
                        title: i18nFilter('l-Atencao'),
                        detail: retornaTextoi18n('l-erro-avancar-etapa')
                    });
                }
            }
        }

        function retrocederEtapa(etapas) {

            var etapaAtual = retornarEtapaAtual(etapas);

            if (etapaAtual.ordem > 1) {

                var proximaEtapa = retornaEtapaPorOrdem(etapas, etapaAtual.ordem - 1);

                if (proximaEtapa.ativo) {
                    $state.go(proximaEtapa.nome);
                }
            }
        }

        function liberarProximaEtapa(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);

            if (etapaAtual) {

                if (etapas.length > etapaAtual.ordem) {

                    var proximaEtapa = etapas[etapaAtual.ordem];

                    if (!proximaEtapa.ativo) {
                        proximaEtapa.ativo = true;
                        etapaAtual.realizado = true;
                    }
                }
            }
        }

        function liberarEtapaAtual(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);

            if (etapaAtual) {

                etapaAtual.ativo = true;
                etapaAtual.realizado = true;
            }
        }

        function restringirProximasEtapas(etapas) {
            var etapaAtual = retornarEtapaAtual(etapas);

            angular.forEach(etapas, function (etapa) {
                if (etapa.ordem > etapaAtual.ordem) {
                    etapa.ativo = false;
                    etapa.realizado = false;
                }
            }, self);
        }

        function verificarRedirecionamentoValido(etapas, stateRedirecionamento) {

            if (stateRedirecionamento !== eduConstantesGlobaisConsts.EduStateViewPadrao)
            {
                var stateEtapaRedirecionamento = retornaEtapaPorNome(etapas, stateRedirecionamento);
                var etapaPadrao = retornaEtapaPadrao(etapas);

                var nomeStatePadraoPaginaMatricula = etapaPadrao.nome.split('.')[0];

                if ((stateEtapaRedirecionamento && stateEtapaRedirecionamento.ativo) || (!stateEtapaRedirecionamento && stateRedirecionamento !== nomeStatePadraoPaginaMatricula)) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                return true;
            }
        }

        function redirecionar(etapas, stateRedirecionamento) {
            if (verificarRedirecionamentoValido(etapas, stateRedirecionamento)) {
                $state.go(stateRedirecionamento);
            } else {

                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: retornaTextoi18n('l-erro-avancar-etapa')
                });

            }
        }

        function verificarStatusEtapaAtualAtivo(etapas) {

            var etapaAtual = retornarEtapaAtual(etapas);
            var etapaPadrao = retornaEtapaPadrao(etapas);
            if (!etapaAtual || !etapaAtual.ativo) {
                $state.go(etapaPadrao.nome);
            }
        }

        function retornaParametrosMatriculaOnlineAlunoAsync(callback) {
            eduMatriculaFactory.retornaParametrosMatriculaOnlineAlunoAsync(callback);
        }

        function retornaValidacoesPLMatriculaAlunoAsync(codTurma, codCurso, codHabilitacao, codGrade, callback) {
            eduMatriculaFactory.retornaValidacoesPLMatriculaAlunoAsync(codTurma, codCurso, codHabilitacao, codGrade, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaHabilitacoesPLDisponveisMatriculaAlunoAsync(callback) {
            eduMatriculaFactory.retornaHabilitacoesPLDisponveisMatriculaAlunoAsync(function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaDisciplinasMatriculaAlunoAsync(codTurma, codCurso, codHabilitacao, codGrade, callback) {
            eduMatriculaFactory.retornaDisciplinasMatriculaAlunoAsync(codTurma, codCurso, codHabilitacao, codGrade, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaDiscplinasMatriculaESAsync(idPerLet, callback) {
            eduMatriculaFactory.retornaDiscplinasMatriculaESAsync(idPerLet, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaInfoTurmaDisc(idTurmaDisc, codDisc, callback) {
            eduMatriculaFactory.retornaInfoTurmaDisc(idTurmaDisc, codDisc, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaPlanosPagamentoDisponiveisAsync(idHabilitacaoFilial, idPerLet, listaIdTurmaDisc, callback) {
            eduMatriculaFactory.retornaPlanosPagamentoDisponiveisAsync(idHabilitacaoFilial, idPerLet, listaIdTurmaDisc, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaPlanoPagamentoDefaultAsync(idHabilitacaoFilial, idPerLet, callback) {
            eduMatriculaFactory.retornaPlanoPagamentoDefaultAsync(idHabilitacaoFilial, idPerLet, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function permitirAlterarPlanoPagamentoAsync(idHabilitacaoFilial, idPerLet, callback) {
            eduMatriculaFactory.permitirAlterarPlanoPagamentoAsync(idHabilitacaoFilial, idPerLet, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaContratoMatriculaAsync(idHabilitacaoFilial, idPerLet, codPlanoPagamento, listaIdTurmaDisc, callback) {
            eduMatriculaFactory.retornaContratoMatriculaAsync(idHabilitacaoFilial, idPerLet, codPlanoPagamento, listaIdTurmaDisc, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaComprovanteMatriculaAsync(idHabilitacaoFilial, idPerLet, callback) {
            eduMatriculaFactory.retornaComprovanteMatriculaAsync(idHabilitacaoFilial, idPerLet, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function efetuarEnsinoBasicoMatriculaAsync(idHabilitacaoFilial, codPlanoPagamento, idPerLet, callback) {

            eduMatriculaFactory.efetuarEnsinoBasicoMatriculaAsync(idHabilitacaoFilial, codPlanoPagamento, idPerLet, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        /**
         * Realiza matricula do ES
         * @param {any} listaDiscAlteradas Lista de disciplinas alteradas
         * @param {any} listaDiscAdd Lista de disciplinas adicionadas
         * @param {any} listaDiscRemovidas Lista de disciplinas removidas
         * @param {any} listaDiscSubstituicao Lista de troca de disciplina por substituição
         * @param {any} codPlanoPgtoSelecionado Cód plano de pagamento selecionado
         * @param {any} idPeriodoLetivo Id. do período letivo
         * @param {function} callback Callback
         */
        function efetuarEnsinoSuperiorMatriculaAsync(listaDiscAlteradas, listaDiscAdd, listaDiscRemovidas,
            listaDiscSubstituicao, codPlanoPgtoSelecionado, idPeriodoLetivo, callback) {
            
            eduMatriculaFactory.executarMatriculaES(listaDiscAlteradas, listaDiscAdd, listaDiscRemovidas,
                listaDiscSubstituicao, codPlanoPgtoSelecionado, idPeriodoLetivo, function (result) {
                    if (result) {
                        if (angular.isFunction(callback)) {
                            callback(result);
                        }
                    }
            });
        }

        function retornaPesquisaDisciplinasExtrasAsync(idPerLet, filtroDisciplina, codDiscSDD, callback) {

            eduMatriculaFactory.retornaPesquisaDisciplinasExtrasAsync(idPerLet, filtroDisciplina, codDiscSDD, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function retornaTurmasDisciplinasExtrasAsync(idPerLet, codDisc, callback) {

            eduMatriculaFactory.retornaTurmasDisciplinasExtrasAsync(idPerLet, codDisc, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function carregarEtapasEB(parametrosMatricula) {
            var etapas = [];

            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaEB.apresentacao',
                descricao: retornaTextoi18n('l-etapa-apresentacao'),
                ativo: true,
                realizado: false,
                padrao: true
            });
            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaEB.periodo-letivo',
                descricao: retornaTextoi18n('l-etapa-selecao-periodo-letivo'),
                ativo: false,
                realizado: false
            });

            if (parametrosMatricula.AlterarDadosCadastraisNaMatricula === true) {
                etapas.push({
                    ordem: etapas.length + 1,
                    nome: 'matriculaEB.dados-pessoais',
                    descricao: retornaTextoi18n('l-etapa-confirmacao-dados-aluno'),
                    ativo: false,
                    realizado: false
                });
            }

            if (parametrosMatricula.AlterarFichaMedicaNaMatricula === true) {
                etapas.push({
                    ordem: etapas.length + 1,
                    nome: 'matriculaEB.ficha-medica',
                    descricao: retornaTextoi18n('l-etapa-ficha-medica'),
                    ativo: false,
                    realizado: false
                });
            }

            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaEB.disciplinas',
                descricao: retornaTextoi18n('l-etapa-disciplinas'),
                ativo: false,
                realizado: false
            });

            if (parametrosMatricula.VisualizarPlanoPgto === true) {
                etapas.push({
                    ordem: etapas.length + 1,
                    nome: 'matriculaEB.planos-pagamento',
                    descricao: retornaTextoi18n('l-etapa-planos-pagamento'),
                    ativo: false,
                    realizado: false
                });
            }

            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaEB.finalizacao',
                descricao: retornaTextoi18n('l-etapa-finalizacao'),
                ativa: false,
                realizado: false
            });

            return etapas;
        }

        function carregarEtapasES(parametrosMatricula) {

            var etapas = [];

            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaES.apresentacao',
                descricao: retornaTextoi18n('l-etapa-apresentacao'),
                ativo: true,
                realizado: false,
                padrao: true
            });
            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaES.periodo-letivo',
                descricao: retornaTextoi18n('l-etapa-selecao-periodo-letivo'),
                ativo: false,
                realizado: false
            });
            if (parametrosMatricula.AlterarDadosCadastraisNaMatricula === true) {
                etapas.push({
                    ordem: etapas.length + 1,
                    nome: 'matriculaES.dados-pessoais',
                    descricao: retornaTextoi18n('l-etapa-confirmacao-dados-aluno'),
                    ativo: false,
                    realizado: false
                });
            }
            if (parametrosMatricula.AlterarFichaMedicaNaMatricula === true) {
                etapas.push({
                    ordem: etapas.length + 1,
                    nome: 'matriculaES.ficha-medica',
                    descricao: retornaTextoi18n('l-etapa-ficha-medica'),
                    ativo: false,
                    realizado: false
                });
            }
            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaES.disciplinas',
                descricao: retornaTextoi18n('l-etapa-selecao-disciplinas'),
                ativo: false,
                realizado: false
            });
            if (parametrosMatricula.VisualizarPlanoPgto === true) {
                etapas.push({
                    ordem: etapas.length + 1,
                    nome: 'matriculaES.planos-pagamento',
                    descricao: retornaTextoi18n('l-etapa-planos-pagamento'),
                    ativo: false,
                    realizado: false
                });
            }
            etapas.push({
                ordem: etapas.length + 1,
                nome: 'matriculaES.finalizacao',
                descricao: retornaTextoi18n('l-etapa-finalizacao'),
                ativa: false,
                realizado: false
            });
            return etapas;
        }

        function retornaValidacoesReqDisciplinas(matricItensList, matricItensListDel, callback) {
            eduMatriculaFactory.retornaValidacoesReqDisciplinas(matricItensList, matricItensListDel, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        function executaFVSelecaoDisc(matricItensList, codcoligada, codtipocurso, codfilial, callback) {
            eduMatriculaFactory.executaFVSelecaoDisc(matricItensList, codcoligada, codtipocurso, codfilial, function (result) {
                if (result) {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }
    }
});
