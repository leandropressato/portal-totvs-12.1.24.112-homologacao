define(['aluno/matricula/matricula.module',
        'aluno/matricula/matricula.service',
        'aluno/financeiro/financeiro.service',
        'aluno/financeiro/financeiro.factory',
        'aluno/financeiro/lancamentos/lancamentos-pagcartao.controller',
        'utils/edu-constantes-globais.constants'], function () {

    'use strict';

    angular
        .module('eduMatriculaModule')
        .controller('EduMatriculaEBController', EduMatriculaEBController);

    EduMatriculaEBController.$inject = ['$state',
                                        'i18nFilter',
                                        '$rootScope',
                                        'eduMatriculaService',
                                        '$scope',
                                        '$sce',
                                        'totvs.app-notification.Service',
                                        '$filter',
                                        'EduFinanceiroService',
                                        'EduFinanceiroFactory',
                                        'eduUtilsFactory',
                                        'eduConstantesGlobaisConsts'];

    function EduMatriculaEBController($state,
                                        i18nFilter,
                                        $rootScope,
                                        eduMatriculaService,
                                        $scope,
                                        $sce,
                                        totvsNotification,
                                        $filter,
                                        eduFinanceiroService,
                                        eduFinanceiroFactory,
                                        eduUtilsFactory,
                                        eduConstantesGlobaisConsts) {

        var self = this,
            parametrosEducacional = null;

        self.etapas = [];
        self.parametrosMatricula = {};
        self.instrucoesMatriculaHTML = '';
        self.tituloContratoFinanceiro = '';
        self.tituloComprovanteMatricula = '';
        self.MensagemMatriculaNaoDisponivelHTML = '';
        self.textoContratoFinanceiro = '';
        self.textoComprovante = '';
        self.tituloColunaHabilitacaoSerie = i18nFilter('l-serie');

        self.htmlParaImpressao = '';

        self.habilitacoesPLDisponiveis = [];
        self.habilitacaoSelecionada = null;
        self.validacoesPLSelecionado = [];
        self.disciplinasHabilitacaoSelecionada = [];
        self.planosPagamentoDisponiveis = [];
        self.listaIdTurmaDisc = [];
        self.planoPagamentoSelecionado = null;
        self.AceiteContrato = false;
        self.boleto = null;
        self.processoFinalizado = false;
        self.permiteAlterarPlanoPagamento = true;

        self.retornarEtapaAtual = retornarEtapaAtual;
        self.avancarEtapa = avancarEtapa;
        self.retrocederEtapa = retrocederEtapa;
        self.redirecionar = redirecionar;
        self.realizarMatricula = realizarMatricula;
        self.emitirBoleto = emitirBoleto;
        self.exibirOpcoesPagamento = exibirOpcoesPagamento;
        self.exibeBtnPagamentoCartao = exibeBtnPagamentoCartao;
        self.exibeBtnPagamentoBoleto = exibeBtnPagamentoBoleto;
        self.exibirDadosPagCartao = exibirDadosPagCartao;

        self.HabilitacaoPLSelecionado = HabilitacaoPLSelecionado;

        self.exibirModalContratoMatricula = exibirModalContratoMatricula;
        self.exibirModalComprovante = exibirModalComprovante;

        self.aoIniciarEtapaApresentacao = aoIniciarEtapaApresentacao;
        self.aoIniciarEtapaPeriodoLetivo = aoIniciarEtapaPeriodoLetivo;
        self.aoIniciarEtapaDisciplinas = aoIniciarEtapaDisciplinas;
        self.aoIniciarEtapaPlanosPagamento = aoIniciarEtapaPlanosPagamento;
        self.aoIniciarEtapaFinalizacao = aoIniciarEtapaFinalizacao;
        self.formataMoeda = formataMoeda;

        self.mensagemConfirmacaoMatricula = i18nFilter('l-matricula-eb-sucesso', '[]', 'js/aluno/matricula');
        self.contratoDisponivelImpressao = false;
        self.comprovanteDisponivelImpressao = false;

        //Devem ser registrados todas as funções de inicialização, quando uma etapa do wizard é acionada
        self.eventosStateEtapas = {
            'matriculaEB.apresentacao': 'aoIniciarEtapaApresentacao',
            'matriculaEB.periodo-letivo': 'aoIniciarEtapaPeriodoLetivo',
            'matriculaEB.disciplinas': 'aoIniciarEtapaDisciplinas',
            'matriculaEB.planos-pagamento': 'aoIniciarEtapaPlanosPagamento',
            'matriculaEB.finalizacao': 'aoIniciarEtapaFinalizacao'
        };

        init();

        function init() {

            carregarParametrosEducacional();

            self.tituloContratoFinanceiro = i18nFilter('l-titulo-contrato', '[]', 'js/aluno/matricula');
            self.tituloComprovanteMatricula = i18nFilter('l-titulo-comprovante', '[]', 'js/aluno/matricula');

            eduMatriculaService.inicializarWizardMatriculaEnsinoBasicoAsync($scope, self.eventosStateEtapas, function (etapas, parametrosMatricula) {

                self.etapas = etapas;

                self.parametrosMatricula = parametrosMatricula;

                self.instrucoesMatriculaHTML = $sce.trustAsHtml(parametrosMatricula.InstrucoesMatricula);

                self.MensagemMatriculaNaoDisponivelHTML = $sce.trustAsHtml(parametrosMatricula.MensagemMatriculaNaoDisponivel);

                if (parametrosMatricula.TituloContratoFinanceiro) {
                    self.tituloContratoFinanceiro = parametrosMatricula.TituloContratoFinanceiro;
                }

                if (parametrosMatricula.TituloComprovanteMatricula) {
                    self.tituloComprovanteMatricula = parametrosMatricula.TituloComprovanteMatricula;
                }
            });

        }

        /**
         * Valida se o usuário tem permissão no Menu
         */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_MATRICULAEB) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        function carregarParametrosEducacional () {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (paramEdu) {
                parametrosEducacional = paramEdu;
            });
        }

        function retornarEtapaAtual() {
            return eduMatriculaService.retornarEtapaAtual(self.etapas);
        }

        function avancarEtapa() {

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaEB.apresentacao') {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaEB.periodo-letivo') {
                var bloquearMatricula = false;
                self.validacoesPLSelecionado.forEach(function (item) {
                    if (item.bloquearMatricula) {
                        bloquearMatricula = true;
                    }
                }, this);

                if (!bloquearMatricula && !self.habilitacaoSelecionada) {
                    bloquearMatricula = true;
                }

                if (!bloquearMatricula) {
                    eduMatriculaService.liberarProximaEtapa(self.etapas);
                }
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaEB.dados-pessoais') {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaEB.ficha-medica') {
                eduMatriculaService.liberarProximaEtapa(self.etapas);
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaEB.disciplinas') {

                if ((self.parametrosMatricula.ContratoDisponivelImpressao) && (self.parametrosMatricula.VisualizarPlanoPgto)) {
                    eduMatriculaService.liberarProximaEtapa(self.etapas);
                } else {
                    executarContratoMatricula();
                    return;
                }
            }

            if (eduMatriculaService.retornarEtapaAtual(self.etapas).nome === 'matriculaEB.planos-pagamento') {
                if ((self.planoPagamentoSelecionado && self.planoPagamentoSelecionado !== '0') || !self.permiteAlterarPlanoPagamento) {
                    executarContratoMatricula();

                    return;
                }
            }

            eduMatriculaService.avancarEtapa(self.etapas);
        }

        function retrocederEtapa() {
            eduMatriculaService.retrocederEtapa(self.etapas);
        }

        function redirecionar(state) {
            if (!self.processoFinalizado) {
                eduMatriculaService.redirecionar(self.etapas, state);
            }
        }

        //Evento disparado quando é iniciallizado a etapa de apresentação
        function aoIniciarEtapaApresentacao() {

        }

        //Evento disparado quando é iniciallizado a etapa de seleção de período letivo
        function aoIniciarEtapaPeriodoLetivo() {

            if (self.habilitacoesPLDisponiveis.length === 0) {

                eduMatriculaService.retornaHabilitacoesPLDisponveisMatriculaAlunoAsync(function (data) {
                    self.habilitacoesPLDisponiveis = data.HABILITACAOPLDISPONIVEIS;

                    //Se não tiver habilitação disponível mostra mensagem de matrícula indisponível
                    if (self.habilitacoesPLDisponiveis.length === 0) {
                        self.parametrosMatricula.MatriculaDiscponivel = false;
                    }

                    //Se tiver apenas um registro de habilitações exibidas para o usuário, já seleciona automaticamente e realiza as validações.
                    if (self.habilitacoesPLDisponiveis.length === 1) {
                        self.habilitacaoSelecionada = self.habilitacoesPLDisponiveis[0];
                        HabilitacaoPLSelecionado(self.habilitacaoSelecionada);
                    }
                });
            }

            var myWatch = $scope.$watch('controller.habilitacaoSelecionada', function (newValue, oldValue) {
                // Se o usuário trocar o período letivo durante a navegação da tela (anterior/próximo), então será
                // necessário reiniciar as etapas posteriores, para evitar que o usuário avance, com as regras
                // do período letivo selecionado anteriormente.
                if (newValue && oldValue && newValue.IDHABILITACAOFILIAL !== oldValue.IDHABILITACAOFILIAL) {

                    eduMatriculaService.restringirProximasEtapas(self.etapas);

                    // Limpa a lista de planos de pagamento disponíveis, pois ela precisará ser
                    // carregada novamente, devido a alteração do período letivo selecionado.
                    // Carga realizada na function aoIniciarEtapaPlanosPagamento
                    self.planosPagamentoDisponiveis = [];

                    myWatch();
                }
            });
        }

        //* Função chamada quando um período letivo é selecionado pelo usuário *//
        function HabilitacaoPLSelecionado(plSelecionado) {
            if (plSelecionado == []) {
                return;
            }

            eduMatriculaService.retornaValidacoesPLMatriculaAlunoAsync(plSelecionado.IDHABILITACAOFILIAL, plSelecionado.IDPERLET,
                function (data) {
                    self.validacoesPLSelecionado = data;
                });

            // Carrega o período letivo selecionado para matrícula.
            // Esse campo será utilizado na rotina de documentos exigidos, da etapa "Confirmação Dados do Aluno", na matrícula.
            $rootScope.plSelecionadoMatricula = plSelecionado;
        }

        //Evento disparado quando é iniciallizado a etapa de seleção de disciplinas
        function aoIniciarEtapaDisciplinas() {

            if (self.habilitacaoSelecionada) {

                eduMatriculaService.retornaDisciplinasMatriculaAlunoAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET,
                    function (data) {

                        self.disciplinasHabilitacaoSelecionada = data;

                        //Caso não esteja parametrizado no sistema, será ocultado o campo TURMA da listagm de disciplinas
                        if (!self.parametrosMatricula.ExibirCampoTurmaNaListagemDisciplinasRematricula) {
                            self.gridInstance.hideColumn(4);
                        }
                    });
            } else {
                self.disciplinasHabilitacaoSelecionada = [];
            }
        }

        function aoIniciarEtapaPlanosPagamento() {
            if (self.planosPagamentoDisponiveis.length === 0) {
                preparaListaIdTurmaDisc(function() {
                    eduMatriculaService.retornaPlanosPagamentoDisponiveisAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET, self.listaIdTurmaDisc, function (data) {
                        self.planosPagamentoDisponiveis = data;
                    });
    
                    eduMatriculaService.retornaPlanoPagamentoDefaultAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET, function (data) {
                        eduMatriculaService.permitirAlterarPlanoPagamentoAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET,
                            function(permite) {
                                self.permiteAlterarPlanoPagamento = permite.value;
                                self.planoPagamentoSelecionado = data.CODPLANOPGTO;

                                if (!permite.value && data.CODPLANOPGTO === "0")
                                self.planoPagamentoSelecionado = '';
                            });
                    });
                });               
            }
        }

        function preparaListaIdTurmaDisc(callback) {
            self.listaIdTurmaDisc = [];
            self.disciplinasHabilitacaoSelecionada.forEach(function(disciplina) {
                self.listaIdTurmaDisc.push(disciplina.IDTURMADISC);
            });

            if (callback) {
                callback();
            }
        }

        function aoIniciarEtapaFinalizacao() {

            //Etapa de finallização será realizada automaticamente
            eduMatriculaService.liberarEtapaAtual(self.etapas);

            if (!self.textoComprovante) {
                eduMatriculaService.retornaComprovanteMatriculaAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.habilitacaoSelecionada.IDPERLET,
                    function (result) {

                        if (result && result.value) {
                            self.textoComprovante = $sce.trustAsHtml(result.value);
                        }
                    });
            }
        }

        function executarContratoMatricula() {

            if (!eduMatriculaService.retornarEtapaAtual(self.etapas).realizado) {
                if (self.parametrosMatricula.ContratoDisponivelImpressao) {
                    preparaListaIdTurmaDisc(function() {
                        eduMatriculaService.retornaContratoMatriculaAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL,
                            self.habilitacaoSelecionada.IDPERLET, self.planoPagamentoSelecionado, self.listaIdTurmaDisc,
                            function (result) {

                            if (result.Contrato === null) {
                                result.Contrato = '';
                            }

                            if (result.Contrato || result.Contrato === '') {
                                self.textoContratoFinanceiro = $sce.trustAsHtml(result.Contrato);
                                exibirModalContratoMatricula(true);
                            }
                        });
                    });
                } else {
                    self.AceiteContrato = true;
                    realizarMatricula();
                }
            } else {
                eduMatriculaService.avancarEtapa(self.etapas);
            }
        }

        function exibirModalContratoMatricula(exibir) {
            if (exibir) {
                self.htmlParaImpressao = self.textoContratoFinanceiro;
                $('#modalContratoMatricula').modal('show');
            } else {
                $('#modalContratoMatricula').modal('hide');
            }
        }

        function exibirModalComprovante(exibir) {
            if (exibir) {
                self.htmlParaImpressao = self.textoComprovante;
                $('#modalComprovanteMatricula').modal('show');
            } else {
                $('#modalComprovanteMatricula').modal('hide');
            }
        }

        /**
         * Executar processo de confirmação de matrícula EB.
         *
         */
        function realizarMatricula() {

            if (self.AceiteContrato) {
                exibirModalContratoMatricula(false);
                eduMatriculaService.efetuarEnsinoBasicoMatriculaAsync(self.habilitacaoSelecionada.IDHABILITACAOFILIAL, self.planoPagamentoSelecionado, self.habilitacaoSelecionada.IDPERLET, function (result) {
                    if (result.$messages[0].type === 'success') {

                        if (self.parametrosMatricula.MensagemConfirmacaoMatricula) {
                            self.mensagemConfirmacaoMatricula = self.parametrosMatricula.MensagemConfirmacaoMatricula;
                        }

                        self.contratoDisponivelImpressao = self.parametrosMatricula.ContratoDisponivelImpressao;
                        self.comprovanteDisponivelImpressao = self.parametrosMatricula.ComprovanteDisponivelImpressao;

                        eduMatriculaService.liberarProximaEtapa(self.etapas);
                        eduMatriculaService.avancarEtapa(self.etapas);

                        self.processoFinalizado = true;

                        if (result['Lancamentos'].data && result['Lancamentos'].data.BOLETOS.SBoletos.length > 0) {
                            self.boleto = result['Lancamentos'].data.BOLETOS.SBoletos[0];
                        }
                    }
                });
            } else {

                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-necessario-aceite-contrato', '[]', 'js/aluno/matricula')
                });

            }
        }

        // Função utilizada para formatar o campo 'Valor simulado' do grid de detalhamento das parcelas.
        function formataMoeda(item) {
            return $filter('currency')(item.valor);
        }

        /**
         * Exibir opção de pagamento na confirmação da matrícula
         *
         * @returns {Boolean}
         */
        function exibirOpcoesPagamento() {
            return self.boleto !== null && (exibeBtnPagamentoBoleto() || exibeBtnPagamentoCartao());
        }

        /**
         *
         * Realizar o pagamento via cartão.
         */
        function exibirDadosPagCartao() {
            eduFinanceiroFactory.exibirDadosPagCartao(self.boleto.IDBOLETO);
        }

        /**
         * Exibe opção de pagamento via cartão
         *
         * @returns {Boolean}
         */
        function exibeBtnPagamentoCartao () {
            return eduFinanceiroService.permitePagamentoCartao(self.boleto, parametrosEducacional);
        }

        /**
         * Exibe opção de pagamento via impressão do boleto
         *
         * @returns
         */
        function exibeBtnPagamentoBoleto() {
            return eduFinanceiroService.permitePagamentoBoleto(self.boleto, parametrosEducacional);
        }

        /**
         * Emite boleto
         */
        function emitirBoleto() {

            if (self.boleto) {
                eduFinanceiroService.visualizarBoleto(self.boleto.IDBOLETO, self.boleto.NOSSONUMERO, self.boleto.DATAVENCIMENTO, parametrosEducacional, function (objInfoBoleto) {
                    self.objInfoBoleto = objInfoBoleto;
                });
            }

        }
    }
});
