define(['aluno/requerimentos/requerimentos.module',
    'aluno/requerimentos/requerimentos.factory'
], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .controller('eduRequerimentosDisponiveisController', EduRequerimentosDisponiveisController);

    EduRequerimentosDisponiveisController.$inject = ['$filter', 'eduRequerimentosFactory', '$state', '$sce'];

    function EduRequerimentosDisponiveisController($filter, eduRequerimentosFactory, $state, $sce) {

        var self = this;

        self.mensagem = 'TODO: Disponíveis';
        self.model = [];
        self.modelAllRecords = [];
        self.grupoAtendimento = -1;
        self.requerimento = {};

        self.grupoAtendimentoAlterado = grupoAtendimentoAlterado;
        self.getTemplateTipoRequerimento = getTemplateTipoRequerimento;
        self.getTemplateDetalheRequerimento = getTemplateDetalheRequerimento;
        self.redirecionarTelaNovaSolicitacao = redirecionarTelaNovaSolicitacao;
        self.abrirModalDetalheRequerimento = abrirModalDetalheRequerimento;
        self.closeModalAndRedirect = closeModalAndRedirect;

        init();

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function init() {

            buscarRequerimentosDisponiveisAsync();
        }

        function buscarRequerimentosDisponiveisAsync() {

            self.model = [];
            eduRequerimentosFactory.getOpcoesAtendimentoAsync(function (result) {

                if (result && result.HTIPOATENDIMENTO) {

                    self.model = result.HTIPOATENDIMENTO;
                    self.modelAllRecords = result.HTIPOATENDIMENTO;
                }
            });
        }

        function grupoAtendimentoAlterado() {
            if (self.grupoAtendimento === -1) {
                self.model = self.modelAllRecords;
            } else {
                self.model = $filter('filter')(self.modelAllRecords,
                                { 'CODGRUPOATENDIMENTO': self.grupoAtendimento },
                                true);
            }
        }

        function getTemplateTipoRequerimento(dataItem) {

            var template = '';

            template += '<a ng-click="controller.redirecionarTelaNovaSolicitacao(' + dataItem.CODGRUPOATENDIMENTO + ',' + dataItem.CODTIPOATENDIMENTO + ')" ';
            template += 'tooltip-append-to-body=\'appendToBody=true\' data-tooltip="';
            template += dataItem.NOMETIPO + '" tooltip-placement="left">';
            template += dataItem.NOMETIPO;
            template += '</a>';

            return template;
        }

        function getTemplateDetalheRequerimento(dataItem) {

            var template = '';

            template += '<a href="" ng-click="controller.abrirModalDetalheRequerimento(' + dataItem.CODGRUPOATENDIMENTO + ', ' + dataItem.CODTIPOATENDIMENTO + ')">';
            template += filtroI18n('l-ver-detalhes');
            template += '</a>';

            return template;
        }

        function abrirModalDetalheRequerimento(codGrupoAtendimento, codTipoAtendimento) {
            self.requerimento = $filter('filter')(self.modelAllRecords, { CODGRUPOATENDIMENTO: codGrupoAtendimento, CODTIPOATENDIMENTO: codTipoAtendimento }, true)[0];
            // indica que o html é válido
            if (typeof (self.requerimento.DESCRICAO) !== 'object') {
                self.requerimento.DESCRICAO = $sce.trustAsHtml(self.requerimento.DESCRICAO);
            }
            if (typeof (self.requerimento.PROCEDIMENTOS) !== 'object') {
                self.requerimento.PROCEDIMENTOS = $sce.trustAsHtml(self.requerimento.PROCEDIMENTOS);
            }

            $('#mReqInfo').modal('show');
        }

        function redirecionarTelaNovaSolicitacao(codGrupoAtendimento, codTipoAtendimento) {

            var params = {
                codGrupoAtendimento: codGrupoAtendimento,
                codTipoAtendimento: codTipoAtendimento
            };

            $state.go('requerimentos.edit', params);
        }

        function filtroI18n(nomeCampo) {
            return $filter('i18n')(nomeCampo, [], 'js/aluno/requerimentos');
        }

        function closeModalAndRedirect(codGrupoAtendimento, codTipoAtendimento) {

            $('#mReqInfo').on('hidden.bs.modal', function () {
                redirecionarTelaNovaSolicitacao(codGrupoAtendimento, codTipoAtendimento);
            });
        }
    }
});
