define(['aluno/requerimentos/requerimentos.module',
    'aluno/requerimentos/requerimentos.factory',
    'aluno/requerimentos/requerimentos.service',
    'utils/edu-enums.constants',
    'utils/edu-utils.service'
], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .controller('eduRequerimentosSolicitadosController', EduRequerimentosSolicitadosController);

    EduRequerimentosSolicitadosController.$inject = [
        'eduRequerimentosFactory',
        'eduRequerimentosService',
        'eduEnumsConsts',
        'totvs.app-notification.Service',
        'i18nFilter',
        '$filter',
        '$state',
        'eduUtilsService',
        '$sce',
        '$scope',
        '$timeout',
        '$rootScope'
    ];

    function EduRequerimentosSolicitadosController(eduRequerimentosFactory,
                                                   eduRequerimentosService,
                                                   EduEnumsConsts, 
                                                   totvsNotification, 
                                                   i18nFilter, 
                                                   $filter, 
                                                   $state, 
                                                   eduUtilsService, 
                                                   $sce, 
                                                   $scope, 
                                                   $timeout, 
                                                   $rootScope) {

        var self = this;

        //Model do atendimento selecionado para executar ação na tela
        self.model = {};

        //filtro prontocolo atendimento
        self.protocoloFiltro = '';

        self.listaRequerimentos = [];
        self.limitAt = 20; // limite de resultados para paginação
        self.objContexto = {};
        self.periodoLetivo = 0;
        self.intRequerimentosTotal = 0;
        self.listaRequerimentos = [];

        self.Cancelamento = {};
        self.Discussao = {};
        self.Arquivos = {};
        self.anexoForm = {};

        self.getRequerimentos = getRequerimentos;
        self.search = search;
        self.getCssClassStatus = getCssClassStatus;
        self.preencherDetalhesAtendimento = preencherDetalhesAtendimento;
        self.tratamentoQuebraLinhasCampoString = tratamentoQuebraLinhasCampoString;
        self.onclickDownload = onclickDownload;
        self.verificaStatusPermiteAcao = verificaStatusPermiteAcao;
        self.verificaStatusPermiteAcao = verificaStatusPermiteAcao;
        self.registrarCancelamentoRequerimento = registrarCancelamentoRequerimento;
        self.registrarDiscussaoRequerimento = registrarDiscussaoRequerimento;
        self.registrarAnexoRequerimento = registrarAnexoRequerimento;
        self.filtrarRequerimentos = filtrarRequerimentos;

        self.abrirModalCancelamento = abrirModalCancelamento;
        self.abrirModalUploadArquivos = abrirModalUploadArquivos;
        self.abrirModalDiscussao = abrirModalDiscussao;
        self.limparFiltroProtocolo = limparFiltroProtocolo;

        //Watch que irá fazer um delay ao buscar protocolo na lista de requerimentos, para evitar travamentos na busca
        var tempFilterText = '',
            filterTextTimeout;
        $scope.$watch('controller.searchText', function (val) {

            if (filterTextTimeout) {
                $timeout.cancel(filterTextTimeout);
            }

            tempFilterText = val;
            filterTextTimeout = $timeout(function () {
                self.protocoloFiltro = tempFilterText;
                if (self.protocoloFiltro) {
                    selecionarDestacarRequerimento();
                }
            }, 250);
        });

        init();

        function init() {

            // configura permissão do botão "Outras opções"
            self.exibeBtnOutrasOpcoes = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_SECRETARIA_REQUERIMENTOS_ALTERACOES_ACOMPANHAMENTO);
            
            // configura permissão do botão "Cancelar Requerimento"
            self.exibeBtnCancelarRequerimento = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_SECRETARIA_REQUERIMENTOS_CANCELAR_REQUERIMENTO);

            self.search(false, function () {
                //Caso exista um filtro por protolo, presente na URL
                //Utilizado apenas quando é gerado um novo requerimento e redirecionado para a tela
                if ($state.params.protocolo) {
                    self.searchText = $state.params.protocolo;
                }
            });

        }

        function search(isMore, callback) {
            var startAt = 0;

            if (isMore) {
                startAt = self.listaRequerimentos.length;
            } else {
                self.listaRequerimentos = [];
            }

            self.getRequerimentos(startAt, callback);
        }

        function atualizarListagem(callback) {

            self.limitAt = self.listaRequerimentos.length;
            self.startAt = 0;
            self.listaRequerimentos = [];

            self.getRequerimentos(self.startAt, callback);
        }

        function getRequerimentos(startAt, callback) {

            eduRequerimentosFactory.getAtendimentosAsync(startAt, self.limitAt, function (result) {

                if (result) {

                    angular.forEach(result, function (value) {

                        self.listaRequerimentos.push(value);
                    });

                    self.intRequerimentosTotal = self.listaRequerimentos.length;

                    if (angular.isFunction(callback)) {
                        callback();
                    }
                }
            });
        }

        function preencherDetalhesAtendimento(atendimento) {

            if (!angular.isDefined(atendimento.Detalhes)) {

                getDetalheAtendimento(atendimento.CODLOCAL, atendimento.CODATENDIMENTO, function (result) {

                    if (result) {
                        atendimento.Detalhes = result;
                        getParametrosTipoAtendimento(atendimento.CODTIPOATENDIMENTO, function (resultadoTipoAtendimento) {
                            if (resultadoTipoAtendimento && angular.isDefined(atendimento.Detalhes)) {
                                atendimento.Detalhes.TipoAtendimento = resultadoTipoAtendimento;
                                tratamentoValorCamposDinamicos(atendimento.Detalhes);
                            }
                        });
                    }
                });
            }
        }

        function getDetalheAtendimento(codLocal, codAtendimento, callback) {
            eduRequerimentosFactory.getDetalhesAtendimentoAsync(codLocal, codAtendimento, callback);
        }

        function getParametrosTipoAtendimento(codTipoAtendimento, callback) {
            eduRequerimentosFactory.getOpcaoAtendimentoDetalheAsync(codTipoAtendimento, callback);
        }

        function tratamentoValorCamposDinamicos(detalhesAtendimento) {

            var valorCampoDinamicoPorCodigo = [];
            if (angular.isDefined(detalhesAtendimento.HParamAtendimento)) {

                for (var nomeCampo in detalhesAtendimento.HParamAtendimento[0]) {

                    if (nomeCampo.indexOf('PARAMETRO') >= 0) {
                        var codigoCampo = nomeCampo.split('_')[1];
                        valorCampoDinamicoPorCodigo[codigoCampo] = detalhesAtendimento.HParamAtendimento[0][nomeCampo];
                    }
                }
            }

            if (valorCampoDinamicoPorCodigo.length > 0 && angular.isDefined(detalhesAtendimento.TipoAtendimento.LISTPARAMETROSOLICITACAO)) {

                for (var i = 0; i < detalhesAtendimento.TipoAtendimento.LISTPARAMETROSOLICITACAO.length; i++) {

                    var item = detalhesAtendimento.TipoAtendimento.LISTPARAMETROSOLICITACAO[i];
                    if (!item.CodSentenca) {
                        // se o parâmetro for date, converte um formato mais amigavel
                        if (item.TipoParametro === 3) {
                            item.Valor = $filter('date')(valorCampoDinamicoPorCodigo[item.CodParametro], 'shortDate', 'UTC');
                        } else {
                            item.Valor = valorCampoDinamicoPorCodigo[item.CodParametro];
                        }
                    }
                    else {
                        //Tratamento de campos que são resultados de uma consulta
                        var valorSentenca = valorCampoDinamicoPorCodigo[item.CodParametro],
                            camposSentenca = [];

                        if (detalhesAtendimento.TipoAtendimento.CAMPOSPARAM[item.CodSentenca] && detalhesAtendimento.TipoAtendimento.CAMPOSPARAM[item.CodSentenca].length > 0) {
                            for (var itemCampoSentenca in detalhesAtendimento.TipoAtendimento.CAMPOSPARAM[item.CodSentenca][0]) {

                                if (angular.isString(itemCampoSentenca)) {
                                    camposSentenca.push(itemCampoSentenca);
                                }
                            }
                            camposSentenca.reverse();
                        }

                        for (var j = 0; j < detalhesAtendimento.TipoAtendimento.CAMPOSPARAM[item.CodSentenca].length; j++) {
                            var itemSentenca = detalhesAtendimento.TipoAtendimento.CAMPOSPARAM[item.CodSentenca][j];

                            if (itemSentenca[camposSentenca[0]].toString() === valorSentenca) {
                                valorSentenca = itemSentenca[camposSentenca[1]];
                            }
                        }

                        item.Valor = valorSentenca;
                    }
                }
            }
        }

        function getCssClassStatus(codStatus) {

            var cssClass = '';

            switch (codStatus) {
                //concluido
                case 'F':
                case 'U':
                    cssClass = 'status-requerimento-concluido';
                    break;
                //cancelado
                case 'C':
                    cssClass = 'status-requerimento-cancelado';
                    break;
                //em andamento
                default://Status (A,D,E,G,O,R,T,V)
                    cssClass = 'status-requerimento-em-andamento';
                    break;
            }

            return cssClass;
        }

        function tratamentoQuebraLinhasCampoString(texto) {
            if (texto) {
                return $sce.trustAsHtml(eduUtilsService.tratarQuebrasDeLinhaStringParaHTML(texto));
            }
            else {
                return '';
            }

        }

        function onclickDownload(nomeArquivo, arquivoBytes) {
            eduUtilsService.abrirJanelaDownloadArquivo(nomeArquivo, arquivoBytes);
        }

        function verificaStatusPermiteAcao(codStatus) {
            var retorno = false;

            switch (codStatus) {
                //concluido
                case 'F':
                case 'U':
                //cancelado
                case 'C':
                    retorno = false;
                    break;
                //em andamento
                default://Status (A,D,E,G,O,R,T,V)
                    retorno = true;
                    break;
            }

            return retorno;
        }

        function cancelarRequerimento() {
            totvsNotification.question({
                title: i18nFilter('l-confirma-cancelamento-requerimento', [], 'js/aluno/requerimentos'),
                text: i18nFilter('l-msg-confirmacao-cancelamento-requerimento', [], 'js/aluno/requerimentos'),
                cancelLabel: 'l-no',
                confirmLabel: 'l-yes',
                callback: function (isPositiveResult) {

                    if (isPositiveResult) {

                        eduRequerimentosFactory.cancelarAtendimentoAsync(self.model.codLocal, self.model.codAtendimento, self.Cancelamento.Justificativa, function () {

                            $('#modalCancelamento').modal('hide');

                            self.searchText = '';
                            atualizarListagem(function () {
                                self.searchText = self.model.codAtendimento;
                            });

                        });
                    }
                }
            });
        }

        function incluirDiscussao() {
            eduRequerimentosFactory.incluirDiscussaoAtendimentoAsync(self.model.codLocal, self.model.codAtendimento, self.Discussao.TextoDiscussao, function () {

                $('#modalDiscussao').modal('hide');

                self.searchText = '';
                atualizarListagem(function () {
                    self.searchText = self.model.codAtendimento;
                });
            });
        }

        function registrarDiscussaoRequerimento() {

            if (self.discussaoForm.$invalid) {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-discussao', '[]', 'js/aluno/requerimentos')
                });
            } else {
                incluirDiscussao();
            }
        }

        function registrarCancelamentoRequerimento() {

            if (self.cancelamentoForm.$invalid) {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-justificativa', '[]', 'js/aluno/requerimentos')
                });
            } else {
                cancelarRequerimento();
            }
        }

        function registrarAnexoRequerimento() {

            if (self.anexoForm.$invalid) {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-descricao-arquivo', '[]', 'js/aluno/requerimentos')
                });
            } else if (angular.isUndefined(self.Arquivos.File) || !(self.Arquivos.File)) {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-arquivo', '[]', 'js/aluno/requerimentos')
                });
            }
            else {
                eduRequerimentosService.incluirAnexo(self.model.codLocal, self.model.codAtendimento, self.Arquivos, function (result) {
                    if (result.SUCESSO) {
                        $('#modalArquivo').modal('hide');
                        getDetalheAtendimento(self.model.codLocal, self.model.codAtendimento, function (detalhes) {
                            self.listaRequerimentos.find(function (item) {
                                if (item.CODATENDIMENTO === self.model.codAtendimento && item.CODLOCAL === self.model.codLocal) {
                                    item.Detalhes = detalhes;
                                }
                            });
                        });
                    }
                });
            }
        }

        function abrirModalCancelamento(codLocal, codAtendimento) {
            self.model = { codLocal: codLocal, codAtendimento: codAtendimento };
            self.Cancelamento.Justificativa = '';
            $('#modalCancelamento').modal('show');
        }

        function abrirModalUploadArquivos(codLocal, codAtendimento) {
            self.model = { codLocal: codLocal, codAtendimento: codAtendimento };
            self.Arquivos.File = null;
            self.Arquivos.Descricao = '';
            limparCampoFileUpload();
            $('#modalArquivo').modal('show');
        }

        function abrirModalDiscussao(codLocal, codAtendimento) {
            self.model = { codLocal: codLocal, codAtendimento: codAtendimento };
            self.Discussao.TextoDiscussao = '';
            $('#modalDiscussao').modal('show');
        }

        function limparCampoFileUpload() {
            var element = document.getElementsByName('campoFile');
            $(element).val('');

            element = document.getElementsByName('campoFile_fake');
            $(element).val('');
        }

        function filtrarRequerimentos(item) {

            var itemValido = true;

            if (self.grupoAtendimentoFiltro !== -1 && self.grupoAtendimentoFiltro !== item.CODGRUPOATENDIMENTO) {

                return false;
            }

            if (self.protocoloFiltro && Number(self.protocoloFiltro) !== item.CODATENDIMENTO) {

                return false;
            }

            return itemValido;
        }

        function selecionarDestacarRequerimento() {

            var itemExpandido = $('.item-details.open');

            if (itemExpandido.length === 0) {

                if (angular.isDefined($('.info-more a')[0])) {
                    $timeout(function () {$('.info-more a')[0].click();}, 100);
                }
            }
        }

        /**
         * Limpa filtro por número do Protocolo.
         */
        function limparFiltroProtocolo () {
            if (self.searchText) {
                self.searchText = '';
            }
        }
    }
});
