define(['aluno/requerimentos/requerimentos.module'], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .factory('eduRequerimentosFactory', EduRequerimentosFactory);

    EduRequerimentosFactory.$inject = ['$totvsresource'];

    function EduRequerimentosFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/Solicitacao/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.getOpcoesAtendimentoAsync = getOpcoesAtendimentoAsync;
        factory.getOpcaoAtendimentoDetalheAsync = getOpcaoAtendimentoDetalheAsync;
        factory.gerarSolicitacaoAsync = gerarSolicitacaoAsync;
        factory.getAtendimentosAsync = getAtendimentosAsync;
        factory.getDetalhesAtendimentoAsync = getDetalhesAtendimentoAsync;
        factory.cancelarAtendimentoAsync = cancelarAtendimentoAsync;
        factory.incluirDiscussaoAtendimentoAsync = incluirDiscussaoAtendimentoAsync;
        factory.incluirAnexoAtendimentoAsync = incluirAnexoAtendimentoAsync;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function getOpcoesAtendimentoAsync(callback) {

            var parameters = {
                method: 'OpcoesAtendimento',
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getOpcaoAtendimentoDetalheAsync(codTipoAtendimento, callback) {

            var parameters = {
                method: 'DetalhesAtendimento',
                codTipoAtendimento: codTipoAtendimento
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function gerarSolicitacaoAsync(listaParametros, codGrupoAtendimento, codTipoAtendimento, justificativa, codServico, codLocalEntrega, valorEntrega, nomeTipoAtendimento, callback) {

            var parameters = {
                ListParametroSolicitacao: listaParametros,
                codTipoAtendimento: codTipoAtendimento,
                codGrupoAtendimento: codGrupoAtendimento,
                justificativa: justificativa,
                codServico: codServico,
                codLocalEntrega: codLocalEntrega,
                valorEntrega: valorEntrega,
                nomeTipoAtendimento: nomeTipoAtendimento
            };

            return factory.TOTVSSave({ method: 'GerarSolicitacaoAtendimento' }, parameters, callback);
        }

        function getAtendimentosAsync(startRecord, maxRecord, callback) {

            var parameters = {
                method: 'AcompanhamentoAtendimentos',
                startRecord: startRecord,
                maxRecord: maxRecord
            };

            return factory.TOTVSQuery(parameters, callback);
        }

        function getDetalhesAtendimentoAsync(codLocal, codAtendimento, callback) {

            var parameters = {
                method: 'TodasInformacoesAtendimento',
                codLocal: codLocal,
                codAtendimento: codAtendimento
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function cancelarAtendimentoAsync(codLocal, codAtendimento, justificativa, callback) {

            var parameters = {
                method: 'CancelarAtendimento',
                codAtendimento: codAtendimento,
                codLocal: codLocal
            },
                model = {
                    justificativa: justificativa
                };

            return factory.TOTVSSave(parameters, model, callback);
        }

        function incluirDiscussaoAtendimentoAsync(codLocal, codAtendimento, textoDiscussao, callback) {

            var parameters = {
                method: 'InserirDiscussao',
                codAtendimento: codAtendimento,
                codLocal: codLocal
            },
                model = {
                    discussao: textoDiscussao
                };

            return factory.TOTVSSave(parameters, model, callback);
        }

        function incluirAnexoAtendimentoAsync(codLocal, codAtendimento, arquivo, nome, detalhe, callback) {

            var parameters = {
                method: 'SalvarAnexosAtendimento',
                codAtendimento: codAtendimento,
                codLocal: codLocal
            },
                model = {
                    Arquivo: arquivo,
                    Nome: nome,
                    Detalhe: detalhe
                };

            return factory.TOTVSSave(parameters, [model], callback);
        }
    }
});
