/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.22
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module requerimentosModule
 * @name requerimentosAvaliacoesService
 * @object service
 *
 * @created 2018-09-06 v12.1.22
 * @updated
 *
 * @requires $modal
 *
 * @description Service utilizada para exibir anexar documentos em requerimentos
 */

define(['aluno/requerimentos/requerimentos.module',
        'aluno/requerimentos/requerimentos.factory'], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .service('eduRequerimentosService', EduRequerimentosService);


    EduRequerimentosService.$inject = ['eduRequerimentosFactory'];

    function EduRequerimentosService(eduRequerimentosFactory) {

        this.incluirAnexo = incluirAnexo;

        function incluirAnexo(codLocal, codAtendimento, arquivo, callback) {

            if (arquivo.File) {
                var reader = new FileReader(),
                    file = arquivo.File;

                reader.onload = function (e) {
                    var arrayBinario = e.target.result.split(',')[1];

                    eduRequerimentosFactory.incluirAnexoAtendimentoAsync(codLocal,
                        codAtendimento,
                        arrayBinario,
                        file.name,
                        arquivo.Descricao, function (result) {
                            if (angular.isFunction(callback)) {
                                callback(result);
                            }
                    });
                };
                
                reader.readAsDataURL(file);
            }            
        }

    }
});
