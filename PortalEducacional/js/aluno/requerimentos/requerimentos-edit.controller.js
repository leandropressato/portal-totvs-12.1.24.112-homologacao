define(['aluno/requerimentos/requerimentos.module',
    'aluno/requerimentos/requerimentos.factory',
    'aluno/requerimentos/requerimentos.service',
    'utils/edu-constantes-globais.constants'
], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .controller('eduRequerimentosEditController', EduRequerimentosEditController);

    EduRequerimentosEditController.$inject = [
        '$rootScope',
        '$state',
        'eduRequerimentosFactory',
        'eduRequerimentosService',
        'eduConstantesGlobaisConsts',
        '$compile',
        '$scope',
        'totvs.app-notification.Service',
        'i18nFilter',
        '$sce',
        '$window',
        'TotvsDesktopContextoCursoFactory'];

    function EduRequerimentosEditController($rootScope, 
                                            $state, 
                                            eduRequerimentosFactory, 
                                            eduRequerimentosService,
                                            eduConstantesGlobaisConsts, 
                                            $compile, 
                                            $scope,
                                            totvsNotification, 
                                            i18nFilter, 
                                            $sce, 
                                            $window, 
                                            TotvsDesktopContextoCursoFactory) {

        var self = this;

        self.model = {};
        self.Arquivos = {};
        self.anexoForm = {};

        self.model.RequerimentoSolicitacao = {};
        self.model.RequerimentoSolicitacao.VALORTOTAL = 0.00;
        self.possuiDescDetalhada = true;
        self.onChange = onChange;
        self.criarCamposDinamicos = criarCamposDinamicos;
        self.getModelCampo = getModelCampo;
        self.registrarRequerimento = registrarRequerimento;
        self.exibirDescricaoDetalhada = exibirDescricaoDetalhada;
        self.getTemplateCheckbox = getTemplateCheckbox;
        self.verificaRowSelecionada = verificaRowSelecionada;
        self.getTemplateColunaGeral = getTemplateColunaGeral;
        self.periodoLetivoAlterado = periodoLetivoAlterado;
        self.registrarAnexoRequerimento = registrarAnexoRequerimento;
        self.abrirModalUploadArquivos = abrirModalUploadArquivos;
        self.limparCampoFileUpload = limparCampoFileUpload;                                        

        $scope.linhaSelecionada = null;
        self.ra = null;
        self.nomeAluno = null;
        init();

        function getModelCampo(i) {

            return self.model.ListCamposDinamicos[i];
        }

        function criarCamposDinamicos() {

            if (angular.isArray(self.model.ListCamposDinamicos)) {

                var containerDiv = document.getElementById('camposDinamicosContainer');
                angular.element(containerDiv).empty();

                for (var i = 0; i < self.model.ListCamposDinamicos.length; i++) {

                    var template = retornaDiretivaTOTVSPorTipoCampo(self.model.ListCamposDinamicos[i], i, self.model),
                        diretivaTotvs = $compile(template);

                    diretivaTotvs = diretivaTotvs($scope);

                    angular.element(containerDiv).append(diretivaTotvs);
                }
            }
        }

        function init() {

            if ($state.params.codTipoAtendimento) {

                buscarDadosAluno();
                buscarDadosAtendimentoRequerimentoAsync();
            }
            else {
                $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
            }
           
        }

        function buscarDadosAluno()
        {
            var contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
            self.ra = contexto.cursoSelecionado.RA;
            self.nomeAluno = contexto.cursoSelecionado.NOMEALUNO;
        }

        function getTemplateCheckbox(dataItem) {
            return '<span aria-selected="true" ng-class="controller.verificaRowSelecionada(' + dataItem.CODLOCAL + ',' + dataItem.CODLOCALENTREGA + ') ?\'glyphicon glyphicon-check\': \'glyphicon glyphicon-unchecked\'" aria-hidden="true"></span>';
        }

        function getTemplateColunaGeral(coluna) {
            return ('<span tooltip-append-to-body=\'appendToBody=true\' data-tooltip="#:data["' + coluna + '"]#" tooltip-placement="left">' +
                '#:data["' + coluna + '"]# </span>');
        }

        function verificaRowSelecionada (codLocal, codLocalEntrega) {
            var estaSelecionada = false;

            if ($scope.linhaSelecionada) {

                if ($scope.linhaSelecionada.CODLOCAL === codLocal && $scope.linhaSelecionada.CODLOCALENTREGA === codLocalEntrega) {
                    estaSelecionada = true;
                }
            }

            return estaSelecionada;
        }

        function buscarDadosAtendimentoRequerimentoAsync() {

            self.model = {};
            self.model.RequerimentoSolicitacao = {};

            eduRequerimentosFactory.getOpcaoAtendimentoDetalheAsync($state.params.codTipoAtendimento, function (result) {
                if (result) {

                    var valorEntrega = null;

                    if (result.HTIPOATENDIMENTO && result.HTIPOATENDIMENTO.length > 0) {
                        self.model.Requerimento = result.HTIPOATENDIMENTO[0];

                        if (self.model.Requerimento.ACAOTEMPLATE === 1) {
                            self.model.Requerimento.VALOR = 0;
                        }

                        // se não tem uma descrição detalhada, oculta o botão
                        self.possuiDescDetalhada = Boolean(self.model.Requerimento.DESCRICAODETALHADA);
                        // indica que o html é válido
                        self.model.Requerimento.DESCRICAO = $sce.trustAsHtml(self.model.Requerimento.DESCRICAO);
                        self.model.Requerimento.PROCEDIMENTOS = $sce.trustAsHtml(self.model.Requerimento.PROCEDIMENTOS);
                        self.model.Requerimento.ACEITE = $sce.trustAsHtml(self.model.Requerimento.ACEITE);
                        self.model.Requerimento.DESCRICAODETALHADA = $sce.trustAsHtml(self.model.Requerimento.DESCRICAODETALHADA);
                    }

                    if (result.HLOCALENTREGA) {
                        for(var i = 0; i <  result.HLOCALENTREGA.length; i++){
                            result.HLOCALENTREGA[i]['VALORFORMATADO'] = result.HLOCALENTREGA[i].VALOR.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}); 
                        }
                        self.model.ListaLocaisEntrega = result.HLOCALENTREGA;
                        if (self.model.ListaLocaisEntrega.length === 1) {
                            // seleciona o único item da lista
                            $scope.linhaSelecionada = self.model.ListaLocaisEntrega[0];
                            self.selectedRow = $scope.linhaSelecionada;
                            valorEntrega = $scope.linhaSelecionada.VALOR;
                        }
                    }

                    if (result.LISTPARAMETROSOLICITACAO) {
                        self.model.ListCamposDinamicos = result.LISTPARAMETROSOLICITACAO;
                    }

                    if (result.OUTRASINFOS) {
                        self.model.OutrasInformacoes = result.OUTRASINFOS;
                        self.model.OBSERVACAO = result.OUTRASINFOS.TEXTOSOLICITACAO;
                        verificaFluxoDeTarefasTipoAtendimento(self.model.OutrasInformacoes.CODPRIMEIRATAREFA);
                    }

                    if (result.CAMPOSPARAM) {
                        self.model.ConteudoParametrosSelect = result.CAMPOSPARAM;
                    }

                    calcularCustoTotal(valorEntrega);
                    criarCamposDinamicos();

                }
            });
        }

        function calcularCustoTotal(valorEntrega) {

            if (valorEntrega !== null) {
                if (self.model.Requerimento.ACAOTEMPLATE === 1) {
                    self.model.RequerimentoSolicitacao.VALORTOTAL = valorEntrega;
                }
                else {
                    self.model.RequerimentoSolicitacao.VALORTOTAL = valorEntrega + self.model.Requerimento.VALOR;
                }
            }
        }

        function onChange() {

            var selectedItem = self.grid.dataItem(self.grid.select());
            $scope.linhaSelecionada = selectedItem;
            calcularCustoTotal(selectedItem.VALOR);
        }

        function retornaDiretivaTOTVSPorTipoCampo(item, posicaoItemArray, model) {

            var diretiva = '';

            //campo input select
            if (item.CodSentenca) {
                diretiva = getTOTVSFieldTemplateSelect(item, posicaoItemArray, model);
            }
            else {
                switch (item.TipoParametro) {

                    case 0://campo input números inteiros
                        diretiva = getTOTVSFieldTemplateNumber(item, posicaoItemArray);
                        break;
                    case 1://campo input números decimais
                        diretiva = getTOTVSFieldTemplateDecimal(item, posicaoItemArray);
                        break;
                    case 2://campo input para alfanuméricos
                    case 6://campo input url e email
                        diretiva = getTOTVSFieldTemplateInput(item, posicaoItemArray);
                        break;
                    case 3://campo input para datas
                        diretiva = getTOTVSFieldTemplateDate(item, posicaoItemArray);
                        break;
                    case 4://campo input do tipo área de texto
                        diretiva = getTOTVSFieldTemplateTextArea(item, posicaoItemArray);
                        break;
                    case 5://campo checkbox
                        diretiva = getTOTVSFieldTemplateCheckBox(item, posicaoItemArray);
                        break;
                }
            }

            return diretiva;
        }

        function getTOTVSFieldTemplateCheckBox(itemCampo, posicaoItemArray) {

            var template = '';

            template += '<field ';
            template += ' type="checkbox"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';
            template += ' >';
            template += '</field>';

            return template;
        }

        function getTOTVSFieldTemplateDate(itemCampo, posicaoItemArray) {

            var template = '';

            template += '<totvs-field';
            template += ' name="$NAME_CAMPO"';
            template += ' totvs-datepicker';
            template += ' t-mask-date=""';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' canclean';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ').Required"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';
            template += ' >';
            template += ' <validator key="required">{{ ::"l-msg-campo-obrigatorio" | i18n : [] : "js/aluno/requerimentos" }}</validator>';
            template += '</totvs-field>';

            template = template.replace('$NAME_CAMPO', 'campo_' + itemCampo.CodParametro);

            return template;
        }

        function getTOTVSFieldTemplateInput(itemCampo, posicaoItemArray) {

            var template = '';

            template += '<totvs-field';
            template += ' name="$NAME_CAMPO"';
            template += ' totvs-input';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ').Required"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';
            template += ' maxlength="{{controller.getModelCampo(' + posicaoItemArray + ').Tamanho}}"';
            template += ' >';
            template += ' <validator key="required">{{ ::"l-msg-campo-obrigatorio" | i18n : [] : "js/aluno/requerimentos" }}</validator>';
            template += '</totvs-field>';

            template = template.replace('$NAME_CAMPO', 'campo_' + itemCampo.CodParametro);

            return template;
        }

        function getTOTVSFieldTemplateNumber(itemCampo, posicaoItemArray) {

            var template = '';

            template += '<totvs-field';
            template += ' totvs-number';
            template += ' name="$NAME_CAMPO"';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ').Required"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';
            template += ' >';
            template += ' <validator key="required">{{ ::"l-msg-campo-obrigatorio" | i18n : [] : "js/aluno/requerimentos" }}</validator>';
            template += '</totvs-field>';

            template = template.replace('$NAME_CAMPO', 'campo_' + itemCampo.CodParametro);

            return template;
        }

        function getTOTVSFieldTemplateDecimal(itemCampo, posicaoItemArray) {

            var template = '';

            template += '<totvs-field';
            template += ' totvs-decimal';
            template += ' name="$NAME_CAMPO"';
            template += ' canclean';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' m-dec="2" a-dec=","  a-sep="." ';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ').Required"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';
            template += ' >';
            template += ' <validator key="required">{{ ::"l-msg-campo-obrigatorio" | i18n : [] : "js/aluno/requerimentos" }}</validator>';
            template += '</totvs-field>';

            template = template.replace('$NAME_CAMPO', 'campo_' + itemCampo.CodParametro);

            return template;
        }

        function getTOTVSFieldTemplateTextArea(itemCampo, posicaoItemArray) {

            var template = '';

            template += '<field';
            template += ' name="$NAME_CAMPO"';
            template += ' type="textarea"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12"';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ').Required"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';
            template += ' >';
            template += ' <validator key="required">{{ ::"l-msg-campo-obrigatorio" | i18n : [] : "js/aluno/requerimentos" }}</validator>';
            template += '</field>';

            template = template.replace('$NAME_CAMPO', 'campo_' + itemCampo.CodParametro);

            return template;
        }

        function getTOTVSFieldTemplateSelect(itemCampo, posicaoItemArray, model) {

            var template = '';

            template += '<field';
            template += ' type="combo"';
            template += ' name="$NAME_CAMPO"';
            template += ' class="col-lg-12 col-md-12 col-sm-12 col-xs-12 requerimento-field"';
            template += ' style="padding:0 5px 0 5px;"';
            template += ' canclean';
            template += ' ng-hide="controller.getModelCampo(' + posicaoItemArray + ').Invisivel"';
            template += ' ng-model="controller.getModelCampo(' + posicaoItemArray + ').Valor"';
            template += ' ng-required="controller.getModelCampo(' + posicaoItemArray + ').Required"';
            template += ' label="{{controller.getModelCampo(' + posicaoItemArray + ').NomeDisplay}}"';

            var arrayResultSentenca = model.ConteudoParametrosSelect[itemCampo.CodSentenca];
            if (arrayResultSentenca && angular.isArray(arrayResultSentenca) && arrayResultSentenca.length > 0) {

                var camposSelect = [];

                for (var item in arrayResultSentenca[0]) {

                    if (angular.isString(item)) {
                        camposSelect.push(item);
                    }
                }

                camposSelect.reverse();

                if (camposSelect.length >= 2) {
                    template += ' ng-options="{1} as {2} for item in {3}"';
                    template = template.replaceAllSplitAndJoin('{1}', ' item.' + camposSelect[0]);
                    template = template.replaceAllSplitAndJoin('{2}', ' item.' + camposSelect[1]);
                    template = template.replaceAllSplitAndJoin('{3}', 'controller.model.ConteudoParametrosSelect[\'' + itemCampo.CodSentenca + '\'] ');
                }
                //else adicionado, para pegar campos complementares que tem apenas um parametro.
                else if (camposSelect.length === 1){
                    template += ' ng-options="{1} as {2} for item in {3}"';
                    template = template.replaceAllSplitAndJoin('{1}', ' item.' + camposSelect[0]);
                    template = template.replaceAllSplitAndJoin('{2}', ' item.' + camposSelect[0]);
                    template = template.replaceAllSplitAndJoin('{3}', 'controller.model.ConteudoParametrosSelect[\'' + itemCampo.CodSentenca + '\'] ');
                }

            }

            template += '>';
            template += ' <validator key="required">{{ ::"l-msg-campo-obrigatorio" | i18n : [] : "js/aluno/requerimentos" }}</validator>';
            template += '</field>';

            template = template.replace('$NAME_CAMPO', 'campo_' + itemCampo.CodParametro);

            return template;

        }

        function registrarRequerimento() {

            var validacaoOK = validarFormulario();

            if (validacaoOK) {
                var codLocalEntrega = null,
                    valorEntrega = null;

                if ($scope.linhaSelecionada) {
                    codLocalEntrega = $scope.linhaSelecionada.CODLOCALENTREGA;
                    valorEntrega = $scope.linhaSelecionada.VALOR;
                }

                totvsNotification.question({
                    title: i18nFilter('l-confirma-requerimento', [], 'js/aluno/requerimentos'),
                    text: i18nFilter('l-msg-confirmacao-requerimento', [], 'js/aluno/requerimentos'),
                    size: 'md', // sm = small | md = medium | lg = larger
                    cancelLabel: 'l-no',
                    confirmLabel: 'l-yes',
                    callback: function (isPositiveResult) {

                        if (isPositiveResult) {

                            eduRequerimentosFactory.gerarSolicitacaoAsync(
                                self.model.ListCamposDinamicos,
                                $state.params.codGrupoAtendimento,
                                $state.params.codTipoAtendimento,
                                self.model.OBSERVACAO,
                                self.model.Requerimento.CODSERVICO,
                                codLocalEntrega,
                                valorEntrega,
                                self.model.Requerimento.NOMETIPO,
                                function (result) {

                                    if (!angular.isUndefined(self.Arquivos.File)) {
                                        eduRequerimentosService.incluirAnexo(result.CODLOCAL, result.CODATENDIMENTO, self.Arquivos);
                                    }
                                    
                                    if (result) {

                                        if (result.IDBOLETO) {
                                            redirecionarParaTelaExtratoFinanceiro(result.IDBOLETO, result.CODATENDIMENTO);
                                        }
                                        else if (result.SUCESSO) {
                                            registrarMensagemOperacaoEfetuadaComSucesso(result.CODATENDIMENTO);
                                            $state.go('requerimentos.start', { protocolo: result.CODATENDIMENTO});
                                        }
                                        else {
                                            restauraValorOriginalCamposDinamicos();
                                        }
                                    }
                                });
                        }
                    }
                });
            }
        }

        function registrarMensagemOperacaoEfetuadaComSucesso (codRegistroAtendimento) {

            totvsNotification.notify({
                type: 'success',
                title: i18nFilter('l-Sucesso'),
                detail: i18nFilter('l-msg-operacao-sucesso', '[]', 'js/aluno/requerimentos') + codRegistroAtendimento
            });
        }

        function redirecionarParaTelaExtratoFinanceiro (IDBOLETO, codRegistroAtendimento) {

            var mensagem = i18nFilter('l-msg-operacao-sucesso', '[]', 'js/aluno/requerimentos') +
                           codRegistroAtendimento + '. ' + i18nFilter('l-msg-redirecionamento-financeiro', [], 'js/aluno/requerimentos');

            totvsNotification.question({
                title: i18nFilter('l-titulo=msg-redirecionamento-financeiro', [], 'js/aluno/requerimentos'),
                text: mensagem,
                cancelLabel: 'l-no',
                confirmLabel: 'l-yes',
                callback: function (isPositiveResult) {

                    if (isPositiveResult) {
                        $state.go('financeiro.start', { boleto: IDBOLETO });
                    }
                    else {
                        $state.go('requerimentos.start', { protocolo: codRegistroAtendimento});
                    }
                }
            });
        }

        function validarFormulario() {

            if ($scope.linhaSelecionada === null && self.model.ListaLocaisEntrega.length > 0) {

                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-seleciona-local-entrega', '[]', 'js/aluno/requerimentos')
                });

                return false;
            }

            if (self.formulario.$valid && validarCamposDinamicos()) {
                return true;
            }
            else {

                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-campos-preenchimento-obrigatorio')
                });

                return false;
            }
        }

        function validarCamposDinamicos() {

            var validacao = true;

            if (angular.isDefined(self.model.ListCamposDinamicos) && angular.isArray(self.model.ListCamposDinamicos)) {

                for (var i = 0; i < self.model.ListCamposDinamicos.length; i++) {

                    //Caso seja um campo checkbox, tranforma o resultado em campo inteiro
                    if (self.model.ListCamposDinamicos[i].TipoParametro === 5) {

                        if (self.model.ListCamposDinamicos[i].Valor) {
                            self.model.ListCamposDinamicos[i].Valor = 1;
                        }
                        else {
                            self.model.ListCamposDinamicos[i].Valor = 0;
                        }
                    }

                    //Caso o campo dinâmico seja obrigatório, valida se o valor está indefinido, nulo ou vazio.
                    if (self.model.ListCamposDinamicos[i].Required && isUndefinedOrNullOrEmpty(self.model.ListCamposDinamicos[i].Valor)) {
                        validacao = false;
                    } else {
                        // caso seja um campo date, converte para datetime, pois vem como timespan
                        if (self.model.ListCamposDinamicos[i].TipoParametro === 3) {
                            self.model.ListCamposDinamicos[i].Valor = new Date(self.model.ListCamposDinamicos[i].Valor);
                        }
                    }
                }
            }

            return validacao;
        }

        function restauraValorOriginalCamposDinamicos () {
            if (angular.isDefined(self.model.ListCamposDinamicos) && angular.isArray(self.model.ListCamposDinamicos)) {

                for (var i = 0; i < self.model.ListCamposDinamicos.length; i++) {
                    //Caso seja um campo checkbox, tranforma o resultado em campo boolean
                    if (self.model.ListCamposDinamicos[i].TipoParametro === 5) {

                        if (self.model.ListCamposDinamicos[i].Valor) {
                            self.model.ListCamposDinamicos[i].Valor = true;
                        }
                        else {
                            self.model.ListCamposDinamicos[i].Valor = false;
                        }
                    }
                }
            }
        }

        function exibirDescricaoDetalhada() {
            $('#mReqInfo').modal('show');
        }

        function verificaFluxoDeTarefasTipoAtendimento (codPrimeiraTarefa) {

            if (codPrimeiraTarefa <= 0) {

                totvsNotification.notify({
                    type: 'warning',
                    title: i18nFilter('l-Atencao'),
                    detail: i18nFilter('l-msg-tipo-atendimento-sem-fluxo-def', '[]', 'js/aluno/requerimentos')
                });

                $window.history.back();
            }
        }

        function periodoLetivoAlterado() {
            $scope.$broadcast('alteraPeriodoLetivoEvent');
        }

        //Verifica se o objeto está undefined, null ou vazio
        function isUndefinedOrNullOrEmpty(objeto) {
            return (!angular.isDefined(objeto) || objeto === null || objeto === '');
        }

        // Assina o evento de alteração de período
        $scope.$on('alteraPeriodoLetivoEvent', function () {
            self.refresh = true;
            buscarDadosAtendimentoRequerimentoAsync();
        });

        function registrarAnexoRequerimento() {

            if (self.anexoForm.$invalid) {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-descricao-arquivo', '[]', 'js/aluno/requerimentos')
                });
            } else if (angular.isUndefined(self.Arquivos.File) || !(self.Arquivos.File)) {
                totvsNotification.notify({
                    type: 'error',
                    title: i18nFilter('l-Erro'),
                    detail: i18nFilter('l-msg-campo-obrigatorio-arquivo', '[]', 'js/aluno/requerimentos')
                });
            }
            else {
                $('#modalArquivo').modal('hide');
                arquivoAnexadoButton();
            }
        }

        function arquivoAnexadoButton(){
            var buttonAnexo = $('#buttonAnexo');
            var buttonSpanAnexo = $("#spanbuttonAnexo");
            buttonSpanAnexo.removeClass("glyphicon-paperclip");
            buttonSpanAnexo.addClass("glyphicon-ok");
            $('#buttonAnexo label').html(i18nFilter('l-arquivo-anexado', '[]', 'js/aluno/requerimentos'));
            buttonAnexo.addClass("hoverbuttonactive");
        }

        function anexarArquivoButton(){
            var buttonAnexo = $('#buttonAnexo');
            var buttonSpanAnexo = $("#spanbuttonAnexo");
            buttonSpanAnexo.removeClass("glyphicon-ok");
            buttonSpanAnexo.addClass("glyphicon-paperclip");
            $('#buttonAnexo label').html(i18nFilter('l-adicionar-anexo', '[]', 'js/aluno/requerimentos'));
            buttonAnexo.removeClass("hoverbuttonactive");
        }

        function abrirModalUploadArquivos() {
            $('#modalArquivo').modal({backdrop: 'static', keyboard: false, show: true});
        }

        function limparCampoFileUpload() {
            $('[data-dismiss=modal]').on('click', function (e) {
                var $t = $(this),
                    target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];
                
              $(target)
                .find("input,textarea,select")
                   .val('')
                   .end()
                .find("input[type=checkbox], input[type=radio]")
                   .prop("checked", "")
                   .end();
            });
            anexarArquivoButton();
        }
    }
});
