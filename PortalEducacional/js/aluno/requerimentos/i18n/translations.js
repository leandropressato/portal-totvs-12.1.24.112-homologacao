[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-adicionar-anexo": {
            "pt": "Adicionar anexo",
            "en": "Add attachment",
            "es": "Añadir adjunto"
        },  
        "l-arquivo-anexado": {
            "pt": "Arquivo anexado",
            "en": "Attached file",
            "es": "Archivo adjunto"
        },
        "l-requerimentos-disponiveis": {
            "pt": "Requerimentos disponíveis",
			"en": "Requirements available",
			"es": "Requerimientos disponibles"
        },
        "l-todos-grupos-atendimentos": {
            "pt": "Todos os grupos de atendimento",
			"en": "All service groups",
			"es": "Todos los grupos de atención"
        },
        "l-nenhum-requerimentos-disponivel": {
            "pt": "No momento não existem tipos de atendimentos disponíveis para um novo requerimento",
			"en": "There are no types of services available for a new requirement now",
			"es": "Por el momento no existem tipos de atención disponibles para un nuevo requerimiento"
        },
        "l-tipo": {
            "pt": "Tipo",
			"en": "Type",
			"es": "Tipo"
        },
        "l-prazo": {
            "pt": "Prazo",
			"en": "Deadline",
			"es": "Plazo"
        },
        "l-valor": {
            "pt": "Valor",
			"en": "Value",
			"es": "Valor"
        },
        "l-ver-detalhes": {
            "pt": "ver detalhes",
			"en": "see details",
			"es": "ver detalles"
        },
        "l-requerimentos": {
            "pt": "Requerimentos",
			"en": "Requirements",
			"es": "Requerimientos"
        },
        "l-disponiveis": {
            "pt": "Requerimentos Disponíveis",
			"en": "Requirements Available",
			"es": "Requerimientos disponibles"
        },
        "l-solicitados": {
            "pt": "Requerimentos Solicitados",
			"en": "Requirements requested",
			"es": "Requerimientos solicitados"
        },
        "l-grupo-atendimento": {
            "pt": "Grupo de atendimento",
			"en": "Service group",
			"es": "Grupo de atención"
        },
        "l-detalhes-requerimento": {
            "pt": "Detalhes do Requerimento:",
			"en": "Details of the requirement",
			"es": "Detalles del requerimiento:"
        },
        "l-tipo-requerimento": {
            "pt": "Tipo de Requerimento",
			"en": "Type of Requirement",
			"es": "Tipo de requerimiento"
        },
        "l-descricao-requerimento": {
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "l-procedimentos-requerimento": {
            "pt": "Procedimentos",
			"en": "Procedures",
			"es": "Procedimientos"
        },
        "l-valor-servico-requerimento": {
            "pt": "Valor do serviço",
			"en": "Service value",
			"es": "Valor del servicio"
        },
        "l-entrega-requerimento": {
            "pt": "Entrega da solicitação",
			"en": "Delivery of request",
			"es": "Entrega de la solicitud"
        },
        "l-observacoes-requerimento": {
            "pt": "Solicitação",
			"en": "Request",
			"es": "Solicitud"
        },
        "l-aceite-requerimento": {
            "pt": "Aceite",
			"en": "Acceptance",
			"es": "Aceptación"
        },
        "l-local-entrega": {
            "pt": "Local de Entrega",
			"en": "Delivery Location",
			"es": "Local de entrega"
        },
        "l-localidade": {
            "pt": "Localidade",
			"en": "Location",
			"es": "Localidad"
        },
        "l-taxa": {
            "pt": "Taxa (R$)",
			"en": "Rate (R$)",
			"es": "Tasa (R$)"
        },
        "l-valor-total-requerimento": {
            "pt": "Custo Total",
			"en": "Total Cost",
			"es": "Costo total"
        },
        "l-botao-registrar-requerimento": {
            "pt": "Solicitar",
			"en": "Request",
			"es": "Solicitar"
        },
        "l-seleciona-local-entrega": {
            "pt": "Selecione o local para entrega!",
			"en": "Select location for delivery!",
			"es": "¡Seleccione el local para entrega!"
        },
        "l-btn-solicitar": {
            "pt": "Solicitar",
			"en": "Request",
			"es": "Solicitar"
        },
        "l-confirma-requerimento": {
            "pt": "Confirmação de Requerimento",
			"en": "Requirement confirmation",
			"es": "Confirmación de requerimiento"
        },
        "l-msg-confirmacao-requerimento": {
            "pt": "Você confirma a abertura do requerimento com base nos dados informados?",
			"en": "Do you confirm the opening of the requirement based on the data informed?",
			"es": "¿Confirma la apertura del requerimiento con base en los datos informados?"
        },
        "l-detalhes-solicitacao": {
            "pt": "Detalhes do Requerimento",
			"en": "Details of the requirement",
			"es": "Detalles del requerimiento"
        },
        "l-visualizar-descricao-detalhada": {
            "pt": "Visualizar descrição detalhada do requerimento",
			"en": "View detailed description of the requirement",
			"es": "Visualizar descripción detallada del requerimiento"
        },
        "l-msg-redirecionamento-financeiro": {
            "pt": "A abertura deste requerimento possui um custo associado ao mesmo, por esse motivo o sistema gerou um boleto a ser pago. Você deseja visualizar o extrato financeiro para impressão deste boleto?",
			"en": "The opening of this requirement has cost associated to it, for this reason, the system generated a bank slip to be paid. Do you want to view the financial statement to print this bank slip?",
			"es": "La apertura de este requerimiento tiene un costo vinculado a este, por este motivo, el sistema generó una boleta para pagar. ¿Desea visualizar el extracto financiero para imprimir esta boleta?"
        },
        "l-titulo=msg-redirecionamento-financeiro": {
            "pt": "Confirmação",
			"en": "Confirmation",
			"es": "Confirmación"
        },
        "l-msg-operacao-sucesso": {
            "pt": "O requerimento foi encaminhado com sucesso para a Secretaria! Você poderá acompanhar seu andamento na aba \"Requerimentos Solicitados\", pelo protocolo de registro ",
			"en": "The requirement was successfully followed-up to the main office! You can follow your progress in the tab \"Requirements requested\". Protocol of the record:",
			"es": "¡El requerimiento se encaminó a la secretaría con éxito! Puede acompañar su ejecución en la solapa \"Requerimientos solicitados\". Comprobante de registro: "
        },
        "l-status-em-andamento": {
            "pt": "Em andamento",
			"en": "In progress",
			"es": "En ejecución"
        },
        "l-status-cancelado": {
            "pt": "Cancelado",
			"en": "Canceled",
			"es": "Anulado"
        },
        "l-status-concluido": {
            "pt": "Concluído",
			"en": "Concluded",
			"es": "Concluido"
        },
        "l-cancelar-requerimento": {
            "pt": "Cancelar Requerimento",
			"en": "Cancel Requirement",
			"es": "Anular requerimiento"
        },
        "l-enviar-arquivos-requerimento": {
            "pt": "Enviar arquivos",
			"en": "Send files",
			"es": "Enviar archivos"
        },
        "l-inserir-discussoes-requerimento": {
            "pt": "Adicionar discussão",
			"en": "Add discussion",
			"es": "Agregar discusión"
        },
        "l-protocolo": {
            "pt": "Protocolo",
			"en": "Protocol",
			"es": "Comprobante"
        },
        "l-abertura": {
            "pt": "Abertura",
			"en": "Opening",
			"es": "Apertura"
        },
        "l-etapa-atual": {
            "pt": "Etapa Atual",
			"en": "Current Stage",
			"es": "Etapa actual"
        },
        "l-status": {
            "pt": "Status",
			"en": "Status",
			"es": "Estatus"
        },
        "l-entrega-prevista": {
            "pt": "Entrega Prevista",
			"en": "Estimated Delivery",
			"es": "Entrega prevista"
        },
        "l-solicitacao": {
            "pt": "Solicitação",
			"en": "Request",
			"es": "Solicitud"
        },
        "l-discussao": {
            "pt": "Discussão",
			"en": "Discussion",
			"es": "Discusión"
        },
        "l-solucao": {
            "pt": "Solução",
			"en": "Solution",
			"es": "Solución"
        },
        "l-informacoes-complementares": {
            "pt": "Informações Complementares",
			"en": "Complementary Information",
			"es": "Información complementaria"
        },
        "l-etapas": {
            "pt": "Etapas",
			"en": "Stages",
			"es": "Etapas"
        },
        "l-arquivos": {
            "pt": "Arquivos",
			"en": "Files",
			"es": "Archivos"
        },
        "l-download": {
            "pt": "Download",
			"en": "Download",
			"es": "Descarga"
        },
        "l-arquivo": {
            "pt": "Arquivo",
			"en": "File",
			"es": "Archivo"
        },
        "l-tamanho": {
            "pt": "Tamanho",
			"en": "Size",
			"es": "Tamaño"
        },
        "l-data-conclusao": {
            "pt": "Data de conclusão",
			"en": "Conclusion date",
			"es": "Fecha de conclusión"
        },
        "l-historico-etapas": {
            "pt": "Histórico de etapas",
			"en": "History of stages",
			"es": "Historial de etapas"
        },
        "l-requerimentos-solicitados": {
            "pt": "Requerimentos Solicitados",
			"en": "Requirements Requested",
			"es": "Requerimientos solicitados"
        },
        "l-descricao":{
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "l-add-arquivo-anexo":{
            "pt": "Anexar Arquivos ao Requerimento",
			"en": "Attach Files to Requirement",
			"es": "Adjuntar archivos al requerimiento"
        },
        "l-add-discussao":{
            "pt": "Adicionar Discussão ao Requerimento",
			"en": "Add Discussion to Requirement",
			"es": "Agregar discusión al requerimiento"
        },
        "l-confirma-cancelamento-requerimento":{
            "pt": "Confirmação de cancelamento",
			"en": "Confirmation of cancellation",
			"es": "Confirmación de anulación"
        },
        "l-msg-confirmacao-cancelamento-requerimento": {
            "pt": "Deseja realmente efetuar o cancelamento deste Requerimento? Se prosseguir esta operação não poderá ser desfeita.",
			"en": "Do you really want to cancel this Requirement? If you proceed, you will not be able to undo this operation.",
			"es": "¿Realmente desea efectuar la anulación de este requerimiento? Si continua con esta operación no podrá revertirla."
        },
        "l-msg-cancelamento-agendado-sucesso":{
            "pt": "O cancelamento do requerimento foi agendado com sucesso! Aguarde alguns instantes e consulte o status do requerimento novamente.",
			"en": "The cancelation of the requirement was successfully scheduled! Wait some minutes and query the status of the requirement again.",
			"es": "La anulación del requerimiento se programó en agenda con éxito! Espere algunos instantes y consulte nuevamente el estatus del requerimiento."
        },
        "l-justificativa": {
            "pt": "Justificativa",
			"en": "Justification",
			"es": "Justificación"
        },
        "l-texto-discussao": {
            "pt": "Texto de discussão",
			"en": "Discussion text",
			"es": "Texto de discusión"
        },
        "l-msg-campo-obrigatorio": {
            "pt": "Campo de preenchimento obrigatório",
			"en": "Completion field is required",
			"es": "Campo de cumplimentación obligatoria"
        },
        "l-btn-adicionar-requerimento":{
            "pt": "Adicionar ao requerimento",
			"en": "Add to requirement",
			"es": "Agregar al requerimiento"
        },
        "l-msg-campo-obrigatorio-discussao":{
            "pt": "O campo \"Texto de Discussão\" é de preenchimento obrigatório!",
			"en": "The field \"Text of Discussion\" must be completed!",
			"es": "¡El campo \"Texto de discusión\" debe cumplimentarse obligatoriamente!"
        },
        "l-msg-campo-obrigatorio-justificativa":{
            "pt": "O campo \"Justificativa\" é de preenchimento obrigatório!",
			"en": "The field \"Justification\" must be completed!",
			"es": "¡El campo \"Justificación\" debe cumplimentarse obligatoriamente!"
        },
        "l-btn-efetuar-cancelamento":{
            "pt": "Efetuar Cancelamento",
			"en": "Cancel",
			"es": "Efectuar anulación"
        },
        "l-msg-campo-obrigatorio-descricao-arquivo": {
            "pt": "O campo \"Descrição\" é de preenchimento obrigatório!",
			"en": "The field\"Description\" must be completed!",
			"es": "¡El campo \"Descripción\" debe cumplimentarse obligatoriamente!"
        },
        "l-msg-campo-obrigatorio-arquivo": {
            "pt": "Selecione o arquivo que deseja anexar ao requerimento!",
			"en": "Select file you want to attach to requirement!",
			"es": "¡Seleccione el archivo que desea adjuntar al requerimiento!"
        },
        "l-fitlro-protocolo": {
            "pt": "Filtrar por Nº Protocolo",
			"en": "Filter by Number of Protocol",
			"es": "Filtrar por Nº de comprobante"
        },
        "l-msg-tipo-atendimento-sem-fluxo-def": {
            "pt": "Este tipo de solicitação não está disponível, pois não possui um fluxo de tarefas definido.",
			"en": "This type of request is not available, as it does not have a flow of tasks defined.",
			"es": "Este tipo de solicitud no está disponible, porque no tiene un flujo de tareas definido."
        },
        "l-enviar-arquivo": {
            "pt": "Enviar Arquivo",
			"en": "Send File",
			"es": "Enviar archivo"
        },
        "l-requerimento-nao-encontrado": {
            "pt": "Não foram encontrados requerimentos.",
			"en": "No requirements found.",
			"es": "No se encontraron requerimientos."
        },
        "l-data-alteracao-arq": {
            "pt": "Data alteração",
			"en": "Edition date",
			"es": "Fecha de modificación"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        }
    }
]
