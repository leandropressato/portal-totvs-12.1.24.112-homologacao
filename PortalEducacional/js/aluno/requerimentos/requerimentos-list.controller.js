define(['aluno/requerimentos/requerimentos.module',
    'aluno/requerimentos/requerimentos.factory',
    'utils/edu-constantes-globais.constants',
    'widgets/widget.constants'
], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .controller('eduRequerimentosController', EduRequerimentosController);

    EduRequerimentosController.$inject = ['$rootScope', '$state', 'eduConstantesGlobaisConsts', 'eduWidgetsConsts', 'totvs.app-notification.Service', 'i18nFilter'];

    function EduRequerimentosController($rootScope, $state, eduConstantesGlobaisConsts, eduWidgetsConsts, totvsNotification, i18nFilter) {

        var self = this;

        self.exibeTab = exibeTab;
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Requerimentos;

        init();

        var permissionsWatch = $rootScope.$watch('objPermissions', function () {

            if ($rootScope.objPermissions) {

                if (!$rootScope.objPermissions.EDU_ACADEMICO_SECRETARIA_REQUERIMENTOS) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        function init() {

            if ($state.params.protocolo) {
                self.activeTabSolicitados = true;
            }
        }

        function exibeTab(tabName) {
            var exibe = false;

            if ($rootScope.objPermissions === null) {
                return exibe;
            }

            switch (tabName) {
                case 'requerimentos-disponiveis':
                    exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_SECRETARIA_REQUERIMENTOS_DISPONIVEIS);
                    break;
                case 'requerimentos-solicitados':
                    exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_SECRETARIA_REQUERIMENTOS_SOLICITADOS);
                    break;
            }
            return exibe;
        }
    }
});
