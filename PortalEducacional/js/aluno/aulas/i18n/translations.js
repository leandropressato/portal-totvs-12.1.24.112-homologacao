[
    {
        "l-titulo": {
            "pt": "Aulas da disciplina",
			"en": "Classes of the course subject",
			"es": "Clases de la materia"
        },
        "l-home": {
            "pt": "Home",
			"en": "Home",
			"es": "Home"
        },
        "l-datas": {
            "pt": "Datas",
			"en": "Dates",
			"es": "Fechas"
        },
        "l-data": {
            "pt": "Data",
			"en": "Date",
			"es": "Fecha"
        },
        "l-aulas-do-dia": {
            "pt": "Visualizar todas as aulas do dia",
			"en": "Visualizar todas as aulas do dia",
			"es": "Visualizar todas as aulas do dia"
        },
        "l-aula": {
            "pt": "Aula",
			"en": "Class",
			"es": "Clase"
        },
        "l-faltas": {
            "pt": "Faltas",
			"en": "Absences",
			"es": "Faltas"
        },
        "l-falta": {
            "pt": "Falta",
			"en": "Absence",
			"es": "Falta"
        },
        "l-frequencia": {
            "pt": "Frequência",
			"en": "Frequency",
			"es": "Frecuencia"
        },
        "l-avaliacoes": {
            "pt": "Avaliações",
			"en": "Evaluations",
			"es": "Evaluaciones"
        },
        "l-avaliacao": {
            "pt": "Avaliação",
			"en": "Evaluation",
			"es": "Evaluación"
        },
        "l-material": {
            "pt": "Material",
			"en": "Material",
			"es": "Material"
        },
        "l-materiais": {
            "pt": "Materiais",
			"en": "Materials",
			"es": "Materiales"
        },
        "l-ocorrencia": {
            "pt": "Ocorrência",
			"en": "Occurrence",
			"es": "Ocurrencia"
        },
        "l-ocorrencias": {
            "pt": "Ocorrências",
			"en": "Occurrences",
			"es": "Ocurrencias"
        },
        "l-licao-de-casa": {
            "pt": "Lição de casa",
			"en": "Homework",
			"es": "Lección de casa"
        },
        "l-licoes": {
            "pt": "Lições de casa",
			"en": "Homework",
			"es": "Lecciones de casa"
        },
        "l-btn-filtrar":{
            "pt": "Filtrar",
			"en": "Filter",
			"es": "Filtrar"
        },
        "l-btn-dias":{
            "pt": "dias anteriores",
			"en": "previous days",
			"es": "días anteriores"
        },
        "l-date-format": {
            "pt": "dd/MM/yyyy",
			"en": "MM/dd/yyyy",
			"es": "dd/MM/aaaa"
        },
        "l-culture": {
            "pt": "pt",
			"en": "en",
			"es": "es"
        },
        "l-date-range-placeholder": {
            "pt": "dia/mês/ano",
			"en": "month/day/year",
			"es": "día/mes/año"
        },
        "l-material-grid": {
            "pt": "material",
			"en": "material",
			"es": "material"
        },
        "l-materiais-grid": {
            "pt": "materiais",
			"en": "materials",
			"es": "materiales"
        },
        "l-em": {
            "pt": "em",
			"en": "in",
			"es": "en"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Course Subject",
			"es": "Materia"
        },
        "l-inicio": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-termino": {
            "pt": "Término",
			"en": "End",
			"es": "Final"
        },
        "l-tipo": {
            "pt": "Tipo",
			"en": "Type",
			"es": "Tipo"
        },
        "l-local": {
            "pt": "Local",
			"en": "Location",
			"es": "Local"
        },
        "l-plano-aula": {
            "pt": "Plano de aula",
			"en": "Lesson plan",
			"es": "Plan de clase"
        },
        "l-arquivos-da-aula": {
            "pt": "Arquivos da aula",
			"en": "Class files",
			"es": "Archivos de la clase"
        },
        "l-conteudo-previsto": {
            "pt": "Conteúdo previsto",
			"en": "Estimated content",
			"es": "Contenido previsto"
        },
        "l-conteudo-realizado": {
            "pt": "Conteúdo realizado",
			"en": "Content executed",
			"es": "Contenido realizado"
        },
        "l-data-realizado": {
            "pt": "Data",
			"en": "Date",
			"es": "Fecha"
        },
        "l-msg-conteudo-vazio": {
            "pt": "Conteúdo não cadastrado",
			"en": "Content not registered",
			"es": "Contenido no registrado"
        },
        "l-nao-cadastrado": {
            "pt": "(não cadastrado)",
			"en": "(not registered)",
			"es": "(no registrado)"
        },
        "l-aulateorica": {
            "pt": "Aula teórica"
        },
        "l-aulapratica": {
            "pt": "Aula prática"
        },
        "l-teorica": {
            "pt": "Teórica",
			"en": "Theorical",
			"es": "Teórica"
        },
        "l-pratica": {
            "pt": "Prática",
			"en": "Practical",
			"es": "Práctica"
        },
        "l-laboratorio": {
            "pt": "Laboratório",
			"en": "Lab",
			"es": "Laboratorio"
        },
        "l-estagio": {
            "pt": "Estágio",
			"en": "Internship",
			"es": "Pasantía"
        },
        "l-mista": {
            "pt": "Mista",
			"en": "Mixed",
			"es": "Mixta"
        },
        "l-btn-fechar": {
            "pt": "Fechar",
			"en": "Close",
			"es": "Cerrar"
        },
        "l-arquivo": {
            "pt": "Arquivo",
			"en": "File",
			"es": "Archivo"
        },
        "l-tamanho": {
            "pt": "Tamanho",
			"en": "Size",
			"es": "Tamaño"
        },
        "btn-download": {
            "pt": "Download",
			"en": "Download",
			"es": "Descarga"
        },
        "l-KB": {
            "pt": "KB",
			"en": "KB",
			"es": "KB"
        },
        "l-data-expiracao": {
            "pt": "Data de expiração",
			"en": "Expiration date",
			"es": "Fecha de expiración"
        },
        "l-nenhum-registro-encontrado-aula": {
            "pt": "Nenhum registro de aula foi localizado para o período informado!",
			"en": "No record of class was found for the period informed!",
			"es": "¡No se encontró ningún registro de clase para el período informado!"
        },
        "l-esta-semana": {
            "pt": "Esta semana",
			"en": "This week",
			"es": "Esta semana"
        },
        "l-hoje": {
            "pt": "Hoje",
			"en": "Today",
			"es": "Hoy"
        },
        "l-ultima-semana": {
            "pt": "Última semana",
			"en": "Last week",
			"es": "Última semana"
        },
        "l-proxima-semana": {
            "pt": "Próxima semana",
			"en": "Next week",
			"es": "Próxima semana"
        },
        "l-turma": {
            "pt": "Turma",
			"en": "Class",
			"es": "Grupo"
        },
        "l-mais-infomacoes-aula": {
            "pt": "Ver mais informações",
			"en": "See further information",
			"es": "Ver más información"
        },
        "l-mais-infomacoes-turmadisc": {
            "pt": "Ver disciplina",
			"en": "See course subject",
			"es": "Ver materia"
        },
        "l-dia-semana": {
            "pt": "Dia da semana",
			"en": "Weekday",
			"es": "Día de la semana"
        },
        "l-informacao":{
            "pt": "Informação",
			"en": "Information",
			"es": "Información"
        },
        "l-aviso-redirecionamento":{
            "pt": "Você foi redirecionado para a tela inicial do sistema, pois o houve uma nova seleção de curso!",
			"en": "You were redirected to the home screen of the system, as there was a new selection of the course!",
			"es": "Usted fue reorientado a la pantalla inicial del sistema, porque hubo una nueva selección de curso."
        },
        "l-visualizar-licao-casa":{
            "pt": "Visualizar",
			"en": "View",
			"es": "Visualizar"
        },
        "l-download-materiais":{
            "pt": "Download de Materiais",
			"en": "Download of Materials",
			"es": "Descarga de materiales"
        },
        "l-abonado":{
            "pt": "abonado",
			"en": "waived",
			"es": "abonado"
        },
        "l-justificado":{
            "pt": "justificado",
			"en": "justified",
			"es": "justificado"
        },
        "l-justificativa-falta":{
            "pt":"Informações sobre a Falta",
			"en":"Information on the Absence",
			"es":"Información sobre la falta"
        },
        "l-sem-justificativa-falta":{
            "pt":"Falta não justificada",
			"en":"Absence not justified",
			"es":"Falta no justificada"
        },
        "l-justificativa": {
            "pt": "Justificativa",
			"en": "Justification",
			"es": "Justificación"
        },
        "l-gerou-abono":{
            "pt": "Abono",
			"en": "Waive",
			"es": "Abono"
        },
        "l-descricao-material":{
            "pt": "Descrição do material",
			"en": "Description of material",
			"es": "Descripción del material"
        },
        "l-nome-arquivo":{
            "pt": "Nome do Arquivo",
			"en": "File Name",
			"es": "Nombre del archivo"
        },
        "l-publicacao":{
            "pt": "Publicação",
			"en": "Publication",
			"es": "Publicación"
        },
        "l-predio": {
            "pt": "Prédio",
            "en": "Building",
            "es": "Edificio"
        },
        "l-bloco": {
            "pt": "Bloco",
            "en": "Block",
            "es": "Bloque"
        },
        "l-sala": {
            "pt": "Sala",
            "en": "Room",
            "es": "Sala"
        }
    }
]
