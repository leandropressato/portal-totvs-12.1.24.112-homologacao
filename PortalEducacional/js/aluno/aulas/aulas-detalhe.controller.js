/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduAulasModule
 * @name EduAulasListController
 * @object controller
 *
 * @created 2016-09-05 v12.1.15
 * @updated
 *
 * @requires Aulas.module
 *
 * @dependencies eduAulasFactory
 *
 * @description Controller de Aulas
 */
define([
    'aluno/aulas/aulas.module',
    'aluno/aulas/aulas.route',
    'aluno/aulas/aulas.factory',
    'utils/edu-enums.constants',
    'utils/edu-constantes-globais.constants',
    'utils/edu-utils.service'
], function () {

    'use strict';

    angular.module('eduAulasModule')
        .controller('eduAulasDetalheController', EduAulasDetalheController);

    EduAulasDetalheController.$inject = ['$scope',
        '$filter',
        'eduAulasFactory',
        'TotvsDesktopContextoCursoFactory',
        '$state',
        '$sce',
        'eduEnumsConsts',
        'totvs.app-notification.Service',
        'i18nFilter',
        'eduConstantesGlobaisConsts',
        'eduUtilsService'
    ];

    function EduAulasDetalheController($scope,
        $filter,
        eduAulasFactory,
        TotvsDesktopContextoCursoFactory,
        $state,
        $sce,
        eduEnumsConsts,
        totvsNotification,
        i18nFilter,
        eduConstantesGlobaisConsts,
        eduUtilsService) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.limitAtArquivos = 20; // limite de resultados para paginação
        self.arquivosCount = 0;
        self.arquivos = [];

        self.onclickMaisInfoDisciplina = onclickMaisInfoDisciplina;
        self.getDescTipoAula = getDescTipoAula;
        self.search = search;
        self.onclickDownload = onclickDownload;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        init();

        //Manipula evento disparado pelo modal de seleção de contexto
        $scope.$on('OnChangeCursoSelecionado:Event', function () {

            totvsNotification.notify({
                type: 'info',
                title: i18nFilter('l-informacao', [], 'js/aluno/aulas'),
                detail: i18nFilter('l-aviso-redirecionamento', [], 'js/aluno/aulas')
            });

            $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
        });

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        /**
         * Inicializa.
         */
        function init() {

            self.idTurmaDisc = $state.params.idTurmaDisc;
            self.idPlanoAula = $state.params.idPlanoAula;

            search(false);

        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************
        function search(isMore) {

            var startAtArquivos = 0;

            self.arquivosCount = 0;

            if (isMore) {
                startAtArquivos = self.arquivos.length;
            } else {
                self.arquivos = [];
            }

            buscarDados(startAtArquivos);
        }

        function buscarDados(startAtArquivos) {

            eduAulasFactory.retornarDetalhesAula(self.idTurmaDisc, self.idPlanoAula, function (result) {
                if (result) {

                    self.aula = result.SPlanoAula[0];

                    /* trata campos com possíveis tags HTML */
                    self.aula.CONTEUDO = $sce.trustAsHtml(eduUtilsService.tratarQuebrasDeLinhaStringParaHTML(self.aula.CONTEUDO));
                    self.aula.CONTEUDOEFETIVO = $sce.trustAsHtml(eduUtilsService.tratarQuebrasDeLinhaStringParaHTML(self.aula.CONTEUDOEFETIVO));
                    self.aula.LICAOCASA = $sce.trustAsHtml(eduUtilsService.tratarQuebrasDeLinhaStringParaHTML(self.aula.LICAOCASA));

                    self.aula.local = eduAulasFactory.montarDescricaoLocal(self.aula.PREDIO, self.aula.BLOCO, self.aula.SALA);
                }
            });

            eduAulasFactory.retornarArquivosAula(startAtArquivos,self.limitAtArquivos, self.idTurmaDisc, self.idPlanoAula, function (result) {

                if (result) {

                    angular.forEach(result, function (value) {

                            if (value && value.$length) {
                                self.arquivosCount = value.$length;
                            }

                            self.arquivos.push(value);

                        });

                }
            });

        }

        function getDescTipoAula(tipoAula) {

            return eduAulasFactory.getDescTipoAula(tipoAula);

        }

        /**
         * Redireciona para a página da turma/disciplina
         */
        function onclickMaisInfoDisciplina() {

            var params = {
                idTurmaDisc: self.idTurmaDisc,
                codDisc: self.aula.CODDISC
            };

            $state.go('disciplina.start', params);

        }

        function onclickDownload(idMaterialSec, pathArquivo) {
            var link = document.createElement('a');
            link.download = pathArquivo;
            link.href = eduAulasFactory.retornarArquivo(idMaterialSec, pathArquivo);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

    }
});
