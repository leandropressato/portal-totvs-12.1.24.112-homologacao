/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduAulasModule
 * @name EduAulasListController
 * @object controller
 *
 * @created 2016-09-05 v12.1.15
 * @updated
 *
 * @requires Aulas.module
 *
 * @dependencies eduAulasFactory
 *
 * @description Controller de Aulas
 */
define([
    'aluno/aulas/aulas.module',
    'aluno/aulas/aulas.route',
    'aluno/aulas/aulas.factory',
    'utils/edu-constantes-globais.constants',
    'utils/edu-utils.service',
    'aluno/arquivos/arquivo.factory'
], function () {

    'use strict';

    angular.module('eduAulasModule')
        .controller('eduAulasListController', EduAulasListController);

    EduAulasListController.$inject = ['$scope',
        '$filter',
        '$state',
        'eduAulasFactory',
        'eduConstantesGlobaisConsts',
        '$rootScope',
        '$sce',
        'eduUtilsService',
        'eduMaterialFactory'
    ];

    function EduAulasListController($scope,
        $filter,
        $state,
        eduAulasFactory,
        eduConstantesGlobaisConsts,
        $rootScope,
        $sce,
        eduUtilsService,
        eduMaterialFactory) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        self.getTemplateCampoMateriaisAula = getTemplateCampoMateriaisAula;
        self.getTemplateCampoLicoesAula = getTemplateCampoLicoesAula;
        self.getTemplateCampoFrequenciaAula = getTemplateCampoFrequenciaAula;
        self.getTemplateCampoHorarioAula = getTemplateCampoHorarioAula;

        self.AbrirVisualizacaoAvaliacoes = AbrirVisualizacaoAvaliacoes;
        self.AbrirVisualizacaoOcorrencias = AbrirVisualizacaoOcorrencias;
        self.AbrirVisualizacaoLicaoCasa = AbrirVisualizacaoLicaoCasa;
        self.AbrirVisualizacaoDownloadMateriais = AbrirVisualizacaoDownloadMateriais;
        self.AbrirVisualizacaoJustificativaFalta = AbrirVisualizacaoJustificativaFalta;

        self.filtrarPlanoAulaPorData = filtrarPlanoAulaPorData;
        self.filtrarPlanoAulaUltimaSemana = filtrarPlanoAulaUltimaSemana;
        self.filtrarPlanoAulaDataAtual = filtrarPlanoAulaDataAtual;
        self.filtrarPlanoAulaProximaSemana = filtrarPlanoAulaProximaSemana;
        self.naoPodeFiltrar = naoPodeFiltrar;
        self.downloadMaterial = downloadMaterial;

        self.model = [];
        self.dataRangeFiltro = {};
        self.activeBtn = 'btn-ultima-semana';
        self.codSubTurma = '';

        self.infoArrayLicoesCasaSelecionados = [];
        self.infoArrayDownloadMateriaisSelecionados = [];
        self.infoJustificativaFalta = {};

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        init();

        function init() {
            var myWatch = $rootScope.$watch('objPermissions', function (data) {
                if (data !== null) {
                    self.possuiPermissaoPlanoAula = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_PLANOAULA);
                    myWatch();
                }
            });

            if ($state.params.idTurmaDisc) {

                configurarGridOptions();

                var parent = $scope.$parent['controller'];

                parent.getDiscInfo(function (result) {
                    if (result.DisciplinaInfo[0].CODSUBTURMA !== null) {
                        self.codSubTurma = result.DisciplinaInfo[0].CODSUBTURMA;
                    }
                    filtrarPlanoAulaUltimaSemana();
                });
            } else {
                $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
            }
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Configura o grid options (propriedades/comportamentos/atributos)
         */
        function configurarGridOptions() {

            self.gridOptionsGridPlanoAulas = {
                columns: definirColunasGridPlanoAulas(),
                dataSource: {
                    data: self.model
                },
                detailTemplate: kendo.template(getTemplateGridAulas()),
                dataBound: function () {
                    //Expande a primeira linha de aula
                    //this.expandRow(this.tbody.find('tr.k-master-row').first());
                },
                resizable: false,
                selectable: false
            };
        }

        function carregarAulasDisciplinaAsync(idTurmaDisc, dataInicio, dataFim, callback) {
            eduAulasFactory.retornarAulasDisciplinaPlanoAula(idTurmaDisc, self.codSubTurma, dataInicio, dataFim, function (result) {

                if (result) {
                    self.model = result;
                }

                if (typeof callback === 'function') {
                    callback(result);
                }
            });
        }

        function filtrarPlanoAulaPorData(callback) {

            var startDate = new Date(self.dataRangeFiltro.startDate),
                endDate = new Date(self.dataRangeFiltro.endDate),
                dataInicio = new Date(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getUTCDate(), 12),
                dataFim = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate(), 12);

            carregarAulasDisciplinaAsync($state.params.idTurmaDisc, dataInicio, dataFim, callback);
        }

        function filtrarPlanoAulaUltimaSemana() {

            // Limpa filtro por DataRange
            self.dataRangeFiltro = [];
            self.activeBtn = 'btn-ultima-semana';

            var dataAux = new Date();

            //Se o dia da semana da data for domingo, deve ser retirado um dia
            if (dataAux.getDay() === 0) {
                dataAux = new Date(dataAux.setDate(dataAux.getDate() - 1));
            }

            //Retorna o primeiro dia útil da semana em questão
            var dataInicio = retornaPrimeiroDiaUtilSemana(dataAux);

            //Data fim corresponderá ao último dia da semana como sendo domingo da próxima semana
            dataAux = new Date(dataInicio);
            dataAux.setDate(dataAux.getDate() + 6);
            var dataFim = new Date(dataAux);

            carregarAulasDisciplinaAsync($state.params.idTurmaDisc, dataInicio, dataFim, null);
        }

        function filtrarPlanoAulaDataAtual() {

            // Limpa filtro por DataRange
            self.dataRangeFiltro = [];
            self.activeBtn = 'btn-hoje';

            var dataHoje = new Date();

            carregarAulasDisciplinaAsync($state.params.idTurmaDisc, dataHoje, dataHoje, null);
        }

        function filtrarPlanoAulaProximaSemana() {

            // Limpa filtro por DataRange
            self.dataRangeFiltro = [];
            self.activeBtn = 'btn-proxima-semana';

            var dataAux = new Date();

            //Se o dia da semana da data for diferente de domingo, será adicionado 7 dias para acessar a próxima semana
            if (dataAux.getDay() !== 0) {
                dataAux = new Date(dataAux.setDate(dataAux.getDate() + 7));
            }

            //Retorna o primeiro dia útil da semana em questão
            var dataInicio = retornaPrimeiroDiaUtilSemana(dataAux);

            //Data fim corresponderá ao último dia da semana como sendo domingo da próxima semana
            dataAux = new Date(dataInicio);
            dataAux.setDate(dataAux.getDate() + 6);
            var dataFim = new Date(dataAux);

            carregarAulasDisciplinaAsync($state.params.idTurmaDisc, dataInicio, dataFim, null);
        }

        function retornaPrimeiroDiaUtilSemana(data) {

            var day = data.getDay(),
                diff = data.getDate() - day + (day === 0 ? -6 : 1);//Será desconsiderado o dia de domingo

            return new Date(data.setDate(diff));
        }

        function naoPodeFiltrar() {
            if (self.dataRangeFiltro.startDate == null || self.dataRangeFiltro.endDate == null) {
                return true;
            }
            return false;
        }

        function AbrirVisualizacaoAvaliacoes() {

            //Verifica se a view 'pai' possui o método a ser invocado
            if ($scope.$parent['controller'] !== null) {
                if (typeof ($scope.$parent['controller'].selecionarTabAvaliacoes) === 'function') {
                    $scope.$parent['controller'].selecionarTabAvaliacoes();
                }
            }
        }

        function AbrirVisualizacaoOcorrencias() {

            //Verifica se a view 'pai' possui o método a ser invocado
            if ($scope.$parent['controller'] !== null) {
                if (typeof ($scope.$parent['controller'].selecionarTabOcorrencias) === 'function') {
                    $scope.$parent['controller'].selecionarTabOcorrencias();
                }
            }
        }

        function AbrirVisualizacaoLicaoCasa(dataAula, idPlanoAula) {

            self.infoArrayLicoesCasaSelecionados = [];

            if (self.model.length > 0) {

                if (idPlanoAula) {

                    var dataAulasObjVerificaPlanoAula = self.model.find(function (item) {
                        if (item.DataAula === dataAula) {
                            return item;
                        }
                    }),
                        aulasObj = dataAulasObjVerificaPlanoAula.ListaDetalhesPlanoAulaPortal.find(function (item) {

                            if (item.PlanoAula.IdPlanoAula === idPlanoAula) {
                                return item;
                            }
                        }),
                        licaoDeCasaHTML = eduUtilsService.tratarQuebrasDeLinhaStringParaHTML(aulasObj.PlanoAula.LicaoCasa);

                    licaoDeCasaHTML = $sce.trustAsHtml(licaoDeCasaHTML);

                    self.infoArrayLicoesCasaSelecionados.push(licaoDeCasaHTML);
                }
                else {

                    var dataAulasObj = self.model.find(function (item) {
                        if (item.DataAula === dataAula) {
                            return item;
                        }
                    });

                    angular.forEach(dataAulasObj.ListaDetalhesPlanoAulaPortal, function (item) {

                        var licaoDeCasaHTML = eduUtilsService.tratarQuebrasDeLinhaStringParaHTML(item.PlanoAula.LicaoCasa);
                        licaoDeCasaHTML = $sce.trustAsHtml(licaoDeCasaHTML);

                        self.infoArrayLicoesCasaSelecionados.push(licaoDeCasaHTML);

                    }, this);
                }

                $('#modalLicaoCasa').modal('show');
            }
        }

        function AbrirVisualizacaoDownloadMateriais(dataAula, idPlanoAula) {

            self.infoArrayDownloadMateriaisSelecionados = [];

            if (self.model.length > 0) {

                if (idPlanoAula) {

                    var dataAulasObjVerificaPlanoAula = self.model.find(function (item) {
                        if (item.DataAula === dataAula) {
                            return item;
                        }
                    });

                    angular.forEach(dataAulasObjVerificaPlanoAula.ListaMateriais, function (item) {

                        if (item.IdPlanoAula === idPlanoAula) {
                            self.infoArrayDownloadMateriaisSelecionados.push(item);
                        }

                    }, this);
                }
                else {

                    var dataAulasObj = self.model.find(function (item) {
                        if (item.DataAula === dataAula) {
                            return item;
                        }
                    });

                    angular.forEach(dataAulasObj.ListaMateriais, function (item) {
                        self.infoArrayDownloadMateriaisSelecionados.push(item);
                    }, this);
                }

                $('#modalDownloadMateriais').modal('show');
            }
        }

        function AbrirVisualizacaoJustificativaFalta(dataAula, IdHorarioTurma) {

            self.infoJustificativaFalta = {};
            if (self.model.length > 0) {

                if (IdHorarioTurma) {

                    var dataAulasObjVerificaPlanoAula = self.model.find(function (item) {
                        if (item.DataAula === dataAula) {
                            return item;
                        }
                    });

                    angular.forEach(dataAulasObjVerificaPlanoAula.ListaFaltas, function (item) {

                        if (item.IdHorarioTurma === IdHorarioTurma) {
                            self.infoJustificativaFalta = item;
                        }

                    }, this);
                }

                $('#modalJustificativaFalta').modal('show');
            }
        }

        function definirColunasGridPlanoAulas() {

            var arrColunas = [{
                field: 'DataAula',
                attributes: {
                    'class': 'gridRow text-center'
                },
                title: filtroI18n('l-data'),
                template: getTemplateCampoData
            },
            {
                field: 'DataAula',
                attributes: {
                    'class': 'gridRow text-center'
                },
                title: filtroI18n('l-dia-semana'),
                template: getTemplateCampoDiaSemana
            },
            {
                field: 'QuantidadeFaltas',
                attributes: {
                    'class': 'gridRow text-center'
                },
                title: filtroI18n('l-faltas'),
                template: getTemplateCampoFaltas
            },
            {
                field: 'QuantidadeMateriais',
                attributes: {
                    'class': 'gridRow text-center'
                },
                title: filtroI18n('l-materiais'),
                template: getTemplateCampoMateriais
            },
            {
                field: 'QuantidadeAvaliacoes',
                attributes: {
                    'class': 'gridRow text-center'
                },
                title: filtroI18n('l-avaliacoes'),
                template: getTemplateCampoAvaliacoes
            },
            {
                field: 'QuantidadeOcorrencia',
                attributes: {
                    'class': 'gridRow text-center'
                },
                title: filtroI18n('l-ocorrencias'),
                template: getTemplateCampoOcorrencias
            }
            ];

            if (self.possuiPermissaoPlanoAula) {
                arrColunas.splice(3, 0, {
                    field: 'QuantidadeLicoesDeCasa',
                    attributes: {
                        'class': 'gridRow text-center'
                    },
                    title: filtroI18n('l-licoes'),
                    template: getTemplateCampoLicoes
                });
            }

            return arrColunas;
        }

        function filtroI18n(nomeCampo) {
            return $filter('i18n')(nomeCampo, [], 'js/aluno/aulas');
        }

        function filtroData(valorCampo) {
            return $filter('date')(valorCampo, 'dd/MM/yyyy', 'UTC');
        }

        function getTemplateGridAulas() {

            var template = '';

            template += '                <totvs-grid grid-data="dataItem.ListaDetalhesPlanoAulaPortal" selectable="false">';
            template += '                    <totvs-grid-column';
            template += '                        class="gridRow text-center"';
            template += '                        template="controller.getTemplateCampoHorarioAula">';
            template += '                        {{::"l-aula" | i18n : [] : "js/aluno/aulas" }}';
            template += '                    </totvs-grid-column>';
            template += '                     <totvs-grid-column';
            template += '                        class="gridRow text-center"';
            template += '                        template="controller.getTemplateCampoFrequenciaAula">';
            template += '                        {{::"l-frequencia" | i18n : [] : "js/aluno/aulas" }}';
            template += '                    </totvs-grid-column>';
            template += '                    <totvs-grid-column';
            template += '                        class="gridRow text-center"';
            template += '                        template="controller.getTemplateCampoMateriaisAula">';
            template += '                        {{::"l-materiais" | i18n : [] : "js/aluno/aulas" }}';
            template += '                    </totvs-grid-column>';

            if (self.possuiPermissaoPlanoAula) {
                template += '                    <totvs-grid-column';
                template += '                        class="gridRow text-center"';
                template += '                        template="controller.getTemplateCampoLicoesAula">';
                template += '                        {{::"l-licao-de-casa" | i18n : [] : "js/aluno/aulas" }}';
                template += '                    </totvs-grid-column>';
            }

            template += '                </totvs-grid>';

            return template;
        }

        function getTemplateCampoDiaSemana(dataItem) {

            var template = '';

            if (dataItem.DataAula) {
                var numeroDiaSemana = new Date(dataItem.DataAula).getDay();

                //define a culture (browser dialect) a ser considerado pelo kendo
                kendo.culture('pt');
                template = eduUtilsService.capitalizeFirstLetter(kendo.culture().calendar.days.names[numeroDiaSemana]);
            }

            return template;
        }

        function getTemplateCampoData(dataItem) {

            var template = '';

            if (dataItem.DataAula) {
                template = filtroData(dataItem.DataAula);
            }

            return template;
        }

        function getTemplateCampoAvaliacoes(dataItem) {

            var template = '';

            if (dataItem.QuantidadeAvaliacoes > 0) {
                template += '<a href="" class="text-decoration-none" ng-click="controller.AbrirVisualizacaoAvaliacoes()">';
                template += '<span class="icon-avaliacao icon-font-grid">';
                template += '</span>';
                template += '</a>';
            }
            else {
                template += '-';
            }

            return template;
        }

        function getTemplateCampoOcorrencias(dataItem) {

            var template = '';

            if (dataItem.QuantidadeOcorrencia > 0) {
                template += '<a href="" class="text-decoration-none" ng-click="controller.AbrirVisualizacaoOcorrencias()">';
                template += '<span class="icon-ocorrencia icon-font-grid">';
                template += '</span>';
                template += '</a>';
            }
            else {
                template += '-';
            }

            return template;
        }

        function getTemplateCampoFaltas(dataItem) {

            var template = '';

            template += dataItem.QuantidadeFaltas;

            return template;
        }

        function getTemplateCampoLicoes(dataItem) {

            var template = '',
                argumentosFunctionVisualizacaoLicaoCasa = '';

            argumentosFunctionVisualizacaoLicaoCasa = '\'' + dataItem.DataAula + '\'';

            if (dataItem.QuantidadeLicoesDeCasa > 0) {
                template += '<a href="" ng-click="controller.AbrirVisualizacaoLicaoCasa(' + argumentosFunctionVisualizacaoLicaoCasa + ')">';
            }
            else {
                template += '<a href="">';
            }
            template += dataItem.QuantidadeLicoesDeCasa;
            template += '</a>';

            return template;
        }

        function getTemplateCampoLicoesAula(dataItem) {

            var template = '';

            if (dataItem.PlanoAula.LicaoCasa) {

                var dataAula = dataItem.parent().parent().DataAula,
                    idPlanoAula = dataItem.PlanoAula.IdPlanoAula,
                    argumentosFunctionVisualizacaoLicaoCasa = '';

                argumentosFunctionVisualizacaoLicaoCasa = '\'' + dataAula + '\' , ' + idPlanoAula;

                template += '<a href="" ng-click="controller.AbrirVisualizacaoLicaoCasa(' + argumentosFunctionVisualizacaoLicaoCasa + ')">';
                template += filtroI18n('l-visualizar-licao-casa');
                template += '</a>';
            }
            else {
                template += '-';
            }

            return template;
        }

        function getTemplateCampoMateriais(dataItem) {

            var template = '',
                argumentosFunctionVisualizacaoDownloadmateriais = '';

            argumentosFunctionVisualizacaoDownloadmateriais = '\'' + dataItem.DataAula + '\'';

            if (dataItem.QuantidadeMateriais > 0) {
                template += '<a href="" ng-click="controller.AbrirVisualizacaoDownloadMateriais(' + argumentosFunctionVisualizacaoDownloadmateriais + ')">';
                template += dataItem.QuantidadeMateriais;
                template += '</a>';
            }
            else {
                template += dataItem.QuantidadeMateriais;
            }

            return template;
        }

        function getTemplateCampoMateriaisAula(dataItem) {

            var template = '';

            if (dataItem.QuantidadeMateriais) {

                var dataAula = dataItem.parent().parent().DataAula,
                    idPlanoAula = dataItem.PlanoAula.IdPlanoAula,
                    argumentosFunctionVisualizacaoDownloadmateriais = '';

                argumentosFunctionVisualizacaoDownloadmateriais = '\'' + dataAula + '\' , ' + idPlanoAula;

                template += '<a href="" ng-click="controller.AbrirVisualizacaoDownloadMateriais(' + argumentosFunctionVisualizacaoDownloadmateriais + ')">';
                template += dataItem.QuantidadeMateriais;
                template += '</a>';
            }

            return template;
        }

        function getTemplateCampoFrequenciaAula(dataItem) {

            var template = '';

            if (dataItem.Frequencia) {

                var dataAula = dataItem.parent().parent().DataAula,
                    IdHorarioTurma = dataItem.Horario.IdHorarioTurma,
                    argumentosFunctionVisualizacaoJustificativaFalta = '',
                    descricaoFrequencia = '';

                argumentosFunctionVisualizacaoJustificativaFalta = '\'' + dataAula + '\' , ' + IdHorarioTurma;

                descricaoFrequencia = retornaDescricaoFrequencia(dataItem);

                if (dataItem.Frequencia === 'Ausente') {
                    template += '<a href="" ng-click="controller.AbrirVisualizacaoJustificativaFalta(' + argumentosFunctionVisualizacaoJustificativaFalta + ')" >';
                    template += descricaoFrequencia;
                    template += '</a>';
                } else {
                    template += descricaoFrequencia;
                }

            }
            else {
                template += '-';
            }

            return template;
        }

        function retornaDescricaoFrequencia(dataItem) {

            var descricao = dataItem.Frequencia,
                Aula = dataItem.parent().parent(),
                IdHorarioTurma = dataItem.Horario.IdHorarioTurma,
                faltaObj = Aula.ListaFaltas.find(function (item) {

                    if (item.IdHorarioTurma === IdHorarioTurma) {
                        return item;
                    }
                });

            if (faltaObj.AbonoFalta === 'Sim') {
                descricao += ' (' + filtroI18n('l-abonado') + ')';
            } else if (faltaObj.JustificativaFalta) {
                descricao += ' (' + filtroI18n('l-justificado') + ')';
            }

            return descricao;
        }

        function getTemplateCampoHorarioAula(dataItem) {

            var template = '';

            if (self.possuiPermissaoPlanoAula) {
                template += '<a href="#/aulas/detalhe/' + dataItem.PlanoAula.IdTurmaDisc + '/' + dataItem.PlanoAula.IdPlanoAula + '">';
                template += '<span>';
                template += dataItem.Horario.HoraInicial + ' - ' + dataItem.Horario.HoraFinal;
                template += '</span>';
                template += '</a>';
            }
            else {
                template += '<span>';
                template += dataItem.Horario.HoraInicial + ' - ' + dataItem.Horario.HoraFinal;
                template += '</span>';
            }

            return template;
        }

        function downloadMaterial(item) {

            var link = document.createElement('a');

            link.download = item.PathArquivo;
            link.href = eduMaterialFactory.urlDownloadArquivo(item.IdMaterialSec, item.PathArquivo);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
});
