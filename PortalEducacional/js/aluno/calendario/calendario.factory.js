/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduCalendarioModule
 * @name eduCalendarioFactory
 * @object factory
 *
 * @created 2017-02-06 v12.1.15
 * @updated
 *
 * @requires eduCalendarioModule
 *
 * @dependencies
 *
 * @description Factory utilizada no menu calendário.
 */

define(['aluno/calendario/calendario.module'], function () {

    'use strict';

    angular
        .module('eduCalendarioModule')
        .factory('eduCalendarioFactory', EduCalendarioFactory);

    EduCalendarioFactory.$inject = ['$totvsresource'];

    function EduCalendarioFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.retornaCalendario = retornaCalendario;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Retorna o calendário do aluno em um período letivo
         *
         * @param {any} callback   - método de callback
         */
        function retornaCalendario(callback) {

            var parameters = {};

            parameters.method = 'Aluno/CalendarioAluno';
            factory.TOTVSGet(parameters, callback);

        }

    }
});
