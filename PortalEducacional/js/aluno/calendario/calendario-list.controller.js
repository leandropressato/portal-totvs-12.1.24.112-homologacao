/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/calendario/calendario.module',
    'aluno/calendario/calendario.route',
    'aluno/calendario/calendario.factory',
    'utils/edu-utils.factory',
    'utils/reports/edu-relatorio.service',
    'utils/edu-enums.constants',
    'aluno/avaliacoes/avaliacoes.service',
    'aluno/periodo-matricula/periodo-matricula.service',
    'aluno/aulas/aulas.factory',
    'aluno/atividades-extras/atividades-extras.service',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'], function () {

        'use strict';

        angular
            .module('eduCalendarioModule')
            .controller('eduCalendarioController', EduCalendarioController);

        EduCalendarioController.$inject = ['$rootScope',
            '$filter',
            '$state',
            'maskFilter',
            'totvs.app-notification.Service',
            'i18nFilter',
            '$resource',
            'eduCalendarioFactory',
            'eduUtilsFactory',
            'eduRelatorioService',
            'eduEnumsConsts',
            'eduAvaliacoesService',
            'eduPeriodoMatriculaService',
            'eduAulasFactory',
            '$scope',
            'eduAtividadesExtrasService',
            '$compile',
            'eduWidgetsConsts',
            'eduConstantesGlobaisConsts'];

        /**
         *  Controller do calendário do aluno
         *
         * @param {any} $rootScope rootScope
         * @param {any} $filter filter
         * @param {any} state state
         * @param {any} maskFilter maskFilter
         * @param {any} totvsNotification totvsNotification
         * @param {any} i18nFilter i18nFilter
         * @param {any} $resource resource
         * @param {any} eduCalendarioFactory Factory do calendário
         * @param {any} eduUtilsFactory Factory utils
         * @param {any} eduRelatorioService Service para utilizar relatórios
         * @param {any} eduEnumsConsts Constantes e enumerados
         * @param {any} eduAvaliacoesService Service de avaliações
         * @param {any} eduAulasFactory Factory de aulas
         * @param {any} $scope scope
         * @param {any} eduAtividadesExtrasService Service de atividades extras
         * @param {any} $compile compile
         * @param {any} eduWidgetsConsts Constantes dos Widgets
         */
        /* jshint maxparams: false */
        function EduCalendarioController($rootScope,
            $filter,
            state,
            maskFilter,
            totvsNotification,
            i18nFilter,
            $resource,
            eduCalendarioFactory,
            eduUtilsFactory,
            eduRelatorioService,
            eduEnumsConsts,
            eduAvaliacoesService,
            eduPeriodoMatriculaService,
            eduAulasFactory,
            $scope,
            eduAtividadesExtrasService,
            $compile,
            eduWidgetsConsts,
            eduConstantesGlobaisConsts) {
            // *********************************************************************************
            // *** Variáveis
            // *********************************************************************************

            var self = this,
                codRelatorioImpressao = null,
                codColRelatorioImpressao = null;

            self.strFiltro = 'filtro';
            self.strCheckFiltro = 'checkFiltro';
            self.permiteImpressao = false;
            self.periodoLetivo = 0;
            self.filtrosSelecionados = [];
            self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Calendario;

            self.imprimir = imprimir;
            self.aplicarFiltros = aplicarFiltros;
            self.selecionarEvento = selecionarEvento;
            self.periodoLetivoAlterado = periodoLetivoAlterado;
            self.onChangeDisciplina = onChangeDisciplina;
            self.mobile = verificarTipoDispositivo();

            self.Atualizar = function () {

            };

            // *********************************************************************************
            // *** Propriedades públicas e métodos
            // *********************************************************************************

            init();

            // *********************************************************************************
            // *** Inicialização do controller
            // *********************************************************************************

            /* Valida se o usuário tem permissão no Menu */
            var permissionsWatch = $rootScope.$watch('objPermissions', function () {
                if ($rootScope.objPermissions) {
                    if (!$rootScope.objPermissions.EDU_CALENDARIO) {
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-Atencao'),
                            detail: i18nFilter('l-usuario-sem-permissao')
                        });
                        state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                    }

                    //destroy watch
                    permissionsWatch();
                }
            });

            /* Metodo de inicialização do controller */
            function init() {
                retornaCalendario();
                configurarScheduler();
                carregarParametrosEducacional();
            }

            // *********************************************************************************
            // *** Functions
            // *********************************************************************************

            /* Dispara o evento para os controles filhos quando o o periodo letivo for alterado. */
            function periodoLetivoAlterado() {
                $scope.$broadcast('alteraPeriodoLetivoEvent');
            }

            /* Assina o evento de alteração de período */
            $scope.$on('alteraPeriodoLetivoEvent', function () {
                self.refresh = true;
            });

            /**
             * Evento disparado ao alterar a disciplina
             * @param {int} idTurmaDisc - Identficador Turma/Disciplina
             * @param {int} idTurmaDiscMista - Identficador Turma/Disciplina Mista
             */
            function onChangeDisciplina(idTurmaDisc, idTurmaDiscMista) {
                self.idTurmaDiscMista = idTurmaDiscMista;
                aplicarFiltros();
            }

            /* Retorna dados do calendário do aluno */
            function retornaCalendario() {
                self.eventosCalendario = [];

                eduCalendarioFactory.retornaCalendario(function (result) {

                    if (angular.isDefined(result.APPOINTMENTS)) {

                        result.APPOINTMENTS.forEach(function (item) {
                            self.eventosCalendario.push(criarEvento(item));
                        });
                    }

                    // atualiza o datasource do scheduler
                    var dataSource = new kendo.data.SchedulerDataSource({
                        data: self.eventosCalendario
                    });
                    self.instance.setDataSource(dataSource);

                    /*alert(self.instance.options.mobile);*/
                    // preenche o filtro de tipos de eventos
                    self.filtrosTipoEvento = getResourceArray();
                });
            }

            /**
             * Cria um evento para ser adicionado ao dataSouce do scheduler
             *
             * @param {any} item com os dados do evento
             * @returns
             */
            function criarEvento(item) {
                var evento = {
                    id: item.ID + 1, // o evento não pode ter o valor 0
                    title: item.SUBJECT + '. ' + item.DESCRIPTION,
                    description: item.DESCRIPTION,
                    start: new Date(item.STARTTIME),
                    end: new Date(item.ENDTIME),
                    isAllDay: item.ALLDAY,
                    type: parseInt(item.LABEL),
                    IDTURMADISC: item.IDTURMADISC,
                    IDPLANOAULA: item.IDPLANOAULA,
                    CODETAPA: item.CODETAPA,
                    CODPROVA: item.CODPROVA,
                    IDOFERTA: item.IDOFERTA
                };
                return evento;
            }

            /* Aplica o filtro do tipo de evento selecionado pelo usuário */
            function aplicarFiltros() {
                var i,
                    objFiltrosEventos = [],
                    objFiltroDisciplina,
                    filter;

                if (self.instance === undefined) {
                    return;
                }

                for (i = 0; i < self.filtrosSelecionados.length; i++) {
                    objFiltrosEventos.push({ field: 'type', operator: 'eq', value: self.filtrosSelecionados[i] });
                }

                if (self.filtrosSelecionados.length === 0) {
                    // retorna todos os tipos de eventos
                    objFiltrosEventos.push({ field: 'type', operator: 'neq', value: -1 });
                }

                if (self.disciplina !== -1) {
                    objFiltroDisciplina = {
                        logic: 'or',
                        filters: [{ field: 'IDTURMADISC', operator: 'eq', value: self.disciplina },
                        { field: 'IDTURMADISC', operator: 'eq', value: null }]
                    };

                    if (self.idTurmaDiscMista !== null) {
                        // se for turma origem de uma turma mista, retorna também os dados
                        objFiltroDisciplina.filters.push({ field: 'IDTURMADISC', operator: 'eq', value: self.idTurmaDiscMista });
                    }

                }
                else {
                    objFiltroDisciplina = {
                        logic: 'or',
                        filters: [{ field: 'IDTURMADISC', operator: 'eq', value: null },
                        { field: 'IDTURMADISC', operator: 'neq', value: null }]
                    };
                }

                filter = [{
                    logic: 'or',
                    filters: objFiltrosEventos
                },
                    objFiltroDisciplina];

                self.instance.dataSource.filter(filter);
            }

            /**
             * Armazena e executa o filtro pelo tipo de evento selecioando
             *
             * @param {any} idTipoEvento identificador do tipo de evento
             */
            function selecionarEvento(idTipoEvento) {
                if (self.filtrosSelecionados.indexOf(idTipoEvento) === -1) {
                    if (self.filtrosSelecionados.length > 0) {
                        // limpa o CSS do item que estava selecionado
                        $('#leg' + self.filtrosSelecionados[0]).toggleClass('legenda-click-active');
                        // é necessário limpar o filtro porque só é possível filtrar por 1 tipo de evento por vez
                        self.filtrosSelecionados = [];
                    }
                    self.filtrosSelecionados.push(idTipoEvento);
                }
                else {
                    self.filtrosSelecionados = [];
                }

                // altera o CSS do tipo de evento selecionado
                $('#leg' + idTipoEvento).toggleClass('legenda-click-active');

                self.aplicarFiltros();
            }

            /* Configura o scheduler */
            function configurarScheduler() {
                self.views = [
                    'day',
                    'week',
                    { type: 'month', selected: true },
                    'agenda'
                ];

                self.resources = [
                    {
                        field: 'type',
                        title: 'Tipo de evento',
                        dataSource: getResourceArray()
                    }
                ];

                self.editable = {
                    create: false,
                    destroy: false,
                    move: false,
                    resize: false,
                    editRecurringMode: 'occurrence'
                };
            }

            function getResourceArray() {
                var resourcesValues = [];

                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.MatriculaPresencial));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.MatriculaPortal));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.Trancamento));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.Avaliacoes));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.AtividadesInsc));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.Aula));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.EntregaTrab));
                resourcesValues.push(getTipoEvento(eduEnumsConsts.TipoEventoCalendario.LicaoCasa));

                return resourcesValues;
            }

            /**
             * retorna um objeto que representa um tipo de evento para ser usado na
             * legenda ou no resource do scheduler
             *
             * @param {any} idTipoEvento id do tipo de evento
             * @returns retorna um objeto que representa um tipo de evento
             */
            function getTipoEvento(idTipoEvento) {
                var text,
                    value,
                    color,
                    colorCSS;

                /* jshint maxcomplexity: false */
                switch (parseInt(idTipoEvento)) {
                    case eduEnumsConsts.TipoEventoCalendario.MatriculaPresencial:
                        text = i18nFilter('l-matricula-presencial', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.MatriculaPresencial;
                        colorCSS = 'legenda-verde';
                        color = '#c2d6d6';/*@color-legend-01*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.MatriculaPortal:
                        text = i18nFilter('l-matricula-Portal', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.MatriculaPortal;
                        colorCSS = 'legenda-verde-2';
                        color = '#d9f2e6';/*@color-legend-02*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.Trancamento:
                        text = i18nFilter('l-limite-trancamento', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.Trancamento;
                        colorCSS = 'legenda-vermelho';
                        color = '#ff8080';/*@color-legend-03*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.Avaliacoes:
                        text = i18nFilter('l-avaliacoes', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.Avaliacoes;
                        colorCSS = 'legenda-azul';
                        color = '#b3cce6';/*@color-legend-04*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.AtividadesInsc:
                        text = i18nFilter('l-atv-inscritas', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.AtividadesInsc;
                        colorCSS = 'legenda-laranja';
                        color = '#ffd1b3';/*@color-legend-05*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.Aula:
                        text = i18nFilter('l-aula', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.Aula;
                        colorCSS = 'legenda-cinza';
                        color = '#e0e0d1';/*@color-legend-06*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.EntregaTrab:
                        text = i18nFilter('l-entrega-trab', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.EntregaTrab;
                        colorCSS = 'legenda-amarelo';
                        color = '#fff0b3';/*@color-legend-07*/
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.LicaoCasa:
                        text = i18nFilter('l-licao-casa', [], 'js/aluno/calendario');
                        value = eduEnumsConsts.TipoEventoCalendario.LicaoCasa;
                        colorCSS = 'legenda-azul-2';
                        color = '#cceeff';/*@color-legend-08*/
                        break;
                }

                return {
                    text: text,
                    value: value,
                    color: color,
                    colorCSS: colorCSS
                };
            }

            self.onEventEdit = function (e) {
                /* jshint maxcomplexity: false */
                e.preventDefault();

                switch (e.event.type) {
                    case eduEnumsConsts.TipoEventoCalendario.Aula:
                        if (e.event.IDPLANOAULA !== null) {
                            eduAulasFactory.exibirResumoAula(e.event.IDTURMADISC, e.event.IDPLANOAULA);
                        }
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.LicaoCasa:
                        if (e.event.IDPLANOAULA !== null) {
                            eduAulasFactory.exibirResumoAula(e.event.IDTURMADISC, e.event.IDPLANOAULA, null, true);
                        }
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.Avaliacoes:
                        eduAvaliacoesService.exibirDetalhesAvaliacao(e.event.IDTURMADISC, e.event.CODETAPA,
                            eduEnumsConsts.TipoEtapa.Nota, e.event.CODPROVA);
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.EntregaTrab:
                        eduAvaliacoesService.exibirDetalhesAvaliacao(e.event.IDTURMADISC, e.event.CODETAPA,
                            eduEnumsConsts.TipoEtapa.Nota, e.event.CODPROVA);
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.AtividadesInsc:
                        eduAtividadesExtrasService.exibirInformacoesAtividadeInscrita(e.event.IDOFERTA);
                        break;
                    case eduEnumsConsts.TipoEventoCalendario.MatriculaPresencial:
                    case eduEnumsConsts.TipoEventoCalendario.MatriculaPortal:
                    case eduEnumsConsts.TipoEventoCalendario.Trancamento:
                        eduPeriodoMatriculaService.exibirDetalhesPeriodoMatricula();
                        break;
                }
            };

            /* Carrepar parâmetros do TOTVS Educacional */
            function carregarParametrosEducacional() {
                eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (parametros) {
                    codRelatorioImpressao = parametros.CodRelatorioCalendarioReports;
                    codColRelatorioImpressao = parametros.CodColRelatorioCalendarioReports;
                    self.permiteImpressao = !!parametros.CodRelatorioCalendarioReports;                    
                    self.itensCalendario = parametros.ItensCalendarioAluno;

                    configurarLegendas();
                });
            }

            /* Cria a legenda dinâmica */
            function configurarLegendas() {
                var legenda = '',
                    itemLegenda,
                    index,
                    fechouTagLegenda = true,
                    arrItensCalendario = self.itensCalendario.split(';');

                if (arrItensCalendario[arrItensCalendario.length - 1] === '') {
                    arrItensCalendario.pop();
                }

                for (index = 0; index < arrItensCalendario.length; index++) {

                    // divide os itens em 2 linhas (atualmente o máximo são 8 itens)
                    if (index === 0 || index === 4) {
                        legenda += '<totvs-page-tags class="ng-scope">';
                        fechouTagLegenda = false;
                    }

                    itemLegenda = getTipoEvento(arrItensCalendario[index], true);
                    itemLegenda.index = index;

                    legenda += getTemplateItemLegenda(itemLegenda);

                    // fecha a tag "totvs-page-tags" para dividir os itens em 2 linhas
                    if (index === 3) {
                        legenda += '</totvs-page-tags>';
                        fechouTagLegenda = true;
                    }
                }

                if (!fechouTagLegenda) {
                    legenda += '</totvs-page-tags>';
                    fechouTagLegenda = true;
                }

                $('#divLegenda').empty();

                compilarLegendas(legenda);
            }

            /**
             * Retorna o HTML de cada item da legenda
             *
             * @param {any} itemLegenda item com os dados da legenda
             * @returns
             */
            function getTemplateItemLegenda(itemLegenda) {
                return '<tag id="leg' + itemLegenda.value + '" class="legend legenda-click ' + itemLegenda.colorCSS + ' legenda-content-' + (itemLegenda.index + 1) +
                       '" ng-click="controller.selecionarEvento(' + itemLegenda.value + ')">' + itemLegenda.text + '</tag>';
            }

            /**
             * Compila as strings com o html das legendas dinâmicas
             *
             * @param {any} htmlLegenda string com as tags das legendas dinâmicas
             */
            function compilarLegendas(htmlLegenda) {
                // cria o elemento através do angular.
                var elmLegenda = angular.element(htmlLegenda);

                // insere o elemento na div
                elmLegenda = $('#divLegenda').append(elmLegenda);

                // compila o elemento na tela.
                elmLegenda = $compile(elmLegenda)($scope);
            }

            /* Executa emissão do relatório associado a visão(Parametrizado no sistema). */
            function imprimir() {
                if (codRelatorioImpressao) {
                    eduRelatorioService.emitirRelatorio(codRelatorioImpressao, codColRelatorioImpressao);
                }
            }

            /**
             * Função que retorna se o dispositivo é tablet
             *
             * @returns tablet or false
             */
            function verificarTipoDispositivo() {
                if (navigator.userAgent.match(/Tablet|iPad|Mobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Opera Mini|\bCrMo\/|Opera Mobi/i)) {
                    return 'tablet';
                } else {
                    return false;
                }
            }
        }
    });
