[{
    "l-home": {
        "pt": "Início",
		"en": "Start",
		"es": "Inicio"
    },
    "l-titulo": {
        "pt": "Calendário",
		"en": "Calendar",
		"es": "Calendario"
    },
    "l-matricula-presencial": {
        "pt": "Período de matrícula presencial",
		"en": "Period of enrollment by attendance",
		"es": "Período de matrícula presencial"
    },
    "l-matricula-Portal": {
        "pt": "Período de matrícula no Portal",
		"en": "Enrollment period in the Portal",
		"es": "Período de matrícula en el Portal"
    },
    "l-limite-trancamento": {
        "pt": "Data limite de trancamento",
		"en": "Limit date of leave of absence",
		"es": "Fecha límite de aplazamiento de matrícula"
    },
    "l-avaliacoes": {
        "pt": "Datas de avaliação/provas",
		"en": "Evaluation/test dates",
		"es": "Fecha de evaluación/pruebas"
    },
    "l-atv-inscritas": {
        "pt": "Atividades inscritas",
		"en": "Activities registered",
		"es": "Actividades inscritas"
    },
    "l-aula": {
        "pt": "Aulas",
		"en": "Classes",
		"es": "Clases"
    },
    "l-entrega-trab": {
        "pt": "Data limite de entrega de trabalho",
		"en": "Limit date to hand the paper in",
		"es": "Fecha límite de entrega de trabajo"
    },
    "l-licao-casa": {
        "pt": "Datas de Lição de Casa",
		"en": "Homework Dates",
		"es": "Fecha de lección de casa"
    },
    "l-filtro-tipos-evento": {
        "pt": "Eventos",
		"en": "Events",
		"es": "Eventos"
    },
    "l-widget-title": {
      "pt": "Painel do Aluno",
	  "en": "Student's Panel",
	  "es": "Panel del alumno"
    }
}]
