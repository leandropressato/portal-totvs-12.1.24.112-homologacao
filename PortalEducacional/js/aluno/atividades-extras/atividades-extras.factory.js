/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduAtividadesExtrasModule
 * @name eduAtividadesExtrasFactory
 * @object factory
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @dependencies $totvsresource
 *
 * @description Factory utilizada nas Ocorrencias.
 */
define(['aluno/atividades-extras/atividades-extras.module'], function () {

    'use strict';

    angular
        .module('eduAtividadesExtrasModule')
        .factory('eduAtividadesExtrasFactory', EduAtividadesExtrasFactory);

    EduAtividadesExtrasFactory.$inject = ['$totvsresource'];

    function EduAtividadesExtrasFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.getAtividadesExtras = getAtividadesExtras; // Busca todas as ocorrencias
        factory.realizaInscricaoAtividade = realizaInscricaoAtividade; // Atualiza uma ocorrencia
        factory.getAtividadesInscritas = getAtividadesInscritas;
        factory.deleteAtividadesInscritas = deleteAtividadesInscritas;
        factory.getAtividadeInscrita = getAtividadeInscrita;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function getAtividadesExtras(startAt, limitAt, callback) {
            var pStart = startAt || 0,
                pLimit = limitAt || 0,
                parameters = {};

            parameters['method'] = 'AtividadesOfertadas';
            parameters['start'] = pStart;
            parameters['limit'] = pLimit;

            return factory.TOTVSQuery(parameters, callback);
        }

        function realizaInscricaoAtividade(model, callback) {
            var parameters = {};
            parameters['method'] = 'InscricaoAtividade';
            parameters['codColigadaAtiv'] = model.CODCOLIGADA;
            parameters['idAtivOfertada'] = model.IDOFERTA;

            return factory.TOTVSSave(parameters, model, callback);
        }

        /**
         * Retorna as atividades inscritas pelo aluno
         *
         * @param {any} callback - function de retorno
         * @returns dados das atividades inscritas
         */
        function getAtividadesInscritas(callback) {
            var parameters = {};
            parameters['method'] = 'AtividadesInscritas';

            return factory.TOTVSQuery(parameters, callback);
        }

        /**
         * Retorno Atividade Inscrita pelo aluno
         *
         * @param {Number} idOferta - Id. Oferta Inscrita
         * @param {Function} callback - Função de retorno
         * @returns TOTVSQuery
         */
        function getAtividadeInscrita(idOferta, callback) {
            var fac = $totvsresource.REST(CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/AtividadeInscrita/:idOferta', {}, {});
            return fac.TOTVSQuery({'idOferta': idOferta}, callback);
        }

        /**
         * Cancela a inscrição do aluno na atividade
         *
         * @param {any} model - dados para cancelamento
         * @param {any} callback - função callback
         * @returns
         */
        function deleteAtividadesInscritas(model, callback) {
            var parameters = {};
            parameters['method'] = 'AtividadesInscritas';
            parameters['codColigadaAtividade'] = model.CODCOLIGADA;
            parameters['idAtividadeInscrita'] = model.IDATIVIDADE;

            return factory.TOTVSRemove(parameters, callback);
        }
    }
});
