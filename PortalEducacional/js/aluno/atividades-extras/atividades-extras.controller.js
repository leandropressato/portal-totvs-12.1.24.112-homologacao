/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduAtividadesExtrasModule
 * @name EduAtividadesExtrasController
 * @object controller
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @requires ocorrencias.module
 *
 * @dependencies eduOcorrenciasFactory
 *
 * @description Controller do Ocorrencias
 */
define(['aluno/atividades-extras/atividades-extras.module',
    'aluno/atividades-extras/atividades-extras.factory',
    'utils/edu-enums.constants',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'
], function () {

    'use strict';

    angular
        .module('eduAtividadesExtrasModule')
        .controller('EduAtividadesExtrasController', EduAtividadesExtrasController);

    EduAtividadesExtrasController.$inject = ['$rootScope', '$state', 'eduWidgetsConsts', 'eduConstantesGlobaisConsts', 'totvs.app-notification.Service', 'i18nFilter'];

    function EduAtividadesExtrasController($rootScope, $state, eduWidgetsConsts, eduConstantesGlobaisConsts, totvsNotification, i18nFilter) {

        var self = this;
        // variaveis
        self.exibeTab = exibeTab;
        self.activeTabInscritas = false;
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.AtividadesCurriculares;
        // metodos
        init();

        /**
         * Valida se o usuário tem permissão no Menu
         */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_OPORTUNIDADES_ATIVIDADESEXTRAS) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        /**
         * init da funcionalidade
         */
        function init() {
            if ($state.params.tab === 'inscritas') {
                self.activeTabInscritas = true;
            }
        }

        /**
         * Valida a permissão para cada Tab
         *
         * @param {string} tabName - Nome da tab
         * @returns True or False
         */
        function exibeTab(tabName) {
            var exibe = false;

            if ($rootScope.objPermissions === null) {
                return exibe;
            }

            switch (tabName) {
                case 'inscricao':
                    exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_OPORTUNIDADES_ATIVIDADESEXTRAS_INSCREVER);
                    break;
                case 'inscritas':
                    exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_OPORTUNIDADES_ATIVIDADESEXTRAS_INSCRITAS);
                    break;
            }
            return exibe;
        }
    }
});
