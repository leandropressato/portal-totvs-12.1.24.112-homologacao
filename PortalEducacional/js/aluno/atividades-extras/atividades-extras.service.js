/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduAtividadesExtrasModule
 * @name eduAtividadesExtrasService
 * @object service
 *
 * @created 2016-11-18 v12.1.15
 * @updated
 *
 * @requires -
 *
 * @description Service utilizado para tarefas comuns a inscrição e inscritos
 */

define(['aluno/atividades-extras/atividades-extras.module',
        'aluno/atividades-extras/atividades-extras.factory'], function () {

    'use strict';

    angular
        .module('eduAtividadesExtrasModule')
        .service('eduAtividadesExtrasService', EduAtividadesExtrasService);

    EduAtividadesExtrasService.$inject = ['$filter', '$modal', 'eduAtividadesExtrasFactory'];

    function EduAtividadesExtrasService($filter, $modal, eduAtividadesExtrasFactory) {

        this.objComboComponentes = objComboComponentes;
        this.formataLabelComboComponentes = formataLabelComboComponentes;
        this.exibirInformacoesAtividadeInscrita = exibirInformacoesAtividadeInscrita;

        /**
         * Retorna um objeto contendo os dados dos componentes para criar o filtro de componentes (combobox)
         *
         * @param {any} data - objeto com os dados de atividades extras
         * @param {any} callback - retorna o resultado no callback
         */
        function objComboComponentes(data, callback) {

            var result = [],
                tempData = [];

            data = $filter('orderBy')(data, 'DESCCOMPONENTE');

            if (angular.isDefined(data) && data.length > 0) {
                angular.forEach(data, function (item) {
                    var existe = $filter('filter')(tempData, { 'CODCOMPONENTE': item.CODCOMPONENTE }, true);
                    if (existe.length === 0) {
                        tempData.push({
                            'CODCOMPONENTE': item.CODCOMPONENTE,
                            'DESCCOMPONENTE': item.DESCCOMPONENTE,
                            'TOTAL': $filter('filter')(data, { 'CODCOMPONENTE': item.CODCOMPONENTE }, true).length
                        });
                    }
                });
                result.push({
                    'CODCOMPONENTE': -1,
                    'DESCCOMPONENTE': $filter('i18n')('l-todos-componentes', [], 'js/aluno/atividades-extras'),
                    'TOTAL': data.length
                });

                result = result.concat(tempData);
            }

            if (typeof callback === 'function') {
                callback(result);
            }
        }


        /**
         * Formata o option do combo de componentes
         *
         * @param {any} obj - values do option
         * @returns option label formatado
         */
        function formataLabelComboComponentes(obj) {
            var retorno = '';
            if (obj.CODCOMPONENTE === -1) {
                retorno = obj.DESCCOMPONENTE;
            } else {
                retorno = obj.DESCCOMPONENTE + ' (' + obj.TOTAL + ')';
            }
            return retorno;
        }

        /**
         * Exibir informações sobre a atividade inscrita pelo aluno(Modal).
         *
         * @param {Number} idOferta - Id. Atividade Ofertada
         */
        function exibirInformacoesAtividadeInscrita(idOferta) {
            eduAtividadesExtrasFactory.getAtividadeInscrita(idOferta, function (result) {
                if (angular.isDefined(result)) {
                    $modal.open({
                                templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/atividades-extras/inscritas/atividades-extras-inscritas-detalhes.view.html',
                                controller: 'AtividadeExtraDetalhesController',
                                controllerAs: 'controller',
                                size: 'md',
                                resolve: {
                                    atividade: function () {
                                        return result[0];
                                    }
                                },
                                backdrop: 'true'
                            });
                };
            });
        }
    }
});
