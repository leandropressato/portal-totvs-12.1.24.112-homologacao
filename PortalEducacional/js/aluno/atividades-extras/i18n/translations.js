[
    {
        "l-atividades-inscritas": {
            "pt": "Atividades Inscritas",
			"en": "Registered Activities",
			"es": "Actividades inscritas"
        },
        "l-titulo":{
            "pt": "Atividades Curriculares",
			"en": "Curricular Activities",
			"es": "Actividades curriculares"
        },
        "l-home":{
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-inscricao": {
            "pt": "Inscrever",
			"en": "Register",
			"es": "Inscribir"
        },
        "l-em-andamento": {
            "pt": "Em andamento",
			"en": "In progress",
			"es": "En proceso"
        },
        "l-pendente": {
            "pt": "Pendente",
			"en": "Pending",
			"es": "Pendiente"
        },
        "l-cancelado": {
            "pt": "Cancelado",
			"en": "Canceled",
			"es": "Anulado"
        },
        "l-cancelar": {
            "pt": "Cancelar Inscrição",
			"en": "Cancel Registration",
			"es": "Anular inscripción"
        },
        "l-componente": {
            "pt": "Componente",
			"en": "Save",
			"es": "Componente"
        },
        "l-modalidade": {
            "pt": "Modalidade",
			"en": "Modality",
			"es": "Modalidad"
        },
        "l-data-inicial-insc": {
            "pt": "Início da inscrição",
			"en": "Start of registration",
			"es": "Inicio de la inscripción"
        },
        "l-data-final-insc": {
            "pt": "Término da inscrição",
			"en": "End of registration",
			"es": "Final de la inscripción"
        },
        "l-carga-horaria": {
            "pt": "Carga horária",
			"en": "Workload",
			"es": "Carga horaria"
        },
        "l-creditos": {
            "pt": "Créditos",
			"en": "Credits",
			"es": "Créditos"
        },
        "l-data-inicial": {
            "pt": "Início atividade",
			"en": "Activities start",
			"es": "Inicio actividad"
        },
        "l-data-final": {
            "pt": "Término atividade",
			"en": "Activity end",
			"es": "Final actividad"
        },
        "l-data-ativ-inicial": {
            "pt": "Início atividade do aluno",
			"en": "Student's activity start",
			"es": "Inicio actividad del alumno"
        },
        "l-data-ativ-final": {
            "pt": "Término atividade do aluno",
			"en": "Student's activity end",
			"es": "Final actividad del alumno"
        },
        "l-local-insc": {
            "pt": "Local da inscrição",
			"en": "Location of registration",
			"es": "Local de la inscripción"
        },
        "l-local-ativ": {
            "pt": "Local da atividade",
			"en": "Location of activity",
			"es": "Local de la actividad"
        },
        "l-carga-horaria-aproveitada": {
            "pt": "Carga horária aproveitada",
			"en": "Workload enjoyed",
			"es": "Carga horaria aprovechada"
        },
        "l-conteudo": {
            "pt": "Conteúdo",
			"en": "Content",
			"es": "Contenido"
        },
        "l-observacao": {
            "pt": "Observação",
			"en": "Note",
			"es": "Observación"
        },
        "l-nenhum-registro-encontrado": {
            "pt": "Nenhum registro encontrado!",
			"en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },
         "l-em-aberto": {
            "pt": "Aberto para inscrições",
			"en": "Opened for registrations",
			"es": "Abierto para inscripciones"
        },
        "l-ultimas-vagas": {
            "pt": "Últimas vagas",
			"en": "Last vacancies",
			"es": "Últimas vacantes"
        },
        "l-ultimos-dias": {
            "pt": "Últimos dias",
			"en": "Last days",
			"es": "Últimos días"
        },
        "l-vagas-esgotadas": {
            "pt": "Vagas esgotadas",
			"en": "Vacancies exhausted",
			"es": "Vacantes agotadas"
        },
        "l-todos-componentes":{
            "pt": "TODOS os componentes",
			"en": "ALL the components",
			"es": "TODOS los componentes"
        },
        "l-atividades-disponiveis": {
            "pt": "Atividades Disponíveis",
			"en": "Available Activities",
			"es": "Actividades disponibles"
        },
        "l-local-atv": {
            "pt": "Local da atividade",
			"en": "Location of activity",
			"es": "Local de la actividad"
        },
        "l-msg-confirmacao-inscricao":{
            "pt": "Confirma sua inscrição na atividade?",
			"en": "Confirm your registration in the activity?",
			"es": "¿Confirma su inscripción en la actividad?"
        },
        "l-msg-confirmacao-cancelamento":{
            "pt": "Confirma o cancelamento de participação na atividade?",
			"en": "Confirm the cancelation of the participation in the activity?",
			"es": "¿Confirma la anulación de participación en la actividad?"
        },
        "l-msg-inscricao-sucesso":{
            "pt": "Inscrição realizada com sucesso!",
			"en": "Registration successfully made",
			"es": "¡Inscripción realizada con éxito!"
        },
         "l-num-vagas":{
            "pt": "Vagas",
			"en": "Vacancies",
			"es": "Vacantes"
        },
         "l-nao-iniciada":{
            "pt": "Não iniciada",
			"en": "Not started",
			"es": "No iniciada"
        },
         "l-cancelamento-ok":{
            "pt": "O cancelamento da inscrição foi efetuado com sucesso!",
			"en": "The cancelation of the registration was successfully made!",
			"es": "¡La anulación de la inscripción se realizó con éxito!"
        },
         "l-cancelamento-falha":{
            "pt": "Falha ao tentar efetuar o cancelamento da inscrição!",
			"en": "Failure trying to cancel the registration!",
			"es": "¡Falla al intentar efectuar la anulación de la inscripción!"
        },
         "l-concluida":{
            "pt": "Concluída",
			"en": "Concluded",
			"es": "Concluida"
        },
        "l-data-conclusao": {
            "pt": "Data de conclusão",
			"en": "Conclusion date",
			"es": "Fecha de conclusión"
        },
        "l-descricao": {
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "btn-mais-informacoes": {
            "pt": "Mais informações",
			"en": "Further information",
			"es": "Más información"
        },
        "l-local": {
            "pt": "Local",
			"en": "Location",
			"es": "Local"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        }
    }
]
