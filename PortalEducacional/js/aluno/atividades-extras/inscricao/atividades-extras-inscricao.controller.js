/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduAtividadesExtrasModule
 * @name EduAtividadesExtrasController
 * @object controller
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @requires eduAtividadesExtras.module
 *
 * @dependencies eduAtividadesExtrasFactory
 *
 * @description Controller do Ocorrencias
 */
define(['aluno/atividades-extras/atividades-extras.module',
    'aluno/atividades-extras/atividades-extras.factory',
    'aluno/atividades-extras/atividades-extras.service',
    'utils/edu-enums.constants'
], function () {

    'use strict';

    angular
        .module('eduAtividadesExtrasModule')
        .controller('EduAtividadesExtrasInscricaoController', EduAtividadesExtrasInscricaoController);

    EduAtividadesExtrasInscricaoController.$inject = [
        '$rootScope',
        'eduAtividadesExtrasService',
        'eduAtividadesExtrasFactory',
        'i18nFilter',
        'totvs.app-notification.Service',
        '$filter',
        '$state'
    ];

    /**
     * Controller Atividades Extras - Inscrição
     *
     * @param {object} $rootScope  Trafega informações padrão do sistema.
     * @param {object} eduAtividadesExtrasService  Service Atividades Extras
     * @param {object} eduAtividadesExtrasFactory Factory Atividades Extras
     * @param {object} i18nFilter filtro
     * @param {object} totvsNotification serviços de notificação
     * @param {object} $filter filtro
     * @param {object} $state state da app
     */
    function EduAtividadesExtrasInscricaoController($rootScope,
        eduAtividadesExtrasService,
        eduAtividadesExtrasFactory,
        i18nFilter,
        totvsNotification,
        $filter,
        $state) {

        var self = this;

        // Objetos e variáveis
        self.objAtividadesExtrasList = [];
        self.objAtividadesExtrasListFiltrado = [];
        self.intAtividadesExtrasCount = 0;
        self.model = {};
        self.tipoAtividade = -1;
        self.listAtividadesDisponiveis = [];

        // métodos
        self.filtrarComponente = filtrarComponente;
        self.search = search;
        self.getAtividadesExtras = getAtividadesExtras;
        self.atualizarInformacoesCombo = atualizarInformacoesCombo;
        self.inscrever = inscrever;
        self.finalizarInscricao = finalizarInscricao;
        self.legendaClass = legendaClass;
        self.formataComboComponentes = formataComboComponentes;

        init();

        /**
         * Inicialia as informações da tela
         */
        function init() {
            search(false);
        }

        /**
         * Realiza a busca pelos dados da tela
         *
         * @param {bool} isMore Parâmetro que define a paginação
         */
        function search(isMore) {
            var startAt = 0;

            self.intAtividadesExtrasCount = 0;

            if (isMore) {
                startAt = self.objAtividadesExtrasList.length;
            } else {
                self.objAtividadesExtrasList = [];
            }

            self.getAtividadesExtras(startAt);
        }

        /**
         * Filtra as atividades pelo componente selecionado
         */
        function filtrarComponente() {
            if (self.tipoAtividade !== -1) {
                self.objAtividadesExtrasListFiltrado = $.grep(self.objAtividadesExtrasList, function (e) {
                    return e.CODCOMPONENTE === self.tipoAtividade;
                });
            } else {
                self.objAtividadesExtrasListFiltrado = self.objAtividadesExtrasList;
            }
            self.objAtividadesExtrasListFiltrado = $filter('orderBy')(self.objAtividadesExtrasListFiltrado, '-[ULTIMASVAGAS, ULTIMOSDIASINSC, VAGASEMABERTO]');
        }

        /**
         * Define qual legenda será aplicada por atividade
         *
         * @param {any} model
         * @returns
         */
        function legendaClass(model) {
            if (model) {
                if (Boolean(model.VAGASESGOTADAS)) {
                    return 'legenda-vermelho legenda-content-4';
                } else if (Boolean(model.ULTIMASVAGAS)) {
                    return 'legenda-laranja legenda-content-2';
                } else if (Boolean(model.ULTIMOSDIASINSC)) {
                    return 'legenda-amarelo legenda-content-3';
                } else {
                    return 'legenda-verde legenda-content-1';
                }
            }
        }

        /**
         *  Busca os registros de Atividades Extras
         *
         * @param {int} startAt Indica a paginação
         */
        function getAtividadesExtras(startAt) {

            eduAtividadesExtrasFactory.getAtividadesExtras(startAt, self.limitAt, function (result) {
                if (result) {
                    angular.forEach(result, function (value) {
                        if (value && value.$length) {
                            self.intAtividadesExtrasCount = value.$length;
                        }
                        self.objAtividadesExtrasList.push(value);
                    });

                    self.objAtividadesExtrasListFiltrado = $filter('orderBy')(self.objAtividadesExtrasList, '-[ULTIMASVAGAS, ULTIMOSDIASINSC, VAGASEMABERTO]');
                    self.atualizarInformacoesCombo();
                }
            });
        }

        /**
         * Atualiza a lista do combo de componentes
         */
        function atualizarInformacoesCombo() {
            eduAtividadesExtrasService.objComboComponentes(self.objAtividadesExtrasList,
                function (values) {
                    self.listAtividadesDisponiveis = [];
                    angular.forEach(values, function (value) {
                        self.listAtividadesDisponiveis.push(value);
                    });
                });
        }

        /**
         * Evento para inscrever na atividade selecionada
         *
         * @param {any} model dados da atividade
         */
        function inscrever(model) {
            totvsNotification.question({
                title: i18nFilter('l-Confirmacao'),
                text: i18nFilter('l-msg-confirmacao-inscricao', [], 'js/aluno/atividades-extras'),
                cancelLabel: 'l-no',
                size: 'md', // sm = small | md = medium | lg = larger
                confirmLabel: 'l-yes',
                callback: function (isPositiveResult) {
                    if (isPositiveResult) {
                        self.finalizarInscricao(model);
                    }
                }
            });
        }

        /**
         * Finaliza a inscrição
         *
         * @param {any} model dados de atividades
         */
        function finalizarInscricao(model) {
            eduAtividadesExtrasFactory.realizaInscricaoAtividade(model, function (result) {
                if (result) {
                    if (angular.isDefined(result.exception)) {
                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-titulo', [], 'js/aluno/atividades-extras'),
                            detail: result.message
                        });
                    } else {
                        // recarrega a rota
                        reload();
                        totvsNotification.notify({
                            type: 'success',
                            title: i18nFilter('l-titulo', [], 'js/aluno/atividades-extras'),
                            detail: result.message
                        });
                    }
                }
            });
        }

        /**
         * Formata o option do combo de componentes
         *
         * @param {any} item - values do option
         * @returns option label formatado
         */
        function formataComboComponentes (item) {
            return eduAtividadesExtrasService.formataLabelComboComponentes(item);
        }

        /**
         * Recarrega a rota
         */
        function reload() {
            $state.go($state.current, { tab: '' }, { reload: true });
        }
    }
});
