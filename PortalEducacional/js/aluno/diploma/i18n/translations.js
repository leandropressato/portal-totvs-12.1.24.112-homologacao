[
  {
    "l-nomeCurso": {
      "pt": "Curso",
      "en": "Curso",
      "es": "Curso"
    },
    "l-nomeExpeditora": {
      "pt": "Expeditora",
      "en": "Expeditora",
      "es": "Expeditora"
    },    
    "l-cpf": {
      "pt": "CPF",
      "en": "CPF",
      "es": "CPF"
    },
    "l-eMECCurso": {
      "pt": "e-MEC Curso",
      "en": "e-MEC Curso",
      "es": "e-MEC Curso"
    },
      "l-eMECExpeditora": {
      "pt": "e-MEC Expeditora",
      "en": "e-MEC Expeditora",
      "es": "e-MEC Expeditora"
    },
    "l-eMECRegistradora": {
      "pt": "e-MEC registradora",
      "en": "e-MEC registradora",
      "es": "e-MEC registradora"
    },
      "l-nomeAluno": {
      "pt": "Aluno",
      "en": "Aluno",
      "es": "Aluno"
    },
    "l-registradora": {
      "pt": "Registradora",
      "en": "Registradora",
      "es": "Registradora"
    },
    "l-dataIngresso": {
      "pt": "Data ingresso",
      "en": "Data ingresso",
      "es": "Data ingresso"
    },
    "l-dataConclusao": {
      "pt": "Data conclusão",
      "en": "Data conclusão",
      "es": "Data conclusão"
    },
    "l-dataExpedicao": {
      "pt": "Data expedição",
      "en": "Data expedição",
      "es": "Data expedição"
    },
    "l-instituicao": {
      "pt": "Instituição",
      "en": "Instituição",
      "es": "Instituição"
    },
    "l-dataPublicacaoDOU": {
      "pt": "Data publicação DOU",
      "en": "Data publicação DOU",
      "es": "Data publicação DOU"
    },
    "l-numeroRegistro": {
      "pt": "Número do registro",
      "en": "Número do registro",
      "es": "Número do registro"
    },   
    "l-nProcessoRegistro": {
      "pt": "N. processo registro",
      "en": "N. processo registro",
      "es": "N. processo registro"
    },             
    "l-dataRegistro": {
      "pt": "Data registro",
      "en": "Data registro",
      "es": "Data registro"
    }
  }
]