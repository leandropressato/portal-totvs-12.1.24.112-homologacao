(function () {
    'use strict';

    var modulo = angular.module('eduDiplomaModule', ['ngResource', 'ui.select', 'ngSanitize', 'totvsHtmlFramework', 'vcRecaptcha', 'ui.mask']);

    modulo.controller('eduDiplomaController', EduDiplomaController);
    modulo.config(totvsAppConfig);

    EduDiplomaController.$inject = ['$sce', '$totvsresource', 'i18nFilter', '$window', '$rootScope'];
    function EduDiplomaController($sce, $totvsresource, i18nFilter, $window, $rootScope) {

        var self = this;        

        self.Submitted = false;
        self.ListaCampos = [];
        self.listaResultado = [];
        self.CampoSelecionado = {};
        self.ValorPesquisa = "";
        self.recaptchaResponse = "";
        self.mask = '';
        self.keyRecaptcha = KEY_RECAPTCHA;

        self.buscar = buscar;
        self.voltar = voltar;
        self.retornarConsultaDiploma = retornarConsultaDiploma;
        self.setMask = setMask;
        self.nenhumResultado = false;

        init();

        function init() {

            //Configurar idioma padrão da tradução
            $rootScope.currentuser = { dialect: EDU_CONST_GLOBAL_CUSTOM_IDIOMA };
            carregarCamposBusca();            
        }

        // Monitora o $rootScope.pendingRequests para apresentar ou esconder o
        // backdrop de loading. Este controle é realizado através do appHTTPInterceptor
        // que monitora as requisições ao servidor.
        this.hasPendingRequests = function() {
            return ($rootScope.pendingRequests > 0);
        };

        function carregarCamposBusca()
        {
            if (EDU_CONST_CONSULTA_PUBLICA_DIPLOMA)
            {
                if (EDU_CONST_CONSULTA_PUBLICA_DIPLOMA.Nome)
                {
                    var obj = {};
                    obj.Codigo = 0;
                    obj.Descricao  = i18nFilter('l-Nome');
                    obj.Mask = "";

                    self.ListaCampos.push(obj);
                }

                if (EDU_CONST_CONSULTA_PUBLICA_DIPLOMA.CPF)
                {
                    var obj = {};
                    obj.Codigo = 1;
                    obj.Descricao = i18nFilter('l-CPF');
                    obj.Mask = "999.999999-99";

                    self.ListaCampos.push(obj)
                }
                
                if (EDU_CONST_CONSULTA_PUBLICA_DIPLOMA.Registro)
                {
                    var obj = {};
                    obj.Codigo = 2;
                    obj.Descricao = i18nFilter('l-Registro');
                    obj.Mask = "";

                    self.ListaCampos.push(obj)
                }  

                if (EDU_CONST_CONSULTA_PUBLICA_DIPLOMA.Processo)
                {
                    var obj = {};
                    obj.Codigo = 3;
                    obj.Descricao = i18nFilter('l-Processo');
                    obj.Mask = "";

                    self.ListaCampos.push(obj)
                }  
                 
                self.CampoSelecionado = 0;
            }
        }

        function setMask() {
            self.Mask = self.ListaCampos[self.CampoSelecionado].Mask;
            $('#ValorPesquisa').val('');
            self.ValorPesquisa = '';
        }

        function buscar() {
            self.Submitted = true;

            retornarConsultaDiploma(self.CampoSelecionado, self.ValorPesquisa, self.recaptchaResponse, function (result) {

                if (result) {
                    self.listaResultado = result;
                    self.nenhumResultado = result.length == 0;
                }
                else
                {
                    self.Submitted = false;
                    self.listaResultado = [];
                    self.nenhumResultado = true;
                }
            });
        }

        function voltar() {
            self.Submitted = false;
            self.listaResultado = [];
            self.CampoSelecionado = 0;
            self.ValorPesquisa = '';
            self.Mask = '';
        }

        function retornarConsultaDiploma(
            campoBusca,
            valorBusca,
            recaptchaResponse,
            callback) {

            var parameters = {
                method: 'ConsultaDiploma',
                campoBusca: campoBusca,
                valorBusca: valorBusca,
                recaptchaResponse: recaptchaResponse
            },

            urlServicos = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/:method',
            factory = $totvsresource.REST(urlServicos, {}, {});

            return factory.TOTVSQuery(parameters, callback);
        }

        function RecaptchaSucesso(response)
        {
        }
    }

    totvsAppConfig.$inject = ['$httpProvider', 'TotvsI18nProvider'];
    function totvsAppConfig($httpProvider, TotvsI18nProvider) {
        $httpProvider.interceptors.push('totvsHttpInterceptor');
        TotvsI18nProvider.setBaseContext(EDU_CONST_GLOBAL_URL_BASE_APP);
    }

    angular.bootstrap(document, ['eduDiplomaModule']);
}());
