[{
    "l-classificacao-avaliacao": {
        "pt": "Classificação",
		"en": "Classification",
		"es": "Clasificación"
    },
    "l-situacaoMatricula": {
        "pt": "Sit. Matrícula",
		"en": "Enrollment Status",
		"es": "Sit. Matrícula"
    },
    "l-situacaoResultado": {
        "pt": "Sit. Resultado:",
		"en": "Result Status",
		"es": "Sit. Resultado:"
    },
    "l-home": {
        "pt": "Notas",
		"en": "Score",
		"es": "Notas"
    },
    "l-todas-disciplinas": {
        "pt": "Todas as disciplinas",
		"en": "All course subjects",
		"es": "Todas las materias"
    },
    "l-titulo": {
        "pt": "Avaliações",
		"en": "Evaluations",
		"es": "Evaluaciones"
    },
    "l-avaliacao": {
        "pt": "Avaliação",
		"en": "Evaluation",
		"es": "Evaluación"
    },
    "l-avaliacao-descritiva": {
        "pt": "Avaliação descritiva",
		"en": "Descriptive evaluation",
		"es": "Evaluación descriptiva"
    },
    "l-etapa": {
        "pt": "Etapa",
		"en": "Stage",
		"es": "Etapa"
    },
    "l-codEtapa": {
        "pt": "Cód. Etapa",
		"en": "Stage Code",
		"es": "Cód. Etapa"
    },
    "l-data": {
        "pt": "Data",
		"en": "Date",
		"es": "Fecha"
    },
    "l-atividade": {
        "pt": "Atividade",
		"en": "Activity",
		"es": "Actividad"
    },
    "l-disciplina": {
        "pt": "Disciplina",
		"en": "Course discipline",
		"es": "Materia"
    },
    "l-valor": {
        "pt": "Valor",
		"en": "Value",
		"es": "Valor"
    },
    "l-nota": {
        "pt": "Nota",
		"en": "Score",
		"es": "Nota"
    },
    "l-media": {
        "pt": "Média",
		"en": "Average",
		"es": "Promedio"
    },
    "l-detalhes": {
        "pt": "Detalhes",
		"en": "Details",
		"es": "Detalles"
    },
    "l-acimaMedia": {
        "pt": "Acima da média",
		"en": "Above average",
		"es": "Superior al promedio"
    },
    "l-abaixoMedia": {
        "pt": "Abaixo da média",
		"en": "Below average",
		"es": "Inferior al promedio"
    },
    "l-aguardandoNota": {
        "pt": "Aguardando nota",
		"en": "Waiting score",
		"es": "Esperando nota"
    },
    "l-pendente": {
        "pt": "Pendente",
		"en": "Pending",
		"es": "Pendiente"
    },
    "l-perdida": {
        "pt": "Perdida",
		"en": "Lost",
		"es": "Perdida"
    },
    "l-verDisciplina": {
        "pt": "Ver disciplina",
		"en": "See course subject",
		"es": "Ver materia"
    },
    "l-verAula": {
        "pt": "Ver aula",
		"en": "See class",
		"es": "Ver clase"
    },
    "l-verTodasAvaliacoes": {
        "pt": "Ver todas avaliações",
		"en": "See all evaluations",
		"es": "Ver todas evaluaciones"
    },
    "l-somatorio": {
        "pt": "Somatório",
		"en": "Sum",
		"es": "Sumatorio"
    },
    "l-msg-aval-nao-executada": {
        "pt": "A Avaliação Descritiva não foi executada.",
		"en": "The Descriptive Assessment was not executed.",
		"es": "La evaluación descriptiva no se ejecutó."
    },
    "l-legenda-aval-desc": {
        "pt": "<small><u>Instrução:</u> Nas questões de múltiplas escolhas, as questões marcadas no momento da execução da avaliação, estão com a opção em <span class='label label-primary font-size-90perc'>azul</span> e sua descrição em <strong>negrito</strong>.</small>",
		"en": "<small><u>Instrução:</u> In the multiple choice questions, the questions selected during execution of evaluation have option in <span class='label label-primary font-size-90perc'>blue</span> and its description is in <strong>negrito</strong>.</small>",
		"es": "<small><u>Instrucción:</u> En las preguntas de múltiple opción, las preguntas marcadas durante la ejecución de la evaluación, tienen la opción en <span class='label label-primary font-size-90perc'>azul</span> y su descripción en <strong>negrito</strong>.</small>"
    },
    "l-aggregate-total": {
        "pt": "Total: ",
		"en": "Total:",
		"es": "Total: "
    },
    "l-aggregate-valor-etapa": {
        "pt": "Valor Etapa: ",
		"en": "Stage Value:",
		"es": "Valor etapa: "
    },
    "l-avaliacao-institucional":{
        "pt": "Avaliação Institucional",
		"en": "Institutional Evaluation",
		"es": "Evaluación institucional"
    },
    "l-responder":{
        "pt": "Responder",
		"en": "Answer",
		"es": "Responder"
    },
    "l-objeto-avaliado": {
        "pt": "Objeto Avaliado",
		"en": "Objected Evaluated",
		"es": "Objeto evaluado"
    },
    "l-nome-curso": {
        "pt": "Curso",
		"en": "Course",
		"es": "Curso"
    },
    "l-nome-grade": {
        "pt": "Grade",
		"en": "Grid",
		"es": "Cuadrícula"
    },
    "l-cod-turma": {
        "pt": "Turma",
		"en": "Class",
		"es": "Grupo"
    },
    "l-nome-disciplina":{
        "pt": "Disciplina",
		"en": "Course Subject",
		"es": "Materia"
    },
    "l-nome-professor":{
        "pt": "Professor",
		"en": "Teacher",
		"es": "Profesor"
    },
    "l-todos":{
        "pt": "TODOS",
		"en": "ALL",
		"es": "TODOS"
    },
    "l-avaliacao-sobre":{
        "pt": "Avaliação sobre",
		"en": "Evaluation on",
		"es": "Evaluación sobre"
    },
    "l-msg-avaliacao-sucesso":{
        "pt": "Avaliação finalizada com sucesso.",
		"en": "Evaluation successfully finalized.",
		"es": "Evaluación finalizada con éxito."
    },
    "l-widget-title": {
      "pt": "Painel do Aluno",
	  "en": "Student Panel",
	  "es": "Panel del alumno"
    }
}]
