/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduAtividadesExtrasModule
 * @name eduAtividadesExtrasInscritasController
 * @object controller
 *
 * @created 21/11/2016 v12.1.15
 * @updated
 *
 * @requires eduAtividadesExtras.module
 *
 * @dependencies eduAvaliacoesFactory
 *
 * @description Controller de Atividades Inscritas
 */
define(['aluno/avaliacoes/avaliacoes.module',
    'aluno/avaliacoes/avaliacoes.factory',
    'aluno/avaliacoes/avaliacoes.service',
    'utils/edu-enums.constants',
    'utils/edu-utils.factory',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'
], function () {

    'use strict';

    angular
        .module('eduAvaliacoesModule')
        .controller('EduAvaliacoesInstitucionaisListController', EduAvaliacoesInstitucionaisListController);

    EduAvaliacoesInstitucionaisListController.$inject = [
        '$rootScope',
        'eduAvaliacoesService',
        'eduAvaliacoesFactory',
        '$filter',
        'eduUtilsFactory',
        'eduWidgetsConsts',
        'eduConstantesGlobaisConsts',
        '$state',
        'totvs.app-notification.Service',
        'i18nFilter'
    ];

    function EduAvaliacoesInstitucionaisListController(
        $rootScope,
        eduAvaliacoesService,
        eduAvaliacoesFactory,
        $filter,
        eduUtilsFactory,
        eduWidgetsConsts,
        eduConstantesGlobaisConsts,
        $state,
        totvsNotification,
        i18nFilter) {

        var self = this;

        // Objetos e variáveis
        self.avaliacaoInstitucionalLista = [];
        self.avaliacaoInstitucionalFiltradaLista = [];
        self.intAvaliacoesCount = 0;
        self.listComponentes = [];
        self.objetoAvaliado = $filter('i18n')('l-todos', [], 'js/aluno/avaliacoes');
        self.objetoAvaliadosLista = [];
        self.urlPortal = '';
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.AvaliacaoInstitucional;

        // métodos
        self.filtrarObjetoAvaliado = filtrarObjetoAvaliado;
        self.getAvaliacoesInstitucionais = getAvaliacoesInstitucionais;
        self.getObjetosAvaliados = getObjetosAvaliados;
        self.getConstantesTestis = getConstantesTestis;
        self.redirecionarResponderAvaliacao = redirecionarResponderAvaliacao;

        init();

        /**
         * Inicialia as informações da tela
         */
        function init() {
            getAvaliacoesInstitucionais();
            getParametroGlobal();
        }

        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_AVALIACAO_INSTITUCIONAL) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        /**
         * Recupera todas as avaliações institucionais do aluno
         */
        function getAvaliacoesInstitucionais() {
            eduAvaliacoesFactory.retornaAvaliacaoInstitucional([], function (result) {
                if (angular.isDefined(result) && angular.isDefined(result.Avaliacao)) {
                    angular.forEach(result.Avaliacao, function (value) {
                        if (value && value.$length) {
                            self.intAvaliacoesCount = value.$length;
                        }
                        self.avaliacaoInstitucionalLista.push(value);
                    });
                    self.avaliacaoInstitucionalFiltradaLista = self.avaliacaoInstitucionalLista;
                    self.objetoAvaliadosLista = self.getObjetosAvaliados();
                    self.constTestis = self.getConstantesTestis();
                }
            });
        }

        /**
         * Retorna o parâmetro Global
         */
        function getParametroGlobal(){
            var nomeCampo = 'URLPORTAL';

            eduUtilsFactory.getParametroGlobal(nomeCampo, function(result){
                if(angular.isDefined(result)){
                    self.urlPortal = result[0][nomeCampo];
                }
            });
        }

        /**
         * Aplica o filtro quando o usuário altera o combo de tipo de avaliação.
         * */
        function filtrarObjetoAvaliado() {
            if (self.objetoAvaliado === $filter('i18n')('l-todos', [], 'js/aluno/avaliacoes')) { // todos
                self.avaliacaoInstitucionalFiltradaLista = self.avaliacaoInstitucionalLista;
            } else {
                self.avaliacaoInstitucionalFiltradaLista =
                    $.grep(self.avaliacaoInstitucionalLista, function (e) {
                        return e.OBJETOAVALIADO === self.objetoAvaliado;
                    });
            }
        }

        /**
         * Busca os objetos avaliados para montar o combobox
         */
        function getObjetosAvaliados() {
            var tempData = [];

            if (angular.isDefined(self.avaliacaoInstitucionalLista) && self.avaliacaoInstitucionalLista.length > 0) {

                angular.forEach(self.avaliacaoInstitucionalLista, function (item) {
                    var existe = $filter('filter')(tempData, {
                        'OBJETOAVALIADO': item.OBJETOAVALIADO
                    }, true);

                    if (existe.length === 0) {
                        tempData.push({
                            'OBJETOAVALIADO': item.OBJETOAVALIADO
                        });
                    }
                });
                tempData = $filter('orderBy')(tempData, 'OBJETOAVALIADO');
                tempData.unshift({
                    'OBJETOAVALIADO': $filter('i18n')('l-todos', [], 'js/aluno/avaliacoes')
                });
            }
            return tempData;
        }

        /**
         * Retorna as constantes do Testis
         *
         */
        function getConstantesTestis() {
            eduAvaliacoesFactory.retornaConstantesTestis([], function (result) {
                if (angular.isDefined(result)) {
                    self.constTestis = result;
                }
            });
        }

        /**
         * Redireciona para a página da avaliação no Testis
         *
         * @param {any} objAvaliados - objeto contendo as informações
         */
        function redirecionarResponderAvaliacao(objAvaliados) {

            // Janela de avaliação
            var winAvaliacao = window;

            // Mensagem que será apresentada após responder a avaliação
            winAvaliacao.mensagemSucesso = $filter('i18n')('l-msg-avaliacao-sucesso', [], 'js/aluno/avaliacoes');

            // Url de retorno
            var urlRetorno = winAvaliacao.location.origin +
                             winAvaliacao.location.pathname +
                             'js/aluno/avaliacoes/institucional/avaliacoes-institucionais-after.view.html';

            if (angular.isDefined(self.constTestis)) {
                // Monta e abre a URL, passando os parâmetros usados pelo Testis
                winAvaliacao.open(
                    self.urlPortal +
                    'SOURCE/Edu-Educacional/RM.EDU.AVALIACAOINSTITUCIONAL/EduExecutaAvaliacao.aspx?' +
                    self.constTestis.PARAMETER_EXECTEST + '=' +
                    objAvaliados.IDEXECPROVA + '&' +
                    self.constTestis.PARAMETER_ACTION + '=TstQuestionarioActionWeb&' +
                    self.constTestis.PARAMETER_FILESTYLENAME +
                    '=EduAvaliacao&' + self.constTestis.PARAMETER_FILESTYLEDIRECTORY +
                    '=SOURCE/Edu-Educacional/RM.EDU.AVALIACAOINSTITUCIONAL/Styles/&' +
                    self.constTestis.PARAMETER_CSSCLASSNAME +
                    '=EduBtn2&' + self.constTestis.PARAMETER_FINISHURL +
                    '='+ urlRetorno,
                    '_blank', 'Height=550px,Width=600px,Top=50,Left=50,help=no,status=yes,resizable=0,scrollbars=1');
            }
        }
    }
});
