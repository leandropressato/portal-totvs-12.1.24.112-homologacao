/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduGradeCurricularModule
* @name EduGradeCurricularController
* @object controller
*
* @created 04/11/2016 v12.1.15
* @updated
*
* @requires grade-curricular.module
*
* @dependencies eduGradeCurricularFactory
*
* @description Controller da Grade Curricular
*/
define(['aluno/grade-curricular/grade-curricular.module',
    'aluno/grade-curricular/grade-curricular.factory',
    'aluno/matricula/matricula-disciplina.service',
    'utils/edu-enums.constants',
    'utils/edu-utils.factory',
    'diretivas/edu-selecao-periodoletivo.factory',
    'utils/reports/edu-relatorio.service',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'], function () {

        'use strict';

        angular
            .module('eduGradeCurricularModule')
            .controller('EduGradeCurricularController', EduGradeCurricularController);

        EduGradeCurricularController.$inject = [
            'TotvsDesktopContextoCursoFactory',
            'eduGradeCurricularFactory',
            'eduSelecaoPeriodoletivoFactory',
            'MatriculaDisciplinaService',
            'eduEnumsConsts',
            'eduUtilsFactory',
            '$scope',
            '$state',
            'i18nFilter',
            '$timeout',
            'eduRelatorioService',
            'eduWidgetsConsts',
            'eduConstantesGlobaisConsts',
            '$rootScope',
            'totvs.app-notification.Service'
        ];

        /**
         * Controller da Grade Curricular
         * @param   {object}    objContextoCursoFactory        Factory do Contexto Educacional
         * @param   {object}    eduGradeCurricularFactory      Factory da Grade Curricular
         * @param   {object}    EduSelecaoPeriodoletivoFactory Factory da Seleção de Períodos
         * @param   {object}    MatriculaDisciplinaService     Serviço para exibir o modal de disciplina
         * @param   {constants} EduEnumsConsts                 Constantes Enums do Educacional
         * @param   {object}    objUtilsFactory                Factory para serviços dos Parametros do Educacional
         * @param   {object}    $scope                         Objeto de Escopo
         * @param   {object}    $state                         Objeto de estado de rotas
         * @param   {object}    i18nFilter                     Objeto para aplicar filtro de tradução
         * @param   {object}    $timeout                       Objeto timeout angular
         * @param   {object}    eduRelatorioService            Objeto de serivço de relatórios RM
         * @param   {object}    eduWidgetsConsts               Constantes de Widgets
         */
        function EduGradeCurricularController(objContextoCursoFactory, eduGradeCurricularFactory, EduSelecaoPeriodoletivoFactory,
            MatriculaDisciplinaService, EduEnumsConsts, objUtilsFactory, $scope, $state, i18nFilter, $timeout, eduRelatorioService, eduWidgetsConsts, eduConstantesGlobaisConsts, $rootScope, totvsNotification) {

            /* jshint maxparams: false */
            /* jshint maxstatements: false */
            var self = this,
                codRelatorioImpressao = null,
                codColRelatorioImpressao = null,
                resumo = '<i>Resumo</i>';

            self.tabEnum = {
                Todos: 'T',
                EmCurso: 'EC',
                Pendentes: 'P',
                Concluidos: 'C'
            };

            self.tipoPeriodo = {
                Disciplinas: 'D',
                Componentes: 'C'
            };

            self.objContexto = {};
            self.objPeridosLetivosList = [];
            self.objGradeCurricular = {};
            self.objHabilitacaoAluno = {};

            self.objGradeCurricular = {};

            self.objPeriodosList = [];
            self.objDisciplinasList = [];
            self.objTCCList = [];

            self.EduTipoDiscEnum = {
                SempreMostrar: 0,
                Obrigatorias: 1,
                Equivalentes: 2,
                EletivasOptativas: 3,
                AtivCurriculares: 4,
                Extras: 5
            };

            self.EduStatusEnum = {
                SempreMostrar: 0,
                EmCurso: 1,
                Pendentes: 2,
                Concluidos: 3
            };

            self.strCurrentTab = self.tabEnum.Todos;
            self.intNumCasasDecimaisCH = 0;
            self.intNumCasasDecimaisCredAcad = 0;
            self.blnExibirDiscExtra = true;
            self.blnUsaGrupoOptEletiva = false;
            self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.GradeCurricular;

            self.getContexto = getContexto;
            self.loadPeridosLetivos = loadPeridosLetivos;
            self.verificaTipoEnsino = verificaTipoEnsino;
            self.loadGradeCurricular = loadGradeCurricular;
            self.loadNumCasasDecimais = loadNumCasasDecimais;
            self.loadDisciplinasTCC = loadDisciplinasTCC;
            self.setPeriodosDisciplinas = setPeriodosDisciplinas;
            self.exibirResumoNaAnalise = exibirResumoNaAnalise;
            self.expandirTodos = expandirTodos;
            self.loadDisciplinasByTab = loadDisciplinasByTab;
            self.changeTab = changeTab;
            self.aplicarFiltro = aplicarFiltro;
            self.detalhesDisciplina = detalhesDisciplina;
            self.getImageStatus = getImageStatus;
            self.getColunaDisciplina = getColunaDisciplina;
            self.getColunaCodDisc = getColunaCodDisc;
            self.getColunaConceitoNota = getColunaConceitoNota;
            self.exibeGrupo = exibeGrupo;
            self.exibirMsgNenhumPeriodo = exibirMsgNenhumPeriodo;
            self.getColunaStatus = getColunaStatus;

            self.retornaWidgetUrl = retornaWidgetUrl;
            self.permiteImprimir = false;
            self.imprimir = imprimir;
            self.destacarDisciplinaEquivalente = destacarDisciplinaEquivalente;
            self.setDisciplinasEquivalentesComoSubItem = setDisciplinasEquivalentesComoSubItem;

            init();

            /**
             * Inicialização
             */
            function init() {

                self.getContexto();
                self.loadPeridosLetivos();
                self.verificaTipoEnsino();
                self.loadNumCasasDecimais();
                self.loadGradeCurricular();

                /**
                 * Ao mudar da aba verifica se marcou para
                 * expandir ou recolher todos e aplica
                 */
                $scope.$watch('controller.objPeriodoList', function () {
                    self.expandirTodos();
                });
            }

            /* Valida se o usuário tem permissão no Menu */
            var permissionsWatch = $rootScope.$watch('objPermissions', function () {
                if ($rootScope.objPermissions) {
                    if (!$rootScope.objPermissions.EDU_ACADEMICO_HISTORICOENSINOSUPERIOR) {
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-Atencao'),
                            detail: i18nFilter('l-usuario-sem-permissao')
                        });
                        $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                    }

                    //destroy watch
                    permissionsWatch();
                }
            });

            /* Verifica se é Ensino Superior, caso contrário retorna para a página inicial */
            function verificaTipoEnsino() {
                // Verifica se o tipo de ensino é Ensino Superior
                if (parseInt(self.objContexto.cursoSelecionado.APRESENTACAO) !== EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoSuperior) {
                    $state.go('mural.start');
                }
            }

            /* Obtém o contexto Educacional */
            function getContexto() {
                self.objContexto = objContextoCursoFactory.getCursoSelecionado();
            }

            /* Carrega a lista de períodos letivos em aberto */
            function loadPeridosLetivos() {
                EduSelecaoPeriodoletivoFactory.getPeriodos(function (result) {
                    if (result && angular.isArray(result)) {
                        self.objPeridosLetivosList = result;
                    }
                });
            }

            /* Obtém a Grade curricular do curso do contexto */
            function loadGradeCurricular() {
                eduGradeCurricularFactory.gradeCurricularEnsinoSuperior(
                    function (result) {
                        if (result) {
                            setDisciplinasEquivalentesComoSubItem(result);
                            self.objGradeCurricular = result;
                            if (angular.isDefined(self.objGradeCurricular.SHabilitacaoAluno) && angular.isArray(self.objGradeCurricular.SHabilitacaoAluno)) {
                                self.objHabilitacaoAluno = self.objGradeCurricular.SHabilitacaoAluno[0];
                            }

                            if (angular.isDefined(self.objGradeCurricular.SDisciplinaTCC) && angular.isArray(self.objGradeCurricular.SDisciplinaTCC)) {
                                self.loadDisciplinasTCC(self.objGradeCurricular.SDisciplinaTCC,
                                    self.objGradeCurricular.SDisciplinaTCCOrientador,
                                    self.objGradeCurricular.SDisciplinaTCCBanca);
                            }

                            // Inicial na aba Todos
                            self.loadDisciplinasByTab(self.strCurrentTab);
                        }
                    });
            }

            /* Carrega o número de casas decimais */
            function loadNumCasasDecimais() {
                objUtilsFactory.getParametrosTOTVSEducacionalAsync(function (parametros) {
                    self.intNumCasasDecimaisCH = parametros.NumCasasDecimaisCH;
                    self.intNumCasasDecimaisCredAcad = parametros.NumCasasDecimaisCredAcad;
                    self.blnExibirDiscExtra = parametros.ExibirDiscExtra;
                    codRelatorioImpressao = parametros.CodRelatorioGradeCurricularReports;
                    codColRelatorioImpressao = parametros.CodColRelatorioGradeCurricularReports;
                    self.permiteImprimir = !!codRelatorioImpressao;
                    self.UsaGrupoOptEletiva = parametros.UsaGrupoOptEletiva;
                });
            }

            /**
             * Carregam as informações das disciplinas de TCC
             * @param {array} lstSDisciplinaTCC           Temas/Trabalhos de TCC
             * @param {array} lstSDisciplinaTCCOrientador Orientadores
             * @param {array} lstSDisciplinaTCCBanca      Locais e datas das bancas
             */
            function loadDisciplinasTCC(lstSDisciplinaTCC, lstSDisciplinaTCCOrientador, lstSDisciplinaTCCBanca) {
                for (var i = 0; i < lstSDisciplinaTCC.length; i++) {
                    self.objTCCList[i] = {};
                    self.objTCCList[i].IDTCC = lstSDisciplinaTCC[i].IDTCC;
                    self.objTCCList[i].TEMA = lstSDisciplinaTCC[i].TEMA;
                    self.objTCCList[i].NOMETIPOTCC = lstSDisciplinaTCC[i].NOMETIPOTCC;
                    self.objTCCList[i].NOMELINHAPESQUISA = lstSDisciplinaTCC[i].NOMELINHAPESQUISA;
                    self.objTCCList[i].NOMESTATUS = lstSDisciplinaTCC[i].NOMESTATUS;
                    self.objTCCList[i].objDiscTCCList = [];
                    self.objTCCList[i].objDiscTCCList.push(lstSDisciplinaTCC[i]);

                    self.objTCCList[i].objOrientadorList = [];
                    for (var j = 0; j < lstSDisciplinaTCCOrientador.length; j++) {
                        if (lstSDisciplinaTCCOrientador[j].IDTCC === lstSDisciplinaTCC[i].IDTCC) {
                            self.objTCCList[i].objOrientadorList.push(lstSDisciplinaTCCOrientador[j]);
                        }
                    }

                    self.objTCCList[i].objBancaList = [];
                    for (var k = 0; k < lstSDisciplinaTCCBanca.length; k++) {
                        if (lstSDisciplinaTCCBanca[k].IDTCC === lstSDisciplinaTCC[i].IDTCC) {
                            self.objTCCList[i].objBancaList.push(lstSDisciplinaTCCBanca[k]);
                        }
                    }

                    if (self.objTCCList[i].objBancaList.length > 0) {
                        self.objTCCList[i].objBancaList.sort(function (a, b) {
                            return new Date(a.DATAHORABANCA) - new Date(b.DATAHORABANCA);
                        });
                    }
                }
            }

            /**
             * Seta a lista dos períodos agrupados e as disciplinas
             * correspondentes a cada período.
             * @param {array} lstGradeCurricular Lista com a grade curricular de acordo com o filtro
             */
            function setPeriodosDisciplinas(lstGradeCurricular) {
                self.objPeriodoList = [];
                self.objDisciplinasList = [];
                var codPeriodo = 0,
                    codGrupo = "",
                    subGrupoCod = "0",
                    objPeriodo = {};

                // Solução paleativa para &nbsp; que é retornado do server nos grupos de disciplinas optativas / eletiva.
                // Situação só ocorre caso sejá um grupo de disciplinas optativas eletivas que esta em um periodo onde não existe materia.
                for (var i = 0; i < self.objGradeCurricular.APRESENTACAOHISTORICO.length; i++) {
                    var registro = self.objGradeCurricular.APRESENTACAOHISTORICO[i];

                    registro.CODGRUPO = registro.CODGRUPO.replace(/&nbsp;/g, '');

                    // Tratamento específico para o caso onde só existem disciplinas optativas/eletivas (currículo individual), onde não existe período
                    // e todos os registros de disciplinas optativas/eletivas são retornados com o campo CODPERIODO = 98
                    if (registro.CODPERIODO === 98) {
                        if( registro.CODPERIODO !== codPeriodo && registro.CODGRUPO !== codGrupo) {
                            registro.CODPERIODO = parseInt(String(registro.CODPERIODO) + i);
                            codPeriodo = registro.CODPERIODO;
                            codGrupo = registro.CODGRUPO;
                        } else if (registro.CODGRUPO === codGrupo) {
                            registro.CODPERIODO = codPeriodo;
                        }
                    }
                }

                codPeriodo = 0;

                angular.forEach(lstGradeCurricular, function (value) {
                    if (value.CODPERIODO !== codPeriodo && value.NEGRITO !== false) { // NEGRITO === false é a modalidade ativ. curricular
                        // verifica se não existe disciplina
                        if (angular.isDefined(self.objDisciplinasList[codPeriodo]) &&
                            self.objDisciplinasList[codPeriodo].length === 0) {

                            self.objPeriodoList.splice(-1, 1);
                        }

                        objPeriodo = {
                            CODPERIODO: value.CODPERIODO,
                            NOMEPERIODO: value.CODGRUPO,
                            TIPOENUM: value.TIPOENUM
                        };

                        if (value.CODDISC === '' || value.CODDISC === '-1') {
                            objPeriodo.tipoPeriodo = self.tipoPeriodo.Componentes;
                        } else {
                            objPeriodo.tipoPeriodo = self.tipoPeriodo.Disciplinas;
                        }

                        self.objPeriodoList.push(objPeriodo);
                    }

                    if (angular.isUndefined(self.objDisciplinasList[value.CODPERIODO])) {
                        self.objDisciplinasList[value.CODPERIODO] = [];
                    }

                    if (value.DISCIPLINA !== '') {
                        if(angular.isDefined(value.SUBGRUPOCOD) && value.SUBGRUPO !== null) {
                            if (value.SUBGRUPOCOD !== subGrupoCod){
                                self.objDisciplinasList[value.CODPERIODO].push({CODPERIODO:value.CODPERIODO, SUBGRUPO:value.SUBGRUPO, CODDISC:'agrupadorOptElet', DISCIPLINA:''});
                            }
                        }
                        self.objDisciplinasList[value.CODPERIODO].push(value);
                    }

                    codPeriodo = value.CODPERIODO;
                    if(angular.isDefined(value.SUBGRUPOCOD) && value.SUBGRUPOCOD !== null)
                        subGrupoCod = value.SUBGRUPOCOD;
                });

                if (angular.isDefined(self.objDisciplinasList[codPeriodo]) &&
                    self.objDisciplinasList[codPeriodo].length === 0) {

                    self.objPeriodoList.splice(-1, 1);
                }
            }

            /**
             * Verifica se o resumo da análise curricular será exibido
             * @returns {boolean} Verdadeiro para exibir o resumo da análise
             */
            function exibirResumoNaAnalise() {
                var blnReturn = false;
                if (angular.isDefined(self.objHabilitacaoAluno) &&
                    angular.isDefined(self.objHabilitacaoAluno.QTDTOTALDISCOBR) &&
                    self.objHabilitacaoAluno.QTDTOTALDISCOBR != null) {

                    blnReturn = true;
                }

                return blnReturn;
            }

            /* Expande/Recolhe as informações dos períodos */
            function expandirTodos() {
                $timeout(function () {
                    if ($('#chkExpandirTodos:checked').length > 0) {
                        $('#lblExpandirTodos').parent().attr('class', 'btn btn-success');
                        $('#txtExpandirTodos').html(' ' + i18nFilter('l-recolher-todos', [], 'js/aluno/grade-curricular'));
                        $('#lblExpandirTodos').attr('class', 'glyphicon ico-zoom-out');
                        toggleElement(true);
                    }
                    else {
                        $('#lblExpandirTodos').parent().attr('class', 'btn btn-info');
                        $('#txtExpandirTodos').html(' ' + i18nFilter('l-expandir-todos', [], 'js/aluno/grade-curricular'));
                        $('#lblExpandirTodos').attr('class', 'glyphicon ico-zoom-in');
                        toggleElement(false);
                    }
                }, 0);
            }

            /**
             * Mostra ou esconde todas as disciplinas de todos os períodos
             * @param {boolean} blnShow Verdadeiro se é para mostrar todos
             */
            function toggleElement(blnShow) {
                angular.forEach(angular.element('span.content'), function (value) {
                    var group = angular.element(value);
                    if ((blnShow && group.css('display') === 'none') ||
                        (!blnShow && group.css('display') !== 'none')) {

                        $timeout(function () {
                            group.prev().children().children().click();
                        }, 0);

                    }
                });
            }

            /**
             * Aplica o filtro ao selecionar uma tab na view
             * @param {string} tab Tab selecionada na view
             * @param {string} id Identificador html da tab
             */
            function changeTab(tab, id) {
                self.strCurrentTab = tab;
                var filter = angular.element('#' + id).find('.btn-group label.active').children().val();
                self.aplicarFiltro(parseInt(filter));
            }

            /**
             * Carrega a Disciplina de acordo com a tab selecionada
             * @param {string} tab Tab selecionada na view
             */
            function loadDisciplinasByTab(tab) {
                self.strCurrentTab = tab;

                switch (tab) {
                    case self.tabEnum.Todos:
                        self.setPeriodosDisciplinas(getGradeCurricularTodos());
                        break;
                    case self.tabEnum.EmCurso:
                        self.setPeriodosDisciplinas(getGradeCurricularEmCurso());
                        break;
                    case self.tabEnum.Pendentes:
                        self.setPeriodosDisciplinas(getGradeCurricularPendentes());
                        break;
                    case self.tabEnum.Concluidos:
                        self.setPeriodosDisciplinas(getGradeCurricularConcluidos());
                        break;
                }
            }

            /**
             * Evento ao clicar nos filtros de disciplinas à exibir
             * @param {int} filter Código do Período
             */
            function aplicarFiltro(filter) {
                self.loadDisciplinasByTab(self.strCurrentTab);
                if (filter === self.EduTipoDiscEnum.SempreMostrar) {
                    return;
                }

                var lstPeriodos = $.grep(self.objPeriodoList, function (value) {
                    return (value.TIPOENUM === filter || value.TIPOENUM === self.EduTipoDiscEnum.SempreMostrar);
                });

                self.objPeriodoList = lstPeriodos;
            }

            /**
             * Tag relativa ao status
             * @param   {object} objItem Objeto da linha corrente da disciplina
             * @returns {string} Tag do status
             */
            function getImageStatus(objItem) {
                if (angular.isDefined(objItem.IMG)) {
                    switch (objItem.IMG) {
                        case 'img_concluida.PNG':
                            return '<span class="tag legend legenda-verde legenda-content-1"></span>';
                        case 'img_pendente.PNG':
                            return '<span class="tag legend legenda-vermelho legenda-content-2"></span>';
                        case 'img_naoconcluida.PNG':
                            return '<span class="tag legend legenda-amarelo legenda-content-3"></span>';
                        case 'equivalente.gif':
                            return '<span class="tag legend legenda-azul legenda-content-4"></span>';
                        default:
                            return '';
                    }
                }
                else if (self.strCurrentTab === self.tabEnum.Pendentes) {
                    return '<span class="tag legend legenda-vermelho legenda-content-2"></span>';
                }
                else if (self.strCurrentTab === self.tabEnum.Concluidos) {
                    return '<span class="tag legend legenda-verde legenda-content-1"></span>';
                }
                else {
                    return '';
                }

            }

            /**
             * @param {any} dataItem Objeto corrente do grid
             * @returns {string} HTML com o código da disciplina
             */
            function getColunaCodDisc(dataItem) {
                if (angular.isDefined(dataItem.CODDISC) && dataItem.CODDISC === 'agrupadorOptElet') {
                    var strToReturn = '<b class="colAgrupadorOptElet">' + dataItem.SUBGRUPO + '</b>'

                    $timeout(function () {
                        angular.element('.colAgrupadorOptElet').parent().attr('colspan', '9');
                        angular.element('.colAgrupadorOptElet').parent().css('background-color', '#ddd');
                        angular.element('.colAgrupadorOptElet').parent().prevAll().remove();
                        angular.element('.colAgrupadorOptElet').parent().nextAll().remove();
                    }, 0);

                    return strToReturn;
                }
                else {
                    return dataItem.CODDISC;
                }
            }

            /**
             * Template da coluna de Disciplinas
             * @param   {object} dataItem Objeto corrente do grid
             * @returns {string} HTML com o código e nome da disciplina
             */
            function getColunaDisciplina(dataItem) {
                var strToReturn = '';

                strToReturn = dataItem.DISCIPLINA;

                if (dataItem.IDTURMADISC && dataItem.IDTURMADISC !== '' && dataItem.IDTURMADISC !== 0) {
                    strToReturn = '<a ng-style="{\'cursor\': \'pointer\', \'color\': \'#1e99c0\'}" ng-click="controller.detalhesDisciplina(dataItem.IDTURMADISC)">{{dataItem.DISCIPLINA}}</a>';
                }

                var blnExecTimeout = false;
                if (dataItem.CODDISC === '-1' && dataItem.ITALICO === true) {
                    strToReturn = '<i class="colModalidade">' + strToReturn + '</i>';
                    blnExecTimeout = true;
                }
                else if (dataItem.CODDISC === '-1') {
                    strToReturn = '<strong class="colModalidade">' + strToReturn + '</strong>';
                    blnExecTimeout = true;
                }

                if (blnExecTimeout) {
                    $timeout(function () {
                        angular.element('.colModalidade').parent().attr('colspan', '9');
                        angular.element('.colModalidade').parent().css('background-color', '#ddd');
                        angular.element('.colModalidade').parent().prevAll().remove();
                        angular.element('.colModalidade').parent().nextAll().remove();
                    }, 0);
                }

                return strToReturn;
            }

            /**
             * Template da coluna de Conceito/Nota
             * @param   {object} dataItem Objeto corrente do grid
             * @returns {string} HTML com o código e nome da disciplina
             */
            function getColunaConceitoNota(dataItem) {
                var strToReturn = '';

                if ((dataItem.NOTA && dataItem.CONCEITO)) {
                    strToReturn += '<span>' + dataItem.NOTA + '/' + dataItem.CONCEITO + '</span>';
                } else if (dataItem.NOTA) {
                    strToReturn += '<span>' + dataItem.NOTA + '</span>';
                } else if (dataItem.CONCEITO) {
                    strToReturn += '<span>' + dataItem.CONCEITO + '</span>';
                }

                return strToReturn;
            }

            /**
             * Template da coluna de Disciplinas
             * @param   {object} dataItem Objeto corrente do grid
             * @returns {string} HTML com a situação da disciplina na grade curricular
             */
            function getColunaStatus(dataItem) {
                var strToReturn = '';

                if (dataItem && dataItem.STATUS) {

                    //*Equiv. é uma constante do sistema, para indica disciplina equivalente
                    if (dataItem.STATUS === '*Equiv.') {
                        strToReturn += '<span>';
                        strToReturn += '<a ng-style="{\'cursor\': \'pointer\', \'color\': \'#1e99c0\'}" ng-click="controller.destacarDisciplinaEquivalente(this)">';
                        strToReturn += dataItem.STATUS;
                        strToReturn += '</a>';
                        strToReturn += '</span>';
                    } else {
                        strToReturn += dataItem.STATUS;
                    }
                }

                return strToReturn;
            }

            function destacarDisciplinaEquivalente(sender) {
                if (sender.dataItem && sender.dataItem.Equivalencia && sender.dataItem.Equivalencia.length > 0) {

                    for (var i = 0; i < sender.dataItem.Equivalencia.length; i++) {
                        
                        for (var j = 0; j < sender.dataItem.parent().length; j++) {

                            if (sender.dataItem.parent()[j].CODDISC === sender.dataItem.Equivalencia[i].CODDISC) {
                                var uidRowDisciplinaEquivalenteDestaque = sender.dataItem.parent()[j].uid,
                                    elementRowDisicplina = $('tr[data-uid=' + uidRowDisciplinaEquivalenteDestaque + ']');


                                if (elementRowDisicplina.length > 0 && !elementRowDisicplina.hasClass('gradeCurricularDestaqueEquivalencia')) {
                                    elementRowDisicplina.addClass('gradeCurricularDestaqueEquivalencia');
                                    break;
                                }
                                else {
                                    elementRowDisicplina.removeClass('gradeCurricularDestaqueEquivalencia');
                                    break;
                                }
                            }
                        }
                    }
                }

            }

            /**
             * Mostra os Detalhes da Disciplina no Modal
             * @param {int} idTurmaDisc Turma/Disciplina
             */
            function detalhesDisciplina(idTurmaDisc) {

                MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(self.objContexto.cursoSelecionado.CODCOLIGADA,
                    idTurmaDisc, self.objContexto.cursoSelecionado.RA);
            }

            /**
             * View com o Widget
             * @returns {string} View do Widget
             */
            function retornaWidgetUrl() {
                return 'js/aluno/widgets/widget-area.view.html';
            }

            /* Executa emissão do relatório associado a visão(Parametrizado no sistema). */
            function imprimir() {
                if (codRelatorioImpressao) {
                    eduRelatorioService.emitirRelatorio(codRelatorioImpressao, codColRelatorioImpressao);
                }
            }

            /**
             * Verifica se deve exibir o grupo (utilizado nos grupos de componentes curriculares)
             *
             * @param {any} objPeriodo Objeto Período
             * @param {any} objDisciplinasPeriodoList Lista de Disciplinas do período
             */
            function exibeGrupo(objPeriodo, objDisciplinasPeriodoList) {

                if (objPeriodo.tipoPeriodo === self.tipoPeriodo.Disciplinas) {
                    // não possui informações de componentes curriculares, existe outra validação para ocultar períodos de disciplinas
                    return true;
                } else {
                    // quando o grid com componentes curriculares estiver vazio, são adicionados 2 itens
                    if (objDisciplinasPeriodoList.length > 2) {
                        // possui pelo menos 1 registro de componente curricular
                        return true;
                    } else {
                        // a grid está vazia
                        return false;
                    }
                }

            }

            function exibirMsgNenhumPeriodo(objPeriodosList) {

                if (!angular.isDefined(objPeriodosList)) {
                    return false;
                }

                if (objPeriodosList.length === 0) {
                    return true;
                }

                var i, countPeriodosComp = 0,
                    countPeriodosCompVazio = 0;

                for (i = 0; i < objPeriodosList.length; i++) {
                    if (objPeriodosList[i].tipoPeriodo === self.tipoPeriodo.Componentes) {
                        // algum período com dados de componentes curriculares possui dados
                        countPeriodosComp++;
                        if ((self.objDisciplinasList[objPeriodosList[i].CODPERIODO]).length <= 2) {
                            // se existirem períodos de componentes curriculares vazios, eles terão 2 linhas
                            countPeriodosCompVazio++;
                        }
                    }
                }

                if (countPeriodosComp === objPeriodosList.length) {
                    // todos os períodos visíveis são de componentes curriculares
                    if (countPeriodosComp === countPeriodosCompVazio) {
                        // todos os períodos visíveis estão vazios
                        return true;
                    } else {
                        // algum período de componente curricular está preenchido
                        return false;
                    }
                } else {
                    // existe algum período de disciplinas
                    return false;
                }

            }

            function setDisciplinasEquivalentesComoSubItem(lstGradeCurricular) {
                //Disciplinas equivalentes em curso
                if (angular.isArray(lstGradeCurricular.SDiscEquivEmCurso)) {

                    angular.forEach(lstGradeCurricular.SDiscEquivEmCurso, function (infoDisciplinaEquivalenteEmCurso) {

                        var disciplinaEquivalencia = lstGradeCurricular.APRESENTACAOHISTORICO.find(function (disciplinaHistorico) {

                            if (infoDisciplinaEquivalenteEmCurso.CODDISC === disciplinaHistorico.CODDISC) {
                                return disciplinaHistorico;
                            }
                        }),
                            disciplinaEquivalente = lstGradeCurricular.APRESENTACAOHISTORICO.find(function (disciplinaHistorico) {

                                if (infoDisciplinaEquivalenteEmCurso.CODDISCEQV === disciplinaHistorico.CODDISC) {
                                    return disciplinaHistorico;
                                }
                            });

                        if (disciplinaEquivalencia && disciplinaEquivalente) {
                            if (!disciplinaEquivalente.Equivalencia) {
                                disciplinaEquivalente.Equivalencia = [];
                            }

                            disciplinaEquivalente.Equivalencia.push(disciplinaEquivalencia);
                        }
                    });
                }

                //Disciplinas equivalentes concluídas
                if (angular.isArray(lstGradeCurricular.SDiscEquivConcluidas)) {

                    angular.forEach(lstGradeCurricular.SDiscEquivConcluidas, function (infoDisciplinaEquivalenteEmCurso) {

                        var disciplinaEquivalencia = lstGradeCurricular.APRESENTACAOHISTORICO.find(function (disciplinaHistorico) {

                            if (infoDisciplinaEquivalenteEmCurso.CODDISC === disciplinaHistorico.CODDISC) {
                                return disciplinaHistorico;
                            }
                        }),
                            disciplinaEquivalente = lstGradeCurricular.APRESENTACAOHISTORICO.find(function (disciplinaHistorico) {

                                if (infoDisciplinaEquivalenteEmCurso.CODDISCEQV === disciplinaHistorico.CODDISC) {
                                    return disciplinaHistorico;
                                }
                            });

                        if (disciplinaEquivalencia && disciplinaEquivalente) {

                            if (!disciplinaEquivalente.Equivalencia) {
                                disciplinaEquivalente.Equivalencia = [];
                            }
                            disciplinaEquivalente.Equivalencia.push(disciplinaEquivalencia);
                        }
                    });
                }
            }

            ////////////////////////////////////////////////////////////////
            /// Funções privadas
            ////////////////////////////////////////////////////////////////

            /**
             * Retorna toda a grade curricular
             * @returns {array} Lista com toda a grade curricular
             */
            function getGradeCurricularTodos() {
                if (angular.isDefined(self.objGradeCurricular.APRESENTACAOHISTORICO)) {
                    parseToDecimal(self.objGradeCurricular.APRESENTACAOHISTORICO, ['CH', 'NUMCREDITOS']);
                    return self.objGradeCurricular.APRESENTACAOHISTORICO;
                }

                return [];
            }

            /**
             * Retorna as disciplinas em curso na grade curricular
             * @returns {array} Lista com todas as disciplinas em curso da grade curricular
             */
            function getGradeCurricularEmCurso() {
                var listToRemove = getListToExclude(self.EduStatusEnum.EmCurso);
                var discEmCurso = Object.create(self.objGradeCurricular.APRESENTACAOHISTORICO);
                for(var i = 0; i < discEmCurso.length-1; i++){
                    angular.forEach(listToRemove, function (objValue) {
                        if (discEmCurso[i] === objValue) {
                            discEmCurso.splice(i, 1);
                        }
                    });
                 }

                if (angular.isDefined(discEmCurso)) {
                    parseToDecimal(discEmCurso, ['CH', 'NUMCREDITOS']);
                    return $.grep(discEmCurso, function (value) {
                        return (value.STATUSENUM === self.EduStatusEnum.EmCurso || value.STATUSENUM === self.EduStatusEnum.SempreMostrar);
                    });
                }

                return [];
            }

            /**
             * Retorna as disciplinas pendentes na grade curricular
             * @returns {array} Lista com todas as disciplinas pendentes da grade curricular
             */
            function getGradeCurricularPendentes() {
                var listToRemove = getListToExclude(self.EduStatusEnum.Pendentes);
                var discPendentes = Object.create(self.objGradeCurricular.APRESENTACAOHISTORICO);

                for(var i = 0; i < discPendentes.length-1; i++){
                    angular.forEach(listToRemove, function (objValue) {
                        if (discPendentes[i] === objValue) {
                            discPendentes.splice(i, 1);
                        }
                    });
                }

                if (angular.isDefined(discPendentes)) {
                    parseToDecimal(discPendentes, ['CH', 'NUMCREDITOS']);
                    return $.grep(discPendentes, function (value) {
                        return (value.STATUSENUM === self.EduStatusEnum.Pendentes || value.STATUSENUM === self.EduStatusEnum.SempreMostrar);
                    });
                }
                return [];
            }

            /**
             * Retorna as disciplinas concluídas na grade curricular
             * @returns {array} Lista com todas as disciplinas concluídas da grade curricular
             */
            function getGradeCurricularConcluidos() {
                var listToRemove = getListToExclude(self.EduStatusEnum.Concluidos);
                var discConcluidos = Object.create(self.objGradeCurricular.APRESENTACAOHISTORICO);

                for(var i = 0; i < discConcluidos.length-1; i++){
                    angular.forEach(listToRemove, function (objValue) {
                        if (discConcluidos[i] === objValue) {
                            discConcluidos.splice(i, 1);
                        }
                    });
                }

                if (angular.isDefined(discConcluidos)) {
                    parseToDecimal(discConcluidos, ['CH', 'NUMCREDITOS']);
                    return $.grep(discConcluidos, function (value) {
                        return (value.STATUSENUM === self.EduStatusEnum.Concluidos || value.STATUSENUM === self.EduStatusEnum.SempreMostrar);
                    });
                }
                return [];
            }

            function getListToExclude(statusEnum){
                var listToRemove = $.grep(self.objGradeCurricular.APRESENTACAOHISTORICO, function (grupo) {
                    if (grupo.CODDISC == resumo) {
                        var discgrupo = $.grep(self.objGradeCurricular.APRESENTACAOHISTORICO, function (disc) {
                            if ((disc.STATUSENUM === statusEnum || disc.STATUSENUM === self.EduStatusEnum.SempreMostrar) &&
                                disc.CODDISC != resumo &&
                                disc.CODPERIODO == grupo.CODPERIODO &&
                                disc.SUBGRUPOCOD == grupo.SUBGRUPOCOD) {
                                    return disc;
                            }
                        });

                        if (discgrupo.length == 0)
                            return grupo;
                    }
                });
                return listToRemove;
            }

            /**
             * Realiza o parse de tipos decimal que estão como string
             * @param {array} lstGradeCurricular Lista com informações da grade
             * @param {array} lstToParse Lista com os campos a receberem o parse
             */
            function parseToDecimal(lstGradeCurricular, lstToParse) {
                angular.forEach(lstGradeCurricular, function (objValue) {
                    for (var i = 0; i < lstToParse.length; i++) {
                        if (objValue[lstToParse[i]]) {
                            var value = objValue[lstToParse[i]];
                            if (typeof value === 'string') {
                                value = value.replace('.', '');
                                value = value.replace(',', '.');
                                objValue[lstToParse[i]] = parseFloat(value);
                            }
                        }
                    }
                });
            }
        }
    });
