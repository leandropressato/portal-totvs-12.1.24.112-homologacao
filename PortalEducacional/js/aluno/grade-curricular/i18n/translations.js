[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Grade Curricular",
			"en": "Curriculum Grid",
			"es": "Mapa curricular"
        },
        "l-Nenhum-Registro-Encontrado": {
            "pt": "Nenhum Registro Encontrado!",
			"en": "No Record Found!",
			"es": "¡No se encontró ningún registro!"
        },
        "l-concluida": {
            "pt": "Concluída",
			"en": "Concluded",
			"es": "Concluida"
        },
        "l-nao-concluida": {
            "pt": "Não Concluída",
			"en": "Not Concluded",
			"es": "No concluida"
        },
        "l-pendente": {
            "pt": "Pendente",
			"en": "Pending",
			"es": "Pendiente"
        },
        "l-equivalente": {
            "pt": "Equivalente",
			"en": "Equivalent",
			"es": "Equivalente"
        },
        "l-todos": {
            "pt": "Todos",
			"en": "All",
			"es": "Todos"
        },
        "l-pendentes": {
            "pt": "Pendentes",
			"en": "Pending",
			"es": "Pendientes"
        },
        "l-em-curso": {
            "pt": "Em curso",
			"en": "In course",
			"es": "En curso"
        },
        "l-concluidas": {
            "pt": "Concluídas",
			"en": "Concluded",
			"es": "Concluidas"
        },
        "l-concluidos": {
            "pt": "Concluídos",
			"en": "Concluded",
			"es": "Concluidos"
        },
        "l-numero": {
            "pt": "Número",
			"en": "Number",
			"es": "Número"
        },
        "l-todas": {
            "pt": "Todas",
			"en": "All",
			"es": "Todas"
        },
        "l-disciplinas-obrigatorias": {
            "pt": "Disciplinas Obrigatórias",
			"en": "Required Course Subjects",
			"es": "Materias obligatorias"
        },
        "l-disciplinas-optativas": {
            "pt": "Disciplinas Eletivas/Optativas",
			"en": "Elective/Optional Course Subjects",
			"es": "Materias electivas/optativas"
        },
        "l-disciplinas-optativas-resumo": {
            "pt": "Disciplinas Optativas",
			"en": "Optional Course Subjects",
			"es": "Materias optativas"
        },
        "l-disciplinas-eletivas": {
            "pt": "Disciplinas Eletivas",
			"en": "Elective Course Subjects",
			"es": "Materias electivas"
        },
        "l-disciplinas-equivalentes": {
            "pt": "Disciplinas Equivalentes",
			"en": "Equivalent Course Subjects",
			"es": "Materias equivalentes"
        },
        "l-atividades-curriculares": {
            "pt": "Componentes Curriculares",
			"en": "Curriculum Components",
			"es": "Componentes curriculares"
        },
        "l-disciplinas-extras": {
            "pt": "Disciplinas Extras",
			"en": "Extra Course Subjects",
			"es": "Materias extras"
        },
        "l-situacao": {
            "pt": "Situação",
			"en": "Status",
			"es": "Situación"
        },
        "l-periodo-letivo": {
            "pt": "Período Letivo",
			"en": "School Period",
			"es": "Período lectivo"
        },
        "l-cod-disciplina": {
            "pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Course subjects",
			"es": "Materia"
        },
        "l-creditos": {
            "pt": "Créditos",
			"en": "Credits",
			"es": "Créditos"
        },
        "l-carga-horaria": {
            "pt": "CH",
			"en": "CH",
			"es": "CH"
        },
        "l-tipo": {
            "pt": "Tipo",
			"en": "Type",
			"es": "Tipo"
        },
        "l-faltas": {
            "pt": "Faltas",
			"en": "Absences",
			"es": "Faltas"
        },
        "l-nota-final": {
            "pt": "Nota Final",
			"en": "End Score",
			"es": "Nota final"
        },
        "l-conceito": {
            "pt": "Conceito",
			"en": "Concept",
			"es": "Concepto"
        },
        "l-periodo": {
            "pt": "Período",
			"en": "Period",
			"es": "Período"
        },
        "l-resumo": {
            "pt": "Resumo",
			"en": "Summary",
			"es": "Resumen"
        },
        "l-tcc": {
            "pt": "TCC - Trabalho de Conclusão de Curso",
			"en": "FP - Final Paper ",
			"es": "TCC - Trabajo de conclusión de curso"
        },
        "l-linha-pesquisa": {
            "pt": "Linha de Pesquisa",
			"en": "Line of Research",
			"es": "Línea de búsqueda"
        },
        "l-situacao-tcc": {
            "pt": "Situação do TCC",
			"en": "Final Paper Status",
			"es": "Situación del TCC"
        },
        "l-orientador": {
            "pt": "Orientador(a)",
			"en": "Advisor",
			"es": "Orientador(a)"
        },
        "l-data-banca": {
            "pt": "Data da Banca",
			"en": "Examination Board Date",
			"es": "Fecha de la mesa examinadora"
        },
        "l-local-banca": {
            "pt": "Local da Banca",
			"en": "Examination Board Location",
			"es": "Local de la mesa examinadora"
        },
        "l-expandir-todos": {
            "pt": "Expandir Todos",
			"en": "Expand All",
			"es": "Expandir todos"
        },
        "l-recolher-todos": {
            "pt": "Recolher Todos",
			"en": "Collect All",
			"es": "Pagar todos"
        },
        "l-frequencia": {
            "pt": "Frequência",
			"en": "Frequency",
			"es": "Frecuencia"
        },
        "l-nota-conceito": {
            "pt": "Nota/Conceito",
			"en": "Score/Concept",
			"es": "Nota/Concepto"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        },
        "l-habilitacao": {
            "pt": "Habilitação: ",
            "en": "Habilitação: ",
            "es": "Habilitação: "
        },
        "l-matriz-curricular": {
            "pt": "Matriz curricular: ",
            "en": "Matriz curricular: ",
            "es": "Matriz curricular: "
        },
        "l-data-ingresso": {
            "pt": "Data de ingresso: ",
            "en": "Data de ingresso: ",
            "es": "Data de ingresso: "
        },
        "l-turno": {
            "pt": "Turno: ",
            "en": "Turno: ",
            "es": "Turno: "
        },
        "l-tipo-ingresso": {
            "pt": "Tipo de ingresso: ",
            "en": "Tipo de ingresso: ",
            "es": "Tipo de ingresso: "
        },
        "l-coeficiente-rendimento": {
            "pt": "Coeficiente de rendimento: ",
            "en": "Coeficiente de rendimento: ",
            "es": "Coeficiente de rendimento: "
        },
        "l-media-global": {
            "pt": "Média global: ",
            "en": "Média global: ",
            "es": "Média global: "
        }
    }
]
