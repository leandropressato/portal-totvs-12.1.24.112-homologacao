/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduGradeCurricularModule
* @name eduGradeCurricularFactory
* @object factory
*
* @created 26/09/2016 v12.1.15
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada na Grade Curricular.
*/
define(['aluno/grade-curricular/grade-curricular.module'], function () {

    'use strict';

    angular
        .module('eduGradeCurricularModule')
        .factory('eduGradeCurricularFactory', EduGradeCurricularFactory);

    EduGradeCurricularFactory.$inject = ['$totvsresource'];

    function EduGradeCurricularFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/GradeCurricular/:method';
        var factory = $totvsresource.REST(url, {}, {});

        factory.gradeCurricularEnsinoSuperior = gradeCurricularEnsinoSuperior;
        factory.gradeCurricularEnsinoBasico = gradeCurricularEnsinoBasico;
        factory.historicoEnsinoBasico = historicoEnsinoBasico;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function gradeCurricularEnsinoSuperior(callback) {

            var parameters = { method: 'EnsinoSuperior' };
            return factory.TOTVSGet(parameters, callback);
        }

        function gradeCurricularEnsinoBasico(callback) {

            var parameters = { method: 'EnsinoBasico' };
            return factory.TOTVSQuery(parameters, callback);
        }

        function historicoEnsinoBasico(callback) {

            var parameters = { method: 'HistoricoEnsinoBasico' };
            return factory.TOTVSQuery(parameters, callback);
        }
    }
});
