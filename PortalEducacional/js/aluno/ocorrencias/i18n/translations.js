[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Ocorrências",
			"en": "Occurrences",
			"es": "Ocurrencias"
        },
        "btn-CIENTE": {
            "pt": "Ciente",
			"en": "Aware",
			"es": "Informado"
        },
        "l-DATAOCORRENCIA": {
            "pt": "Data da Ocorrência",
			"en": "Occurrence Date",
			"es": "Fecha de la ocurrencia"
        },
        "l-DESCGRUPOOCOR": {
            "pt": "Grupo",
			"en": "Group",
			"es": "Grupo"
        },
        "l-DISCIPLINA": {
            "pt": "Disciplina",
			"en": "Course Subject",
			"es": "Materia"
        },
        "l-NOMEPROF": {
            "pt": "Professor",
			"en": "Teacher",
			"es": "Profesor"
        },
        "l-CODTURMA": {
            "pt": "Turma",
			"en": "Class",
			"es": "Grupo"
        },
        "l-ETAPA": {
            "pt": "Etapa",
			"en": "Stage",
			"es": "Etapa"
        },
        "l-DTRESPONSAVELCIENTE": {
            "pt": "Data do Aceite",
			"en": "Acceptance Date",
			"es": "Fecha de la aceptación"
        },
        "l-NOMEUSUARIOCIENTE": {
            "pt": "Responsável Ciente",
			"en": "Responsible Party Aware",
			"es": "Responsable informado"
        },
        "l-OBSERVACOES": {
            "pt": "Observação",
			"en": "Note",
			"es": "Observación"
        },
        "l-AceiteValidado": {
            "pt": "Ciente",
			"en": "Aware",
			"es": "Informado"
        },
        "l-AguardandoAceite": {
            "pt": "Aguardando Aceite",
			"en": "Waiting Acceptance",
			"es": "Esperando aceptación"
        },
        "l-success-updated": {
            "pt": "atualizado(a) com sucesso", 
			"en": "successfully updated",
			"es": "actualizado(a) con éxito" 
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        }
    }
]