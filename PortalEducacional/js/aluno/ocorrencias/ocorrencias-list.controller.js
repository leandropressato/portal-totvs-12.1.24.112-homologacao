/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduOcorrenciasModule
 * @name EduOcorrenciasController
 * @object controller
 *
 * @created 26/09/2016 v12.1.15
 * @updated
 *
 * @requires ocorrencias.module
 *
 * @dependencies eduOcorrenciasFactory
 *
 * @description Controller do Ocorrencias
 */
define(['aluno/ocorrencias/ocorrencias.module',
    'aluno/ocorrencias/ocorrencias.factory',
    'aluno/matricula/matricula-disciplina.service',
    'utils/edu-enums.constants',
    'utils/edu-utils.factory',
    'utils/reports/edu-relatorio.service',
    'widgets/widget.constants',
    'utils/edu-constantes-globais.constants'
], function () {

    'use strict';

    angular
        .module('eduOcorrenciasModule')
        .controller('EduOcorrenciasController', EduOcorrenciasController);

    EduOcorrenciasController.$inject = [
        'TotvsDesktopContextoCursoFactory',
        'eduOcorrenciasFactory',
        'MatriculaDisciplinaService',
        'eduEnumsConsts',
        'totvs.app-notification.Service',
        'i18nFilter',
        '$state',
        'eduUtilsFactory',
        'eduRelatorioService',
        'eduWidgetsConsts',
        'eduConstantesGlobaisConsts',
        '$rootScope',
        'totvs.app-notification.Service'
    ];

    function EduOcorrenciasController(objContextoCursoFactory, eduOcorrenciasFactory, MatriculaDisciplinaService,
        EduEnumsConsts, notification, i18nFilter, $state, eduUtilsFactory,
        eduRelatorioService, eduWidgetsConsts, eduConstantesGlobaisConsts, $rootScope, totvsNotification) {

        var self = this,
        codRelatorioImpressao = null,
        codColRelatorioImpressao = null;

        self.objContexto = {};
        self.periodoLetivo = 0;
        self.intOcorrenciasFiltradas = 0;
        self.intOcorrenciasTotal = 0;
        self.objOcorrenciasList = [];
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Ocorrencias;

        self.getContexto = getContexto;
        self.retornaWidgetUrl = retornaWidgetUrl;
        self.periodoLetivoAlterado = periodoLetivoAlterado;
        self.getOcorrencias = getOcorrencias;
        self.responsavelCiente = responsavelCiente;
        self.aguardandoAceite = aguardandoAceite;
        self.detalhesDisciplina = detalhesDisciplina;
        self.respCienteVisible = respCienteVisible;
        self.invisibleLegend = invisibleLegend;
        self.permiteImprimir = false;
        self.imprimir = imprimir;

        init();

        function init() {
            carregarParametrosEdu();

            if ($state.params.idTurmaDisc && $state.params.idTurmaDisc !== -1) {
                self.disciplina = $state.params.idTurmaDisc;
                self.ocultaCompTela = true;
            } else {
                self.apresentaSelecaoPeriodo = false;
            }

            self.getContexto();
            self.getOcorrencias();
        }

        /* Valida se o usuário tem permissão no Menu */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_OCORRENCIAS) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        function getContexto() {
            self.objContexto = objContextoCursoFactory.getCursoSelecionado();
        }

        /* Carregar parâmetros do Educacional */
        function carregarParametrosEdu () {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function(parametros){
                codRelatorioImpressao = parametros.CodRelatorioConsultaOcorrenciaReports;
                codColRelatorioImpressao = parametros.CodColRelatorioConsultaOcorrenciaReports;
                self.permiteImprimir = !!codRelatorioImpressao;
            });
        }

        function retornaWidgetUrl() {
            return 'js/aluno/widgets/widget-area.view.html';
        }

        function periodoLetivoAlterado() {
            self.getOcorrencias();
        }

        function detalhesDisciplina(idTurmaDisc) {
            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(self.objContexto.cursoSelecionado.CODCOLIGADA,
                idTurmaDisc, self.objOcorrenciasList.RA);
        }

        function getOcorrencias() {
            if (self.periodoLetivo !== 0) {
                
                eduOcorrenciasFactory.findRecords($state.params.idTurmaDisc, function (result) {
                    if (result) {
                        self.objOcorrenciasList = [];
                        angular.forEach(result, function (value) {

                            if (value && value.$length) {
                                self.intOcorrenciasTotal = value.$length;
                            }

                            if (self.disciplina) {
                                if (self.disciplina !== -1 && self.disciplina === value.IDTURMADISC) {
                                    self.objOcorrenciasList.push(value);
                                }
                            } else {
                                self.objOcorrenciasList.push(value);
                            }
                        });

                        self.intOcorrenciasFiltradas = self.objOcorrenciasList.length;
                    }
                });
            }
        }

        function responsavelCiente(objOcorrencia) {
            var dataset = {};
            dataset['dsOcorrencia'] = new Array(objOcorrencia);
            eduOcorrenciasFactory.updateRecord(objOcorrencia.id, dataset,
                function (result) {

                    if (result) {
                        if (angular.isDefined(result.exception)) {
                            notification.notify({
                                type: 'error',
                                title: i18nFilter('l-titulo', [], 'js/aluno/ocorrencias'),
                                detail: result.message
                            });
                        } else {
                            objOcorrencia.RESPONSAVELCIENTE = result.SOcorrenciaAluno[0].RESPONSAVELCIENTE;
                            objOcorrencia.CODUSUARIOCIENTE = result.SOcorrenciaAluno[0].CODUSUARIOCIENTE;
                            objOcorrencia.NOMEUSUARIOCIENTE = result.SOcorrenciaAluno[0].NOMEUSUARIOCIENTE;
                            objOcorrencia.DTRESPONSAVELCIENTE = result.SOcorrenciaAluno[0].DTRESPONSAVELCIENTE;

                            notification.notify({
                                type: 'success',
                                title: i18nFilter('l-titulo', [], 'js/aluno/ocorrencias'),
                                detail: i18nFilter('l-titulo', [], 'js/aluno/ocorrencias') + ': ' + objOcorrencia.DESCTIPOOCOR + ', ' +
                                    i18nFilter('l-success-updated', [], 'js/aluno/ocorrencias') + '!'
                            });
                        }
                    }
                });
        }

        function aguardandoAceite(objOcorrencias) {
            var blnReturn = (self.objContexto.entrarComo === EduEnumsConsts.EduTipoUsuarioPortalEnum.Responsavel &&
                objOcorrencias.RESPONSAVELCIENTE !== EduEnumsConsts.EduSimOuNaoEnum.Sim);
            return blnReturn;
        }

        function respCienteVisible(objOcorrencias) {
            var blnReturn = (parseInt(self.objContexto.cursoSelecionado.APRESENTACAO) === EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico &&
                objOcorrencias.RESPONSAVELCIENTE === EduEnumsConsts.EduSimOuNaoEnum.Sim);
            return blnReturn;
        }

        function invisibleLegend() {
            var blnReturn = (parseInt(self.objContexto.cursoSelecionado.APRESENTACAO) === EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoSuperior &&
                self.objContexto.entrarComo !== EduEnumsConsts.EduTipoUsuarioPortalEnum.Responsavel);
            return !blnReturn;
        }

        /* Executa emissão do relatório associado a visão(Parametrizado no sistema). */
        function imprimir() {
            if(codRelatorioImpressao){
                eduRelatorioService.emitirRelatorio(codRelatorioImpressao, codColRelatorioImpressao);
            }
        }
    }
});
