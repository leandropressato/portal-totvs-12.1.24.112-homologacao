/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduOcorrenciasModule
* @name eduOcorrenciasFactory
* @object factory
*
* @created 26/09/2016 v12.1.15
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada nas Ocorrencias.
*/
define(['aluno/ocorrencias/ocorrencias.module'], function () {

    'use strict';

    angular
        .module('eduOcorrenciasModule')
        .factory('eduOcorrenciasFactory', EduOcorrenciasFactory);

    EduOcorrenciasFactory.$inject = ['$totvsresource'];

    function EduOcorrenciasFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method';
        var factory = $totvsresource.REST(url, {}, {});

        factory.findRecords = findRecords; // Busca todas as ocorrencias
        factory.updateRecord = updateRecord; // Atualiza uma ocorrencia

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function findRecords(idTurmaDisc, callback) {
            var pIdTurmaDisc = idTurmaDisc || 0;

            var parameters = {};
            parameters['method'] = 'Ocorrencias';
            parameters['idTurmaDisc'] = pIdTurmaDisc;

            return factory.TOTVSQuery(parameters, callback);
        }

        function updateRecord(id, model, callback) {
            var parameters = {
                method: 'ResponsavelCiente',
                id: id
            };

            return factory.TOTVSUpdate(parameters, model, callback);
        }
    }
});
