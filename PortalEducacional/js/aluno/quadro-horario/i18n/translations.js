[
    {
        "l-home": {
          "pt": "Início",
		  "en": "Start",
		  "es": "Inicio"
        },
        "l-titulo": {
          "pt": "Quadro de Horário",
		  "en": "Timetable",
		  "es": "Cuadro de horario"
        },
        "l-horario": {
            "pt": "Horário",
			"en": "Schedule",
			"es": "Horario"
        },
        "l-segunda": {
            "pt": "Segunda",
			"en": "Monday",
			"es": "Lunes"
        },
        "l-terca": {
            "pt": "Terça",
			"en": "Tuesday",
			"es": "Martes"
        },
        "l-quarta": {
            "pt": "Quarta",
			"en": "Wednesday",
			"es": "Miércoles"
        },
        "l-quinta": {
            "pt": "Quinta",
			"en": "Thursday",
			"es": "Jueves"
        },
        "l-sexta": {
            "pt": "Sexta",
			"en": "Friday",
			"es": "Viernes"
        },
        "l-sabado": {
            "pt": "Sábado",
			"en": "Saturday",
			"es": "Sábado"
        },
        "l-domingo": {
            "pt": "Domingo",
			"en": "Sunday",
			"es": "Domingo"
        },
        "l-subturma": {
            "pt": "Subturma",
			"en": "Sub-class",
			"es": "Subgrupo"
        },
        "l-maisInfomacoesTurmaDisc": {
          "pt": "Ver mais informações",
		  "en": "See further information",
		  "es": "Ver más información"
        },
        "l-quadroHorario": {
          "pt": "Quadro de Horário",
		  "en": "Timetable",
		  "es": "Cuadro de horario"
        },
        "l-disciplina-modular": {
            "pt": "Disciplina Modular",
			"en": "Modular course subject",
			"es": "Materia modular"
        },
        "l-Nenhum-Registro-Encontrado": {
            "pt": "Nenhum registro encontrado!",
			"en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },
        "l-mostrar-todos": {
            "pt": "Mostrar todos os horários independente da data de término",
			"en": "Display all schedules irrespective of the end date",
			"es": "Mostrar todos los horarios independientemente de la fecha de finalización"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        }
    }
]
