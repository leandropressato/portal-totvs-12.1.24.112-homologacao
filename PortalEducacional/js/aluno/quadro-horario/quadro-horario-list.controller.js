/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduQuadroHorarioModule
 * @name QuadroHorarioController
 * @object controller
 *
 * @created 2016-09-05 v12.1.15
 * @updated
 *
 * @requires quadro-horario.module
 *
 * @dependencies QuadroHorarioFactory
 *
 * @description Controller do Quadro de horário
 */
define(['utils/edu-utils.service',
        'aluno/quadro-horario/quadro-horario.module',
        'aluno/quadro-horario/quadro-horario.route',
        'aluno/quadro-horario/quadro-horario.factory',
        'aluno/matricula/matricula-disciplina.service',
        'utils/edu-utils.factory',
        'utils/reports/edu-relatorio.service',
        'widgets/widget.constants',
        'utils/edu-constantes-globais.constants'], function () {

    'use strict';

    angular
        .module('eduQuadroHorarioModule')
        .controller('QuadroHorarioController', QuadroHorarioController);

    QuadroHorarioController.$inject = [
        '$scope',
        '$filter',
        'i18nFilter',
        '$timeout',
        'QuadroHorarioFactory',
        'eduUtilsService',
        'MatriculaDisciplinaService',
        'eduUtilsFactory',
        'eduRelatorioService',
        'eduWidgetsConsts',
        '$rootScope',
        'totvs.app-notification.Service',
        'eduConstantesGlobaisConsts',
        '$state'
    ];

    function QuadroHorarioController($scope,
        $filter,
        i18nFilter,
        $timeout,
        QuadroHorarioFactory,
        EduUtilsService,
        MatriculaDisciplinaService,
        eduUtilsFactory,
        eduRelatorioService,
        eduWidgetsConsts,
        $rootScope,
        totvsNotification,
        eduConstantesGlobaisConsts,
        $state) {

        if (!Array.prototype.find) {
            Array.prototype.find = function (predicate) {
                if (this === null) {
                    throw new TypeError('Array.prototype.find called on null or undefined');
                }
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }
                var list = Object(this);
                var length = list.length >>> 0;
                var thisArg = arguments[1];
                var value;

                for (var i = 0; i < length; i++) {
                    value = list[i];
                    if (predicate.call(thisArg, value, i, list)) {
                        return value;
                    }
                }
                return undefined;
            };
        }


        var self = this,
            _listaHorarios = {},
            codRelatorioImpressao = null,
            codColRelatorioImpressao = null;

        self.QuadroHorario = {};
        self.diaSemanaHoje = 0;
        self.temHorarioSabado = false;
        self.temHorarioDomingo = false;
        self.temDisciplinaModular = false;
        self.countHorarios = 0;
        self.mostrarTodos = false;

        self.preencherQuadroHorario = preencherQuadroHorario;
        self.retornaInfoFaixaHorario = retornaInfoFaixaHorario;
        self.retornaInfoDisciplinasSegunda = retornaInfoDisciplinasSegunda;
        self.retornaInfoDisciplinasTerca = retornaInfoDisciplinasTerca;
        self.retornaInfoDisciplinasQuarta = retornaInfoDisciplinasQuarta;
        self.retornaInfoDisciplinasQuinta = retornaInfoDisciplinasQuinta;
        self.retornaInfoDisciplinasSexta = retornaInfoDisciplinasSexta;
        self.retornaInfoDisciplinasSabado = retornaInfoDisciplinasSabado;
        self.retornaInfoDisciplinasDomingo = retornaInfoDisciplinasDomingo;
        self.abrirDetalhesMatricula = abrirDetalhesMatricula;
        self.hideColumns = hideColumns;

        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.QuadroHorarios;

        self.periodoLetivo = 0;
        self.periodoLetivoAlterado = periodoLetivoAlterado;

        self.permiteImpressao = false;
        self.imprimir = imprimir;

        init();

        function init() {
            setDiaSemanaHoje();
            carregarParametrosEducacional();
        }

        /* Valida se o usuário tem permissão no Menu */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_QUADROHORARIOS) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        function setDiaSemanaHoje() {
            var dttNow = new Date();
            self.diaSemanaHoje = dttNow.getDay();
        }

        function carregarParametrosEducacional() {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (parametros) {
                codRelatorioImpressao = parametros.CodRelatorioQuadroHorarioReports;
                codColRelatorioImpressao = parametros.CodColRelatorioQuadroHorarioReports;
                self.permiteImpressao = !!codRelatorioImpressao;
            });
        }

        function retornaWidgetUrl() {
            return 'js/aluno/widgets/widget-area.view.html';
        }

        function retornaInfoFaixaHorario(item) {
            return item.retornarTemplateFaixaHorario();
        }

        function retornaInfoDisciplinasSegunda(item) {
            return item.retornarTemplateHorario('segunda');
        }

        function retornaInfoDisciplinasTerca(item) {
            return item.retornarTemplateHorario('terca');
        }

        function retornaInfoDisciplinasQuarta(item) {
            return item.retornarTemplateHorario('quarta');
        }

        function retornaInfoDisciplinasQuinta(item) {
            return item.retornarTemplateHorario('quinta');
        }

        function retornaInfoDisciplinasSexta(item) {
            return item.retornarTemplateHorario('sexta');
        }

        function retornaInfoDisciplinasSabado(item) {
            return item.retornarTemplateHorario('sabado');
        }

        function retornaInfoDisciplinasDomingo(item) {
            return item.retornarTemplateHorario('domingo');
        }

        function preencherQuadroHorario() {

            QuadroHorarioFactory.buscaHorarioAluno({}, function (result) {

                self.mostrarTodos = !self.mostrarTodos;

                _listaHorarios = result;
                if (angular.isDefined(_listaHorarios["SHorarioAluno"]))
                    self.countHorarios = _listaHorarios["SHorarioAluno"].length;

                self.QuadroHorario = new QuadroHorario();
                self.QuadroHorario.preencherDados(_listaHorarios);
                self.hideColumns();
            });
        }

        function hideColumns() {
            if (!self.temHorarioSabado || !self.temHorarioDomingo) {
                $timeout(function () {

                    if (!self.temHorarioSabado) {
                        $('#grid table colgroup col:nth-child(7)').hide();
                        $('#grid table th:nth-child(7)').hide();
                        $('#grid table td:nth-child(7)').hide();
                    }

                    if (!self.temHorarioDomingo) {
                        $('#grid table colgroup col:nth-child(8)').hide();
                        $('#grid table th:nth-child(8)').hide();
                        $('#grid table td:nth-child(8)').hide();
                    }

                }, 100);
            }
        }

        function periodoLetivoAlterado() {
            preencherQuadroHorario();
        }

        function abrirDetalhesMatricula(idTurmaDisc, idHorarioTurma) {
            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplinaComHorario(idTurmaDisc, idHorarioTurma);
        }

        function buscarIdTurmaDisc(codDisc) {
            var tDisc = _listaHorarios['SDescDisc'].find(function (turmaDisc) {
                if (turmaDisc.CODDISC === codDisc) {
                    return turmaDisc;
                }
            });

            if (tDisc && tDisc['IDTURMADISC']) {
                return tDisc.IDTURMADISC;
            }
        }

        function QuadroHorario() {

            this.Horarios = [];
            this.preencherDados = preencherDados;

            function preencherDados(listaHorarios) {
                listaHorarios['SHorarioAluno'].forEach(function (horario) {

                    var horarioExistente = this.Horarios.find(function (valor) {
                        var _faixaHorario = horario.HORAINICIAL + '<br />' + horario.HORAFINAL;
                        return (valor.faixaHorario === _faixaHorario);
                    });

                    if (!horarioExistente) {
                        var faixahorario = new Horario(horario);
                        faixahorario.adicionarHorario(horario);
                        this.Horarios.push(faixahorario);
                    } else {
                        horarioExistente.adicionarHorario(horario);
                    }

                }, this);
            }
        }

        function Horario(horario) {
            this.faixaHorario = horario.HORAINICIAL + '<br />' + horario.HORAFINAL;
            this.segunda = [];
            this.terca = [];
            this.quarta = [];
            this.quinta = [];
            this.sexta = [];
            this.sabado = [];
            this.domingo = [];

            this.adicionarHorario = adicionarHorario;
            this.retornarTemplateFaixaHorario = retornarTemplateFaixaHorario;
            this.retornarTemplateHorario = retornarTemplateHorario;

            function adicionarHorario(horarioDia) {

                horarioDia.idTurmaDisc = buscarIdTurmaDisc(horarioDia.CODDISC);

                switch (horarioDia.DIASEMANA) {
                    case '1':
                        this.domingo.push(horarioDia);
                        self.temHorarioDomingo = true;
                        break;
                    case '2':
                        this.segunda.push(horarioDia);
                        break;
                    case '3':
                        this.terca.push(horarioDia);
                        break;
                    case '4':
                        this.quarta.push(horarioDia);
                        break;
                    case '5':
                        this.quinta.push(horarioDia);
                        break;
                    case '6':
                        this.sexta.push(horarioDia);
                        break;
                    case '7':
                        this.sabado.push(horarioDia);
                        self.temHorarioSabado = true;
                        break;
                }
            }

            function retornarTemplateFaixaHorario() {
                return '<div class="quadro-horario-faixahorario">' + this.faixaHorario + '</div>';
            }

            function retornarTemplateHorario(dia) {

                var _template = '',
                    horarioModular,
                    adicionarHorario;

                this[dia].forEach(function (disciplina) {

                    // verifica se é horário modular
                    horarioModular = (disciplina.DATAINICIAL != null && disciplina.DATAFINAL != null);

                    if (horarioModular) {
                        // se for horário modular, verifica se ele deve ser adicionado
                        adicionarHorario = (new Date(disciplina.DATAFINAL) >= new Date());

                        if (adicionarHorario)
                            self.temDisciplinaModular = true;

                    } else {
                        // se não for horário modular, sempre deve adicionar o horário na listagem
                        adicionarHorario = true;
                    }

                    if (adicionarHorario || self.mostrarTodos) {

                        _template += '<span ' +
                                        'ng-click="controller.abrirDetalhesMatricula(' + disciplina.IDTURMADISC + ',' + disciplina.IDHORARIOTURMA + ')">' +
                                        '<span ' +
                                            'class="' + ((horarioModular) ? 'glyphicon glyphicon-asterisk' : 'glyphicon glyphicon-triangle-right')  + '">' +
                                        '</span> ' +
                                        '<span ' +
                                            'class="quadro-horario-disciplina">' +
                                            EduUtilsService.escapeHtml(disciplina.NOME) +
                                        '</span>' +
                                    '</span>' +
                                    '<br />';

                        if (horarioModular) {
                            _template += '<span ' +
                                            'class="quadro-horario-infoPredioSala">' +
                                                $filter('date')(disciplina.DATAINICIAL, 'dd/MM/yyyy') + ' - ' + $filter('date')(disciplina.DATAFINAL, 'dd/MM/yyyy') +
                                        '</span>' +
                                        '<br />';
                        }

                        if (disciplina.CODSUBTURMA) {
                            _template += '<span ' +
                                'class="quadro-horario-infoPredioSala">' +
                                i18nFilter('l-subturma', [], 'js/aluno/quadro-horario') + ': ' +
                                EduUtilsService.escapeHtml(disciplina.CODSUBTURMA) +
                                '</span>' +
                                '<br />';
                        }

                    }

                });

                return _template;
            }
        }

        /* Executa e emite o relatório parametrizado no Educacional */
        function imprimir() {
            if (codRelatorioImpressao){
                eduRelatorioService.emitirRelatorio(codRelatorioImpressao, codColRelatorioImpressao);
            }
        }
    }
});
