/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

/**
* @module eduQuadroHorarioModule
* @name QuadroHorarioFactory
* @object factory
*
* @created 2016-09-05 v12.1.15
* @updated
*
* @requires eduQuadroHorarioModule
*
* @dependencies QuadroHorarioFactory
*
* @description Factory utilizada no Quadro de horário.
*/

define(['aluno/quadro-horario/quadro-horario.module'], function () {

    'use strict';

    angular
        .module('eduQuadroHorarioModule')
        .factory('QuadroHorarioFactory', QuadroHorarioFactory);

    QuadroHorarioFactory.$inject = ['$totvsresource'];

    function QuadroHorarioFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.buscaHorarioAluno = buscaHorarioAluno;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function buscaHorarioAluno(parameters, callback) {
            parameters.method = 'QuadroHorarioAluno';
            factory.TOTVSGet(parameters, callback);
        }
    }
});
