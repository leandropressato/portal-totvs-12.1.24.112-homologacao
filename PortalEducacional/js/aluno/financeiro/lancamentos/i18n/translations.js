[{
    "l-titulo": {
        "pt": "Lançamentos",
		"en": "Entries",
		"es": "Asientos"
    },
    "l-data-vencimento": {
        "pt": "Data vencimento",
		"en": "Expiration date",
		"es": "Fecha de vencimiento"
    },
    "l-valor-bruto": {
        "pt": "Valor bruto",
		"en": "Gross value",
		"es": "Valor bruto"
    },
    "l-valor-pagar": {
        "pt": "Valor à pagar",
		"en": "Value payable",
		"es": "Valor por pagar"
    },
    "l-linha-digitavel": {
        "pt": "Linha digitável",
		"en": "Line to enter",
		"es": "Línea digitable"
    },
    "l-periodo-letivo": {
        "pt": "Período letivo",
		"en": "School period",
		"es": "Período lectivo"
    },
    "l-situacao": {
        "pt": "Situação",
		"en": "Status",
		"es": "Situación"
    },
    "l-vencimento": {
        "pt": "Vencimento",
		"en": "Expiration",
		"es": "Vencimiento"
    },
    "l-responsavel": {
        "pt": "Responsável",
		"en": "Responsible party",
		"es": "Responsable"
    },
    "l-servico": {
        "pt": "Serviço",
		"en": "Service",
		"es": "Servicio"
    },
    "l-parcela": {
        "pt": "Parcela",
		"en": "Installment",
		"es": "Cuota"
    },
    "l-cota": {
        "pt": "Cota",
		"en": "Quota",
		"es": "Cota"
    },
    "l-competencia": {
        "pt": "Competência",
		"en": "Reference",
		"es": "Competencia"
    },
    "l-dtbaixa": {
        "pt": "Dt. Baixa",
		"en": "Issue Date",
		"es": "Fch. Baja"
    },
    "l-valordesconto": {
        "pt": "Valor de desconto",
		"en": "Discount value",
		"es": "Valor de descuento"
    },
    "l-juros": {
        "pt": "Juros",
		"en": "Interests",
		"es": "Intereses"
    },
    "l-multa": {
        "pt": "Multa",
		"en": "Fine",
		"es": "Multa"
    },
    "l-desconto": {
        "pt": "Desconto",
		"en": "Discount",
		"es": "Descuento"
    },
    "l-baixado": {
        "pt": "Baixado",
		"en": "Issued",
		"es": "Dado de baja"
    },
    "l-bolsa": {
        "pt": "Bolsa",
		"en": "Scholarship",
		"es": "Beca"
    },
    "l-filtrarpor": {
        "pt": "Filtrar por",
		"en": "Filter by",
		"es": "Filtrar por"
    },
    "l-pago": {
        "pt": "Pago",
		"en": "Paid",
		"es": "Pagado"
    },
    "l-aberto": {
        "pt": "Em aberto",
		"en": "Pending",
		"es": "Pendiente"
    },
    "l-pago-parcialmente": {
        "pt": "Pago parcialmente",
		"en": "Partially paid",
		"es": "Pagado parcialmente"
    },
    "l-cancelado": {
        "pt": "Cancelado",
		"en": "Canceled",
		"es": "Anulado"
    },
    "l-inativo": {
        "pt": "Inativo",
		"en": "Inactive",
		"es": "Inactivo"
    },
    "l-boleto": {
        "pt": "Boleto",
		"en": "Bank slip",
		"es": "Boleta"
    },
    "l-redirecionarbanco": {
        "pt": "Você será redirecionado para o site do banco da instituição para emissão do seu boleto. Deseja continuar?",
		"en": "You will be redirected for the site of the institution bank for the issue of your bank slip. Do you want to proceed?",
		"es": "Usted será reorientado al sitio del banco de la institución para la emisión de su boleta. ¿Desea continuar?"
    },
    "l-pagar": {
        "pt": "Pagar",
		"en": "Pay",
		"es": "Pagar"
    },
    "l-valorop1": {
        "pt": "Valor Opcional 1",
		"en": "Optional Value 1",
		"es": "Valor opcional 1"
    },
    "l-valorop2": {
        "pt": "Valor Opcional 2",
		"en": "Optional Value 2",
		"es": "Valor opcional 2"
    },
    "l-valorop3": {
        "pt": "Valor Opcional 3",
		"en": "Optional Value 3",
		"es": "Valor opcional 3"
    },
    "l-valorop4": {
        "pt": "Valor Opcional 4",
		"en": "Optional Value 4",
		"es": "Valor opcional 4"
    },
    "l-valorop5": {
        "pt": "Valor Opcional 5",
		"en": "Optional Value 5",
		"es": "Valor opcional 5"
    },
    "l-valorop6": {
        "pt": "Valor Opcional 6",
		"en": "Optional Value 6",
		"es": "Valor opcional 6"
    },
    "l-valorop7": {
        "pt": "Valor Opcional 7",
		"en": "Optional Value 7",
		"es": "Valor opcional 7"
    },
    "l-valorop8": {
        "pt": "Valor Opcional 8",
		"en": "Optional Value 8",
		"es": "Valor opcional 8"
    },
    "l-listarAbertos": {
        "pt": "Boletos em aberto de todos os períodos letivos",
		"en": "Pending bank slips related to school periods",
		"es": "Boletas pendientes de todos los períodos lectivos"
    },
    "l-acrescimo-acordo": {
        "pt": "Acréscimo do acordo",
		"en": "Increase of the agreement",
		"es": "Aumento del acuerdo"
    },
    "l-juros-acordo": {
        "pt": "Juros do acordo",
		"en": "Interests of agreement",
		"es": "Interés del acuerdo"
    },
    "l-desconto-acordo": {
        "pt": "Desconto do acordo",
		"en": "Discount of the agreement",
		"es": "Descuento del acuerdo"
    },
    "l-pagamento": {
        "pt": "Pagamento",
		"en": "Payment",
		"es": "Pago"
    },
    "l-dadoscadastrais": {
        "pt": "Dados cadastrais",
		"en": "Register data",
		"es": "Datos de registro"
    },
    "l-nome": {
        "pt": "Nome",
		"en": "Name",
		"es": "Nombre"
    },
    "l-primeiro-nome": {
        "pt": "Primeiro nome",
        "en": "Primeiro nome",
		"es": "Primeiro nome"
    },
    "l-sobrenome": {
        "pt": "Sobrenome",
        "en": "Sobrenome",
		"es": "Sobrenome"
    },    
    "l-email": {
        "pt": "E-Mail",
		"en": "E-mail",
		"es": "E-Mail"
    },
    "l-bairro": {
        "pt": "Bairro",
		"en": "District",
		"es": "Barrio"
    },
    "l-uf": {
        "pt": "UF",
		"en": "State",
		"es": "Est/Prov/Reg"
    },
    "l-telefone": {
        "pt": "Telefone",
		"en": "Phone number",
		"es": "Teléfono"
    },
    "l-cpf": {
        "pt": "CPF",
		"en": "CPF",
		"es": "RCPF"
    },
    "l-endereco": {
        "pt": "Endereço",
		"en": "Address",
		"es": "Dirección"
    },
    "l-cidade": {
        "pt": "Cidade",
		"en": "City",
		"es": "Ciudad"
    },
    "l-cep": {
        "pt": "CEP",
		"en": "Zip Code",
		"es": "CP"
    },
    "l-dadospagamento": {
        "pt": "Dados do pagamento",
		"en": "Payment data",
		"es": "Datos del pago"
    },
    "l-referentea": {
        "pt": "Referente a",
		"en": "Related to",
		"es": "Referente a"
    },
    "l-valortotal": {
        "pt": "Valor total: ",
		"en": "Total value:",
		"es": "Valor total: "
    },
    "l-valortotal2": {
        "pt": "Valor total",
		"en": "Total value",
		"es": "Valor total"
    },
    "l-valor": {
        "pt": "Valor",
		"en": "Value",
		"es": "Valor"
    },
    "l-numparcelas": {
        "pt": "Nº de parcelas",
		"en": "Number of Installments",
		"es": "Nº de cuotas"
    },
    "l-formapagamento": {
        "pt": "Forma de pagamento",
		"en": "Payment method",
		"es": "Forma de pago"
    },
    "l-cartao": {
        "pt": "Cartão",
		"en": "Card",
		"es": "Tarjeta"
    },
    "l-credito": {
        "pt": "Cartão de Crédito",
		"en": "Credit Card",
		"es": "Tarjeta de crédito"
    },
    "l-debito": {
        "pt": "Cartão de Débito",
		"en": "Debit Card",
		"es": "Tarjeta de débito"
    },
    "l-parcelamento": {
        "pt": "Parcelas",
		"en": "Installments",
		"es": "Cuotas"
    },
    "l-erro": {
        "pt": "Ocorreu erro na consulta/captura da transação",
		"en": "Error in query/capture of transaction",
		"es": "Ocurrió un error en la consulta/captura de la transacción"
    },
    "l-codigo": {
        "pt": "Código",
		"en": "Code",
		"es": "Código"
    },
    "l-mensagem": {
        "pt": "Mensagem",
		"en": "Message",
		"es": "Mensaje"
    },
    "l-pagnaoautenticado": {
        "pt": "Pagamento não autenticado",
		"en": "Payment not authenticated",
		"es": "Pago no autenticado"
    },
    "l-pagnaoautorizado": {
        "pt": "Pagamento não autorizado",
		"en": "Payment not authorized",
		"es": "Pago no autorizado"
    },
    "l-pagcapturado": {
        "pt": "Pagamento realizado com sucesso",
		"en": "Payment successfully made",
		"es": "Pago realizado con éxito"
    },
    "l-pagemautenticacao": {
        "pt": "Pagamento em autenticação",
		"en": "Payment in authentication",
		"es": "Pago en autenticación"
    },
    "l-respfinanceiro": {
        "pt": "Resp. financeiro",
		"en": "Financial responsible party",
		"es": "Resp. Financiero"
    },
    "l-datahora": {
        "pt": "Data/hora",
		"en": "Date/time",
		"es": "Fecha/hora"
    },
    "l-tid": {
        "pt": "Código de Verificação",
		"en": "Verification Code",
		"es": "Código de verificación"
    },
    "l-numpedido": {
        "pt": "Número do pedido",
		"en": "Order number",
		"es": "Número del pedido"
    },
    "l-numautor": {
        "pt": "Número de autorização",
		"en": "Authorzation number",
		"es": "Número de autorización"
    },
    "l-nsu": {
        "pt": "Número Sequencial Único",
		"en": "Single Sequence Number",
		"es": "Número secuencial único"
    },
    "l-produto": {
        "pt": "Produto",
		"en": "Product",
		"es": "Producto"
    },
    "l-semresultado": {
        "pt": "Sem resultado",
		"en": "No result",
		"es": "Sin resultado"
    },
    "l-msgsemresultado": {
        "pt": "Aconteceu um erro ao recuperar os dados do serviço da Cielo",
		"en": "Error recovering Cielo data service",
		"es": "Ocurrió un error al recuperar los datos del servicio de Cielo"
    },
    "l-semcodigotransacao": {
        "pt": "Sem código de transação Cielo",
		"en": "No code of transaction Cielo",
		"es": "Sin código de transacción Cielo"
    },
    "l-msgsemcodigotransacao": {
        "pt": "Não existe código de transação cielo",
		"en": "No code of transaction Cielo",
		"es": "No existe código de transacción cielo"
    },
    "l-boleto-nao-registrado": {
        "pt": "O boleto não foi registrado e não pode ser exibido.",
		"en": "The bank slip was not registered and cannot be displayed.",
		"es": "La boleta no se registró y no puede mostrarse."
    },
    "l-erro-gerar-boleto": {
        "pt": "Erro ao tentar gerar o boleto.",
		"en": "Error trying to generate bank slip.",
		"es": "Error al intentar generar la boleta."
    },
    "l-numero": {
        "pt": "Número",
        "en": "Número",
        "es": "Número"
    },
    "l-Logradouro": {
        "pt": "Logradouro",
        "en": "Logradouro",
        "es": "Logradouro"
    },
    "l-complemento": {
        "pt": "Complemento",
        "en": "Complemento",
        "es": "Complemento"
    },    
    "l-pais": {
        "pt": "País",
        "en": "País",
        "es": "País"
    },
    "l-continuar" : {
        "pt": "Continuar",
        "en": "Continuar",
        "es": "Continuar"
    }
}]