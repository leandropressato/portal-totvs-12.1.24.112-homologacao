[
    {
        "l-titulo": {
            "pt": "Benefícios",
			"en": "Benefits",
			"es": "Beneficios"
        },
        "l-ativo": {
            "pt": "Ativo",
			"en": "Active",
			"es": "Activo"
        },
        "l-inativo": {
            "pt": "Inativo",
			"en": "Inactive",
			"es": "Inactivo"
        },
        "btn-candidatar-beneficio": {
            "pt": "Candidatar a benefício",
			"en": "Apply to benefit",
			"es": "Postular a beneficio"
        },
        "l-servico": {
            "pt": "Serviço",
			"en": "Service",
			"es": "Servicio"
        },
        "l-servicos": {
            "pt": "Serviços",
			"en": "Services",
			"es": "Servicios"
        },
        "l-vencimento": {
            "pt": "Vencimento",
			"en": "Expiration",
			"es": "Vencimiento"
        },
        "l-tipo-desconto": {
            "pt": "Tipo desconto",
			"en": "Discount type",
			"es": "Tipo descuento"
        },
        "l-contrato": {
            "pt": "Contrato",
			"en": "Contract",
			"es": "Contrato"
        },
        "l-parcela-inicial": {
            "pt": "Parcela inicial",
			"en": "Start installment",
			"es": "Cuota inicial"
        },
        "l-parcela-final": {
            "pt": "Parcela final",
			"en": "End installment",
			"es": "Cuota final"
        },
        "l-valor-desconto": {
            "pt": "Desconto",
			"en": "Discount",
			"es": "Descuento"
        },
        "l-valor-maximo": {
            "pt": "Valor máximo",
			"en": "Maximum value",
			"es": "Valor máximo"
        },
        "l-data-inicial": {
            "pt": "Data início",
			"en": "Start date",
			"es": "Fecha inicial"
        },
        "l-data-termino": {
            "pt": "Data término",
			"en": "End date",
			"es": "Fecha final"
        },
        "l-data-autorizacao": {
            "pt": "Data autorização",
			"en": "Authorization date",
			"es": "Fecha autorización"
        },
        "l-data-concessao": {
            "pt": "Data concessão",
			"en": "Granting date",
			"es": "Fecha concesión"
        },
        "l-data-cancelamento": {
            "pt": "Data cancelamento",
			"en": "Cancellation date",
			"es": "Fecha anulación"
        },
        "l-usuario-cancelamento": {
            "pt": "Usuário cancelamento",
			"en": "Cancellation user",
			"es": "Usuario anulación"
        },
        "l-motivo-cancelamento": {
            "pt": "Motivo cancelamento",
			"en": "Cancellation reason",
			"es": "Motivo anulación"
        },
        "l-vencido": {
            "pt": "Vencido",
			"en": "Expired",
			"es": "Vencido"
        },
        "l-a-vencer": {
            "pt": "A vencer",
			"en": "To expire",
			"es": "Por vencer"
        },
        "l-todos-servicos": {
            "pt": "TODOS",
			"en": "ALL",
			"es": "TODOS"
        },
        "l-bolsa-retroativa": {
            "pt": "Bolsa retroativa",
			"en": "Retroactive scholarship",
			"es": "Beca retroactiva"
        }
    }
]
