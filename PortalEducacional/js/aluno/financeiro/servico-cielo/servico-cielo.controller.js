(function () {
    'use strict';

    var modulo = angular.module('eduFinanceiroModule', ['ngResource', 'ui.select', 'ngSanitize', 'totvsHtmlFramework']);

    modulo.controller('eduServicoCieloController', EduServicoCieloController);

    modulo.config(totvsAppConfig);

    EduServicoCieloController.$inject = ['$sce', '$totvsresource', 'i18nFilter', '$window', '$rootScope'];
    function EduServicoCieloController($sce, $totvsresource, i18nFilter, $window, $rootScope) {

        var self = this,
        tipoAviso, titulo, mensagem;

        init();

        function init() {
            //Configurar idioma padrão da tradução
            $rootScope.currentuser = { dialect: EDU_CONST_GLOBAL_CUSTOM_IDIOMA };

            if ($window.opener.EduTransacaoCielo) {
                var transacao = angular.fromJson($window.opener.EduTransacaoCielo);

                retornarStatusPagamento(transacao.idTransacao,
                    transacao.idTransacaoCielo,
                    transacao.idBoleto,
                    transacao.descricaoBoleto,
                    transacao.valorBoleto,
                    transacao.nomeRespFinanceiro,
                    transacao.emailRespFinanceiro,
                    transacao.numParcelas,
                    transacao.isCredito, function (result) {

                    if (result) {
                        self.status = result;

                        switch (self.status.Status) {
                            case -1:
                                tipoAviso = 'error';
                                titulo = i18nFilter('l-erro', [], 'js/aluno/financeiro/lancamentos');
                                mensagem = i18nFilter('l-codigo', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Codigo + '<br/>' +
                                           i18nFilter('l-mensagem', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Mensagem;
                                break;
                            case 3:
                                tipoAviso = 'warning';
                                titulo = i18nFilter('l-pagnaoautenticado', [], 'js/aluno/financeiro/lancamentos');
                                mensagem = i18nFilter('l-codigo', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Codigo + '<br/>' +
                                           i18nFilter('l-mensagem', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Mensagem;
                                break;
                            case 5:
                                tipoAviso = 'warning';
                                titulo = i18nFilter('l-pagnaoautorizado', [], 'js/aluno/financeiro/lancamentos');
                                mensagem = i18nFilter('l-codigo', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.LR + '<br/>' +
                                           i18nFilter('l-mensagem', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Mensagem;
                                break;
                            case 6:
                                tipoAviso = 'info';
                                titulo = i18nFilter('l-pagcapturado', [], 'js/aluno/financeiro/lancamentos');
                                mensagem = i18nFilter('l-mensagem', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Mensagem + '<br/>' +
                                           i18nFilter('l-codigo', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Codigo + '<br/>' +
                                           i18nFilter('l-numpedido', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.NumPedido  + '<br/>' +
                                           i18nFilter('l-tid', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.TID  + '<br/>' +
                                           i18nFilter('l-nsu', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.NSU  + '<br/>';
                                break;
                            case 10:
                                tipoAviso = 'info';
                                titulo = i18nFilter('l-pagemautenticacao', [], 'js/aluno/financeiro/lancamentos');
                                mensagem = i18nFilter('l-codigo', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Codigo + '<br/>' +
                                           i18nFilter('l-mensagem', [], 'js/aluno/financeiro/lancamentos') + ': ' + self.status.Mensagem;
                                break;
                        }

                        window.opener.notificacaoResultadoComunicacaoCielo(tipoAviso, titulo, mensagem);
                        window.close();
                    } else
                    {
                        titulo = i18nFilter('l-semresultado', [], 'js/aluno/financeiro/lancamentos');
                        mensagem = i18nFilter('l-msgsemresultado', [], 'js/aluno/financeiro/lancamentos');

                        window.opener.notificacaoResultadoComunicacaoCielo('error', titulo, mensagem);
                        window.close();
                    }
                });
            }
            else {
                titulo = i18nFilter('l-semcodigotransacao', [], 'js/aluno/financeiro/lancamentos');
                mensagem = i18nFilter('l-msgsemcodigotransacao', [], 'js/aluno/financeiro/lancamentos');

                window.opener.notificacaoResultadoComunicacaoCielo('error', titulo, mensagem);
                window.close();
            }
        }

        /**
         *  Retorna o status do pagamento Buy Page cielo.
         *
         * @param {any} idTransacaoFin - Identificador transação financeiro.
         * @param {any} idTransacaoCielo - Identificador transação Cielo.
         * @param {any} idBoleto - Id do boleto.
         * @param {any} descricaoBoleto - Descrição do boleto.
         * @param {any} valorBoleto - Valor do boleto.
         * @param {any} nomeRespFinanceiro - Nome do resp. financeiro.
         * @param {any} emailRespFinanceiro - Email do resp. financeiro.
         * @param {any} numParcelas - Número de parcelas.
         * @param {any} isCredito - Se é pagamento por cartão de crédito.
         * @param {any} callback - Função de callback, se necessário.
         * @returns Status do pagamento Buy Page cielo.
         */
        function retornarStatusPagamento(idTransacaoFin,
            idTransacaoCielo,
            idBoleto,
            descricaoBoleto,
            valorBoleto,
            nomeRespFinanceiro,
            emailRespFinanceiro,
            numParcelas,
            isCredito,
            callback) {

            var parameters = {
                method: 'StatusBuyPageCielo',
                idTransacaoFin: idTransacaoFin,
                idTransacaoCielo: idTransacaoCielo,
                idBoleto: idBoleto,
                descricaoBoleto: descricaoBoleto,
                valorBoleto: valorBoleto,
                nomeRespFinanceiro: nomeRespFinanceiro,
                emailRespFinanceiro: emailRespFinanceiro,
                numParcelas: numParcelas,
                isCredito: isCredito
            },
                urlServicos = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Financeiro/:method',
                factory = $totvsresource.REST(urlServicos, {}, {});

            return factory.TOTVSGet(parameters, callback);
        }
    }

    totvsAppConfig.$inject = ['$httpProvider', 'TotvsI18nProvider'];
    function totvsAppConfig($httpProvider, TotvsI18nProvider) {

        $httpProvider.interceptors.push('totvsHttpInterceptor');

        TotvsI18nProvider.setBaseContext(EDU_CONST_GLOBAL_URL_BASE_APP);
    }

    angular.bootstrap(document, ['eduFinanceiroModule']);
}());
