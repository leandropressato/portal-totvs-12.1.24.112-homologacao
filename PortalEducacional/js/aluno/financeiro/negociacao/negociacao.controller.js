/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduNegociacaoModule
* @name EduNegociacaoController
* @object controller
*
* @created 26/09/2016 v12.1.15
* @updated
*
* @requires Negociacao.module
*
* @dependencies eduNegociacaoFactory
*
* @description Controller do Negociacao
*/
define(['aluno/financeiro/financeiro.module'], function () {

    'use strict';

    angular
        .module('eduFinanceiroModule')
        .controller('EduNegociacaoController', EduNegociacaoController);

    EduNegociacaoController.$inject = ['TotvsDesktopContextoCursoFactory'];

    function EduNegociacaoController(objContextoCursoFactory) {

        // *********************************************************************************
        // *** Variáveis
        // *********************************************************************************

        var self = this;

        // *********************************************************************************
        // *** Propriedades públicas e métodos
        // *********************************************************************************



        // *********************************************************************************
        // *** Inicialização do controller
        // *********************************************************************************

        init();

        /**
         * Metodo de inicialização do controller
         */
        function init() {

        }
    }
});
