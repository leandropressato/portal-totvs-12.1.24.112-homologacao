[{
    "l-home": {
        "pt": "Início",
		"en": "Start",
		"es": "Inicio"
    },
    "l-titulo": {
        "pt": "Financeiro",
		"en": "Financial",
		"es": "Financiero"
    },
    "l-lancamentos": {
        "pt": "Lançamentos",
		"en": "Entries",
		"es": "Registros"
    },
    "l-boletos": {
        "pt": "Boletos",
		"en": "Bank slips",
		"es": "Boletas"
    },
    "l-beneficios": {
        "pt": "Benefícios",
		"en": "Benefits",
		"es": "Beneficios"
    },
    "l-negociacao": {
        "pt": "Negociação",
		"en": "Negotiation",
		"es": "Negociación"
    },
    "l-widget-title": {
      "pt": "Painel do Aluno",
	  "en": "Student's Panel",
	  "es": "Panel del alumno"
    }
}]