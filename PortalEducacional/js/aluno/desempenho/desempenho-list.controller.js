/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.20
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduDesempenhoModule
 * @name eduDesempenhoController
 * @object controller
 *
 * @created 2018-02-10 v12.1.20
 * @updated
 *
 * @requires desempenho.module
 *
 * @dependencies
 *
 * @description Controller de Desempenho de notas do aluno
 */
define([
    'aluno/desempenho/desempenho.module',
    'aluno/desempenho/desempenho.factory',
    'widgets/widget.constants'
], function () {

    'use strict';

    angular.module('eduDesempenhoModule')
        .controller('eduDesempenhoController', eduDesempenhoController);

    eduDesempenhoController.$inject = [
        '$scope',
        '$rootScope',
        '$filter',
        'TotvsDesktopContextoCursoFactory',
        'eduDesempenhoFactory',
        'totvs.app-notification.Service',
        'i18nFilter',
        '$state',
        'eduConstantesGlobaisConsts',
        'eduWidgetsConsts'
    ];

    function eduDesempenhoController(
        $scope,
        $rootScope,
        $filter,
        TotvsDesktopContextoCursoFactory,
        eduDesempenhoFactory,
        totvsNotification,
        i18nFilter,
        $state,
        eduConstantesGlobaisConsts,
        eduWidgetsConsts
    ) {
        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Desempenho;

        self.desempenho = [];
        self.categoryAxis = [];
        self.series = [];
        self.valueAxis = [];
        self.categories = [];
        self.notasAluno = [];
        self.mediaTurma = [];
        self.mediaCurso = [];
        self.etapas = [];
        self.plotBands = [];
        self.discNotaConceitual = [];
        self.textoDiscNotaConceitual = "";
        self.format = 'N1';
        self.periodoLetivo = 0;
        self.casasDecimais = 0;
        self.etapaSelecionada = -1;
        self.mediaEtapa = 0;
        self.maxValue = 0;
        self.espessuraLinha = 0;
        self.pontosDistribuidos = 0;
        self.chartHeight = 0;
        self.onLegendItemClick;
        self.setFnExport;
        self.indexMediaEtapa = 0;
        self.indexPontosDistribuidos = 0;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.buscaDesempenhoAluno = buscaDesempenhoAluno;
        self.buscaEtapasTurmaDiscAluno = buscaEtapasTurmaDiscAluno;
        self.periodoLetivoAlterado = periodoLetivoAlterado;

        /**
         * Assina o evento de alteração de período
         */
        $scope.$on('alteraPeriodoLetivoEvent', buscaEtapasTurmaDiscAluno);

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        init();

        var permissionsWatch = $rootScope.$watch('objPermissions', function () {

            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_DESEMPENHO) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        function init() {

            self.setFnExport = function (obj) {
                self.exportToPdf = obj.pdf;
                self.exportToPng = obj.png;
            };

            //Chamado ao clicar na Legenda que exibe ou oculta o item selecionado na legenda.
            //Se clicado na Média da Etap ou Pontos distribuídos, é preciso retirar do gráfico manualmente
            self.onLegendItemClick = function (e) {
                setPlotBands(e.seriesIndex, !e.series.visible);
            };
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function periodoLetivoAlterado() {
            $scope.$broadcast('alteraPeriodoLetivoEvent');
        }

        /**
         * Busca as etapas de todas as turmas disciplinas matriculadas do aluno
         * no PL selecionado.
         * 
         * @returns Retorna as etapsa para preencher o combo de seleção de etapa
         */           
        function buscaEtapasTurmaDiscAluno() {

            eduDesempenhoFactory.EtapasTurmaDiscAluno(function (result) {
                if (typeof result != 'undefined' &&
                    typeof result.Etapas != 'undefined') {                
                    self.etapas = result.Etapas;
                    if (self.etapas.length > 0) {
                        self.etapaSelecionada = self.etapas[0].CODETAPA;
                        buscaDesempenhoAluno();
                    } else {
                        //Se nao tem etapa, nao tem notas para serem exibidas.
                        zeraGrafico(true);
                        self.etapas = [];
                    }
                } else {
                    //Se nao tem etapa, nao tem notas para serem exibidas.
                    zeraGrafico(true);
                    self.etapas = [];
                }
            });
        }

        /**
         * Busca as notas do aluno, média da turma e média do curso de todas as disciplinas
         * matriculadas do aluno, no PL selcionado.
         *
         * @returns Gráfico com o desempenho do aluno
         */   
        function buscaDesempenhoAluno() {

            eduDesempenhoFactory.DesempenhoAluno(self.etapaSelecionada, function (result) {
                zeraGrafico();

                self.desempenho = result.Notas;
                self.casasDecimais = self.desempenho[0].DECIMAIS;
                self.mediaEtapa = self.desempenho[0].MEDIA;
                self.pontosDistribuidos = self.desempenho[0].PONTDIST;

                if (typeof self.casasDecimais != 'undefined') {                    
                    self.format = 'N' + self.casasDecimais;
                }

                getCategorias();
                setParametrosGrafico();
            });

        }

        /**
         * Define os valores retornados pelo banco nas variáveis utilizadas pelo gráfico.
         *
         * @returns Valores utilizados pelo gráfico. Disciplinas, notas, etc.
         */
        function getCategorias() {
            if (self.desempenho) {
                for (var i = 0; i < self.desempenho.length; i++) {
                    if (self.desempenho[i].TIPONOTA != 'C') {
                        self.categories.push(self.desempenho[i].DISCIPLINA);
                        self.notasAluno.push(formataCasasDecimais(self.desempenho[i].NOTA));
                        self.mediaTurma.push(formataCasasDecimais(self.desempenho[i].MEDIATURMA));
                        self.mediaCurso.push(formataCasasDecimais(self.desempenho[i].MEDIACURSO));
                    } else {
                        self.discNotaConceitual.push(self.desempenho[i].DISCIPLINA);
                    }
                }

                //tratamento se todas as disciplinas forem conceituais.
                //para não exibir um gráfico sem nenhuma barra de notas.
                if (typeof self.discNotaConceitual != 'undefined' &&
                    self.desempenho.length > 0 &&
                    self.desempenho.length == self.discNotaConceitual.length) {
                        zeraGrafico(false);
                        self.etapas = [];
                }

                if (typeof self.discNotaConceitual != 'undefined') {
                    setTextoDiscConceituais();
                }

                //O tamanho do gráfico é definido dinamicamente. De acordo com a quantidade
                //de disciplinas. Quanto mais disciplinas, mais alto o gráfico ficará.
                //Isso evita que as barras fiquem muito finas caso tenham muitas disciplinas
                //matriculadas no mesmo PL. 
                self.chartHeight = self.categories.length * 80;
                self.chartHeight = (self.chartHeight < 500) ? 500 : self.chartHeight;

                getMaxValue(self.notasAluno, self.mediaTurma, self.mediaCurso);
            }
        }

        /**
         * Cria o texto de informação com as disciplinas com notas conceituais, informando
         * que estas disciplinas não estão presentes no gráfico.
         *
         * @returns Texto a ser exibido para o usuário, com nome das disciplinas que são conceituais.
         */        
        function setTextoDiscConceituais() {
            var texto = '';
            if (self.discNotaConceitual.length > 1) {
                texto = $filter('i18n')('l-disc-nota-conceitual-p', [], 'js/aluno/desempenho');
            } else if (self.discNotaConceitual.length == 1) {
                texto = $filter('i18n')('l-disc-nota-conceitual-s', [], 'js/aluno/desempenho');
            } else {
                return;
            }

            var nomeDisciplinas = '';

            for (var i = 0; i < self.discNotaConceitual.length; i++) {
                if (i == 0) {
                    nomeDisciplinas = self.discNotaConceitual[i];
                }
                
                if (i > 0 && i < self.discNotaConceitual.length -1) {
                    nomeDisciplinas = nomeDisciplinas + ', ' + self.discNotaConceitual[i];
                }

                if (i > 0 && i == self.discNotaConceitual.length -1) {
                    nomeDisciplinas = nomeDisciplinas + ' e ' + self.discNotaConceitual[i];
                }                
            }


            String.prototype.format = function() {
                var s = this,
                    i = arguments.length;
            
                while (i--) {
                    s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
                }
                return s;
            };
            
            self.textoDiscNotaConceitual = texto.format(nomeDisciplinas);
        }

        /**
         * Determina o valor máximo do Eixo X do gráfico.
         * O valor o eixo X será 20% maior que o maior valor de nota retornado pelo banco.
         * 
         * Essa porcentagem de 20% foi definido para o gráfico ficar visualmente bem apresentado,
         * e que nenhuma barra de notas chegue até o final do eixo X (até toda a direita)
         *
         * @param {object} notasAluno - As notas de todas as disciplinas do aluno
         * @param {object} mediaTurma - As notas média da turma de todas as disciplinas do aluno
         * @param {object} mediaCurso - As notas média do curso de todas as disciplinas do aluno
         * @returns Valor máximo que será atribuído ao eixo X do gráfico.
         */        
        function getMaxValue(notasAluno, mediaTurma, mediaCurso) {

            var valoresMaximos = [];
            valoresMaximos.push(Math.max.apply(Math, notasAluno));
            valoresMaximos.push(Math.max.apply(Math, mediaTurma));
            valoresMaximos.push(Math.max.apply(Math, mediaCurso));
            if (self.pontosDistribuidos > 0) {
                valoresMaximos.push(self.pontosDistribuidos);
            }

            if (Math.max.apply(Math, valoresMaximos) > 0) {
                self.maxValue = ((Math.max.apply(Math, valoresMaximos) * 20) / 100) +
                    Math.max.apply(Math, valoresMaximos);

                //Espessura das linhas Média de etapa e Pontos distribuídos.
                //Essa espessura são calculadas de acordo com os valores do gráfico (mínimo e máximo), 
                //já que não é possível linha uma linha. Então foi criado barras que simulam linha.
                self.espessuraLinha = ((self.maxValue * 0.15) / 100);
            }
        }

        /**
         * Define os parametros do gráfico. Séries, categorias, etc.
         * 
         * @returns Parametros do gráfico.
         */           
        function setParametrosGrafico() {

            kendo.culture('pt-BR');

            self.series = [{
                    tooltip: {
                        visible: true,
                        font: '12px Arial',
                        format: filtroI18n('l-nota-aluno') + ': {0:N' + self.casasDecimais + '}'
                    },
                    data: self.notasAluno,
                    name: filtroI18n('l-nota-aluno'),
                    color: '#37822c',
                    target: {
                        color: '#008000'
                    },
                    labels: {
                        format: self.format
                    }
                },
                {
                    tooltip: {
                        visible: true,
                        font: '12px Arial',
                        format: filtroI18n('l-media-turma') + ': {0:N' + self.casasDecimais + '}'
                    },
                    data: self.mediaTurma,
                    name: 'Média da turma',
                    color: '#1a53ff',
                    labels: {
                        format: self.format
                    }
                },
                {
                    tooltip: {
                        visible: true,
                        font: '12px Arial',
                        format: filtroI18n('l-media-curso') + ': {0:N' + self.casasDecimais + '}'
                    },
                    data: self.mediaCurso,
                    name: filtroI18n('l-media-curso'),
                    color: '#ff9900',
                    labels: {
                        format: self.format
                    }
                }
            ];

            if (self.mediaEtapa > 0) {
                self.series.push({
                    name: filtroI18n('l-media-etapa') + ': (' + formataDigitosDecimais(formataCasasDecimais(self.mediaEtapa)) + ')',
                    color: '#c00'
                });
                
                //O Index da barra de Media por etapa será 3, pois nesse instante teremos 4 séries.
                self.indexMediaEtapa = self.series.length - 1;
            }

            if (self.pontosDistribuidos > 0) {
                self.series.push({
                    name: filtroI18n('l-pontos-distribuidos') + ': (' + formataDigitosDecimais(formataCasasDecimais(self.pontosDistribuidos)) + ')',
                    color: '#051151'
                });

                //O Index da barra de Pontos distribuídos por etapa será 3 se nao tiver Meida por etapa,
                //e 4 se tiver com a média por etapa.
                self.indexPontosDistribuidos = self.series.length - 1;
            }            

            self.categoryAxis = [{
                //categories: self.categories,
                labels: {
                    rotation: 'auto',
                    format: self.format
                },
                minorGridLines: {
                    visible: false
                },
                majorGridLines: {
                    visible: true
                }
            }];

            self.valueAxis = {
                plotBands: self.plotBands,
                min: 0,
                max: self.maxValue,
                line: {
                    visible: true
                },
                labels: {
                    visible: true,
                    rotation: 'auto'
                },
                minorGridLines: {
                    visible: true,
                    step: 1
                },
                majorGridLines: {
                    visible: true
                }
            };

            if (self.format != null) {
                self.valueAxis['labels'] = {
                    format: self.format,
                    visible: true,
                    rotation: 'auto'
                };
            }

            //Linha da média de Etapa. O Index da série é o 3.
            if (self.indexMediaEtapa > 0) {
                setPlotBands(self.indexMediaEtapa, true);
            }

            //Linha de pontos distribuídos. O Index da série é o 4.
            if (self.indexPontosDistribuidos > 0) {
                setPlotBands(self.indexPontosDistribuidos, true);
            }
        }

        /**
         * Cria a barra de Média da Etapa e Pontos distribuídos.
         *
         * Esses valores são definidos no cadastro da Etapa.
         * 
         * Estas linhas exibidas no gráfico são barras e não linhas (com valor inicial e final).
         * A Kendo não dispõe dessa funcionalidade, portanto foi preciso utilizar esse artifício para
         * acrescentar a informação no gráfico.
         * 
         *  
         *
         * @param {int} index - Index da categoria do PlotBand
         * @param {object} visualizaBand - Exibe ou oculta a linha de uma série
         * @returns Objeto do PlotBand criado de acordo com as definições.
         */           
        function setPlotBands(index, visualizaBand) {
            if ((self.mediaEtapa > 0 &&
                    visualizaBand) &&
                index == self.indexMediaEtapa) {

                self.plotBands.push({
                    id: index,
                    from: self.mediaEtapa,
                    to: self.mediaEtapa + self.espessuraLinha,
                    color: 'red'
                });
            }

            if ((self.pontosDistribuidos > 0 &&
                    visualizaBand) &&
                index == self.indexPontosDistribuidos) {

                self.plotBands.push({
                    id: index,
                    from: self.pontosDistribuidos,
                    to: self.pontosDistribuidos + self.espessuraLinha,
                    color: '#051151'
                });
            }

            if (!visualizaBand) {
                for (var i = self.plotBands.length - 1; i >= 0; i--) {
                    if (self.plotBands[i].id === index) {
                        self.plotBands.splice(i, 1);
                    }
                }
            }
        }

        /**
         * Chamado quando não são retornados dados de disciplinas ou etapas do banco.
         */          
        function zeraGrafico(apagaNotaConceitual) {
            if (apagaNotaConceitual) {
                self.discNotaConceitual = [];
            }
            
            self.desempenho = [];
            self.notasAluno = [];
            self.mediaTurma = [];
            self.mediaCurso = [];
            self.series = [];
            self.categoryAxis = [];
            self.valueAxis = [];
            self.categories = [];
            self.plotBands = [];
        }

        /**
         * Formata os valores numéricos de acordo com a quantidade de casas decimais definidos no 
         * cadastro de etapas.
         */
        function formataCasasDecimais(valor) {
            if (valor == null) return null;
            
            return parseFloat(Math.round(valor * 100) / 100)
                .toFixed(self.casasDecimais);
        }

        function formataDigitosDecimais(valor) {
            return valor.replace('.', ',')
                        .toString().replace(/\B(?=(\d{4})+(?!\d))/g, '.');
        }

        function filtroI18n(nomeCampo) {
            return $filter('i18n')(nomeCampo, [], 'js/aluno/desempenho');
        }
    }
});
