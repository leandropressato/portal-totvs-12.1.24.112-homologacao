[{
    "l-titulo": {
        "pt": "Desempenho",
		"en": "Performance",
		"es": "Desempeño"
    },
    "l-titulo-grafico": {
        "pt": "Desempenho de notas do aluno",
		"en": "Performance of student's score  ",
		"es": "Desempeño de notas del alumno"
    },
    "l-etapa": {
        "pt": "Etapa",
		"en": "Stage",
		"es": "Etapa"
    },
    "l-nota-aluno": {
        "pt": "Nota do aluno",
		"en": "Student's score",
		"es": "Nota del alumno"
    },
    "l-media-turma": {
        "pt": "Média da turma",
		"en": "Average score of the class",
		"es": "Promedio del grupo"
    },
    "l-media-curso": {
        "pt": "Média do curso",
		"en": "Average score of the course",
		"es": "Promedio del curso"
    },
    "l-media-serie": {
        "pt": "Média da série",
		"en": "Average score of the series",
		"es": "Promedio de la serie"
    },
    "l-media-etapa": {
        "pt": "Média da etapa",
		"en": "Average of the stage",
		"es": "Promedio de la etapa"
    },
    "l-pontos-distribuidos": {
        "pt": "Pontos distribuídos",
		"en": "Scores distributed",
		"es": "Puntos distribuidos"
    },
    "l-exportar": {
        "pt": "Salvar gráfico como imagem",
		"en": "Save chart as image",
		"es": "Grabar gráfico como imagen"
    },
    "l-sem-etapas": {
        "pt": "Sem etapas",
		"en": "Without stages",
		"es": "Sin etapas"
    },    
    "l-msg-nenhum-registro": {
        "pt": "Nenhum registro de notas foi localizado para exibir o gráfico de Desempenho do aluno!",
		"en": "No record of scores was found to display the student's performance chart",
		"es": "¡No se encontró ningún registro de notas para mostrar el gráfico de Desempeño del alumno!"
    },
    "l-disc-nota-conceitual-p": {
        "pt": "As disciplinas {0}, possuem nota conceitual cadastrada. As disciplinas conceituais não são exibidas no gráfico de desempenho. Para visualizar estas notas, acesse a funcionalidade específica de Notas.",
		"en": "The course subjects {0} has conceptual score registered. The conceptual course subjects are not displayed in the performance chart. To view these scores, access the specific functionality of the Scores.",
		"es": "Las materias {0}, tienen nota conceptual registrada. Las materias conceptuales no se muestran en el gráfico de desempeño. Para visualizar estas notas, acceda a la funcionalidad específica de Notas."
    },
    "l-disc-nota-conceitual-s": {
        "pt": "A disciplina {0} possui nota conceitual cadastrada. As disciplinas conceituais não são exibidas no gráfico de desempenho. Para visualizar estas notas, acesse a funcionalidade específica de Notas.",
		"en": "The course subjects {0} has conceptual score registered. The course subjects are not displayed in the performance chart. To view these score, access the specific functionality of the Scores.",
		"es": "La materia {0} tiene nota conceptual registrada. Las materias conceptuales no se muestran en el gráfico de desempeño. Para visualizar estas notas, acceda a la funcionalidad específica de Notas."
    }    
        
}]
