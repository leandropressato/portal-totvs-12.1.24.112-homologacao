/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.20
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduDesempenhoModule
 * @name EduFaltasUnificadaFactory
 * @object factory
 *
 * @created 2018-02-20 v12.1.20
 * @updated
 *
 * @requires eduDesempenhoModule
 *
 * @dependencies eduDesempenhoFactory
 *
 * @description Factory utilizada para Desempenho de notas do aluno.
 */

define(['aluno/desempenho/desempenho.module'], function() {

    'use strict';

    angular
        .module('eduDesempenhoModule')
        .factory('eduDesempenhoFactory', EduDesempenhoFactory);

    EduDesempenhoFactory.$inject = ['$totvsresource'];

    function EduDesempenhoFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.DesempenhoAluno = DesempenhoAluno;
        factory.EtapasTurmaDiscAluno = EtapasTurmaDiscAluno;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function DesempenhoAluno(codEtapa, callback) {
            var parameters = [];
            parameters['method'] = 'v1/DesempenhoAluno';
            parameters['codEtapa'] = codEtapa;

            factory.TOTVSGet(parameters, callback);
        }

        function EtapasTurmaDiscAluno(callback) {
            var parameters = [];
            parameters['method'] = 'v1/EtapasNotasTurmaDiscAluno';

            factory.TOTVSGet(parameters, callback);
        }
    }
});