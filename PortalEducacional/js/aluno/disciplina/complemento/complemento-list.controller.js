/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/disciplina/disciplina.module',
    'aluno/disciplina/disciplina.factory',
    'utils/edu-enums.constants'
], function () {

    'use strict';

    angular
        .module('eduDisciplinaModule')
        .controller('eduDisciplinaComplementoController', EduDisciplinaComplementoController);

    EduDisciplinaComplementoController.$inject = [
        'TotvsDesktopContextoCursoFactory',
        'eduDisciplinaFactory',
        'eduEnumsConsts',
        'totvs.app-notification.Service',
        'i18nFilter',
        '$filter',
        '$sce',
        '$state'
    ];

    function EduDisciplinaComplementoController(
        objContextoCursoFactory,
        EduDisciplinaFactory,
        EduEnumsConsts,
        notification,
        i18nFilter,
        $filter,
        $sce,
        $state) {

        var self = this;

        // contém a modelo de dados com informações da disciplina
        self.model = [];
        // grid de Publicações
        self.gridPublic = null;
        self.gridOptions = null;
        // retorna o html confiavel pelo angular
        self.getTrustHtml = getTrustHtml;

        init();

        /* FUNCTIONS */

        function init() {
            // pega o codDisc na URL
            var codDisc = $state.params.codDisc;

            var params = [];
            params['codDisc'] = codDisc;
            EduDisciplinaFactory.InfoDisciplina(params, function (result) {
                if (result.SComplDisc) {
                    // filtra os tipos de complementos e monta o model
                    angular.forEach(result.SComplDisc, function (item) {
                        var existe = $filter('filter')(self.model, { 'tipo': item.TIPO }, true);
                        if (existe.length === 0) {
                            var complementos = $filter('filter')(result.SComplDisc, { 'TIPO': item.TIPO }, true);
                            var publicacoes = $filter('filter')(result.SCOMPLDISCPUBLIC, { 'IDCOMPLDISC': item.IDCOMPLDISC }, true);
                            self.model.push({
                                'tipo': item.TIPO,
                                'descricao': item.TIPOCOMPLDISC,
                                'complementos': complementos,
                                'publicacoes': publicacoes
                            });
                        }
                    });
                }
            });

            // configura a grid
            self.gridOptions = {
                groupable: {
                    messages: {
                        empty: $filter('i18n')('grid-agrupamento-vazio')
                    }
                }
            };
        }

        /**
         * Retorna um HTML confiavel pelo Angular
         *
         * @param {any} html - conteudo HTML
         * @returns HTML trusted
         */
        function getTrustHtml (html) {
            return $sce.trustAsHtml(html);
        }
    }
});
