/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/disciplina/disciplina.module',
    'aluno/disciplina/disciplina.factory',
    'utils/edu-enums.constants',
    'utils/edu-constantes-globais.constants',
    'diretivas/edu-selecao-periodoletivo.factory'], function () {

        'use strict';

        angular
            .module('eduDisciplinaModule')
            .controller('eduDisciplinaController', EduDisciplinaController);

        EduDisciplinaController.$inject = [
            'MatriculaDisciplinaDetalhesFactory',
            'eduDisciplinaFactory',
            'TotvsDesktopContextoCursoFactory',
            '$state',
            '$scope',
            '$rootScope',
            'eduConstantesGlobaisConsts',
            'totvs.app-notification.Service',
            'i18nFilter',
            'eduSelecaoPeriodoletivoFactory',
            'eduEnumsConsts'
        ];

        function EduDisciplinaController(
            eduMatriculaDisciplinaDetalhesFactory,
            eduDisciplinaFactory,
            TotvsDesktopContextoCursoFactory,
            $state,
            $scope,
            $rootScope,
            eduConstantesGlobaisConsts,
            totvsNotification,
            i18nFilter,
            eduSelecaoPeriodoletivoFactory,
            eduEnumsConsts) {

            var self = this;

            //Contém a modelo de dados com informações da disciplina
            self.model = {};
            self.model.disciplinaInfo = {};
            self.model.professoresDiscInfo = [];

            //Propriedades que controlam a tab selecionada
            self.tabSelecionada = '';
            self.activeTabAulas = true;
            self.activeTabTCC = false;
            self.activeTabInformacoes = false;
            self.activeTabFaltas = false;
            self.activeTabAvaliacoes = false;
            self.activeTabMateriais = false;
            self.activeTabOcorrencias = false;
            self.isTCC = false;

            self.exibeTab = exibeTab;
            self.selecionarTabOcorrencias = selecionarTabOcorrencias;
            self.selecionarTabAvaliacoes = selecionarTabAvaliacoes;
            self.textoPeriodoLetivoSelecionado = '';
            self.getDiscInfo = getDiscInfo;

            //Manipula evento disparado pelo modal de seleção de contexto
            $scope.$on('OnChangeCursoSelecionado:Event', function () {

                totvsNotification.notify({
                    type: 'info',
                    title: i18nFilter('l-informacao', [], 'js/aluno/disciplina'),
                    detail: i18nFilter('l-aviso-redirecionamento', [], 'js/aluno/disciplina')
                });

                $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
            });

            // Só executa o método init após carregar o objeto com as permissões do usuário
            var myWatch = $rootScope.$watch('objPermissions', function (data) {
                if (data !== null) {
                    init();
                    myWatch();
                }
            });

            function init() {

                if ($state.params.idTurmaDisc) {

                    carregarInfoDisciplinaAsync($state.params.idTurmaDisc, function () {

                        //Se não foi encontrado nenhuma informação de disciplina faz tratamento
                        if (self.model.disciplinaInfo.length === 0) {
                            $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                        }
                        else {

                            //Preenche a descrição do período letivo do aluno
                            eduSelecaoPeriodoletivoFactory.getDescricaoPeriodoLetivoPorIdAsync(self.model.disciplinaInfo.IDPERLET, function (descricao) {

                                if (descricao) {
                                    self.textoPeriodoLetivoSelecionado = descricao;
                                }
                            });
                        }

                    });

                } else {
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                self.contextoCurso = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
                verificarApresentacao();
            }

            function getDiscInfo(callback) {
                carregarInfoDisciplinaAsync($state.params.idTurmaDisc, callback);
            }

            function carregarInfoDisciplinaAsync(idTurmaDisc, callback) {
                self.model.disciplinaInfo = {};
                self.model.professoresDiscInfo = [];

                eduMatriculaDisciplinaDetalhesFactory.retornarDetalhesMatriculaDisciplina({ idTurmaDisc: idTurmaDisc }, function (result) {

                    if (result.DisciplinaInfo && result.DisciplinaInfo.length > 0) {
                        self.model.disciplinaInfo = result.DisciplinaInfo[0];
                        self.isTCC = (self.model.disciplinaInfo.DISCIPLINATCC === 'S');

                        if (self.isTCC) {
                            self.activeTabAulas = false;
                            self.activeTabTCC = true;
                        }
                    }

                    if (result.ProfessorTurmaDiscInfo && result.ProfessorTurmaDiscInfo.length > 0) {
                        self.model.professoresDiscInfo = result.ProfessorTurmaDiscInfo.filter(function(elem, index, self) {
                            return index == self.findIndex(ProfessorDistinct, elem);
                        });
                    }

                    if (typeof callback === 'function') {
                        callback(result);
                    }
                });
            }

            function ProfessorDistinct(element, index, array) {
                return element.CODPROF === this.CODPROF;
            }

            function verificarApresentacao() {

                self.apresentacaoEB = (parseInt(self.contextoCurso.cursoSelecionado.APRESENTACAO) === eduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico);

            }

            function selecionarTabOcorrencias() {
                self.activeTabOcorrencias = true;
            }

            function selecionarTabAvaliacoes() {
                self.activeTabAvaliacoes = true;
            }

            /**
             * Valida a permissão para cada Tab
             *
             * @param {string} tabName - Nome da tab
             * @returns True or False
             */
            function exibeTab(tabName) {
                var exibe = false;

                if ($rootScope.objPermissions === null) {
                    return exibe;
                }

                switch (tabName) {
                    case 'aulas':
                        exibe = true;
                        break;
                    case 'disciplina':
                        exibe = true;
                        break;
                    case 'faltas':
                        exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_FALTAS);
                        break;
                    case 'avaliacoes':
                        exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_NOTAS_NOTASAVALIACOES);
                        break;
                    case 'materiais':
                        exibe = angular.isDefined($rootScope.objPermissions.EDU_ARQUIVOS_MATERIAIS_DISCIPLINAS);
                        break;
                    case 'ocorrencias':
                        exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_OCORRENCIAS);
                        break;
                    case 'entregas':
                        exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_NOTAS_ENTREGAS);
                        break;
                }
                return exibe;
            }
        }
    });
