[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo-disciplina": {
            "pt": "Disciplina",
			"en": "Subject",
			"es": "Materia"
        },
        "l-tipo": {
            "pt": "Tipo",
			"en": "Type",
			"es": "Tipo"
        },
        "l-credito": {
            "pt": "Crédito",
			"en": "Credit",
			"es": "Crédito"
        },
        "l-cargahoraria": {
            "pt": "Carga Horária",
			"en": "Class hours",
			"es": "Carga horaria"
        },
        "l-periodo": {
            "pt": "Período",
			"en": "Period",
			"es": "Período"
        },
        "l-turma": {
            "pt": "Turma",
			"en": "Class",
			"es": "Grupo"
        },
        "l-subturma": {
            "pt": "Subturma",
			"en": "Subclass",
			"es": "Subgrupo"
        },
        "l-professor": {
            "pt": "Professor",
			"en": "Professor",
			"es": "Profesor"
        },
        "l-horario": {
            "pt": "Horário",
			"en": "Schedule",
			"es": "Horario"
        },
        "l-local": {
            "pt": "Local",
			"en": "Location",
			"es": "Local"
        },
        "l-aulas": {
            "pt": "Aulas",
			"en": "Classes",
			"es": "Clases"
        },
        "l-informacoes": {
            "pt": "Informações",
			"en": "Information",
			"es": "Información"
        },
        "l-faltas": {
            "pt": "Faltas",
			"en": "Absences",
			"es": "Faltas"
        },
        "l-avaliacoes": {
            "pt": "Avaliações",
			"en": "Evaluations",
			"es": "Evaluaciones"
        },
        "l-materiais": {
            "pt": "Materiais",
			"en": "Material",
			"es": "Materiales"
        },
        "l-ocorrencias": {
            "pt": "Ocorrências",
			"en": "Occurrences",
			"es": "Ocurrencias"
        },
        "l-nao-informado": {
            "pt": "(não cadastrado)",
			"en": "(not registered)",
			"es": "(no registrado)"
        },
        "l-title-complemento-disciplina": {
            "pt": "Informações sobre a disciplina",
			"en": "Information about the subject",
			"es": "Información sobre la materia"
        },
        "l-nenhum-registro-encontrado": {
            "pt": "Nenhum Registro Encontrado!",
			"en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },
        "l-publicao-vinculada": {
            "pt": "Publicações Vinculadas",
			"en": "Linked publications",
			"es": "Publicaciones vinculadas"
        },
        "l-titulo": {
            "pt": "Título",
			"en": "Title",
			"es": "Título"
        },
        "l-exemplares": {
            "pt": "Qtde Exemplares Disponíveis",
			"en": "Number of available copies",
			"es": "Cant. Ejemplares disponibles"
        },
        "l-exemplares-necessario": {
            "pt": "Qtde Exemplares Necessários",
			"en": "Number of copies required",
			"es": "Cant. Ejemplares necesarios"
        },
        "l-codigo-disciplina": {
            "pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-creditos": {
            "pt": "Créditos",
			"en": "Credits",
			"es": "Créditos"
        },
        "l-cargahoraria-ch":{
            "pt": "Carga horária - CH",
			"en": "Class hours - CH",
			"es": "Carga horaria - CH"
        },
        "l-professores":{
            "pt": "Professor(es)",
			"en": "Professor(s)",
			"es": "Profesor(es)"
        },
        "l-nao-definido":{
            "pt": "(não definido)",
			"en": "(not defined)",
			"es": "(no definido)"
        },
        "l-nao-definida":{
            "pt": "(não definida)",
			"en": "(not defined)",
			"es": "(no definida)"
        },
        "l-informacao":{
            "pt": "Informação",
			"en": "Information",
			"es": "Información"
        },
        "l-aviso-redirecionamento":{
            "pt": "Você foi redirecionado para a tela inicial do sistema, pois o houve uma nova seleção de curso!",
			"en": "You were redirected to the system home screen due to a new course selection!",
			"es": "Usted fue reorientado a la pantalla inicial del sistema, porque hubo una nueva selección de curso."
        },
        "l-tcc-sigla":{
            "pt": "TCC",
			"en": "TCC",
			"es": "TCC"
        },
        "l-periodo-letivo":{
            "pt": "Período Letivo",
			"en": "School term",
			"es": "Período lectivo"
        },
        "l-filial":{
            "pt": "Filial",
			"en": "Branch",
			"es": "Sucursal"
        },
        "l-entregas":{
            "pt": "Entregas",
			"en": "Deliveries",
			"es": "Entregas"
        }
    }
]
