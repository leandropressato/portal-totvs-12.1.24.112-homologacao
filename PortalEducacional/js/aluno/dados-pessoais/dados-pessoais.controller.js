/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/dados-pessoais/dados-pessoais.module',
    'aluno/dados-pessoais/dados-pessoais.route',
    'aluno/dados-pessoais/dados-pessoais.service',
    'utils/edu-utils.module',
    'utils/edu-utils.factory'], function () {

    'use strict';

    angular
        .module('eduDadosPessoaisModule')
        .controller('eduDadosPessoaisController', EduDadosPessoaisController);

    EduDadosPessoaisController.$inject = ['$rootScope',
        '$filter',
        '$state',
        'maskFilter',
        'eduDadosPessoaisService',
        'eduUtilsFactory',
        'eduEnumsConsts',
        'eduUtilsService',
        'totvs.app-notification.Service',
        'i18nFilter',
        'TotvsDesktopContextoCursoFactory'];

    function EduDadosPessoaisController($rootScope,
        $filter,
        state,
        maskFilter,
        EduDadosPessoaisService,
        EduUtilsFactory,
        eduEnumsConsts,
        eduUtilsService,
        totvsNotification,
        i18nFilter,
        TotvsDesktopContextoCursoFactory) {
        // *********************************************************************************
        // *** Variáveis
        // *********************************************************************************

        var self = this,
            parametrosEdu = {};

        // *********************************************************************************
        // *** Propriedades públicas e métodos
        // *********************************************************************************

        self.Aluno = {};
        self.listCargos = [];
        self.listEstados = [];
        self.listaMunicipios = [];
        self.listaPaises = [];
        self.activeTabResp = false;
        self.activeTabFichaMed = false;
        self.atualizarDadosPessoais = atualizarDadosPessoais;
        self.permiteAlterarDadosPessoais = false;
        self.modoAlteracaoDadosPessoais = eduEnumsConsts.EduDadosCadastraisAlunoEnum.ApenasConsulta;
        self.MascarasSistema = EduDadosPessoaisService.carregaMascarasSistema();
        self.ensinoBasico = false;
        self.textoBotaoAtualizacao = textoBotaoAtualizacao;
        self.buscarEnderecoCEP = buscarEnderecoCEP;
        self.carregarMunicipios = carregarMunicipios;
        self.onChangePais = onChangePais;
        self.exibeTab = exibeTab;
        self.gridFiliacaoNOME = gridFiliacaoNOME; 
        self.gridFiliacaoRELACIONAMENTO = gridFiliacaoRELACIONAMENTO;
        $rootScope.origemMatricula = state.params.origem === 'matricula-eb';

        init();

        // *********************************************************************************
        // *** Inicialização do controller
        // *********************************************************************************

        /**
         * Metodo de inicialização do controller
         */
        function init() {
            carregarParametrosEdu();
            preencherDadosAluno();
            carregaCargosProfissionais();

            if (angular.isDefined(state.params) && angular.isDefined(state.params.tab) && state.params.tab === 'responsaveis') {
                self.activeTabResp = true;

            } else if (angular.isDefined(state.params) && angular.isDefined(state.params.tab) && state.params.tab === 'ficha-medica') {
                self.activeTabFichaMed = true;
            }

            self.gridFiliacao = {
                groupable: false,
                pageable: false,
                sortable: true,
                resizable: true,
                selectable: false
            }
        }

        /**
         * Preenche informações sobre os dados pessoais do aluno.
         */
        function preencherDadosAluno() {
            carregarPaises();
            carregaEstados();

            EduDadosPessoaisService.carregaAlunoDoContexto(function (result) {
                self.modoAlteracaoDadosPessoais = parametrosEdu.modoAlteracaoDadosPessoais;
                self.Aluno = result;
                self.permiteAlterarDadosPessoais = self.modoAlteracaoDadosPessoais !== eduEnumsConsts.EduDadosCadastraisAlunoEnum.ApenasConsulta;
                self.ensinoBasico = (self.Aluno.apresentacao == eduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico);
                self.textoBotaoSalvar = textoBotaoAtualizacao();
            });
        }

        /**
         * Metodo para salvar o registro
         *
         * @param {any} pagina - página que deseja atualizar os dados
         */
        function atualizarDadosPessoais(pagina) {
            EduDadosPessoaisService.atualizarDados(pagina, self.Aluno, function (alunoAtualizado) {
                self.Aluno = alunoAtualizado;
            });
        }

        /**
         *Carrega lista de Cargos cadastrados no RM
         */
        function carregaCargosProfissionais() {
            //TODO: verificar permissão, para evitar chamada no server
            EduUtilsFactory.getListaOcupacoesAsync(function (result) {
                if (result.ETabocup) {
                    self.listCargos = result.ETabocup;
                }
            });
        }

        /**
         * Carrega lista de Estados cadastrados no RM.
         *
         * @param {any} idPais - Id. do país que deseja carregar os estados
         */
        function carregaEstados(idPais) {
            //TODO: verificar permissão, para evitar chamada no server
            if (idPais) {
                EduUtilsFactory.getListaEstadosAsync(idPais, function (result) {
                    if (result.GEtd) {
                        self.listEstados = result.GEtd;
                    }
                });
            } else {
                EduUtilsFactory.getListaEstadosBRAsync(function (result) {
                    if (result.GEtd) {
                        self.listEstados = result.GEtd;
                    }
                });
            }
        }

        /**
         * Evento de alteração do país.(Recarrega a lista de estados)
         */
        function onChangePais() {
            self.listEstados = [];
            var idPais = buscarIdPais(self.Aluno.pais);
            carregaEstados(idPais);
        }

        /**
         * Carrega lista de países cadastrados no RM
         */
        function carregarPaises() {
            EduUtilsFactory.getListaPaisesAsync(function (result) {
                if (result.GPais) {
                    self.listaPaises = result.GPais;
                }
            });
        }

        /**
         * Carrega lista de municípios do estado
         */
        function carregarMunicipios() {
            if (self.Aluno && self.Aluno.pais && self.Aluno.estado) {
                var idPais = buscarIdPais(self.Aluno.pais);
                EduUtilsFactory.getListaMunicipiosAsync(idPais, self.Aluno.estado, function (result) {
                    if (result.GMUNICIPIO) {
                        self.listaMunicipios = result.GMUNICIPIO;
                    }
                });
            }
        }

        /**
         * Busca informações do endereço com base no CEP
         *
         * @param {any} pagina -página que deseja carregar os dados
         */
        function buscarEnderecoCEP(pagina) {
            if (pagina === 'dados-pessoais') {
                EduUtilsFactory.getEnderecoCEPAsync(self.Aluno.cep, function (endereco) {
                    if (endereco[0]) {
                        self.Aluno.bairro = endereco[0].BAIRRO;
                        self.Aluno.cidade = endereco[0].NOME;
                        self.Aluno.rua = endereco[0].NOMELOGRADOURO;
                        self.Aluno.estado = endereco[0].UF;
                        // focus no campo número
                        $('#controller_aluno_numero').focus();
                    } else {
                        notificaCepNaoEncontrado();
                        // foca no campo logradouro
                        $('#controller_aluno_rua').focus();
                    }
                });
            } else if (pagina === 'dados-profissionais') {
                EduUtilsFactory.getEnderecoCEPAsync(self.Aluno.empresaCep, function (endereco) {
                    if (endereco[0]) {
                        self.Aluno.empresaRua = endereco[0].NOMELOGRADOURO;
                        self.Aluno.empresaBairro = endereco[0].BAIRRO;
                        self.Aluno.empresaCidade = endereco[0].NOME;
                        self.Aluno.empresaUF = endereco[0].UF;
                        // focus no campo número
                        $('#controller_aluno_empresanumero').focus();
                    } else {
                        notificaCepNaoEncontrado();
                        // focus no campo logradouro
                        $('#controller_aluno_empresarua').focus();
                    }
                });
            }
        }

        /**
         * Toaster informando que o CEP não foi encontrado.
         */
        function notificaCepNaoEncontrado() {
            totvsNotification.notify({
                type: 'info',
                title: i18nFilter('l-Atencao'),
                detail: i18nFilter('l-msg-cep-nao-encontrado', [], 'js/aluno/dados-pessoais')
            });
        }

        /**
         * busca Id. do país a partir do nome
         *
         * @param {any} nomePais - Nome do país que deseja encontrar o Id.
         * @returns - Id. do país
         */
        function buscarIdPais(nomePais) {
            var pais = self.listaPaises.find(function (item) {
                if (item.DESCRICAO === nomePais) {
                    return item;
                }
            });

            return pais.IDPAIS;
        }

        /**
         * Configuração do texto que deve ser exibido no botão de salvar alteração.
         *
         * @returns - Texto a ser exibido no botão de salvar.
         */
        function textoBotaoAtualizacao() {
            if (self.permiteAlterarDadosPessoais &&
                self.modoAlteracaoDadosPessoais !== eduEnumsConsts.EduDadosCadastraisAlunoEnum.PermiteAlteracao) {
                return $filter('i18n')('btn-solicitar-alteracao', [], 'js/aluno/dados-pessoais');
            }

            return $filter('i18n')('btn-atualizar-informacoes', [], 'js/aluno/dados-pessoais');
        }

        /**
         * Verifica se o usuário pode acessar uma determinada tab
         * @param {string} tabName - Nome da tab para verificar
         *
         * @returns {bool} - Permissão para acessar a tab
         */
        function exibeTab(tabName) {
            if ($rootScope.objPermissions === null) {
                return false;
            }

            switch (tabName) {
                case 'dados-pessoais':
                    return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_DADOSPESSOAIS);
                case 'dados-profissionais':
                    return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_DADOSPROFISSIONAIS);
                case 'responsaveis':
                    return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_RESPONSAVEIS);
                case 'documentos':
                    return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_DOCUMENTOSOBRIGATORIOS);
                case 'movimentacao-academica':
                    return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_MOVIMENTACAOACADEMICA);
                case 'ficha-medica':
                    return exibirTabFichaMedica();
            }
        }


        function exibirTabFichaMedica() {
            return angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_FICHA_MEDICA) && state.params.ocultarTab !== 'ficha-medica';
        }

        /**
         * Carrega parâmetros do Educacional.
         */
        function carregarParametrosEdu() {
            EduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (params) {
                parametrosEdu = params;
            });
        }

        /**
         * Preenche a coluna de NOME da Filiação do aluno.
         */        
        function gridFiliacaoNOME(item) {

            if (item.NOME == null) {
                return '';
            }

            return '<span>' + eduUtilsService.escapeHtml(item.NOME) + '</span>';
        }
        
        /**
         * Preenche a coluna de TIPORELACIONAMENTO da Filiação do aluno.
         */        
        function gridFiliacaoRELACIONAMENTO(item) {

            if (item.TIPORELACIONAMENTO == null) {
                return '';
            }

            var tipoRelacionamento = (item.TIPORELACIONAMENTO == 'P' ? 
            $filter('i18n')('l-pai', [], 'js/aluno/dados-pessoais') : 
            $filter('i18n')('l-mae', [], 'js/aluno/dados-pessoais'));

            return '<span>' + eduUtilsService.escapeHtml(tipoRelacionamento) + '</span>';
        }
    }
});
