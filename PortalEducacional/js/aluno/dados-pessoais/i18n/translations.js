[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Dados pessoais",
			"en": "Personal data",
			"es": "Datos personales"
        },
        "l-dados-pessoais":{
            "pt": "Dados pessoais",
			"en": "Personal data",
			"es": "Datos personales"
        },
        "l-identificacao":{
            "pt": "Identificação",
			"en": "Identification",
			"es": "Identificación"
        },
        "l-dados-profissionais":{
            "pt": "Dados profissionais",
			"en": "Professional data",
			"es": "Datos profesionales"
        },
        "l-movimentacao-academica":{
            "pt": "Movimentação acadêmica",
			"en": "Academic movement",
			"es": "Movimiento académico"
        },
        "l-responsaveis":{
            "pt": "Responsáveis",
			"en": "People in charge",
			"es": "Responsables"
        },
        "l-documentos":{
            "pt": "Documentos",
			"en": "Documents",
			"es": "Documentos"
        },
        "l-data-nascimento":{
            "pt": "Data de nascimento",
			"en": "Date of birth",
			"es": "Fecha de nacimiento"
        },
        "l-naturalidade":{
            "pt": "Naturalidade",
			"en": "Place of birth",
			"es": "Lugar de nacimiento"
        },
        "l-estado-natal":{
            "pt": "Estado natal",
			"en": "State of birth",
			"es": "Estado/Prov/Reg natal"
        },
        "l-contato":{
            "pt": "Contato",
			"en": "Contact",
			"es": "Contacto"
        },
        "l-endereco":{
            "pt": "Endereço",
			"en": "Address",
			"es": "Dirección"
        },
        "l-ra":{
            "pt": "Registro acadêmico",
			"en": "Academic record",
			"es": "Registro académico"
        },
        "l-curso":{
            "pt": "Curso",
			"en": "Course",
			"es": "Curso"
        },
        "l-habilitacao":{
            "pt": "Habilitação",
			"en": "Specialization",
			"es": "Especialidad"
        },
        "l-turno":{
            "pt": "Turno",
			"en": "Shift",
			"es": "Turno"
        },
        "l-email-pessoal":{
            "pt": "E-mail",
			"en": "E-mail",
			"es": "E-mail"
        },
        "l-email-corporativo":{
            "pt": "E-mail corporativo",
			"en": "Corporate e-mail",
			"es": "E-mail corporativo"
        },
        "l-telefone-residencial":{
            "pt": "Telefone residencial",
			"en": "Home phone number",
			"es": "Teléfono residencial"
        },
        "l-telefone-comercial":{
            "pt": "Telefone comercial",
			"en": "Business phone number",
			"es": "Teléfono comercial"
        },
        "l-telefone-celular":{
            "pt": "Telefone celular",
			"en": "Mobile phone number",
			"es": "Teléfono celular"
        },
        "l-telefone-corporativo":{
            "pt": "Telefone corporativo",
			"en": "Corporate phone number",
			"es": "Teléfono corporativo"
        },
        "l-telefone-fax":{
            "pt": "Fax",
			"en": "Fax number",
			"es": "Fax"
        },
        "l-logradouro":{
            "pt": "Logradouro",
			"en": "Address",
			"es": "Vía pública"
        },
        "l-numero":{
            "pt": "Número",
			"en": "Number",
			"es": "Número"
        },
        "l-cep":{
            "pt": "CEP",
			"en": "Postal code",
			"es": "CP"
        },
        "l-cidade":{
            "pt": "Cidade",
			"en": "City",
			"es": "Ciudad"
        },
        "l-bairro":{
            "pt": "Bairro",
			"en": "District",
			"es": "Barrio"
        },
        "l-complemento":{
            "pt": "Complemento",
			"en": "Address line 2",
			"es": "Complemento"
        },
        "l-estado":{
            "pt": "Estado",
			"en": "State",
			"es": "Estado/Prov/Reg"
        },
        "l-pais":{
            "pt": "País",
			"en": "Country",
			"es": "País"
        },
        "l-foto-aluno":{
            "pt": "Fotografia do aluno",
			"en": "Student's picture",
			"es": "Fotografía del alumno"
        },
        "l-filiacao":{
            "pt": "Filiação",
			"en": "Parentage",
			"es": "Filiación"
        },
        "btn-salvar":{
            "pt": "Salvar",
			"en": "Save",
			"es": "Grabar"
        },
        "l-mensagem-atualizacao":{
            "pt": "Deseja atualizar os dados?",
			"en": "Update data?",
			"es": "¿Desea actualizar los datos?"
        },
        "l-mensagem-dados-atualizados":{
            "pt": "Dados atualizados!",
			"en": "Updated data!",
			"es": "¡Datos actualizados!"
        },
        "l-confirmar":{
            "pt": "Confirmar",
			"en": "Confirm",
			"es": "Confirmar"
        },
        "l-responsaveis-cadastrados":{
            "pt": "Responsáveis cadastrados",
			"en": "Registered people in charge",
			"es": "Responsables registrados"
        },
        "l-responsaveis-temporarios":{
            "pt": "Responsáveis Temporários",
			"en": "Temporary people in charge",
			"es": "Responsables temporales"
        },
        "l-NOME":{
            "pt": "Nome",
			"en": "Name",
			"es": "Nombre"
        },
        "l-TELEFONE":{
            "pt": "Telefone",
			"en": "Telephone number",
			"es": "Teléfono"
        },
        "l-RESPONSAVEL":{
            "pt": "Responsável",
			"en": "Person in charge",
			"es": "Responsable"
        },
        "l-PERMISSAO":{
            "pt": "Permissão",
			"en": "Permission",
			"es": "Permiso"
        },
        "l-OBSERVACAO":{
            "pt": "Observação",
			"en": "Note",
			"es": "Observación"
        },
        "l-PARENTESCO":{
            "pt": "Parentesco",
			"en": "Kinship",
			"es": "Parentesco"
        },
        "l-Nenhum-Registro-Encontrado":{
            "pt": "Nenhum registro encontrado!",
			"en": "No record found",
			"es": "¡No se encontró ningún registro!"
        },
        "l-Responsavel-Academico":{
            "pt": "Acadêmico",
			"en": "Academic",
			"es": "Académico"
        },
        "l-Responsavel-Financeiro":{
            "pt": "Financeiro",
			"en": "Financials",
			"es": "Financiero"
        },
        "l-Atencao":{
            "pt": "Atenção!",
			"en": "Attention",
			"es": "¡Atención!"
        },
        "l-titulo-responsavel-temporario":{
            "pt":"Responsável Temporário",
			"en":"Temporary person in charge",
			"es":"Responsable temporal"
        },
        "l-tipo-responsavel":{
            "pt":"Tipo de responsável",
			"en":"Type of person in charge",
			"es":"Tipo de responsable"
        },
         "l-status":{
            "pt":"Status",
			"en":"Status",
			"es":"Estatus"
        },
         "l-cpf":{
            "pt":"CPF",
			"en":"CPF",
			"es":"RCPF"
        },
        "l-cidade-natal":{
            "pt":"Cidade natal",
			"en":"City of birth",
			"es":"Ciudad natal"
        },
        "l-ativo":{
            "pt":"Ativo",
			"en":"Active",
			"es":"Activo"
        },
         "l-inativo":{
            "pt":"Inativo",
			"en":"Inactive",
			"es":"Inactivo"
        },
        "l-serie":{
            "pt":"Série",
			"en":"Series",
			"es":"Serie"
        },
        "l-msg-campo-obrigatorio":{
            "pt":"campo de preenchimento obrigatório",
			"en":"field required",
			"es":"campo de cumplimentación obligatoria"
        },
        "l-empresa":{
          "pt": "Empresa",
		  "en": "Company",
		  "es": "Empresa"
        },
        "l-cargo":{
          "pt": "Cargo",
		  "en": "Position",
		  "es": "Cargo"
        },
        "l-msg-cancelar-inclusa-resp-temp":{
          "pt": "Deseja realmente cancelar a inclusão do responsável temporário?",
		  "en": "Do you want to cancel the inclusion of the temporary person in charge?",
		  "es": "¿Realmente desea anular la inclusión del responsable temporal?"
        },
        "l-telefone-empresa":{
          "pt": "Telefone da empresa",
		  "en": "Company phone number",
		  "es": "Teléfono de la empresa"
        },
        "l-horario-trabalho":{
          "pt": "Horário de Trabalho",
		  "en": "Work hours",
		  "es": "Horario de trabajo"
        },
        "btn-solicitar-alteracao":{
          "pt": "Solicitar alteração",
		  "en": "Request change",
		  "es": "Solicitar modificación"
        },
        "btn-atualizar-informacoes":{
          "pt": "Atualizar informações",
		  "en": "Update information",
		  "es": "Actualizar información"
        },
        "l-observacao":{
          "pt": "Observação",
		  "en": "Note",
		  "es": "Observación"
        },
        "l-dia-mes-ano":{
          "pt": "dia/mês/ano",
		  "en": "day/month/year",
		  "es": "día/mes/año"
        },
        "l-data-movimentacao":{
          "pt": "Data",
		  "en": "Date",
		  "es": "Fecha"
        },
        "l-situacao-matricula-anterior":{
          "pt": "Situação anterior",
		  "en": "Previous status",
		  "es": "Situación anterior"
        },
        "l-atividade":{
          "pt": "Atividade",
		  "en": "Activity",
		  "es": "Actividad"
        },
        "l-motivo":{
          "pt": "Motivo",
		  "en": "Reason",
		  "es": "Motivo"
        },
        "l-turma":{
          "pt": "Turma",
		  "en": "Class",
		  "es": "Grupo"
        },
        "l-situacao-matricula":{
          "pt": "Situação atual",
		  "en": "Current status",
		  "es": "Situación actual"
        },
        "l-usuario":{
          "pt": "Usuário",
		  "en": "User",
		  "es": "Usuario"
        },
        "l-operacao":{
          "pt": "Operação",
		  "en": "Operation",
		  "es": "Operación"
        },
        "l-periodo":{
          "pt": "Período",
		  "en": "Period",
		  "es": "Período"
        },
        "btn-add-responsavel-temporario":{
            "pt": "Adicionar responsável temporário",
			"en": "Add temporary person in charge",
			"es": "Agregar responsable temporal"
        },
        "l-msg-cep-nao-encontrado":{
            "pt": "CEP não encontrado! Informe os dados manualmente.",
			"en": "Postal code not found! Enter data manually.",
			"es": "¡CP no encontrado! Informe los datos manualmente."
        },
        "l-situacao":{
            "pt": "Situação",
			"en": "Status",
			"es": "Situación"
        },
        "l-descricao":{
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "l-quantidade-entregue":{
            "pt": "Quantidade entregue",
			"en": "Amount delivered",
			"es": "Cantidad entregada"
        },
        "l-quantidade-prevista":{
            "pt": "Quantidade prevista",
			"en": "Amount expected",
			"es": "Cantidad prevista"
        },
        "l-data-entrega":{
            "pt": "Data entrega",
			"en": "Delivery date",
			"es": "Fecha de entrega"
        },
        "l-data-prazo":{
            "pt": "Data prazo",
			"en": "Deadline",
			"es": "Fecha de plazo"
        },
        "l-enade":{
            "pt": "ENADE",
			"en": "ENADE",
			"es": "ENADE"
        },
        "l-enade-situacao-aluno":{
            "pt": "Classificação Enade",
			"en": "Enade classification",
			"es": "Clasificación Enade"
        },
        "l-enade-situacao-aluno-mec":{
            "pt": "Situação MEC",
			"en": "MEC status",
			"es": "Situación MEC"
        },
        "l-enade-data-prova":{
            "pt": "Data da prova",
			"en": "Test date",
			"es": "Fecha de la prueba"
        },
        "l-enade-status-comparecimento":{
            "pt": "Compareceu",
			"en": "Attended",
			"es": "Compareció"
        },
        "l-periodo-letivo":{
            "pt": "Período letivo",
			"en": "School term",
			"es": "Período lectivo"
        },
        "l-entregue":{
            "pt": "Entregue",
			"en": "Delivered",
			"es": "Entregado"
        },
        "l-tipo-ingresso":{
            "pt": "Tipo de ingresso",
			"en": "Type of admission",
			"es": "Tipo de ingreso"
        },
        "l-data-ingresso":{
            "pt": "Data do ingresso",
			"en": "Date of admission",
			"es": "Fecha del ingreso"
        },
        "l-media-global":{
            "pt": "   Média global",
			"en": "   Global average",
			"es": "   Promedio global"
        },
        "l-coeficiente-rendimento":{
            "pt": "Coefic. rendimento",
			"en": "Performance coeff.",
			"es": "Coefic. Rendimiento"
        },
        "l-conclusao-curso":{
            "pt": "Dt. conclusão curso",
			"en": "Course conclusion date",
			"es": "Fch. Conclusión de curso"
        },
        "l-pendente":{
            "pt": "Pendente",
			"en": "Pending",
			"es": "Pendiente"
        },
        "l-email-invalido":{
            "pt": "e-mail inválido",
			"en": "invalid e-mail",
			"es": "e-mail no válido"
		},
        "l-ficha-medica":{
            "pt": "Ficha Médica",
			"en": "Medical record",
			"es": "Ficha médica"
		},
        "l-pergunta-alergia":{
            "pt": "A criança é alérgica?",
			"en": "Is the child allergic?",
			"es": "¿El niño es alérgico?"
		},
        "l-txtarea-alergia":{
            "pt": "Alergias",
			"en": "Allergies",
			"es": "Alergias"
		},
        "l-pergunta-medicamento":{
            "pt": "Faz uso de medicamentos?",
			"en": "Do they take any medication?",
			"es": "¿Toma algún medicamento?"
		},
        "l-txtarea-medicamentos":{
            "pt": "Medicamentos",
			"en": "Medication",
			"es": "Medicamentos"
		},
        "l-txtarea-socorro-hosp":{
            "pt": "Socorro hospitalar",
			"en": "Hospital assistance",
			"es": "Socorro hospitalario"
		},
        "l-txtarea-contato-medico":{
            "pt": "Médico (nome/contato)",
			"en": "Physician (name/contact)",
			"es": "Médico (nombre/contacto)"
		},
        "l-txtarea-tratamentos":{
            "pt": "Tratamentos",
			"en": "Treatment",
			"es": "Tratamientos"
		},
        "l-txtarea-outros":{
            "pt": "Outros",
			"en": "Other",
			"es": "Otros"
		},
      "l-pai":{
          "pt": "PAI",
          "en": "FATHER",
          "es": "PADRE"
    },
      "l-mae":{
          "pt": "MÃE",
          "en": "MOTHER",
          "es": "MADRE"
    },
      "l-tipo-relacionamento":{
          "pt": "Tipo de relacionamento",
          "en": "Type of relationship",
          "es": "Tipo de relación"
    }
  }
]
