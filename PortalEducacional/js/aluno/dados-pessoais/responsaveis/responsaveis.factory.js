/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduResponsaveisModule
* @name eduResponsaveisFactory
* @object factory
*
* @created 07/10/2016 v12.1.15
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada para os responsáveis do Aluno.
*/
define(['aluno/dados-pessoais/responsaveis/responsaveis.module'], function () {

    'use strict';

    angular
        .module('eduResponsaveisModule')
        .factory('eduResponsaveisFactory', eduResponsaveisFactory);

    eduResponsaveisFactory.$inject = ['$totvsresource'];

    function eduResponsaveisFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/:method';
        var factory = $totvsresource.REST(url, {}, {});
        
        factory.getResponsaveis = getResponsaveis;
        factory.atualizarResponsavelAcad = atualizarResponsavelAcad;
        factory.atualizarResponsavelFin = atualizarResponsavelFin;
        factory.atualizarResponsavelTemp = atualizarResponsavelTemp;
        factory.atualizarResponsavelTempEnd = atualizarResponsavelTempEnd;
        factory.salvarResponsavelTemp = salvarResponsavelTemp;
        
        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************
        
        function getResponsaveis(callback) {
            var parameters = {
                method: 'Responsaveis'
            };
            
            return factory.TOTVSGet(parameters, callback);
        }
        
        function atualizarResponsavelAcad(codPessoa, model, callback) {
            var parameters = {
                method: 'ResponsavelAcademico/Endereco',
                codPessoa: codPessoa
            };
            
            return factory.TOTVSUpdate(parameters, model, callback);
        }
        
        function atualizarResponsavelFin(codColigadaCfo, codCfo, model, callback) {
            var parameters = {
                method: 'ResponsavelFinanceiro/Endereco',
                codColigadaCfo: codColigadaCfo,
                codCfo: codCfo
            };
            
            return factory.TOTVSUpdate(parameters, model, callback);
        }
        
        function atualizarResponsavelTemp(codPessoa, model, callback) {
            var parameters = {
                method: 'ResponsavelTemporario',
                codPessoa: codPessoa
            };
            
            return factory.TOTVSUpdate(parameters, model, callback);
        }
        
        function atualizarResponsavelTempEnd(codPessoa, model, callback) {
            var parameters = {
                method: 'ResponsavelTemporario/Endereco',
                codPessoa: codPessoa
            };
            
            return factory.TOTVSUpdate(parameters, model, callback);
        }
        
        function salvarResponsavelTemp(model, callback) {
            var parameters = {
                method: 'ResponsavelTemporario'
            };

            return factory.TOTVSSave(parameters, model, callback);
        }
    }
});