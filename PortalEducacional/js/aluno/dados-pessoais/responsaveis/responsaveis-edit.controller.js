/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduResponsaveisModule
 * @name eduResponsaveisController
 * @object controller
 *
 * @created 07/10/2016 v12.1.15
 * @updated
 *
 * @requires responsaveis.module
 *
 * @dependencies eduResponsaveisFactory
 *
 * @description Controller do cadastro dos Responsaveis do Aluno
 */
define(['aluno/dados-pessoais/responsaveis/responsaveis.module',
    'aluno/dados-pessoais/responsaveis/responsaveis.factory',
    'utils/edu-enums.constants',
    'utils/edu-utils.factory'],
    function () {

        'use strict';

        angular
            .module('eduResponsaveisModule')
            .controller('eduResponsaveisController', EduResponsaveisController);

        EduResponsaveisController.$inject = [
            'TotvsDesktopContextoCursoFactory',
            'eduResponsaveisFactory',
            'eduEnumsConsts',
            'eduUtilsFactory',
            'i18nFilter',
            '$state',
            '$window',
            'totvs.app-notification.Service'
        ];

        /**
         * Controller do cadastro dos responsáveis temporários do aluno
         * @param   {object} TotvsDesktopContextoCursoFactory Objeto factory para manipulação do contexto Educacional
         * @param   {object} eduResponsaveisFactory           Objeto factory para os serviços dos responsáveis
         * @param   {object} EduEnumsConsts                   Constantes do Educacional
         * @param   {object} eduUtilsFactory                  Objeto factory para os serviços comuns
         * @param   {object} i18nFilter                       Objeto para manipulação da tradução
         * @param   {object} $state                           Objeto de rotas
         * @param   {object} $window                          Objeto window
         * @param   {object} totvsNotification                Objeto para exibir mensagens de respostas
         */
        function EduResponsaveisController(TotvsDesktopContextoCursoFactory, eduResponsaveisFactory, EduEnumsConsts, 
                                            eduUtilsFactory, i18nFilter, $state, $window, totvsNotification) {

            var self = this;

            //Máscara de CPF
            self.mask = '999.999.999-99';

            //Contém a modelo de dados dos campos disponíveis no formulário
            self.model = {};

            //Contém metadados dos campos exibidos na view (como exemplo indicação se o campo é obrigatorio)
            //Referente ao cadastro de Responsável Temporário
            self.campos = {};

            //Contém os parâmetros do sistema
            self.parametros = {};
            self.parametros.GestaoPessoas = {};

            self.listPaises = [];
            self.listEstados = [];
            self.listMunicipios = [];
            self.listStatus = [];
            self.listTipoResponsavel = [];
            self.listParentesco = [];

            //Registra eventos utilizados na view
            self.onChangeEstado = onChangeEstado;
            self.onChangePais = onChangePais;
            //Registra métodos usados na view
            self.cancel = cancel;
            self.save = save;
            self.saveNew = saveNew;

            init();

            /**
             * Função de inicialização
             */
            function init() {

                if (verificaContextoEnsinoBasico()) {
                    getMascaraTelefone();
                    carregarListaParametrosTotvsGestaoPessoasAsync(configurarCamposObrigatorios);
                    carregarListasTodosCadastrosBasicosAsync(null);
                }
                else {
                    $state.go('mural.start', null);
                }
            }

            /**
             * Configura os campos obrigatórios para o formulário
             * @param {boolean} unicidadePessoaVerificadaPorCPF Verdadeiro se unicidade de pessoa é por CPF
             * @param {boolean} cpfObrigatorio                  Verdadeiro se CPF é obrigatório
             */
            function configurarCamposObrigatorios(unicidadePessoaVerificadaPorCPF, cpfObrigatorio) {

                self.campos.nome = {};
                self.campos.dataNascimento = {};
                self.campos.parentesco = {};
                self.campos.observacao = {};
                self.campos.CPF = {};
                self.campos.pais = {};
                self.campos.estado = {};
                self.campos.municipio = {};
                self.campos.tipoResponsavel = {};
                self.campos.status = {};

                self.campos.nome.obrigatorio = true;
                self.campos.dataNascimento.obrigatorio = true;
                self.campos.parentesco.obrigatorio = false;
                self.campos.observacao.obrigatorio = false;

                self.campos.CPF.obrigatorio = cpfObrigatorio || unicidadePessoaVerificadaPorCPF;
                self.campos.pais.obrigatorio = !unicidadePessoaVerificadaPorCPF;
                self.campos.estado.obrigatorio = !unicidadePessoaVerificadaPorCPF;
                self.campos.municipio.obrigatorio = !unicidadePessoaVerificadaPorCPF;
                self.campos.tipoResponsavel.obrigatorio = false;
                self.campos.status.obrigatorio = true;
            }

            /**
             * Obtém a listagem dos cadastros básicos
             * @param {function} callback Função de retorno
             */
            function carregarListasTodosCadastrosBasicosAsync(callback) {

                //Carrega lista de tipos de responsaveis
                carregarListaTiposResponsavelAsync(callback);
                //Carrega lista de tipos de parentesco
                carregarListaTiposParentescoAsync(callback);
                //Carregar lista de status
                carregarListaTiposStatus(callback);
                //Carrega lista de países
                carregarListaPaisesAsync(callback);
            }

            /**
             * Obtém a listagem de países
             * @param   {function} callback Função de retorno
             */
            function carregarListaPaisesAsync(callback) {

                self.listPaises = [];

                eduUtilsFactory.getListaPaisesAsync(function (result) {

                    if (result.GPais) {
                        self.listPaises = result.GPais;

                        //Verifica se existe o país Brasil na lista de países
                        var paisBrasil = self.listPaises.find(function (pais) {
                            if (pais.IDPAIS === 1 || pais.CODPAIS === 'BRA') {
                                return pais;
                            }
                        });

                        //Se contém o país brasil, seleciona o mesmo por default e carrega os estados
                        if (paisBrasil) {
                            self.model.IDPAIS = paisBrasil.IDPAIS;
                            carregarListaEstadosAsync(paisBrasil.IDPAIS, callback);
                        }
                    }
                    else {
                        if (typeof callback === 'function') {
                            callback(result);
                        }
                    }
                });
            }

            /**
             * Obtém a listagem dos estados
             * @param {int}      idPAIS   Identificador do país
             * @param {function} callback Função de retorno
             */
            function carregarListaEstadosAsync(idPAIS, callback) {

                self.listEstados = [];

                eduUtilsFactory.getListaEstadosAsync(idPAIS, function (result) {

                    if (result.GEtd) {
                        self.listEstados = result.GEtd;
                    }

                    if (typeof callback === 'function') {
                        callback(result);
                    }
                });
            }

            function carregarListaMunicipiosAsync(idPAIS, codETD, callback) {

                eduUtilsFactory.getListaMunicipiosAsync(idPAIS, codETD, function (result) {

                    self.listMunicipios = [];

                    if (result.GMUNICIPIO) {
                        self.listMunicipios = result.GMUNICIPIO;
                    }

                    if (typeof callback === 'function') {
                        callback(result);
                    }
                });
            }

            /**
             * Obtém a listagem dos tipos de responsáveis
             * @param {function} callback Função de retorno
             */
            function carregarListaTiposResponsavelAsync(callback) {

                eduUtilsFactory.getListaTiposResponsavelAsync(function (result) {

                    self.listTipoResponsavel = [];

                    if (result.STIPORESPONSAVEL) {
                        self.listTipoResponsavel = result.STIPORESPONSAVEL;
                    }

                    if (typeof callback === 'function') {
                        callback(result);
                    }
                });
            }

            /**
             * Obtém a listagem de status
             * @param {function} callback Função de retorno
             */
            function carregarListaTiposStatus(callback) {

                var objAtivo = {},
                    objInativo = {};

                objAtivo.CODIGO = 'A';
                objAtivo.DESCRICAO = i18nFilter('l-ativo', [], 'js/aluno/dados-pessoais');

                objInativo.CODIGO = 'I';
                objInativo.DESCRICAO = i18nFilter('l-inativo', [], 'js/aluno/dados-pessoais');

                self.listStatus.push(objAtivo);
                self.listStatus.push(objInativo);

                //Define valor padrão
                self.model.STATUS = 'A';

                if (typeof callback === 'function') {
                    callback();
                }
            }

            /**
             * Obtém a listagem dos tipos de parentes
             * @param {function} callback Função de retorno
             */
            function carregarListaTiposParentescoAsync(callback) {

                eduUtilsFactory.getListaTiposParentescoAsync(function (result) {

                    self.listParentesco = [];

                    if (result.PCODPARENT) {
                        self.listParentesco = result.PCODPARENT;
                    }

                    if (typeof callback === 'function') {
                        callback(result);
                    }
                });
            }

            /**
             * Obtém a máscara do telefone
             */
            function getMascaraTelefone() {
                self.mascaraTelefone = TotvsDesktopContextoCursoFactory.getMascarasSistema().mascaraTelefone;
            }

            /**
             * Carrega a listagem dos parâmetros do Educacional
             * @param {function} callback Função de retorno
             */
            function carregarListaParametrosTotvsGestaoPessoasAsync(callback) {

                eduUtilsFactory.getParametrosTOTVSGestaoPessoasAsync(function (result) {

                    self.parametros.GestaoPessoas = {};

                    if (result) {
                        self.parametros.GestaoPessoas = result;

                        if (typeof callback === 'function') {
                            callback(self.parametros.GestaoPessoas.UnicidadePessoaPorCPF, self.parametros.GestaoPessoas.CpfObrigatorio);
                        }
                    }
                    else {
                        if (typeof callback === 'function') {
                            callback(result);
                        }
                    }
                });
            }

            /**
             * Verifica se o contexto é de ensino básico
             * @returns {boolean} Verdadeiro se o contexto do aluno é de ensino básico
             */
            function verificaContextoEnsinoBasico() {

                var contextoCurso = TotvsDesktopContextoCursoFactory.getCursoSelecionado();

                if (contextoCurso) {
                    return (parseInt(contextoCurso.cursoSelecionado.APRESENTACAO) === EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico);
                }
                else {
                    return false;
                }
            }

            /**
             * Ação de cancelar do formulário
             */
            function cancel() {
                totvsNotification.question({
                    title: i18nFilter('l-Confirmacao'),
                    text: i18nFilter('l-msg-cancelar-inclusa-resp-temp', [], 'js/aluno/dados-pessoais'),
                    cancelLabel: 'l-no',
                    confirmLabel: 'l-yes',
                    size: 'md',
                    callback: function (isPositiveResult) {
                        if (isPositiveResult) {
                            $state.go('dados-pessoais.start', { tab: 'responsaveis' });
                        }
                    }
                });
            }

            /**
             * Salvar o responsável temporário
             * @param {boolean} blnSaveNew Verdadeiro se é para salvar e continuar na página
             */
            function save(blnSaveNew) {
                var saveNew = blnSaveNew || false;
                
                var validacaoOK = validarFormulario();

                if (validacaoOK)
                {
                    eduResponsaveisFactory.salvarResponsavelTemp([self.model], function (result) {
                        if (result && angular.isUndefined(result['RMException:StackTrace']))
                        {
                            if (saveNew) {
                                $state.go($state.current, {}, { reload: true });
                            }
                            else {
                                $state.go('dados-pessoais.start', { tab: 'responsaveis' });
                            }  
                        }
                    });
                }
            }

            /**
             * Salvar o responsável temporário e continuar na página
             */
            function saveNew() {
                self.save(true);
            }

            /**
             * Validação dos campos do formulário
             * @returns {boolean} Verdadeiro se os campos estão ok
             */
            function validarFormulario() {

                if (self.formulario.$valid) {
                    // converte de timespan para datetime
                    self.model.DTNASCIMENTO = new Date(self.model.DTNASCIMENTO);
                    if (self.model.CPF === '') {
                        self.model.CPF = null;
                    }
                        
                    return true;
                }
                else {
                    
                    if (angular.isDefined(self.formulario.$error.email) && self.formulario.$error.email.length > 0) {
                        setFocusCampo('campoEmail');
                        
                        totvsNotification.notify({
                            type: 'warning',
                            title: i18nFilter('l-Atencao', [], 'js/aluno/dados-pessoais'),
                            detail: i18nFilter('l-email-invalido', [], 'js/aluno/dados-pessoais')
                        });
                    }
                    else {
                        for (var nomeCampo in self.formulario) {

                            if (nomeCampo.startsWith('campo') && self.formulario[nomeCampo].$invalid) {
                                setFocusCampo(nomeCampo);
                                break;
                            }
                        }   

                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-Atencao'),
                            detail: i18nFilter('l-msg-campos-preenchimento-obrigatorio')
                        });
                    }
                }
            }

            /**
             * Seta o focus de um determinado campo do formulário
             * @param {string} nomeCampo Nome do campo para setar o focus
             */
            function setFocusCampo(nomeCampo) {
                var seletor = '[name=' + nomeCampo + '] input, [name=' + nomeCampo + '] select';

                $(seletor).focus();
            }

            /**
             * Método ao alterar um país na lista
             */
            function onChangePais() {

                self.model.ESTADONATAL = null;
                self.model.NATURALIDADE = null;

                self.listEstados = [];
                self.listMunicipios = [];

                //Carrega lista de estados
                carregarListaEstadosAsync(self.model.IDPAIS, null);
            }

            /**
             * Método ao alterar um estado na lista
             */
            function onChangeEstado() {

                self.model.NATURALIDADE = null;

                self.listMunicipios = [];

                //Verifica se o estado e país foi preenchido
                if (self.model.ESTADONATAL && self.model.IDPAIS) {
                    carregarListaMunicipiosAsync(self.model.IDPAIS, self.model.ESTADONATAL);
                }
                else {
                    self.model.NATURALIDADE = '';
                }

            }
        }
    });
