/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.19
 * (c) 2017-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/dados-pessoais/ficha-medica/ficha-medica.module',
        'aluno/dados-pessoais/ficha-medica/ficha-medica.factory'], function () {

    'use strict';

    angular
        .module('eduFichaMedicaModule')
        .controller('eduFichaMedicaController', EduFichaMedicaController);

    EduFichaMedicaController.$inject = ['$rootScope', '$filter', '$state', 'maskFilter', 'eduFichaMedicaFactory', 'eduEnumsConsts', 'totvs.app-notification.Service', 'i18nFilter'];

    function EduFichaMedicaController($rootScope, $filter, state, maskFilter, EduFichaMedicaFactory, eduEnumsConsts, totvsNotification, i18nFilter) {

        var self = this;

		// vars e propriedades
		self.fichaMedica = {
			temAlergia: null,
			alergia: '',
			usaMedicamento: null,
			remedio: '',
			socHosp: '',
			medico: '',
			tratamento: '',
			outro: ''
		};

        self.rdSimNao = [
            { value: 0, label: i18nFilter('l-no') },
            { value: 1, label: i18nFilter('l-yes') }
        ];

        self.disabled = !angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_FICHA_MEDICA_EDITAR);

		// métodos
		self.save = save;

        // init
        init();

        /**
         * Metodo de inicialização do controller
         */
        function init() {
			loadFichaMedica();
		}

		/**
		 * Salva os dados da ficha médica
		 */
		function save() {
			EduFichaMedicaFactory.updateFichaMedica(self.fichaMedica);
		}

		function loadFichaMedica() {
			EduFichaMedicaFactory.getFichaMedica(function (result) {
				if (result && angular.isDefined(result.SALUNOFICHAMEDICA)) {
					if (result.SALUNOFICHAMEDICA.length > 0) {
						self.fichaMedica.temAlergia = (result.SALUNOFICHAMEDICA[0].ALERGIA !== null ? 1 : 0);
						self.fichaMedica.alergia = result.SALUNOFICHAMEDICA[0].ALERGIA;
						self.fichaMedica.usaMedicamento = (result.SALUNOFICHAMEDICA[0].REMEDIO !== null ? 1 : 0);
						self.fichaMedica.remedio = result.SALUNOFICHAMEDICA[0].REMEDIO;
						self.fichaMedica.socHosp = result.SALUNOFICHAMEDICA[0].SOCHOSP;
						self.fichaMedica.medico = result.SALUNOFICHAMEDICA[0].MEDICO;
						self.fichaMedica.tratamento = result.SALUNOFICHAMEDICA[0].TRATAMENTO;
						self.fichaMedica.outro = result.SALUNOFICHAMEDICA[0].OUTRO;
					}
                }
            });
		}

    }
});
