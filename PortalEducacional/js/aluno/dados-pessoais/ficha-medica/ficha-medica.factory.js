/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/dados-pessoais/ficha-medica/ficha-medica.module'], function () {

    'use strict';

    angular
        .module('eduFichaMedicaModule')
        .factory('eduFichaMedicaFactory', EduFichaMedicaFactory);

		EduFichaMedicaFactory.$inject = ['$totvsresource'];

    // *********************************************************************************
    // *** Factory
    // *********************************************************************************
    function EduFichaMedicaFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.getFichaMedica = getFichaMedica;
        factory.updateFichaMedica = updateFichaMedica;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function getFichaMedica(callback) {
			var parameters = {
				method: 'FichaMedica'
			};

			return this.TOTVSGet(parameters, callback);
        }

        function updateFichaMedica(fichaMedica, callback) {
			var parameters = {
				method: 'FichaMedica'
			},
            dados = [];
			dados.push(fichaMedica);

			delete dados[0].temAlergia;
			delete dados[0].usaMedicamento;

            return this.TOTVSUpdate(parameters, dados, callback);
        }
    }
});
