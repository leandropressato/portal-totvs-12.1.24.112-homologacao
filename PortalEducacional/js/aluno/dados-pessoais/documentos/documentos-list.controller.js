/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

define(['aluno/dados-pessoais/documentos/documentos.module',
        'aluno/dados-pessoais/documentos/documentos.factory',
        'utils/edu-enums.constants',
        'utils/edu-utils.factory'
    ],
    function () {

        'use strict';

        angular
            .module('eduDocumentosModule')
            .controller('eduDocumentosListController', EduDocumentosListController);

        EduDocumentosListController.$inject = [
            '$rootScope',
            'TotvsDesktopContextoCursoFactory',
            'eduDocumentosFactory',
            'eduEnumsConsts',
            'eduUtilsFactory',
            'i18nFilter',
            '$scope'
        ];

        function EduDocumentosListController($rootScope, TotvsDesktopContextoCursoFactory, eduDocumentosFactory, EduEnumsConsts, eduUtilsFactory, i18nFilter, $scope) {

            var self = this;

            self.objDocumentosList = [];
            self.periodoLetivo = 0;

            self.getContexto = getContexto;
            self.getDocumentosObrigatorios = getDocumentosObrigatorios;
            self.getDocumentosObrigatoriosPorIdPerlet = getDocumentosObrigatoriosPorIdPerlet;
            self.periodoLetivoAlterado = periodoLetivoAlterado;
            self.formatarColunaDtEntrega = formatarColunaDtEntrega;
            self.formatarColunaDtPrevista = formatarColunaDtPrevista;
            self.formatarColunaSituacao = formatarColunaSituacao;
            self.origemMatricula = $rootScope.origemMatricula;

            // Assina o evento de alteração de período
            $scope.$on('alteraPeriodoLetivoEvent', getDocumentosObrigatorios);

            init();

            function init() {
                self.gridOptions = {
                    scrollable: true,
                    sortable: false,
                    resizable: true,
                    selectable: false
                };

                if (self.origemMatricula) {
                    self.periodoLetivo = $rootScope.plSelecionadoMatricula.IDPERLET;
                    self.getDocumentosObrigatoriosPorIdPerlet($rootScope.plSelecionadoMatricula);
                }
                else {
                    self.getContexto();
                    self.getDocumentosObrigatorios();
                }
            }

            function getContexto() {
                self.objContexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
            }

            function getDocumentosObrigatorios() {

                self.objDocumentosList = [];

                eduDocumentosFactory.getDocumentosObrigatorios(
                    function (result) {
                        if (result) {
                            if (angular.isArray(result.SDocExigidos)) {
                                self.objDocumentosList = result.SDocExigidos;
                            }
                        }
                    });
            }

            // Utilizado apenas para recuperar os documentos obrigatórios, durante a matrícula.
            function getDocumentosObrigatoriosPorIdPerlet(plSelecionado) {
                self.objDocumentosList = [];

                eduDocumentosFactory.getDocumentosObrigatoriosPorIdPerlet(plSelecionado.IDPERLET, plSelecionado.IDHABILITACAOFILIAL, 
                    function (result) {
                        if (result) {
                            if (angular.isArray(result.SDocExigidos)) {
                                self.objDocumentosList = result.SDocExigidos;
                            }
                        }
                });
            }

            function periodoLetivoAlterado() {
                $scope.$broadcast('alteraPeriodoLetivoEvent');
            }

            function formatarColunaDtEntrega(item) {

                if (item.DATAENTREGA) {
                    return item.DATAENTREGA.substring(0,10);
                } else {
                    return '';
                }

            }

            function formatarColunaDtPrevista(item) {

                if (item.DATAPREVISTA) {
                    return item.DATAPREVISTA.substring(0,10);
                } else {
                    return '';
                }

            }

            function formatarColunaSituacao(item) {

                var conteudo;

                if (item.ENTREGUE === EduEnumsConsts.EduSimOuNaoEnum.Sim) {
                    conteudo = '<span class="tag legend doc-entregue"></span>';
                }
                else {
                    conteudo = '<span class="tag legend doc-pendente"></span>';
                }

                return '<span>' + conteudo + '</span>';

            }

        }
    });
