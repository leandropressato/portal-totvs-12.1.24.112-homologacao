/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduDocumentosModule
 * @name eduDocumentosFactory
 * @object factory
 *
 * @created 07/10/2016 v12.1.15
 * @updated
 *
 * @dependencies $totvsresource
 *
 * @description Factory utilizada para os documentos do Aluno.
 */
define(['aluno/dados-pessoais/documentos/documentos.module'], function () {

    'use strict';

    angular
        .module('eduDocumentosModule')
        .factory('eduDocumentosFactory', EduDocumentosFactory);

    EduDocumentosFactory.$inject = ['$totvsresource'];

    function EduDocumentosFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.getDocumentosObrigatorios = getDocumentosObrigatorios;
        factory.getDocumentosObrigatoriosPorIdPerlet = getDocumentosObrigatoriosPorIdPerlet;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function getDocumentosObrigatorios(callback) {
            var parameters = {
                method: 'DocumentosObrigatorios'
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getDocumentosObrigatoriosPorIdPerlet(idPerlet, idHabilitacaoFilial, callback) {
            var parameters = {
                method: 'DocumentosObrigatorios/{IdPerLet}/{IdHabilitacaoFilial}',
                IdPerlet: idPerlet,
                IdHabilitacaoFilial: idHabilitacaoFilial
            };

            return factory.TOTVSGet(parameters, callback);
        }
    }
});
