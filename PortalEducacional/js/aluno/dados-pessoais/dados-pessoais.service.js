/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduDadosPessoaisModule
 * @name eduDadosPessoaisService
 * @object service
 *
 * @created 2016-10-13 v12.1.15
 * @updated
 *
 * @requires
 *
 * @description Service utilizada para exibir as informações pessoais do aluno
 */

define(['aluno/dados-pessoais/dados-pessoais.module',
        'aluno/dados-pessoais/dados-pessoais.factory'], function () {

    'use strict';

    angular
        .module('eduDadosPessoaisModule')
        .service('eduDadosPessoaisService', EduDadosPessoaisService);

    EduDadosPessoaisService.$inject = ['$filter',
                                        'totvs.app-notification.Service',
                                        'TotvsDesktopContextoCursoFactory',
                                        'eduDadosPessoaisFactory'];

    function EduDadosPessoaisService($filter,
                                     appNotificationService,
                                     TotvsDesktopContextoCursoFactory,
                                     EduDadosPessoaisFactory) {

        // *********************************************************************************
        // *** Variáveis
        // *********************************************************************************

        var self = this;

        // *********************************************************************************
        // *** Propriedades públicas e métodos
        // *********************************************************************************

        self.Aluno = {};

        self.carregaAlunoDoContexto = carregaAlunoDoContexto;
        self.carregaMascarasSistema = carregaMascarasSistema;
        self.atualizarDados = atualizarDados;

        /**
         * Carrega os dados do Aluno que está carregado no contexto
         *
         * @param {any} callback - função de callback a ser chamada
         */
        function carregaAlunoDoContexto (callback) {

            var contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();

            EduDadosPessoaisFactory.buscaDadosPessoais(contexto.cursoSelecionado, function (result) {
                self.Aluno = new Aluno(result, contexto);

                if (typeof callback === 'function') {
                    callback(self.Aluno);
                }
            });
        }

        /**
         * Carrega as máscaras utilizadas no sistema
         *
         * @returns Objeto com as máscaras do sistema
         */
        function carregaMascarasSistema () {
            return TotvsDesktopContextoCursoFactory.getMascarasSistema();
        }

        /**
         * Metodo para salvar o registro
         *
         * @param {any} pagina - página que deseja ser atulizada
         * @param {any} alunoParam - aluno que deseja atualizar
         * @param {any} callbackUpdate - função callback a ser executada
         */
        function atualizarDados(pagina, alunoParam, callbackUpdate) {
            var contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
            self.Aluno = alunoParam;

            appNotificationService.question({
                title: $filter('i18n')('l-confirmar' , [], 'js/aluno/dados-pessoais/'),
                text: $filter('i18n')('l-mensagem-atualizacao' , [], 'js/aluno/dados-pessoais/'),
                size: 'sm',
                cancelLabel: 'l-no',
                confirmLabel: 'l-yes',
                callback: function (atualizar) {
                    if (atualizar) {

                        var dadosAtualizacao = {};
                        dadosAtualizacao.codColigada = self.Aluno.codColigada;
                        dadosAtualizacao.ra = self.Aluno.ra;

                        if (pagina === 'dados-pessoais') {
                            dadosAtualizacao.email = self.Aluno.email.value;
                            dadosAtualizacao.telefone1 = self.Aluno.telefone1.value;
                            dadosAtualizacao.telefone2 = self.Aluno.telefone2.value;
                            dadosAtualizacao.telefone3 = self.Aluno.telefone3.value;
                            dadosAtualizacao.empresaTelefone = self.Aluno.empresaTelefone.value;
                            dadosAtualizacao.fax = self.Aluno.fax.value;
                            dadosAtualizacao.rua = self.Aluno.rua.value;
                            dadosAtualizacao.numero = parseInt(self.Aluno.numero.value);
                            dadosAtualizacao.bairro = self.Aluno.bairro.value;
                            dadosAtualizacao.cep = self.Aluno.cep.value;
                            dadosAtualizacao.complemento = self.Aluno.complemento.value;
                            dadosAtualizacao.estado = self.Aluno.estado.value;
                            dadosAtualizacao.cidade = self.Aluno.cidade.value;
                            dadosAtualizacao.pais = self.Aluno.pais.value;
                        } else if (pagina === 'dados-profissionais') {
                            dadosAtualizacao.empresaNome = self.Aluno.empresaNome.value;
                            dadosAtualizacao.codOcupacao = self.Aluno.codOcupacao.value;
                            dadosAtualizacao.empresaCep = self.Aluno.empresaCep.value;
                            dadosAtualizacao.empresaRua = self.Aluno.empresaRua.value;
                            dadosAtualizacao.empresaNro = self.Aluno.empresaNumero.value;
                            dadosAtualizacao.empresaComplemento = self.Aluno.empresaComplemento.value;
                            dadosAtualizacao.empresaBairro = self.Aluno.empresaBairro.value;
                            dadosAtualizacao.empresaCidade = self.Aluno.empresaCidade.value;
                            dadosAtualizacao.empresaUF = self.Aluno.empresaUF.value;
                            dadosAtualizacao.empresaTelefone = self.Aluno.empresaTelefone.value;
                            dadosAtualizacao.empresaHorario = self.Aluno.empresaHorario.value;
                        }

                        EduDadosPessoaisFactory.atualizarDadosPessoais(dadosAtualizacao, function (alunoAtualizado) {

                            if (alunoAtualizado && alunoAtualizado['ra'] && alunoAtualizado['codColigada']) {

                                if (typeof callbackUpdate === 'function') {
                                    self.Aluno = {};
                                    self.Aluno = new Aluno(alunoAtualizado, contexto);
                                    callbackUpdate(self.Aluno);
                                }
                            }
                        });
                    }
                }
            });
        }

        /**
         * Carrega o objeto Aluno
         *
         * @param {any} dadosAluno - dados do aluno a ser carregado
         * @param {any} contexto - contexto do aluno
         */
        function Aluno (dadosAluno, contexto) {

            //Contexto
            this.codColigada = contexto.cursoSelecionado.CODCOLIGADA;
            this.nome = contexto.cursoSelecionado.NOMEALUNO;
            this.ra = contexto.cursoSelecionado.RA;
            this.curso = contexto.cursoSelecionado.NOMECURSO;
            this.habilitacao = contexto.cursoSelecionado.NOMEHABILITACAO;
            this.turno = contexto.cursoSelecionado.NOMETURNO;
            this.situacao = situacaoAluno();
            this.apresentacao = contexto.cursoSelecionado.APRESENTACAO;

            //Identificação
            this.foto = '';
            this.dataNascimento = '';
            this.naturalidade = '';
            this.estadoNatal = '';
            this.filiacao = [];

            //Contato
            this.email = new RegrasCampo();
            this.telefone1 = new RegrasCampo();
            this.telefone2 = new RegrasCampo();
            this.telefone3 = new RegrasCampo();
            this.empresaTelefone = new RegrasCampo();
            this.fax = new RegrasCampo();

            //Endereço
            this.rua = new RegrasCampo();
            this.numero = new RegrasCampo();
            this.bairro = new RegrasCampo();
            this.cep = new RegrasCampo();
            this.complemento = new RegrasCampo();
            this.cidade = new RegrasCampo();
            this.estado = new RegrasCampo();
            this.pais = new RegrasCampo();

            //Dados Profissionais
            this.empresaNome = new RegrasCampo();
            this.codOcupacao = new RegrasCampo();
            this.empresaCep = new RegrasCampo();
            this.empresaRua = new RegrasCampo();
            this.empresaNumero = new RegrasCampo();
            this.empresaComplemento = new RegrasCampo();
            this.empresaBairro = new RegrasCampo();
            this.empresaCidade = new RegrasCampo();
            this.empresaUF = new RegrasCampo();
            this.empresaHorario = new RegrasCampo();

            carregarDados(this, dadosAluno);

            function carregarDados(aluno, dados) {
                if (dados && dados['SAluno'][0]) {

                    aluno.foto = dados['SAluno'][0].IMAGEM;
                    aluno.dataNascimento = dados['SAluno'][0].DTNASCIMENTO;
                    aluno.naturalidade = dados['SAluno'][0].NATURALIDADE;
                    aluno.estadoNatal = dados['SAluno'][0].ESTADONATAL;
                    aluno.filiacao = dados['VFiliacao'];

                    aluno.email.value = dados['SAluno'][0].EMAIL;
                    aluno.email.visible = preparaVisible(dados['SAluno'][0].EMAIL);
                    aluno.email.readOnly = preparaReadOnly(aluno.email.visible, dados["CamposPerfil"][0].EMAIL);

                    aluno.telefone1.value = dados['SAluno'][0].TELEFONE1;
                    aluno.telefone1.visible = preparaVisible(dados['SAluno'][0].TELEFONE1);
                    aluno.telefone1.readOnly = preparaReadOnly(aluno.telefone1.visible, dados["CamposPerfil"][0].TELEFONE1);

                    aluno.telefone2.value = dados['SAluno'][0].TELEFONE2;
                    aluno.telefone2.visible = preparaVisible(dados['SAluno'][0].TELEFONE2);
                    aluno.telefone2.readOnly = preparaReadOnly(aluno.telefone2.visible, dados["CamposPerfil"][0].TELEFONE2);

                    aluno.telefone3.value = dados['SAluno'][0].TELEFONE3;
                    aluno.telefone3.visible = preparaVisible(dados['SAluno'][0].TELEFONE3);
                    aluno.telefone3.readOnly = preparaReadOnly(aluno.telefone3.visible, dados["CamposPerfil"][0].TELEFONE3);

                    aluno.fax.value = dados['SAluno'][0].FAX;
                    aluno.fax.visible = preparaVisible(dados['SAluno'][0].FAX);
                    aluno.fax.readOnly = preparaReadOnly(aluno.fax.visible, dados["CamposPerfil"][0].FAX);

                    aluno.rua.value = dados['SAluno'][0].RUA;
                    aluno.rua.visible = preparaVisible(dados['SAluno'][0].RUA);
                    aluno.rua.readOnly = preparaReadOnly(aluno.rua.visible, dados["CamposPerfil"][0].RUA);

                    aluno.numero.value = dados['SAluno'][0].NUMERO;
                    aluno.numero.visible = preparaVisible(dados['SAluno'][0].NUMERO);
                    aluno.numero.readOnly = preparaReadOnly(aluno.numero.visible, dados["CamposPerfil"][0].NUMERO);

                    aluno.bairro.value = dados['SAluno'][0].BAIRRO;
                    aluno.bairro.visible = preparaVisible(dados['SAluno'][0].BAIRRO);
                    aluno.bairro.readOnly = preparaReadOnly(aluno.bairro.visible, dados["CamposPerfil"][0].BAIRRO);

                    aluno.cep.value = dados['SAluno'][0].CEP;
                    aluno.cep.visible = preparaVisible(dados['SAluno'][0].CEP);
                    aluno.cep.readOnly = preparaReadOnly(aluno.cep.visible, dados["CamposPerfil"][0].CEP);

                    aluno.complemento.value = dados['SAluno'][0].COMPLEMENTO;
                    aluno.complemento.visible = preparaVisible(dados['SAluno'][0].COMPLEMENTO);
                    aluno.complemento.readOnly = preparaReadOnly(aluno.complemento.visible, dados["CamposPerfil"][0].COMPLEMENTO);

                    aluno.cidade.value = dados['SAluno'][0].CIDADE;
                    aluno.cidade.visible = preparaVisible(dados['SAluno'][0].CIDADE);
                    aluno.cidade.readOnly = preparaReadOnly(aluno.cidade.visible, dados["CamposPerfil"][0].CIDADE);

                    aluno.estado.value = dados['SAluno'][0].ESTADO;
                    aluno.estado.visible = preparaVisible(dados['SAluno'][0].ESTADO);
                    aluno.estado.readOnly = preparaReadOnly(aluno.estado.visible, dados["CamposPerfil"][0].ESTADO);

                    aluno.pais.value = dados['SAluno'][0].PAIS;
                    aluno.pais.visible = preparaVisible(dados['SAluno'][0].PAIS);
                    aluno.pais.readOnly = preparaReadOnly(aluno.pais.visible, dados["CamposPerfil"][0].PAIS);

                    aluno.empresaNome.value = dados['SAluno'][0].EMPRESANOME;
                    aluno.empresaNome.visible = preparaVisible(dados['SAluno'][0].EMPRESANOME);
                    aluno.empresaNome.readOnly = preparaReadOnly(aluno.empresaNome.visible, dados["CamposPerfil"][0].EMPRESANOME);

                    aluno.codOcupacao.value = dados['SAluno'][0].CODOCUPACAO;
                    aluno.codOcupacao.visible = preparaVisible(dados['SAluno'][0].CODOCUPACAO);
                    aluno.codOcupacao.readOnly = preparaReadOnly(aluno.codOcupacao.visible, dados["CamposPerfil"][0].CODOCUPACAO);

                    aluno.empresaCep.value = dados['SAluno'][0].EMPRESACEP;
                    aluno.empresaCep.visible = preparaVisible(dados['SAluno'][0].EMPRESACEP);
                    aluno.empresaCep.readOnly = preparaReadOnly(aluno.empresaCep.visible, dados["CamposPerfil"][0].EMPRESACEP);

                    aluno.empresaRua.value = dados['SAluno'][0].EMPRESARUA;
                    aluno.empresaRua.visible = preparaVisible(dados['SAluno'][0].EMPRESARUA);
                    aluno.empresaRua.readOnly = preparaReadOnly(aluno.empresaRua.visible, dados["CamposPerfil"][0].EMPRESARUA);

                    aluno.empresaNumero.value = dados['SAluno'][0].EMPRESANUMERO;
                    aluno.empresaNumero.visible = preparaVisible(dados['SAluno'][0].EMPRESANUMERO);
                    aluno.empresaNumero.readOnly = preparaReadOnly(aluno.empresaNumero.visible, dados["CamposPerfil"][0].EMPRESANUMERO);

                    aluno.empresaComplemento.value = dados['SAluno'][0].EMPRESACOMPLEMENTO;
                    aluno.empresaComplemento.visible = preparaVisible(dados['SAluno'][0].EMPRESACOMPLEMENTO);
                    aluno.empresaComplemento.readOnly = preparaReadOnly(aluno.empresaComplemento.visible, dados["CamposPerfil"][0].EMPRESACOMPLEMENTO);

                    aluno.empresaBairro.value = dados['SAluno'][0].EMPRESABAIRRO;
                    aluno.empresaBairro.visible = preparaVisible(dados['SAluno'][0].EMPRESABAIRRO);
                    aluno.empresaBairro.readOnly = preparaReadOnly(aluno.empresaBairro.visible, dados["CamposPerfil"][0].EMPRESABAIRRO);

                    aluno.empresaCidade.value = dados['SAluno'][0].EMPRESACIDADE;
                    aluno.empresaCidade.visible = preparaVisible(dados['SAluno'][0].EMPRESACIDADE);
                    aluno.empresaCidade.readOnly = preparaReadOnly(aluno.empresaCidade.visible, dados["CamposPerfil"][0].EMPRESACIDADE);

                    aluno.empresaUF.value = dados['SAluno'][0].EMPRESAUF;
                    aluno.empresaUF.visible = preparaVisible(dados['SAluno'][0].EMPRESAUF);
                    aluno.empresaUF.readOnly = preparaReadOnly(aluno.empresaUF.visible, dados["CamposPerfil"][0].EMPRESAUF);

                    aluno.empresaHorario.value = dados['SAluno'][0].EMPRESAHORARIO;
                    aluno.empresaHorario.visible = preparaVisible(dados['SAluno'][0].EMPRESAHORARIO);
                    aluno.empresaHorario.readOnly = preparaReadOnly(aluno.empresaHorario.visible, dados["CamposPerfil"][0].EMPRESAHORARIO);

                    aluno.empresaTelefone.value = dados['SAluno'][0].EMPRESATELEFONE;
                    aluno.empresaTelefone.visible = preparaVisible(dados['SAluno'][0].EMPRESATELEFONE);
                    aluno.empresaTelefone.readOnly = preparaReadOnly(aluno.empresaTelefone.visible, dados["CamposPerfil"][0].EMPRESATELEFONE);                   
                }
            }

            /**
             * Situação de matrícula oo aluno baseado no contexto.
             *
             * @returns Situação de matrícula do aluno na habilitação.
             */
            function situacaoAluno () {
                if (contexto.cursoSelecionado.SITMATHABILITACAO) {
                    return contexto.cursoSelecionado.SITMATHABILITACAO;
                }
                else {
                    return contexto.cursoSelecionado.SITMATPERLET;
                }
            }
        }

        /**
         * Carrega o objeto com as regras de cada campo
         */
        class RegrasCampo {
            constructor() {
                this.value = '';
                this.visible = true;
                this.readOnly = false;
            }
        }

        function preparaVisible(campoVisivel) {
            return angular.isDefined(campoVisivel); 
        }

        function preparaReadOnly(campoVisivel, campoReadOnly) {
            return campoVisivel && angular.isDefined(campoReadOnly) && campoReadOnly;
        }
    }
});
