/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduMovimentacaoAcademicaModule
 * @name EduMovimentacaoAcademicaController
 * @object directive
 *
 * @created 2016-10-25 v12.1.15
 * @updated
 *
 * @requires movimentacao-academica.module
 *
 * @dependencies
 *
 * @description Controller de movimentação acadêmica
 */
define(['utils/edu-utils.service',
        'aluno/dados-pessoais/movimentacao-academica/movimentacao-academica.module',
        'aluno/dados-pessoais/movimentacao-academica/movimentacao-academica.factory'], function () {

    'use strict';

    angular.module('eduMovimentacaoAcademicaModule')
           .controller('eduMovimentacaoAcademicaController', EduMovimentacaoAcademicaController);

    EduMovimentacaoAcademicaController.$inject = [
        '$filter',
        'i18nFilter',
        '$compile',
        '$scope',
        'eduEnumsConsts',
        'eduUtilsService',
        'eduMovimentacaoAcademicaFactory',
        'TotvsDesktopContextoCursoFactory'];

    function EduMovimentacaoAcademicaController(
        $filter,
        i18nFilter,
        $compile,
        $scope,
        eduEnumsConsts,
        eduUtilsService,
        eduMovimentacaoAcademicaFactory,
        TotvsDesktopContextoCursoFactory) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        self.MovimentacaoAcademica = [];
        self.DadosEnade = [];
        self.InfoCurso = [];
        self.ContextoEnsinoBasico = contextoEnsinoBasico;

        self.MovimentacaoAcademicaData = MovimentacaoAcademicaData;
        self.MovimentacaoAcademicaSituacaoMatriculaAnterior = MovimentacaoAcademicaSituacaoMatriculaAnterior;
        self.MovimentacaoAcademicaResultado = MovimentacaoAcademicaResultado;
        self.MovimentacaoAcademicaTurma = MovimentacaoAcademicaTurma;
        self.MovimentacaoAcademicaMotivo = MovimentacaoAcademicaMotivo;
        self.MovimentacaoAcademicaSituacaoMatricula = MovimentacaoAcademicaSituacaoMatricula;

        self.EnadePeriodo = EnadePeriodo;
        self.EnadeData = EnadeData;
        self.EnadeStatusComparecimento = EnadeStatusComparecimento;
        self.EnadeSituacaoAlunoENADE = EnadeSituacaoAlunoENADE;
        self.EnadeSituacaoAlunoMEC = EnadeSituacaoAlunoMEC;
        self.periodoLetivoAlterado = periodoLetivoAlterado;

        // Assina o evento de alteração de período
        $scope.$on('alteraPeriodoLetivoEvent', retornaMovimentacaoAcademica);

        init();

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            contextoEnsinoBasico();
            retornaMovimentacaoAcademica();
            retornaDadosEnade();
            retornaInfoCurso();
            setGridOptions();
        }

        function setGridOptions()
        {
            self.gridOptionsMovimentacaoAcademica = {
                groupable: false,
                pageable: false,
                sortable: true,
                resizable: true,
                selectable: false
            };

            self.gridOptionsEnade = {
                groupable: false,
                pageable: false,
                sortable: false,
                resizable: true,
                selectable: false
            };
        }

        function periodoLetivoAlterado() {
            retornaMovimentacaoAcademica();
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function contextoEnsinoBasico() {
            var contextoCurso = TotvsDesktopContextoCursoFactory.getCursoSelecionado();

            if (contextoCurso) {
                self.ContextoEnsinoBasico = (parseInt(contextoCurso.cursoSelecionado.APRESENTACAO) === eduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico);
            }
            else {
                self.ContextoEnsinoBasico = false;
            }
        }

        function retornaMovimentacaoAcademica() {
            eduMovimentacaoAcademicaFactory.getMovimentacaoAcademica(function (result) {
                self.MovimentacaoAcademica = [];
                result.forEach(function (item) {
                    self.MovimentacaoAcademica.push(item);
                });
            });
        }

        function retornaInfoCurso() {
            self.InfoCurso = [];

            eduMovimentacaoAcademicaFactory.getInfoCurso(function (result) {
                self.InfoCurso = result;
            });
        }

        function retornaDadosEnade() {
            self.DadosEnade = [];

            eduMovimentacaoAcademicaFactory.getDadosEnade(function (result) {
                result.forEach(function (item) {
                    self.DadosEnade.push(item);
                });
            });
        }

        // *********************************************************************************
        // *** Formatação das colunas de Movimentação Acadêmica
        // *********************************************************************************

        function MovimentacaoAcademicaData(item) {

            if (item.DTALTERACAO == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + $filter('date')(item.DTALTERACAO, 'dd/MM/yyyy') + '">' +
                        $filter('date')(item.DTALTERACAO, 'dd/MM/yyyy') +
                    '</span>';
        }

        function MovimentacaoAcademicaSituacaoMatriculaAnterior(item) {

            if (item.STATUSANT == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.STATUSANT) + '">' +
                        eduUtilsService.escapeHtml(item.STATUSANT) +
                    '</span>';
        }

        function MovimentacaoAcademicaSituacaoMatricula(item) {

            if (item.STATUS == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.STATUS) + '">' +
                        eduUtilsService.escapeHtml(item.STATUS) +
                    '</span>';
        }

        function MovimentacaoAcademicaResultado(item) {

            if (item.STATUSRES == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.STATUSRES) + '">' +
                        eduUtilsService.escapeHtml(item.STATUSRES) +
                    '</span>';
        }

        function MovimentacaoAcademicaMotivo(item) {

            if (item.MOTIVO == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.MOTIVO) + '">' +
                        eduUtilsService.escapeHtml(item.MOTIVO) +
                    '</span>';
        }

        function MovimentacaoAcademicaTurma(item) {

            if (item.CODTURMA == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.CODTURMA) + '">' +
                        eduUtilsService.escapeHtml(item.CODTURMA) +
                    '</span>';
        }

        // *********************************************************************************
        // *** Formatação das colunas dos dados do Enade
        // *********************************************************************************

        function EnadeData(item) {

            if (item.DTPROVAENADE == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + $filter('date')(item.DTPROVAENADE, 'dd/MM/yyyy') + '">' +
                        $filter('date')(item.DTPROVAENADE, 'dd/MM/yyyy') +
                    '</span>';
        }

        function EnadePeriodo(item) {

            if (item.PLETIVO == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.PLETIVO) + '">' +
                        eduUtilsService.escapeHtml(item.PLETIVO) +
                    '</span>';
        }

        function EnadeStatusComparecimento(item) {

            if (item.COMPARECEUENADE == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.COMPARECEUENADE) + '">' +
                        eduUtilsService.escapeHtml(item.COMPARECEUENADE) +
                    '</span>';
        }

        function EnadeSituacaoAlunoENADE(item) {

            if (item.SELINSTENADE == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.SELINSTENADE) + '">' +
                        eduUtilsService.escapeHtml(item.SELINSTENADE) +
                    '</span>';
        }

        function EnadeSituacaoAlunoMEC(item) {

            if (item.SELMECENADE == null) {
                return '';
            }

            return '<span tooltip-append-to-body="appendToBody=true" ' +
                   'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.SELMECENADE) + '">' +
                        eduUtilsService.escapeHtml(item.SELMECENADE) +
                    '</span>';
        }
    }
});
