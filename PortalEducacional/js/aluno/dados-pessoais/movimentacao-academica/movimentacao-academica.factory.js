/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduResponsaveisModule
* @name eduMovimentacaoAcademicaFactory
* @object factory
*
* @created 07/10/2016 v12.1.15
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada para os responsáveis do Aluno.
*/
define(['aluno/dados-pessoais/movimentacao-academica/movimentacao-academica.module'], function () {

    'use strict';

    angular
        .module('eduMovimentacaoAcademicaModule')
        .factory('eduMovimentacaoAcademicaFactory', EduMovimentacaoAcademicaFactory);

    EduMovimentacaoAcademicaFactory.$inject = ['$totvsresource'];

    function EduMovimentacaoAcademicaFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.getMovimentacaoAcademica = getMovimentacaoAcademica;
        factory.getDadosEnade = getDadosEnade;
        factory.getInfoCurso = getInfoCurso;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function getMovimentacaoAcademica(callback) {
            var parameters = {
                method: 'MovimentacaoAcademica'
            };

            return factory.TOTVSQuery(parameters, callback);
        }

        function getDadosEnade(callback) {
            var parameters = {
                method: 'DadosEnade'
            };

            return factory.TOTVSQuery(parameters, callback);
        }

        function getInfoCurso(callback) {
            var parameters = {
                method: 'InfoCurso'
            };

            return factory.TOTVSGet(parameters, callback);
        }
    }
});
