[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Notas",
			"en": "Grades",
			"es": "Notas"
        },
        "l-codigo": {
			"pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Subject",
			"es": "Materia"
        },
        "l-etapa": {
            "pt": "Etapa",
			"en": "Stage",
			"es": "Etapa"
        },
        "l-data-avaliacao": {
            "pt": "Data da avaliação",
			"en": "Test date",
			"es": "Fecha de la evaluación"
        },
        "l-avaliacao": {
            "pt": "Avaliação",
			"en": "Test",
			"es": "Evaluación"
        },
        "l-nota": {
            "pt": "Nota",
			"en": "Grade",
			"es": "Nota"
        },
        "l-valor": {
            "pt": "Valor",
			"en": "Value",
			"es": "Valor"
        },
        "l-data-devolucao": {
            "pt": "Data devolução",
			"en": "Return date",
			"es": "Fecha devolución"
        },
        "l-ver-disciplina": {
            "pt": "ver disciplina",
			"en": "see subject",
			"es": "ver materia"
        },
        "l-notas-avaliacao": {
            "pt": "Notas de avaliação",
			"en": "Test grades",
			"es": "Notas de evaluación"
        },
        "l-msg-nenhum-registro": {
            "pt": "Não existem notas disponíveis para a etapa selecionada",
			"en": "No grade available for the selected stage",
			"es": "No existen notas disponibles para la etapa seleccionada"
        },
        "l-codigo-disciplina":{
            "pt": "Cód. Disciplina",
			"en": "Subject code",
			"es": "Cód. Materia"
        },
        "l-situacao": {
            "pt": "Situação",
			"en": "Status",
			"es": "Situación"
        },
        "l-codigo-turma": {
            "pt": "Turma",
			"en": "Class",
			"es": "Grupo"
        },
        "l-filial": {
            "pt": "Filial",
			"en": "Branch",
			"es": "Suursal"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student board",
		  "es": "Panel del alumno"
        }
    }
]
