/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduFaltasModule
 * @name eduFaltasController
 * @object controller
 *
 * @created 2016-09-27 v12.1.15
 * @updated
 *
 * @requires faltas.module
 *
 * @dependencies
 *
 * @description Controller de Faltas
 */
define(['aluno/faltas/faltas.module',
        'aluno/faltas/faltas.route',
        'aluno/faltas/faltas.factory',
        'aluno/faltas/faltas.service',
        'utils/edu-utils.factory',
        'utils/reports/edu-relatorio.service',
        'widgets/widget.constants',
        'utils/edu-constantes-globais.constants'
    ],
    function () {

        'use strict';

        angular
            .module('eduFaltasModule')
            .controller('eduFaltasController', EduFaltasController);

        EduFaltasController.$inject = [
            '$scope',
            '$filter',
            'eduFaltasFactory',
            'eduFaltasService',
            '$rootScope',
            'eduUtilsFactory',
            'eduRelatorioService',
            'eduWidgetsConsts',
            'eduConstantesGlobaisConsts',
            '$state',
            'totvs.app-notification.Service',
            'i18nFilter'
        ];

    function EduFaltasController($scope, $filter, eduFaltasFactory, eduFaltasService, $rootScope, eduUtilsFactory, eduRelatorioService, eduWidgetsConsts, eduConstantesGlobaisConsts, $state, totvsNotification, i18nFilter) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this,
        codRelatorioImpressao = null,
        codColRelatorioImpressao = null;

        self.idFuncionalidade = eduWidgetsConsts.EduWidgetsFuncionalidade.Faltas;
        self.activeBtn;
        self.dataRange = [];
        self.disciplina;
        self.todasFaltasPorAula;
        self.faltasPorAula;
        self.activeTabFaltasPorEtapa = true;
        self.activeTabFaltasPorAula = false;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        //TODO: alterar
        self.widgetUrl01 = 'js/aluno/widgets/widget-pie.view.html';
        self.widgetUrl02 = 'js/aluno/widgets/widget-pie.view.html';

        self.retornaMaisDetalhesFalta = retornaMaisDetalhesFalta;
        self.exibirMaisDetalhesFalta = exibirMaisDetalhesFalta;
        self.preencheGridFaltasPorAula = preencheGridFaltasPorAula;
        self.filtrarPorData = filtrarPorData;
        self.disciplinaAlterada = disciplinaAlterada;
        self.onChangeDisciplina = onChangeDisciplina;
        self.periodoLetivoAlterado = periodoLetivoAlterado;
        self.exibeTab = exibeTab;
        self.naoPodeFiltrar = naoPodeFiltrar;
        self.selecionarTabFaltasPorAula = selecionarTabFaltasPorAula;
        self.refresh = false;
        self.permiteImprimir = false;
        self.imprimir = imprimir;

        /* Só executa o método init após carregar o objeto com as permissões do usuário */
        var myWatch = $rootScope.$watch('objPermissions', function (data) {
            if (data !== null) {
                init();
                myWatch();
            }
        });

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        function init() {
            preencheGridFaltasPorAula(30); // inicia com -30 dias
            carregarParametrosEdu();
        }

        /* Valida se o usuário tem permissão no Menu */
        var permissionsWatch = $rootScope.$watch('objPermissions', function () {
            if ($rootScope.objPermissions) {
                if (!$rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_FALTAS) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao'),
                        detail: i18nFilter('l-usuario-sem-permissao')
                    });
                    $state.go(eduConstantesGlobaisConsts.EduStateViewPadrao);
                }

                //destroy watch
                permissionsWatch();
            }
        });

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function preencheGridFaltasPorAula(subDias) {
            self.activeBtn = subDias;

            var dateNow = new Date();
            var tempDate = new Date(dateNow);
            tempDate.setDate(tempDate.getDate() - subDias);
            var dataDe = new Date(tempDate);

            var parameters = [];
            parameters['tipoFrequencia'] = 'A'; // ausência
            parameters['dataDe'] = dataDe;
            parameters['dataAte'] = dateNow;

            carregaFaltasPorAula(parameters);
        }

        function carregarParametrosEdu (){
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function(parametros){
                codRelatorioImpressao = parametros.CodRelatorioConsultaFaltaReports;
                codColRelatorioImpressao = parametros.CodColRelatorioConsultaFaltaReports;
                self.permiteImprimir = !!codRelatorioImpressao;
            });
        }

        function periodoLetivoAlterado() {
            $scope.$broadcast('alteraPeriodoLetivoEvent');
        }

        /* Assina o evento de alteração de período */
        $scope.$on('alteraPeriodoLetivoEvent', function () {
            self.refresh = true;
            preencheGridFaltasPorAula(30); // inicia com -30 dias
        });

        function retornaMaisDetalhesFalta(item) {
            return '<a href="javascript:void(0)" id="' + item.AULA + '" class="" ' +
                   'ng-click="controller.exibirMaisDetalhesFalta(' + item.AULA + ')" >{{::"l-mais-detalhes" | i18n : [] : "js/aluno/faltas" }}</a>';
        }

        function exibirMaisDetalhesFalta(idAula) {
            var faltaSelecionada = $filter('filter')(this.faltasPorAula, {
                AULA: idAula
            }, true);

            this.falta = faltaSelecionada;

            $('#mFaltasInfo').modal('show');
        }

        function filtrarPorData(datasDeAte) {
            self.activeBtn = '';

            var dataDe = new Date(datasDeAte.startDate);
            var dataAte = new Date(datasDeAte.endDate);

            var parameters = [];
            parameters['tipoFrequencia'] = 'A'; // ausência
            parameters['dataDe'] = dataDe;
            parameters['dataAte'] = dataAte;

            carregaFaltasPorAula(parameters);
        }

        function carregaFaltasPorAula(parameters) {
            eduFaltasFactory.FaltasPorAulaAluno(parameters, function (result) {
                self.faltasPorAula = result.SFREQUENCIAALUNO;
                self.todasFaltasPorAula = result.SFREQUENCIAALUNO;
            });
        }

        function onChangeDisciplina(obj) {
            disciplinaAlterada(obj);
        }

        function disciplinaAlterada(obj) {
            if (obj) {
                self.faltasPorAula = $filter('filter')(self.todasFaltasPorAula, {
                    'IDTURMADISC': obj
                }, true);
            } else {
                self.faltasPorAula = self.todasFaltasPorAula;
            }
        }

        function naoPodeFiltrar() {
            if (self.dataRange.startDate == null || self.dataRange.endDate == null) {
                return true;
            }
            return false;
        }

        /**
         * Valida a permissão para cada Tab
         *
         * @param {string} tabName - Nome da tab
         * @returns True or False
         */
        function exibeTab(tabName) {
            return eduFaltasService.exibeTab(tabName);
        }

        function selecionarTabFaltasPorAula() {
            self.activeTabFaltasPorAula = true;
        }

        /* Executa emissão do relatório associado a visão(Parametrizado no sistema). */
        function imprimir() {
            if (codRelatorioImpressao) {
                eduRelatorioService.emitirRelatorio(codRelatorioImpressao, codColRelatorioImpressao);
            }
        }

    }
});
