/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module EduFaltasModule
 * @name EduFaltasFactory
 * @object factory
 *
 * @created 2016-09-26 v12.1.15
 * @updated
 *
 * @requires EduFaltasModule
 *
 * @dependencies EduFaltasFactory
 *
 * @description Factory utilizada nas faltas.
 */

define(['aluno/faltas/faltas.module'], function () {

    'use strict';

    angular
        .module('eduFaltasModule')
        .factory('eduFaltasFactory', EduFaltasFactory);

    EduFaltasFactory.$inject = ['$totvsresource'];

    function EduFaltasFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.FaltasPorAulaAluno = FaltasPorAulaAluno;
        factory.FaltasPorAulaAlunoTurmaDisc = FaltasPorAulaAlunoTurmaDisc;
        factory.FaltasPorEtapaAluno = FaltasPorEtapaAluno;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function FaltasPorAulaAluno(parameters, callback) {
            parameters.method = 'Aluno/Falta/Aula';
            factory.TOTVSGet(parameters, callback);
        }

        function FaltasPorAulaAlunoTurmaDisc(parameters, callback) {
            parameters.method = 'Aluno/Falta/AulaTurmaDisc';
            factory.TOTVSGet(parameters, callback);
        }

        function FaltasPorEtapaAluno(parameters, callback) {

            if (parameters['idTurmaDisc'] && parameters['idTurmaDisc'] < 0) {
                parameters.idTurmaDisc = null;
            }

            parameters.method = 'FaltaEtapa';
            factory.TOTVSGet(parameters, callback);
        }
    }
});
