/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduFaltasModule
 * @name eduFaltasAulaListControlller
 * @object controller
 *
 * @created 2016-09-27 v12.1.15
 * @updated
 *
 * @requires faltas.module
 *
 * @dependencies
 *
 * @description Controller das faltas por aula
 */
define(['aluno/faltas/faltas.module',
    'aluno/faltas/faltas.route',
    'aluno/faltas/faltas.factory',
    'utils/edu-utils.service',
    'utils/edu-enums.constants',
    'aluno/aulas/aulas.factory'
],
    function () {

        'use strict';

        angular
            .module('eduFaltasModule')
            .controller('eduFaltasAulaListControlller', EduFaltasAulaListControlller);

        EduFaltasAulaListControlller.$inject = [
            '$scope',
            '$filter',
            'eduFaltasFactory',
            'eduUtilsService',
            'TotvsDesktopContextoCursoFactory',
            'MatriculaDisciplinaService',
            '$state',
            '$rootScope',
            'eduEnumsConsts',
            'eduAulasFactory'
        ];

        /**
         * Controller Faltas por Aula.
         *
         * @param {object} $scope - Objeto do scope
         * @param {object} $filter - Objeto Filter
         * @param {object} eduFaltasFactory - Objeto Factory de Faltas
         * @param {object} eduUtilsService - Objeto de serviço de faltas
         * @param {object} TotvsDesktopContextoCursoFactory - Contexto factory
         * @param {object} MatriculaDisciplinaService - Matrícula Service
         * @param {object} $state - Objeto state
         * @param {object} $rootScope - RootScope
         * @param {object} eduEnumsConsts - Objeto de enumerados
         * @param {object} eduAulasFactory - Objeto factory de aulas
         */
        function EduFaltasAulaListControlller($scope, $filter, eduFaltasFactory, eduUtilsService,
            TotvsDesktopContextoCursoFactory, MatriculaDisciplinaService, $state, $rootScope, eduEnumsConsts,
            eduAulasFactory) {

            // *********************************************************************************
            // *** Variables
            // *********************************************************************************

            var self = this;
            self.activeBtn = {};
            self.dataRange = [];
            self.disciplina = -1;
            self.todasFaltasPorAula = [];
            self.faltasPorAula = [];
            self.permiteFiltrarEtapa = true;
            self.habilitaFiltroEtapa = true;
            self.exibeColDisciplina = true;
            self.exibeColAula = false;
            self.indiceColDisciplina = 1;
            self.indiceColAula = 2;
            self.tipoEtapa = eduEnumsConsts.TipoEtapa.Falta;
            self.gridOptions = null;
            self.ehTelaDisciplina = false;

            // *********************************************************************************
            // *** Public Properties and Methods
            // *********************************************************************************

            self.retornaMaisDetalhesFalta = retornaMaisDetalhesFalta;
            self.exibirMaisDetalhesFalta = exibirMaisDetalhesFalta;
            self.preencheGridFaltasPorAula = preencheGridFaltasPorAula;
            self.filtrarPorData = filtrarPorData;
            self.disciplinaAlterada = disciplinaAlterada;
            self.onChangeDisciplina = onChangeDisciplina;
            self.periodoLetivoAlterado = periodoLetivoAlterado;
            self.naoPodeFiltrar = naoPodeFiltrar;
            self.formatarColunaDisciplina = formatarColunaDisciplina;
            self.abrirDetalhesDisciplina = abrirDetalhesDisciplina;
            self.formatarColunaAula = formatarColunaAula;
            self.onData = onData;
            self.onChangeEtapa = onChangeEtapa;
            self.formatarColunaData = formatarColunaData;
            self.exibirResumoAula = exibirResumoAula;
            self.refresh = false;

            $rootScope.$on('selecionaDisciplina', function (event, data, dias) {
                self.disciplina = data;
                preencheGridFaltasPorAula(dias);
            });

            init();

            // *********************************************************************************
            // *** Controller Initialize
            // *********************************************************************************

            /**
             * Inicializa dados da tela.
             */
            function init() {

                // configura a grid
                self.gridOptions = {
                    columns: definirColunas(),
                    dataSource: {
                        data: self.faltasPorAula
                    },
                    groupable: {
                        messages: {
                            empty: $filter('i18n')('grid-agrupamento-vazio')
                        }
                    }
                };

                // se recebeu o idTurmaDisc, exibe os dados apenas desta disciplina
                if ($state.params.idTurmaDisc && $state.params.idTurmaDisc !== -1) {
                    selecionarDisciplina($state.params.idTurmaDisc);
                    self.ehTelaDisciplina = true;
                }

                preencheGridFaltasPorAula(30); // inicia com -30 dias
            }

            /**
             * Define as colunas apresentadas no grid.
             *
             * @returns {object} Objeto com a definição das colunas.
             */
            function definirColunas() {

                var colunasDefinicao = [{
                    field: 'DATAFALTA',
                    filter: 'date : shortDate',
                    template: formatarColunaData,
                    attributes: {
                        'class': 'gridRow textAlignCenter'
                    },
                    title: obterNomeCampos('l-gridcol-data'),
                    groupHeaderTemplate: function (item) {

                        return obterNomeCampos('l-gridcol-data') + ': ' +  $filter('date')(item.value, 'shortDate');
                    }
                }, {
                    field: 'DISCIPLINA',
                    template: formatarColunaDisciplina,
                    attributes: {
                        'class': 'gridRow'
                    },
                    width: '300px',
                    title: obterNomeCampos('l-gridcol-disciplina'),
                    groupHeaderTemplate: obterNomeCampos('l-gridcol-disciplina') + ': #= value#'
                }, {
                    field: 'AULA',
                    template: formatarColunaAula,
                    attributes: {
                        'class': 'gridRow'
                    },
                    title: obterNomeCampos('l-gridcol-aula'),
                    groupHeaderTemplate: obterNomeCampos('l-gridcol-aula') + ': #= value#'
                }, {
                    field: 'HORAINICIAL',
                    attributes: {
                        'class': 'gridRow textAlignCenter'
                    },
                    width: '60px',
                    title: obterNomeCampos('l-gridcol-inicio'),
                    groupHeaderTemplate: obterNomeCampos('l-gridcol-inicio') + ': #= value#'
                }, {
                    field: 'HORAFINAL',
                    attributes: {
                        'class': 'gridRow textAlignCenter'
                    },
                    width: '60px',
                    title: obterNomeCampos('l-gridcol-fim'),
                    groupHeaderTemplate: obterNomeCampos('l-gridcol-fim') + ': #= value#'
                }, {
                    field: 'SITUACAO',
                    attributes: {
                        'class': 'gridRow'
                    },
                    title: obterNomeCampos('l-gridcol-situacao'),
                    groupHeaderTemplate: obterNomeCampos('l-gridcol-situacao') + ': #= value#'
                }, {
                    template: retornaMaisDetalhesFalta,
                    attributes: {
                        'class': 'gridRow textAlignCenter'
                    },
                    groupable: false
                }];

                return colunasDefinicao;
            }

            function formatarCabecalhoData(item) {
                return $filter('i18n')('l-gridcol-data', [], 'js/aluno/faltas') +
                    ': ' + $filter('date')(item.value, 'dd/MM/yyyy');
            }

            /**
             * Retorna o nome amigável da coluna.
             *
             * @param {string} nomeCampo - nome que identifica o nome do campo no arquivo de tradução
             * @returns
             */
            function obterNomeCampos(nomeCampo) {
                return $filter('i18n')(nomeCampo, [], 'js/aluno/faltas');
            }

            // *********************************************************************************
            // *** Functions
            // *********************************************************************************

            /**
             * Exibe os dados apenas de uma disciplina
             *
             * @param {int} idTurmaDisc - Identificador da Turma/Disciplina
             */
            function selecionarDisciplina(idTurmaDisc) {
                self.disciplina = idTurmaDisc;
                self.permiteFiltrarDisciplina = false;
                self.exibeColDisciplina = false;
                self.exibeColAula = true;
            }

            /**
             * Preenche grid de faltas por aula
             *
             * @param {int} subDias - Número de dias
             */
            function preencheGridFaltasPorAula(subDias) {

                // limpa os inputs do data range
                self.dataRange = [];
                self.activeBtn = subDias;

                // limpa o filtro por etapas
                self.codEtapa = -1;

                var dateNow = new Date(),
                    tempDate = new Date(dateNow);

                tempDate.setDate(tempDate.getDate() - subDias);

                var dataDe = new Date(tempDate),
                    parameters = [];

                parameters['tipoFrequencia'] = 'A'; // ausência
                parameters['dataDe'] = dataDe;
                parameters['dataAte'] = dateNow;

                carregaFaltasPorAula(parameters);
            }

            /**
             * Evento disparado ao alterar o período letivo
             */
            function periodoLetivoAlterado() {
                $scope.$broadcast('alteraPeriodoLetivoEvent');
            }

            // Assina o evento de alteração de período
            $scope.$on('alteraPeriodoLetivoEvent', function () {
                self.refresh = true;
                preencheGridFaltasPorAula(30); // inicia com -30 dias
            });

            /**
             * Retorna os detalhes de falta.
             *
             * @param {object} item - Objeto de falta.
             * @returns Link com os detalhes
             */
            function retornaMaisDetalhesFalta(item) {
                return '<a href="javascript:void(0)" id="' + item.AULA + '" class="" ' +
                    'ng-click="controller.exibirMaisDetalhesFalta(' + item.AULA + ')" >{{::"l-detalhes" | i18n : [] : "js/aluno/faltas" }}</a>';
            }

            /**
             *  Exite mais detalhes da falta.
             *
             * @param {int} idAula - Idenficador da Aula
             */
            function exibirMaisDetalhesFalta(idAula) {

                var faltaSelecionada = $filter('filter')(self.faltasPorAula, {
                    AULA: idAula
                }, true);

                self.falta = faltaSelecionada;

                $('#mFaltasInfo').modal('show');
            }

            /**
             * Filtra por Data
             *
             * @param {date} datasDeAte - Data limite
             */
            function filtrarPorData(datasDeAte) {

                self.activeBtn = '';

                if (self.etapa) {
                    // limpa o filtro por etapas
                    self.codEtapa = -1;
                    self.ignorarFiltroEtapas = true;
                }

                var startDate = new Date(datasDeAte.startDate),
                    endDate = new Date(datasDeAte.endDate);

                var dataDe = new Date(startDate.getUTCFullYear(),
                    startDate.getUTCMonth(),
                    startDate.getUTCDate(), 12),
                    dataAte = new Date(endDate.getUTCFullYear(),
                        endDate.getUTCMonth(),
                        endDate.getUTCDate(), 12),

                    parameters = [];
                parameters['tipoFrequencia'] = 'A'; // ausência
                parameters['dataDe'] = dataDe;
                parameters['dataAte'] = dataAte;

                carregaFaltasPorAula(parameters);
            }

            /**
             * Carrega faltas por aula
             *
             * @param {object} parameters - Parâmetros {@tipoFrequencia | @dataDe | @dataAte}
             */
            function carregaFaltasPorAula(parameters) {

                if (self.ehTelaDisciplina) {

                    parameters.idTurmaDisc = self.disciplina;
                    eduFaltasFactory.FaltasPorAulaAlunoTurmaDisc(parameters, function (result) {
                        preencherObjetos(result);
                    });
                }
                else {
                    eduFaltasFactory.FaltasPorAulaAluno(parameters, function (result) {
                        preencherObjetos(result);
                    });
                }
            }

            /**
             * Preenche os dados de faltas
             *
             * @param {any} result
             */
            function preencherObjetos(result) {
                self.faltasPorAula = result.SFREQUENCIAALUNO;
                self.todasFaltasPorAula = result.SFREQUENCIAALUNO;

                if (self.disciplina > 0) {
                    disciplinaAlterada(self.disciplina);
                }
            }

            /**
             * Evento disparado ao alterar a disciplina
             *
             * @param {int} idTurmaDisc - Identficador Turma/Disciplina
             */
            function onChangeDisciplina(idTurmaDisc) {

                disciplinaAlterada(idTurmaDisc);

                if (self.ehTelaDisciplina && self.permiteFiltrarEtapa) {
                    selecionarDisciplina(idTurmaDisc);
                }

            }

            /**
             * Altera a disciplina
             *
             * @param {int} idTurmaDisc - identificador Turma/Disciplina
             */
            function disciplinaAlterada(idTurmaDisc) {
                if (idTurmaDisc > 0) {
                    self.faltasPorAula = $filter('filter')(self.todasFaltasPorAula, {
                        'IDTURMADISC': idTurmaDisc
                    }, true);
                } else {
                    self.faltasPorAula = self.todasFaltasPorAula;
                }
            }

            /**
             * Verifica se pode realizar o filtro
             *
             * @returns {bool} Retorna verdadeiro/falso
             */
            function naoPodeFiltrar() {

                var nPodeFiltrar = false;

                if (self.dataRange.startDate == null || self.dataRange.endDate == null) {
                    nPodeFiltrar = true;
                }
                return nPodeFiltrar;
            }

            /**
             * Formata a coluna disciplina
             *
             * @param {objeto} item - Objeto falta
             * @returns {string} Coluna formatada
             */
            function formatarColunaDisciplina(item) {

                if (item.DISCIPLINA == null) {
                    return '';
                }

                return '<a href="javascript:void(0)" class="" ' +
                    'ng-click="controller.abrirDetalhesDisciplina(' + item.IDTURMADISC + ')" >' +
                    eduUtilsService.escapeHtml(item.DISCIPLINA) + '</a>';
            }

            /**
             * Formata a coluna aula
             *
             * @param {objeto} item - Objeto falta
             * @returns {string} Coluna formatada
             */
            function formatarColunaAula(item) {
                return '{{::"l-aula" | i18n : [] : "js/aluno/faltas" }} ' + item.AULA;
            }

            /**
             * Formata a coluna data
             *
             * @param {objeto} item - Objeto falta
             * @returns {string} Coluna formatada
             */
            function formatarColunaData(item) {

                if (item.DATAFALTA == null) {
                    return '';
                }

                var ngClick = 'controller.exibirResumoAula(' + item.IDTURMADISC + ',' + item.IDPLANOAULA + ',\'' + item.CODDISC + '\')';

                return eduAulasFactory.retornarLinkAula(item.IDPLANOAULA, item.DATAFALTA, ngClick);

            }

            /**
             * Exibe o resumo da aula
             *
             * @param {int} idTurmaDisc - Identficador Turma/Disciplina
             * @param {int} idPlanoAula - Idenficador Plando de Aula
             * @param {string} codDisc - Código da disciplina
             */
            function exibirResumoAula(idTurmaDisc, idPlanoAula, codDisc) {

                eduAulasFactory.exibirResumoAula(idTurmaDisc, idPlanoAula, codDisc);

            }

            /**
             * Evento disparado ao quando está ocorrendo a vinculação dos dados a partir da fonte de dados.
             *
             */
            function onDataBound() {

                var grid = self.myGrid;

                if (grid) {

                    for (var i = 0; i < grid.columns.length; i++) {
                        grid.showColumn(i);
                    }
                    // Percorremos as colunas que estão agrupadas, que são identificadas pela
                    // class k-group-indicator, dessa forma aplicamos o hide.
                    $('div.k-group-indicator').each(function (i, v) {
                        var fieldName = $(v).data('field').toString().replace(/'/g, '\"');
                        grid.hideColumn(fieldName);
                    });

                    if (!self.exibeColDisciplina) {
                        grid.hideColumn(self.indiceColDisciplina);
                    }

                    if (!self.exibeColAula) {
                        grid.hideColumn(self.indiceColAula);
                    }

                }
            }

            /**
             * Evento disparado ao realizar o carregamento dos dados.
             *
             */
            function onData() {
                var grid = self.myGrid;
                if (grid) {
                    grid.bind('dataBound', onDataBound);
                }
            }

            /**
             * Abri detalhes da disciplina
             *
             * @param {int} idTurmaDisc - Idenficador Turma/Disciplina
             */
            function abrirDetalhesDisciplina(idTurmaDisc) {

                var contexto, codColigada, ra;

                contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
                codColigada = contexto.cursoSelecionado.CODCOLIGADA;
                ra = contexto.cursoSelecionado.RA;

                MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(codColigada, idTurmaDisc, ra);

            }

            /**
             * filtra as avaliações de acordo com a etapa selecionada
             *
             * @param {string} descEtapa - Descrição da etapa
             * @param {object} objEtapa - Objeto da etapa
             * @returns {return} Objeto da etapa.
             */
            function onChangeEtapa(descEtapa, objEtapa) {

                if (self.ignorarFiltroEtapas) {
                    self.ignorarFiltroEtapas = false;
                    return;
                }

                if (objEtapa && objEtapa.CODETAPA) {
                    if (objEtapa.CODETAPA === -1) {
                        if (self.dataRange.length === 0) {
                            self.dataRange = [];
                            if (!self.activeBtn) {
                                preencheGridFaltasPorAula(30); // inicia com -30 dias
                            }
                        }
                    } else {
                        self.etapa = objEtapa;
                        self.activeBtn = '';

                        if (objEtapa.DTINICIO && objEtapa.DTFIM) {
                            self.dataRange.startDate = new Date(objEtapa.DTINICIO);
                            self.dataRange.endDate = new Date(objEtapa.DTFIM);

                            var parameters = [];
                            parameters['tipoFrequencia'] = 'A'; // ausência
                            parameters['dataDe'] = self.dataRange.startDate;
                            parameters['dataAte'] = self.dataRange.endDate;

                            carregaFaltasPorAula(parameters);
                        } else {
                            self.dataRange = [];
                            if (!self.activeBtn) {
                                preencheGridFaltasPorAula(30); // inicia com -30 dias
                                self.codEtapa = objEtapa.CODETAPA;
                            }
                        }
                    }

                }

            }
      
            // *********************************************************************************
            // *** Objetos
            // *********************************************************************************

        }
    });
