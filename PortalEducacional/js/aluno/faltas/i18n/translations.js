[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Faltas",
			"en": "Absences",
			"es": "Faltas"
        },
        "l-faltas-etapa": {
            "pt": "Faltas por etapa",
			"en": "Absences by stage",
			"es": "Faltas por etapa"
        },
        "l-faltas-aula": {
            "pt": "Faltas por aula",
			"en": "Absences by class",
			"es": "Faltas por clase"
        },
        "l-gridcol-data": {
            "pt": "Data da Aula",
			"en": "Class Date",
			"es": "Fecha de la clase"
        },
        "l-gridcol-disciplina": {
            "pt": "Disciplina",
			"en": "Course Subject",
			"es": "Materia"
        },
        "l-gridcol-inicio": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-gridcol-fim": {
            "pt": "Fim",
			"en": "End",
			"es": "Final"
        },
        "l-gridcol-situacao": {
            "pt": "Situação da falta",
			"en": "Status of absence",
			"es": "Situación de la falta"
        },
        "l-detalhes": {
            "pt": "detalhes",
			"en": "details",
			"es": "detalles"
        },
        "l-aula": {
            "pt": "Aula",
			"en": "Class",
			"es": "Clase"
        },
        "l-data": {
            "pt": "Data",
			"en": "Date",
			"es": "Fecha"
        },
        "l-horario": {
            "pt": "Horário",
			"en": "Schedule",
			"es": "Horario"
        },
        "l-situacaofaltas": {
            "pt": "Sit. Falta",
			"en": "Absence Status",
			"es": "Sit. Falta"
        },
        "l-situacao-falta": {
            "pt": "Situação da Falta",
			"en": "Absence Status",
			"es": "Situación de la falta"
        },
        "l-justificativa": {
            "pt": "Justificativa",
			"en": "Justification",
			"es": "Justificación"
        },
        "l-situacao": {
            "pt": "Situação",
			"en": "Status",
			"es": "Situación"
        },
        "l-detalhes-falta": {
            "pt": "Detalhes da falta",
			"en": "Details of absence",
			"es": "Detalles de la falta"
        },
        "l-btn-fechar": {
            "pt": "Fechar",
			"en": "Close",
			"es": "Cerrar"
        },
        "l-btn-15": {
            "pt": "15",
			"en": "15",
			"es": "15"
        },
        "l-btn-30": {
            "pt": "30",
			"en": "30",
			"es": "30"
        },
        "l-btn-90": {
            "pt": "90",
			"en": "90",
			"es": "90"
        },
        "l-btn-180": {
            "pt": "180",
			"en": "180",
			"es": "180"
        },
        "l-btn-360": {
            "pt": "360",
			"en": "360",
			"es": "360"
        },
        "l-btn-dias": {
            "pt": "dias anteriores",
			"en": "previous days",
			"es": "días anteriores"
        },
        "l-lbl-filtro": {
            "pt": "Filtro por dias anteriores ou intervalo de datas:",
			"en": "Filter by previous days or interval of dates:",
			"es": "Filtro por días anteriores o intervalo de fechas:"
        },
        "l-date-range-placeholder": {
            "pt": "dia/mês/ano",
			"en": "day/month/year",
			"es": "día/mes/año"
        },
        "l-btn-filtrar": {
            "pt": "Filtrar",
			"en": "Filter",
			"es": "Filtrar"
        },
        "l-todas-disciplinas": {
            "pt": "Todas disciplinas",
			"en": "All course subjects",
			"es": "Todas materias"
        },
        "l-disciplinas": {
            "pt": "Disciplinas",
			"en": "Course Subjects",
			"es": "Materias"
        },
        "l-turma": {
            "pt": "Turma",
			"en": "Class",
			"es": "Grupo"
        },
        "l-codigo-disciplina": {
            "pt": "Código",
			"en": "Code",
			"es": "Código"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Course Subject",
			"es": "Materia"
        },
        "l-percentual": {
            "pt": "Percentual",
			"en": "Percentage",
			"es": "Porcentaje"
        },
        "l-msg-nenhum-registro": {
            "pt": "Nenhum registro de falta foi localizado!",
			"en": "No record of absence was found!",
			"es": "¡No se encontró ningún registro de falta!"
        },
        "l-acimaLimite": {
            "pt": "Faltas acima do limite",
			"en": "Absences above the limit",
			"es": "Faltas superiores al límite"
        },
        "l-dentroLimite": {
            "pt": "Faltas dentro do limite",
			"en": "Absences within the limit",
			"es": "Faltas dentro del límite"
        },
        "l-proximoLimite": {
            "pt": "Faltas próximas do limite",
			"en": "Absences next to the limit",
			"es": "Faltas próximas del límite"
        },
        "l-mais-detalhes": {
            "pt": "detalhes",
			"en": "details",
			"es": "detalles"
        },
        "l-gridcol-aula": {
            "pt": "Aula",
			"en": "Class",
			"es": "Clase"
        },
        "l-faltas-disciplina": {
            "pt": "Etapas",
			"en": "Stages",
			"es": "Etapas"
        },
        "l-date-format": {
            "pt": "dd/MM/yyyy",
			"en": "dd/MM/yyyy",
			"es": "dd/MM/aaaa"
        },
        "l-culture": {
            "pt": "pt",
			"en": "en",
			"es": "es"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student's Panel",
		  "es": "Panel del alumno"
        }
    }
]
