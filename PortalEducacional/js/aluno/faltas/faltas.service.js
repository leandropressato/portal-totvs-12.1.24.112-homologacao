/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module EduFaltasModule
 * @name EduFaltasService
 * @object service
 *
 * @created 2016-09-21 v12.1.15
 * @updated
 *
 * @requires $modal
 *
 * @description Service utilizada para exibir as notas de avaliações da disciplina
 */

define(['aluno/faltas/faltas.module'], function () {

    'use strict';

    angular
        .module('eduFaltasModule')
        .service('eduFaltasService', EduFaltasService);

    EduFaltasService.$inject = ['$modal', '$rootScope'];

    function EduFaltasService($modal, $rootScope) {

        var self = this;
        self.exibeTab = exibeTab;

        function EduFaltasService($modal) {

            this.exibirFaltas = exibirFaltas;

            function exibirFaltas(idTurmaDisc) {

                var params = {
                    idTurmaDisc: idTurmaDisc
                };

                $modal.open({
                    templateUrl: EDU_CONST_GLOBAL_URL_BASE_APP + '/js/aluno/faltas/faltas-detalhes.view.html',
                    controller: 'eduFaltasDetalhesController',
                    controllerAs: 'controller',
                    size: 'lg',
                    resolve: {
                        parametros: function () {
                            return params;
                        }
                    },
                    backdrop: 'true'
                });
            }

        }

        /**
         * Valida a permissão para cada Tab
         *
         * @param {string} tabName - Nome da tab
         * @returns True or False
         */
        function exibeTab(tabName) {
            var exibe = false;

            if ($rootScope.objPermissions === null) {
                return exibe;
            }

            switch (tabName) {
                case 'faltas-etapas':
                    exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_NOTAS_NOTASETAPA);
                    break;
                case 'faltas-aulas':
                    exibe = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_CENTRALALUNO_FALTAS_FALTASPORAULA);
                    break;
            }
            return exibe;
        }
    }
});
