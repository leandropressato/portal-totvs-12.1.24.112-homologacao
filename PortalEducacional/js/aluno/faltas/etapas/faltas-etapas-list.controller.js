/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module faltasModule
 * @name faltasEtapasListaController
 * @object directive
 *
 * @created 2016-09-16 v12.1.15
 * @updated
 *
 * @requires faltas.module
 *
 * @dependencies  faltasFactory
 *
 * @description Controller da lista de faltas
 */
define(['aluno/faltas/faltas.module',
    'aluno/faltas/faltas.factory',
    'aluno/faltas/faltas.service',
    'utils/edu-utils.factory',
    'utils/edu-utils.service'
], function () {

    'use strict';

    angular.module('eduFaltasModule')
        .controller('eduFaltasEtapasListControlller', EduFaltasEtapasListControlller);

    EduFaltasEtapasListControlller.$inject = [
        '$filter',
        'i18nFilter',
        'TotvsDesktopContextoCursoFactory',
        'eduFaltasFactory',
        '$compile',
        '$scope',
        '$state',
        'MatriculaDisciplinaService',
        'eduFaltasService',
        'eduUtilsFactory',
        'eduUtilsService'
    ];

    function EduFaltasEtapasListControlller(
        $filter,
        i18nFilter,
        TotvsDesktopContextoCursoFactory,
        eduFaltasFactory,
        $compile,
        $scope,
        $state,
        MatriculaDisciplinaService,
        eduFaltasService,
        eduUtilsFactory,
        eduUtilsService
    ) {

        var self = this,
            defColunas = [];
        self.faltas = [];
        self.gridOptions = [];
        self.abrirDetalhesDisciplina = abrirDetalhesDisciplina;
        self.abrirVisualizacaoFaltasPorAula = abrirVisualizacaoFaltasPorAula;
        self.formatarColunaSituacaoFalta = formatarColunaSituacaoFalta;
        self.buscafaltas = buscafaltas;
        self.onData = onData,
        self.disciplina = null;
        self.exibeFaltaPorAula = false;
        self.refresh = false;

        // Assina o evento de alteração de período
        $scope.$on('alteraPeriodoLetivoEvent', buscafaltas);

        init();

        // Inicializa
        function init() {
          self.exibeFaltaPorAula = eduFaltasService.exibeTab('faltas-aulas');
          carregarParametrosEdu();
        }

        /**
         * Evento disparado ao quando está ocorrendo a vinculação dos dados a partir da fonte de dados.
         *
         * @param {any} event - atributos do evento
         */
        function onDataBound(event) {

            var grid = self.myGrid;
            if (grid) {
                for (var i = 0; i < grid.columns.length; i++) {
                    grid.showColumn(i);
                }
                // Percorremos as colunas que estão agrupadas, que são identificadas pela
                // class k-group-indicator, dessa forma aplicamos o hide.
                $('div.k-group-indicator').each(function (i, v) {
                    var fieldName = $(v).data('field').toString().replace(/'/g, "\"");
                    grid.hideColumn(fieldName);
                });
            }
        }

        /**
         * Evento disparado ao realizar o carregamento dos dados.
         *
         * @param {any} data - atributos do evento.
         */
        function onData(data) {
            var grid = self.myGrid;
            if (grid) {
                grid.bind('dataBound', onDataBound);
            }
        }

        /**
         * Realiza a busca pelos dados de falta do aluno no contexto selecionado
         */
        function buscafaltas() {

            self.refresh = true;
            
            eduFaltasFactory.FaltasPorEtapaAluno({idTurmaDisc : self.disciplina},
                function (result) {
                    self.faltas = [];
                    result['FaltasEtapa'].forEach(function (valor) {
                        valor.verFaltas = 'Ver faltas';
                        self.faltas.push(valor);
                    });

                    if (self.faltas.length > 0) {
                        defColunas = configuraColunasGrid(self.faltas[0]);
                    }

                    self.gridOptions = {
                        columns: defColunas,
                        dataSource: self.faltas,
                        groupable: {
                            messages: {
                                empty: $filter('i18n')('grid-agrupamento-vazio')
                            }
                        },
                        pageable: false,
                        sortable: true,
                        resizable: true,
                        scrollable: true,
                        selectable: false
                    };

                    criaGrid();
                });
        }

        /**
         * Configura as colunas com valores fixos.
         *
         * @param {any} valor - Chave<Valor> da coluna.
         * @param {any} colunaConfig - coluna configurada.
         *
         * @returns coluna configurada
         */
        function retornaColunasFixas(valor, colunaConfig) {
            //Primeira coluna
            if (!self.escondeColSituacao) {
                colunaConfig.push({
                    field: 'SITUACAOFALTAS',
                    width: '8%',
                    title: buscaColunaDescricao('SITUACAOFALTAS'),
                    template: formatarColunaSituacaoFalta,
                    attributes: atributosBase()
                });
            }

            if (valor['Filial']) {
                colunaConfig.push({
                    field: 'Filial',
                    width: '15%',
                    title: buscaColunaDescricao('Filial'),
                    template: formatarTemplateGeral('Filial'),
                    attributes: atributosBase(),
                    groupHeaderTemplate: formatarCabecalhoGrupoGeral
                });
            }

            colunaConfig.push({
                field: 'Disciplina',
                width: '30%',
                title: buscaColunaDescricao('Disciplina'),
                template: formatarTemplateDetalhesDisciplina(),
                attributes: atributosBase(),
                groupHeaderTemplate: formatarCabecalhoGrupoDisciplina
            });

            return colunaConfig;
        }

        /**
         * Retorna as colunas customizadas e/ou geradas dinâmicamente.
         *
         * @param {any} valor - Chave<valor> da coluna.
         * @param {any} colunaConfig - coluna configurada.
         * @returns coluna configurada.
         */
        function retornaColunasCustomizadas(valor, colunaConfig) {
            var coluna;

            for (coluna in valor) {
                if (valor.hasOwnProperty(coluna) &&
                    (coluna.indexOf('$') === -1)) {

                    var nomeColuna = buscaColunaDescricao(coluna);

                    switch (coluna) {

                        case 'IDTURMADISC':
                        case 'SITUACAOFALTAS':
                        case 'Filial':
                        case 'CODDISC':
                        case 'Disciplina':
                        case 'Situação':
                            continue;
                        case 'PERCENTUAL':
                        if (typeof self.parametrosEdu != 'undefined' &&
                            typeof self.parametrosEdu.PercentualLimiteFaltas != 'undefined' && self.parametrosEdu.PercentualLimiteFaltas != null) {
                                colunaConfig.push({
                                    field: coluna,
                                    width: '10%',
                                    title: nomeColuna,
                                    groupable: false,
                                    template: '<span>#=data["' + coluna + '"]#%</span>',
                                    attributes: {
                                        'class': 'gridRow textAlignCenter'
                                    }
                                });
                            }
                            break;
                        case 'CODTURMA':
                        case 'Situação':
                            colunaConfig.push({
                                field: coluna,
                                width: '10%',
                                title: nomeColuna,
                                template: formatarTemplateGeral(coluna),
                                attributes: {
                                    'class': 'gridRow textAlignCenter'
                                },
                                groupHeaderTemplate: formatarCabecalhoGrupoGeral
                            });
                            break;
                        case 'verFaltas':
                            colunaConfig.push({
                                field: '',
                                width: '7%',
                                title: '&nbsp',
                                groupable: false,
                                attributes: {
                                    'class': 'gridRow textAlignCenter'
                                },
                                template: formatarTemplateVerFaltasPorAula()
                            });
                            break;
                        default:
                            colunaConfig.push({
                                field: '["' + coluna + '"]',
                                title: nomeColuna,
                                width: '10%',
                                groupable: false,
                                attributes: {
                                    'class': 'gridRow textAlignCenter'
                                }
                            });
                    }
                }
            }

            return colunaConfig;
        }

        /**
         * Realiza a configuração das colunas que serão apresentadas no grid
         *
         * @param {any} valor - Conteúdo retornado pelo REST. Listagem de Notas por etapa.
         * @returns
         */
        function configuraColunasGrid(valor) {

            var colunaConfig = [];

            colunaConfig = retornaColunasFixas(valor, colunaConfig);
            colunaConfig = retornaColunasCustomizadas(valor, colunaConfig);

            return colunaConfig;
        }

        /**
         * Busca a descrição das colunas que será o cabeçalho apresentado no grid.
         *
         * @param {any} colunaNome - Nome da coluna
         * @returns Descrição da coluna
         */
        function buscaColunaDescricao(colunaNome) {

            var descricaoColuna,
                colunasFixas = {
                    'SITUACAOFALTAS': $filter('i18n')('l-situacaofaltas', [], 'js/aluno/faltas/'),
                    'CODDISC': $filter('i18n')('l-codigo-disciplina', [], 'js/aluno/faltas/'),
                    'DISCIPLINA': $filter('i18n')('l-disciplina', [], 'js/aluno/faltas/'),
                    'Situação': $filter('i18n')('l-situacao', [], 'js/aluno/faltas/'),
                    'CODTURMA': $filter('i18n')('l-turma', [], 'js/aluno/faltas/'),
                    'PERCENTUAL': $filter('i18n')('l-percentual', [], 'js/aluno/faltas/')
                };

            descricaoColuna = colunasFixas[colunaNome];

            if (!descricaoColuna) {
                descricaoColuna = colunaNome;
            }
            return descricaoColuna;
        }

        /**
         * Cria o elemento grid na tela
         */
        function criaGrid() {
            $('#faltasGrid').empty();
            // cria o elemento através do angular.
            var grid = angular.element('<totvs-grid></totvs-grid>');

            // adiciona o atributo grid-options com as configurações do grid
            grid.attr('grid-options', 'controller.gridOptions');
            grid.attr('grid', 'controller.myGrid');
            grid.attr('on-data', 'controller.onData(data)');

            $('#faltasGrid').css('padding-top', '10px');
            $('#faltasGrid tr').addClass('GridRow');

            // insere o elemento na div
            grid = $('#faltasGrid').append(grid);

            // compila o elemento na tela.
            grid = $compile(grid)($scope);
        }

        /**
         * Abre o modal de Detalhes da Disciplina
         *
         * @param {any} idTurmaDisc - Identificador da turma disciplina
         */
        function abrirDetalhesDisciplina(idTurmaDisc) {
            var contexto, codColigada, ra;

            contexto = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
            codColigada = contexto.cursoSelecionado.CODCOLIGADA;
            ra = contexto.cursoSelecionado.RA;

            MatriculaDisciplinaService.exibirDetalhesMatriculaDisciplina(codColigada, idTurmaDisc, ra);
        }

        /**
         * Abre a visualização de faltas por aula
         *
         * @param {any} idTurmaDisc - Identificador da turma disciplina
         */
        function abrirVisualizacaoFaltasPorAula(idTurmaDisc) {
            //Verifica se a view 'pai' possui o método a ser invocado
            if ($scope.$parent['controller'] !== null) {
                if (typeof ($scope.$parent['controller'].selecionarTabFaltasPorAula) === 'function') {
                    $scope.$parent['controller'].selecionarTabFaltasPorAula();
                    $scope.$emit('selecionaDisciplina', idTurmaDisc, 360);
                }
            }
        }

        /**
         * retorna os atributos padrões do grid
         *
         * @returns retorna atributos padrões
         */
        function atributosBase() {
            return {
                class: 'gridRow'
            };
        }

        /**
         * Formata coluna de detalhes da disciplina
         *
         * @returns Coluna formatada.
         */
        function formatarTemplateDetalhesDisciplina() {
            return ('<a ng-click="controller.abrirDetalhesDisciplina(#:data.IDTURMADISC#)" ' +
                'tooltip-append-to-body=\'appendToBody=true\' data-tooltip="#:data.Disciplina#" tooltip-placement="left">' +
                '#:data.Disciplina#</a>');
        }


        /**
         * Formata coluna de Ver Faltas
         *
         * @returns Coluna formatada.
         */
        function formatarTemplateVerFaltasPorAula() {
            return ('<a ng-click="controller.abrirVisualizacaoFaltasPorAula(#:data.IDTURMADISC#)">#:data.verFaltas#</a>');
        }

        /**
         * Formata colunas padrões
         *
         * @param {any} coluna - Nome da Coluna
         * @returns Coluna formatada.
         */
        function formatarTemplateGeral(coluna) {
            return ('<span tooltip-append-to-body=\'appendToBody=true\' data-tooltip="#:data["' + coluna + '"]#" tooltip-placement="left">' +
                '#:data["' + coluna + '"]# </span>')
        }

        /**
         * Formata colunas padrões
         *
         * @param {any} item - Nome da Coluna
         * @returns Coluna formatada.
         */
        function formatarColunaSituacaoFalta(item) {
            var conteudo;
            switch (item.SITUACAOFALTAS) {
                case 1:
                    conteudo = '<span class="tag legend dentro-limite"></span>';
                    break;
                case 2:
                    conteudo = '<span class="tag legend acima-limite"></span>';
                    break;
                case 3:
                    conteudo = '<span class="tag legend proximo-limite"></span>';
                    break;
                default:
                    conteudo = '';
            }

            return '<span>' + conteudo + '</span>';
        }

        /**
         * Carrega os parâmetros do educacional
         *
         */
        function carregarParametrosEdu() {
            eduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (params) {
                self.parametrosEdu = params;
                self.escondeColSituacao = !(self.parametrosEdu.PercentualLimiteFaltas);
                buscafaltas();
            });
        }

        /**
         * Retorna o identificador da turma disciplina de acordo com a disciplina passada por parâmetro
         *
         * @param {string} disciplina - disciplina
         * @returns Id. Turma Disciplina
         */
        function retornaIdTurmaDisc(disciplina) {
            var idTurmaDisc = 0,
                disciplinaObjeto = $.grep(self.faltas, function (e) {
                    return e.Disciplina === disciplina;
                });

            if (disciplinaObjeto && disciplinaObjeto.length > 0) {
                idTurmaDisc = disciplinaObjeto[0].IDTURMADISC;
            }
            return idTurmaDisc;
        }

        /**
         * Formata o cabeçalho da coluna de agrupamento de disciplina
         *
         * @param {any} item - objeto de itens.
         * @returns Cabeçalho formatado.
         */
        function formatarCabecalhoGrupoDisciplina(item) {
            return '<a tooltip-append-to-body="appendToBody=true" ' +
                'tooltip-placement="left" data-tooltip="' + eduUtilsService.escapeHtml(item.value) + '"' +
                'ng-click="controller.abrirDetalhesDisciplina(' + retornaIdTurmaDisc(item.value) + ')"> Disciplina: ' +
                eduUtilsService.escapeHtml(item.value) + '</a>';
        }

        /**
         * Formata o cabeçalho da coluna de agrupamento
         *
         * @param {any} item
         */
        function formatarCabecalhoGrupoGeral(item){
            return buscaColunaDescricao(item.field) + ': ' + item.value;
        }
    }
});
