[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-titulo": {
            "pt": "Entregas de avaliações/trabalhos",
			"en": "Delivery of tests/papers",
			"es": "Entregas de evaluaciones/trabajos"
        },
        "l-widget-title": {
          "pt": "Painel do Aluno",
		  "en": "Student board",
		  "es": "Panel del alumno"
        },
        "l-tag-enviado": {
            "pt": "Enviado",
			"en": "Sent",
			"es": "Enviado"
        },
        "l-tag-prazo-expirado": {
            "pt": "Prazo expirado",
			"en": "Deadline expired",
			"es": "Plazo expirado"
        },
        "l-tag-entrega-disponivel": {
            "pt": "Entrega disponível",
			"en": "Delivery available",
			"es": "Enpublicaçãotrega disponible"
        },
        "l-btn-enviar-file": {
            "pt": "Enviar arquivo",
			"en": "Send file",
			"es": "Enviar archivo"
        },
        "l-btn-update-file": {
            "pt": "Atualizar arquivo",
			"en": "Update file",
			"es": "Actualizar archivo"
        },
        "l-btn-excluir-file": {
            "pt": "Excluir arquivo",
			"en": "Delete file",
			"es": "Borrar archivo"
        },
        "l-btn-baixar-file": {
            "pt": "Baixar arquivo",
			"en": "Download file",
			"es": "Bajar archivo"
        },
        "l-nenhum-registro-encontrado": {
            "pt": "Nenhum Registro Encontrado!",
			"en": "No record found!",
			"es": "¡No se encontró ningún registro!"
        },
        "l-data-limite": {
            "pt": "Data Limite de Entrega",
			"en": "Delivery deadline",
			"es": "Fecha límite de entrega"
        },
        "l-valor": {
            "pt": "Valor",
			"en": "Value",
			"es": "Valor"
        },
        "l-disciplina": {
            "pt": "Disciplina",
			"en": "Subject",
			"es": "Materia"
        },
        "l-nota": {
            "pt": "Nota",
			"en": "Grade",
			"es": "Nota"
        },
        "l-arquivo": {
            "pt": "Arquivo",
			"en": "File",
			"es": "Archivo"
        },
        "l-tamanho-arquivo": {
            "pt": "Tamanho do Arquivo",
			"en": "File size",
			"es": "Tamaño del archivo"
        },
        "l-data-entrega": {
            "pt": "Data da Entrega",
			"en": "Delivery date",
			"es": "Fecha de la entrega"
        },
        "l-observacao": {
            "pt": "Observação",
			"en": "Note",
			"es": "Observación"
        },
        "l-protocolo-entrega": {
            "pt": "Protocolo da Entrega",
			"en": "Delivery protocol",
			"es": "Comprobante de la entrega"
        },
        "l-add-arquivo-anexo": {
            "pt": "Enviar Arquivo do Trabalho/Avaliação",
			"en": "Send paper/test file",
			"es": "Enviar archivo del Trabajo/Evaluación"
        },
        "l-descricao": {
            "pt": "Descrição",
			"en": "Description",
			"es": "Descripción"
        },
        "l-etapa": {
            "pt": "Etapa",
			"en": "Stage",
			"es": "Etapa"
        },
        "l-msg-campo-obrigatorio": {
            "pt": "Campo de preenchimento obrigatório",
			"en": "Required field",
			"es": "Campo de cumplimentación obligatoria"
        },
        "l-enviar-arquivo": {
            "pt": "Enviar Arquivo",
			"en": "Send file",
			"es": "Enviar archivo"
        },
        "l-btn-entregar": {
            "pt": "Entregar",
			"en": "Deliver",
			"es": "Entregar"
        },
        "l-msg-delete-file": {
            "pt": "Arquivo excluído com sucesso!",
			"en": "File deleted successfully!",
			"es": "¡Archivo borrado con éxito!"
        },
        "l-msg-delete-file-erro": {
            "pt": "Ocorreu um erro ao tentar excluir o arquivo.",
			"en": "An error occurred while trying to delete the file.",
			"es": "Ocurrió un error al intentar borrar el archivo."
        },
        "l-msg-confirmacao-exclusao": {
            "pt": "Deseja excluir o arquivo entregue?",
			"en": "Delete delivered file?",
			"es": "¿Desea borrar el archivo entregado?"
        },
        "l-ext-permitida": {
            "pt": "Extensões permitidas",
			"en": "Extensions allowed",
			"es": "Extensiones permitidas"
        },
        "l-tamanho-max-arq": {
            "pt": "Tamanho máximo do arquivo",
			"en": "Maximum file size",
			"es": "Tamaño máximo del archivo"
        }
    }
]
