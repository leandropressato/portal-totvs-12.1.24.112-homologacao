/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.20
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduPlanoAulaModule
 * @name eduPlanoAulaController
 * @object controller
 *
 * @created 2018-02-10 v12.1.20
 * @updated
 *
 * @requires planoAula.module
 *
 * @dependencies
 *
 * @description Controller de Plano de Aula
 */
define([
    'aluno/plano-aula/plano-aula.module',
    'aluno/plano-aula/plano-aula.factory',
    'aluno/aulas/aulas.factory',
    'aluno/arquivos/arquivo.factory'
], function () {

    'use strict';

    angular.module('eduPlanoAulaModule')
        .controller('eduPlanoAulaController', eduPlanoAulaController);

    eduPlanoAulaController.$inject = [
        '$scope',
        '$rootScope',
        '$filter',
        '$state',
        'TotvsDesktopContextoCursoFactory',
        'eduPlanoAulaFactory',
        'eduAulasFactory',
        'eduMaterialFactory'
    ];

    function eduPlanoAulaController(
        $scope,
        $rootScope,
        $filter,
        $state,
        TotvsDesktopContextoCursoFactory,
        eduPlanoAulaFactory,
        eduAulasFactory,
        eduMaterialFactory
    ) {
        // *********************************************************************************
        // *** Variables
        // *********************************************************************************

        var self = this;
        self.planoAulas = [];
        self.ArquivosPlanoAula = [];
        self.DataAulas = '';

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.buscaAulas = buscaAulas;
        self.downloadMaterial = downloadMaterial;
        self.tipoAula = tipoAula;

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************

        Date.prototype.isValid = function () {
            // An invalid date object returns NaN for getTime() and NaN is the only
            // object not strictly equal to itself.
            return this.getTime() === this.getTime();
        };         

        init();

        function init() {
            self.DataAulas = new Date($state.params.DataAulas);

            if (!self.DataAulas.isValid()) {
                self.DataAulas = new Date();
            }
                        
            self.setFnExport = function (obj) {
                self.exportToPdf = obj.pdf;
                self.exportToPng = obj.png;
            };

            buscaAulas(self.DataAulas);
        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Busca todas as aulas de todas as disciplinas de um determinado dia, no PL corrente
         * 
         * @returns Retorna as etapsa para preencher o combo de seleção de etapa
         */           
        function buscaAulas() {
            eduPlanoAulaFactory.PlanoAulaAluno(self.DataAulas, function (result) {
                if (typeof result != 'undefined') {
                    self.planoAulas = result.SPlanoAula;
                    self.ArquivosPlanoAula = result.SPLANOAULAARQUIVO;
                    if (self.planoAulas) {
                        self.planoAulas.forEach(function(value, index) {
                            value['LOCAL'] = eduAulasFactory.montarDescricaoLocal(value.PREDIO, value.BLOCO, value.SALA);
                        });
                    }
                }
            });
        }

        function tipoAula(codTipoAula) {
            switch (codTipoAula) {
                case 'T':
                    return $filter('i18n')('l-aulateorica', [], 'js/aluno/plano-aula');
                    break;
                case 'P':
                    return $filter('i18n')('l-aulapratica', [], 'js/aluno/plano-aula');
                    break;               
                case 'L':
                    return $filter('i18n')('l-laboratorio', [], 'js/aluno/plano-aula');
                    break;
                case 'E':
                    return $filter('i18n')('l-estagio', [], 'js/aluno/plano-aula');
                    break;                                                   
                default:
                    break;
            }
        }

        /**
         * Download do material do plano de aula
         *
         * @param {Object} idMaterial IdMaterial para download
         * @param {Object} patharquivo Path do arquivo para download
         */
        function downloadMaterial(idMaterial, patharquivo) {
            var link = document.createElement('a');
            link.download = patharquivo;
            link.href = eduMaterialFactory.urlDownloadArquivo(idMaterial, patharquivo);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }        
    }
});
