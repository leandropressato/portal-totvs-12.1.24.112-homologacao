/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.20
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduPlanoAulaModule
 * @name EduFaltasUnificadaFactory
 * @object factory
 *
 * @created 2018-02-20 v12.1.20
 * @updated
 *
 * @requires eduPlanoAulaModule
 *
 * @dependencies eduPlanoAulaFactory
 *
 * @description Factory utilizada para PlanoAula.
 */

define(['aluno/plano-aula/plano-aula.module'], function() {

    'use strict';

    angular
        .module('eduPlanoAulaModule')
        .factory('eduPlanoAulaFactory', EduPlanoAulaFactory);

    EduPlanoAulaFactory.$inject = ['$totvsresource', 'TotvsDesktopContextoCursoFactory'];

    function EduPlanoAulaFactory($totvsresource, TotvsDesktopContextoCursoFactory) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.PlanoAulaAluno = PlanoAulaAluno;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function PlanoAulaAluno(dataSelecionada, callback) {
            var data = new Date(dataSelecionada);

            var parameters = {};
            parameters['method'] = 'v1/planoaulas/aulas';
            parameters['dtFinal'] = data;
            parameters['dtInicial'] = data;
            parameters['idContextoAluno'] = TotvsDesktopContextoCursoFactory.getIdContextoEncode();

            factory.TOTVSGet(parameters, callback);
        }
    }
});