[{
    "l-titulo": {
        "pt": "Plano de Aula"
    },
    "l-disciplina": {
        "pt": "Disciplina"
    },
    "l-turma": {
        "pt": "Turma"
    },
    "l-subturma": {
        "pt": "Subturma"
    },    
    "l-aula": {
        "pt": "Aula"
    },    
    "l-tipo": {
        "pt": "Tipo"
    },
    "l-inicio": {
        "pt": "Início"
    },
    "l-termino": {
        "pt": "Término"
    },
    "l-filtrar": {
        "pt": "Filtrar"
    },    
    "l-local": {
        "pt": "Local"
    },
    "l-data-aula": {
        "pt": "Data da aula"
    },    
    "l-conteudoprevisto": {
        "pt": "Conteúdo previsto"
    },
    "l-conteudoefetivo": {
        "pt": "Conteúdo realizado"
    },
    "l-licaocasa": {
        "pt": "Lição de casa"
    },
    "l-arquivos": {
        "pt": "Arquivos da aula"
    },
    "l-aulateorica": {
        "pt": "Aula teórica"
    },
    "l-aulapratica": {
        "pt": "Aula prática"
    },
    "l-aulamista": {
        "pt": "Aula mista"
    },
    "l-laboratorio": {
        "pt": "Laboratório"
    },
    "l-aulapraticalaboratorial": {
        "pt": "Prática laboratorial"
    },
    "l-aulateoricopratica": {
        "pt": "Teórica/prática"
    },
    "l-trabalhocampo": {
        "pt": "Trabalho de campo"
    },
    "l-seminario": {
        "pt": "Seminário"
    },
    "l-estagio": {
        "pt": "Estágio"
    },
    "l-orientacaotutorial": {
        "pt": "Orientação tutorial"
    },
    "l-msg-nenhum-registro": {
        "pt": "Nenhum registro de notas foi localizado para exibir o gráfico de Desempenho do aluno!"
    },
    "l-nenhum-registro-encontrado": {
        "pt": "Nenhum registro encontrado!"
    }
}]
