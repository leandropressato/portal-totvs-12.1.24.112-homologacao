/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.12
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
* @module eduRelatoriosModule
* @name eduRelatoriosFactory
* @object factory
*
* @created 30/08/2018 v12.1.22
* @updated
*
* @dependencies $totvsresource
*
* @description Factory utilizada na tela de Relatorios.
*/
define(['aluno/relatorios/relatorios.module'], function () {

    'use strict';

    angular
        .module('eduRelatoriosModule')
        .factory('eduRelatoriosFactory', EduRelatoriosFactory);

    EduRelatoriosFactory.$inject = ['$totvsresource'];

    function EduRelatoriosFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_API + 'educational/:method';
        var factory = $totvsresource.REST(url, {}, {});

        factory.listReports = listReports; // Busca os relatorios
        factory.viewReport = viewReport; //Executa o relatório

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function listReports(order, page, pageSize, cursoAluno, callback) {
            var parameters = {};
            parameters['method'] = 'v1/educationalreports';
            parameters['options.order'] = order;
            parameters['options.page'] = page;
            parameters['options.pageSize'] = pageSize;
            parameters['educationalReport.companyCode'] = cursoAluno.cursoSelecionado.CODCOLIGADA;
            parameters['educationalReport.branchCode'] = cursoAluno.cursoSelecionado.CODFILIAL;
            parameters['educationalReport.levelEducationCode'] = cursoAluno.cursoSelecionado.CODTIPOCURSO;

            return factory.TOTVSGet(parameters, callback);
        }

        function viewReport(id, context, callback) {
            var parameters = {};
            parameters['method'] = 'v1/educationalreports/' + id + '/vieweducationalreports';

            var model = {
                CompanyCode: context.cursoSelecionado.CODCOLIGADA,
                StudentCode: context.cursoSelecionado.RA,
                SpecializationBranchCode: context.cursoSelecionado.IDHABILITACAOFILIAL,
                TermCode: context.cursoSelecionado.IDPERLET
            };

            return factory.TOTVSPost(parameters, model, callback);
        }
    }
});
