/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.22
 * (c) 2015-2018 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduRelatoriosModule
 * @name EduRelatoriosController
 * @object controller
 *
 * @created 30/08/2018 v12.1.22
 * @updated
 *
 * @requires relatorios.module
 *
 * @dependencies eduRelatoriosFactory
 *
 * @description Controller de Relatorios
 */
define(['aluno/relatorios/relatorios.module',
        'aluno/relatorios/relatorios.factory',
        'utils/reports/edu-relatorio.service'
], function () {

    'use strict';

    angular
        .module('eduRelatoriosModule')
        .controller('EduRelatoriosController', EduRelatoriosController);

    EduRelatoriosController.$inject = ['eduRelatoriosFactory',
        'eduRelatorioService',
        'TotvsDesktopContextoCursoFactory',
        'i18nFilter',
        'totvs.app-notification.Service'
    ];

    function EduRelatoriosController(eduRelatoriosFactory, 
        eduRelatorioService,
        TotvsDesktopContextoCursoFactory,
        i18nFilter, 
        totvsNotification) {

        var self = this;

        self.typeSelected = 0;
        self.pageSize = 100;
        self.page = 0;
        self.hasNext = false;
        self.order = 'ReportCode';

        self.objReportList = [];
        self.objReportListNoFilter = [];
        self.objTypeReportList = [];

        self.viewReport = viewReport;
        self.periodoLetivoAlterado = periodoLetivoAlterado;
        self.getListReports = getListReports;
        self.nextPage = nextPage;
        self.typeChange = typeChange;
        self.objCursoSelecionado = {};

        init();

        function init() {
            self.page = 1;
            self.objCursoSelecionado = TotvsDesktopContextoCursoFactory.getCursoSelecionado();
        }

        function periodoLetivoAlterado() {
            getListReports();
        }

        function nextPage() {
            self.page++;
            getListReports();
        }

        function getListReports() {
            if (self.periodoLetivo !== 0) {

                self.objReportList = [];
                self.objReportListNoFilter = [];                

                eduRelatoriosFactory.listReports(self.order, self.page, self.pageSize, self.objCursoSelecionado,
                    function (result) {
                    if (typeof result != 'undefined') {

                        angular.forEach(result.Items, function (value) {
                            self.objReportListNoFilter.push(value);
                        });

                        self.hasNext = result.HasNext;

                        getTypeList();
                        typeChange();
                    }
                });
            }
        }

        function getTypeList() {
            self.objTypeReportList = self.objReportListNoFilter.filter(function(elem, index, self) {
                return index === self.findIndex(verifyTypeCode, elem);
            });

            self.objTypeReportList.splice(0, 0, {'ReportTypeCode' : 0, 'ReportTypeDescription': i18nFilter('l-todos', [], 'js/aluno/relatorios')});
        }

        function typeChange() {
            self.objReportList = $.grep(self.objReportListNoFilter, function (e) {
              return e.ReportTypeCode === self.typeSelected || self.typeSelected === 0;
            });
        }        

        function verifyTypeCode(element, index, array) {
            return element.ReportTypeCode === this.ReportTypeCode;
        }

        function viewReport(element) {

            eduRelatoriosFactory.viewReport(element.InternalId, self.objCursoSelecionado, function (result) {

                if (typeof result != 'undefined') {
                    eduRelatorioService.exibirOuSalvarPDF(result.ReportFile);
                } else {
                    totvsNotification.notify({
                        type: 'error',
                        title: i18nFilter('l-erro-titulo', [], 'js/aluno/relatorios'),
                        detail: i18nFilter('l-erro-visualizar', [], 'js/aluno/relatorios')
                    });                    
                }

            });    
        }
    }
});
