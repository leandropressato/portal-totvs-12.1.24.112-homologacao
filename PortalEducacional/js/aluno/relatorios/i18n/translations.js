[
    {
        "l-home": {
            "pt": "Início",
			"en": "Start",
			"es": "Inicio"
        },
        "l-Nenhum-Registro-Encontrado": {
            "pt": "Nenhum registro encontrado!",
			"en": "Nenhum registro encontrado!",
			"es": "Nenhum registro encontrado!"
        },        
        "l-titulo": {
            "pt": "Relatórios",
			"en": "Relatórios",
			"es": "Relatórios"
        },
        "l-erro-visualizar": {
            "pt": "Não foi possível exibir o relatório.",
            "en": "Não foi possível exibir o relatório.",
            "es": "Não foi possível exibir o relatório."
        },  
        "l-erro-titulo": {
            "pt": "Erro na visualização.",
            "en": "Erro na visualização.",
            "es": "Erro na visualização."
        },               
        "btn-emitirrelatorio": {
            "pt": "Emitir relatório",
			"en": "Emitir relatório",
			"es": "Emitir relatório"
        },
        "l-TIPO": {
            "pt": "Tipo de relatório",
			"en": "Tipo de relatório",
			"es": "Tipo de relatório"
        },
        "l-NOME": {
            "pt": "Nome",
			"en": "Nome",
			"es": "Nome"
        },
        "l-todos": {
            "pt": "TODOS",
            "en": "TODOS",
            "es": "TODOS"
        },
        "l-tipos-relatorios": {
            "pt": "Tipos de relatórios",
            "en": "Tipos de relatórios",
            "es": "Tipos de relatórios"
        }        
    }
]