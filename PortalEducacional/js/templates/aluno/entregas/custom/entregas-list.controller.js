/**
 * @description: Template para criação de controller de entregas de trabalhos.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e
 * o segundo é a declaração de uma função.
 *
 * Ex:
 *
 * define(['js/aluno/entregas/entregas.factory'], function(){});
 */
define([], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller de ocorrências
     */
    angular
        .module('eduEntregasModule')
        .controller('eduEntregasController', EduEntregasController);

    /**
     * Injeção das dependências do controller em forma de array.
     *
     * Ex:
     *
     * EduOcorrenciasController.$inject = ['EduOcorrenciaFactory'];
     */
    EduEntregasController.$inject = ['$scope'];

    /**
     * Função construtora do controller.
     * Nela deve conter toda a dependência injetada, como parâmetro.
     *
     * Ex:
     *
     * function EduOcorrenciasController(ocorrenciaFactory){};
     */
    function EduEntregasController($scope) {

        var self = this;

        init();

        /**
         * Função disparada para iniciar o controller.
         * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
         *
         */
        function init() {

        }
    }
});
