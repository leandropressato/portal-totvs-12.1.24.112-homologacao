/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduCalendarioModule
 * @name eduCalendarioFactory
 * @object factory
 *
 * @created 2017-02-06 v12.1.15
 * @updated
 *
 * @requires eduCalendarioModule
 *
 * @dependencies
 *
 * @description Factory utilizada no menu calendário.
 */

define(['aluno/calendario/calendario.module'], function () {

    'use strict';

    angular
        .module('eduCalendarioModule')
        .factory('eduCalendarioFactory', EduCalendarioFactory);

    EduCalendarioFactory.$inject = ['$totvsresource'];

    function EduCalendarioFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/:method',
            factory;

        factory = $totvsresource.REST(url, {}, {});
        factory.retornaCalendario = retornaCalendario;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        /**
         * Retorna o calendário do aluno em um período letivo
         *
         * @param {any} callback   - método de callback
         */
        function retornaCalendario(callback) {

            var parameters = {};

            parameters.method = 'Aluno/CalendarioAluno';
            factory.TOTVSGet(parameters, callback);

        }

    }
});

/**
 * @description: Template para criação de factory.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 * 
 * define(['js/aluno/calendarios/calendario.module'], function(){});
 */
define(['aluno/calendario/calendario.module'], function () {

    'use strict';

    angular
        .module('eduCalendarioModule')
        .factory('eduCalendarioFactory', EduCalendarioFactory);

    /**
    * Injeção do módulo e declaração  factory
    */
    EduCalendarioFactory.$inject = [];

    /**
     *  Injeção das dependências da factory em forma de array.
     * 
     * Ex:
     * 
     * EduOcorrenciasController.$inject = ['$totvsresource'];
     */
    function EduCalendarioFactory() {
        /**
               * Url do serviço que será consumido pela factory
               */
        var url = '';
        var factory;

        /**
         * Métodos expostos pela factory
         */
        factory.findRecords = findRecords;

        return factory;

        function findRecords(parameters, callback) {

            return;
        }
    }
});

