/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/atividades-extras.factory'], function(){});
 */
define(['widgets/widget.constants'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
       angular
        .module('eduAtividadesExtrasModule')
        .controller('AtividadeExtraDetalhesController', AtividadeExtraDetalhesController);

    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * AtividadeExtraDetalhesController.$inject = ['EduArquivosFactory'];
    */
    AtividadeExtraDetalhesController.$inject = ['$scope',
        '$rootScope',
        '$state',
        'eduWidgetsConsts'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * 
    * function AtividadeExtraDetalhesController(arquivosFactory){};
    */
    function AtividadeExtraDetalhesController($scope, $rootScope,$state, eduWidgetsConsts) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});