/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduAtividadesExtrasModule
 * @name eduAtividadesExtrasInscritasController
 * @object controller
 *
 * @created 21/11/2016 v12.1.15
 * @updated
 *
 * @requires eduAtividadesExtras.module
 *
 * @dependencies eduAtividadesExtrasFactory
 *
 * @description Controller de Atividades Inscritas
 */
define(['aluno/atividades-extras/atividades-extras.module',
        'aluno/atividades-extras/atividades-extras.factory',
        'aluno/atividades-extras/atividades-extras.service',
        'utils/edu-enums.constants'
], function () {

    'use strict';

    angular
        .module('eduAtividadesExtrasModule')
        .controller('eduAtividadesExtrasInscritasController', EduAtividadesExtrasInscritasController);

    EduAtividadesExtrasInscritasController.$inject = [
        '$rootScope',
        '$scope',
        'eduAtividadesExtrasService',
        'eduAtividadesExtrasFactory',
        '$filter',
        'i18nFilter',
        'totvs.app-notification.Service',
        '$state',
        'eduEnumsConsts'];

    /**
     * Controller Atividades Extras - Inscritas
     *
     * @param {any} $rootScope  Trafega informações padrão do sistema.
     * @param {any} $scope Trafega informações da funcionalidade.
     * @param {any} eduAtividadesExtrasService  Service Atividades Extras
     * @param {any} eduAtividadesExtrasFactory Factory Atividades Extras
     * @param {any} $filter Factory Atividades Extras
     * @param {any} i18nFilter Filter de tradução
     * @param {any} totvsNotification Serviço de notificações
     * @param {any} $state Estado da aplicação
     * @param {any} EduEnumsConsts Constantes do app
     */
    function EduAtividadesExtrasInscritasController(
        $rootScope,
        $scope,
        eduAtividadesExtrasService,
        eduAtividadesExtrasFactory,
        $filter,
        i18nFilter,
        totvsNotification,
        $state,
        EduEnumsConsts) {

        var self = this;

        // Objetos e variáveis
        self.objAtividadesExtrasList = [];
        self.objAtividadesExtrasListFull = [];
        self.intAtividadesExtrasCount = 0;
        self.listComponentes = [];
        self.cmbComponenteSelected = -1;
        self.permiteCancelar = true;
        self.codStatusAtvConcluida = EduEnumsConsts.StatusAtivInscrita.Concluida;

        // métodos
        self.getAtividadesInscritas = getAtividadesInscritas;
        self.cancelarAtividade = cancelarAtividade;
        self.filtraPorComponente = filtraPorComponente;
        self.formataComboComponentes = formataComboComponentes;
        self.legendaClass = legendaClass;

        init();

        /**
         * Inicialia as informações da tela
         */
        function init() {
            self.objAtividadesExtrasList = [];
            self.objAtividadesExtrasListFull = [];
            getAtividadesInscritas();
            
            var myWatch = $rootScope.$watch('objPermissions', function (data) {
                if (data !== null) {
                    self.permiteCancelar = angular.isDefined($rootScope.objPermissions.EDU_ACADEMICO_OPORTUNIDADES_ATIVIDADESEXTRAS_INSCRITAS_CANCELAR);
                    myWatch();
                }
            });
        }

        /**
         * Recupera todas as atividades inscritas pelo aluno
         */
        function getAtividadesInscritas () {
            eduAtividadesExtrasFactory.getAtividadesInscritas(function (result) {
                if (result) {
                    angular.forEach(result, function (value) {
                        if (value && value.$length) {
                            self.intAtividadesExtrasCount = value.$length;
                        }
                        self.objAtividadesExtrasList.push(value);
                        self.objAtividadesExtrasListFull.push(value);
                    });
                    eduAtividadesExtrasService.objComboComponentes(self.objAtividadesExtrasList, function (values) {
                        angular.forEach(values, function (value) {
                            self.listComponentes.push(value);
                        });
                    });
                }
            });
        }

        /**
         * Cancela a participação do aluno na atividade.
         *
         * @param {any} objAtividade - objeto da atividade
         */
        function cancelarAtividade (objAtividade) {
            totvsNotification.question({
                title: i18nFilter('l-Confirmacao'),
                text: i18nFilter('l-msg-confirmacao-cancelamento', [], 'js/aluno/atividades-extras'),
                cancelLabel: 'l-no',
                size: 'md', // sm = small | md = medium | lg = larger
                confirmLabel: 'l-yes',
                callback: function (isPositiveResult) {
                    if (isPositiveResult) {
                        deletaAtividadeInscrita(objAtividade);
                    }
                }
            });
        }

        /**
         * Efetua a exclusão do aluno na atividade
         *
         * @param {any} objAtividade - model atividade
         */
        function deletaAtividadeInscrita(objAtividade) {
            eduAtividadesExtrasFactory.deleteAtividadesInscritas(objAtividade, function (result) {
                if (result.data) {
                    if (angular.isDefined(result.exception)) {
                        totvsNotification.notify({
                            type: 'error',
                            title: i18nFilter('l-atividades-inscritas', [], 'js/aluno/atividades-extras'),
                            detail: result.message
                        });
                    } else {
                        totvsNotification.notify({
                            type: 'success',
                            title: i18nFilter('l-atividades-inscritas', [], 'js/aluno/atividades-extras'),
                            detail: i18nFilter('l-cancelamento-ok', [], 'js/aluno/atividades-extras')
                        });
                        // se efetuou o cancelamento corretamente, recarrega a rota
                        reloadAndActiveTab();
                    }
                } else {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-atividades-inscritas', [], 'js/aluno/atividades-extras'),
                        detail: i18nFilter('l-cancelamento-falha', [], 'js/aluno/atividades-extras')
                    });
                }
            });
        }

        /**
         * Aplica o filtro quando o usuário altera o combo de componentes.
         * */
        function filtraPorComponente () {
            if (self.cmbComponenteSelected === -1) { // todos
                self.objAtividadesExtrasList = self.objAtividadesExtrasListFull;
            } else {
                self.objAtividadesExtrasList =
                    $filter('filter')(self.objAtividadesExtrasListFull,
                                        { 'CODCOMPONENTE': self.cmbComponenteSelected },
                                        true);
            }
        }

        /**
         * Formata o option do combo de componentes
         *
         * @param {any} item - values do option
         * @returns option label formatado
         */
        function formataComboComponentes (item) {
            return eduAtividadesExtrasService.formataLabelComboComponentes(item);
        }

        /**
         * Define qual legenda será aplicada por atividade inscrita
         *
         * @param {any} model - objeto da inscrição
         * @returns class css da legenda
         */
        function legendaClass(model) {
            if (model) {
                if (model.STATUS === EduEnumsConsts.StatusAtivInscrita.NaoIniciada) {
                    return 'legenda-vermelho legenda-content-1';
                } else if (model.STATUS === EduEnumsConsts.StatusAtivInscrita.EmAndamento) {
                    return 'legenda-verde legenda-content-2';
                } else if (model.STATUS === EduEnumsConsts.StatusAtivInscrita.Concluida) {
                    return 'legenda-azul legenda-content-3';
                } else {
                    return '';
                }
            }
        }

        /**
         * Recarrega as rotas e seta a aba Inscritas como ativa
         */
        function reloadAndActiveTab() {
            $state.go($state.current, { tab: 'inscritas' }, { reload: true });
        }

    }
});


/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/atividades-extras.factory'], function(){});
 */
define(['widgets/widget.constants'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
        angular
        .module('eduAtividadesExtrasModule')
        .controller('eduAtividadesExtrasInscritasController', EduAtividadesExtrasInscritasController);


    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * eduAtividadesExtrasInscritasController.$inject = ['EduArquivosFactory'];
    */
    eduAtividadesExtrasInscritasController.$inject = ['$scope',
        '$rootScope',
        '$state',
        'eduWidgetsConsts'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * 
    * function eduAtividadesExtrasInscritasController(arquivosFactory){};
    */
    function eduAtividadesExtrasInscritasController($scope, $rootScope,$state, eduWidgetsConsts) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});