/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/atividades-extras.factory'], function(){});
 */
define(['widgets/widget.constants'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
    angular
        .module('eduAtividadesExtrasModule')
        .controller('EduAtividadesExtrasInscricaoController', EduAtividadesExtrasInscricaoController);

    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * EduAtividadesExtrasInscricaoController.$inject = ['EduArquivosFactory'];
    */
    EduAtividadesExtrasInscricaoController.$inject = ['$scope',
        '$rootScope',
        '$state',
        'eduWidgetsConsts'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * 
    * function EduAtividadesExtrasInscricaoController(arquivosFactory){};
    */
    function EduAtividadesExtrasInscricaoController($scope, $rootScope,$state, eduWidgetsConsts) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});
