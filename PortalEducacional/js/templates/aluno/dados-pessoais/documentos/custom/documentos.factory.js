/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduDocumentosModule
 * @name eduDocumentosFactory
 * @object factory
 *
 * @created 07/10/2016 v12.1.15
 * @updated
 *
 * @dependencies $totvsresource
 *
 * @description Factory utilizada para os documentos do Aluno.
 */
define(['aluno/dados-pessoais/documentos/documentos.module'], function () {

    'use strict';

    angular
        .module('eduDocumentosModule')
        .factory('eduDocumentosFactory', EduDocumentosFactory);

    EduDocumentosFactory.$inject = ['$totvsresource'];

    function EduDocumentosFactory($totvsresource) {

        var url = CONST_GLOBAL_URL_BASE_SERVICOS + 'TOTVSEducacional/Aluno/:method',
            factory = $totvsresource.REST(url, {}, {});

        factory.getDocumentosObrigatorios = getDocumentosObrigatorios;
        factory.getDocumentosObrigatoriosPorIdPerlet = getDocumentosObrigatoriosPorIdPerlet;

        return factory;

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function getDocumentosObrigatorios(callback) {
            var parameters = {
                method: 'DocumentosObrigatorios'
            };

            return factory.TOTVSGet(parameters, callback);
        }

        function getDocumentosObrigatoriosPorIdPerlet(idPerlet, idHabilitacaoFilial, callback) {
            var parameters = {
                method: 'DocumentosObrigatorios/{IdPerLet}/{IdHabilitacaoFilial}',
                IdPerlet: idPerlet,
                IdHabilitacaoFilial: idHabilitacaoFilial
            };

            return factory.TOTVSGet(parameters, callback);
        }
    }
});


/**
 * @description: Template para criação de factory.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 * 
 * define(['js/aluno/arquivos/arquivo.module'], function(){});
 */
define(['aluno/avaliacoes/avaliacoes.module'], function () {

    'use strict';

     angular
        .module('eduDocumentosModule')
        .factory('eduDocumentosFactory', EduDocumentosFactory);

    /**
    * Injeção do módulo e declaração  factory
    */
    EduDadosPessoaisFactory.$inject = [];

    /**
     *  Injeção das dependências da factory em forma de array.
     * 
     * Ex:
     * 
     * EduOcorrenciasController.$inject = ['$totvsresource'];
     */
    function EduDadosPessoaisFactory() {
        /**
               * Url do serviço que será consumido pela factory
               */
        var url = '';
        var factory;

        /**
         * Métodos expostos pela factory
         */
        factory.findRecords = findRecords;

        return factory;

        function findRecords(parameters, callback) {

            return;
        }
    }
});
