/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 * @description
 */

/**
 * @module eduResponsaveisModule
 * @name EduResponsaveisListController
 * @object controller
 *
 * @created 07/10/2016 v12.1.15
 * @updated
 *
 * @requires responsaveis.module
 *
 * @dependencies eduResponsaveisFactory
 *
 * @description Controller da exibição dos Responsaveis do Aluno
 */
define(['aluno/dados-pessoais/responsaveis/responsaveis.module',
        'aluno/dados-pessoais/responsaveis/responsaveis.factory',
        'utils/edu-enums.constants'
    ],
    function () {

        'use strict';

        angular
            .module('eduResponsaveisModule')
            .controller('eduResponsaveisListController', EduResponsaveisListController);

        EduResponsaveisListController.$inject = [
            '$rootScope',
            'TotvsDesktopContextoCursoFactory',
            'eduResponsaveisFactory',
            'i18nFilter',
            'totvs.app-notification.Service',
            'eduEnumsConsts'
        ];

        /**
         * Controller para a listagem dos responsáveis do aluno
         * @param {object} rootScope                Escopo principal
         * @param {object} objContextoCursoFactory  Objeto Factory para acesso aos serviços de contexto
         * @param {object} eduResponsaveisFactory   Objeto Factory para acesso aos serviços de responsáveis
         * @param {object} i18nFilter               Objeto para a tradução
         * @param {object} totvsNotification        Objeto para notificações
         * @param {object} EduEnumsConsts           Constantes do educacional
         */
        function EduResponsaveisListController(rootScope, objContextoCursoFactory, eduResponsaveisFactory, i18nFilter,
            totvsNotification, EduEnumsConsts) {

            var self = this;

            self.objResponsaveisList = [];
            self.objResponsaveisTempList = [];
            self.objStatusList = [];
            self.mascaraTelefone = '';
            self.exibirRespAcadFin = false;
            self.exibirRespTemp = false;
            self.podeAlterarRespAcadFin = false;
            self.podeIncluirRespTemp = false;
            self.podeAlterarRespTemp = false;

            self.TipoRespEnum = {
                Academico: 'A',
                Financeiro: 'F',
                Temporario: 'T'
            };

            self.widgetUrl = 'js/aluno/widgets/widget-pie.view.html';

            self.getContexto = getContexto;
            self.getMascara = getMascara;
            self.retornaWidgetUrl = retornaWidgetUrl;
            self.getResponsaveis = getResponsaveis;
            self.loadStatusList = loadStatusList;
            self.atualizarResponsavel = atualizarResponsavel;
            self.atualizarResponsavelAcad = atualizarResponsavelAcad;
            self.atualizarResponsavelFin = atualizarResponsavelFin;
            self.atualizarResponsavelTemp = atualizarResponsavelTemp;
            self.ensinoBasico = ensinoBasico;
            self.setPodeAlterar = setPodeAlterar;

            init();

            /**
             * Função de inicialização
             */
            function init() {
                self.getContexto();
                self.getMascara();
                self.loadStatusList();
                self.getResponsaveis();
            }

            /**
             * Obtém o contexto educacional
             */
            function getContexto() {
                self.objContexto = objContextoCursoFactory.getCursoSelecionado();
            }

            /**
             * Obtém a máscara para o telefone
             */
            function getMascara() {
                self.mascaraTelefone = objContextoCursoFactory.getMascarasSistema().mascaraTelefone;
            }

            /**
             * Carrega a lista de status
             */
            function loadStatusList() {
                var objAtivo = {},
                    objInativo = {};

                objAtivo.CODIGO = 'A';
                objAtivo.DESCRICAO = i18nFilter('l-ativo', [], 'js/aluno/dados-pessoais');

                objInativo.CODIGO = 'I';
                objInativo.DESCRICAO = i18nFilter('l-inativo', [], 'js/aluno/dados-pessoais');

                self.objStatusList.push(objAtivo);
                self.objStatusList.push(objInativo);
            }

            /**
             * Função para widget
             * @returns {string} caminho da visão do widget
             */
            function retornaWidgetUrl() {
                return 'js/aluno/widgets/widget-area.view.html';
            }

            /**
             * Verifica se é ensino básico
             * @returns {boolean} Verdadeiro se ensino básico
             */
            function ensinoBasico() {
                var blnReturn = (parseInt(self.objContexto.cursoSelecionado.APRESENTACAO) === EduEnumsConsts.EduTipoApresentacaoEnum.EnsinoBasico);
                return blnReturn;
            }

            /**
             * Verifica se pode alterar/incluir responsáveis
             * @param {string} tipo Tipo de responsável
             */
            function setPodeAlterar(tipo) {
                if (tipo === self.TipoRespEnum.Temporario) {
                    self.exibirRespTemp = angular.isDefined(rootScope.objPermissions.EDU_ACADEMICO_RESPONSAVEIS_TEMPORARIOS);
                    self.podeIncluirRespTemp = angular.isDefined(rootScope.objPermissions.EDU_ACADEMICO_RESPONSAVEIS_TEMPORARIOS_INCLUIR);
                    self.podeAlterarRespTemp = angular.isDefined(rootScope.objPermissions.EDU_ACADEMICO_RESPONSAVEIS_TEMPORARIOS_EDITAR);
                } else {
                    self.exibirRespAcadFin = angular.isDefined(rootScope.objPermissions.EDU_ACADEMICO_RESPONSAVEIS_ACADEMICOFINANCEIRO);
                    self.podeAlterarRespAcadFin = angular.isDefined(rootScope.objPermissions.EDU_ACADEMICO_RESPONSAVEIS_ACADEMICOFINANCEIRO_EDITAR);
                }
            }

            /**
             * Carrega a lista de responsáveis
             */
            function getResponsaveis() {
                self.objResponsaveisList = [];
                self.objResponsaveisTempList = [];

                eduResponsaveisFactory.getResponsaveis(
                    function (result) {
                        if (result) {
                            if (angular.isArray(result.SAluno)) {
                                var objResp = result.SAluno[0];

                                if (objResp.RESPFINANCEIRO) {
                                    var objRespFin = new Responsavel();
                                    objRespFin.load(objResp, objResp.RESPFINANCEIRO, null, objResp.EMAILFCFO,
                                        objResp.TELEFONEFCFO, objResp.CELULARFCFO, objResp.DESCPARENTFCFO,
                                        self.TipoRespEnum.Financeiro);

                                    self.objResponsaveisList.push(objRespFin);
                                }

                                if (objResp.RESPACADEMICO) {
                                    var objRespAcad = new Responsavel();
                                    objRespAcad.load(objResp, objResp.RESPACADEMICO, objResp.IMGRACA, objResp.EMAILRESPACAD,
                                        objResp.TEL1RESPACAD, objResp.TEL2RESPACAD, objResp.DESCPARENTRACA,
                                        self.TipoRespEnum.Academico);

                                    self.objResponsaveisList.push(objRespAcad);
                                }
                            }

                            if (self.ensinoBasico()) {
                                if (angular.isArray(result.SALUNORESPONSAVEL)) {
                                    self.objResponsaveisTempList = result.SALUNORESPONSAVEL;
                                }
                            }

                            var myWatch = rootScope.$watch('objPermissions', function (data) {
                                if (data !== null) {
                                    self.setPodeAlterar(self.TipoRespEnum.Academico);
                                    self.setPodeAlterar(self.TipoRespEnum.Temporario);
                                    myWatch();
                                }
                            });
                        }
                    });
            }

            /**
             * Atualiza um responsável
             * @param {object} objResp Objeto responsável
             */
            function atualizarResponsavel(objResp) {

                if (!validaEmail(objResp.EMAIL)) {
                    return false;
                }

                if (objResp.TIPORESP === self.TipoRespEnum.Academico) {
                    self.atualizarResponsavelAcad(objResp);
                } else if (objResp.TIPORESP === self.TipoRespEnum.Financeiro) {
                    self.atualizarResponsavelFin(objResp);
                }
            }

            /**
             * Atualiza responsável acadêmico
             * @param {object} objResp Objeto responsável acadêmico
             */
            function atualizarResponsavelAcad(objResp) {
                objResp.TELEFONE1 = objResp.TELEFONE;
                objResp.TELEFONE2 = objResp.CELULAR;
                objResp.EMAILRESPACAD = objResp.EMAIL;
                delete objResp.load;

                var datatable = [objResp];
                eduResponsaveisFactory.atualizarResponsavelAcad(objResp.CODPESSOARACA, datatable);
            }

            /**
             * Atualiza responsável financeiro
             * @param {object} objResp Objeto responsável financeiro
             */
            function atualizarResponsavelFin(objResp) {
                objResp.TELEFONEFCFO = objResp.TELEFONE;
                objResp.CELULARFCFO = objResp.CELULAR;
                objResp.EMAILFCFO = objResp.EMAIL;
                delete objResp.load;

                var datatable = [objResp];
                eduResponsaveisFactory.atualizarResponsavelFin(objResp.COLIGADAFCFO, objResp.CODCFO, datatable);
            }

            /**
             * Atualiza responsável temporário
             * @param {object} objRespTemp Objeto responsável temporário
             */
            function atualizarResponsavelTemp(objRespTemp) {

                if (!validaEmail(objRespTemp.EMAIL)) {
                    return false;
                }

                var datatable = [objRespTemp];
                eduResponsaveisFactory.atualizarResponsavelTemp(objRespTemp.CODPESSOA, datatable);
            }

            /**
             * Classe de parse para a view
             */
            function Responsavel() {
                var vm = this;

                vm.NOME = '';
                vm.IMAGEM = '';
                vm.EMAIL = '';
                vm.TELEFONE = '';
                vm.CELULAR = '';
                vm.PARENTESCO = '';
                vm.RESPONSAVEL = '';
                vm.TIPORESP = '';
                vm.load = load;

                /**
                 * Carrega o objeto Responsável
                 * @param {object} objOrigin       Objeto original vindo do serviço
                 * @param {string} nome            Nome do responsável
                 * @param {string} imagem          Imagem do responsável
                 * @param {string} email           Email do responsável
                 * @param {string} telefone        Telefone do responsável
                 * @param {string} celular         Celular do responsável
                 * @param {string} parentesco      Parentesco do responsável
                 * @param {string} tipoResponsavel Tipo do responsável
                 */
                function load(objOrigin, nome, imagem, email, telefone, celular, parentesco, tipoResponsavel) {

                    vm.NOME = nome;
                    vm.IMAGEM = imagem;
                    vm.EMAIL = email;
                    vm.TELEFONE = telefone;
                    vm.CELULAR = celular;
                    vm.PARENTESCO = parentesco;
                    vm.TIPORESP = tipoResponsavel;

                    switch (tipoResponsavel) {
                        case self.TipoRespEnum.Academico:
                            vm.RESPONSAVEL = i18nFilter('l-Responsavel-Academico', [], 'js/aluno/dados-pessoais');
                            break;
                        case self.TipoRespEnum.Financeiro:
                            vm.RESPONSAVEL = i18nFilter('l-Responsavel-Financeiro', [], 'js/aluno/dados-pessoais');
                            break;
                        default:
                            vm.RESPONSAVEL = i18nFilter('l-Responsavel-Temporario', [], 'js/aluno/dados-pessoais');
                            break;
                    }

                    for (var name in objOrigin) {
                        if (objOrigin.hasOwnProperty(name)) {
                            vm[name] = objOrigin[name];
                        }
                    }
                }
            }

            function validaEmail (mail) {
                if (mail === undefined) {
                    totvsNotification.notify({
                        type: 'warning',
                        title: i18nFilter('l-Atencao', [], 'js/aluno/dados-pessoais'),
                        detail: i18nFilter('l-email-invalido', [], 'js/aluno/dados-pessoais')
                    });
                    return false;
                }
                return true;
            }
        }
    });

/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/arquivo.factory'], function(){});
 */
define(['aluno/dados-pessoais/dados-pessoais.module'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
       angular
            .module('eduResponsaveisModule')
            .controller('eduResponsaveisController', EduResponsaveisListController);

    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * EduResponsaveisListController.$inject = ['EduArquivosFactory'];
    */
    EduResponsaveisListController.$inject = ['$scope',
        '$rootScope',
        '$state'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * 
    * function EduResponsaveisListController(arquivosFactory){};
    */
    function EduResponsaveisListController($scope, $rootScope, $state) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});
