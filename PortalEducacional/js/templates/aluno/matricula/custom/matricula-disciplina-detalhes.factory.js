/**
 * @description: Template para criação de factory.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 * 
 * define(['js/aluno/arquivos/arquivo.module'], function(){});
 */
define(['aluno/matricula/matricula-disciplina.module'], function () {

    'use strict';

   angular
        .module('eduMatriculaDisciplinaModule')
        .factory('MatriculaDisciplinaDetalhesFactory', MatriculaDisciplinaDetalhesFactory); 


    /**
    * Injeção do módulo e declaração  factory
    */
    MatriculaDisciplinaDetalhesFactory.$inject = [];

    /**
     *  Injeção das dependências da factory em forma de array.
     * 
     * Ex:
     * 
     * EduOcorrenciasController.$inject = ['$totvsresource'];
     */
    function MatriculaDisciplinaDetalhesFactory() {
        /**
               * Url do serviço que será consumido pela factory
               */
        var url = '';
        var factory;

        /**
         * Métodos expostos pela factory
         */
        factory.findRecords = findRecords;

        return factory;

        function findRecords(parameters, callback) {

            return;
        }
    }
});

