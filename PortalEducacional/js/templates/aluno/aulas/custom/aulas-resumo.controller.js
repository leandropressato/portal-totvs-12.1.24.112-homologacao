/**
 * @license TOTVS | Portal - TOTVS Educacional v12.1.15
 * (c) 2015-2016 TOTVS S/A https://www.totvs.com
 * License: Comercial
 */

/**
 * @module eduAulasModule
 * @name eduAulasResumoListController
 * @object controller
 *
 * @created 2016-11-23 v12.1.15
 * @updated
 *
 * @requires Aulas.module
 *
 * @dependencies eduAulasFactory
 *
 * @description Controller do resumo da aula
 */
define([
    'aluno/aulas/aulas.module',
    'aluno/aulas/aulas.route',
    'aluno/aulas/aulas.factory',
    'aluno/disciplina/disciplina.factory'
], function () {

    'use strict';

    angular.module('eduAulasModule')
        .controller('eduAulasResumoListController', eduAulasResumoListController);

    eduAulasResumoListController.$inject = ['$scope',
        '$filter',
        '$state',
        'eduAulasFactory',
        'parametros',
        '$sce',
        '$modalInstance',
        'eduDisciplinaFactory'
    ];

    function eduAulasResumoListController($scope,
        $filter,
        $state,
        eduAulasFactory,
        parametros,
        $sce,
        $modalInstance,
        eduDisciplinaFactory) {

        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************

        self.getDescTipoAula = getDescTipoAula;
        self.onclickMaisInfoDisciplina = onclickMaisInfoDisciplina;
        self.onclickMaisInfoAula = onclickMaisInfoAula;
        self.modalInstance = $modalInstance;

        // *********************************************************************************
        // *** Controller Initialize
        // *********************************************************************************
        init();

        function init() {

            self.idTurmaDisc = parametros.idTurmaDisc;
            self.idPlanoAula = parametros.idPlanoAula;
            self.exibeLicaoCasa = parametros.exibeLicaoCasa;

            buscarDados();

        }

        // *********************************************************************************
        // *** Functions
        // *********************************************************************************

        function buscarDados() {

            eduAulasFactory.retornarDetalhesAula(self.idTurmaDisc, self.idPlanoAula, function (result) {
                if (result) {

                    self.aula = result.SPlanoAula[0];

                    /* trata campos com possíveis tags HTML */
                    self.aula.CONTEUDO = $sce.trustAsHtml(self.aula.CONTEUDO);
                    self.aula.CONTEUDOEFETIVO = $sce.trustAsHtml(self.aula.CONTEUDOEFETIVO);
                    self.aula.LICAOCASA = $sce.trustAsHtml(self.aula.LICAOCASA);

                    self.aula.local = eduAulasFactory.montarDescricaoLocal(self.aula.PREDIO, self.aula.BLOCO, self.aula.SALA);
                }
            });

        }

        function getDescTipoAula(tipoAula) {

            return eduAulasFactory.getDescTipoAula(tipoAula);

        }

        /**
         * Redireciona para a página da turma/disciplina
         *
         */
        function onclickMaisInfoDisciplina() {

            eduDisciplinaFactory.ExibirInfoDisciplina(self.idTurmaDisc, self.aula.CODDISC, function () {
                $modalInstance.dismiss();
            });

        }

        /**
         * Redireciona para a página da aula
         *
         */
        function onclickMaisInfoAula() {

            eduAulasFactory.exibirInfoAula(self.idTurmaDisc, self.idPlanoAula, function () {
                $modalInstance.dismiss();
            });

        }

    }
});

/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/arquivo.factory'], function(){});
 */
define(['widgets/widget.constants'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
    angular.module('eduAulasModule')
        .controller('eduAulasResumoListController', eduAulasResumoListController);

    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * eduAulasResumoListController.$inject = ['EduArquivosFactory'];
    */
    eduAulasResumoListController.$inject = ['$scope',
        '$rootScope',
        '$state',
        'eduWidgetsConsts'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * 
    * function eduAulasResumoListController(arquivosFactory){};
    */
    function eduAulasResumoListController($scope, $rootScope, $state, eduWidgetsConsts) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});
