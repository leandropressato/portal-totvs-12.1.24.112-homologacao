/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/arquivo.factory'], function(){});
 */
define(['widgets/widget.constants'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
    angular.module('eduAulasModule')
        .controller('eduAulasListController', EduAulasListController);

    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * EduAulasListController.$inject = ['EduArquivosFactory'];
    */
    EduAulasListController.$inject = ['$scope',
        '$rootScope',
        '$state',
        'eduWidgetsConsts'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * EduAulasListController
    * function EduAulasListController(arquivosFactory){};
    */
    function EduAulasListController($scope, $rootScope, $state, eduWidgetsConsts) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});
