/**
 * @description: Template para criação de controller.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 *
 * define(['js/aluno/arquivos/arquivo.factory'], function(){});
 */
define(['aluno/disciplina/disciplina.module'], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller
     */
       angular
        .module('eduDisciplinaModule')
        .controller('eduDisciplinaComplementoController', EduDisciplinaComplementoController);


    /**
    * Injeção das dependências do controller em forma de array.
    * 
    * Ex:
    * 
    * EduDisciplinaComplementoController.$inject = ['EduArquivosFactory'];
    */
    EduDisciplinaComplementoController.$inject = ['$scope',
        '$rootScope',
        '$state'];

    /**
    * Função construtora do controller.
    * Nela deve conter toda a dependência injetada, como parâmetro.
    * 
    * Ex:
    * 
    * function EduDisciplinaComplementoController(arquivosFactory){};
    */
    function EduDisciplinaComplementoController($scope, $rootScope, $state) {

        var self = this;

        init();

        /**
        * Função disparada para iniciar o controller.
        * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
        * 
        */
        function init() {

        }
    }
});

