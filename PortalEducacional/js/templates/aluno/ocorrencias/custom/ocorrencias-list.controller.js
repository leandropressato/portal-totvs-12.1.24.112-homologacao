/**
 * @description: Template para criação de controller de ocorrências.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 * 
 * define(['js/aluno/ocorrencias/ocorrencias.factory'], function(){});
 */
define([], function () {

    'use strict';

    /**
     * Injeção do módulo e declaração do  controller de ocorrências
     */
    angular
        .module('eduOcorrenciasModule')
        .controller('EduOcorrenciasController', EduOcorrenciasController);

    /**
     * Injeção das dependências do controller em forma de array.
     * 
     * Ex:
     * 
     * EduOcorrenciasController.$inject = ['EduOcorrenciaFactory'];
     */
    EduOcorrenciasController.$inject = ['$scope'];

    /**
     * Função construtora do controller.
     * Nela deve conter toda a dependência injetada, como parâmetro.
     * 
     * Ex:
     * 
     * function EduOcorrenciasController(ocorrenciaFactory){};
     */
    function EduOcorrenciasController($scope) {

        var self = this;
        
        init();

        /**
         * Função disparada para iniciar o controller.
         * Nela deve conter as chamadas das funções responsáveis pelo carregamento da tela.
         * 
         */
        function init() {
        
        }
    }
});
