/**
 * @description: Template para criação de factory.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 * 
 * define(['js/aluno/ocorrencias/ocorrencias.module'], function(){});
 */
define(['aluno/ocorrencias/ocorrencias.module'], function () {

    'use strict';

     /**
     * Injeção do módulo e declaração  factory de ocorrências
     */
    angular
        .module('eduOcorrenciasModule')
        .factory('eduOcorrenciasFactory', EduOcorrenciasFactory);

    /**
     *  Injeção das dependências da factory em forma de array.
     * 
     * Ex:
     * 
     * EduOcorrenciasController.$inject = ['$totvsresource'];
     */
    EduOcorrenciasFactory.$inject = [];

    /**
     * Função construtora da Factory
    */
    function EduOcorrenciasFactory() {

        /**
         * Url do serviço que será consumido pela factory
         */
        var url = '';
        var factory;

        /**
         * Métodos expostos pela factory
         */
        factory.findRecords = findRecords;
        
        return factory;

        function findRecords(parameters, callback) {
            
            return factory.TOTVSQuery(parameters, callback);
        }
    }
});
