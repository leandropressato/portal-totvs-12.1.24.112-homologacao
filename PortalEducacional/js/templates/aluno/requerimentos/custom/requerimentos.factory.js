/**
 * @description: Template para criação de factory.
 */

/**
 * Usado para informar a dependência do modulo,
 * onde o primeiro argumento é um array com a  indicação caminho/nome da dependência e 
 * o segundo é a declaração de uma função.
 * 
 * Ex:
 * 
 * define(['js/aluno/arquivos/arquivo.module'], function(){});
 */
define(['aluno/requerimentos/requerimentos.module'], function () {

    'use strict';

    angular
        .module('eduRequerimentosModule')
        .factory('eduRequerimentosFactory', EduRequerimentosFactory);


    /**
    * Injeção do módulo e declaração  factory
    */
    EduRequerimentosFactory.$inject = [];

    /**
     *  Injeção das dependências da factory em forma de array.
     * 
     * Ex:
     * 
     * EduOcorrenciasController.$inject = ['$totvsresource'];
     */
    function EduRequerimentosFactory() {
        /**
               * Url do serviço que será consumido pela factory
               */
        var url = '';
        var factory;

        /**
         * Métodos expostos pela factory
         */
        factory.findRecords = findRecords;

        return factory;

        function findRecords(parameters, callback) {

            return;
        }
    }
});

