[
    {
        "l-voce-esta-em": {
            "pt": "Você está em:",
            "en": "You are in:",
            "es": "Usted está en:"
        },
        "/": {
            "pt": "Mural",
            "en": "Mural",
            "es": "Mural"
        },
        "/arquivos": {
            "pt": "Arquivos",
            "en": "Files",
            "es": "Archivos"
        },
        "/atividades-curriculares": {
            "pt": "Atividades Curriculares",
            "en": "Curriculum Activities",
            "es": "Actividades curriculares"
        },
        "/avaliacoes": {
            "pt": "Avaliação Institucional",
            "en": "Institutional Evaluation",
            "es": "Evaluación institucional"
        },
        "/calendario": {
            "pt": "Calendário",
            "en": "Calendar",
            "es": "Calendario"
        },
        "/relatorios": {
            "pt": "Relatórios",
            "en": "Reports",
            "es": "Informes"
        },
        "/disciplinas": {
            "pt": "Disciplinas",
            "en": "Course Subjects",
            "es": "Materias"
        },
        "/disciplinas-eb": {
            "pt": "Disciplinas",
            "en": "Course Subjects",
            "es": "Materias"
        },
        "/disciplinas-es": {
            "pt": "Disciplinas",
            "en": "Course Subjects",
            "es": "Materias"
        },
        "/faltas": {
            "pt": "Faltas",
            "en": "Absences",
            "es": "Faltas"
        },
        "/financeiro": {
            "pt": "Financeiro",
            "en": "Financials",
            "es": "Financiero"
        },
        "/grade-curricular": {
            "pt": "Grade Curricular",
            "en": "Curriculum Grid",
            "es": "Mapa curricular"
        },
        "/historico": {
            "pt": "Histórico",
            "en": "History",
            "es": "Historial"
        },
        "/notas": {
            "pt": "Notas",
            "en": "Score",
            "es": "Notas"
        },
        "/ocorrencias": {
            "pt": "Ocorrências",
            "en": "Occurrences",
            "es": "Ocurrencias"
        },
        "/quadro-horario": {
            "pt": "Quadro de Horário",
            "en": "Timetable",
            "es": "Cuadro de horario"
        },
        "/requerimentos": {
            "pt": "Requerimento",
            "en": "Request",
            "es": "Requerimiento"
        },
        "l-aumentar-fonte": {
            "pt": "Aumentar tamanho da fonte",
            "en": "Increase size of source",
            "es": "Aumentar tamaño de la fuente"
        },
        "l-diminuir-fonte": {
            "pt": "Diminuir tamanho da fonte",
            "en": "Decrease size of source",
            "es": "Disminuir tamaño de la fuente"
        },
        "l-aplicar-contraste": {
            "pt": "Aplicar contraste",
            "en": "Apply contrast",
            "es": "Aplicar contraste"
        },
        "/eb/matricula/apresentacao": {
            "pt": "Rematrícula",
            "en": "Rematrícula",
            "es": "Rematrícula"
        },
        "/atividades-extras": {
            "pt": "Atividades extras",
            "en": "Atividades extras",
            "es": "Atividades extras"
        },
        "/desempenho": {
            "pt": "Desempenho",
            "en": "Desempenho",
            "es": "Desempenho"
        },
        "/nota-falta-unificada": {
            "pt": "Nota/Falta Unificada",
            "en": "Nota/Falta Unificada",
            "es": "Nota/Falta Unificada"
        }
    }
]
