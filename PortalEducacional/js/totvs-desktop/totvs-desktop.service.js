/**
* @license TOTVS | Portal - TOTVS Educacional v12.1.15
* (c) 2015-2016 TOTVS S/A https://www.totvs.com
* License: Comercial
*/

define(['totvs-desktop/totvs-desktop.module',
        'totvs-desktop/totvs-desktop.factory',
        'totvs-desktop/totvs-desktop-contexto/totvs-desktop-contexto-curso.factory',
        'utils/edu-utils.factory',
        'utils/edu-enums.constants',
        'setup/sso/sso.factory'], function () {

    'use strict';

    angular
        .module('totvsDesktop')
        .service('totvsDesktopService', TotvsDesktopService);

    TotvsDesktopService.$inject = ['$rootScope',
                                   '$window',
                                   'totvsDesktopFactory',
                                   'eduUtilsFactory',
                                   'eduEnumsConsts',
                                   'TotvsDesktopContextoCursoFactory',
                                   'i18nFilter',
                                   'totvs.app-notification.Service',
                                   'eduSSOFactory'];

    /**
     * Service geral do portal
     * @param   {object} $rootScope             Objeto do escopo principal
     * @param   {object} $window                Objeto window angular
     * @param   {object} objTotvsDesktopFactory Objeto Factory
     * @param   {object} objEduUtilsFactory     Objeto Utils Factory
     * @param   {object} EduEnumsConsts         Objeto de Constantes do Educacional
     * @param   {object} objContextoFactory     Objeto Factory do Contexto
     * @param   {object} i18nFilter             Objeto para tradução
     * @param   {object} totvsNotification      Objeto para notificações
	 * @param   {object} eduSSOFactory          Dados de permissão do menu portal antigo
     * @returns {object} Service Totvs Desktop
     */
    function TotvsDesktopService($rootScope, $window, objTotvsDesktopFactory, objEduUtilsFactory, EduEnumsConsts,
                                  objContextoFactory, i18nFilter, totvsNotification, eduSSOFactory) {
        // *********************************************************************************
        // *** Variables
        // *********************************************************************************
        var self = this;

        //Objeto que contém as informações do usuário logado
        $rootScope.InformacoesLogin = null;

        //Objeto que contém todas as permissões utilizadas no sistema
        $rootScope.objPermissions = null;

        // *********************************************************************************
        // *** Public Properties and Methods
        // *********************************************************************************
        self.getParamURL = getParamURL;
        self.realizaLoginFrameHTML = realizaLoginFrameHTML;
        self.exibirMensagemErroAoLogar = exibirMensagemErroAoLogar;
        self.verificaUsuarioLogadoSetInfoLogin = verificaUsuarioLogadoSetInfoLogin;
        self.loadPermissions = loadPermissions;
        self.redirecionarParaPaginaLogin = redirecionarParaPaginaLogin;
        self.loadParamsHeader = loadParamsHeader;
        self.getMenuPortalEducacional = getMenuPortalEducacional;
        self.getMenuCorporeRMAsync = getMenuCorporeRMAsync;
        self.getIconsMenu = getIconsMenu;
        self.verificaUsuarioLogadoAsync = verificaUsuarioLogadoAsync;
        self.TipoIntegracaoBibliotecaria = 0;

        // *********************************************************************************
        // *** Public Methods
        // *********************************************************************************

        /**
         * @public
         * @function Obtém os parâmetros da URL
         * @name getParamURL
         * @param   {string} nomeParametro Nome do parâmetro
         * @returns {object} Objeto parâmetro
         */
        function getParamURL(nomeParametro) {
            var results = new RegExp('[\?&]' + nomeParametro + '=([^&#]*)').exec($window.location.href);

            if (angular.isArray(results) && results.length > 1) {
                return decodeURIComponent(results[1]);
            } else {
                return undefined;
            }
        }

        /**
         * @public
         * @function Realiza o Login no FrameHTML
         * @name realizaLoginFrameHTML
         * @param {string}   key      Chave para logar
         * @callback Função de retorno ao logar
         */
        function realizaLoginFrameHTML(key, callback) {

            objTotvsDesktopFactory.realizarAutoLoginAsync(key, function (result) {

                if (result && !result.Excessao) {

                    if (angular.isFunction(callback)) {
                        callback();
                    }
                }
                else {
                    self.exibirMensagemErroAoLogar(function () {
                        $window.location.href = $window.location.href.split('#')[0] + 'login';
                    });
                }
            });
        }

        /**
         * @public
         * @function Exibe uma mensagem de erro ao logar
         * @name exibirMensagemErroAoLogar
         * @callback Função de retorno após exibir a mensagem
         */
        function exibirMensagemErroAoLogar(callback) {

            totvsNotification.message({
                title: i18nFilter('l-titulo-erro-login'),
                text: i18nFilter('l-msg-erro-login'),
                size: 'md', // sm = small | md = medium | lg = larger,
                callback: callback
            });
        }

        /**
         * @public
         * @function Verifica se o usuário está logado e set informações de login
         * @name verificaUsuarioLogadoAsync
         * @callback Função de retorno caso esteja logado
         */
        function verificaUsuarioLogadoSetInfoLogin(callback) {
            objTotvsDesktopFactory.verificaUsuarioLogadoAsync(function (result) {

                if (result.value) {

                    //Retorna as informações do usuário logado
                    objContextoFactory.getInformacoesUsuarioLogadoAsync(function (result) {

                        if (result) {

                            //Registra as informações do usuário logado no $rootScope
                            $rootScope.InformacoesLogin = result;

                            //Inicializa a aplicação
                            objContextoFactory.buscarRegistros({}, callback);
                        }
                        else {
                            //Caso não consiga retorna as informações do usuário logado
                            self.redirecionarParaPaginaLogin();
                        }
                    });
                }
                else {
                    //Caso o usuário não esteja logado no sistema, o mesmo será redirecionado para tela de login
                    self.redirecionarParaPaginaLogin();
                }
            });
        }

        /**
         * @public
         * @function Carrega as permissões da aplicação
         * @name eventosGlobais
         */
        function loadPermissions() {
            // Load permissions
            objTotvsDesktopFactory.getPermissions(
                function (result) {
                    if (angular.isDefined(result)) {
                        $rootScope.objPermissions = result;
                    }
                });
        }

        /**
         * @public
         * @function Redireciona para a url de login
         * @name redirecionarParaPaginaLogin
         */
        function redirecionarParaPaginaLogin() {
            var actionRedirect = $window.location.href.split('/#/')[1];
            if (actionRedirect === '') {
                $window.location.href = $window.location.href.split('#')[0] + 'login';
            }
            else {
                $window.location.href = $window.location.href.split('#')[0] + 'login?redirect=' + actionRedirect;
            }
        }

        /**
         * @public
         * @function Carrega os parâmetros para a customização do cabeçalho e menus
         * @name loadParamsHeader
         * @callback Função para retornar os parâmetros
         */
        function loadParamsHeader(callback) {
            objEduUtilsFactory.getParametrosTOTVSEducacionalAsync(function (objParams) {

                if (angular.isDefined(objParams)) {
                    var objParamsHeader = {
                        CorCabecalho: objParams.CorCabecalho,
                        CorFonteCabecalho: objParams.CorFonteCabecalho,
                        CorMenu: objParams.CorMenu,
                        CorFonteMenu: objParams.CorFonteMenu,
                        HotLink1: objParams.HotLink1,
                        HotLink2: objParams.HotLink2,
                        HotLink3: objParams.HotLink3,
                        LogoPortal: objParams.LogoPortal,
                        PermiteAlterarSenha: objParams.PermiteAlterarSenha,
                        ExibirNomeFilial: objParams.ExibirNomeFilial,
                        TipoIntegracaoBibliotecaria: objParams.TipoIntegracaoBibliotecaria
                    };

                    self.TipoIntegracaoBibliotecaria = objParamsHeader.TipoIntegracaoBibliotecaria;
                    
                    if (angular.isFunction(callback)) {
                        callback(objParamsHeader);
                    }
                }
            });
        }

        /**
         * @public
         * @function Carrega as listas com os menus
         * @name getMenuPortalEducacional
         * @callback Função para retornar os menus
         */
        function getMenuPortalEducacional(callback) {
            objTotvsDesktopFactory.getMenuPortalEducacional(function (menus) {
                customMenu(menus, function(menus){
                    callback(buildMenu(menus));
                });
            });
        }

		/**
		 * @public
		 * @function Carrega a lista de permissões do Corpore.Net
		 * @name getMenuCorporeRMAsync
		 * @callback Função para retornar as permissões do portal antigo Corpore.Net
		*/
        function getMenuCorporeRMAsync(callback) {
            eduSSOFactory.getMenuCorporeRMAsync(function (result) {
                if (typeof callback === 'function') {
                    callback(result);
                }
            });
        }

        /**
         * @public
         * @function Define-se os ícones dos menus
         * @name getIconsMenu
         * @param {int}    Identificador do Menu
         * @param {string} Url do Menu (Rota)
         * @returns {string} Classe CSS do menu
         */
        function getIconsMenu(idMenu, urlMenu) {
            var prefixIcon = 'ico-';

            if (urlMenu === '/') { // Se for Mural
                return prefixIcon + 'mural';
            }
            else if (urlMenu === '#') { // Se for submenu

                switch (idMenu) {
                    case 'EDU_PORTAL_ACADEMICO_CENTRALALUNO': // Central do aluno
                        return prefixIcon + 'central-aluno';
                        break;
                    case 'EDU_PORTAL_ACADEMICO_OPORTUNIDADES': // Oportunidades
                        return prefixIcon + 'oportunidades';
                        break;
                    case 'EDU_PORTAL_ACADEMICO_SECRETARIA': // Secretaria
                        return prefixIcon + 'secretaria';
                        break;
                    case 'EDU_PORTAL_ACADEMICO_URLSEXTERNAS': // Url's externas
                        return prefixIcon + 'links-uteis';
                        break;
                    }
            }
            else if (urlMenu && urlMenu !== '') { // Rota normal
                // Menus de customização receberão um ícone padrão.
                if (idMenu.substring(0, 4) === 'CST_')
                {
                    return prefixIcon + 'bookmark';
                }
                else if (urlMenu === '/es/matricula/apresentacao')
                {
                    return prefixIcon + 'user-plus';
                }
                else if (urlMenu === '/eb/matricula/apresentacao')
                {
                    return prefixIcon + 'user-check';
                }
                else if (idMenu === 'EDU_PORTAL_PERGAMUM'){
                    return prefixIcon + 'book';
                }
                else {
                    return prefixIcon + urlMenu.replace('/', '');
                }
            }
            else {
                return '';
            }
        }

        /**
         * @public
         * @function Verifica se o usuário está logado
         * @name verificaUsuarioLogadoAsync
         * @callback Função de retorno
         */
        function verificaUsuarioLogadoAsync(callback) {
            objTotvsDesktopFactory.verificaUsuarioLogadoAsync(function (result) {
                if (!result.value) {
                    self.exibirMensagemErroAoLogar(function () {
                        self.redirecionarParaPaginaLogin();
                    });
                }
                else {
                    if (angular.isFunction(callback)) {
                        callback(result);
                    }
                }
            });
        }

        // *********************************************************************************
        // *** Private Methods
        // *********************************************************************************

        /**
         * @private
         * @function Customiza o menu
         * @name customMenu
         * @param {Array} menus Lista de objetos de menu
         * @returns {Array} Lista com os menus do portal Educacional
         */
        function customMenu(menus, callback) {
            //Adiciona menu de acesso às funcionalidade do portal CorporeRM
            //Esse menu existirá temporáriamente apenas, até que o portal seja completamente migrado
            if (angular.isArray(menus) && menus.length > 0) {
                menus.push({'id':'EDU_PORTAL_CORPORERM','name': EDU_CONST_GLOBAL_NAME_MENU_PORTAL_CORPORERM,'url':'/corporerm','itens':[]});

                //Adiciona no menu o link para o Pergamum.
                //Caso esteja parametrizado na parametrização.
                if (self.TipoIntegracaoBibliotecaria === EduEnumsConsts.TipoIntegracaoBibliotecaria.Pergamum){
                    var url = '';
                    objTotvsDesktopFactory.getURLPergamum(function (result) {
                        url = result;
                        if (!angular.isUndefined(url)){
                            menus.push({'id':'EDU_PORTAL_PERGAMUM', 'name': i18nFilter('l-titulo-meu-pergamum'), 'url': url[0].URL, 'itens': []});
                            callback(menus);
                        }
                        else {
                            callback(menus);
                        }
                    });
                }
                else{
                    callback(menus);
                }
            }
        }

        /**
         * @private
         * @function Constrói os menus do portal
         * @name buildMenu
         * @param   {Array} menus Lista com os objetos de menu
         * @returns {Array} Lista com os objetos de menus da aplicação
         */
        function buildMenu(menus) {
            var objMenuList = [];
            angular.forEach(menus, function (value) {
                // Obtém os ícones dos menus
                value.icon = self.getIconsMenu(value.id, value.url);

                var objMenu = new Menu(value.id, value.name, value.url, value.icon);

                if (angular.isArray(value.itens) && value.itens.length > 0) {
                    objMenu.submenus = buildMenu(value.itens);
                }

                objMenuList.push(objMenu);
            });

            return objMenuList;
        }

        /**
         * @class
         * @classdesc Classe para definir o formato do menu
         * @param {string} id       Identificador do menu
         * @param {string} name     Nome do menu
         * @param {string} url      Rotas do menu
         * @param {string} icon     Classe CSS do ícone do menu
         * @param {Array}  submenus Lista de submenus
         */
        function Menu(id, name, url, icon, submenus) {
            this.id = id || 1;
            this.name = name || '[name]';
            this.url = url || '/';
            this.icon = icon || '[icon]';
            this.submenus = submenus || [];
        }
    }
});
